@if (Auth::user()->user_type == 1)
<aside class="main-sidebar">
    <section class="sidebar">
        <ul class="sidebar-menu" data-widget="tree">
            <li class="header">MAIN NAVIGATION</li>
            <li class="treeview">
                <a href="#">
                    <i class="fa fa-users"></i> <span> Users</span>
                    <span class="pull-right-container">
                        <i class="fa fa-angle-left pull-right"></i>
                    </span>
                </a>
                <ul class="treeview-menu">
                    <li><a href="{!! url('/allusers'); !!}"><i class="fa fa-circle-o"></i> All Users</a></li>
                    <li><a href="{!! url('/adduser'); !!}"><i class="fa fa-circle-o"></i> Add User</a></li>
                    <li><a href="{!! url('/users'); !!}"><i class="fa fa-circle-o"></i> Users</a></li>
                    <li><a href="{!! url('/admins'); !!}"><i class="fa fa-circle-o"></i> Admins</a></li>
                    <li><a href="{!! url('/retailers'); !!}"><i class="fa fa-circle-o"></i> Retailers</a></li>
                    <li><a href="{!! url('/alldeleted'); !!}"><i class="fa fa-circle-o"></i> All Deleted Users</a></li>
                </ul>
            </li>
            <li class="treeview">
                <a href="#">
                    <i class="fa fa-list-alt"></i>
                    <span> Categories</span>
                    <span class="pull-right-container">
                        <i class="fa fa-angle-left pull-right"></i>
                    </span>
                </a>
                <ul class="treeview-menu">
                    <li><a href="{!! url('/categories'); !!}"><i class="fa fa-circle-o"></i> All Categories</a></li>
                    <li><a href="{!! url('/category/create'); !!}"><i class="fa fa-circle-o"></i> Add
                            Category/SubCategory</a></li>
                </ul>
            </li>
            <li class="treeview">
                <a href="#">
                    <i class="fa fa-home"></i>
                    <span> Shops</span>
                    <span class="pull-right-container">
                        <i class="fa fa-angle-left pull-right"></i>
                    </span>
                </a>
                <ul class="treeview-menu">
                    <li><a href="{!! url('/shops'); !!}"><i class="fa fa-circle-o"></i> All Shops</a></li>
                    <li><a href="{!! url('/shop/create'); !!}"><i class="fa fa-circle-o"></i> Add Shop</a></li>
                    <li><a href="{!! url('/amenities'); !!}"><i class="fa fa-circle-o"></i> Shop Amenities</a></li>
                    <li><a href="{!! url('/payments'); !!}"><i class="fa fa-circle-o"></i> Shop accepted payment types</a></li>
                </ul>
            </li>
            <li>
            <li class="treeview">
                <a href="#">
                    <i class="fa fa-gift"></i>
                    <span> Wing Deals</span>
                    <span class="pull-right-container">
                        <i class="fa fa-angle-left pull-right"></i>
                    </span>
                </a>
                <ul class="treeview-menu">
                    <li><a href="{!! url('/deals'); !!}"><i class="fa fa-circle-o"></i> All Deals</a></li>
                    <li><a href="{!! url('/deal/create'); !!}"><i class="fa fa-circle-o"></i> Add Deal</a></li>
                </ul>
            </li>
            <li class="treeview">
                <a href="#">
                    <i class="fa fa-gift"></i>
                    <span>Offers</span>
                    <span class="pull-right-container">
                        <i class="fa fa-angle-left pull-right"></i>
                    </span>
                </a>
                <ul class="treeview-menu">
                    <li><a href="{!! url('/offers'); !!}"><i class="fa fa-circle-o"></i> All offers</a></li>
                    <li><a href="{!! url('/offer/create'); !!}"><i class="fa fa-circle-o"></i> Add Offer</a></li>
                </ul>
            </li>
            <li class="treeview">
                <a href="#">
                    <i class="fa fa-gift"></i>
                    <span>News</span>
                    <span class="pull-right-container">
                        <i class="fa fa-angle-left pull-right"></i>
                    </span>
                </a>
                <ul class="treeview-menu">
                    <li><a href="{!! url('/news'); !!}"><i class="fa fa-circle-o"></i> All NewsFeed</a></li>
                    <li><a href="{!! url('/news/create'); !!}"><i class="fa fa-circle-o"></i> Add Newsfeed</a></li>
                </ul>
            </li>
            <li class="treeview">
                <a href="#">
                    <i class="fa fa-gift"></i>
                    <span>Events</span>
                    <span class="pull-right-container">
                        <i class="fa fa-angle-left pull-right"></i>
                    </span>
                </a>
                <ul class="treeview-menu">
                    <li><a href="{!! url('/events'); !!}"><i class="fa fa-circle-o"></i> All Events</a></li>
                    <li><a href="{!! url('/events/create'); !!}"><i class="fa fa-circle-o"></i> Add Event</a></li>
                </ul>
            </li>
            <li class="treeview">
                <a href="#">
                    <i class="fa fa-gift"></i>
                    <span>Gifts</span>
                    <span class="pull-right-container">
                        <i class="fa fa-angle-left pull-right"></i>
                    </span>
                </a>
                <ul class="treeview-menu">
                    <li><a href="{!! url('/gifts'); !!}"><i class="fa fa-circle-o"></i> All gifts</a></li>
                    <li><a href="{!! url('/gift/create'); !!}"><i class="fa fa-circle-o"></i> Add gift</a></li>
                </ul>
            </li>

            <li class="treeview">
                <a href="#">
                    <i class="fa fa-gift"></i>
                    <span>Tokens</span>
                    <span class="pull-right-container">
                        <i class="fa fa-angle-left pull-right"></i>
                    </span>
                </a>
                <ul class="treeview-menu">
                    <li><a href="{!! url('/tokens'); !!}"><i class="fa fa-circle-o"></i> All Tokens</a></li>
                    <li><a href="{!! url('/token/create'); !!}"><i class="fa fa-circle-o"></i> Add Token</a></li>
                </ul>
            </li>
            <li class="treeview">
                <a href="#">
                    <i class="fa fa-gift"></i>
                    <span> Orders</span>
                    <span class="pull-right-container">
                        <i class="fa fa-angle-left pull-right"></i>
                    </span>
                </a>
                <ul class="treeview-menu">
                    <li><a href="#"><i class="fa fa-circle-o"></i> All Orders</a></li>
                    <li><a href="#"><i class="fa fa-circle-o"></i> Pending Orders</a></li>
                    <li><a href="#"><i class="fa fa-circle-o"></i> Finished Orders</a></li>
                </ul>
            </li>

            <li class="treeview">
                <a href="#">
                    <i class="fa fa-gift"></i>
                    <span>FAQs</span>
                    <span class="pull-right-container">
                        <i class="fa fa-angle-left pull-right"></i>
                    </span>
                </a>
                <ul class="treeview-menu">
                    <li><a href="{!! url('/faqs'); !!}"><i class="fa fa-circle-o"></i> All Faq</a></li>
                    <li><a href="{!! url('/faq/create'); !!}"><i class="fa fa-circle-o"></i> Add Faq</a></li>
                </ul>
            </li>

            <li class="treeview">
                <a href="#">
                    <i class="fa fa-gift"></i>
                    <span>Pages</span>
                    <span class="pull-right-container">
                        <i class="fa fa-angle-left pull-right"></i>
                    </span>
                </a>
                <ul class="treeview-menu">
                    <li><a href="{!! url('/pages'); !!}"><i class="fa fa-circle-o"></i> All Pages</a></li>
                    {{-- <li><a href="{!! url('/pages/create'); !!}"><i class="fa fa-circle-o"></i> Add Page</a></li> --}}
                </ul>
            </li>

            {{-- <li>
                <a href="/home_page">
                    <i class="fa fa-gift"></i>
                    <span> Home page</span>
                </a>
            </li> --}}



            <li class="treeview">
                <a href="#">
                    <i class="fa fa-gift"></i>
                    <span>Home page</span>
                    <span class="pull-right-container">
                        <i class="fa fa-angle-left pull-right"></i>
                    </span>
                </a>
                <ul class="treeview-menu">
                    <li><a href="{!! url('/slider/edit/1') !!}"><i class="fa fa-circle-o"></i> Slider 1 edit</a></li>
                    <li><a href="{!! url('/slider/edit/2') !!}"><i class="fa fa-circle-o"></i> Slider 1 edit</a></li>
                    <li><a href="{!! url('/imagediv/edit/1') !!}"><i class="fa fa-circle-o"></i> Images row 1 edit</a>
                    </li>
                    <li><a href="{!! url('/imagediv/edit/4') !!}"><i class="fa fa-circle-o"></i> Images row 2 edit</a>
                    </li>
                    <li><a href="{!! url('/imagediv/edit/2') !!}"><i class="fa fa-circle-o"></i> Images row 3 edit</a>
                    </li>
                    <li><a href="{!! url('/imagediv/edit/5') !!}"><i class="fa fa-circle-o"></i> Images row 4 edit</a>
                    </li>
                    <li><a href="{!! url('/imagediv/edit/3') !!}"><i class="fa fa-circle-o"></i> Images row 5 edit</a>
                    </li>
                    <li><a href="{!! url('/imagediv/edit/6') !!}"><i class="fa fa-circle-o"></i> Images row 6 edit</a>
                    </li>
                    <li><a href="{!! url('/imagediv/edit/7') !!}"><i class="fa fa-circle-o"></i> Images row 7 edit</a>
                    </li>
                    <li><a href="{!! url('/imagediv/edit/8') !!}"><i class="fa fa-circle-o"></i> Images row 8 edit</a>
                    </li>
                    <li><a href="{!! url('/imagediv/edit/9') !!}"><i class="fa fa-circle-o"></i> Images row 9 edit</a>
                    </li>

                </ul>
            </li>

            <li>
                <a href="{!! url('/admin/settings') !!}">
                    <i class="fa fa-cog"></i>
                    <span> Settings</span>
                </a>
            </li>
        </ul>
    </section>
</aside>
{{-- @else --}}

{{-- <aside class="main-sidebar">
    <section class="sidebar">
        <ul class="sidebar-menu" data-widget="tree">
            <li class="header">MAIN NAVIGATION</li>
            <li>
                <a href="{!! url('/deals') !!}">
                    <i class="fa fa-cog"></i>
                    <span> Deals</span>
                </a>
            </li>
            <li>
                <a href="{!! url('/offers') !!}">
                    <i class="fa fa-cog"></i>
                    <span> Offers</span>
                </a>
            </li>
        </ul>

    </section>
</aside> --}}

@endif
