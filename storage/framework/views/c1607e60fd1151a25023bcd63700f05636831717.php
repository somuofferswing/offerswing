<?php $__env->startSection('content'); ?>
<div class="container-fluid">
    <nav aria-label="breadcrumb">
        <ol class="breadcrumb">
            <li class="breadcrumb-item"><a href="/">Home</a></li>
            <li class="breadcrumb-item active" aria-current="page">About Us</li>
        </ol>
    </nav>

    <div class="row">
        <div class="col-md-12">
            <img src="<?php echo e(asset('main/images/1.jpg')); ?>" style="border-radius: 0px;height: 300px;width:100%"
                class="img-fluid">
        </div>
        <div class="row">
            <div class="col-md-3 px-3">
                <img src="<?php echo e(asset('main/images/icons/im.jpg')); ?>" height="200px" width="100%" class="img-fluid"
                    style=" border-radius: 10px;margin-top: 200px;">
            </div>

            <div class="col-md-6 px-3">
                <h1 class="text-center border-bottom">About Us</h1>
                <div class="" style="margin-top:50px;">
                    <p>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Offerswing.com is India's best app which will help you find the
                        best local discounts, deals, offers, and any local search. Instead of looking at all the
                        confusing advertisements around you, you are requested to have a look at our user-friendly app
                        “Offerswing.com” which saves your time, money and energy finding the best local deals. It will
                        help customers discover fantastic offers at their favorite outlets and this can also become a
                        sales channel for offline merchants.</p>

                    <p>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;We provide you the facility by which you can easily sort the shop
                        and its products by distance, price, brand value in all the 48 major and 250 subcategories
                        depending on location and we will get you the required notification which will tempt you to grab
                        them. Through this easy navigation system, you can find the essential information like vendor’s
                        location and shop timings at a single click.</p>
                    <p> &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;We being entevested some time miss the best ongoing deals in our
                        locality due to out of time having been occupied by some other work.</p>
                    <p> “ Offerswing “ is the app that is taken shape from the need and thought of “Why we have no space
                        to share the information with the people around “.
                    </p>

                    <br />

                </div>

            </div>

            <div class="col-md-3 px-3">
                <img src="<?php echo e(asset('main/images/icons/about2.png')); ?>" height="200px" width="100%" class="img-fluid"
                    style=" border-radius: 10px;margin-top: 200px;">
            </div>

        </div>



    </div>


    <div class="row">

        <div class="col-md-3" style="border-radius: 10px;margin-top: 80px;">
            <img src="<?php echo e(asset('main/images/icons/vendor1.png')); ?>" height="150px" width="auto" class="mgin1"
                style="padding-left: 200px;border-radius: 10px; margin-top: 50px;">
        </div>

        <div class="col-md-6">
            <div class="mgin1 git" style="margin-top:0px; border-radius: 10px">

                <h1 style="font-weight: 800; text-align: left; "> Our promises to vendors
                    <!--<hr style="border-bottom:2px solid #df685d;width:10%;margin:0px auto;">-->

                </h1>
                <div class="" style="margin-top:50px;">
                    <ol>
                        <li>Your Business promotion is our responsibility./ We will take your promotion work to the next
                            level.</li>
                        <li>We Bring The Best deals from our side. </li>
                        <li>We upgrade running offers & promotions on your dedicated page. </li>
                        <li>Transforming your business into digital India./ business in a digital way. </li>
                    </ol>
                </div>
            </div>
        </div>

        <div class="col-md-3" style="border-radius: 10px;margin-top:80px;">
            <p>&nbsp;</p>
            <img src="<?php echo e(asset('main/images/icons/ll.png')); ?>" height="200px" width="auto" class="mgin1"
                style=" border-radius: 10px">
        </div>


    </div>





    <div class="row">


        <div class="col-md-3" style="border-radius: 10px;margin-top: 0px;">
            <p>&nbsp;</p>
            <img src="<?php echo e(asset('main/images/icons/users.png')); ?>" height="150px" width="auto" class="mgin1"
                style="padding-left: 150px;border-radius: 10px;margin-top: 50px;">
        </div>

        <div class="col-md-6">
            <div class="mgin1 git" style="margin-top:0px; border-radius: 10px">

                <h1 style="font-weight: 800;text-align: left; ">Benefits to App Users
                    <!--<hr style="border-bottom:2px solid #df685d;width:10%;margin:0px auto;">-->
                </h1>
                <div class="" style="margin-top:50px;">
                    <ol>
                        <li>We provide you hassle-free registration, complimentary coupons, and referrals.</li>
                        <li>You will get the notifications for the best discounts and offers in your location.</li>
                        <li> Auto-detection of your location with the help of GPS to reach your favorite store. </li>
                    </ol>

                </div>
            </div>
        </div>


        <div class="col-md-3" style="border-radius: 10px;margin-top: 0px;">
            <p>&nbsp;</p>
            <img src="<?php echo e(asset('main/images/icons/users1.png')); ?>" height="150px" width="auto" class="mgin1"
                style="border-radius: 10px;margin-top: 50px;">
        </div>
    </div>
</div>
<?php $__env->stopSection(); ?>

<?php echo $__env->make('layouts.app', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH C:\wamp64\www\offers\resources\views/aboutus.blade.php ENDPATH**/ ?>