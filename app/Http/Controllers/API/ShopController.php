<?php

namespace App\Http\Controllers\API;

// use Illuminate\Http\Request;

use App\Http\Controllers\Controller;
use App\Models\Shop;

class ShopController extends Controller
{
    public $successStatus = 200;

    public function index()
    {
        return Shop::all();
    }

    public function getShopsbyCategory($id)
    {
        $sh = Shop::where('shop_category', $id)->get();

        if (!$sh->isEmpty()) {
            foreach ($sh as $key => $value) {
                $shop = Shop::find($value['id']);
                $shops[$key]['shop'] = $value;
                $shops[$key]['shop_images'] = $shop->images;
                $shops[$key]['offers'] = $shop->offers;
                $shops[$key]['deals'] = $shop->deals;
            }
            $result = $shops;
        } else {
            $result = 'No shops Found';
        }

        return response()->json(['status' => 'success', 'data' => $result], $this->successStatus);
    }

    public function getShopDetails($id)
    {
        $shop = Shop::find($id);
        if (!empty($shop)) {
            $shop_details['shop'] = $shop;
            $shop_details['shop_images'] = $shop->images;
            $shop_details['offers'] = $shop->offers;
            $shop_details['deals'] = $shop->deals;

            return response()->json(['status' => 'success', 'data' => $shop_details], $this->successStatus);
        }

        return response()->json(['status' => 'success', 'data' => 'No Data found'], $this->successStatus);
    }
}
