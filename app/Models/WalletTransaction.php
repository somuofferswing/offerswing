<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class WalletTransaction extends Model
{
    protected $table = 'wallet_transactions';

    protected $fillable = [
        'wallet_id', 'amount', 'hash', 'trans_type', 'accepted', 'meta','type'
    ];

    protected $casts = [
        'amount' => 'float',
        'meta' => 'json'
    ];

    /**
     * Retrieve the wallet from this transaction
     */
    public function wallet()
    {
        return $this->belongsTo(Wallet::class);
    }

    /**
     * Retrieve the amount with the positive or negative sign
     */
    public function getAmountWithSignAttribute()
    {
        return in_array($this->trans_type, ['deposit', 'refund'])
            ? '+' . $this->amount
            : '-' . $this->amount;
    }
}
