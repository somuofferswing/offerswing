@extends('layouts.app')



@section('content')

<style>
    /* card */

    .ps-post {

        padding: 0px;

    }



    .ps-post .ps-post__title {

        margin: 15px 0px;

    }



    .ps-post .ps-post__content p {

        color: inherit;

    }



    @media (max-width: 767px) {

        .ps-post--small-thumbnail .ps-post__thumbnail {

            margin: 0px;

        }

        .ps-post--small-thumbnail .ps-post__content {

            padding-left: 1.5rem;

        }



        .ps-post .ps-post__title {

            margin: 15px 0px;

        }

    }



    @media (min-width: 768px) {

        .ps-post {

            border-radius: 25px;

        }

        .ps-post .ps-post__thumbnail img {

            border-top-right-radius: 25px;

            border-bottom-right-radius: 25px;

        }

    }
</style>

<div class="ps-breadcrumb">

    <div class="container">

        <ul class="breadcrumb text-capitalize" id="breadcrumb">

            <li><a href="{{ url('/') }}">Home</a></li>

            <li>Shops</li>

        </ul>

    </div>

</div>

<section>

    <div class="ps-page--blog">

        <div class="container">

            <div class="ps-page__header">

                <h1>Shops List</h1>

                <hr class="highlighter">

            </div>

            <div class="ps-blog--sidebar">

                <div class="ps-blog__left pr-0">

                    @php

                    $category_id = request()->route('slug');

                    $category = \App\Models\Category::where(['slug'=>$category_id])->first();

                    $parent_category = $category->parentCategories;

                    @endphp



                    @if ($shops->isNotEmpty())

                    @foreach ($shops as $item)

                    <div class="ps-post ps-post--small-thumbnail ps-highlight">

                        <div class="ps-post__content">

                            <div class="ps-post__top">

                                <a class="ps-post__title"
                                    href="{!! url('/viewshop',[$item->slug]) !!}">{{ $item->name }}



                                    @if(Auth::guest())

                                    <span class="badge zoom ml-2" onClick="return false;" data-id="{{ $item->shopid }}"
                                        data-toggle="modal" data-target="#myModal1">

                                        <span data-toggle="tooltip" data-placement="right" title=""
                                            data-original-title="Like">

                                            <i class="fa fa-heart-o fa-lg text-danger"></i>

                                        </span>

                                    </span>

                                    @else

                                    @if (in_array($item->id,$likes))

                                    <span class="badge zoom ml-2 unlike" data-id="{{ $item->id }}" data-toggle="tooltip"
                                        data-placement="right" title="" data-original-title="Unlike">

                                        <i class="fa fa-heart fa-lg text-danger"></i>

                                    </span>

                                    @else

                                    <span class="badge zoom ml-2 like" data-id="{{ $item->id }}" data-toggle="tooltip"
                                        data-placement="right" title="" data-original-title="Like">

                                        <i class="fa fa-heart-o fa-lg text-danger"></i>

                                    </span>

                                    @endif

                                    @endif



                                </a>



                                <div class="ps-post__desc">

                                    <p><i class="fa fa-map-marker fa-lg text-warning pr-2"></i>{{ $item->address }}</p>

                                    <p><i class="fa fa-phone text-success pr-2"></i>{{ $item->phone_number }}</p>

                                    <p>
                                        <span class="p-2 border rounded">
                                            <span class="border-right mr-2">
                                                <strong class="defaultColor fontRighteous">OW</strong>
                                            </span>
                                            @for ($i = 0; $i < 5; $i++) 

                                                @if ($i< (int)$item->shop_rating)

                                                <span class="fa fa-star text-warning"></span>

                                                @else

                                                @if (($item->shop_rating-(int)$item->shop_rating) == 0.5 &&

                                                ((int)$item->shop_rating) ==

                                                $i )

                                                <span class="fa fa-star-half-o text-warning"></span>

                                                @else

                                                <span class="fa fa-star-o text-warning"></span>

                                                @endif

                                                @endif

                                            @endfor
                                            <strong>{{ $item->shop_rating }}</strong>
                                        </span>


                                            @php

                                            $deals = $item->dealsOngoing;

                                            $offers = $item->offersOngoing;

                                            @endphp

                                            @if ($offers->isNotEmpty())

                                            <span class="text-danger text-right pl-5"><i
                                                    class="fa fa-gift pr-2"></i>Offers</span>

                                            @endif



                                            @if ($deals->isNotEmpty())

                                            <span class="text-danger text-right pl-5"><i
                                                    class="fa fa-tags pr-2"></i>Deals</span>

                                            @endif

                                    </p>

                                    @if (in_array(date('N',time()) , explode(",",$item->active_days)))
                                        <p><strong class="text-success">Open Today</strong></p>
                                    @else
                                        <p><strong class="text-danger">Closed</strong></p>
                                    @endif
                                </div>

                            </div>

                        </div>



                        <div class="ps-post__thumbnail">

                            <a class="ps-post__overlay" href="{!! url('/viewshop',[$item->slug]); !!}"></a>

                            @php
                            $images = $item->shop_images;
                            $image = $images[0]['image_path'];
                            @endphp

                            <img src="{{ asset('storage/'.$image) }}" alt="">

                        </div>

                    </div>



                    @endforeach

                    @else

                    <h3 class="text-danger text-uppercase">No Shops Found</h3>

                    @endif

                </div>



                <div class="ps-blog__right d-none d-md-block">

                    <div class="ps-block__slider">

                        <div class="ps-carousel--product-box owl-slider" data-owl-auto="true" data-owl-loop="true"
                            data-owl-speed="7000" data-owl-gap="0" data-owl-nav="true" data-owl-dots="true"
                            data-owl-item="1" data-owl-item-xs="1" data-owl-item-sm="1" data-owl-item-md="1"
                            data-owl-item-lg="1" data-owl-duration="500" data-owl-mousedrag="off">

                            <a href="#">

                                <img src="http://nouthemes.net/html/martfury/img/slider/home-3/clothing-1.jpg" alt="">

                            </a>

                            <a href="#">

                                <img src="http://nouthemes.net/html/martfury/img/slider/home-3/clothing-2.jpg" alt="">

                            </a>

                            <a href="#">

                                <img src="http://nouthemes.net/html/martfury/img/slider/home-3/clothing-3.jpg" alt="">

                            </a>

                        </div>

                    </div>

                </div>



            </div>

        </div>

    </div>

</section>



<script>
    $(document).on('click', ".unlike", function (e) {

        e.preventDefault();

        var id = $(this).data('id');

        var element = this;



        var data = {

                shop_id:id

            };

        $.ajax({

                type: "post",

                url: '{{ url("removefavouriteshop") }}',

                headers: {

                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')

                 },

                data: data,

                cache: false,

                success: function (data)

                {

                    if(data.status == "success"){

                        $(element).removeClass('unlike').addClass('like').attr('data-original-title', 'Like').html('<i class="fa fa-heart-o text-danger"></i>');

                    }

                },

                error: function (data){

                    swal('Fail to run unlike..');

                }

            });

    });



    $(document).on('click', ".like", function (e) {

        e.preventDefault();

        var id = $(this).data('id');

        var element = this;



        var data = {

                shop_id:id

            };

        $.ajax({

            type: "post",

            url: '{{ url("addfavouriteshop") }}',

            headers: {

            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')

                },

            data: data,

            cache: false,

            success: function (data)

            {

                if(data.status == "success"){

                    $(element).removeClass('like').addClass('unlike').attr('data-original-title', 'Unlike').html('<i class="fa fa-heart text-danger"></i>');

                }

            },

            error: function (data){

                swal('Fail to run like..');

            }

        });

    });

</script>



<script>
    $(document).ready(function(){

        var category = JSON.parse('{!! json_encode($category) !!}');

        var parent_category = JSON.parse('{!! json_encode($parent_category) !!}');

        if(parent_category != null){

           var data =  toarray([parent_category]);

           var da = {

                    'id': category.id,

                    'category_name':category.category_name,

                    'parent_id':category.parent_id

                };



            data.unshift(da);

            data.reverse();

            breadcrumb(data);

        }else{

            var data = [];

            var da = {

                    'id': category.id,

                    'category_name':category.category_name,

                    'parent_id':category.parent_id

                };

            data.unshift(da);

            breadcrumb(data);



        }







        function toarray(data,cat){

            var categories = (cat== null) ? []: cat;

            data.forEach(element => {

                id = element.id;

                category_name = element.category_name;

                parent_id = element.parent_id;



                var data = {

                    'id': id,

                    'category_name':category_name,

                    'parent_id':parent_id

                };



                categories.push(data);

                var cat = categories;

                if(element.parent_categories) { toarray([element.parent_categories],cat); }



            });

            return categories;

        }



        function breadcrumb(data){

            data.forEach((element,key,array)=>{

                var id = element.id;

                var category_name = element.category_name;

                    category_name = category_name.toLowerCase();

                var parent_id = element.parent_id;

                    var li = `<li><a href="{{ url('subcategories') }}/`+id+`">`+category_name+`</a></li>`;

                    $('#breadcrumb').find(' > li:nth-last-child(1)').before(li);



            });

        }

        });



</script>

@endsection