<!DOCTYPE html>
<html lang="en">

<head>
    <title>Bootstrap Example</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.0/css/bootstrap.min.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.0/js/bootstrap.min.js"></script>
</head>

<body>

    <div class="container">
        <div class="row">
            <div class="col-md-8 col-md-offset-2" style="padding:5px !important;">
                <div calss="row">

                    <img src="http://www.offerswing.com/main/images/newlogo.png" style="height:100px"
                        class="img-responsive" />



                </div>
                <div class="row">
                    <div class="col-md-6 col-xs-12">
                        <h4>Gift receipt</h4>
                        <h5>Order no: <?php echo e($order->order_id); ?></h5>
                        
                    </div>
                    <div class="col-md-6 col-xs-12 text-right">
                        <h4>Address</h4>
                        <h5>Srikakulam</h5>

                        <h5>Andhra Pradesh</h5>

                        <div class="details" style="margin-top:20px;">
                            <h5>1234567890</h5>
                            <h5><a href="#">offerswing.com</a></h5>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <?php
                    $gift = $order->gift;
                    ?>
                    <div class="col-md-6 col-md-offset-3 border my-3">
                        <div class="image">
                            <img src="<?php echo asset("$gift->image"); ?>" class="img-fluid"
                                style="width:100%; height:200px;">
                        </div>
                        <hr>
                        <div class="dealbottom">
                            <h5>Gift Title : <span><?php echo e($gift->gift_name); ?></span> </h5>
                        </div>
                        <div class="text-center">
                            <?php echo QrCode::size(250)->generate($claimed->unique_code);; ?>


                        </div>

                        <div class="dealdescription ">

                            <table style="margin:0 auto;">
                                <tr>
                                    <td>Booking id</td>
                                    <td style="width:20px;">:</td>
                                    <td style="text-align:left;"><?php echo e($claimed->unique_code); ?></td>
                                </tr>
                                <tr>
                                    <td>Claimed item</td>
                                    <td style="width:20px;">:</td>
                                    <td style="text-align:left;"><?php echo e($gift->gift_name); ?></td>
                                </tr>
                                <tr>
                                    <td>Claimed on</td>
                                    <td style="width:20px;">:</td>
                                    <td style="text-align:left;">
                                        <?php echo e(date('d-m-Y h:i A',strtotime($claimed->created_at))); ?></td>
                                </tr>
                                
                            </table>

                        </div>

                        <div class="storeaddress text-center">
                            <h5>Our team will contact you soon</h5>


                        </div>

                        

                    </div>

                </div>

            </div>
        </div>
    </div>

</body>

</html>
<?php /**PATH /home/offerkrx/public_html/offerswing/resources/views/email/order_gift_details.blade.php ENDPATH**/ ?>