<?php $__env->startSection('content'); ?>

<div class="content-wrapper">

    <section class="content-header">

        <h1>

            Wing Deals

            <small>All Wing Deals</small>

        </h1>

        

    </section>

    <section class="content">

        <?php if(session()->get('success')): ?>

        <div class="alert alert-success" id="success">

            <?php echo e(session()->get('success')); ?>


        </div><br />

        <?php endif; ?>

        <?php if(Auth::user()->user_type == 1): ?>

























































































        <?php endif; ?>



        <div class="box">

            <div class="box-header with-border">

                <h3 class="box-title">All Offers</h3>

                <?php if(Auth::user()->user_type == 1): ?>

                <a href="<?php echo url('/offer/create');; ?>" class="pull-right btn btn-info">Add Offer</a>

                <?php endif; ?>

            </div>

            <div class="box-body">

                <div class="table-responsive">

                    <table class="table table-striped">

                        <thead>

                            <tr>

                                <th>OfferID</th>

                                <th>Name</th>

                                <th>Description</th>

                                <th>Start Date</th>

                                <th>End Date</th>

                                <th>Shop Name</th>



                                <th colspan="2">Action</th>

                            </tr>

                        </thead>

                        <tbody>

                            <?php $__currentLoopData = $offers; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $offer): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>

                            <tr>

                                <td><?php echo e($offer->Offer_id); ?></td>

                                <td><?php echo e($offer->offer_name); ?></td>

                                <td><?php echo $offer->description; ?></td>

                                <td><?php echo e($offer->from_date); ?></td>

                                <td><?php echo e($offer->to_date); ?></td>

                                <td><?php echo e($offer->name); ?></td>



                                <?php if(Auth::user()->user_type == 1): ?>

                                <td><a class="btn btn-default"

                                        href="<?php echo e(route('edit-offer', [$offer->Offer_id])); ?>">Edit</a>

                                </td>



                                <td>

                                    <form action="<?php echo e(route('delete-offer', [$offer->Offer_id])); ?>" method="post">

                                        <?php echo csrf_field(); ?>

                                        <?php echo method_field('DELETE'); ?>

                                        <button class="btn btn-danger" type="submit">Delete</button>

                                    </form>

                                </td>

                                <?php else: ?>

                                <td><a class="btn btn-default" href="<?php echo e(route('view-offer', [$offer->Offer_id])); ?>">view

                                        details</a>

                                </td>

                                <?php endif; ?>

                            </tr>

                            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>

                        </tbody>

                    </table>

                </div>

            </div>



        </div>

    </section>

</div>

<?php $__env->stopSection(); ?>

<?php $__env->startSection('scripts'); ?>

<script>

    $('#success').delay(5000).fadeOut('slow');

    $('#reset').on('click',function(){

        // $("#filter")[0].reset();

        $('#shop_id').val('');

        $('#category_id').val('');

        $('#filter')[0].submit();

    });



</script>

<?php $__env->stopSection(); ?>

<?php echo $__env->make('layouts.admin', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH C:\wamp64\www\offers\resources\views/backend/offers/index.blade.php ENDPATH**/ ?>