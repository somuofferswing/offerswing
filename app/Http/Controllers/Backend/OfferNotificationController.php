<?php

namespace App\Http\Controllers\Backend;

use App\Http\Controllers\Controller;
use App\Models\OfferNotification;

class OfferNotificationController extends Controller
{
    public function getUserNotification()
    {
        $user = auth()->user();
        $user_id = $user['id'];

        $notifications = OfferNotification::where('user_id', $user_id)->get();
        $read_notifications = OfferNotification::where('user_id', $user_id)->where('is_read', 1)->get();
        $unread_notifications = OfferNotification::where('user_id', $user_id)->where('is_read', 0)->get();

        return view('backend.notifications', compact('read_notifications', 'unread_notifications', 'notifications'));
    }
}
