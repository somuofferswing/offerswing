<?php $__env->startSection('content'); ?>
<style>

</style>

<div class="container d-flex flex-column">
    <?php
    $deal = $order->deal;
    ?>

    <div class="row ">
        <h4 class="mx-auto">Your Deal Order: <?php echo e($order->order_id); ?></h4>
        <hr>
        <div class="col-md-8 offset-2 border my-3">
            <div class="image">
                <img src="<?php echo asset("$deal->pic"); ?>" class="img-fluid" style="width:100%; height:200px;">
            </div>
            <hr>
            <div class="dealbottom">
                <h5>
                    Claimed Date : <span><?php echo e(date('d-m-Y',strtotime($claimed->created_at))); ?></span>

                    <span class="float-right">Expires on : <?php echo e(date('d-m-Y',strtotime($claimed->valid_upto))); ?></span>
                </h5>
            </div>
            <div class="text-center">
                <?php echo QrCode::size(250)->generate($claimed->unique_code);; ?>

                
            </div>

            <div class="dealdescription py-3">

                <table style="margin:0 auto;">
                    <tr>
                        <td>Booking id</td>
                        <td style="width:20px;">:</td>
                        <td style="text-align:left;"><?php echo e($claimed->unique_code); ?></td>
                    </tr>
                    <tr>
                        <td>About Deal</td>
                        <td style="width:20px;">:</td>
                        <td style="text-align:left;"><?php echo e($deal->discount_percentage); ?>% Off</td>
                    </tr>
                    <tr>
                        <td>Brought on</td>
                        <td style="width:20px;">:</td>
                        <td style="text-align:left;"><?php echo e(date('d-m-Y h:i A',strtotime($claimed->created_at))); ?></td>
                    </tr>
                    <tr>
                        <td>Expires on</td>
                        <td style="width:20px;">:</td>
                        <td style="text-align:left;"><?php echo e(date('d-m-Y h:i A',strtotime($claimed->valid_upto))); ?></td>
                    </tr>
                </table>

            </div>

            <div class="storeaddress">
                <?php
                    $shop = $deal->shop;
                ?>
                <p> <b> Store Address : </b> <?php echo e($shop->address.",".$shop->city.",".$shop->state.",".$shop->pincode); ?></p>
                <p> <b> Contact Number : </b> +91 <?php echo e($shop->phone_number.",". $shop->phone_number2); ?></p>

            </div>
            
            <div class="storecondition">
            <p><b>Terms & Conditions :</b> <?php echo $deal->terms_conditions; ?></p>
                
            </div>

        </div>





    </div>

</div>
<?php $__env->stopSection(); ?>

<?php echo $__env->make('layouts.app', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH C:\wamp64\www\offers\resources\views/deals/orders.blade.php ENDPATH**/ ?>