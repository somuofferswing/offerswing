<!DOCTYPE html>

<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">



<head>

    <meta charset="utf-8">

    <meta http-equiv="X-UA-Compatible" content="IE=edge">

    <meta name="viewport" content="width=device-width, initial-scale=1.0">

    <meta name="format-detection" content="telephone=no">

    <meta name="apple-mobile-web-app-capable" content="yes">

    <meta name="author" content="">

    <meta name="keywords" content="">

    <meta name="description" content="">

    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>Offerswing</title>

    <style>

      :root {

        --main-bg-color: #fe5347;  

      }

    </style>

    <link href="https://fonts.googleapis.com/css?family=Work+Sans:300,400,500,600,700&amp;amp;subset=latin-ext" rel="stylesheet">

    <link rel="stylesheet" href="{{ asset('/frontend/plugins/font-awesome/css/font-awesome.min.css') }}">

    <link rel="stylesheet" href="{{ asset('/frontend/fonts/Linearicons/Linearicons/Font/demo-files/demo.css') }}">

    <link rel="stylesheet" href="{{ asset('/frontend/plugins/bootstrap4/css/bootstrap.min.css') }}">

    <link rel="stylesheet" href="{{ asset('/frontend/plugins/owl-carousel/assets/owl.carousel.css') }}">

    <link rel="stylesheet" href="{{ asset('/frontend/plugins/slick/slick/slick.css') }}">

    <link rel="stylesheet" href="{{ asset('/frontend/plugins/lightGallery-master/dist/css/lightgallery.min.css') }}">

    <link rel="stylesheet" href="{{ asset('/frontend/plugins/jquery-bar-rating/dist/themes/fontawesome-stars.css') }}">

    <link rel="stylesheet" href="{{ asset('/frontend/plugins/jquery-ui/jquery-ui.min.css') }}">

    <link rel="stylesheet" href="{{ asset('/frontend/plugins/select2/dist/css/select2.min.css') }}">

    <link rel="stylesheet" href="{{ asset('/frontend/css/spinkit.min.css') }}">

    <link rel="stylesheet" href="{{ asset('/frontend/css/style.css') }}">

    <link rel="stylesheet" href="{{ asset('/frontend/css/current.css') }}">
    @yield("css")

    <!-- <script src="https://kit.fontawesome.com/d13f3c35d9.js" crossorigin="anonymous"></script> -->

    <script src="{{ asset('frontend/plugins/jquery-1.12.4.min.js') }}"></script>

    <script src="{{ asset('frontend/plugins/popper.min.js') }}"></script>

    <script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>
    <script src="{{ asset('js/share.js') }}"></script>

</head>

<body>

        <!-- header -->

        @include('layouts.mainheader')

        

        <!-- content -->

        @yield('content')



        <!-- footer -->

        @include('layouts.mainfooter')



    <script src="{{ asset('frontend/plugins/owl-carousel/owl.carousel.min.js') }}"></script>

    <script src="{{ asset('frontend/plugins/bootstrap4/js/bootstrap.min.js') }}"></script>

    <script src="{{ asset('frontend/plugins/imagesloaded.pkgd.min.js') }}"></script>

    <script src="{{ asset('frontend/plugins/masonry.pkgd.min.js') }}"></script>

    <script src="{{ asset('frontend/plugins/isotope.pkgd.min.js') }}"></script>

    <script src="{{ asset('frontend/plugins/jquery.matchHeight-min.js') }}"></script>

    <script src="{{ asset('frontend/plugins/slick/slick/slick.min.js') }}"></script>

    <script src="{{ asset('frontend/plugins/jquery-bar-rating/dist/jquery.barrating.min.js') }}"></script>

    <script src="{{ asset('frontend/plugins/slick-animation.min.js') }}"></script>

    <script src="{{ asset('frontend/plugins/lightGallery-master/dist/js/lightgallery-all.min.js') }}"></script>

    <script src="{{ asset('frontend/plugins/jquery-ui/jquery-ui.min.js') }}"></script>

    <script src="{{ asset('frontend/plugins/sticky-sidebar/dist/sticky-sidebar.min.js') }}"></script>

    <script src="{{ asset('frontend/plugins/jquery.slimscroll.min.js') }}"></script>

    <script src="{{ asset('frontend/plugins/select2/dist/js/select2.full.min.js') }}"></script>

    <!-- custom scripts-->

    <script src="{{ asset('frontend/js/main.js') }}"></script>
    <script type="text/javascript">
        $(document).ready(function(){            
            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });            
        });
        $('.datepicker').datepicker({
            dateFormat: "yy-mm-dd",
            maxDate: new Date(),
        });
        $('.copy_share_url').on('click', function(e){
            e.preventDefault();
            var $temp = $("<input>");
            $("body").append($temp);
            $temp.val($(this).data('url')).select();
            document.execCommand("copy");
            $temp.remove();
        });
    </script>
    @yield('js')
</body>

</html>
