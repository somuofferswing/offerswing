<?php $__env->startSection('content'); ?>
<style>
    /* card */
    .ps-post{
        padding:0px;
    }

    .ps-post .ps-post__title{
        margin:15px 0px;
    }

    .ps-post .ps-post__content p{
        color: inherit;
    } 

    @media (max-width: 767px){
        .ps-post--small-thumbnail .ps-post__thumbnail {
            margin:0px;
        }
        .ps-post--small-thumbnail .ps-post__content {
            padding-left: 1.5rem;
        }
    }
    
    @media (min-width: 768px){
        .ps-post{
            border-radius: 25px;
        }
        .ps-post .ps-post__thumbnail img{
            border-top-right-radius: 25px;
            border-bottom-right-radius: 25px;
        }
    }
</style>
<div class="ps-breadcrumb">
    <div class="container">
        <ul class="breadcrumb text-capitalize" id="breadcrumb">
            <li><a href="<?php echo e(url('/')); ?>">Home</a></li>
            <li>Favourite Shops</li>
        </ul>
    </div>
</div>
<section>
    <div class="ps-page--blog">
        <div class="container">
            <div class="ps-page__header">
                <h1>Favourite Shops</h1>
                <hr class="highlighter">
            </div>
            <div class="ps-blog--sidebar">
                <div class="ps-blog__left pr-0">
                <?php $__currentLoopData = $shops; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $key=>$item): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                    <?php
                    $image = $item->shop_images;
                    $firstimage = $image[0];
                    ?>

                    <div class="ps-post ps-post--small-thumbnail ps-highlight">
               
                        <div class="ps-post__content">
                            <div class="ps-post__top">
                                <a class="ps-post__title" href="<?php echo url('/viewshop',[$item->id]);; ?>"><?php echo e(ucfirst($item->shop_name)); ?>

                                    <?php if(Auth::guest()): ?>
                                        <span class="badge zoom ml-2" onClick="return false;" data-id="<?php echo e($item->shopid); ?>" data-toggle="modal" data-target="#myModal1">
                                            <span data-toggle="tooltip" data-placement="right" title="" data-original-title="Like">
                                                <i class="fa fa-heart-o fa-lg text-danger"></i>
                                            </span>
                                        </span>
                                    <?php else: ?>
                                        <?php if(in_array($item->id,$favouriteshops_ids)): ?>
                                            <span class="badge zoom ml-2 unlike" data-id="<?php echo e($item->id); ?>" data-toggle="tooltip" data-placement="right" title="" data-original-title="Unlike">
                                                <i class="fa fa-heart fa-lg text-danger"></i>
                                            </span>
                                        <?php else: ?>
                                            <span class="badge zoom ml-2 like" data-id="<?php echo e($item->id); ?>" data-toggle="tooltip" data-placement="right" title="" data-original-title="Like">
                                                <i class="fa fa-heart-o fa-lg text-danger"></i>
                                            </span>
                                        <?php endif; ?>
                                    <?php endif; ?>
                                    
                                </a>
            
                                <div class="ps-post__desc">
                                    <p><i class="fa fa-map-marker fa-lg text-warning pr-2"></i><?php echo e($item->address); ?></p>
                                    <p><i class="fa fa-phone text-success pr-2"></i><?php echo e($item->phone_number); ?></p>
                                    <p>
                                        <?php for($i = 0; $i < 5; $i++): ?> 
                                            <?php if($i< (int)$item->shop_rating): ?>
                                            <span class="fa fa-star text-danger"></span>
                                            <?php else: ?>
                                                <?php if(($item->shop_rating-(int)$item->shop_rating) == 0.5 &&
                                                ((int)$item->shop_rating) ==
                                                $i ): ?>
                                                    <span class="fa fa-star-half-o text-danger"></span>
                                                <?php else: ?>
                                                    <span class="fa fa-star-o text-danger"></span>
                                                <?php endif; ?>
                                            <?php endif; ?>
                                        <?php endfor; ?>
                                    </p>
                                    <?php if(in_array(date('N',time())+1 , explode(",",$item->active_days))): ?>
                                        <p>
                                            <i class="fa fa-chevron-right pr-2"></i>
                                            <strong class="text-success">Open Now</strong>
                                        </p>
                                    <?php else: ?>
                                        <p>
                                            <i class="fa fa-chevron-right pr-2"></i>
                                            <strong class="text-danger">Closed</strong>
                                        </p>
                                    <?php endif; ?>
                                </div>
                            </div>
                        </div>

                        <div class="ps-post__thumbnail">
                            <a class="ps-post__overlay" href="<?php echo url('/viewshop',[$item->id]);; ?>"></a>
                            <img src="<?php echo asset("$firstimage->image_path"); ?>" alt="">
                        </div>
                    </div>

                <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                </div>
            </div>
        </div>
    </div>
</section>

<script>
     $(document).on('click', ".unlike", function (e) {
        e.preventDefault();
        var id = $(this).data('id');
        var element = this;

        var data = {
                shop_id:id
            };
        $.ajax({
                type: "post",
                url: '<?php echo e(url("removefavouriteshop")); ?>',
                headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                 },
                data: data,
                cache: false,
                success: function (data)
                {
                    if(data.status == "success"){
                        $(element).removeClass('unlike').addClass('like').attr('data-original-title', 'Like').html('<i class="fa fa-heart-o text-danger"></i>');
                    }
                },
                error: function (data){
                    swal('Fail to run unlike..');
                }
            });
    });

    $(document).on('click', ".like", function (e) {
        e.preventDefault();
        var id = $(this).data('id');
        var element = this;

        var data = {
                shop_id:id
            };
        $.ajax({
            type: "post",
            url: '<?php echo e(url("addfavouriteshop")); ?>',
            headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
            data: data,
            cache: false,
            success: function (data)
            {
                if(data.status == "success"){
                    $(element).removeClass('like').addClass('unlike').attr('data-original-title', 'Unlike').html('<i class="fa fa-heart text-danger"></i>');
                }
            },
            error: function (data){
                swal('Fail to run like..');
            }
        });
    });
</script>

<?php $__env->stopSection(); ?>
<?php echo $__env->make('layouts.app', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH C:\wamp64\www\offerswing_test\resources\views/frontend/favouriteshops.blade.php ENDPATH**/ ?>