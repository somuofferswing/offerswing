    <style>
    .showdropLeft{
        min-width:180px;
    }

    @media (max-width: 1199px){
        .showdropLeft{
            transform: translate3d(-90px, 0px, 0px);
        }
    }

    .categoryImg{
        margin-right: 10px;
        width: 20px;
    }
    .categoryTitle{
        position:relative;
        top:5px;
    }
    </style>
    <header class="header header--standard header--autopart shadow" data-sticky="true">

      <div class="header__content">
          <div class="container">
            
            <div class="header__content-left">
              <a class="ps-logo" href="{{ url('/') }}">
                <img src="{{ asset('main/images/newlogo.png') }}" alt="">
              </a>
              <div class="menu--product-categories">
                <div class="menu__toggle"><i class="icon-menu"></i><span>Shop by Category</span></div>
                <div class="menu__content">
                  <ul class="menu--dropdown menuHeight2" style="overflow-y: scroll">
                  @if ($categories->isNotEmpty())
                    @foreach ($categories as $key=>$item)
                        <li>
                            <a href="{!! url('/subcategories',[$item->slug]); !!}">
                                <img class="categoryImg" src="{{ asset('storage/'.$item->cat_image) }}" alt="">
                                <span class="categoryTitle">{{ ucwords(strtolower($item->category_name)) }}</span>
                            </a>
                        </li>
                    @endforeach
                    @else
                        <li><a href="#">No Categories Found</a></li>
                    @endif  
                  </ul>
                </div>
              </div>
            </div>
            
            <div class="header__content-center">
              <form class="ps-form--quick-search" action="{{ url('/search') }}" method="get">
                <div class="form-group--icon locationInput"><i class="icon-chevron-down"></i>
                    <select class="form-control">
                        <option>Location</option>
                        <option value="Srikakaulam">Srikakaulam</option>
                        <option value="Vishakapatnam">Vishakapatnam</option>
                    </select>
                </div>
                <input class="form-control" name="query" type="text" placeholder="Search for local offers, Deals & Near by Shops.....">
                <button>Search</button>
              </form>
            </div>
            
            <div class="header__content-right">
            
              <div class="header__actions">
    
                @guest
                <div class="ps-block--user-header">
                    <div class="ps-block__left"><i class="icon-user"></i></div>
                    <div class="ps-block__right">
                        <a href="#" data-toggle="modal" data-target="#myModal1" id="login_modal">Login</a>
                        <a href="#" data-toggle="modal" data-target="#myModal1" id="register_modal">Register</a>
                    </div>
                </div>
                @else
                <a class="header__extra" href="{{ url('wingpoints') }}" data-toggle="tooltip" data-original-title="Wing Coins">
                    <i class="icon-coin-dollar"></i>
                </a>
                <a class="header__extra" href="{{ url('orders') }}" data-toggle="tooltip"  data-original-title="My Orders">
                    <i class="icon-cart" style="-webkit-text-stroke: .5px #000;"></i>
                </a>
                <ul class="menu">
                    <li class="current-menu-item menu-item-has-children">
                        <a href="#" onClick="return false;">{{ Auth::user()->first_name." ".Auth::user()->last_name }}</a>
                        <span class="sub-toggle"></span>
                        <ul class="sub-menu showdropLeft">
                            <li><a href="{{ url('userprofile') }}">Profile</a></li>
                            <li><a href="{{ url('favouriteshops') }}">Favourite Shops</a></li>
                            <li><a href="{{ url('/referafriend') }}">Refer a Friend</a></li>
                            <li><a href="#">Notifications<sup>{{ count($notifications) }}</sup></a></li>
                            <li>
                                <a href="{{ route('logout') }}" onclick="event.preventDefault(); document.getElementById('logout-form').submit();">Logout</a>
                            </li>
                        </ul>
                    </li>
                </ul>
                @endguest
               
              </div>
            </div>
          </div>
        </div>
        
        <nav class="navigation">
          <div class="container">
              <div class="navigation__left">
                  <div class="menu--product-categories">
                      <div class="menu__toggle"><i class="icon-menu"></i><span class="text-white"> Shop by Category</span></div>
                      <div class="menu__content">
                        <ul class="menu--dropdown menuHeight" style="overflow-y:scroll">
                            
                          @if ($categories->isNotEmpty())
                            @foreach ($categories as $key=>$item)
                                <li>
                                    <a href="{!! url('/subcategories',[$item->slug]); !!}">
                                        <img class="categoryImg" src="{{ asset('storage/'.$item->cat_image) }}" alt="">
                                        <span class="categoryTitle">{{ ucwords(strtolower($item->category_name)) }}</span>
                                    </a>
                                </li>
                            @endforeach
                          @else
                              <li><a href="#">No Categories Found</a></li>
                          @endif  
                        </ul>
                    </div>
                  </div>
              </div>
              <div class="navigation__right">
                  <span class="text-white">
                        Hi, <strong>Good <strong id="greetings">Morning</strong></strong>
                    </span>
                    <ul class="navigation__extra justify-content-end p-3">
                        <li><a href="{{ url('/addyourbusiness') }}">Add Your Business</a></li>
                        <li><a href="{{ url('/contactus') }}">Contact Us</a></li>
                        <li><a href="{{ url('/aboutus') }}">About Us</a></li>
                        <li><a href="{{ url('/faq') }}">FAQ's</a></li>
                    </ul>
              </div>
          </div>
        </nav>
      </header>
  
      <header class="header header--mobile autopart shadow" data-sticky="true">
          <div class="navigation--mobile">
              <div class="navigation__left">
                  <a class="ps-logo" href="index.html">
                      <img src="{{ asset('main/images/newlogo.png') }}" width="120px" alt="">
                  </a>
              </div>
              <div class="navigation__right">
                  <div class="header__actions pt-3">
                      @guest
                          <div class="ps-block--user-header">
                              <div class="ps-block__left">
                                  <a href="#" data-toggle="modal" data-target="#myModal1">
                                      <i class="icon-user"></i>
                                  </a>
                              </div>
                              <div class="ps-block__right">
                                  <a href="#" data-toggle="modal" data-target="#myModal1" id="login_modal">Login</a>
                                  <a href="#" data-toggle="modal" data-target="#myModal1" id="register_modal">Register</a>
                              </div>
                          </div>
                      @else
                          <ul class="menu">
                              <li class="current-menu-item menu-item-has-children">
                                  <i class="icon-user"></i><i class="icon-chevron-down" style="font-size:12px"></i>
                                  <span class="sub-toggle"></span>
                                  <ul class="sub-menu showdropLeft">
                                      <li><a href="{{ url('userprofile') }}">Profile</a></li>
                                      <li><a href="{{ url('favouriteshops') }}">Favourite Shops</a></li>
                                      <li><a href="{{ url('/referafriend') }}">Refer a Friend</a></li>
                                      <li><a href="{{ url('notifications') }}">Notifications<sup>{{ count($notifications) }}</sup></a></li>
                                      <li>
                                          <a href="{{ route('logout') }}" onclick="event.preventDefault(); document.getElementById('logout-form').submit();">Logout</a>
                                      </li>
                                  </ul>
                              </li>
                          </ul>
                      @endguest
                  </div>
              </div>
          </div>
      </header>
  
      <div class="ps-panel--sidebar" id="navigation-mobile">
          <div class="ps-panel__header">
              <h3>Categories</h3>
          </div>
          <div class="ps-panel__content">
              <ul class="menu--mobile">
              @if ($categories->isNotEmpty())
                  @foreach ($categories as $item)
                  <li><a href="{!! url('/subcategories',[$item->id]); !!}">{{ ucwords(strtolower($item->category_name)) }}</a></li>
                  @endforeach
              @else
                  <li><a href="#">No Categories Found</a></li>
              @endif   
              </ul>
          </div>
      </div>
      <div class="navigation--list">
          <div class="navigation__content">
              <a class="navigation__item ps-toggle--sidebar" href="#menu-mobile">
                  <i class="icon-menu"></i><span> Menu</span>
              </a>
              <a class="navigation__item ps-toggle--sidebar" href="#navigation-mobile">
                  <i class="icon-list4"></i><span> Categories</span>
              </a>
              <a class="navigation__item ps-toggle--sidebar" href="#search-sidebar">
                  <i class="icon-magnifier"></i><span> Search</span>
              </a>
              @auth
              <a class="navigation__item ps-toggle--sidebar" href="{{ url('wingpoints') }}">
                  <i class="icon-coin-dollar"></i><span> Coins</span>
              </a>
              <a class="navigation__item ps-toggle--sidebar" href="{{ url('orders') }}">
                  <i class="icon-cart" style="-webkit-text-stroke: .5px #000;"></i><span> Orders</span>
              </a>
              @endauth
          </div>
      </div>
      <div class="ps-panel--sidebar" id="search-sidebar">
          <div class="ps-panel__header">
              <form class="ps-form--quick-search" action="{{ url('/search') }}" method="get">
                <div class="form-group--icon locationInput"><i class="icon-chevron-down"></i>
                    <select class="form-control">
                        <option>Location</option>
                        <option value="Srikakaulam">Srikakaulam</option>
                        <option value="Vishakapatnam">Vishakapatnam</option>
                    </select>
                </div>
                <input class="form-control" name="query" type="text" placeholder="Search for local offers, Deals & Near by Shops.....">
                <button><i class="icon-magnifier"></i></button>
              </form>
          </div>
          <div class="navigation__content"></div>
      </div>
      <div class="ps-panel--sidebar" id="menu-mobile">
          <div class="ps-panel__header">
              <h3>Menu</h3>
          </div>
          <div class="ps-panel__content">
              <ul class="menu--mobile">
                  <li class="current-menu-item"><a href="index.html">Add Your Business</a></li>
                  <li class="current-menu-item"><a href="index.html">Contact Us</a></li>
                  <li class="current-menu-item"><a href="index.html">About Us</a></li>
                  <li class="current-menu-item"><a href="index.html">FAQ's</a></li>
              </ul>
          </div>
      </div>

    <!-- log out -->
    <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
         @csrf
    </form>