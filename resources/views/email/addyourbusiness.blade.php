<!DOCTYPE html>
<html lang="en-US">
    <head>
        <meta charset="utf-8">
    </head>
    <body>
        <h2>Request for Business:</h2>

        <div>
            <ul>
                <li>Name: {{$input['name']}}</li>
                <li>Email: {{$input['email']}}</li>
                <li>Mobile: {{$input['mobile']}}</li>
                <li>Business Name: {{$input['business_name']}}</li>
                <li>Address: {{$input['address']}}</li>
                <li>City: {{$input['city']}}</li>
                <li>Category: {{$input['category']}}</li>
            </ul>
        </div>

    </body>
</html>
