<?php $__env->startSection('content'); ?>
<style>
    .subcte .carousel-inner>.item>img {
        height: 400px;
    }

    .card {
        background-color: #fff;
        border: 1px solid transparent;
        border-radius: 6px;
        -moz-box-shadow: 0 0 5px #888;
        -webkit-box-shadow: 0 0 5px#888;
        box-shadow: 0 0 5px #888;
    }

    .mgin1 {
        background: white;
        -moz-box-shadow: 0 0 5px #888;
        -webkit-box-shadow: 0 0 5px#888;
        box-shadow: 0 0 5px #888;
    }





    /* social icons */
    ul.socialIcons {
        padding: 0;
        text-align: center;
    }

    .socialIcons li {
        background: yellow;
        list-style: none;
        display: inline-block;
        /* margin: 15px;
    margin-top: 15%; */
        border-radius: 2em;
        overflow: hidden;
    }

    .socialIcons li a {
        display: block;
        padding: .5em;
        max-width: 2.3em;
        min-width: 2.3em;
        height: 2.3em;
        white-space: nowrap;
        line-height: 1.5em;
        transition: .5s;
        text-decoration: none;
        font-family: arial;
        color: #fff;
    }

    .socialIcons li i {
        margin-right: .5em;
    }

    .socialIcons li:hover a {
        max-width: 200px;
        padding-right: 1em;
    }

    .socialIcons .facebook {
        background: #3b5998;
        box-shadow: 0 0 16px #3b5998;
    }

    .socialIcons .twitter {
        background: #00aced;
        box-shadow: 0 0 16px #00aced;
    }

    .socialIcons .instagram {
        background: #cd486b;
        box-shadow: 0 0 16px #cd486b;
    }

    .socialIcons .pinterest {
        background: #c92228;
        box-shadow: 0 0 16px #c92228;
    }

    .socialIcons .steam {
        background: #666666;
        box-shadow: 0 0 16px #666666;
    }

    .details li {
        list-style-type: none;
    }
</style>


<div class="container">
    <h3 style="font-weight: 800;text-align: Left; "> <u>Shops:</u> </h3>
    <hr style="border-bottom:2px solid #df685d;width:5%;margin:0px auto;">

    <div class="row" style="margin-top:20px;">
        <?php if(!$shops->isNotEmpty()): ?>
        No Shops

        <?php else: ?>

        <?php $__currentLoopData = $shops; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $item): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
        <div class="col-md-6 col-xs-12 ">
            <div class="media mgin1 " style="padding:20px;">
                <div class="media-body">
                    <h3><?php echo e($item->shop_name); ?></h3>
                    <h5><i class="fa fa-map-marker fa-1x" aria-hidden="true" style="color:red;"></i>
                        <?php echo e($item->address); ?>,<?php echo e($item->city); ?>,<?php echo e($item->state); ?></h5>
                    <h5><i class="fa fa-mobile" aria-hidden="true"></i> <?php echo e($item->phone_number); ?></h5>
                    <h5> <b>Rating </b> :
                        <?php for($i = 0; $i < 5; $i++): ?> <?php if($i< (int)$item->shop_rating): ?>
                            <span class="fas fa-star" style="color:#df685d"></span>
                            <?php else: ?>
                            <span class="far fa-star" style="color:#df685d"></span>
                            <?php endif; ?>
                            <?php endfor; ?>
                    </h5>

                </div>
                <div class="media-right">
                    
                    <div class="card-img" style="width:200px !important; margin-top:20px;">
                        <a href="<?php echo url('/viewshop',[$item->shopid]);; ?>">

                            <?php $__currentLoopData = $item->images; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $key => $value): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                            <?php if($key == 1): ?>
                            <?php
                                break;
                            ?>
                            
                            <?php endif; ?>
                            <img src="<?php echo asset("$value->image_path"); ?>" class="img-responsive">
                            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>

                        </a>

                    </div>

                    <a href="<?php echo url('/viewshop',[$item->shopid]);; ?>" class="btn btn-danger btn-lg  buy-now">
                        Enter <span class="glyphicon glyphicon-triangle-right"></span>
                    </a>
                </div>


            </div>
        </div>

        <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>

        <?php endif; ?>
    </div>

    <h3 style="font-weight: 800;text-align: left; "><u>Deals:</u></h3>
    <hr style="border-bottom:2px solid #df685d;width:5%;margin:0px auto;">

    <div class="row" style="margin-top:20px;">
        <?php if(!$deals->isNotEmpty()): ?>
        No Deals
        <?php else: ?>
        <?php $__currentLoopData = $deals; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $key=>$item): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>

        <div class="panel panel-primary col-md-6 col-xs-12 ">
            <!-- <div class="panel-heading">
                    <h3 class="panel-title">Panel title</h3>
                </div> -->
            <div class="panel-body">
                <div class="col-sm-4">
                    <img src="https://cdn.pixabay.com/photo/2016/11/29/09/32/antique-1868726_960_720.jpg"
                        class="img-responsive" style="height:200px;width:100%;padding:10px">
                </div>
                <div class="col-sm-8">
                    <div class="col-sm-9">
                        <h3 style="font-weight:800;"><?php echo e($item->name); ?></h3>
                        <div><?php echo $item->description; ?></div>
                        <p><b>Valid Upto: </b> <?php echo e($item->valid_upto); ?></p>
                        <a type="button" class="btn btn-default dealdetails" data-id="<?php echo e($item->id); ?>">Details</a>
                        <a type="button" class="btn btn-info" href="<?php echo e(url('/viewshop',[$item->shop_id])); ?>">Shop
                            Details</a>
                    </div>
                    <div class="col-sm-3">
                        <p>Flat <?php echo e($item->discount_percentage); ?> % off*</p>
                        

                        <?php if(Auth::guest()): ?>
                        <button class="btn btn-primary" data-toggle="modal" data-target="#myModal1">Claim
                            Now</button>
                        <?php else: ?>
                        
                        <button class="btn btn-primary claim" data-id="<?php echo e($item->id); ?>">Claim
                            Now</button>
                        <?php endif; ?>
                    </div>
                </div>
            </div>
        </div>


        <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>

        <?php endif; ?>

    </div>




</div>
<?php $__env->stopSection(); ?>
<?php $__env->startSection('scripts'); ?>

<script>
    $(document).on('click', ".unlike", function() {
        var id = $(this).data('id');
        var element = this;

        var data = {
            shop_id: id
        };
        $.ajax({
            type: "post",
            url: '<?php echo e(url("removefavouriteshop")); ?>',
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            },
            data: data,
            cache: false,
            success: function(data) {
                if (data.status == "success") {
                    $(element).removeClass('unlike').addClass('like').html('<i class="far fa-heart" style="color:#e31b23"></i>');
                }
            },
            error: function(data) {
                alert('Fail to run unlike..');
            }
        });


    });
    $(document).on('click', ".like", function() {
        var id = $(this).data('id');
        var element = this;

        var data = {
            shop_id: id
        };
        $.ajax({
            type: "post",
            url: '<?php echo e(url("addfavouriteshop")); ?>',
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            },
            data: data,
            cache: false,
            success: function(data) {
                console.log(data);
                if (data.status == "success") {
                    $(element).removeClass('like').addClass('unlike').html('<i class="fas fa-heart" style="color:#e31b23"></i>');

                }
            },
            error: function(data) {
                alert('Fail to run like..');
            }
        });


    });
</script>

<?php $__env->stopSection(); ?>

<?php echo $__env->make('layouts.app', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH C:\wamp64\www\offers\resources\views/search.blade.php ENDPATH**/ ?>