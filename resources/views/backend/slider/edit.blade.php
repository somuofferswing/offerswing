@extends('layouts.admin')





@section('content')


<div class="content-wrapper">


    <section class="content-header">


        <h1>


            Slider


            <small>Edit Slider</small>


        </h1>


        <!-- <ol class="breadcrumb">


              <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>


              <li class="active">Dashboard</li>


            </ol> -->


    </section>





    <section class="content">


        <div class="box-body" style="padding: 10px 50px;">


            <form class="form-horizontal" method="POST" action="{{ url('/slider/update',[$slider->id]) }}"
                enctype="multipart/form-data">


                @csrf


                <div class="form-group">


                    <label for="slider_name" class="col-sm-2  control-label">Name</label>


                    <div class="col-sm-10">


                        <input id="slider_name" type="text" class="form-control" name="slider_name"
                            value="{{ $slider->slider_name }}" required autocomplete="slider_name" autofocus>


                    </div>


                </div>





                <hr>


                <button id="addnew" type="button" class="btn btn-primary pull-right">Add New row</button>


                <div class="table-responsive">





                    <table class="table" style="width:100%">


                        <tr>


                            <th>Name</th>


                            <th>Image</th>


                            <th>url(optional)</th>


                        </tr>


                        @foreach ($slider_data as $key=>$item)





                        <tr>


                            <input type="hidden" name="slider_data[{{$key}}][id]" value="{{$item->id}}">


                            <td><input type="text" name="slider_data[{{$key}}][name]" value="{{$item->name}}"></td>


                            <td>


                                {{-- <input type="file" name="slider_data_image[]" onchange="loadFile(event)"" data-key="{{$key}}">


                                --}}


                                <input type="file" name="slider_data_image[]"
                                    onchange="document.getElementById('image'+{{$key}}).src = window.URL.createObjectURL(this.files[0])">


                                <img src="{{ asset("$item->image")}}" alt="" id="image{{$key}}" height="100px">


                            </td>


                            <td><input type="text" name="slider_data[{{$key}}][url]" value="{{$item->url}}"></td>


                        </tr>


                        @endforeach


                    </table>


                </div>


                <div class="pull-right">





                    <button class="btn btn-info" type="submit">Update</button>


                    <button class="btn btn-danger" type="button">Cancel</button>


                </div>


            </form>


        </div>





    </section>


</div>


@endsection


@section('scripts')





<script>
    var loadFile = function(event) {


    var reader = new FileReader();


    reader.onload = function(){


      var output = document.getElementById('output');


      output.src = reader.result;


    };


    reader.readAsDataURL(event.target.files[0]);


  };





  $('#addnew').on('click',function(){

    var count = $('table tr').length;
    var len = count-1;

            var markup = `<tr></tr><td><input type="text" name="slider_data[`+len+`][name]" value="" required></td>
                        <td>


                            <input type="file" name="slider_data_image[]" onchange="document.getElementById('image`+len+`').src = window.URL.createObjectURL(this.files[0])">


                                <img id="image`+len+`" height="100px" required>


                            </td>


                            <td><input type="text" name="slider_data[`+len+`][url]" value=""></td></tr>`;


            $("table tbody").append(markup);


  });


</script>





@endsection