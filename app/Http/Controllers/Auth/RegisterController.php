<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use App\Models\Setting;
use App\Models\User;
use App\Models\WingPoints;
use Illuminate\Foundation\Auth\RegistersUsers;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;

class RegisterController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Register Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles the registration of new users as well as their
    | validation and creation. By default this controller uses a trait to
    | provide this functionality without requiring any additional code.
    |
    */

    use RegistersUsers;

    /**
     * Where to redirect users after registration.
     *
     * @var string
     */
    protected $redirectTo = '/home';

    /**
     * Create a new controller instance.
     */
    public function __construct()
    {
        $this->middleware('guest');
    }

    public function generateRandomString($length = 10)
    {
        // $characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
        $characters = '0123456789';
        $charactersLength = strlen($characters);
        $randomString = '';
        for ($i = 0; $i < $length; ++$i) {
            $randomString .= $characters[rand(0, $charactersLength - 1)];
        }

        return $randomString;
    }

    public function generateReferralcode($name)
    {
        $refid = strtoupper(substr($name, 0, 4).$this->generateRandomString(4));
        $reff = User::where('refid', $refid)->get()->toArray();
        // var_dump($reff);die;
        if (!empty($reff)) {
            var_dump('Hai');
            die;
            $this->generateReferralcode($name);
        } else {
            return $refid;
        }
    }

    public function rewardpointsforreferral($referral, $user_id)
    {
        $reff = User::where('refid', $referral)->first()->toArray();

        $referral_points = Setting::where('name', 'referral_points')->where('status', '1')->first()->toArray();
        $points = $referral_points['value'];

        $wingpoints = new WingPoints();
        $wingpoints->user_id = $reff['id'];
        $wingpoints->type = 'REFERRAL';
        $wingpoints->points = $points;
        $wingpoints->referral_used_user_id = $user_id;

        $wingpoints->save();

        return 'success';
    }

    /**
     * Get a validator for an incoming registration request.
     *
     * @return \Illuminate\Contracts\Validation\Validator
     */
    protected function validator(array $data)
    {
        return Validator::make($data, [
            'first_name' => ['required', 'string', 'max:255'],
            'last_name' => ['required', 'string', 'max:255'],
            'email' => ['required', 'string', 'email', 'max:255', 'unique:users'],
            'mobile' => ['required', 'numeric', 'digits:10', 'unique:users'],
            'gender' => ['required', 'in:MALE,FEMALE,OTHERS'],
            'dob' => ['required', 'date_format:Y-m-d', 'before:today'],
            'password' => ['required', 'string', 'min:8', 'confirmed'],
            'user_type' => ['required'],
        ]);
    }

    /**
     * Create a new user instance after a valid registration.
     *
     * @return \App\User
     */
    protected function create(array $data)
    {
        if (2 == $data['user_type']) {
            $refid = $this->generateReferralcode($data['first_name']);
        } else {
            $refid = '';
        }

        $user = User::create([
            'first_name' => $data['first_name'],
            'last_name' => $data['last_name'],
            'email' => $data['email'],
            'user_type' => $data['user_type'],
            'mobile' => $data['mobile'],
            'gender' => $data['gender'],
            'dob' => $data['dob'],
            'refid' => $refid,
            'password' => Hash::make($data['password']),
        ]);

        $welcome_points = Setting::where('name', 'welcome_points')->where('status', '1')->first()->toArray();
        $points = $welcome_points['value'];

        $wingpoints = new WingPoints();
        $wingpoints->user_id = $user->id;
        $wingpoints->type = 'WELCOME';
        $wingpoints->points = $points;

        $wingpoints->save();

        if (!empty($data['referal_code'])) {
            $res = $this->rewardpointsforreferral($data['referal_code'], $user->id);
        }

        return $user;
    }
}
