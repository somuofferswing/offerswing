<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use App\Models\Category;

class CategoryController extends Controller
{
    public $successStatus = 200;

    public function mainCategories()
    {
        $categories = Category::where('parent_id', null)->get();

        return response()->json(['status' => 'success', 'data' => $categories], $this->successStatus);
    }

    public function getsubCategories($id)
    {
        $categories = Category::where('parent_id', $id)->get();

        if ($categories->isEmpty()) {
            return redirect()->route('shopsbycategory', [$id]);
        }

        return response()->json(['status' => 'success', 'data' => $categories], $this->successStatus);
    }
}
