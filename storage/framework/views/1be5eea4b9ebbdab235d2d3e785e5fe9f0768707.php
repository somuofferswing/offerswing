<?php $__env->startSection('content'); ?>
    <div class="content-wrapper">
        <section class="content-header">
            <h1>
                Shops
                <small>All Shops</small>
            </h1>
            <!-- <ol class="breadcrumb">
              <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
              <li class="active">Dashboard</li>
            </ol> -->
        </section>
        <section class="content">

            <div class="box box-primary">
                <div class="box-header with-border">
                    <h3 class="box-title">All Shops</h3>
                </div>
                <div class="table-responsive">
                    <table class="table table-striped">
                        <thead>
                        <tr>
                            <th>Shop ID</th>
                            <th>Shop Name</th>
                            <th>Email</th>
                            <th>Mobile</th>
                            <th>Address</th>
                            <th>City</th>
                            <th>State</th>
                            <th colspan="3">Action</th>
                        </tr>
                        </thead>
                        <tbody>
                        <?php $__currentLoopData = $shops; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $shop): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                            <tr>
                                <td><?php echo e($shop->id); ?></td>
                                <td><?php echo e($shop->shop_name); ?></td>
                                <td><?php echo e($shop->email); ?></td>
                                <td><?php echo e($shop->phone_number); ?></td>
                                <td><?php echo e($shop->address); ?></td>
                                <td><?php echo e($shop->city); ?></td>
                                <td><?php echo e($shop->state); ?></td>
                                <td><a href="<?php echo e(route('view-shop', [$shop->id])); ?>" class="btn btn-primary">View</a></td>
                                <td><a href="<?php echo e(route('edit-shop', [$shop->id])); ?>" class="btn btn-default">Edit</a></td>
                                <td><a class="btn btn-danger del" data-sub="false" data-id="<?php echo e($shop->id); ?>">Delete</a></td>

                                

                            </tr>
                        <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                        </tbody>
                    </table>
                </div>
            </div>
    </section>
    </div>
<?php $__env->stopSection(); ?>
<?php $__env->startSection('scripts'); ?>

<script>
$('.del').on('click',function(){
        var id = $(this).data('id');
        if (confirm('Do you want to delete shop?')) {
            // alert('Thanks for confirming');
            $.ajax({
                type: "POST",
                url: "<?php echo e(url('shop/delete/')); ?>"+"/"+id,
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
                success: function (response) {
                    // console.log(response);
                    alert(response);
                    window.location.reload();
                }
            });
        } else {

        }

});
</script>

<?php $__env->stopSection(); ?>

<?php echo $__env->make('layouts.admin', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH C:\wamp64\www\offerswing\resources\views/backend/shop/index.blade.php ENDPATH**/ ?>