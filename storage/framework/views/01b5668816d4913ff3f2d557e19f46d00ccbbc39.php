<?php $__env->startSection('content'); ?>

<style>
    /* card */

    .post__badge {

        position: absolute;

        top: 0;

        right: 0;

        color: #fff;

        font-size: 14px;

        font-weight: 600;

        line-height: 20px;

        padding: 5px 10px;

        border-radius: 4px;

        background-color: var(--main-bg-color);

    }
</style>



<div class="ps-breadcrumb">

    <div class="container">

        <ul class="breadcrumb text-capitalize" id="breadcrumb">

            <li><a href="<?php echo e(url('/')); ?>">Home</a></li>

            <li>Wing Deals</li>

        </ul>

    </div>

</div>



<section>

    <div class="ps-page--blog">

        <div class="container">

            <div class="ps-page__header">

                <h1>Wing Deals</h1>

                <hr class="highlighter">

            </div>

            <div class="row">

                <?php $__currentLoopData = $deals; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $key=>$item): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>

                <div class="col-xl-4 col-lg-4 col-md-6 col-sm-12 col-12 ">

                    <div class="ps-post">

                        <div class="ps-post__thumbnail">

                            <a class="ps-post__overlay" href="<?php echo e(url('eventdetails',[$item->id])); ?>"></a>

                            <?php if(!empty($item->pic)): ?>

                            <img src="<?php echo e(asset('storage/'.$item->pic)); ?>" alt="">

                            <?php endif; ?>

                            <div class="post__badge"><?php echo e((($item->original_price- $item->price )/$item->original_price) * 100); ?>% off*</div>

                        </div>

                        <div class="ps-post__content">
                            <div class="ps-post__top">
                                <span class="ps-post__title" ><?php echo e(strtoupper($item->name)); ?></span>
                                <span> ₹. <del><?php echo e($item->original_price); ?> </del> </span> &nbsp;&nbsp; <span>  <?php echo e($item->price); ?> </span>

                                <?php if($item->payment_type == 'wingpoints'): ?>
                                    <p>Pay <?php echo e($item->points_to_claim); ?> (wingpoints) to claim</p>
                                <?php else: ?>
                                    <p>Pay  ₹.<?php echo e($item->price); ?> to claim</p>
                                <?php endif; ?>

                                <p>Flat <?php echo e((($item->original_price- $item->price )/$item->original_price) * 100); ?> % off*</p>

                                <p>
                                    Valid for:
                                    <?php
                                        $valid_for = explode(',',$item->valid_for);
                                        $valid = config('global.valid_for');
                                    ?>
                                    <?php $__currentLoopData = $valid; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $v): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                        <?php if(in_array($v['name'],$valid_for)): ?>
                                        <img src="<?php echo e(asset($v['image'])); ?>" height="10px" width="10px"><?php echo e($v['name']); ?>


                                        <?php endif; ?>
                                    <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                </p>
                            </div>
                            <div class="ps-post__bottom">
                                

                                <a href="#" class="ps-btn btn-xs bg-warning text-dark dealdetails"
                                    onClick="return false;" data-id="<?php echo e($item->id); ?>">Details</a>

                                    <a href="<?php echo url('/viewshop',[$item->slug]);; ?>"
                                        class="ps-btn btn-xs pull-right">Claim Now</a>

                            </div>

                        </div>

                    </div>

                </div>

                <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>

            </div>

        </div>

    </div>

    </div>

</section>



<!-- Modal -->

<div class="modal fade" id="dealdetails" tabindex="-1" role="dialog" aria-labelledby="modelTitleId" aria-hidden="true">

    <div class="modal-dialog" role="document">

        <div class="modal-content">

            <div class="modal-header">

                <h5 class="modal-title">Modal title</h5>

                <button type="button" class="close" data-dismiss="modal" aria-label="Close">

                    <i class="fa fa-remove"></i>

                </button>

            </div>

            <div class="modal-body">

                Body

            </div>

            <div class="modal-footer">

                <button type="button" class="ps-btn" data-dismiss="modal">Claim Now</button>

            </div>

        </div>

    </div>

</div>



<script>
    $('.dealdetails').click(function () {

          let id =  $(this).data('id');



        $.ajax({

          type: "GET",

          url: "<?php echo e(url('/dealdetails/')); ?>"+"/"+id,

          success: function (response) {

            var json_obj = $.parseJSON(response);

            var mymodal = $('#dealdetails');

            mymodal.find('.modal-title').html(`

             <div>

                <h3>`+ json_obj[0]['name'].toUpperCase()+`</h3>



             </div>

             `);



             mymodal.find('.modal-body').html(`

             <div>

                <h4><u>Description:</u></h4>

                <p>`+json_obj[0]['description'] +`</p>

                <hr>

                <h4><u>Active days:</u></h4>

                <p>`+json_obj[0]['days_applicable'].join()+`</p>



                <h4><u>Valid for:</u></h4>

                <p>`+'one Person only'+`</p>



                <h4><u>Valid Dates:</u></h4>

                <p>`+json_obj[0]['valid_from']+' To '+json_obj[0]['valid_from']+`</p>



                <h4><u>Terms & Conditions:</u></h4>

                <p>`+json_obj[0]['terms_conditions'] +`</p>







             </div>

             `);

            mymodal.modal('show');

          }

      });



      })

</script>



<?php $__env->stopSection(); ?>
<?php echo $__env->make('layouts.app', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH C:\wamp64\www\offers\resources\views/frontend/wingdeals.blade.php ENDPATH**/ ?>