<?php $__env->startSection('content'); ?>


<style>
    .tab-pane a,
    .tab-pane a:hover {
        color: black;
        text-decoration: none;
    }
</style>




<div class="container">
    <nav aria-label="breadcrumb">
        <ol class="breadcrumb">
            <li class="breadcrumb-item"><a href="/">Home</a></li>
            <li class="breadcrumb-item active" aria-current="page">My Orders</li>
        </ol>
    </nav>

    <div>

        <ul class="nav nav-tabs">
            <li class="nav-item">
                <a class="nav-link active" data-toggle="tab" href="#deals">Deals</a>
            </li>
            <li class="nav-item">
                <a class="nav-link" data-toggle="tab" href="#events">Events</a>
            </li>
            <li class="nav-item">
                <a class="nav-link" data-toggle="tab" href="#gifts">Gifts</a>
            </li>
        </ul>

        <div class="tab-content">
            <div class="tab-pane container active py-3" id="deals">
                <div class="row">
                    <?php if($deal_orders->isNotEmpty()): ?>
                    <?php $__currentLoopData = $deal_orders; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $item): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                    <div class="col-md-4">
                        <div class="card">
                            <a href="<?php echo e(url('/orderdeal',["$item->order_id"])); ?>">
                                <?php
                                $deal = $item->deal;
                                ?>
                                <img class="card-img-top img-fluid" src="<?php echo asset("$deal->pic"); ?>"
                                    alt="Card image cap" style="height:200px;width:100%;">
                                <div class="card-body">
                                    <h5 class="card-title"> <?php echo e(ucfirst($deal->name)); ?> </h5>
                                    <p class="card-text"> <?php echo $deal->description; ?> </p>
                                    <div class="row">
                                        <div>
                                            Status:
                                            <?php if($item->used == 0): ?>
                                            Not Claimed
                                            <?php else: ?>
                                            Claimed
                                            <?php endif; ?>
                                        </div>
                                        <div>
                                            Expiration :
                                            <?php if($deal->valid_upto >= date('Y-m-d', time())): ?>
                                            <?php echo e($deal->valid_upto); ?>

                                            <?php else: ?>
                                            Expired
                                            <?php endif; ?>
                                        </div>

                                    </div>
                                </div>
                            </a>
                        </div>
                    </div>
                    <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>

                    <?php else: ?>
                    <h4>No Deal Orders</h4>
                    <?php endif; ?>
                </div>
            </div>
            <div class="tab-pane container fade py-3" id="events">
                <div class="row">
                    <?php if($event_orders->isNotEmpty()): ?>
                    <?php $__currentLoopData = $event_orders; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $item): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                    <div class="col-md-4">
                        <div class="card">
                            <a href="<?php echo e(url('/orderevent',["$item->order_id"])); ?>">
                                <?php
                                $event = $item->event;
                                ?>
                                <img class="card-img-top img-fluid" src="<?php echo asset("$event->event_image"); ?>"
                                    alt="Card image cap" style="height:300px;width:100%;">
                                <div class="card-body">
                                    <h5 class="card-title"> <?php echo e($event->title); ?> </h5>
                                    <div class="row">
                                        <div>
                                            Event Date :
                                            <?php echo e($event->event_date); ?>

                                        </div>
                                        <div>
                                            Status:
                                            <?php if($item->used == 0): ?>
                                            Not Claimed
                                            <?php else: ?>
                                            Claimed
                                            <?php endif; ?>
                                        </div>


                                    </div>
                                </div>
                            </a>
                        </div>
                    </div>
                    <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>

                    <?php else: ?>
                    <h4>No Events Orders</h4>

                    <?php endif; ?>
                </div>
            </div>
            <div class="tab-pane container fade py-3" id="gifts">
                <div class="row">
                    <?php if($gift_orders->isNotEmpty()): ?>
                    <?php $__currentLoopData = $gift_orders; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $item): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                    <div class="col-md-4">
                        <div class="card">
                            <a href="<?php echo e(url('/ordergift',["$item->order_id"])); ?>">
                                <?php
                                $gift = $item->gift;
                                ?>
                                <img class="card-img-top img-fluid" src="<?php echo asset("$gift->image"); ?>"
                                    alt="Card image cap" style="height:300px;width:100%;">
                                <div class="card-body">
                                    <h5 class="card-title"> <?php echo e($gift->gift_name); ?> </h5>
                                    <div class="row">
                                        <div>
                                            Status:
                                            <?php if($item->used == 0): ?>
                                            Not Claimed
                                            <?php else: ?>
                                            Claimed
                                            <?php endif; ?>
                                        </div>
                                    </div>
                                </div>
                            </a>
                        </div>
                    </div>
                    <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>

                    <?php else: ?>
                    <h4> NO Gifts Orders</h4>
                    <?php endif; ?>
                </div>
            </div>
        </div>
    </div>
</div>

</div>

<!-- Bootstrap core JavaScript
    ================================================== -->
<!-- Placed at the end of the document so the pages load faster -->

<?php $__env->stopSection(); ?>
<?php echo $__env->make('layouts.app', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH C:\wamp64\www\offers\resources\views/order.blade.php ENDPATH**/ ?>