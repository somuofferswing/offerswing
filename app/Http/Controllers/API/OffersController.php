<?php

namespace App\Http\Controllers\API;

// use Illuminate\Http\Request;

use App\Http\Controllers\Controller;
use App\Models\Offer;

class OffersController extends Controller
{
    public $successStatus = 200;

    public function index()
    {
        return Offer::all();
    }

    public function getOffersDetails($id)
    {
        $offer = Offer::find($id);
        if (!empty($offer)) {
            return response()->json(['status' => 'success', 'data' => $offer], $this->successStatus);
        }

        return response()->json(['status' => 'success', 'data' => 'No Data found'], $this->successStatus);
    }

    public function getShopOffers($id)
    {
        $offer = Offer::where('shop_id', $id)->get();
        if (!empty($offer)) {
            $offers[]['offers'] = $offer;
            $offers[]['shop'] = $offer->shop;

            return response()->json(['status' => 'success', 'data' => $offers], $this->successStatus);
        }

        return response()->json(['status' => 'success', 'data' => 'No Data found'], $this->successStatus);
    }

    public function getCategoryOffers($id)
    {
        $offer = Offer::where('category_id', $id)->get();
        if (!empty($offer)) {
            return response()->json(['status' => 'success', 'data' => $offer], $this->successStatus);
        }

        return response()->json(['status' => 'success', 'data' => 'No Data found'], $this->successStatus);
    }
}
