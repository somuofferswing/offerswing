    <style>

        .categoryClass{

            text-align:center;

            padding-top:20px;

            color:var(--main-bg-color);

        }

      

    </style>

    <h3 class="categoryClass">Categories</h3>

    <ul class="menu--dropdown">

        @if ($categories->isNotEmpty())

            @foreach ($categories as $key=>$item)

                <li>

                    <a href="{!! url('/subcategories',[$item->slug]); !!}">

                        <img class="categoryImg" src="{{ asset('storage/'.$item->cat_image) }}" alt="">

                        <span class="categoryTitle">{{ ucwords(strtolower($item->category_name)) }}</span>

                    </a>

                </li>

            @endforeach

        @else

            <li><a href="#">No Categories Found</a></li>

        @endif   

    </ul>