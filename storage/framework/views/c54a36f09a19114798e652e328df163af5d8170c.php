<?php $__env->startSection('content'); ?>

<div class="ps-breadcrumb">
    <div class="ps-container">
        <ul class="breadcrumb text-capitalize" id="breadcrumb">
            <li><a href="<?php echo e(url('/')); ?>">Home</a></li>
            <li>My Profile</li>
        </ul>
    </div>
</div>

<section>
    <div class="ps-page--blog">
        <div class="container">
            <div class="ps-page__header p-5">
                <h1 class="text-center">My Profile</h1>
                <hr class="highlighter">
            </div>

            <div class="row">
                <div class="col-12 col-md-6">
                   <img src="http://nouthemes.net/html/martfury/img/home/home-photo/share-work.png" alt="">
                </div>
                <div class="col-12 col-md-6">
                    <form id="updateprofile">
                        <div class="form-group">
                            <label>First Name<sup>*</sup></label>
                            <div class="form-group__content">
                                <input class="form-control" type="text" id="fname" name="first_name" readOnly value="<?php echo e($user->first_name); ?>">
                            </div>
                        </div>

                        <div class="form-group">
                            <label>Last Name<sup>*</sup></label>
                            <div class="form-group__content">
                                <input class="form-control" type="text" name="last_name" readOnly value="<?php echo e($user->last_name); ?>">
                            </div>
                        </div>

                        <div class="form-group">
                            <label>Email<sup>*</sup></label>
                            <div class="form-group__content">
                                <input class="form-control" type="email" name="email" readOnly value="<?php echo e($user->email); ?>">
                            </div>
                        </div>

                        <div class="form-group">
                            <label>Mobile<sup>*</sup></label>
                            <div class="form-group__content">
                                <input class="form-control" type="text" name="mobile" readOnly value="<?php echo e($user->mobile); ?>">
                            </div>
                        </div>

                        <div class="form-group">
                            <label>Date Of Birth<sup>*</sup></label>
                            <div class="form-group__content">
                                <input class="form-control datepicker" type="text" name="dob" readOnly value="<?php echo e($user->dob); ?>">
                            </div>
                        </div>

                        <div class="form-group">
                            <label>Gender<sup>*</sup></label>
                            <div class="form-group__content">
                                <div class="form-check form-check-inline ps-radio">
                                    <input class="form-check-input" type="radio" id="gender_male" name="gender" value="MALE" <?php echo e(($user->gender == "MALE") ? 'checked' : ''); ?> disabled>
                                    <label class="form-check-label" for="gender_male">Male</label>
                                </div>
                        
                                <div class="form-check form-check-inline ps-radio">
                                    <input class="form-check-input" type="radio" name="gender" id="gender_female" value="FEMALE" <?php echo e(($user->gender == "FEMALE") ? 'checked' : ''); ?> disabled>
                                    <label class="form-check-label" for="gender_female">Female</label>
                                </div>
                                <div class="form-check form-check-inline ps-radio">
                                    <input class="form-check-input" type="radio" name="gender" id="gender_others" value="OTHERS" <?php echo e(($user->gender == "OTHERS") ? 'checked' : ''); ?> disabled>
                                    <label class="form-check-label" for="gender_others">Other</label>
                                </div>
                            </div>
                        </div>

                        <div class="form-group">
                            <button class="ps-btn" type="button" id="edit">Edit Profile</button>
                            <button class="ps-btn bg-success d-none" type="submit" id="submitbutton">Update</button>
                            <button class="ps-btn bg-dark d-none" type="button" id="cancelbutton">Cancel</button>
                        </div>

                    </form>
                </div>

            </div>
        </div>
    </div>

</section>
<script>
    function chooseFile() {
        $("#profile_image").click();
    }


    function readURL(input) {
        if (input.files && input.files[0]) {
            var reader = new FileReader();

            reader.onload = function (e) {
                $('.image_profile')
                    .attr('src', e.target.result)
                    .width(150);
            };

            reader.readAsDataURL(input.files[0]);

            var data = new FormData();
            var files = input.files[0]
            data.append('file', files);


            $.ajax({
                type: "POST",
                url: '<?php echo e(url("uploadprofileimage")); ?>',
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
                data: data,
                processData: false,
                contentType: false,
                cache: false,
                success: function (response) {
                    console.log(response);

                },
                error: function (data) {
                    alert('Fail to run like..');
                }
            });

        }
    }

    $('#edit').on('click', function (e) {
        e.preventDefault();
        $('#updateprofile input').prop('readOnly', false);
        $('#updateprofile input[name="mobile"]').prop('readOnly', true);
        $('#submitbutton, #cancelbutton').removeClass('d-none');
        $(this).addClass('d-none');

    });

    $('#cancelbutton').on('click', function (e) {
        e.preventDefault();
        $('#updateprofile input').prop('readOnly', true);
        $('input[name="mobile"]').prop('readOnly', true);
        $('#edit').removeClass('d-none');
        $('#submitbutton, #cancelbutton').addClass('d-none');
    });


    $('#submitbutton').on('click', function (e) {
        e.preventDefault();

        var form = $('#updateprofile')[0];
        var formData = new FormData(form);
        var edit = $('#edit');
        var submitbutton = $('#submitbutton');
        $.ajax({
            type: "POST",
            url: '<?php echo e(url("updateprofile")); ?>',
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            },
            data: formData,
            processData: false,
            contentType: false,
            cache: false,
            success: function (response) {
                // console.log(response);
                if (response.status == 'success') {

                    $('#updateprofile input').prop('disabled', true);
                    $('#edit').show();
                    $('#submitbutton').hide();
                    $('#cancelbutton').hide();
                    alert(response.message);
                }else{
                    alert(response.message);
                }
            },
            error: function (data) {
                alert('Fail to run');
            }
        });
    });

    $('#updatepassword').on('click', function (e) {
        e.preventDefault();

        var form = $('#updateprofile_password')[0];
        var formData = new FormData(form);
        console.log(formData);
        for (var pair of formData.entries())
{
 console.log(pair[0]+ ', '+ pair[1]);
}
        $.ajax({
            type: "POST",
            url: '<?php echo e(url("updatepassword")); ?>',
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            },
            data: formData,
            processData: false,
            contentType: false,
            cache: false,
            success: function (response) {
                console.log(response);
                if (response.status == 'success') {

                    $('#new-password_confirmation').val('');
                    $('#new-password').val('');
                    $('#current_password').val('');

                    // $('#updateprofile input').prop('disabled', true);
                    // $('#edit').show();
                    // $('#submitbutton').hide();
                    // $('#cancelbutton').hide();
                    alert(response.message);
                }else{
                    alert(response.message);
                }
            },
            error: function (data) {
                alert('Fail to run');
            }
        });
    });


</script>


<?php $__env->stopSection(); ?>

<?php echo $__env->make('layouts.app', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH C:\xampp\htdocs\offerswing\resources\views/profile.blade.php ENDPATH**/ ?>