<?php

namespace App\Http\Controllers\Backend;

use App\Http\Controllers\Controller;
use App\Models\Amenity;
use App\Models\Offer;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Storage;

class AmenityController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index()
    {
        $amenities = Amenity::all();

        return view('backend.amenities.index', compact('amenities'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function create()
    {
        return view('backend.amenities.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param Request $request
     * @return Response
     */
    public function store(Request $request)
    {
        $amenity = new Amenity();

        $this->validate($request, [
            'name' => 'required',
        ]);

        $amenity->name = $request['name'];

        $amenity->description = $request['description'];

        if ($request->hasFile('image')) {
            $file = $request->file('image');
            $fileName = $request['name'].'_'.time().'.'.$file->getClientOriginalExtension();
            $path = $file->storeAs('amenity', $fileName, 'public');
            $amenity->image = $path;
        }

        $amenity->save();

        return redirect('/amenities')->with('success', 'amenity Created Successfully');
    }

    /**
     * Display the specified resource.
     *
     * @param int $id
     * @return Response
     */
    public function show($id)
    {
        $amenity = Amenity::find($id);

        return view('backend.amenities.show', compact('amenity'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param int $id
     * @return Response
     */
    public function edit($id)
    {
        $amenity = Amenity::find($id);

        return view('backend.amenities.edit', compact('amenity'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param Request $request
     * @param int $id
     * @return Response
     */
    public function update(Request $request, $id)
    {
        $amenity = Amenity::find($id);

        $this->validate($request, [
            'name' => 'required',
        ]);
        $amenity->name = $request['name'];
        $amenity->description = $request['description'];

        if ($request->hasFile('image')) {
            Storage::disk('public')->delete($amenity->image);

            $file = $request->file('image');
            $fileName = $request['name'].'_'.time().'.'.$file->getClientOriginalExtension();
            $path = $file->storeAs('amenity', $fileName, 'public');
            $amenity->image = $path;
        }

        $amenity->save();

        return redirect('/amenities');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param int $id
     * @return Response
     */
    public function destroy($id)
    {
        $amenity = Amenity::find($id);

        // Storage::disk('public')->delete($amenity->image);

        $amenity->delete();

        return redirect('/amenities')->with('success', 'Amenity has been deleted Successfully');
    }
}
