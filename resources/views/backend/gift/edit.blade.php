@extends('layouts.admin')

@section('content')
    <link rel="stylesheet" href="{{ asset('adminlte/bower_components/bootstrap-datepicker/dist/css/bootstrap-datepicker.min.css') }}">
    <div class="content-wrapper">
        <section class="content-header">
            <h1>
                Gifts
                <small>Edit Gift</small>
            </h1>
            <!-- <ol class="breadcrumb">
              <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
              <li class="active">Dashboard</li>
            </ol> -->
        </section>

        <section class="content">

                <div class="box box-info">
                    <div class="box-header with-border">
                        <h3 class="box-title">Edit Gift</h3>
                    </div>
                    <form class="form-horizontal" method="POST" action="{{ route('update-gift',[$gift->id]) }}" enctype="multipart/form-data">
                        @csrf
                        <div class="box-body">
                            <div class="form-group ">
                                <label for="title" class="col-sm-2 control-label">Gift Name</label>
                                <div class="col-sm-10">
                                    <input id="name" type="text" class="form-control" name="gift_name"
                                        value="{{ $gift['gift_name'] }}" required>
                                    @if ($errors->has('gift_name'))
                                    <div class="danger">{{ $errors->first('gift_name') }}</div>
                                    @endif
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="image" class="col-sm-2 control-label">Image</label>
                                <div class="col-sm-10">
                                    <input type="file" class="form-control-file" id="image" name="image"
                                        onchange="loadFile(event)" required>
                                <img id="output" height="100px" width="100px" src="{{ asset($gift['image']) }}"/>
                                </div>
                            </div>
                            <div class="form-group ">
                                <label for="description" class="col-sm-2 control-label">Description</label>
                                <div class="col-sm-10">
                                    <textarea id="description" type="text" class="form-control" name="description" required
                                        autocomplete="description" autofocus>{{ $gift['description'] }}</textarea>
                                    @if ($errors->has('description'))
                                    <div class="danger">{{ $errors->first('description') }}</div>
                                    @endif
                                </div>
                            </div>
                            <div class="form-group ">
                                <label for="points_to_claim" class="col-sm-2 control-label">Points to Claim Price</label>
                                <div class="col-sm-10">
                                    <input id="points_to_claim" type="text" class="form-control" name="points_to_claim" required
                                        autocomplete="description" value='{{ $gift['points_to_claim'] }}'>
                                    @if ($errors->has('points_to_claim'))
                                    <div class="danger">{{ $errors->first('points_to_claim') }}</div>
                                    @endif
                                </div>
                            </div>

                            <div class="form-group ">
                                <label for="claims_per_user" class="col-sm-2 control-label">Claims per user</label>
                                <div class="col-sm-10">
                                    <input id="claims_per_user" type="text" class="form-control" name="claims_per_user" required
                                        autocomplete="description" value='{{ $gift['claims_per_user'] }}'>
                                    @if ($errors->has('claims_per_user'))
                                    <div class="danger">{{ $errors->first('claims_per_user') }}</div>
                                    @endif
                                </div>
                            </div>

                            <div class="form-group ">
                                <label for="quantity" class="col-sm-2 control-label">Quantity</label>
                                <div class="col-sm-10">
                                    <input id="quantity" class="form-control" name="quantity" required autocomplete="quantity"
                                        autofocus value={{ $gift['quantity'] }}>
                                    @if ($errors->has('quantity'))
                                    <div class="danger">{{ $errors->first('quantity') }}</div>
                                    @endif
                                </div>
                            </div>
                            <div class="form-group ">
                                <label for="start_date" class="col-sm-2 control-label">Start Date</label>
                                <div class="col-sm-10">
                                    <input id="start_date" type="text" class="form-control" name="start_date" required
                                        autocomplete="description" value='{{ $gift['start_date'] }}' autofocus>
                                    @if ($errors->has('start_date'))
                                    <div class="danger">{{ $errors->first('start_date') }}</div>
                                    @endif
                                </div>
                            </div>
                            <div class="form-group ">
                                <label for="end_date" class="col-sm-2 control-label">End Date</label>
                                <div class="col-sm-10">
                                    <input id="end_date" type="text" class="form-control" name="end_date" required
                                        autocomplete="description" value='{{ $gift['end_date'] }}' autofocus>
                                    @if ($errors->has('end_date'))
                                    <div class="danger">{{ $errors->first('end_date') }}</div>
                                    @endif
                                </div>
                            </div>

                                <div class="pull-right">
                                    <button type="submit" class="btn btn-info">{{ __('Update') }}</button>
                                        <button type="button" class="btn btn-danger" onclick="window.history.go(-1); return false;">{{ __('Cancel') }}</button>
                                </div>

                        </div>
                    </form>
                </div>
        </section>
    </div>
@endsection
@section('scripts')
    <script src="{{ asset('adminlte/bower_components/bootstrap-datepicker/dist/js/bootstrap-datepicker.min.js') }}"></script>
    <script src="{{ asset('adminlte/bower_components/select2/dist/js/select2.full.min.js') }}"></script>
    <script src="{{ asset('adminlte/bower_components/ckeditor/ckeditor.js') }}"></script>
    <script>
        $(function () {

    CKEDITOR.replace('description')
  })
    </script>
    <script>
    var loadFile = function(event) {
            var output = document.getElementById('output');
            output.src = URL.createObjectURL(event.target.files[0]);
          };
          $(document).ready(function() {
            $('#start_date').datepicker({
                autoclose: true,
                format: 'yyyy-mm-dd',
                startDate: new Date(),
            });

            $('#end_date').datepicker({
                autoclose: true,
                format: 'yyyy-mm-dd',
                startDate: new Date(),
            }).change(function () {
                var from = $('#start_date').val();
                var to = $('#end_date').val();
                if(to < from){
                    $('#end_date').val("");
                    alert('to date should be greater than from date');
                }
            });
        })
</script>
@endsection
