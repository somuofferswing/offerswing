<?php

return [

    'pagination_records' => 10,

    'valid_for' => [
        [
            'name' => 'Males',
            'image' => 'main/images/icons/man.png'
        ],
        [
            'name' => 'Female',
            'image' => ''
        ],
        [
            'name' => 'Childeren',
            'image' => ''
        ],
    ],

    'payment_types' => [
        [
            'name' => 'GPAY',
            'image' => 'main/images/icons/man.png'
        ],
        [
            'name' => 'Phonepe',
            'image' => ''
        ],
        [
            'name' => 'Creditcard',
            'image' => ''
        ],
        [
            'name' => 'Debitcard',
            'image' => ''
        ],
        [
            'name' => 'Cash',
            'image' => ''
        ],
    ],

    'days' => [
            'Sunday',
            'Monday',
            'Tuesday',
            'Wednesday',
            'Thursday',
            'Friday',
            'Saturday'
    ],
];
