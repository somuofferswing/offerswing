<?php $__env->startSection('content'); ?>
<div class="container">
    <div class="row">
        <div class="col-md-12 subg">
            <div class="">
                <img src="<?php echo e(asset('main/images/33.jpeg')); ?>" alt="Los Angeles"
                    style="width:100%;height:300px;max-height:300px;" class="img-fluid">
            </div>
        </div>
    </div>
    <?php
    $category_id = request()->route('id');
    $category = \App\Category::find($category_id);
    $parent_category = $category->parentCategories;
    ?>

    <nav aria-label="breadcrumb">
        <ol class="breadcrumb" id="breadcrumb">
            <li class="breadcrumb-item"><a href="/">Home</a></li>
        </ol>
    </nav>


    <div class="row nav py-3">

        <?php if($sub_categories->isNotEmpty()): ?>
        <?php $__currentLoopData = $sub_categories; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $item): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
        <div class="col-md-4 nav-item text-center">
        
            
            <a href="<?php echo url('/subcategories',[$item->id]);; ?>">
                <div class="subimage py-3">
                    <img src="<?php echo asset("$item->cat_image"); ?>" class="shadow img-fluid rounded-circle border "
                        style="height:75px;width:75px;" />
                </div>
                <h4 class="mx-auto"><?php echo e(ucfirst(strtolower($item->category_name))); ?></h4>
            </a>
        </div>
        <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>

        <?php else: ?>
        No Subcategories Found

        <?php endif; ?>

    </div>
</div>
<?php $__env->stopSection(); ?>

<?php $__env->startSection('scripts'); ?>

<script>
    $(document).ready(function(){
    var category = JSON.parse('<?php echo json_encode($category); ?>');
    var parent_category = JSON.parse('<?php echo json_encode($parent_category); ?>');
    if(parent_category != null){
       var data =  toarray([parent_category]);
       var da = {
                'id': category.id,
                'category_name':category.category_name,
                'parent_id':category.parent_id
            };

        data.unshift(da);
        data.reverse();
        breadcrumb(data);
    }else{
        var data = [];
        var da = {
                'id': category.id,
                'category_name':category.category_name,
                'parent_id':category.parent_id
            };
        data.unshift(da);
        breadcrumb(data);

    }



    function toarray(data,cat){
        var categories = (cat== null) ? []: cat;
        data.forEach(element => {
            id = element.id;
            category_name = element.category_name;
            parent_id = element.parent_id;

            var data = {
                'id': id,
                'category_name':category_name,
                'parent_id':parent_id
            };

            categories.push(data);
            var cat = categories;
            if(element.parent_categories) { toarray([element.parent_categories],cat); }

        });
        return categories;
    }

    function breadcrumb(data){
        data.forEach((element,key,array)=>{
            var id = element.id;
            var category_name = element.category_name.toLowerCase();
            var parent_id = element.parent_id;
            if (key === array.length - 1){

                var li = `<li class="breadcrumb-item active" style="text-transform: capitalize;">`+category_name+`</a></li>`;
                $('#breadcrumb').append(li);
            }else{

                var li = `<li class="breadcrumb-item"><a href="<?php echo e(url('subcategories')); ?>/`+id+`" style="text-transform: capitalize;">`+category_name+`</a></li>`;
                $('#breadcrumb').append(li);
            }
        });
    }
    });

</script>


<?php $__env->stopSection(); ?>

<?php echo $__env->make('layouts.app', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH C:\wamp64\www\offers\resources\views/subcategories.blade.php ENDPATH**/ ?>