    <?php 
        $icons =["car", "wallet", "bicycle2", "woman", "library2", "city", "laptop", "group-work", "home4", "trumpet", "menu3", "graduation-hat", "theater"];
    ?>
        <ul class="menu--dropdown">
            <?php if($categories->isNotEmpty()): ?>
                    <?php $cnt = 1; ?>
                <?php $__currentLoopData = $categories; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $key=>$item): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>

                <?php if($cnt >= 14): ?>
                    <?php if($cnt == 14): ?>
                        <li class="menu-item-has-children has-mega-menu"><a href="#"><i class="icon-ellipsis"></i> More</a>
                            <div class="mega-menu">
                                <div class="mega-menu__column">
                                    <h4>More Categories<span class="sub-toggle"></span></h4>
                                        <ul class="mega-menu__list">
                                            <li>
                                                <a href="<?php echo url('/subcategories',[$item->id]);; ?>">
                                                    <?php echo e(ucwords(strtolower($item->category_name))); ?>

                                                </a>
                    <?php else: ?>
                        <li>
                            <a href="<?php echo url('/subcategories',[$item->id]);; ?>">
                                <?php echo e(ucwords(strtolower($item->category_name))); ?>

                            </a>
                    <?php endif; ?>

                    <?php if($cnt == count($categories)): ?>
                        </ul></div></div></li>
                    <?php endif; ?>

                <?php else: ?>
                    <?php $iconValue = 'icon-'.$icons[$key]; ?>
                    <li>
                        <a href="<?php echo url('/subcategories',[$item->id]);; ?>">
                            <i class="<?php echo e($iconValue); ?>"></i> 
                            <?php echo e(ucwords(strtolower($item->category_name))); ?>

                        </a>
                    </li>
                <?php endif; ?>

                <?php $cnt++; ?>
                <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
            <?php else: ?>
                <li><a href="#">No Categories Found</a></li>
            <?php endif; ?>   
        </ul><?php /**PATH C:\xampp\htdocs\offerswing\resources\views/layouts/mainSidebar.blade.php ENDPATH**/ ?>