

<?php $__env->startSection('content'); ?>
<style>
    .mgin1 {
            background: white;
            -moz-box-shadow: 0 0 5px #888;
            -webkit-box-shadow: 0 0 5px#888;
            box-shadow: 0 0 5px #888;
        }

        .llogin{
        padding:20px;
    }
        </style>
<div class="container">
    <div class="row justify-content-center mgin1">
        <div class="col-md-6 col-md-offset-3">
            <div class="card">
                <div class="card-header text-center llogin"><h4><?php echo e(__('Reset Password')); ?></h4><hr></div>

                <div class="card-body">
                    <?php if(session('status')): ?>
                        <div class="alert alert-success" role="alert">
                            <?php echo e(session('status')); ?>
                        </div>
                    <?php endif; ?>

                    <form method="POST" action="<?php echo e(route('password.email')); ?>" class="form-horizontal">
                        <?php echo csrf_field(); ?>


                        <div class="form-group">
                            <label class="control-label col-sm-3" for="email"><?php echo e(__('E-Mail Address')); ?></label>
                            <div class="col-sm-9">
                                <input id="email" type="email" class="form-control <?php if ($errors->has('email')) :
if (isset($message)) { $messageCache = $message; }
$message = $errors->first('email'); ?> is-invalid <?php unset($message);
if (isset($messageCache)) { $message = $messageCache; }
endif; ?>" name="email" value="<?php echo e(old('email')); ?>" required autocomplete="email" autofocus>
                                <?php if ($errors->has('email')) :
if (isset($message)) { $messageCache = $message; }
$message = $errors->first('email'); ?>
                                <span class="invalid-feedback" role="alert">
                                    <strong style="color:red;"><?php echo e($message); ?></strong>
                                </span>
                            <?php unset($message);
if (isset($messageCache)) { $message = $messageCache; }
endif; ?>
                            </div>
                          </div>


                        


                        <div class="form-group">
                            <div class="col-sm-offset-3 col-sm-9">
                                <button type="submit" class="btn btn-primary">
                                    <?php echo e(__('Send Password Reset Link')); ?>
                                </button>
                            </div>
                          </div>



                        
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
<?php $__env->stopSection(); ?>

<?php echo $__env->make('layouts.app', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH /home/offerkrx/public_html/offerswing_test/resources/views/auth/passwords/email.blade.php ENDPATH**/ ?>