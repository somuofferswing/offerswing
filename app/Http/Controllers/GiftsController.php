<?php

namespace App\Http\Controllers;

use App\Models\Claimed;
use App\Models\Gift;
use App\Models\Notification;
use App\Models\Order;
use App\Models\OrderDetail;
use App\Models\WingPoints;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Mail;

class GiftsController extends Controller
{
    public function getGift($slug)
    {
        $user = auth()->user();
        $user_id = $user['id'];
        $user_email = $user['email'];

        $gift = Gift::where(['slug'=>$slug])->first();
        $gift_id = $gift->id;

        $settings = session()->get('settings');

        $order = OrderDetail::where('user_id', $user_id)->where('order_type', 'gift')->where('order_type_id', $gift_id)->where('claim_status', 0)->first();

        $previousorders = OrderDetail::where('user_id', $user_id)->where('order_type', 'GIFT')->where('order_type_id', $gift_id)->get();
        $previousorderscount = count($previousorders);

        
        if (empty($order) && ($previousorderscount <= $gift->max_count_for_user)) {
            $balance = $user->balance;
            if ($balance >= $gift->points_to_claim) {
                $order = new Order();
                $order_id = $this->generateRandomString(15);
                $order->order_id = $order_id;
                $order->user_id = $user_id;
                $order->payment_type = 'wingpoints';
                $order->payment_status = 'PENDING';
                $order->amount = $gift->points_to_claim;

                $order_details = new OrderDetail();
                $order_details->order_type = 'GIFT';
                $order_details->user_id = $user_id;
                $order_details->order_type_id = $gift_id;
                $order_details->quantity = 1;
                $order_details->price = $gift->points_to_claim;
                $order_details->payment_type = 'wingpoints';
                $order_details->final_price = $gift->points_to_claim;
                $order_details->valid_upto = '';

                $order->save();
                $order->orderDetails()->save($order_details);

                $user->withdraw($gift->points_to_claim, 'withdraw', ['type' => 'Gift', 'description' => 'Spent on Gift','id'=>$gift_id], 'Gift');

                $order->payment_status = 'SUCCESS';
                $order->save();

                $gift->quantity -= 1;
                $gift->save();

                $notifications = new  Notification();

                $notifications->type = 'GIFT';
                $notifications->type_id = $gift_id;
                $notifications->user_id = $user_id;
                $notifications->notification_message = "your Gift '".$gift->name."' has been claimed Successfully!";
        
                $notifications->save();
        
                $from_email = config('settings.contact_email');
                $user_email = $user['email'];
        
                Mail::send('email.order_gift_details', ['order' => $order, 'order_details' => $order_details, 'gift' => $gift], function ($message) use ($user_email, $from_email) {
                    $message->to($user_email)->from($from_email)->subject('Order Details');
                });

                return redirect()->route('giftorder', [$order_id]);
                // $this->giftOrder($order_id);
                  
                   
                // return redirect()->route('dealorder', [$order_id]);
            }
            return back()->with('alert', 'Not Enough WingCoins to claim')->with('class', 'warning');
        }
    }

    public function giftOrder($order_id)
    {
        $user = auth()->user();
        $order = Order::where(['order_id' => $order_id])->first();
        $order_details = $order->orderDetails;
        $gift_id = $order_details->order_type_id;
        $gift = Gift::find($gift_id);
        $user_id = $order_details->user_id;

       

        return view('frontend.giftorders', compact('order', 'order_details', 'gift'));
    }

    public function generateRandomString($length = 10)
    {
        $characters = '0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZ';
        $charactersLength = strlen($characters);
        $randomString = '';
        for ($i = 0; $i < $length; ++$i) {
            $randomString .= $characters[rand(0, $charactersLength - 1)];
        }

        return $randomString;
    }
}
