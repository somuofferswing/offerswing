@extends('layouts.admin')

@section('content')
    <link rel="stylesheet"
          href="{{ asset('adminlte/bower_components/bootstrap-datepicker/dist/css/bootstrap-datepicker.min.css') }}">
    <div class="content-wrapper">
        <section class="content-header">
            <h1>
                Offers
                <small>Add Offer</small>
            </h1>
            <!-- <ol class="breadcrumb">
                  <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
                  <li class="active">Dashboard</li>
                </ol> -->
        </section>

        <section class="content">

            <div class="box box-info">
                <div class="box-header with-border">
                    <h3 class="box-title">Add Offer</h3>
                </div>
                <form class="form-horizontal" method="POST" action="{{ route('store-offer') }}"
                      enctype="multipart/form-data">
                    @csrf
                    {{-- <input type="hidden" value="{{$user_id}}" name="created_by"> --}}
                    <div class="box-body">
                        <div class="form-group ">
                            <label for="name" class="col-sm-2 control-label">Name</label>
                            <div class="col-sm-10">
                                <input id="name" type="text" class="form-control" name="name" value="{{ old('name') }}"
                                       required autocomplete="name" autofocus>
                                @if ($errors->has('name'))
                                    <div class="danger">{{ $errors->first('name') }}</div>
                                @endif
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="offer_image" class="col-sm-2 control-label">Image</label>
                            <div class="col-sm-10">
                                <input type="file" class="form-control-file" id="offer_image" name="offer_image"
                                       onchange="loadFile(event)">
                                <img id="output" height="100px" width="100px"/>
                            </div>
                        </div>
                        <div class="form-group ">
                            <label for="description" class="col-sm-2 control-label">Description</label>
                            <div class="col-sm-10">
                            <textarea id="description" type="text" class="form-control" name="description" required
                                      autocomplete="description" autofocus>{{ old('description') }}</textarea>
                                @if ($errors->has('description'))
                                    <div class="danger">{{ $errors->first('description') }}</div>
                                @endif
                            </div>
                        </div>
                        <div class="form-group ">
                            <label for="shop_id" class="col-sm-2 control-label">Shop</label>
                            <div class="col-sm-10">

                                <select class="form-control" id="shop_id" name="shop_id" value="{{ old('shop_id') }}"
                                        required autocomplete="shop_id" autofocus>
                                    <option>--Select--</option>
                                    @foreach($shops as $shop)
                                        <option value="{{ $shop->id }}">{{ $shop->name }}</option>
                                    @endforeach
                                </select>
                                @if ($errors->has('shop_id'))
                                    <div class="danger">{{ $errors->first('shop_id') }}</div>
                                @endif
                            </div>
                        </div>
{{--                        <div class="form-group ">--}}
{{--                            <label for="times_applicable" class="col-sm-2 control-label">Days Applicable</label>--}}
{{--                            <div class="col-sm-10">--}}
{{--                                <select name="days_applicable[]" id="days_applicable" class="form-control select2"--}}
{{--                                        data-placeholder="Select days applicable" multiple="multiple">--}}
{{--                                    @foreach ($days as $item)--}}
{{--                                        <option value="{{$item->id}}">{{$item->name}}</option>--}}
{{--                                    @endforeach--}}
{{--                                </select>--}}
{{--                                @if ($errors->has('days_applicable'))--}}
{{--                                    <div class="danger">{{ $errors->first('days_applicable') }}</div>--}}
{{--                                @endif--}}
{{--                            </div>--}}
{{--                        </div>--}}
                        <div class="form-group ">
                            <label for="from_date" class="col-sm-2 control-label">From Date</label>
                            <div class="col-sm-10">
                                <input type="text" class="form-control pull-right" id="from_date" name="from_date"
                                       value="{{ old('from_date') }}" required autocomplete="from_date" autofocus>

                                @if ($errors->has('from_date'))
                                    <div class="danger">{{ $errors->first('from_date') }}</div>
                                @endif
                            </div>
                        </div>
                        <div class="form-group ">
                            <label for="to_date" class="col-sm-2 control-label">To Date</label>
                            <div class="col-sm-10">
                                <input type="text" class="form-control pull-right" id="to_date" name="to_date"
                                       value="{{ old('to_date') }}" required autocomplete="to_date" autofocus>
                                @if ($errors->has('to_date'))
                                    <div class="danger">{{ $errors->first('to_date') }}</div>
                                @endif
                            </div>
                        </div>

                        <div class="form-group ">
                            <label for="max_wing_points_assured" class="col-sm-2 control-label">Top Offer</label>
                            <div class="col-sm-10">
                                <input type="checkbox" class="" name="top" value="1">
                                @if ($errors->has('top'))
                                    <div class="danger">{{ $errors->first('top') }}</div>
                                @endif
                            </div>
                        </div>


                        {{-- <div class="form-group ">
                            <label for="terms_conditions" class="col-sm-2 control-label">Terms & Conditions</label>
                            <div class="col-sm-10">
                                <textarea id="terms_conditions" type="text" class="form-control" name="terms_conditions" rows="10"
                                    cols="80">{{ old('terms_conditions') }}</textarea>
                                @if ($errors->has('terms_conditions'))
                                <div class="danger">{{ $errors->first('terms_conditions') }}</div>
                                @endif
                            </div>
                        </div> --}}
                        <div class="pull-right">
                            <button type="submit" class="btn btn-info">{{ __('Add Offer') }}</button>
                            <a type="button" class="btn btn-danger"
                               onclick="window.history.go(-1); return false;">{{ __('Cancel') }}</a>
                        </div>

                    </div>
                </form>
            </div>
        </section>
    </div>
@endsection
@section('scripts')
    <script src="{{ asset('adminlte/bower_components/bootstrap-datepicker/dist/js/bootstrap-datepicker.min.js') }}">
    </script>
    <script src="{{ asset('adminlte/bower_components/select2/dist/js/select2.full.min.js') }}"></script>
    <script src="{{ asset('adminlte/bower_components/ckeditor/ckeditor.js') }}"></script>
    <script>
        $(function () {
            CKEDITOR.replace('description')
        });
        $('.select2').select2();
        $(document).ready(function () {
            $('#from_date').datepicker({
                autoclose: true,
                format: 'yyyy-mm-dd',
                startDate: new Date(),
            });

            $('#to_date').datepicker({
                autoclose: true,
                format: 'yyyy-mm-dd',
                startDate: new Date(),
            }).change(function () {
                var from = $('#from_date').val();
                var to = $('#to_date').val();
                if (to < from) {
                    $('#to_date').val("");
                    alert('to date should be grater than from date');
                }
            });
        })

    </script>
    <script>
        var loadFile = function (event) {
            var output = document.getElementById('output');
            output.src = URL.createObjectURL(event.target.files[0]);
        };
    </script>
@endsection
