<!DOCTYPE html>
<html lang="en">

<head>
    <title>Bootstrap Example</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.0/css/bootstrap.min.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.0/js/bootstrap.min.js"></script>
</head>

<body>

    <div class="container">
        <div class="row">
            <div class="col-md-8 col-md-offset-2" style="padding:5px !important;">
                <div calss="row">

                    <img src="http://www.offerswing.com/main/images/newlogo.png" style="height:100px"
                        class="img-responsive" />



                </div>
                <div class="row">
                    <div class="col-md-6 col-xs-12">
                        <h4>Deals receipt</h4>
                        <h5>Order no: <?php echo e($order->order_id); ?></h5>
                        <h5>Fri,07 Jun 2019, 21:39:02</h5>
                        
                    </div>
                    <div class="col-md-6 col-xs-12 text-right">
                        <h4>Address</h4>
                        <h5>Srikakulam</h5>

                        <h5>Andhra Pradesh</h5>

                        <div class="details" style="margin-top:20px;">
                            <h5>1234567890</h5>
                            <h5><a href="#">offerswing.com</a></h5>
                        </div>
                    </div>
                </div>
                <div class="row" style="padding:20px;">


                    <h4 class="text-center">Your Deals:</h4>
                    <?php
                    $deal = $order->deal;
                    ?>
                    <hr>
                    <h5>Order ID :<span style="font-weight:bold;"><?php echo e($order->order_id); ?> </span></h5>
                    <?php if($deal->payment_type == ''): ?>
                    <h5>Deal Price (WingPoints) :<span style="font-weight:bold;"> <?php echo e($deal->price); ?> </span></h5>
                    <?php else: ?>
                    <h5>Deal Price :<span style="font-weight:bold;"> Rs.<?php echo e($deal->price); ?></span></h5>
                    <?php endif; ?>
                    <h5>Deal Expired :<span style="font-weight:bold;"> 17/10/2019</span></h5>
                    <h5>Deal Description :<span style="font-weight:bold;">

                            <?php echo $deal->description; ?>

                        </span></h5>
                    <hr>


                    

                    

                    <h4>Terms & Conditions:</h4>

                    

                    <p>
                        <?php echo $deal->terms_conditions; ?>

                    </p>



                </div>

            </div>
        </div>
    </div>

</body>

</html>
<?php /**PATH C:\wamp64\www\offers\resources\views/email/order_details.blade.php ENDPATH**/ ?>