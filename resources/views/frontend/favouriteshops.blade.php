@extends('layouts.app')

@section('content')
<style>
    /* card */
    .ps-post{
        padding:0px;
    }

    .ps-post .ps-post__title{
        margin:15px 0px;
    }

    .ps-post .ps-post__content p{
        color: inherit;
    } 

    @media (max-width: 767px){
        .ps-post--small-thumbnail .ps-post__thumbnail {
            margin:0px;
        }
        .ps-post--small-thumbnail .ps-post__content {
            padding-left: 1.5rem;
        }
    }
    
    @media (min-width: 768px){
        .ps-post{
            border-radius: 25px;
        }
        .ps-post .ps-post__thumbnail img{
            border-top-right-radius: 25px;
            border-bottom-right-radius: 25px;
        }
    }
</style>
<div class="ps-breadcrumb">
    <div class="container">
        <ul class="breadcrumb text-capitalize" id="breadcrumb">
            <li><a href="{{ url('/') }}">Home</a></li>
            <li>Favourite Shops</li>
        </ul>
    </div>
</div>
<section>
    <div class="ps-page--blog">
        <div class="container">
            <div class="ps-page__header">
                <h1>Favourite Shops</h1>
                <hr class="highlighter">
            </div>
            <div class="ps-blog--sidebar">
                <div class="ps-blog__left pr-0">
                @foreach ($shops as $key=>$item)
                    @php
                    $image = $item->shop_images;
                    $firstimage = $image[0];
                    @endphp

                    <div class="ps-post ps-post--small-thumbnail ps-highlight">
               
                        <div class="ps-post__content">
                            <div class="ps-post__top">
                                <a class="ps-post__title" href="{!! url('/viewshop',[$item->id]); !!}">{{ ucfirst($item->shop_name) }}

                                    @if(Auth::guest())
                                        <span class="badge zoom ml-2" onClick="return false;" data-id="{{ $item->shopid }}" data-toggle="modal" data-target="#myModal1">
                                            <span data-toggle="tooltip" data-placement="right" title="" data-original-title="Like">
                                                <i class="fa fa-heart-o fa-lg text-danger"></i>
                                            </span>
                                        </span>
                                    @else
                                        @if (in_array($item->id,$favouriteshops_ids))
                                            <span class="badge zoom ml-2 unlike" data-id="{{ $item->id }}" data-toggle="tooltip" data-placement="right" title="" data-original-title="Unlike">
                                                <i class="fa fa-heart fa-lg text-danger"></i>
                                            </span>
                                        @else
                                            <span class="badge zoom ml-2 like" data-id="{{ $item->id }}" data-toggle="tooltip" data-placement="right" title="" data-original-title="Like">
                                                <i class="fa fa-heart-o fa-lg text-danger"></i>
                                            </span>
                                        @endif
                                    @endif
                                    
                                </a>
            
                                <div class="ps-post__desc">
                                    <p><i class="fa fa-map-marker fa-lg text-warning pr-2"></i>{{ $item->address }}</p>
                                    <p><i class="fa fa-phone text-success pr-2"></i>{{ $item->phone_number }}</p>
                                    <p>
                                        @for ($i = 0; $i < 5; $i++) 
                                            @if ($i< (int)$item->shop_rating)
                                            <span class="fa fa-star text-danger"></span>
                                            @else
                                                @if (($item->shop_rating-(int)$item->shop_rating) == 0.5 &&
                                                ((int)$item->shop_rating) ==
                                                $i )
                                                    <span class="fa fa-star-half-o text-danger"></span>
                                                @else
                                                    <span class="fa fa-star-o text-danger"></span>
                                                @endif
                                            @endif
                                        @endfor
                                    </p>
                                    @if (in_array(date('N',time())+1 , explode(",",$item->active_days)))
                                        <p>
                                            <i class="fa fa-chevron-right pr-2"></i>
                                            <strong class="text-success">Open Now</strong>
                                        </p>
                                    @else
                                        <p>
                                            <i class="fa fa-chevron-right pr-2"></i>
                                            <strong class="text-danger">Closed</strong>
                                        </p>
                                    @endif
                                </div>
                            </div>
                        </div>

                        <div class="ps-post__thumbnail">
                            <a class="ps-post__overlay" href="{!! url('/viewshop',[$item->id]); !!}"></a>
                            <img src="<?php echo asset("$firstimage->image_path"); ?>" alt="">
                        </div>
                    </div>

                @endforeach
                </div>
            </div>
        </div>
    </div>
</section>

<script>
     $(document).on('click', ".unlike", function (e) {
        e.preventDefault();
        var id = $(this).data('id');
        var element = this;

        var data = {
                shop_id:id
            };
        $.ajax({
                type: "post",
                url: '{{ url("removefavouriteshop") }}',
                headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                 },
                data: data,
                cache: false,
                success: function (data)
                {
                    if(data.status == "success"){
                        $(element).removeClass('unlike').addClass('like').attr('data-original-title', 'Like').html('<i class="fa fa-heart-o text-danger"></i>');
                    }
                },
                error: function (data){
                    swal('Fail to run unlike..');
                }
            });
    });

    $(document).on('click', ".like", function (e) {
        e.preventDefault();
        var id = $(this).data('id');
        var element = this;

        var data = {
                shop_id:id
            };
        $.ajax({
            type: "post",
            url: '{{ url("addfavouriteshop") }}',
            headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
            data: data,
            cache: false,
            success: function (data)
            {
                if(data.status == "success"){
                    $(element).removeClass('like').addClass('unlike').attr('data-original-title', 'Unlike').html('<i class="fa fa-heart text-danger"></i>');
                }
            },
            error: function (data){
                swal('Fail to run like..');
            }
        });
    });
</script>

@endsection