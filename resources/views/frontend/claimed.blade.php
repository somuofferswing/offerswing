@extends('layouts.app')

@section('content')

<div class="container">


    <div class="row col-md-8 col-md-offset-2">

        <div class="panel panel-primary ">
            <div class="panel-heading">
                <h3 class="panel-title"></h3>
            </div>
            <div class="panel-body">

                @if($order->type == 'deal')
                @php
                $pro = $order->deal
                @endphp
                @else
                @php
                $pro = $order->offer
                @endphp

                @endif

                {{-- <a href="#"> --}}
                @if($order->type == 'deal')
                <img src="<?php echo asset("$pro->pic"); ?>" style="height:300px;width:100%;" class="img-responsive" />
                @else
                <img src="<?php echo asset("$pro->event_image"); ?>" style="height:300px;width:100%;" class="img-responsive" />
                @endif
                {{-- </a> --}}

                </td>
                <div class=" mgin1" style="padding:20px">
                    <div class="col-md-4">
                        <p>Date & Time : 02-10-2019 </p>
                    </div>
                    <div class="col-md-4 text-center">
                            <p>
                                    Status:
                                    @if ($claim->used == 0)
                                    Not Used
                                    @else
                                    Claimed
                                    @endif
                                </p>
                    </div>
                    <div class="col-md-4">
                            <p class="pull-right">
                                    Expiration :
                                    @if ($claim->valid_upto >= date('Y-m-d', time()))
                                    {{ $claim->valid_upto }}
                                    @else
                                    Expired
                                    @endif
                                </p>
                        {{-- <p class="pull-right">Your Deal Expired Date</p> --}}
                    </div>

                </div>


                <div class="qrcode text-center">
                    <img src="https://cdn.pixabay.com/photo/2013/07/12/14/45/qr-code-148732_960_720.png"
                        class="img-responsive" style="width:150px; height:150px;margin:0 auto;margin-top:20px;" />
                    <p calss="">Show the QR Code of the store</p>
                </div>






                <div class="table-responsive">
                    <table class="table table-bordered">


                        <tbody>
                            <tr>
                                <td>Booking Id</td>
                                <td>{{ $order->order_id }}</td>

                            </tr>
                            <tr>
                                <td>About Deal</td>
                                <td>20% Off on something</td>

                            </tr>
                            <tr>
                                <td>Brought On</td>
                                <td>{{ $claim->created_at }}</td>

                            </tr>
                            <tr>
                                <td>Expires On</td>
                                <td>{{ $claim->valid_upto }}</td>

                            </tr>
                            <tr>
                                <td>Store Address</td>
                                <td>Srikakaulam,Srikakaulam,Andhra Pradesh - 500302</td>

                            </tr>
                            <tr>
                                <td>Contact Number</td>
                                <td>+91 7780121551</td>

                            </tr>
                            <tr>
                                <td class="text-center" colspan="2"><b>Payment Details<b></td>


                            </tr>
                            <tr>
                                <td>Price of the store</td>
                                <td>Rs.1000</td>

                            </tr>
                            <tr>
                                <td>Offerswing Price</td>
                                <td>Rs.500</td>

                            </tr>
                            <tr>
                                <td>Amount pay for claiming the wing deal</td>
                                <td>Rs.500</td>

                            </tr>
                            <tr>
                                <td class="" colspan="2"><b>*Terms and condtions apply<b></td>


                            </tr>
                            <tr>
                                <td class="" colspan="2"><b>Amount you saved Rs.500<b></td>


                            </tr>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>


</div>
@endsection

@section('scripts')


@endsection
