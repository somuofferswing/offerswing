@extends('layouts.admin')



@section('content')

<div class="content-wrapper">

    <section class="content-header">

        <h1>

            Category

            <small>All Categories</small>

        </h1>

        <!-- <ol class="breadcrumb">

              <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>

              <li class="active">Dashboard</li>

            </ol> -->

    </section>

    <section class="content">

        <div class="box box-primary">

            <div class="box-header with-border">

                <h3 class="box-title">All Categories</h3>

                <a href="{!! url('/category/create'); !!}" class="btn btn-primary pull-right">Add Category</a>

            </div>

            <div class="table-responsive">

                <table class="table table-striped">

                    <thead>

                        <tr>

                            <th>S.No</th>

                            <th>Name</th>

                            <th>Image</th>

                            <th>Parent Category</th>

                            <th colspan="2">Action</th>

                        </tr>

                    </thead>

                    <tbody>

                        @foreach($allcategories as $key=>$category)

                        <tr>

                            <td>{{$key+1}}</td>

                            <td>{{$category->category_name}}</td>

                        <td><img class="img-thumbnail" src='{{ asset("storage/".$category->cat_image) }}'
                                    height="75px" width="75px"></td>

                            <td>{{$category->parent_category_name}}</td>



                            {{-- <td><a class="btn btn-primary" href="{{ route('view-category', [$category->id]) }}">View</a>
                            </td> --}}

                            <td><a class="btn btn-default" href="{{ route('edit-category', [$category->id]) }}">Edit</a>
                            </td>



                            <td><a class="btn btn-danger del" data-sub="false" data-id="{{$category->id}}">Delete</a>
                            </td>





                        </tr>

                        @endforeach

                    </tbody>

                </table>

                <div class="pull-right" style="padding:10px">

                    {{ $allcategories->links() }}

                </div>

            </div>

        </div>



    </section>

</div>

@endsection

@section('scripts')

<script>
    $('.del').on('click',function(){

    var bool = $(this).data('sub');

    var id = $(this).data('id');



    if(!bool){

        $.ajax({

            type: "POST",

            url: "{{  url('category/delete/') }}"+"/"+id,

            headers: {

                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')

            },

            success: function (response) {

                alert(response);

                window.location.reload();

            }

        });

    }else{

        if (confirm('Do you want to delete all sub categories too?')) {

            // alert('Thanks for confirming');

            $.ajax({

                type: "POST",

                url: "{{  url('category/delete/') }}"+"/"+id,

                headers: {

                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')

                },

                success: function (response) {

                    alert(response);

                    window.location.reload();

                }

            });

            // delete(id);

        } else {



        }

    }



});



// function delete(id){

//     $.ajax({

//                 type: "POST",

//                 url: "{{  url('category/delete/') }}"+id,

//                 headers: {

//                     'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')

//                 },

//                 success: function (response) {

//                     alert(response);

//                     window.location.reload();

//                 }

//             });



// }

</script>



@endsection