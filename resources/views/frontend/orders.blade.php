@extends('layouts.app')

@section('content')
<style>
    .ps-section--shopping {
        padding: 30px 0;
    }

    .ps-table--shopping-cart tbody tr td {
        text-align: center;
    }

    .ps-table--shopping-cart tbody tr td:first-child {
        text-align: left;
    }
</style>

<section>
    
    <div class="ps-page--simple">

        <div class="ps-section--shopping ps-shopping-cart">

            <div class="container">

                <div class="ps-section__header pb-5">

                    <h1>Your Order</h1>

                </div>

                <div class="ps-section__content">

                    <div class="table-responsive">

                        <table class="table ps-table--shopping-cart">

                            <thead>

                                <tr>

                                    <th>ITEM NAME</th>

                                    <th>TYPE</th>

                                    <th>PRICE</th>

                                    <th>QUANTITY</th>

                                    <th>TOTAL</th>

                                    <th></th>

                                </tr>

                            </thead>

                            <tbody>

                                @if ($order_details['order_type']== 'DEAL')

                                <tr>

                                    <td>{{$deal['name']}}</td>

                                    <td>{{$order_details['type']}}</td>

                                    <td>₹ {{$deal['price']}}</td>

                                    <td>{{$order_details['quantity']}}</td>



                                    <td>₹ {{ $deal['price'] }} * {{ $order_details['quantity'] }}</td>

                                    <td><a class="delete" href="#"><i class="icon-cross"></i></a></td>

                                </tr>



                                <div class="col-md-12">

                                    <div class="pull-right">

                                        <table class="table" id="price">

                                            <tr>

                                                <td>Price :</td>

                                                <td>₹ {{ $deal['price'] }} * {{ $order_details['quantity'] }}</td>

                                                <td>₹ <span
                                                        class="pull-right">{{ $deal['price'] * $order_details['quantity'] }}</span>
                                                </td>

                                            </tr>

                                            <tr>

                                                <td>Total Price :</td>

                                                <td></td>

                                                <td>₹ <span
                                                        class="pull-right">{{ $order_details['final_price'] }}</span>
                                                </td>

                                            </tr>

                                        </table>

                                    </div>

                                </div>

                                @else

                                <tr>

                                    <td>{{$deal['title']}}</td>

                                    <td>{{$order_details['order_type']}}</td>

                                    <td>₹ {{$deal['event_price']}}</td>

                                    <td>

                                        <div class="form-group--number">

                                            <button class="up quantity" id="plus">+</button>

                                            <button class="down quantity" id="minus">-</button>

                                            <input class="form-control" type="text" placeholder="1"
                                                value="{{ $order_details['quantity'] }}">

                                        </div>

                                    </td>

                                    <td>₹ {{ $deal['event_price'] * $order_details['quantity'] }}</td>

                                    <td><a class="delete" href="#"><i class="icon-cross"></i></a></td>

                                </tr>

                                @endif

                            </tbody>

                        </table>

                    </div>

                    <div class="ps-section__cart-actions">

                        <a class="ps-btn bg-dark" href="{{ url('/eventdetails',[$deal['id']]) }}">

                            <i class="icon-arrow-left"></i> Back to Event

                        </a>

                        <a class="ps-btn" href="{{ url('/proceedtopay',[$order_details['order_id']])}}">

                            <i class="fa fa-send pr-2"></i>Proceed to Pay

                        </a>

                    </div>

                </div>



            </div>

        </div>

    </div>

</section>

{{-- <script>
    $('#wing_points').on('click',function(){



        var max_points = $('#max_points').val();

        var id = "{{ $order['id'] }}";

var wing_points;

if(total_wing_points > max_points){

wing_points = max_points;

}else{

wing_points = total_wing_points;

}



if(!$(this).prop("checked")){

wing_points = 0;

}

var data= {

id:id,

wing_points:wing_points

}



$.ajax({

type: "post",

url: '{{ url("updateorder") }}',

headers: {

'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')

},

data: data,

// dataType: "dataType",

success: function (response) {

if(response.status == 'success'){

location.reload();

}

}

});

});



$('.quantity').on('click',function(){

var quant = $(this).prop('id');

var id = "{{ $order['id'] }}";



if(quant == 'plus'){

quantity = 1

}else{

quantity= -1

}



var data= {

id:id,

quantity:quantity

}



$.ajax({

type: "post",

url: '{{ url("updateorder") }}',

headers: {

'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')

},

data: data,

// dataType: "dataType",

success: function (response) {

if(response.status == 'success'){

location.reload();

}else{

var url = response.url;

window.location.href = "{{ url('/') }}"+url;

}

}

});

});



$('.delete').on('click',function(e){

e.preventDefault();

var id = "{{ $order['id'] }}";

var data= {

id:id

}



$.ajax({

type: "post",

url: '{{ url("deleteorder") }}',

headers: {

'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')

},

data: data,

success: function (response) {

if(response.status == 'success'){

window.location.href = "{{ url('/') }}";

}

}

});

});



</script> --}}

@endsection