<?php $__env->startSection('content'); ?>
<div class="ps-breadcrumb">
    <div class="ps-container">
        <ul class="breadcrumb text-capitalize" id="breadcrumb">
            <li><a href="<?php echo e(url('/')); ?>">Home</a></li>
            <li>Redeem Gifts</li>
        </ul>
    </div>
</div>
<section>
    <div class="ps-page--blog">
        <div class="container">
            <div class="ps-page__header">
                <h1>Redeem Gifts</h1>
                <hr class="highlighter">
            </div>
            <div class="row mb-40">
                <?php $__currentLoopData = $gifts; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $item): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                    <div class="col-6 col-sm-4">
                        
                        <div class="ps-product">
                            <div class="ps-product__thumbnail">
                                <?php if(!empty($item->image)): ?>
                                    <img src="<?php echo asset("$item->image"); ?>" alt="">
                                <?php endif; ?>
                            </div>
                            <div class="ps-product__container">
                                <div class="ps-product__content">
                                    <div class="ps-product__vendor text-capitalize">
                                        <span>Points Required: <?php echo e($item->points_to_claim); ?></span>
                                        <?php if(Auth::guest()): ?>
                                            <a href="#" class="text-danger pull-right" onClick="return false" data-toggle="modal"  data-target="#myModal1">
                                                Claim Now
                                            </a>
                                        <?php else: ?>
                                                <?php if(Session::get('total_wingpoints')->points >= $item->points_to_claim ): ?>
                                                    <a href="<?php echo e(url('/getgift',[ $item->id])); ?>" class="text-danger pull-right">
                                                        Claim Now
                                                    </a>
                                                <?php else: ?>
                                                    <span class="text-danger pull-right">
                                                        Not Enough Points
                                                    </span> 
                                                <?php endif; ?>
                                        <?php endif; ?>
                                    </div>
                                    <span class="ps-product__title text-danger"><?php echo e(ucfirst($item->gift_name)); ?></span>
                                    <p><?php echo $item->description; ?></p>
                                </div>
                                <div class="ps-product__content hover">
                                    <div class="ps-product__vendor text-capitalize">
                                    <span>Points Required: <?php echo e($item->points_to_claim); ?></span>
                                        <?php if(Auth::guest()): ?>
                                            <a href="#" class="text-danger pull-right" onClick="return false" data-toggle="modal"  data-target="#myModal1">
                                                Claim Now
                                            </a>
                                        <?php else: ?>
                                                <?php if(Session::get('total_wingpoints')->points >= $item->points_to_claim ): ?>
                                                    <a href="<?php echo e(url('/getgift',[ $item->id])); ?>" class="text-danger pull-right">
                                                        Claim Now
                                                    </a>
                                                <?php else: ?>
                                                    <span class="text-danger pull-right">
                                                        Not Enough Points
                                                    </span> 
                                                <?php endif; ?>
                                        <?php endif; ?>
                                    </div>
                                    <span class="ps-product__title text-danger"><?php echo e(ucfirst($item->gift_name)); ?></span>
                                    <p><?php echo $item->description; ?></p>
                                </div>
                            </div>
                        </div>
                    </div>
                <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
            </div>
        </div>
    </div>
</div>
</section>



    <!-- <a class="btn btn-primary" >Trigger modal</a> -->
    <div class="modal fade" id="share">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-body">
                    <div class="a2a_kit a2a_kit_size_32 a2a_default_style">
                        
                        <a class="a2a_button_facebook"></a>
                        <a class="a2a_button_twitter"></a>
                        <a class="a2a_button_email"></a>
                        <a class="a2a_button_whatsapp"></a>
                    </div>
                    <script async src="https://static.addtoany.com/menu/page.js"></script>
                </div>
            </div>
        </div>
    </div>

    <script>
        $(document).on('click', ".unlike", function () {
            var id = $(this).data('id');
            var type = "event";
            var element = this;

            var data = {
                    id:id,
                    type:type
                };
            $.ajax({
                    type: "post",
                    url: '<?php echo e(url("removelike")); ?>',
                    headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                     },
                    data: data,
                    cache: false,
                    success: function (data)
                    {
                        if(data.status == "success"){
                            $(element).removeClass('unlike').addClass('like').html('<i class="far fa-heart" style="color:#e31b23"></i> &nbsp;like');
                        }
                    },
                    error: function (data){
                        alert('Fail to run unlike..');
                    }
                });


        });
        $(document).on('click', ".like", function () {
            var id = $(this).data('id');
            var type = "event";
            var element = this;

            var data = {
                    id:id,
                    type:type
                };
            $.ajax({
                    type: "post",
                    url: '<?php echo e(url("addlike")); ?>',
                    headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                     },
                    data: data,
                    cache: false,
                    success: function (data)
                    {
                        console.log(data);
                        if(data.status == "success"){
                                $(element).removeClass('like').addClass('unlike').html('<i class="fas fa-heart" style="color:#e31b23"></i> &nbsp;Unlike');

                        }
                    },
                    error: function (data){
                        alert('Fail to run like..');
                    }
                });


        });
    </script>
    <?php $__env->stopSection(); ?>

<?php echo $__env->make('layouts.app', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH C:\xampp\htdocs\offerswing\resources\views/redeem.blade.php ENDPATH**/ ?>