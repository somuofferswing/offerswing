    <footer class="ps-footer ps-footer--3">
        <div class="container">
            <div class="ps-block--site-features ps-block--site-features-2">
                <div class="ps-block__item">
                    <div class="ps-block__left"><i class="icon-calendar-text"></i></div>
                    <div class="ps-block__right">
                        <h4>Create Event / Deal</h4>
                        <p>+91 9704 98 96 97</p>
                    </div>
                </div>
                <div class="ps-block__item">
                        <div class="ps-block__left"><i class="fa fa-whatsapp"></i></div>
                <a href="https://api.whatsapp.com/send?phone=919704989697">
                        <div class="ps-block__right">
                            <h4>Stay Connected</h4>
                            <p>with US</p>
                        </div>
                </a>
                </div>
                <div class="ps-block__item">
                    <div class="ps-block__left"><i class="icon-bubbles"></i></div>
                    <div class="ps-block__right">
                        <h4>Follow Us</h4>
                        <ul class="ps-list--social mt-2">
                            <li>
                                <a class="facebook" href="https://www.facebook.com/Offerswing-378561272765031/" target="_blank" title="Facebook"><i class="fa fa-facebook"></i>
                                </a>
                            </li>
                            <li>
                                <a class="twitter" href="https://twitter.com/offerswing2" target="_blank" title="Twitter"><i class="fa fa-twitter"></i>
                                </a>
                            </li>
                            <li>
                                <a class="instagram" href="https://www.instagram.com/offerswing2019/" target="_blank" title="instagram"><i class="fa fa-instagram"></i>
                                </a>
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
            <div class="ps-footer__widgets p-0">
                <aside class="widget widget_footer widget_contact-us">
                    <h4 class="widget-title">Contact us</h4>
                    <div class="widget_content">
                        <p>Call us 24/7</p>
                        <h3>+91 97049 89697</h3>
                        <p>Srikakulam, Andhra Pradesh 532001 <br><a href="mailto:info@offerswing.com">info@offerswing.com</a></p>
                    </div>
                </aside>
                <aside class="widget widget_footer">
                    <h4 class="widget-title">Quick links</h4>
                    <ul class="ps-list--link">
                        <li><a href="<?php echo e(url('page',['privacy_policy'])); ?>">Privacy Policy</a></li>
                        <li><a href="<?php echo e(url('page',['terms_conditions'])); ?>">Terms & Conditions</a></li>
                        <li><a href="<?php echo e(url('/addyourbusiness')); ?>">Add Your Business</a></li>
                        <li><a href="<?php echo e(url('/faq')); ?>">FAQs</a></li>
                    </ul>
                </aside>
                <aside class="widget widget_footer">
                    <h4 class="widget-title">Company</h4>
                    <ul class="ps-list--link">
                        <li><a href="<?php echo e(url('/aboutus')); ?>">About us</a></li>
                        <li><a href="<?php echo e(url('/contactus')); ?>">Contact us</a></li>
                    </ul>
                </aside>
                <aside class="widget widget_footer">
                    <h4 class="widget-title">Download APP</h4>
                    <ul class="ps-list--link">
                        <li><a href="#"><img src="<?php echo e(asset('main/images/PlayStore.png')); ?>" style="height:40px"></a></li>
                        <li><a href="#"><img src="<?php echo e(asset('main/images/apple-app-store-icon.png')); ?>" style="height:45px"></a></li>
                    </ul>
                </aside>
            </div>
        </div>
    </footer>
    <style>
       
    </style>
    <div id="back2top"><i class="icon-chevron-up"></i></div>
    <div class="ps-site-overlay"></div>
    <div id="loader-wrapper">
        <div class="loader-section section-left"></div>
        <div class="loader-section section-right"></div>
    </div>

    <!-- login modal -->
    <div class="modal" id="myModal1" tabindex="-1" role="dialog">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
               
                <div class="modal-body">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <i class="fa fa-remove"></i>
                </button>
                <div class="ps-form--account ps-tab-root">
                    <ul class="ps-tab-list mb-3">
                        <li class="active"><a href="#sign-in">Login</a></li>
                        <li><a href="#register">Register</a></li>
                    </ul>
                    <div class="ps-tabs">

                        <!-- login tab -->
                        <div class="ps-tab active" id="sign-in">
                            <div class="ps-form__content">
                                <h5>Log In Your Account</h5>
                                <form id="loginform" class="form-horizontal" onsubmit="return LoginUser()">
                                <?php echo csrf_field(); ?>
                                <input type="hidden" id="login_user_type" name="user_type" value="2">
                                <div class="form-group">
                                    <input class="form-control" type="email" id="login_email" name="email" placeholder="Email *" required>
                                </div>
                                <div class="form-group form-forgot">
                                    <input class="form-control" type="password" id="login_password" name="password" placeholder="Password *" required>
                                    <a class="tabChange" href="#forgot-pwd">Forgot?</a>
                                </div>
                                <div class="form-group">
                                    <div class="ps-checkbox">
                                        <input class="form-control" type="checkbox" id="login-remember" name="remember">
                                        <label for="login-remember">Rememeber me</label>
                                    </div>
                                </div>
                                <div class="form-group" id="loginBtnDiv">
                                    <button class="ps-btn ps-btn--fullwidth" type="submit">Login</button>         
                                </div>
                                
                                <div class="d-flex justify-content-center">
                                    <div class="sk-flow d-none" id="loginSpinner">
                                        <div class="sk-flow-dot"></div>
                                        <div class="sk-flow-dot"></div>
                                        <div class="sk-flow-dot"></div>
                                    </div>
                                </div>
                                <div id="loginStatus"></div>
                                <p class="text-danger text-center d-none" id="loginInvalid">
                                    <strong>Invalid Credentials</strong>
                                </p>

                                </form>
                            </div>
                            <div class="ps-form__footer">
                                <p>Login with:</p>
                                <a href="#sign-in-otp" class="ps-btn bg-dark tabChange">OTP</a>
                            </div>
                        </div>

                        <!-- register tab -->
                        <div class="ps-tab" id="register">
                            <div class="ps-form__content">
                                <h5>Register An Account</h5>
                                <form id="signupform" method="POST" onsubmit="return SignupUser()">
                                    <?php echo csrf_field(); ?>
                                    <input type="hidden" id="register_user_type" name="user_type" value="2">
                                    <div class="form-group">
                                        <input class="form-control" type="text" id="register_firstname" name="first_name" placeholder="First Name *" required>
                                        <span id="first_name_error" class="errorClass"></span>
                                    </div>
                                    <div class="form-group">
                                        <input class="form-control" type="text" id="register_lastname" name="last_name" placeholder="Last Name *" required>
                                        <span id="last_name_error" class="errorClass"></span>
                                    </div>
                                    <div class="form-group">
                                        <input class="form-control" type="text" id="register_mobile" name="mobile" placeholder="Mobile *" required>
                                        <span id="mobile_error" class="errorClass"></span>
                                    </div>
                                    <div class="form-group">
                                        <input class="form-control" type="text" id="register_email" name="email" placeholder="Email *" required>
                                        <span id="email_error" class="errorClass"></span>
                                    </div>
                                    <div class="form-group">
                                        <input class="form-control datepicker" type="text" id="register_dob" name="dob" required placeholder="Date Of Birth *">
                                        <span id="dob_error" class="errorClass"></span>
                                    </div>
                                    <div class="form-group">
                                        <div class="form-check form-check-inline ps-radio">
                                            <input class="form-check-input" type="radio" id="gender_male" name="register_gender" value="MALE" required>
                                            <label class="form-check-label" for="gender_male">Male</label>
                                        </div>
                                        <div class="form-check form-check-inline ps-radio">
                                            <input class="form-check-input" type="radio" name="register_gender" id="gender_female" value="FEMALE" required>
                                            <label class="form-check-label" for="gender_female">Female</label>
                                        </div>
                                        <div class="form-check form-check-inline ps-radio">
                                            <input class="form-check-input" type="radio" name="register_gender" id="gender_others" value="OTHERS" required>
                                            <label class="form-check-label" for="gender_others">Other</label>
                                        </div>
                                        <span id="gender_error" class="errorClass"></span>
                                    </div>
                                    <div class="form-group">
                                        <input class="form-control" type="password" id="register_password" name="password" placeholder="Password *" required>
                                        <span id="password_error" class="errorClass"></span>
                                    </div>
                                    <div class="form-group">
                                        <input class="form-control" type="password" id="register_password_confirm" name="password_confirmation" placeholder="Confirm Password *" required>
                                    </div>
                                    <div class="form-group">

                                    <?php
                                        if (app('request')->input('ref')) {
                                            $ref = app('request')->input('ref');
                                        }
                                    ?>
                                        <input class="form-control" type="text" id="register_referal_code" name="referal_code" placeholder="Referal Code (Optional)"

                                        <?php if(!empty($ref)): ?>
                                            value= <?php echo e($ref); ?>

                                        <?php endif; ?>>
                                    </div>
                                    <div class="form-group" id="regBtnDiv">
                                        <button class="ps-btn ps-btn--fullwidth" type="submit">Sign Up</button>
                                    </div>

                                    <div class="d-flex justify-content-center">
                                        <div class="sk-flow d-none" id="regSpinner">
                                            <div class="sk-flow-dot"></div>
                                            <div class="sk-flow-dot"></div>
                                            <div class="sk-flow-dot"></div>
                                        </div>
                                    </div>

                                    <div id="sigupstatus"></div>
                                </form>
                            </div>
                        </div>

                        <!-- sign-in otp tab -->
                        <div class="ps-tab" id="sign-in-otp">
                            <div class="ps-form__content">
                                <h5>Login with OTP</h5>
                                <form id="otpform" onsubmit="return SendOtp()" role="form">
                                    <?php echo csrf_field(); ?>
                                    <div class="form-group">
                                        <input class="form-control" type="number" id="otp_mobile" name="otp_mobile" placeholder="Mobile *" required>
                                    </div>
                                    <div class="form-group text-center" id="sendOtpDiv">
                                        <button class="ps-btn" type="submit">Send OTP</button>
                                    </div>
                                    <div id="otpStatus"></div>
                                    <div class="d-flex justify-content-center">
                                        <div class="sk-flow d-none" id="sendOtpSpinner">
                                            <div class="sk-flow-dot"></div>
                                            <div class="sk-flow-dot"></div>
                                            <div class="sk-flow-dot"></div>
                                        </div>
                                    </div>
                                </form>
                            </div>
                        </div>

                        <!-- verify otp tab -->
                        <div class="ps-tab" id="verify-otp">
                            <div class="ps-form__content">
                                <h5>Verify OTP</h5>
                                <div id="verifyStatus"></div>
                                <form id="verifyotp" onsubmit="return Otpverify()">
                                    <?php echo csrf_field(); ?>
                                    <div class="form-group">
                                        <input class="form-control" type="text" id="verifyMobileNum" name="verifyMobileNum"  disabled>
                                    </div>
                                    <div class="form-group">
                                        <input class="form-control" type="text" id="otp" name="otp" placeholder="Please enter OTP *" required>
                                    </div>
                                    <div class="form-group text-center" id="verifyDiv">
                                        <button class="ps-btn" type="submit">Verify</button>
                                        <button type="button" id="resendotp" class="ps-btn bg-dark text-warning d-none" onClick="resendOTP()">
                                            Resend OTP
                                        </button>
                                    </div>
                                    
                                    <div class="d-flex justify-content-center">
                                        <div class="sk-flow d-none" id="verifySpinner">
                                            <div class="sk-flow-dot"></div>
                                            <div class="sk-flow-dot"></div>
                                            <div class="sk-flow-dot"></div>
                                        </div>
                                    </div>
                                    <h4 id="myTimer" class="text-danger text-right"></h4>
                                </form>
                            </div>
                        </div>

                        <!-- reset password tab -->
                        <div class="ps-tab" id="forgot-pwd">
                            <div class="ps-form__content">
                                <h5>Reset Password</h5>
                                <div id="reset_status" class="alert"> </div>
                                <form id="forgotpwd" onsubmit="return forgotPassword()">
                                    <?php echo csrf_field(); ?>
                                    <div class="form-group">
                                        <input class="form-control" type="email" id="reset_email" name="reset_email" placeholder="Email *" required>
                                    </div>
                                    <div class="form-group text-center" id="resetBtnDiv">
                                        <button class="ps-btn " type="submit">Send Password Reset Link</button>
                                    </div>
                                    <div class="d-flex justify-content-center">
                                        <div class="sk-flow d-none" id="resetSpinner">
                                            <div class="sk-flow-dot"></div>
                                            <div class="sk-flow-dot"></div>
                                            <div class="sk-flow-dot"></div>
                                        </div>
                                    </div>
                                </form>
                            </div>
                        </div>
                        
                    </div>
                </div>
                
                </div>
            </div>
        </div>
    </div>

    <!-- alertmodel -->
    <div class="modal fade" id="modal-alert">
        <div class="modal-dialog modal-dialog-centered">
            <div class="modal-content">
                <div class="modal-body">
                    <p id="alert_message" class="alert alert-<?php echo e(session('class')); ?>">
                        <?php echo e(session('alert')); ?>

                    </p>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                </div>
            </div>
        </div>
    </div>
    <!-- alertmodel -->

    <script>
    $(document).ready(function() {
        
        $(".lightgallery").lightGallery();
         
        var now = new Date();
        var hrs = now.getHours();

        if (hrs >  6) { $('#greetings').text("").text("Morning") };
        if (hrs > 12) { $('#greetings').text("").text("Afternoon") };
        if (hrs > 17) { $('#greetings').text("").text("Evening") };
        var alert1 = <?php echo e(Session::has('alert') ? true : 0); ?>;
        if(alert1){
            let msg = "<?php echo e(session('alert')); ?>";
            let status = "<?php echo e(session('class')); ?>";

            if(status == 'danger') status = 'warning';
            swal("", msg, status);
        }
        
        $('#login_modal').click(function(e){
            $("ul.ps-tab-list li.active, .ps-tab").removeClass("active");
            $('#sign-in').addClass("active");
            $('a[href="#sign-in"]').parent("li").addClass("active");
        });

        $('#register_modal').click(function(e){
            $("ul.ps-tab-list li.active, .ps-tab").removeClass("active");
            $('#register').addClass("active");
            $('a[href="#register"]').parent("li").addClass("active");
        });

        $('a.tabChange').click(function(e){
            e.preventDefault();

            let id = $(this).attr('href');
            $("ul.ps-tab-list li.active, .ps-tab").removeClass("active");
            $(id).addClass("active");
        });
    });
   
    </script>
    <script>
    function LoginUser()
        {

            $('#loginInvalid, #loginBtnDiv').addClass('d-none');
            $('#loginSpinner').removeClass('d-none');
            var email     = $("#login_email").val();
            var user_type = $("#login_user_type").val();
            var password  = $("#login_password").val();
            var data = {
                email:email,
                password:password,
                user_type: user_type
            };

            // Ajax Post
            $.ajax({
                type: "post",
                url: '<?php echo e(url("customlogin")); ?>',
                headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                 },
                data: data,
                cache: false,
                success: function (resp)
                {
                    let message = resp.message;
                    $('#loginSpinner').addClass('d-none');
                    if(resp.status == "success"){
                        let msg = `<h4 class="text-center text-success"><i class="fa fa-check pr-2"></i>${message}</h4>`;
                        $('#loginStatus').fadeIn().html(msg);
                      
                        location.reload();
                    }else{
                        $('#loginInvalid,#loginBtnDiv').fadeIn().removeClass('d-none');
                    }
                },
                error: function (data){
                    $('#loginSpinner').addClass('d-none');
                    var errors = data.responseJSON.errors;
                }
            });
            return false;
        }

        function SignupUser()
        {
            $('.errorClass').html('');
            $('#regBtnDiv').addClass('d-none');
            $('#regSpinner').removeClass('d-none');

            // next code goes here...
            var first_name  = $("#register_firstname").val();
            var last_name   = $("#register_lastname").val();
            var email       = $("#register_email").val();
            var password    = $("#register_password").val();
            var mobile      = $("#register_mobile").val();
            var dob         = $("#register_dob").val();
            var gender      = $("input[name='register_gender']:checked").val();
            var referal_code = $("#register_referal_code").val();
            var user_type    = $("#register_user_type").val();
            var password_confirmation = $("#register_password_confirm").val();
            // garnish the data
            var data = {
                first_name:first_name,
                last_name:last_name,
                email:email,
                password:password,
                referal_code:referal_code,
                user_type:user_type,
                password_confirmation:password_confirmation,
                mobile:mobile,
                dob:dob,
                gender:gender
            };

            // ajax post
            $.ajax({
                type: "post",
                url: '<?php echo e(url("customregister")); ?>',
                headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                 },
                data: data,
                cache: false,
                success: function (resp){

                    $('#regSpinner').addClass('d-none');
                    let message = resp.message;
                
                    if(resp.status == "Success"){
                        let msg = `<h4 class="text-center text-success"><i class="fa fa-check pr-2"></i>${message}</h4>`;
                        $('#sigupstatus').fadeIn().html(msg);
                        
                        location.reload();
                    }else{
                        $('#regBtnDiv').removeClass('d-none');
                        let msg = `<h4 class="text-center text-danger">${message}</h4>`;
                        $('#sigupstatus').fadeIn().html(msg);
                    }
                },
                error: function (data){

                    $('#regSpinner').addClass('d-none');
                    $('#regBtnDiv').removeClass('d-none');

                    var errors = data.responseJSON.errors;
                    if(errors){
                        for(let key in errors){
                            let errorMsg = errors[key][0];
                            let spanID = `#${key}_error`;
                            
                            $(spanID).addClass('text-danger').html(errorMsg);
                        }
                    }
                }
            });
            return false;
        }


            var sec = 59;
            var myTimer = document.getElementById('myTimer');
            var myBtn = document.getElementById('resendotp');

            function countDown() {
            if (sec < 10) {
                myTimer.innerHTML = "00:0" + sec;
            } else {
                myTimer.innerHTML = "00:"+sec;
            }
            if (sec <= 0) {
                $("#resendotp").removeClass("d-none");
                $("#myTimer").fadeOut('slow');
                return;
            }
            sec -= 1;
            window.setTimeout(countDown, 1000);
            }



        function resendOTP()
        {
            $('#verifyStatus').removeClass().html('');
            $('#resendotp').addClass('d-none');
            $('#verifySpinner').removeClass('d-none');
            var mobile    = $("#verifyMobileNum").val();
            var data = {
                mobile:mobile
            };
            
            // Ajax Post
            $.ajax({
                type: "post",
                url: '<?php echo e(url("resendOTP")); ?>',
                headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                 },
                data: data,
                cache: false,
                success: function (data)
                {
                    $('#verifySpinner').addClass('d-none');
                    message = data.message;
                    if(data.status == "Success"){
                        let msg = `<i class="fa fa-check pr-2"></i>${message}`;
                        $('#verifyStatus').fadeIn().addClass('alert alert-success').html(msg);
                        sec = 59;
                        $("#myTimer").fadeIn();
                        countDown();
                    }else{
                        $('#verifyStatus').fadeIn().addClass('alert alert-danger').html(message);
                    }
                },
                error: function (data){
                    var errors = data.responseJSON.errors;
                    $('#verifySpinner').addClass('d-none');
                }
            });
            return false;
        }



        function SendOtp()
        {
            $('#sendOtpDiv').addClass('d-none');
            $('#sendOtpSpinner').removeClass('d-none');
            var mobile    = $("#otp_mobile").val();
            var data = {
                mobile:mobile
            };
            // Ajax Post
            $.ajax({
                type: "post",
                url: '<?php echo e(url("loginSendOtp")); ?>',
                headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                 },
                data: data,
                cache: false,
                success: function (data)
                {
                   
                    $('#sendOtpSpinner').addClass('d-none');
                    message = data.message;
                    if(data.status == "Success"){
                        $('ul.ps-tab-list li.active, .ps-tab').removeClass('active');
                        $('#verifyMobileNum').val(mobile);
                        $('#verify-otp').addClass('active');
                        $('ul.ps-tab-list').addClass('d-none');
                        let msg = `<i class="fa fa-check pr-2"></i>${message}`;
                        $('#verifyStatus').fadeIn().addClass('alert alert-success').html(msg);
                        countDown();
                    }else{
                        $('#sendOtpDiv').removeClass('d-none');
                        
                        let msg = `<h4 class="text-center text-danger">${message}</h4>`;
                        $('#otpStatus').fadeIn().html(msg);
                    }
                },
                error: function (data){
                    var errors = data.responseJSON.errors;

                    $('#sendOtpSpinner').addClass('d-none');
                    $('#sendOtpDiv').removeClass('d-none');
                }
            });
            return false;
        }


        function Otpverify()
        {
            $('#verifySpinner').removeClass('d-none');
            $('#verifyDiv').addClass('d-none');

            // next code goes here...
            var otp  = $("#otp").val();

            var data = {
                code:otp,
            };
            // ajax post
            $.ajax({
                type: "post",
                url: '<?php echo e(url("verifyotp")); ?>',
                headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                 },
                data: data,
                cache: false,
                success: function (data){
                    $('#verifySpinner').addClass('d-none');
                    var message = data.message;
                    if(data.status == "Success"){
                        let msg = `<h4 class="text-center text-success"><i class="fa fa-check pr-2"></i>${message}</h4>`;
                        $('#verifyStatus').fadeIn().html(msg);

                        location.reload();
                    }else{
                        $('#verifyDiv').removeClass('d-none');
                        let msg = `<h4 class="text-center text-danger">${message}</h4>`;
                        $('#verifyStatus').fadeIn().html(msg);
                    }
                },
                error: function (data){
                    console.log(data);
                    console.log(data.responseJSON.errors);

                    // var errors = data.responseJSON.errors;
                }
            });
            return false;
        }

        function forgotPassword()
        {
            $('#reset_status').removeClass().html('');
            $('#resetSpinner').removeClass('d-none');
            $('#resetBtnDiv').addClass('d-none');
            // next code goes here...
            var email = $("#reset_email").val();
            var data  = {
                email:email
            };

            $.ajax({
                type: "post",
                url: '<?php echo e(url("customforgotpassword")); ?>',
                headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                 },
                data: data,
                cache: false,
                success: function (data){

                    $('#resetSpinner').addClass('d-none');
                    $('#resetBtnDiv').removeClass('d-none');

                    var message = data.msg;
                    if(data.status == "Success"){
                        let msg = `<i class="fa fa-check pr-2"></i>${message}`;
                        $('#reset_status').fadeIn().addClass('alert alert-success').html(msg);
                    }else{
                        $('#reset_status').fadeIn().addClass('alert alert-danger').html(message);
                    }
                },
                error: function (data){
                    console.log(data.responseJSON.errors);
                    // var errors = data.responseJSON.errors;
                }
            });
            return false;
        }

        var wH = $(window).height();
        $('.menuHeight').height(wH-150);
        $('.menuHeight2').height(wH-80);
</script>
<?php /**PATH C:\wamp64\www\offerswing_test2\resources\views/layouts/mainfooter.blade.php ENDPATH**/ ?>