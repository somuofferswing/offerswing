@extends('layouts.app')

@section('content')

<style>
    {
            {
            $page->style
        }
    }
</style>

<div class="container">
    <nav aria-label="breadcrumb">
        <ol class="breadcrumb">
            <li class="breadcrumb-item"><a href="/">Home</a></li>
            <li class="breadcrumb-item active" aria-current="page">{{$page->page_name}}</li>
        </ol>
    </nav>

    {!! $page->content !!}
</div>


@endsection

@section('scripts')

<script>
    {{ $page->script }}
</script>

@endsection
