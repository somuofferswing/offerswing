@extends('layouts.admin')

@section('content')
    <div class="content-wrapper">
        <section class="content-header">
            <h1>
                Tokens
                <small>All Tokens</small>
            </h1>
            <!-- <ol class="breadcrumb">
              <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
              <li class="active">Dashboard</li>
            </ol> -->
        </section>
        <section class="content">

            <div class="box box-primary">
                <div class="box-header with-border">
                    <h3 class="box-title">All Shops</h3>
                </div>
                <div class="table-responsive">
                    <table class="table table-striped">
                        <thead>
                        <tr>
                            <th>Token Id</th>
                            <th>Points Rewarded</th>

                            <th colspan="2">Action</th>
                        </tr>
                        </thead>
                        <tbody>
                        @foreach($tokens as $token)
                            <tr>
                                <td>{{ $token->token_id }}</td>
                                <td>{{ $token->points_rewarded }}</td>

                                {{-- <td><a href="{{ url('view-shop', [$token->id]) }}" class="btn btn-primary">View</a></td> --}}
                                <td><a href="{{ url('/token/edit', [$token->id]) }}" class="btn btn-default">Edit</a></td>
                                <td>
                                    <form action="{{ url('/token/delete', [$token->id])}}" method="post">
                                        @csrf
                                        @method('POST')
                                        <button class="btn btn-danger" type="submit">Delete</button>
                                    </form>
                                </td>

                                {{-- <td><a href="{{ route('delete-shop', [$shop->id]) }}" class="btn btn-danger">Delete</a></td> --}}
{{--                                <td>Delete</td>--}}
                            </tr>
                        @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
    </section>
    </div>
@endsection
@section('scripts')

<script>
$('.del').on('click',function(){
        var id = $(this).data('id');
        if (confirm('Do you want to delete shop?')) {
            // alert('Thanks for confirming');
            $.ajax({
                type: "POST",
                url: "{{  url('shop/delete/') }}"+"/"+id,
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
                success: function (response) {
                    // console.log(response);
                    alert(response);
                    window.location.reload();
                }
            });
        } else {

        }

});
</script>

@endsection
