<div id="<?php echo e($coursel_id); ?>" class="carousel slide" data-ride="carousel">

    <ul class="carousel-indicators">

        <?php for($i = 0; $i < count($slider) ; $i++): ?> <li data-target="#<?php echo e($coursel_id); ?>" data-slide-to="<?php echo e($i); ?>"

            class="<?php echo e($i==0 ? 'active' : ''); ?>">

            </li>

            <?php endfor; ?>

    </ul>

    <div class="carousel-inner">

        <?php $__currentLoopData = $slider; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $index=>$item): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>

        <div class="carousel-item <?php echo e($index==0 ? 'active' : ''); ?>">

            <?php if($item->image_path): ?>

            <img class="mx-auto d-block img-fluid" alt="<?php echo e($item->name); ?>" src="<?php echo e(asset('storage/'.$item->image_path)); ?>"

                width="100%" style="min-height:300px;max-height:300px;">

            <?php else: ?>

            <img class="mx-auto d-block img-fluid" alt="<?php echo e($item->name); ?>" src="<?php echo e(asset('storage/'.$item->image)); ?>"

                width="100%" style="min-height:300px;max-height:300px;">



            <?php endif; ?>





        </div>

        <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>

    </div>

    <a class="carousel-control-prev" href="#<?php echo e($coursel_id); ?>" data-slide="prev"><span

            class="carousel-control-prev-icon"></span></a>

    <a class="carousel-control-next" href="#<?php echo e($coursel_id); ?>" data-slide="next"><span

            class="carousel-control-next-icon"></span></a>

</div><?php /**PATH /home/offerkrx/public_html/offerswing_test/resources/views/components/carousel.blade.php ENDPATH**/ ?>