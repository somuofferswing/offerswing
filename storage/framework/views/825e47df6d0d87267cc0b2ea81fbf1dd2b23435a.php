<!DOCTYPE html>
<html lang="en">
  <head>
  <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="format-detection" content="telephone=no">
    <meta name="apple-mobile-web-app-capable" content="yes">
    <meta name="author" content="">
    <meta name="keywords" content="">
    <meta name="description" content="">
    <meta name="csrf-token" content="<?php echo e(csrf_token()); ?>">
    <title>Offerswing</title>
    <style>
      :root {
        --main-bg-color: #dd2400;  
      }
    </style>
    <link href="https://fonts.googleapis.com/css?family=Work+Sans:300,400,500,600,700&amp;amp;subset=latin-ext" rel="stylesheet">
    <link rel="stylesheet" href="<?php echo e(asset('/assets/plugins/font-awesome/css/font-awesome.min.css')); ?>">
    <link rel="stylesheet" href="<?php echo e(asset('/assets/fonts/Linearicons/Linearicons/Font/demo-files/demo.css')); ?>">
    <link rel="stylesheet" href="<?php echo e(asset('/assets/plugins/bootstrap4/css/bootstrap.min.css')); ?>">
    <link rel="stylesheet" href="<?php echo e(asset('/assets/plugins/owl-carousel/assets/owl.carousel.css')); ?>">
    <link rel="stylesheet" href="<?php echo e(asset('/assets/plugins/slick/slick/slick.css')); ?>">
    <link rel="stylesheet" href="<?php echo e(asset('/assets/plugins/lightGallery-master/dist/css/lightgallery.min.css')); ?>">
    <link rel="stylesheet" href="<?php echo e(asset('/assets/plugins/jquery-bar-rating/dist/themes/fontawesome-stars.css')); ?>">
    <link rel="stylesheet" href="<?php echo e(asset('/assets/plugins/jquery-ui/jquery-ui.min.css')); ?>">
    <link rel="stylesheet" href="<?php echo e(asset('/assets/plugins/select2/dist/css/select2.min.css')); ?>">
    <link rel="stylesheet" href="<?php echo e(asset('/assets/css/style.css')); ?>">
    <link rel="stylesheet" href="<?php echo e(asset('/assets/css/current.css')); ?>">
    <script src="<?php echo e(asset('assets/plugins/jquery-1.12.4.min.js')); ?>"></script>
    <script src="<?php echo e(asset('assets/plugins/popper.min.js')); ?>"></script>
    <style>
      .locationInput{
            width: 175px;
        }
      </style>
  </head>
  <body class="layout-2">
    <header class="header header--standard header--autopart shadow" data-sticky="true">
      
    <div class="header__content">
        <div class="container">
          
          <div class="header__content-left">
            <a class="ps-logo" href="<?php echo e(url('/')); ?>">
              <img src="<?php echo e(asset('main/images/newlogo.png')); ?>" alt="">
            </a>
            <div class="menu--product-categories">
              <div class="menu__toggle"><i class="icon-menu"></i><span>Shop by Department</span></div>
              <div class="menu__content">
              <?php 
                $icons =["car", "wallet", "bicycle2", "woman", "library2", "city", "laptop", "group-work", "home4", "trumpet", "menu3", "graduation-hat", "theater"];
              ?>
                <ul class="menu--dropdown">
                
                <?php if($categories->isNotEmpty()): ?>
                  <?php $cnt = 1; ?>
                  <?php $__currentLoopData = $categories; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $key=>$item): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>

                    <?php if($cnt >= 14): ?>
                        <?php if($cnt == 14): ?>
                          <li class="menu-item-has-children has-mega-menu"><a href="#"><i class="icon-ellipsis"></i> More</a>
                              <div class="mega-menu">
                                  <div class="mega-menu__column">
                                      <h4>More Categories<span class="sub-toggle"></span></h4>
                                          <ul class="mega-menu__list">
                                              <li>
                                                  <a href="<?php echo url('/subcategories',[$item->id]);; ?>">
                                                      <?php echo e(ucwords(strtolower($item->category_name))); ?>

                                                  </a>
                        <?php else: ?>
                            <li>
                                <a href="<?php echo url('/subcategories',[$item->id]);; ?>">
                                    <?php echo e(ucwords(strtolower($item->category_name))); ?>

                                </a>
                        <?php endif; ?>

                        <?php if($cnt == count($categories)): ?>
                            </ul></div></div></li>
                        <?php endif; ?>

                        <?php else: ?>
                            <?php $iconValue = 'icon-'.$icons[$key]; ?>
                            <li>
                                <a href="<?php echo url('/subcategories',[$item->id]);; ?>">
                                    <i class="<?php echo e($iconValue); ?>"></i> 
                                    <?php echo e(ucwords(strtolower($item->category_name))); ?>

                                </a>
                            </li>
                        <?php endif; ?>

                      <?php $cnt++; ?>
                    <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                  <?php else: ?>
                      <li><a href="#">No Categories Found</a></li>
                  <?php endif; ?>  
                </ul>
              </div>
            </div>
          </div>
          
          <div class="header__content-center">
            <form class="ps-form--quick-search" action="<?php echo e(url('/search')); ?>" method="get">
              <div class="form-group--icon locationInput"><i class="icon-chevron-down"></i>
                  <select class="form-control">
                      <option>Location</option>
                      <option value="Srikakaulam">Srikakaulam</option>
                      <option value="Vishakapatnam">Vishakapatnam</option>
                  </select>
              </div>
              <input class="form-control" name="query" type="text" placeholder="Search for local offers, Deals & Near by Shops.....">
              <button>Search</button>
            </form>
          </div>
          
          <div class="header__content-right">
            <div class="header__actions">
              
              <!-- <div class="ps-block--header-hotline">
                <div class="ps-block__left"><i class="icon-telephone"></i></div>
                <div class="ps-block__right">
                  <p>Call us<strong>+91 9704 98 96 97</strong></p>
                </div>
              </div> -->

              <?php if(auth()->guard()->guest()): ?>
              <div class="ps-block--user-header">
                  <div class="ps-block__left"><i class="icon-user"></i></div>
                  <div class="ps-block__right">
                      <a href="#" data-toggle="modal" data-target="#myModal1" id="login_modal">Login</a>
                      <a href="#" data-toggle="modal" data-target="#myModal1" id="register_modal">Register</a>
                  </div>
              </div>
              <?php else: ?>
              <a class="header__extra" href="<?php echo e(url('wingpoints')); ?>" data-toggle="tooltip" data-original-title="Wing Coins">
                  <i class="icon-coin-dollar"></i>
              </a>
              <a class="header__extra" href="<?php echo e(url('orders')); ?>" data-toggle="tooltip"  data-original-title="My Orders">
                  <i class="icon-cart"></i>
              </a>
              <ul class="menu">
                  <li class="current-menu-item menu-item-has-children">
                      <a href="index.html"><?php echo e(Auth::user()->first_name." ".Auth::user()->last_name); ?></a>
                      <span class="sub-toggle"></span>
                      <ul class="sub-menu showdropLeft">
                          <li><a href="<?php echo e(url('userprofile')); ?>">Profile</a></li>
                          <li><a href="<?php echo e(url('favouriteshops')); ?>">Favourite Shops</a></li>
                          <li><a href="<?php echo e(url('/referafriend')); ?>">Refer a Friend</a></li>
                          <li><a href="#">Notifications<sup><?php echo e(count($notifications)); ?></sup></a></li>
                          <li>
                              <a href="<?php echo e(route('logout')); ?>" onclick="event.preventDefault(); document.getElementById('logout-form').submit();">Logout</a>
                          </li>
                      </ul>
                  </li>
              </ul>
              <?php endif; ?>
             
            </div>
          </div>
        </div>
      </div>
      
      <nav class="navigation">
        <div class="container">
            <div class="navigation__left">
                <div class="menu--product-categories">
                    <div class="menu__toggle"><i class="icon-menu"></i><span class="text-white"> Shop by Department</span></div>
                    <div class="menu__content">
                      <ul class="menu--dropdown">
                          
                        <?php if($categories->isNotEmpty()): ?>
                          <?php $cnt = 1; ?>
                          <?php $__currentLoopData = $categories; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $key=>$item): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
  
                            <?php if($cnt >= 14): ?>
                              <?php if($cnt == 14): ?>
                                <li class="menu-item-has-children has-mega-menu"><a href="#"><i class="icon-ellipsis"></i> More</a>
                                    <div class="mega-menu">
                                        <div class="mega-menu__column">
                                            <h4>More Categories<span class="sub-toggle"></span></h4>
                                                <ul class="mega-menu__list">
                                                    <li>
                                                        <a href="<?php echo url('/subcategories',[$item->id]);; ?>">
                                                            <?php echo e(ucwords(strtolower($item->category_name))); ?>

                                                        </a>
                              <?php else: ?>
                                  <li>
                                      <a href="<?php echo url('/subcategories',[$item->id]);; ?>">
                                          <?php echo e(ucwords(strtolower($item->category_name))); ?>

                                      </a>
                              <?php endif; ?>

                              <?php if($cnt == count($categories)): ?>
                                  </ul></div></div></li>
                              <?php endif; ?>
  
                            <?php else: ?>
                                <?php $iconValue = 'icon-'.$icons[$key]; ?>
                                <li>
                                    <a href="<?php echo url('/subcategories',[$item->id]);; ?>">
                                        <i class="<?php echo e($iconValue); ?>"></i> 
                                        <?php echo e(ucwords(strtolower($item->category_name))); ?>

                                    </a>
                                </li>
                            <?php endif; ?>
  
                          <?php $cnt++; ?>
                          <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                        <?php else: ?>
                            <li><a href="#">No Categories Found</a></li>
                        <?php endif; ?>  
                      </ul>
                  </div>
                </div>
            </div>
            <div class="navigation__right justify-content-end">
                <ul class="navigation__extra p-3">
                    <li><a href="<?php echo e(url('/addyourbusiness')); ?>">Add Your Business</a></li>
                    <li><a href="<?php echo e(url('/contactus')); ?>">Contact Us</a></li>
                    <li><a href="<?php echo e(url('/aboutus')); ?>">About Us</a></li>
                    <li><a href="<?php echo e(url('/faq')); ?>">FAQ's</a></li>
                </ul>
            </div>
        </div>
      </nav>
    </header>

    <header class="header header--mobile autopart shadow" data-sticky="true">
        <div class="navigation--mobile">
            <div class="navigation__left">
                <a class="ps-logo" href="index.html">
                    <img src="<?php echo e(asset('main/images/newlogo.png')); ?>" width="120px" alt="">
                </a>
            </div>
            <div class="navigation__right">
                <div class="header__actions pt-3">
                    <?php if(auth()->guard()->guest()): ?>
                        <div class="ps-block--user-header">
                            <div class="ps-block__left">
                                <a href="#" data-toggle="modal" data-target="#myModal1">
                                    <i class="icon-user"></i>
                                </a>
                            </div>
                            <div class="ps-block__right">
                                <a href="#" data-toggle="modal" data-target="#myModal1" id="login_modal">Login</a>
                                <a href="#" data-toggle="modal" data-target="#myModal1" id="register_modal">Register</a>
                            </div>
                        </div>
                    <?php else: ?>
                        <ul class="menu">
                            <li class="current-menu-item menu-item-has-children">
                                <i class="icon-user"></i><i class="icon-chevron-down" style="font-size:12px"></i>
                                <span class="sub-toggle"></span>
                                <ul class="sub-menu showdropLeft">
                                    <li><a href="<?php echo e(url('userprofile')); ?>">Profile</a></li>
                                    <li><a href="<?php echo e(url('favouriteshops')); ?>">Favourite Shops</a></li>
                                    <li><a href="<?php echo e(url('/referafriend')); ?>">Refer a Friend</a></li>
                                    <li><a href="#">Notifications<sup><?php echo e(count($notifications)); ?></sup></a></li>
                                    <li>
                                        <a href="<?php echo e(route('logout')); ?>" onclick="event.preventDefault(); document.getElementById('logout-form').submit();">Logout</a>
                                    </li>
                                </ul>
                            </li>
                        </ul>
                    <?php endif; ?>
                </div>
            </div>
        </div>
    </header>

    <div class="ps-panel--sidebar" id="navigation-mobile">
        <div class="ps-panel__header">
            <h3>Categories</h3>
        </div>
        <div class="ps-panel__content">
            <ul class="menu--mobile">
            <?php if($categories->isNotEmpty()): ?>
                <?php $__currentLoopData = $categories; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $item): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                <li><a href="<?php echo url('/subcategories',[$item->id]);; ?>"><?php echo e(ucwords(strtolower($item->category_name))); ?></a></li>
                <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
            <?php else: ?>
                <li><a href="#">No Categories Found</a></li>
            <?php endif; ?>   
            </ul>
        </div>
    </div>
    <div class="navigation--list">
        <div class="navigation__content">
            <a class="navigation__item ps-toggle--sidebar" href="#menu-mobile">
                <i class="icon-menu"></i><span> Menu</span>
            </a>
            <a class="navigation__item ps-toggle--sidebar" href="#navigation-mobile">
                <i class="icon-list4"></i><span> Categories</span>
            </a>
            <a class="navigation__item ps-toggle--sidebar" href="#search-sidebar">
                <i class="icon-magnifier"></i><span> Search</span>
            </a>
            <?php if(auth()->guard()->check()): ?>
            <a class="navigation__item ps-toggle--sidebar" href="<?php echo e(url('wingpoints')); ?>">
                <i class="icon-coin-dollar"></i><span> Coins</span>
            </a>
            <a class="navigation__item ps-toggle--sidebar" href="<?php echo e(url('orders')); ?>">
                <i class="icon-cart"></i><span> Cart</span>
            </a>
            <?php endif; ?>
        </div>
    </div>
    <div class="ps-panel--sidebar" id="search-sidebar">
        <div class="ps-panel__header">
            <form class="ps-form--quick-search" action="<?php echo e(url('/search')); ?>" method="get">
              <div class="form-group--icon locationInput"><i class="icon-chevron-down"></i>
                  <select class="form-control">
                      <option>Location</option>
                      <option value="Srikakaulam">Srikakaulam</option>
                      <option value="Vishakapatnam">Vishakapatnam</option>
                  </select>
              </div>
              <input class="form-control" name="query" type="text" placeholder="Search for local offers, Deals & Near by Shops.....">
              <button><i class="icon-magnifier"></i></button>
            </form>
        </div>
        <div class="navigation__content"></div>
    </div>
    <div class="ps-panel--sidebar" id="menu-mobile">
        <div class="ps-panel__header">
            <h3>Menu</h3>
        </div>
        <div class="ps-panel__content">
            <ul class="menu--mobile">
                <li class="current-menu-item"><a href="index.html">Add Your Business</a></li>
                <li class="current-menu-item"><a href="index.html">Contact Us</a></li>
                <li class="current-menu-item"><a href="index.html">About Us</a></li>
                <li class="current-menu-item"><a href="index.html">FAQ's</a></li>
            </ul>
        </div>
    </div>
    <!--include ../../shared/header-mobile-->
    <div class="ps-section--default ps-document">
      <div class="container pt-100">
      <button class="ps-btn">Submit</button>
      <div class="col-xl-3 col-lg-3 col-md-3 col-sm-4 col-6 ">
                    <div class="ps-block--category">
                        <a class="ps-block__overlay" href="http://localhost/offerswing/public/subcategories/401"></a>
                                                <img src="http://localhost/offerswing/public/category_images/HOSPITALS_373.png" alt="" width="90" class="mb-4">
                                                <p>HOSPITALS</p>
                    </div>
                </div>
        <h3>Six big devils from Japan quickly forgot how</h3>
        <p>Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. Aenean massa. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Donec quam felis, ultricies nec, pellentesque eu, pretium quis, sem. Nulla consequat massa quis enim. Donec pede justo, fringilla vel, aliquet nec, vulputate eget, arcu. In enim justo, rhoncus ut, imperdiet a, venenatis vitae, justo. Nullam dictum felis eu pede mollis pretium. Integer tincidunt. Cras dapibus. Vivamus elementum semper nisi. Aenean vulputate eleifend tellus. Aenean leo ligula, porttitor eu, consequat vitae, eleifend ac, enim. Aliquam lorem ante, dapibus in, viverra quis, feugiat a, tellus. Phasellus viverra nulla ut metus varius laoreet. Quisque rutrum. Aenean imperdiet. Etiam ultricies nisi vel augue. Curabitur ullamcorper ultricies nisi. Nam eget dui. Etiam rhoncus. Maecenas tempus, tellus eget condimentum rhoncus, sem quam semper libero, sit amet adipiscing sem neque sed ipsum. Nam quam nunc, blandit vel, luctus pulvinar, hendrerit id, lorem. Maecenas nec odio et ante tincidunt tempus. Donec vitae sapien ut libero venenatis faucibus. Nullam quis ante. Etiam sit amet orci eget eros faucibus tincidunt. Duis leo. Sed fringilla mauris sit amet nibh. Donec sodales sagittis magna. Sed consequat, leo eget bibendum sodales, augue velit cursus nunc, quis gravida magna mi a libero. Fusce vulputate eleifend sapien. Vestibulum purus quam, scelerisque ut, mollis sed, nonummy id, metus. Nullam accumsan lorem in dui. Cras ultricies mi eu turpis hendrerit fringilla. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; In ac dui quis mi consectetuer lacinia. Nam pretium turpis et arcu. Duis arcu tortor, suscipit eget, imperdiet nec, imperdiet iaculis, ipsum. Sed aliquam ultrices mauris. Integer ante arcu, accumsan a, consectetuer eget, posuere ut, mauris. Praesent adipiscing. Phasellus ullamcorper ipsum rutrum nunc. Nunc nonummy metus. Vestibulum volutpat pretium libero. Cras id dui. Aenean ut eros et nisl sagittis vestibulum. Nullam nulla eros, ultricies sit amet, nonummy id, imperdiet feugiat, pede. Sed lectus. Donec mollis hendrerit risus. Phasellus nec sem in justo pellentesque facilisis. Etiam imperdiet imperdiet orci. Nunc nec neque. Phasellus leo dolor, tempus non, auctor et, hendrerit quis, nisi. Curabitur ligula sapien, tincidunt non, euismod vitae, posuere imperdiet, leo. Maecenas malesuada. Praesent congue erat at massa. Sed cursus turpis vitae tortor. Donec posuere vulputate arcu. Phasellus accumsan cursus velit. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; Sed aliquam, nisi quis porttitor congue, elit erat euismod orci, ac</p>
        <p>Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae dicta sunt explicabo. Nemo enim ipsam voluptatem quia voluptas sit aspernatur aut odit aut fugit, sed quia consequuntur magni dolores eos qui ratione voluptatem sequi nesciunt. Neque porro quisquam est, qui dolorem ipsum quia dolor sit amet, consectetur, adipisci velit, sed quia non numquam eius modi tempora incidunt ut labore et dolore magnam aliquam quaerat voluptatem. Ut enim ad minima veniam, quis nostrum exercitationem ullam corporis suscipit laboriosam, nisi ut aliquid ex ea commodi consequatur? Quis autem vel eum iure reprehenderit qui in ea voluptate velit esse quam nihil molestiae consequatur, vel illum qui dolorem eum fugiat quo voluptas nulla pariatur? At vero eos et accusamus et iusto odio dignissimos ducimus qui blanditiis praesentium voluptatum deleniti atque corrupti quos dolores et quas molestias excepturi sint occaecati cupiditate non provident, similique sunt in culpa qui officia deserunt mollitia animi, id est laborum et dolorum fuga. Et harum quidem rerum facilis est et expedita distinctio. Nam libero tempore, cum soluta nobis est eligendi optio cumque nihil impedit quo minus id quod maxime placeat facere possimus, omnis voluptas assumenda est, omnis dolor repellendus. Temporibus autem quibusdam et aut officiis debitis aut rerum necessitatibus saepe eveniet ut et voluptates repudiandae sint et molestiae non recusandae. Itaque earum rerum hic tenetur a sapiente delectus, ut aut reiciendis voluptatibus maiores alias consequatur aut perferendis doloribus asperiores repellat.Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae dicta sunt explicabo. Nemo enim ipsam voluptatem quia voluptas sit aspernatur aut odit aut fugit, sed quia consequuntur magni dolores eos qui ratione voluptatem sequi nesciunt. Neque porro quisquam est, qui dolorem ipsum quia dolor sit amet, consectetur, adipisci velit, sed quia non numquam eius modi tempora incidunt ut labore et dolore magnam aliquam quaerat voluptatem. Ut enim ad minima veniam, quis nostrum exercitationem ullam corporis suscipit laboriosam, nisi ut aliquid ex ea commodi consequatur? Quis autem vel eum iure reprehenderit qui in ea voluptate velit esse quam nihil molestiae consequatur, vel illum qui dolorem eum fugiat quo voluptas nulla pariatur? At vero eos et accusamus et iusto odio dignissimos ducimus qui blanditiis praesentium voluptatum deleniti atque corrupti quos dolores et quas molestias</p>
        <p>Far far away, behind the word mountains, far from the countries Vokalia and Consonantia, there live the blind texts. Separated they live in Bookmarksgrove right at the coast of the Semantics, a large language ocean. A small river named Duden flows by their place and supplies it with the necessary regelialia. It is a paradisematic country, in which roasted parts of sentences fly into your mouth. Even the all-powerful Pointing has no control about the blind texts it is an almost unorthographic life One day however a small line of blind text by the name of Lorem Ipsum decided to leave for the far World of Grammar. The Big Oxmox advised her not to do so, because there were thousands of bad Commas, wild Question Marks and devious Semikoli, but the Little Blind Text didn’t listen. She packed her seven versalia, put her initial into the belt and made herself on the way. When she reached the first hills of the Italic Mountains, she had a last view back on the skyline of her hometown Bookmarksgrove, the headline of Alphabet Village and the subline of her own road, the Line Lane. Pityful a rethoric question ran over her cheek, then she continued her way. On her way she met a copy. The copy warned the Little Blind Text, that where it came from it would have been rewritten a thousand times and everything that was left from its origin would be the word "and" and the Little Blind Text should turn around and return to its own, safe country. But nothing the copy said could convince her and so it didn’t take long until a few insidious Copy Writers ambushed her, made her drunk with Longe and Parole and dragged her into their agency, where they abused her for their projects again and again. And if she hasn’t been rewritten, then they are still using her.Far far away, behind the word mountains, far from the countries Vokalia and Consonantia, there live the blind texts. Separated they live in Bookmarksgrove right at the coast of the Semantics, a large language ocean. A small river named Duden flows by their place and supplies it with the necessary regelialia. It is a paradisematic country, in which roasted parts of sentences fly into your mouth. Even the all-powerful Pointing has no control about the blind texts it is an almost unorthographic life One</p>
      </div>
    </div>
    <div class="ps-newsletter">
      <div class="ps-container">
        <form class="ps-form--newsletter" action="do_action" method="post">
          <div class="row">
            <div class="col-xl-5 col-lg-12 col-md-12 col-sm-12 col-12 ">
              <div class="ps-form__left">
                <h3>Newsletter</h3>
                <p>Subcribe to get information about products and coupons</p>
              </div>
            </div>
            <div class="col-xl-7 col-lg-12 col-md-12 col-sm-12 col-12 ">
              <div class="ps-form__right">
                <div class="form-group--nest">
                  <input class="form-control" type="email" placeholder="Email address">
                  <button class="ps-btn">Subscribe</button>
                </div>
              </div>
            </div>
          </div>
        </form>
      </div>
    </div>
    <div id="back2top"><i class="pe-7s-angle-up"></i></div>
    <div class="ps-site-overlay"></div>
    <div id="loader-wrapper">
      <div class="loader-section section-left"></div>
      <div class="loader-section section-right"></div>
    </div>
    <div class="ps-search" id="site-search"><a class="ps-btn--close" href="#"></a>
      <div class="ps-search__content">
        <form class="ps-form--primary-search" action="do_action" method="post">
          <input class="form-control" type="text" placeholder="Search for...">
          <button><i class="aroma-magnifying-glass"></i></button>
        </form>
      </div>
    </div>

    <div id="myModal1" class="modal fade" role="dialog">
  <div class="modal-dialog">

    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">Modal Header</h4>
      </div>
      <div class="modal-body">
        <p>Some text in the modal.</p>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
      </div>
    </div>

  </div>
</div>
    <!--include shared/cart-sidebar-->
    <!-- Plugins-->
    <script src="<?php echo e(asset('assets/plugins/owl-carousel/owl.carousel.min.js')); ?>"></script>
    <script src="<?php echo e(asset('assets/plugins/bootstrap4/js/bootstrap.min.js')); ?>"></script>
    <script src="<?php echo e(asset('assets/plugins/imagesloaded.pkgd.min.js')); ?>"></script>
    <script src="<?php echo e(asset('assets/plugins/masonry.pkgd.min.js')); ?>"></script>
    <script src="<?php echo e(asset('assets/plugins/isotope.pkgd.min.js')); ?>"></script>
    <script src="<?php echo e(asset('assets/plugins/jquery.matchHeight-min.js')); ?>"></script>
    <script src="<?php echo e(asset('assets/plugins/slick/slick/slick.min.js')); ?>"></script>
    <script src="<?php echo e(asset('assets/plugins/jquery-bar-rating/dist/jquery.barrating.min.js')); ?>"></script>
    <script src="<?php echo e(asset('assets/plugins/slick-animation.min.js')); ?>"></script>
    <script src="<?php echo e(asset('assets/plugins/lightGallery-master/dist/js/lightgallery-all.min.js')); ?>"></script>
    <script src="<?php echo e(asset('assets/plugins/jquery-ui/jquery-ui.min.js')); ?>"></script>
    <script src="<?php echo e(asset('assets/plugins/sticky-sidebar/dist/sticky-sidebar.min.js')); ?>"></script>
    <script src="<?php echo e(asset('assets/plugins/jquery.slimscroll.min.js')); ?>"></script>
    <script src="<?php echo e(asset('assets/plugins/select2/dist/js/select2.full.min.js')); ?>"></script>
    <!-- custom scripts-->
    <script src="<?php echo e(asset('assets/js/main.js')); ?>"></script>
  </body>
</html><?php /**PATH C:\xampp\htdocs\offerswing\resources\views/layouts/m2.blade.php ENDPATH**/ ?>