<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class NewsImage extends Model
{
    protected $fillable = [
     'image', 'news_id'
    ];

    public function parent()
    {
        return $this->belongsTo(NewsFeed::class, 'news_id');
    }
}
