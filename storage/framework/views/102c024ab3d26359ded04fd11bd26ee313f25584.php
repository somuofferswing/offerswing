<?php $__env->startSection('content'); ?>
<div class="ps-breadcrumb">
    <div class="container">
        <ul class="breadcrumb text-capitalize" id="breadcrumb">
            <li><a href="<?php echo e(url('/')); ?>">Home</a></li>
            <li>My Orders</li>
        </ul>
    </div>
</div>

<section>
    <div class="ps-page--product">
        <div class="container">

            <div class="ps-product--detail">
                <ul class="ps-tab-list">
                    <li class="active"><a href="#tab-deals">Deals</a></li>
                    <li><a href="#tab-events">Events</a></li>
                    <li><a href="#tab-gifts">Gifts</a></li>
                </ul>
                <div class="ps-tabs">

                    <!-- deals tab -->
                    <div class="ps-tab active" id="tab-deals">
                        <?php if($deal_orders->isNotEmpty()): ?>
                        <div class="row">
                            <?php $__currentLoopData = $deal_orders; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $item): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>

                            <?php
                            // $order_details = $item->orderDetails
                            $deal = $item->orderDetails->deal;
                            ?>
                            <div class="col-xl-4 col-lg-4 col-md-6 col-sm-12 col-12 ">
                                <div class="ps-post">
                                    <div class="ps-post__thumbnail">
                                        <a class="ps-post__overlay"
                                            href="<?php echo e(url('/orderdeal',["$item->order_id"])); ?>"></a>
                                        <img src="<?php echo e(asset('storage/'.$deal->pic)); ?>" alt="">
                                    </div>
                                    <div class="ps-post__content">
                                        <div class="ps-post__meta">
                                            <?php if($deal->valid_upto >= date('Y-m-d', time())): ?>
                                            <span>Expiry On : <?php echo e($deal->valid_upto); ?></span>
                                            <?php else: ?> <span class="text-danger">Expired</span>
                                            <?php endif; ?>

                                            <span class="pull-right">Status :
                                                <?php if($item->used == 0): ?> Not Claimed
                                                <?php else: ?> Claimed
                                                <?php endif; ?>
                                            </span>
                                        </div>
                                        <a class="ps-post__title text-truncate"
                                            href="<?php echo e(url('/orderdeal',["$item->order_id"])); ?>">
                                            <?php echo e(ucfirst($deal->name)); ?>

                                        </a>
                                        <div class="text-right">
                                            <a class="font-weight-bold"
                                                href="<?php echo e(url('/orderdeal',["$item->order_id"])); ?>">View Invoice</a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                        </div>
                        <?php else: ?>
                        <p>No Deal Orders</p>
                        <?php endif; ?>
                    </div>
                    <!-- end of deals tab -->

                    <!-- events tab -->
                    <div class="ps-tab" id="tab-events">
                        <?php if($event_orders->isNotEmpty()): ?>
                        <div class="row">
                            <?php $__currentLoopData = $event_orders; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $item): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>

                            <?php $event = $item->orderDetails->event; ?>
                            <div class="col-xl-4 col-lg-4 col-md-6 col-sm-12 col-12 ">
                                <div class="ps-post">
                                    <div class="ps-post__thumbnail">
                                        <a class="ps-post__overlay"
                                            href="<?php echo e(url('/orderevent',["$item->order_id"])); ?>"></a>
                                        <img src="<?php echo e(asset('storage/'.$event->event_image)); ?>" alt="">
                                    </div>
                                    <div class="ps-post__content">
                                        <div class="ps-post__meta">
                                            <span>Event Date : <?php echo e($event->event_date); ?></span>
                                            <span class="pull-right">Status :
                                                <?php if($item->used == 0): ?> Not Claimed
                                                <?php else: ?> Claimed
                                                <?php endif; ?>
                                            </span>
                                        </div>
                                        <a class="ps-post__title text-truncate"
                                            href="<?php echo e(url('/orderevent',["$item->order_id"])); ?>"><?php echo e($event->title); ?></a>
                                        <div class="text-right">
                                            <a class="font-weight-bold"
                                                href="<?php echo e(url('/orderevent',["$item->order_id"])); ?>">View Invoice</a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                        </div>
                        <?php else: ?>
                        <p>No Events Orders</p>
                        <?php endif; ?>
                    </div>
                    <!-- end of events tab -->

                    <!-- gits tab -->
                    <div class="ps-tab" id="tab-gifts">
                        <?php if($gift_orders->isNotEmpty()): ?>
                        <div class="row">
                            <?php $__currentLoopData = $gift_orders; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $item): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>

                            <?php $gift = $item->orderDetails->gift; ?>
                            <div class="col-xl-4 col-lg-4 col-md-6 col-sm-12 col-12 ">
                                <div class="ps-post">
                                    <div class="ps-post__thumbnail">
                                        <a class="ps-post__overlay"
                                            href="<?php echo e(url('/ordergift',["$item->order_id"])); ?>"></a>
                                        <img src="<?php echo e(asset('storage/'.$gift->image)); ?>" alt="">
                                    </div>
                                    <div class="ps-post__content">
                                        <div class="ps-post__meta">
                                            <span class="pull-right">Status :
                                                <?php if($item->used == 0): ?> Not Claimed
                                                <?php else: ?> Claimed
                                                <?php endif; ?>
                                            </span>
                                        </div>
                                        <a class="ps-post__title text-truncate"
                                            href="<?php echo e(url('/ordergift',["$item->order_id"])); ?>"><?php echo e($gift->gift_name); ?></a>
                                        <div class="text-right">
                                            <a class="font-weight-bold"
                                                href="<?php echo e(url('/ordergift',["$item->order_id"])); ?>">View Invoice</a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                        </div>
                        <?php else: ?>
                        <p> No Gifts Orders</p>
                        <?php endif; ?>
                    </div>
                    <!-- end of gits tab -->

                </div>
            </div>
        </div>
    </div>
</section>
<?php $__env->stopSection(); ?>
<?php echo $__env->make('layouts.app', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH C:\wamp64\www\offers\resources\views/frontend/order.blade.php ENDPATH**/ ?>