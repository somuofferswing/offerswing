@extends('layouts.admin')



@section('content')

<div class="content-wrapper">

    <section class="content-header">

        <h1>

            Wing Deals

            <small>All Wing Deals</small>

        </h1>

        {{-- <ol class="breadcrumb">

        <li><a href="{{ route('dashboard') }}"><i class="fa fa-dashboard"></i> Dashboard</a></li>

        <li>Offers</li>

        <li class="active">All Offers</li>

        </ol> --}}

    </section>

    <section class="content">

        @if(session()->get('success'))

        <div class="alert alert-success" id="success">

            {{ session()->get('success') }}

        </div><br />

        @endif

        @if (Auth::user()->user_type == 1)

{{--        <div class="box box-primary">--}}

{{--            <div class="box-header with-border">--}}

{{--                <h3 class="box-title">Offers filter</h3>--}}

{{--            </div>--}}

{{--            <div class="box-body">--}}

{{--                <form action="{{ route('offers') }}" id="filter">--}}

{{--                    <div class="row">--}}

{{--                        <div class="form-group  col-sm-6">--}}

{{--                            <label for="shop_id" class="col-sm-2 control-label">Shop</label>--}}

{{--                            <div class="col-sm-10">--}}



{{--                                <select class="form-control" id="shop_id" name="shop_id" value="{{ old('shop_id') }}">--}}

{{--                                    <option value="">--Select--</option>--}}

{{--                                    @foreach($shops as $shop)--}}

{{--                                    <option value="{{ $shop->id }}">{{ $shop->name }}</option>--}}

{{--                                    @endforeach--}}

{{--                                </select>--}}

{{--                            </div>--}}

{{--                        </div>--}}

{{--                        <div class="form-group col-sm-6">--}}

{{--                            <label for="category_id" class="col-sm-2 control-label">Category</label>--}}

{{--                            <div class="col-sm-10">--}}



{{--                                <select class="form-control" id="category_id" name="category_id"--}}

{{--                                    value="{{ old('category_id') }}">--}}

{{--                                    <option value="">--Select--</option>--}}

{{--                                    @foreach($category as $key =>$cat)--}}

{{--                                    <option value="{{ $key }}">{{ $cat }}</option>--}}

{{--                                    @endforeach--}}

{{--                                </select>--}}

{{--                                @if ($errors->has('category_id'))--}}

{{--                                <div class="danger">{{ $errors->first('category_id') }}</div>--}}

{{--                                @endif--}}

{{--                            </div>--}}

{{--                        </div>--}}

{{--                    </div>--}}

{{--                    <div>--}}

{{--                        <button type="submit" class="btn btn-primary">Filter</button>--}}

{{--                        <button type="reset" class="btn btn-warning" id="reset">Reset</button>--}}

{{--                    </div>--}}

{{--                </form>--}}



{{--            </div>--}}

{{--        </div>--}}

        @endif



        <div class="box">

            <div class="box-header with-border">

                <h3 class="box-title">All Offers</h3>

                @if (Auth::user()->user_type == 1)

                <a href="{!! url('/offer/create'); !!}" class="pull-right btn btn-info">Add Offer</a>

                @endif

            </div>

            <div class="box-body">

                <div class="table-responsive">

                    <table class="table table-striped">

                        <thead>

                            <tr>

                                <th>OfferID</th>

                                <th>Name</th>

                                <th>Description</th>

                                <th>Start Date</th>

                                <th>End Date</th>

                                <th>Shop Name</th>

{{--                                <th>Category Name</th>--}}

                                <th colspan="2">Action</th>

                            </tr>

                        </thead>

                        <tbody>

                            @foreach($offers as $offer)

                            <tr>

                                <td>{{$offer->Offer_id}}</td>

                                <td>{{$offer->offer_name}}</td>

                                <td>{!! $offer->description !!}</td>

                                <td>{{$offer->from_date}}</td>

                                <td>{{$offer->to_date}}</td>

                                <td>{{$offer->name}}</td>

{{--                                <td>{{$offer->category_name}}</td>--}}

                                @if (Auth::user()->user_type == 1)

                                <td><a class="btn btn-default"

                                        href="{{ route('edit-offer', [$offer->Offer_id]) }}">Edit</a>

                                </td>



                                <td>

                                    <form action="{{ route('delete-offer', [$offer->Offer_id])}}" method="post">

                                        @csrf

                                        @method('DELETE')

                                        <button class="btn btn-danger" type="submit">Delete</button>

                                    </form>

                                </td>

                                @else

                                <td><a class="btn btn-default" href="{{ route('view-offer', [$offer->Offer_id]) }}">view

                                        details</a>

                                </td>

                                @endif

                            </tr>

                            @endforeach

                        </tbody>

                    </table>

                </div>

            </div>



        </div>

    </section>

</div>

@endsection

@section('scripts')

<script>

    $('#success').delay(5000).fadeOut('slow');

    $('#reset').on('click',function(){

        // $("#filter")[0].reset();

        $('#shop_id').val('');

        $('#category_id').val('');

        $('#filter')[0].submit();

    });



</script>

@endsection
