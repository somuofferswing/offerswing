@extends('layouts.admin')

@section('content')
<style>
    .btn:focus,
    .btn:active,
    button:focus,
    button:active {
        outline: none !important;
        box-shadow: none !important;
    }

    #image-gallery .modal-footer {
        display: block;
    }

    /* .thumb {
        margin-top: 15px;
        margin-bottom: 15px;
    } */

    #map {
        height: 400px;
        /* The height is 400 pixels */
        width: 100%;
        /* The width is the width of the web page */
    }
</style>
<div class="content-wrapper">
    <?php //var_dump($shops); ?>
    <section class="content-header">
        <h1>
            Shop Details
            <small>( {{ $shops->shop_name }} )</small>
        </h1>
        <!-- <ol class="breadcrumb">
              <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
              <li class="active">Dashboard</li>
            </ol> -->
    </section>
    <section class="content">

        <div class="box">
            <div class="box-header with-border">
                <h3 class="box-title">{{ $shops->shop_name }}</h3>
            </div>
            <!-- /.box-header -->
            <div class="box-body">

                <div class="container">
                    <div class="row" >
                        <div class="row">
                            @foreach ($shop_images as $image)

                            <div class="col-lg-3 col-md-4 col-xs-6 thumb">
                                <a class="thumbnail" href="#" data-image-id="" data-toggle="modal" data-title=""
                                    data-image="<?php echo asset("$image->image_path")?>" data-target="#image-gallery">
                                    <img class="img-thumbnail" src="<?php echo asset("$image->image_path")?>"
                                        alt="Another alt text" style='max-width:200px'>
                                </a>
                            </div>

                            @endforeach
                        </div>
                        @if (!empty($shop_menus))
                        <h4>Shop menus</h4>
                        <div class="row">
                            @foreach ($shop_menus as $image)

                            <div class="col-lg-3 col-md-4 col-xs-6 thumb">
                                <a class="thumbnail" href="#" data-image-id="" data-toggle="modal" data-title=""
                                    data-image="<?php echo asset("$image->image_path")?>" data-target="#image-gallery">
                                    <img class="img-thumbnail" src="<?php echo asset("$image->image_path")?>"
                                        alt="Another alt text" style='max-width:200px'>
                                </a>
                            </div>

                            @endforeach
                        </div>

                        @endif



                        <div class="modal fade" id="image-gallery" tabindex="-1" role="dialog"
                            aria-labelledby="myModalLabel" aria-hidden="true">
                            <div class="modal-dialog modal-md">
                                <div class="modal-content">
                                    <div class="modal-header">
                                        <b class="modal-title" id="image-gallery-title"></b>
                                        <button type="button" class="close" data-dismiss="modal"><span
                                                aria-hidden="true">×</span><span class="sr-only">Close</span>
                                        </button>
                                    </div>
                                    <div class="modal-body">
                                        <img id="image-gallery-image" class="img-responsive col-md-12" src="">
                                    </div>
                                    <div class="modal-footer">
                                        <button type="button" class="btn btn-secondary float-left"
                                            id="show-previous-image"><i class="fa fa-arrow-left"></i>
                                        </button>

                                        <button type="button" id="show-next-image"
                                            class="btn btn-secondary float-right"><i class="fa fa-arrow-right"></i>
                                        </button>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="col-sm-6">
                        <table class="table table-striped">
                            <tbody>
                                <tr>
                                    <h3>Shop Details</h3>
                                </tr>
                                <tr>
                                    <td>
                                        <b>Shop Name</b>
                                    </td>
                                    <td>{{ $shops->shop_name }}</td>
                                </tr>
                                <tr>
                                    <td>
                                        <b>Owner Name</b>
                                    </td>
                                    <td>{{ $shops->first_name." ".$shops->last_name  }}</td>
                                </tr>
                                <tr>
                                    <td>
                                        <b>Email</b>
                                    </td>
                                    <td>{{ $shops->email }}</td>
                                </tr>
                                <tr>
                                    <td>
                                        <b>Mobile</b>
                                    </td>
                                    <td>{{ $shops->phone_number }}</td>
                                </tr>

                                <tr>
                                    <td>
                                        <b>Description</b>
                                    </td>
                                    <td>{{ $shops->shop_description }}</td>
                                </tr>
                                <tr>
                                    <td>
                                        <b>Address</b>
                                    </td>
                                    <td>{{ $shops->address}} <br> {{ $shops->city }} <br> {{ $shops->state }} <br>
                                        {{ $shops->pincode }}</td>
                                </tr>
                            </tbody>
                        </table>
                        <table class="table table-striped">
                            <tbody>
                                <tr>
                                    <h3>Bank Details</h3>
                                </tr>
                                <tr>
                                    <td>
                                        <b>Bank Name</b>
                                    </td>
                                    <td>{{ $shops->bank_name }}</td>
                                </tr>
                                <tr>
                                    <td>
                                        <b>Account Number</b>
                                    </td>
                                    <td>{{ $shops->account_number }}</td>
                                </tr>
                                <tr>
                                    <td>
                                        <b>IFSC Code</b>
                                    </td>
                                    <td>{{ $shops->IFSC_code }}</td>
                                </tr>

                            </tbody>
                        </table>
                    </div>
                    <div class="col-sm-6">
                        <div id="map"></div>
                    </div>

                </div>
            </div>
            <!-- /.box-body -->
            <div class="box-footer clearfix">
                <div class="pull-right">
                    <a href="{{ route('edit-shop', [$shops->shop_id]) }}" class="btn btn-primary">Edit</a>
                    <a href="{{ url()->previous() }}" class="btn btn-danger">Back</a>
                </div>
            </div>
        </div>
        <!-- /.box -->
    </section>
</div>

@endsection

@section('scripts')
<script src="http://maps.googleapis.com/maps/api/js?key=AIzaSyBHLFZ4C3XB7MDwjywzpBC1ZWMH0u2zuE4&callback=initMap" async
    defer>
</script>
<script>
    let modalId = $('#image-gallery');

$(document)
  .ready(function () {

    loadGallery(true, 'a.thumbnail');

    //This function disables buttons when needed
    function disableButtons(counter_max, counter_current) {
      $('#show-previous-image, #show-next-image')
        .show();
      if (counter_max === counter_current) {
        $('#show-next-image')
          .hide();
      } else if (counter_current === 1) {
        $('#show-previous-image')
          .hide();
      }
    }

    /**
     *
     * @param setIDs        Sets IDs when DOM is loaded. If using a PHP counter, set to false.
     * @param setClickAttr  Sets the attribute for the click handler.
     */

    function loadGallery(setIDs, setClickAttr) {
      let current_image,
        selector,
        counter = 0;

      $('#show-next-image, #show-previous-image')
        .click(function () {
          if ($(this)
            .attr('id') === 'show-previous-image') {
            current_image--;
          } else {
            current_image++;
          }

          selector = $('[data-image-id="' + current_image + '"]');
          updateGallery(selector);
        });

      function updateGallery(selector) {
        let $sel = selector;
        current_image = $sel.data('image-id');
        $('#image-gallery-title')
          .text($sel.data('title'));
        $('#image-gallery-image')
          .attr('src', $sel.data('image'));
        disableButtons(counter, $sel.data('image-id'));
      }

      if (setIDs == true) {
        $('[data-image-id]')
          .each(function () {
            counter++;
            $(this)
              .attr('data-image-id', counter);
          });
      }
      $(setClickAttr)
        .on('click', function () {
          updateGallery($(this));
        });
    }
  });

// build key actions
$(document)
  .keydown(function (e) {
    switch (e.which) {
      case 37: // left
        if ((modalId.data('bs.modal') || {})._isShown && $('#show-previous-image').is(":visible")) {
          $('#show-previous-image')
            .click();
        }
        break;

      case 39: // right
        if ((modalId.data('bs.modal') || {})._isShown && $('#show-next-image').is(":visible")) {
          $('#show-next-image')
            .click();
        }
        break;

      default:
        return; // exit this handler for other keys
    }
    e.preventDefault(); // prevent the default action (scroll / move caret)
  });

   function initMap() {
  var uluru = {lat: {{ $shops->lat }}, lng: {{ $shops->lng }} };
  var map = new google.maps.Map(
      document.getElementById('map'), {zoom: 12, center: uluru});
  var marker = new google.maps.Marker({position: uluru, map: map});
}
</script>
@endsection
