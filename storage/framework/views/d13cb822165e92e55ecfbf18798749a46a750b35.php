

<?php $__env->startSection('content'); ?>

<style>
    #price>tbody>tr>td {
        /* test-align: right !important; */
        align: "right !important";
    }
</style>

<div class="container">


    <div class="panel panel-primary">
        <div class="panel-heading">
            <h3 class="panel-title">Your Order</h3>
        </div>
        <div class="panel-body">
            <?php if($order['type']== 'deal'): ?>

            <div class="table-responsive">
                <table class="table">
                    <tr>
                        <th>Item Name</th>
                        <th>Type</th>
                        <th>Qunatity</th>
                        <th>Price</th>
                        <th>Delete</th>
                        
                    </tr>

                    <td><?php echo e($deal['name']); ?></td>
                    <td><?php echo e($order['type']); ?></td>
                    <td><?php echo e($order['quantity']); ?></td>
                    <td><?php echo e($deal['price']); ?></td>
                    <td><a href="" class="delete"><i class="fa fa-trash" aria-hidden="true"></i></a></td>
                </table>
            </div>

            <div class="col-md-12">
                <div class="pull-right">
                    <table class="table" id="price">
                        <tr>
                            <td>Price :</td>
                            <td>₹ <?php echo e($deal['price']); ?> * <?php echo e($order['quantity']); ?></td>
                            <td>₹ <span class="pull-right"><?php echo e($deal['price'] * $order['quantity']); ?></span></td>
                        </tr>

                        <?php if($deal['payment_type'] == 'both'): ?>
                        <tr>
                            <td><input type="checkbox" name="" id="wing_points"
                                    <?php echo e(!empty($order['wing_points']) ? 'checked' : ''); ?>>Wing Points :</td>
                            <td><?php echo e(!empty($order['wing_points']) ? $order['wing_points'] : ''); ?></td>
                            <td>₹ <span
                                    class="pull-right"><?php echo e(!empty($order['wing_points']) ? $order['wing_points']/ $settings['value']  : ''); ?></span>
                            </td>
                        </tr>
                        <?php endif; ?>

                        


                        <tr>
                            <td>Total Price :</td>
                            <td></td>
                            <td>₹ <span class="pull-right"><?php echo e($order['final_price']); ?></span></td>
                        </tr>
                    </table>
                </div>
            </div>

            <?php else: ?>

            <div class="table-responsive">
                <table class="table">
                    <tr>
                        <th>Item Name</th>
                        <th>Type</th>
                        <th>Qunatity</th>
                        <th>Price</th>
                        <th>Delete</th>
                        
                    </tr>

                    <td><?php echo e($deal['title']); ?></td>
                    <td><?php echo e($order['type']); ?></td>
                    <td><button class="quantity" id="minus"><i class="fa fa-minus"
                                aria-hidden="true"></i></button><?php echo e($order['quantity']); ?><button class="quantity"
                            id="plus"><i class="fa fa-plus" aria-hidden="true"></i></button> </td>
                    <td><?php echo e($deal['event_price']); ?></td>
                    <td><a class="delete" href="javascript:void(0)"><i class="fa fa-trash" aria-hidden="true"></i></a>
                    </td>
                </table>
            </div>

            <div class="col-md-12">
                <div class="pull-right">
                    <table class="table" id="price">
                        <tr>
                            <td>Price :</td>
                            <td>₹ <?php echo e($deal['event_price'] * $order['quantity']); ?></td>
                        </tr>

                        <tr>
                            <td>Total Price :</td>
                            <td>₹ <?php echo e($deal['event_price'] * $order['quantity']); ?></td>
                        </tr>
                    </table>
                </div>

            </div>

            <?php endif; ?>
            <a href="<?php echo e(url('/proceedtopay',[$order['order_id']])); ?>" class="btn btn-success"> Proceed to pay </a>
        </div>
    </div>



</div>

<?php $__env->stopSection(); ?>

<?php $__env->startSection('scripts'); ?>
<script>
    var points_per_rupee  = '<?php echo e($settings['value']); ?>';
    var total_wing_points = "<?php echo e(Session::get('total_wingpoints')->points); ?>";



    $('#wing_points').on('click',function(){

        var max_points = $('#max_points').val();
        var id = "<?php echo e($order['id']); ?>";
        var wing_points;
        if(total_wing_points > max_points){
            wing_points = max_points;
        }else{
            wing_points = total_wing_points;
        }

        if(!$(this).prop("checked")){
            wing_points = 0;
        }
        var data= {
            id:id,
            wing_points:wing_points
        }

        $.ajax({
            type: "post",
            url: '<?php echo e(url("updateorder")); ?>',
            headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                     },
            data: data,
            // dataType: "dataType",
            success: function (response) {
                if(response.status == 'success'){
                    location.reload();
                }
            }
        });
    });

    $('.quantity').on('click',function(){
        var quant = $(this).prop('id');
        var id = "<?php echo e($order['id']); ?>";

        if(quant == 'plus'){
            quantity = 1
        }else{
            quantity= -1
        }

        var data= {
            id:id,
            quantity:quantity
        }

        $.ajax({
            type: "post",
            url: '<?php echo e(url("updateorder")); ?>',
            headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    },
            data: data,
            // dataType: "dataType",
            success: function (response) {
                if(response.status == 'success'){
                    location.reload();
                }else{
                    var url = response.url;
                    window.location.href = "<?php echo e(url('/')); ?>"+url;
                }
            }
        });
        });

        $('.delete').on('click',function(){
        var id = "<?php echo e($order['id']); ?>";
        var data= {
            id:id
        }

        $.ajax({
            type: "post",
            url: '<?php echo e(url("deleteorder")); ?>',
            headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    },
            data: data,
            success: function (response) {
                if(response.status == 'success'){
                    window.location.href = "<?php echo e(url('/')); ?>";
                }
            }
        });
        });

</script>
<?php $__env->stopSection(); ?>

<?php echo $__env->make('layouts.app', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH /home/offerkrx/public_html/offerswing/resources/views/orders.blade.php ENDPATH**/ ?>