@extends('layouts.app')

@section('content')
<style>
    .ps-faqs{
        padding:40px 0;
    }
    .ps-faqs .ps-section__header{
        padding-bottom:40px;
    }
</style>
<div class="ps-breadcrumb">
    <div class="container">
        <ul class="breadcrumb">
            <li><a href="{{ url('/') }}">Home</a></li>
            <li>FAQ's</li>
        </ul>
    </div>
</div>

<section >
    <div class="ps-faqs">
        <div class="ps-container">
            <div class="ps-section__header">
                <h1>Frequently Asked Questions</h1>
            </div>
            <div class="ps-section__content">
                <div class="row">
                    <div class="col-12 col-md-3">
                        <img src="{{ asset('main/images/icons/faq.png') }}">
                    </div>

                    <div class="col-12 col-md-9">
                        <div class="table-responsive">
                            <table class="table ps-table--faqs">
                                <tbody>
                                 @foreach ($faqs as $key=>$item)
                                    <tr>
                                        <td class="question">{{ $key+1 }} . {{ $item->question }}</td>
                                        <td>{!! $item->answer !!}</td>
                                    </tr>
                                 @endforeach
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>

            </div>
        </div>
    </div>
    <div class="ps-call-to-action">
        <div class="ps-container">
            <h3>We’re Here to Help !<a href="{{ url('/contactus') }}"> Contact us</a></h3>
        </div>
    </div>
</section>
@endsection
