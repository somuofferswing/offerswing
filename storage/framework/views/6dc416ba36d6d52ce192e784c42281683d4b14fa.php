<nav class="navbar navbar-expand-lg bg-light navbar-light fixed-top flex-column p-0">
    <div class="container-fluid border-bottom navtop">
        <ul class="nav mr-auto">
            <li class="nav-item"><span style="color:#dc3545;">Hi, </span> Good <span id="greetings">Morning</span></li>
        </ul>
        <ul class="nav">
            <li class="nav-item">
                <a style="color:#dc3545;" href="<?php echo e(url('/addyourbusiness')); ?>" class="nav-link">Add your Business</a>

            </li>
            <li class="nav-item">
                <a href="<?php echo e(url('/contactus')); ?>" class="nav-link">Contact Us</a>

            </li>
            <li class="nav-item">
                <a href="<?php echo e(url('/aboutus')); ?>" class="nav-link">About Us </a>

            </li>
            <li class="nav-item">
                <a href="<?php echo e(url('/faq')); ?>" class="nav-link"> Faq's </a>

            </li>
        </ul>
    </div>
    <div class="container-fluid pt-1 align-items-center navbottom" style="background-color: white;">
        
        <div class='col-md-3'>
            <a href="<?php echo e(url('/')); ?>"> <img src="<?php echo e(asset('main/images/newlogo.png')); ?>" width="250" alt=""
                    class="img-fluid" style="margin:0 auto !important;"></a>
        </div>

        <div class="col-md-5 align-self-center">
            <form method="GET" action="<?php echo e(url('/search')); ?>">
                <div class="input-group">
                    <select class="form-control col-md-3" style="border-radius:20px 0px 0px 20px;">
                        <option>Select Location</option>
                        <option value="Srikakaulam">Srikakaulam</option>
                        <option value="Vishakapatnam">Vishakapatnam</option>
                    </select>
                    <input type="text" class="form-control col-md-8" name="query"
                        placeholder="Search for local offers, Deals & Near by Shops.....">
                    <button class="btn btn-default col-md-1" type="submit" style="border-radius:0px 20px 20px 0px;">
                        <i class="fas fa-search"></i>
                    </button>
                </div>
            </form>
        </div>
        <div class="col-md-4">

            <?php if(auth()->guard()->guest()): ?>

            <button type="button" data-toggle="modal" data-target="#myModal1" class="btn btn-danger"
                style="color: #fff !important;"><i class="fas fa-user" style="color: #fff !important;"> </i>
                Login/Signup</button>

            <?php else: ?>

            <ul class="nav userdetails">
                <li class="nav-item">
                    <a href="<?php echo e(url("wingpoints")); ?>" class="nav-link text-center">
                        <img src="<?php echo e(asset('main/images/wing.png')); ?>" class="img-fluid" style="height:30px" /><br>
                        <span>Wing Coins</span>
                    </a>
                </li>
                <li class="nav-item">
                    <a href="<?php echo e(url("orders")); ?>" class="nav-link text-center">
                        <img src="<?php echo e(asset('main/images/cart_new.png')); ?>" class="img-fluid" style="height:30px" /><br>
                        <span>My orders</span>
                    </a>
                </li>
                <li class="nav-item dropdown">
                    <a class="nav-link dropdown-toggle text-center" href="#" id="navbardrop"
                        data-toggle="dropdown">
                        <img src="<?php echo e(asset('main/images/user.png')); ?>" class="img-fluid" style="height:30px" /><br>
                        <?php echo e(Auth::user()->first_name." ".Auth::user()->last_name); ?>

                    </a>
                    <div class="dropdown-menu">
                        <a class="dropdown-item" href="<?php echo e(url("userprofile")); ?>">Profile</a>
                        <a class="dropdown-item" href="<?php echo e(url("favouriteshops")); ?>">Favourite Shops</a>
                        <a class="dropdown-item" href="<?php echo e(url('/referafriend')); ?>">Refer a Friend</a>
                        <a class="dropdown-item" href="#">Notifications<sup> <?php echo e(count($notifications)); ?> </sup></a>
                        <a class="dropdown-item" href="<?php echo e(route('logout')); ?>" onclick="event.preventDefault();
                        document.getElementById('logout-form').submit();">Logout</a>
                        <form id="logout-form" action="<?php echo e(route('logout')); ?>" method="POST" style="display: none;">
                            <?php echo csrf_field(); ?>
                        </form>
                    </div>
                </li>
            </ul>
            <?php endif; ?>
        </div>
    </div>
</nav>
<?php /**PATH C:\xampp\htdocs\offerswing\resources\views/layouts/appheader.blade.php ENDPATH**/ ?>