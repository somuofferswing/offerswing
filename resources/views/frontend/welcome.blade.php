@extends('layouts.app')



@section('content')

<style>
    .imgrow2 {

        width: 437;

        height: 234px
    }

    .imgrow3 {

        width: 400;

        height: 240px
    }
</style>

<div id="homepage-2">

    <div class="container">



        <!-- main slider -->

        <div class="row mb-5">

            <div class="col-12 col-md-9 flex-grow-1">

                @component('components.carousel',['slider'=> $slider_1])

                @slot('coursel_id')

                coursel

                @endslot

                @endcomponent

            </div>

            <div class="col-md-3 flex-grow-1">

                @component('components.carousel',['slider'=> $slider_2])

                @slot('coursel_id')

                coursel1

                @endslot

                @endcomponent

            </div>

        </div>



        <!-- deals block -->

        <div class="ps-site-features bg-white mb-5">

            <div class="ps-block--site-features ps-block--site-features-2">

                <div class="ps-block__item justify-content-center">

                    <a href="{{ url('/wingdeals') }}">

                        <img src="{{ asset('main/images/icons/deals.png') }}" alt="" class="img-fluid"
                            style="height: 60px;">

                    </a>

                </div>

                <div class="ps-block__item justify-content-center">

                    <a href="{{ url('/event') }}">

                        <img src="{{ asset('main/images/icons/events.png') }}" alt="" class="img-fluid"
                            style="height: 50px;">

                    </a>

                </div>

                <div class="ps-block__item justify-content-center">

                    <a href="{{ url('/newsfeeds') }}">

                        <img src="{{ asset('main/images/icons/news.png') }}" alt="" class="img-fluid"
                            style="height: 50px;">

                    </a>

                </div>

                <div class="ps-block__item justify-content-center">

                    <div class="ps-block__left m-0">

                        <i class="icon-gift"></i>

                        <!-- <img src="{{ asset('main/images/icons/wing.png') }}" alt="" class="img-fluid"

                            style="height: 50px;"> -->

                    </div>

                    <div class="ps-block__right">

                        <a href="{{ url('/redeem') }}">

                            <strong style="font-size:18px;font-weight:500">Redeem Gifts</strong>

                        </a>

                    </div>

                </div>



                <div class="ps-block__item">

                    <div class="ps-block__left m-0"><i class="fa fa-download"></i></div>

                    <div class="ps-block__right">

                        <a href="https://play.google.com/store?hl=en" target="_blank">

                            <strong style="font-size:18px;font-weight:500">Download APP</strong>

                        </a>

                    </div>

                </div>

            </div>

        </div>

    </div>



    <div class="ps-home-banner">

        <div class="container syd-container">

            <div class="ps-section__left">

                @include('layouts.mainSidebar')

            </div>

            <div class="ps-section__right" style="width: 930px;">



                <div class="row">

                    @foreach ($imagediv2_1 as $item)

                    <div class="col-12 col-md-6">

                        <a class="ps-collection" href="#">

                            <img class="imgrow2" src="{{ asset('storage/'.$item->image) }}" alt="">

                        </a>

                    </div>

                    @endforeach

                </div>



                <div class="row mt-5">

                    @foreach ($imagediv3_1 as $item)

                    <div class="col-12 col-md-4">

                        <a class="ps-collection" href="#">

                            <img class="imgrow3" src="{{ asset('storage/'.$item->image) }}" alt="">

                        </a>

                    </div>

                    @endforeach

                </div>

                <div class="row mt-5 carousel_div2">
                    <div class="ps-carousel--nav-inside owl-slider" data-owl-auto="true" data-owl-loop="true" data-owl-speed="5000" data-owl-gap="0" data-owl-nav="false" data-owl-dots="true" data-owl-item="1" data-owl-item-xs="1" data-owl-item-sm="1" data-owl-item-md="1" data-owl-duration="1000" data-owl-mousedrag="on">
                    @foreach ($imagediv2_2 as $item)

                    <div class="col-12">

                        <a class="ps-collection" href="#">

                            <img class="imgrow2" src="{{ asset('storage/'.$item->image) }}" alt="">

                        </a>

                    </div>

                    @endforeach
                    </div>
                </div>

                {{--
                <div class="row mt-5">
                    <div class="ps-carousel--nav-inside owl-slider" data-owl-auto="true" data-owl-loop="true" data-owl-speed="5000" data-owl-gap="0" data-owl-nav="true" data-owl-dots="true" data-owl-item="1" data-owl-item-xs="1" data-owl-item-sm="1" data-owl-item-md="1" data-owl-duration="1000" data-owl-mousedrag="on">
                        @foreach ($imagediv2_2 as $item)
                            <a href="#">
                                <img class="imgrow2" src="{{ asset('storage/'.$item->image) }}" alt="">
                            </a>
                        @endforeach
                    </div>
                </div>
                --}}
                
                <div class="row mt-5">
                    <div class="ps-carousel--nav-inside owl-slider" data-owl-auto="true" data-owl-loop="true" data-owl-speed="5000" data-owl-gap="10" data-owl-nav="false" data-owl-dots="true" data-owl-item="2" data-owl-item-xs="1" data-owl-item-sm="1" data-owl-item-md="2" data-owl-duration="1000" data-owl-mousedrag="on">
                    @foreach ($imagediv3_2 as $item)

                    <div class="col-12">

                        <a class="ps-collection" href="#">

                            <img class="imgrow3" src="{{ asset('storage/'.$item->image) }}" alt="">

                        </a>

                    </div>

                    @endforeach
                    </div>
                </div>
                
                {{-- 
                <div class="row mt-5">
                    <div class="ps-carousel--nav-inside owl-slider" data-owl-auto="true" data-owl-loop="true" data-owl-speed="5000" data-owl-gap="10" data-owl-nav="true" data-owl-dots="true" data-owl-item="2" data-owl-item-xs="1" data-owl-item-sm="1" data-owl-item-md="2" data-owl-duration="1000" data-owl-mousedrag="on">
                        @foreach ($imagediv3_2 as $item)
                            <a  href="#">
                                <img class="imgrow3 rounded shadow" src="{{ asset('storage/'.$item->image) }}" alt="">
                            </a>
                        @endforeach
                    </div>
                </div>
                --}}

                <div class="row mt-5">

                    @foreach ($imagediv2_3 as $item)

                    <div class="col-12 col-md-6">

                        <a class="ps-collection" href="#">

                            <img class="imgrow2" src="{{ asset('storage/'.$item->image) }}" alt="">

                        </a>

                    </div>

                    @endforeach

                </div>



                <div class="row mt-5">

                    @foreach ($imagediv3_3 as $item)

                    <div class="col-12 col-md-4">

                        <a class="ps-collection" href="#">

                            <img class="imgrow3" src="{{ asset('storage/'.$item->image) }}" alt="">

                        </a>

                    </div>

                    @endforeach

                </div>



                <div class="row mt-5">
                    <div class="ps-carousel--nav-inside owl-slider" data-owl-auto="true" data-owl-loop="true" data-owl-speed="5000" data-owl-gap="10" data-owl-nav="false" data-owl-dots="true" data-owl-item="2" data-owl-item-xs="1" data-owl-item-sm="1" data-owl-item-md="2" data-owl-item-lg="2" data-owl-item-xl="2" data-owl-duration="1000" data-owl-mousedrag="on">
                    @foreach ($imagediv2_4 as $item)

                    <div class="col-12">

                        <a class="ps-collection" href="#">

                            <img class="imgrow2" src="{{ asset('storage/'.$item->image) }}" alt="">

                        </a>

                    </div>

                    @endforeach
                    </div>
                </div>



                <!-- <div class="row mt-5">

                @foreach ($imagediv2_5 as $item)

                <div class="col-12 col-md-6">

                    <a class="ps-collection" href="#">

                        <img class="imgrow2" src="{{ asset('storage/'.$item->image) }}" alt="">

                    </a>

                </div>

                @endforeach

            </div>



            <div class="row mt-5">

                @foreach ($imagediv3_3 as $item)

                <div class="col-12 col-md-4">

                    <a class="ps-collection" href="#">

                        <img class="imgrow3" src="{{ asset('storage/'.$item->image) }}" alt="">

                    </a>

                </div>

                @endforeach

            </div> -->



            </div>

        </div>

    </div>

</div>

@endsection

{{--
@section('js')
    <script src="{{ asset('frontend/js/welcome.js') }}"></script>
@endsection
--}}