<?php

namespace App\Http\Controllers\Backend;

use App\Http\Controllers\Controller;
use App\Models\Claimed;
use App\Models\Gift;
use App\Models\Order;
use App\Models\WingPoints;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Storage;

class GiftsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $gifts = Gift::all();

        return view('backend.gift.index', compact('gifts'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('backend.gift.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $gift = new Gift();

        $this->validate($request, [
            'gift_name' => 'required',
            'description' => 'required',
            'image' => 'required|image|mimes:jpeg,png,jpg,gif,svg',
            'points_to_claim' => 'required',
            'quantity' => 'required',
            'claims_per_user' => 'required',
            'start_date' => 'required',
            'end_date' => 'required',
        ]);

        $gift->gift_name = $request['gift_name'];
        $gift->description = $request['description'];
        $gift->points_to_claim = $request['points_to_claim'];
        $gift->quantity = $request['quantity'];
        $gift->claims_per_user = $request['claims_per_user'];
        $gift->start_date = $request['start_date'];
        $gift->end_date = $request['end_date'];

        $slug = $this->slugify($request['gift_name']);
        $gift->slug = $slug;


        if ($request->hasFile('image')) {
            $file = $request->file('image');
            $fileName = $slug.'_'.time().'.'.$file->getClientOriginalExtension();
            $path = $file->storeAs('gifts', $fileName, 'public');
            $gift->image = $path;
        }

        $gift->save();

        return redirect('/gifts')->with('success', 'question has Created Successfully');
    }

    /**
     * Display the specified resource.
     *
     * @param int $id
     *
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param int $id
     *
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $gift = Gift::find($id);

        return view('backend.gift.edit', compact('gift'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param int $id
     *
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $gift = Gift::find($id);
        $this->validate($request, [
            'gift_name' => 'required',
            'description' => 'required',
            'image' => 'image|mimes:jpeg,png,jpg,gif,svg|max:2048',
            'points_to_claim' => 'required',
            'quantity' => 'required',
            'claims_per_user' => 'required',
            'start_date' => 'required',
            'end_date' => 'required',
        ]);

        $gift->gift_name = $request['gift_name'];
        $gift->description = $request['description'];
        $gift->points_to_claim = $request['points_to_claim'];
        $gift->quantity = $request['quantity'];
        $gift->claims_per_user = $request['claims_per_user'];
        $gift->start_date = $request['start_date'];
        $gift->end_date = $request['end_date'];

        if ($gift->isDirty('gift_name')) {
            $slug = $this->slugify($request['gift_name']);
            
            $gift->slug = $slug;
        } else {
            $slug = $gift->slug;
        }

        if ($request->hasFile('image')) {
            Storage::disk('public')->delete($gift->image);

            $file = $request->file('image');
            $fileName = $slug.'_'.time().'.'.$file->getClientOriginalExtension();
            $path = $file->storeAs('deals', $fileName, 'public');
            $gift->image = $path;
        }

        $gift->save();

        return redirect('/gifts')->with('success', 'Gift has updated Successfully');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param int $id
     *
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $gift = Gift::find($id);
        $gift->delete();

        return redirect('/gifts')->with('success', 'Deal has been deleted Successfully');
    }

    public function slugify($str)
    {
        $search = ['Ș', 'Ț', 'ş', 'ţ', 'Ş', 'Ţ', 'ș', 'ț', 'î', 'â', 'ă', 'Î', 'Â', 'Ă', 'ë', 'Ë'];
        $replace = ['s', 't', 's', 't', 's', 't', 's', 't', 'i', 'a', 'a', 'i', 'a', 'a', 'e', 'E'];
        $str = str_ireplace($search, $replace, strtolower(trim($str)));
        $str = preg_replace('/[^\w\d\-\ ]/', '', $str);
        $str = str_replace(' ', '-', $str);

        return $this->getUniqueGiftSlug(preg_replace('/\-{2,}/', '-', $str));
    }

    private function getUniqueGiftSlug($slug)
    {
        $existingCount = Gift::where('slug', 'like', $slug . '-%')->count();

        if ($existingCount) {
            return $slug . '-' . ($existingCount);
        }

        return $slug;
    }
}
