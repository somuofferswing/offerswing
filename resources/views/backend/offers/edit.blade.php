@extends('layouts.admin')

@section('content')
    <link rel="stylesheet" href="{{ asset('adminlte/bower_components/bootstrap-datepicker/dist/css/bootstrap-datepicker.min.css') }}">
    <div class="content-wrapper">
        <section class="content-header">
            <h1>
                Offers
                <small>Edit offer</small>
            </h1>
            <!-- <ol class="breadcrumb">
              <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
              <li class="active">Dashboard</li>
            </ol> -->
        </section>

        <section class="content">

                <div class="box box-info">
                    <div class="box-header with-border">
                        <h3 class="box-title">Edit offer</h3>
                    </div>
                    <form class="form-horizontal" method="POST" action="{{ route('update-offer',[$offers->id]) }}" enctype="multipart/form-data">
                        @csrf
                        {{-- <input type="hidden" value="{{$user_id}}" name="created_by" > --}}
                        <div class="box-body">
                                <div class="form-group ">
                                    <label for="name" class="col-sm-2 control-label">Name</label>
                                    <div class="col-sm-10">
                                        <input id="name" type="text" class="form-control" name="name"
                                               value="{{ $offers->offer_name }}" required autocomplete="name"
                                               autofocus>
                                        @if ($errors->has('name'))
                                            <div class="danger">{{ $errors->first('name') }}</div>
                                        @endif
                                    </div>
                                </div>
                                <div class="form-group">
                                        <label for="offer_image" class="col-sm-2 control-label">Image</label>
                                        <div class="col-sm-10">
                                        <input type="file" class="form-control-file" id="offer_image" name="offer_image" onchange="loadFile(event)">
                                        {{-- <img  height="100px" width="100px"/> --}}
                                        <img class="img-thumbnail" src='<?php echo asset("$offers->offer_image"); ?>' height="100px" width="100px" id="output">
                                        </div>
                                    </div>
                                <div class="form-group ">
                                    <label for="description" class="col-sm-2 control-label">Description</label>
                                    <div class="col-sm-10">
                                        <textarea id="description" type="text" class="form-control" name="description"
                                              required autocomplete="description"
                                               autofocus>{{ $offers->description }}</textarea>
                                        @if ($errors->has('description'))
                                            <div class="danger">{{ $errors->first('description') }}</div>
                                        @endif
                                    </div>
                                </div>

                            <div class="form-group ">
                                <label for="shop_id" class="col-sm-2 control-label">Shop</label>
                                <div class="col-sm-10">

                                    <select class="form-control" id="shop_id" name="shop_id" value="{{ old('shop_id') }}" required autocomplete="shop_id"
                                            autofocus>
                                        <option>--Select--</option>
                                        @foreach($shops as $shop)
                                            <option value="{{ $shop->id }}" @if ($offers->shop_id == $shop->id )
                                                selected
                                            @endif>{{ $shop->name }}</option>
                                        @endforeach
                                    </select>
                                    @if ($errors->has('shop_id'))
                                        <div class="danger">{{ $errors->first('shop_id') }}</div>
                                    @endif
                                </div>
                            </div>
                            {{-- <div class="form-group ">
                                <label for="category_id" class="col-sm-2 control-label">Category</label>
                                <div class="col-sm-10">

                                    <select class="form-control" id="category_id" name="category_id" value="{{ old('category_id') }}" required autocomplete="category_id"
                                            autofocus>
                                        <option>--Select--</option>
                                        @foreach($category as $key =>$cat)
                                             <option value="{{ $key }}" @if ($offers->category_id == $key)
                                                    selected
                                                @endif>{{ $cat }}</option>
                                        @endforeach
                                    </select>
                                    @if ($errors->has('category_id'))
                                        <div class="danger">{{ $errors->first('category_id') }}</div>
                                    @endif
                                </div>
                            </div> --}}
                            {{-- <div class="form-group ">
                                <label for="times_applicable" class="col-sm-2 control-label">Days Applicable</label>
                                <div class="col-sm-10">
                                    <select name="days_applicable[]" id="days_applicable" class="form-control select2" data-placeholder="Select days applicable" multiple="multiple">
                                        @foreach ($days as $item)
                                    <option value="{{$item->id}}" @if (in_array($item->id, explode(',',$offers->days_applicable)))
                                            selected
                                        @endif>{{$item->name}}</option>
                                        @endforeach
                                    </select>
                                    @if ($errors->has('days_applicable'))
                                        <div class="danger">{{ $errors->first('days_applicable') }}</div>
                                    @endif
                                </div>
                            </div> --}}
                            <div class="form-group ">
                                <label for="from_date" class="col-sm-2 control-label">From Date</label>
                                <div class="col-sm-10">
                                    <input type="text" class="form-control pull-right" id="from_date" name="from_date" value="{{ $offers->from_date }}" required autocomplete="from_date" autofocus>
                                    @if ($errors->has('from_date'))
                                        <div class="danger">{{ $errors->first('from_date') }}</div>
                                    @endif
                                </div>
                            </div>
                            <div class="form-group ">
                                <label for="to_date" class="col-sm-2 control-label">To Date</label>
                                <div class="col-sm-10">
                                    <input type="text" class="form-control pull-right" id="to_date" name="to_date" value="{{ $offers->to_date }}" required autocomplete="to_date" autofocus>

                                    @if ($errors->has('to_date'))
                                        <div class="danger">{{ $errors->first('to_date') }}</div>
                                    @endif
                                </div>
                            </div>

                            <div class="form-group ">
                                    <label for="max_wing_points_assured" class="col-sm-2 control-label">Top Offer</label>
                                    <div class="col-sm-10">
                                        <input type="checkbox" class=""
                                        @if ($offers->top == 1)
                                           checked
                                        @endif
                                        value="1"
                                        name="top" >
                                        @if ($errors->has('top'))
                                        <div class="danger">{{ $errors->first('top') }}</div>
                                        @endif
                                    </div>
                                </div>

                            {{-- <div class="form-group ">
                                    <label for="terms_conditions" class="col-sm-2 control-label">Terms & Conditions</label>
                                    <div class="col-sm-10">
                                        <textarea id="terms_conditions" type="text" class="form-control" name="terms_conditions"

                                        rows="10" cols="80" >{{ $offers->terms_conditions }}</textarea>
                                        @if ($errors->has('terms_conditions'))
                                            <div class="danger">{{ $errors->first('terms_conditions') }}</div>
                                        @endif
                                    </div>
                                </div> --}}
                                <div class="pull-right">
                                    <button type="submit" class="btn btn-info">{{ __('Update') }}</button>
                                        <button type="button" class="btn btn-danger" onclick="window.history.go(-1); return false;">{{ __('Cancel') }}</button>
                                </div>

                        </div>
                    </form>
                </div>
        </section>
    </div>
@endsection
@section('scripts')
    <script src="{{ asset('adminlte/bower_components/bootstrap-datepicker/dist/js/bootstrap-datepicker.min.js') }}"></script>
    <script src="{{ asset('adminlte/bower_components/select2/dist/js/select2.full.min.js') }}"></script>
    <script src="{{ asset('adminlte/bower_components/ckeditor/ckeditor.js') }}"></script>
    <script>
        $(function () {
            // CKEDITOR.replace('terms_conditions')
            CKEDITOR.replace('description')
        });
        $('.select2').select2()
        $(document).ready(function() {
            $('#from_date').datepicker({
                autoclose: true,
                format: 'yyyy-mm-dd',
                startDate: new Date(),
            });

            $('#to_date').datepicker({
                autoclose: true,
                format: 'yyyy-mm-dd',
                startDate: new Date(),
            }).change(function () {
                var from = $('#from_date').val();
                var to = $('#to_date').val();
                if(to < from){
                    $('#to_date').val("");
                    alert('to date should be grater than from date');
                }
            });
        })

    </script>
    <script>
    var loadFile = function(event) {
            var output = document.getElementById('output');
            output.src = URL.createObjectURL(event.target.files[0]);
          };
</script>
@endsection
