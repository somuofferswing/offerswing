@extends('layouts.app')



@section('content')

<style>

@media (min-width: 992px){

    .ps-product--detail .ps-product__header .ps-product__thumbnail{

        max-width:25%;

    }

}



.callBtn{

   background-color:#ffc107;

   color:#000;

}

</style>

<div class="ps-breadcrumb">

    <div class="container">

        <ul class="breadcrumb text-capitalize" id="breadcrumb">

            <li><a href="{{ url('/') }}">Home</a></li>

            <li><a href="{{ url('/event') }}">Events</a></li>

            <li>{{ $event->title }}</li>

        </ul>

    </div>

</div>

<section >

    <div class="ps-page--product">

        <div class="container">



            <div class="ps-product--detail ps-product--full-content">

                <div class="col-12">

                    <div class="ps-product__header">

                        <div class="ps-product__thumbnail" data-vertical="false">

                            <img src="{{ asset('storage/'.$event->event_image) }}" alt="" height="250px">

                        </div>

                        <div class="ps-product__info">

                            <h1>{{ ucfirst($event->title) }}

                            <div class="ps-product__meta"></div>

                            <div class="ps-shop__info">

                                <p><i class="fa fa-calendar pr-2"></i>{{ $event->event_date}}  <i class="fa fa-clock-o pr-2"></i>{{ $event->event_time}}</p>

                                @if ($event->payment_type == 'wingpoints' )
                                <p class="text-danger"><b> {{ $event->points_to_claim }}  wingpoints for each</b></p>
                                @endif

                                @if ($event->payment_type == 'cash' )
                                    
                                <p class="text-danger"><b>&#x20b9 {{ $event->event_price }} for each</b></p>
                                @endif

                            </div>

                      

                            <div class="ps-product__sharing pt-0">

                                <p>Share</p>

                                <a class="facebook" href="#"><i class="fa fa-facebook"></i></a>

                                <a class="twitter" href="#"><i class="fa fa-twitter"></i></a>

                                <a class="instagram bg-danger" href="#"><i class="fa fa-instagram"></i></a>

                                <a class="bg-success" href="#"><i class="fa fa-whatsapp"></i></a>

                                <a class="bg-info" href="#"><i class="fa fa-share-alt"></i></a>

                            </div>

                            

                            <div class="pt-5">

                                @if (Auth::guest())

                                <button class="ps-btn btn-sm callBtn mr-3">Call Us</a>

                                @else

                                <a href="{{ url('getevent',[$event->slug])}}" class="ps-btn btn-sm callBtn mr-3"> Call Us</a>

                                @endif

    

                                @if (Auth::guest())

                                <button class="ps-btn btn-sm" data-toggle="modal" data-target="#myModal1">Book

                                    Now</button>

                                @else

                                <a href="{{ url('getevent',[$event->slug])}}" class="ps-btn btn-sm"> Book Now</a>

                                @endif

            

                            </div>

                        </div>

                    </div>



                

                </div>



                <div class="ps-product__content ps-tab-root pt-5">

                    <ul class="ps-tab-list">

                        <li class="active"><a href="#tab-aboutevent">About Event</a></li>

                        <li><a href="#tab-price">Price & Timings</a></li>

                        <li><a href="#tab-venue">Venue</a></li>

                        <li><a href="#tab-tc">Terms & Conditions</a></li>

                    </ul>

                    <div class="ps-tabs">



                        <!-- about event tab -->

                        <div class="ps-tab active" id="tab-aboutevent">

                            <div class="ps-document">

                                <p>{!! $event->description !!}</p>

                            </div>

                        </div>

                        <!-- end of about event tab -->

                        

                        <!-- price & timing tab -->

                        <div class="ps-tab" id="tab-price">

                            <p><i class="fa fa-calendar pr-2"></i>{{ date('M d, Y', strtotime($event->event_date)) }} </p>

                            <p><i class="fa fa-clock-o pr-2"></i>{{ $event->event_time}}</p>

                            @if ($event->payment_type == 'wingpoints' )
                            <p class="text-danger"><b> {{ $event->points_to_claim }}  wingpoints for each</b></p>
                            @endif

                            @if ($event->payment_type == 'cash' )
                                
                            <p class="text-danger"><b>&#x20b9 {{ $event->event_price }} for each</b></p>
                            @endif

                            {{-- <p class="text-danger"><b>&#x20b9 {{ $event->event_price }} Onwards</b></p> --}}

                        </div>

                        <!-- end of price & timing tab -->



                        <!-- venue tab -->

                        <div class="ps-tab" id="tab-venue">

                            <p>{{ $event->event_address }}</p>

                            <div id="map" style="width:100%;height:450px"></div>

                        </div>

                        <!-- end of venue tab -->



                        <!-- terms & conditions tab -->

                        <div class="ps-tab" id="tab-tc">

                            <p>{!! $event->terms_conditions !!}</p>

                        </div>

                        <!-- end of terms & conditions tab -->

                    </div>



                </div>

            </div>



        </div>

    </div>

</section>



<!-- <a class="btn btn-primary" >Trigger modal</a> -->

<div class="modal fade" id="share">

    <div class="modal-dialog">

        <div class="modal-content">

            <div class="modal-body">

                <div class="a2a_kit a2a_kit_size_32 a2a_default_style">

                    {{-- <a class="a2a_dd" href="https://www.addtoany.com/share"></a> --}}

                    <a class="a2a_button_facebook"></a>

                    <a class="a2a_button_twitter"></a>

                    <a class="a2a_button_email"></a>

                    <a class="a2a_button_whatsapp"></a>

                </div>

                <script async src="https://static.addtoany.com/menu/page.js"></script>

            </div>

        </div>

    </div>

</div>



<script>

    $(document).on('click', ".unlike", function () {

            var id = $(this).data('id');

            var type = "event";

            var element = this;



            var data = {

                    id:id,

                    type:type

                };

            $.ajax({

                    type: "post",

                    url: '{{ url("removelike") }}',

                    headers: {

                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')

                     },

                    data: data,

                    cache: false,

                    success: function (data)

                    {

                        if(data.status == "success"){

                            $(element).removeClass('unlike').addClass('like').html('<i class="far fa-heart" style="color:#e31b23"></i> &nbsp;like');

                        }

                    },

                    error: function (data){

                        alert('Fail to run unlike..');

                    }

                });





        });

        $(document).on('click', ".like", function () {

            var id = $(this).data('id');

            var type = "event";

            var element = this;



            var data = {

                    id:id,

                    type:type

                };

            $.ajax({

                    type: "post",

                    url: '{{ url("addlike") }}',

                    headers: {

                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')

                     },

                    data: data,

                    cache: false,

                    success: function (data)

                    {

                        console.log(data);

                        if(data.status == "success"){

                                $(element).removeClass('like').addClass('unlike').html('<i class="fas fa-heart" style="color:#e31b23"></i> &nbsp;Unlike');



                        }

                    },

                    error: function (data){

                        alert('Fail to run like..');

                    }

                });





        });



        function initMap() {

            var lat =  {{ $event->event_lat }};

           var long =  {{ $event->event_lng }};

            var myLatLng = {lat: lat, lng: long};



            var map = new google.maps.Map(document.getElementById('map'), {

                zoom: 14,

                center: myLatLng

            });



            var marker = new google.maps.Marker({

                position: myLatLng,

                map: map,

                title: 'Hello World!'

            });

        }

</script>

<script

    src="http://maps.googleapis.com/maps/api/js?key=AIzaSyBHLFZ4C3XB7MDwjywzpBC1ZWMH0u2zuE4&libraries=places&callback=initMap"

    async defer>

</script>



@endsection

