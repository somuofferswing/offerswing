@extends('layouts.admin')



@section('content')

<div class="content-wrapper">

    <section class="content-header">

        <h1>

            Amenity

            <small>All Amenities</small>

        </h1>

        <!-- <ol class="breadcrumb">

              <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>

              <li class="active">Dashboard</li>

            </ol> -->

    </section>

    <section class="content">

        <div class="box box-primary">

            <div class="box-header with-border">

                <h3 class="box-title">All Categories</h3>

                <a href="{!! url('/amenity/create'); !!}" class="btn btn-primary pull-right">Add Amenity</a>

            </div>

            <div class="table-responsive">

                <table class="table table-striped">

                    <thead>

                        <tr>

                            <th>S.No</th>

                            <th>Name</th>

                            <th>Image</th>

                            {{--                            <th>Description</th>--}}

                            <th colspan="2">Action</th>

                        </tr>

                    </thead>

                    <tbody>

                        @foreach($amenities as $key=>$amenity)

                        <tr>

                            <td>{{$key+1}}</td>

                            <td>{{$amenity->name}}</td>

                            <td><img class="img-thumbnail" src='{{ asset('storage/'.$amenity->image) }}' height="75px"
                                    width="75px"></td>

                            {{--                                <td>{!! $amenity->description !!}</td>--}}



                            {{-- <td><a class="btn btn-primary" href="{{ route('view-category', [$category->id]) }}">View</a>
                            </td> --}}

                            <td><a class="btn btn-default" href="{{ route('edit-amenity', [$amenity->id]) }}">Edit</a>
                            </td>



                            <td><a class="btn btn-danger del" data-sub="false" data-id="{{$amenity->id}}">Delete</a>
                            </td>





                        </tr>

                        @endforeach

                    </tbody>

                </table>

            </div>

        </div>



    </section>

</div>

@endsection

@section('scripts')

<script>
    $('.del').on('click',function(){

    var bool = $(this).data('sub');
    var id = $(this).data('id');
    if(!bool){
        $.ajax({
            type: "POST",
            url: "{{  url('amenity/delete/') }}"+"/"+id,
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            },
            success: function (response) {
                alert(response);
                window.location.reload();
            }
        });

    }
});

</script>



@endsection