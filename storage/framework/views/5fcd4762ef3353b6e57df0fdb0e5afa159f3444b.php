<?php $__env->startSection('content'); ?>
<style>
.highlighter{
    border-bottom:2px solid #df685d;
    width:10%;
    margin:0px auto;
}

.ps-page--blog .ps-page__header {
    padding: 30px 0px;
}
.ps-product .ps-product__thumbnail{
    height:250px;
}
.ps-product .ps-product__thumbnail img{
    height:100%;
}
</style>

<div class="ps-breadcrumb">
    <div class="ps-container">
        <ul class="breadcrumb text-capitalize" id="breadcrumb">
            <li><a href="<?php echo e(url('/')); ?>">Home</a></li>
            <li>Wing Deals</li>
        </ul>
    </div>
</div>

<section>
    <div class="ps-page--blog">
        <div class="container">
            <div class="ps-page__header">
                <h1>Wing Deals</h1>
                <hr class="highlighter">
            </div>
            <div class="row mb-40">
                <?php $__currentLoopData = $deals; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $key=>$item): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                    <div class="col-xl-4 col-lg-4 col-md-6 col-sm-12 col-12 ">
                        
                        <div class="ps-product">
                            <div class="ps-product__thumbnail">
                                <?php if(!empty($item->pic)): ?>
                                    <img src="<?php echo asset("$item->pic"); ?>" alt="">
                                <?php endif; ?>
                                <div class="ps-product__badge"><?php echo e($item->discount_percentage); ?>% off*</div>
                            </div>
                            <div class="ps-product__container">
                                <div class="ps-product__content">
                                    <div class="ps-product__vendor text-capitalize">
                                        <a href="#" class="dealdetails" onClick="return false;" data-id="<?php echo e($item->id); ?>">ReadMore</a>
                                        <a href="<?php echo url('/viewshop',[$item->shop_id]);; ?>" class="pull-right">ClaimNow</a>
                                    </div>
                                    <a class="ps-product__title" href="<?php echo url('/viewshop',[$item->shop_id]);; ?>"><?php echo e(ucfirst($item->name)); ?></a>
                                    <p>Valid Upto : <?php echo e(date('M d, Y', strtotime($item->valid_upto))); ?></p>
                                </div>
                                <div class="ps-product__content hover">
                                    <div class="ps-product__vendor text-capitalize">
                                        <a href="#" class="dealdetails" onClick="return false;" data-id="<?php echo e($item->id); ?>">ReadMore</a>
                                        <a href="<?php echo url('/viewshop',[$item->shop_id]);; ?>" class="pull-right">ClaimNow</a>
                                    </div>
                                    <a class="ps-product__title" href="<?php echo url('/viewshop',[$item->shop_id]);; ?>"><?php echo e(ucfirst($item->name)); ?></a>
                                    <p>Valid Upto : <?php echo e(date('M d, Y', strtotime($item->valid_upto))); ?></p>
                                </div>
                            </div>
                        </div>
                    </div>
                <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
            </div>
        </div>
    </div>
</div>
</section>

<!-- Modal -->
<div class="modal fade" id="dealdetails" tabindex="-1" role="dialog" aria-labelledby="modelTitleId" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title">Modal title</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <i class="fa fa-remove"></i>
                    </button>
            </div>
            <div class="modal-body">
                Body
            </div>
            <div class="modal-footer">
                <button type="button" class="ps-btn" data-dismiss="modal">Close</button>
            </div>
        </div>
    </div>
</div>


<script>
    $('.dealdetails').click(function () {
          let id =  $(this).data('id');

        $.ajax({
          type: "GET",
          url: "<?php echo e(url('/dealdetails/')); ?>"+"/"+id,

          success: function (response) {

            var json_obj = $.parseJSON(response);
            console.log(json_obj);
            var mymodal = $('#dealdetails');
            mymodal.find('.modal-title').html(`
             <div>
                <h3>`+ json_obj[0]['name'].toUpperCase()+`</h3>

             </div>
             `);

             mymodal.find('.modal-body').html(`
             <div>
                <h4><u>Description:</u></h4>
                <p>`+json_obj[0]['description'] +`</p>
                <hr>
                <h4><u>Active days:</u></h4>
                <p>`+json_obj[0]['days_applicable'].join()+`</p>

                <h4><u>Valid for:</u></h4>
                <p>`+'one Person only'+`</p>

                <h4><u>Valid Dates:</u></h4>
                <p>`+json_obj[0]['valid_from']+' To '+json_obj[0]['valid_from']+`</p>

                <h4><u>Terms & Conditions:</u></h4>
                <p>`+json_obj[0]['terms_conditions'] +`</p>



             </div>
             `);
            mymodal.modal('show');
          }
      });

      })
</script>

<?php $__env->stopSection(); ?>

<?php echo $__env->make('layouts.app', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH C:\xampp\htdocs\offerswing\resources\views/wingdeals.blade.php ENDPATH**/ ?>