@extends('layouts.app')

@section('content')
<style>
    #map {
        width: 100%;
        height: 350px;
    }

    /* */
    .ps-shop__info p {
        font-size:1.6rem;
        color:#000;
    }
</style>

@if (session('alert'))
    <div class="alert alert-{{ session('class') }}">
        {{ session('alert') }}
    </div>
@endif
{{-- @php
    $category = \App\Models\Category::find($category_id);
    $parent_category = $category->parentCategories;
@endphp --}}

<div class="ps-breadcrumb">
    <div class="container">
        <ul class="breadcrumb text-capitalize" id="breadcrumb">
            <li><a href="{{ url('/') }}">Home</a></li>
            <li>{{$shops->name}}</li>
        </ul>
    </div>
</div>

<section >
    <div class="ps-page--product">
        <div class="container">

            <div class="ps-page--product">
                <div class="container">
                    <div class="ps-page__container">

                        <div class="ps-page__left">
                            <div class="ps-product--detail">

                                <div class="ps-product__header">
                                    <div class="ps-product__thumbnail" data-vertical="false">
                                        <figure>
                                            <div class="ps-wrapper">
                                                <div class="ps-product__gallery" data-arrow="true">

                                                @php
                                                    $images = $shops->shop_images;
                                                @endphp
                                                @if(!empty($images))
                                                    @foreach($images as $image)
                                                        <div class="item">
                                                            <a href="<?php echo asset("$image->image_path"); ?>">
                                                                <img src="{{ asset('storage/'.$image->image_path) }}" alt="">
                                                            </a>
                                                        </div>
                                                    @endforeach
                                                @endif

                                                </div>
                                            </div>
                                        </figure>
                                        @if(!empty($images))
                                        <div class="ps-product__variants" data-item="4" data-md="3" data-sm="3" data-arrow="false">
                                            @foreach($images as $image)
                                                <div class="item">
                                                    <img src="{{ asset('storage/'.$image->image_path) }}" alt="">
                                                </div>
                                            @endforeach
                                        </div>
                                        @endif
                                    </div>

                                    <div class="ps-product__info">
                                        <h1>{{ ucfirst($shops->name) }}
                                            @if(Auth::guest())
                                                <a href="#" class="badge zoom ml-2" data-id="{{ $shops->id }}" data-toggle="modal" data-target="#myModal1">
                                                    <span data-toggle="tooltip" data-placement="right" title="" data-original-title="Like">
                                                        <i class="fa fa-heart-o fa-lg text-danger"></i>
                                                    </span>
                                                </a>
                                            @else
                                                @if (!empty($likes))
                                                    <a href="#" class="badge zoom ml-2 unlike" data-id="{{ $shops->id }}" data-toggle="tooltip" data-placement="right" title="" data-original-title="Unlike">
                                                        <i class="fa fa-heart fa-lg text-danger"></i>
                                                    </a>
                                                @else
                                                    <a href="#" class="badge zoom ml-2 like" data-id="{{ $shops->id }}" data-toggle="tooltip" data-placement="right" title="" data-original-title="Like">
                                                        <i class="fa fa-heart-o fa-lg text-danger"></i>
                                                    </a>
                                                @endif
                                            @endif
                                        </h1>
                                        <div class="ps-product__meta"></div>
                                        <div class="ps-shop__info">
                                            <p>
                                                <i class="fa fa-phone text-success pr-2"></i>
                                                {{ $shops->phone_number }}
                                                @if ($shops->phone_number_2)
                                                    {{ ", ".$shops->phone_number_2 }}
                                                @endif
                                            </p>
                                            <p>
                                                <i class="fa fa-whatsapp text-success pr-2"></i>
                                                {{ $shops->phone_number_3 }}
                                            </p>
                                            <p>
                                                <i class="fa fa-envelope pr-2"></i>{{ $shops->email }}
                                            </p>
                                            <p>
                                                <i class="fa fa-map-marker fa-lg pr-2"></i>{{ $shops->address }}
                                            </p>
                                            <p>
                                                <span class="p-2 border rounded">
                                                    <span class="border-right mr-2">
                                                        <strong class="defaultColor fontRighteous">OW</strong>
                                                    </span>
                                                    @for ($i = 0; $i < 5; $i++)
                                                        @if ($i< (int)$shops->shop_rating)
                                                            <span class="fa fa-star text-warning"></span>
                                                        @else
                                                            @if ((($shops->shop_rating - (int)$shops->shop_rating) == 0.5) && (((int)$shops->shop_rating) == $i ))
                                                                <span class="fa fa-star-half-o text-warning"></span>
                                                            @else
                                                                <span class="fa fa-star-o text-warning"></span>
                                                            @endif
                                                        @endif
                                                    @endfor
                                                    <strong>{{ $shops->shop_rating }}</strong>
                                                </span>    
                                            </p>

                                        </div>

                                        <div class="ps-product__sharing pt-0">
                                         
                                            <p>Share</p>
                                        {{--
                                            <a class="facebook" href="#"><i class="fa fa-facebook"></i></a>
                                            <a class="twitter" href="#"><i class="fa fa-twitter"></i></a>
                                            <a class="instagram bg-danger" href="#"><i class="fa fa-instagram"></i></a>
                                            <a class="bg-success" href="#"><i class="fa fa-whatsapp"></i></a>
                                            <a class="bg-info" href="#"><i class="fa fa-share-alt"></i></a>
                                        --}}
                                        </div>
                                        
                                        <ul class="ps-list--social">
                                            {{-- 
                                            <li>
                                                <a class="text-info" href="#" target="_blank" title="share"><i class="fa fa-share-alt"></i></a>
                                            </li>
                                            <li>
                                                <a class="facebook" href="#" target="_blank" title="Facebook"><i class="fa fa-facebook"></i></a>
                                            </li>
                                            <li>
                                                <a class="twitter" href="#" target="_blank" title="Twitter"><i class="fa fa-twitter"></i></a>
                                            </li>
                                            <li>
                                                <a class="instagram" href="#" target="_blank" title="instagram"><i class="fa fa-instagram"></i></a>
                                            </li>
                                            <li>
                                                <a class="text-success" href="#" target="_blank" title="whatsapp"><i class="fa fa-whatsapp"></i></a>
                                            </li>
                                            --}}
                                            <li>
                                                <a class="text-info copy_share_url" href="javascript:void(0);" title="copy" data-url="{{ Request::url() }}"><i class="fa fa-share-alt"></i></a>
                                            </li>
                                            {!! Share::currentPage(null, ['class' => 'facebook', 'title' => 'facebook'], '', '')->facebook() !!}
                                            {!! Share::currentPage(null, ['class' => 'twitter', 'title' => 'twitter'], '', '')->twitter() !!}
                                            {!! Share::currentPage(null, ['class' => 'linkedin', 'title' => 'linkedin'], '', '')->linkedin() !!}
                                            {!! Share::currentPage(null, ['class' => 'text-success', 'title' => 'whatsapp'], '', '')->whatsapp() !!} 
                                            
                                        </ul>                                        
                                    </div>
                                </div>

                                <!-- tabs -->
                                <div class="ps-product__content ps-tab-root pt-5">
                                    <ul class="ps-tab-list">
                                        <li class="active"><a href="#tab-aboutus">About us</a></li>
                                        <li><a href="#tab-offers">Offers</a></li>
                                        <li><a href="#tab-wingdeals">Wing Deals</a></li>
                                        @if($shops->menus->isNotEmpty())
                                            <li><a href="#tab-menus">Menus</a></li>
                                        @endif
                                    </ul>
                                    <div class="ps-tabs">

                                        <!-- about us tab -->
                                        <div class="ps-tab active" id="tab-aboutus">
                                            <div class="ps-document">
                                                <h4>{{ strtoupper($shops->name) }}</h4>
                                                {!! $shops->description !!}
                                                <h5>Phone : <span>+91 {{ $shops->phone_number }}</span></h5>
                                            </div>
                                        </div>
                                        <!-- end of about us tab -->

                                        <!-- offers tab -->
                                        <div class="ps-tab" id="tab-offers">
                                            @if ($shops->offersOngoing->isNotEmpty())
                                                @foreach ($shops->offersOngoing as $item)
                                                    <h4>{{ strtoupper($item->offer_name) }}</h4>
                                                    <div class="m-5">
                                                        @if(!empty($item->offer_image))
                                                        <div class="lightgallery">
                                                            <a href="<?php echo asset("$item->offer_image"); ?>">
                                                                <img class="mb-5" src="{{ asset('storage/'.$item->offer_image) }}" alt="" height="200px" width="auto">
                                                            </a>
                                                        </div>
                                                        @endif
                                                        <h5>Description</h5>
                                                        {!! $item->description !!}
                                                        <h5>Offer Dates : <span>{{ $item->from_date }} to {{ $item->to_date }}</span></h5>

                                                    </div>
                                                    <hr class="mb-5"/>
                                                @endforeach
                                            @else
                                                <p>Sorry no more offers available</p>
                                            @endif
                                        </div>
                                        <!-- end of offers tab -->

                                        <!-- wing deals tab -->
                                        <div class="ps-tab" id="tab-wingdeals">
                
                                            @if ($shops->dealsOngoing->isNotEmpty())
                                                @foreach ($shops->dealsOngoing as $item)

                                                {{-- @php
                                                    echo "<pre>";
                                                        print_r($item);
                                                        die;
                                                @endphp --}}
                                                    <div class="ps-post ps-post--small-thumbnail" id='{{ $item->slug }}'>
                                                        <div class="ps-post__thumbnail">
                                                            @if(!empty($item->pic))
                                                                <img src="{{ asset('storage/'.$item->pic) }}" alt="">
                                                            @endif
                                                        </div>
                                                        <div class="ps-post__content">
                                                            <div class="ps-post__top">
                                                                <span class="ps-post__title" >{{ strtoupper($item->name) }}</span>
                                                                @if ($item->payment_type == 'wingpoints')
                                                                    <p>Pay {{ $item->points_to_claim }} (wingpoints) to claim</p>
                                                                @else
                                                                    <p>Pay  ₹ {{ $item->price }} to claim</p>
                                                                @endif
                                                                {{-- 
                                                                @if ($item->payment_type == 'wingpoints')
                                                                    <p>(wingpoints) {{ $item->price }}</p>
                                                                @else
                                                                    <hp>₹.{{ $item->price }}</p>
                                                                @endif
                                                                <span> ₹. <del>{{ $item->original_price}} </del> </span> &nbsp;&nbsp; <span>  {{$item->price}} </span>
                                                                                    

                                                                <p>Flat {{ (($item->original_price - $item->price ) / $item->original_price) * 100 }} % off*</p>
                                                                --}}

                                                                <p>Flat 
                                                                    @php 
                                                                        echo round((($item->original_price - $item->price ) / $item->original_price) * 100)
                                                                    @endphp
                                                                 % off*
                                                             </p>
                                                                {{-- 
                                                                <p>
                                                                    Valid for:
                                                                    @php
                                                                        $valid_for = explode(',',$item->valid_for);
                                                                        $valid = config('global.valid_for');
                                                                    @endphp
                                                                    @foreach ($valid as $v)
                                                                        @if (in_array($v['name'],$valid_for))
                                                                        <img src="{{ asset($v['image']) }}" height="10px" width="10px">{{ $v['name'] }}

                                                                        @endif
                                                                    @endforeach
                                                                </p>
                                                                --}}
                                                            </div>
                                                            <div class="ps-post__bottom">
                                                                <button class="ps-btn btn-sm bg-warning text-dark dealdetails" data-id="{{ $item->id }}">
                                                                    <strong>Details</strong>
                                                                </button>

                                                                @if (Auth::guest())
                                                                    <button class="ps-btn btn-sm pull-right" data-toggle="modal" data-target="#myModal1">Claim Now</button>
                                                                @else
                                                                    @if ($item->payment_type != 'wingpoints')
                                                                    <a href="{{ url('/getdeal',[ $item->slug]) }}" class="ps-btn btn-sm pull-right"data-id="{{ $item->id }}">Claim Now</a>
                                                                    @else
                                                                        @if (Auth::user()->balance >= $item->points_to_claim)
                                                                            <a href="{{ url('/getdeal',[ $item->slug]) }}" class="ps-btn btn-sm pull-right" data-id="{{ $item->id }}">Claim Now</a>
                                                                        @else
                                                                            <button class="ps-btn btn-sm bg-dark pull-right" disabled> Not Enough Points </button>
                                                                        @endif
                                                                    @endif
                                                                @endif

                                                            </div>

                                                        </div>
                                                    </div>
                                                @endforeach
                                            @else
                                                <p>Sorry no deals available </p>
                                            @endif
                                        </div>
                                        <!-- end of wing deals tab -->

                                        <!-- menus tab -->
                                        @if (!empty($shops->menus))
                                        <div class="ps-tab" id="tab-menus">
                                            @foreach ($shops->menus as $item)
                                            <div class="col-md-4 thumbnail">
                                                <div class="lightgallery">
                                                    <a href="<?php echo asset("$item->image_path"); ?>">
                                                        <img src="<?php echo asset("$item->image_path"); ?>" alt="" style="width:100%;max-height:240px; height:240px;">
                                                    </a>
                                                </div>

                                            </div>
                                            @endforeach
                                        </div>
                                        @endif
                                        <!-- end of menus tab -->
                                    </div>
                                </div>
                                <!-- end of tabs -->

                            </div>
                        </div>

                        <div class="ps-page__right">
                            <aside class="widget">
                                <div id="map"></div>
                            </aside>
                            <aside class="widget widget_product widget_features">
                                <h4 class="text-center">Business Hours
                                @if (in_array(date('N',time())+1 , explode(",",$shops->active_days)))
                                    <small class="text-success"><b>( Open Now )</b></small>
                                @else`
                                    <small class="text-danger"><b>( Closed )</b></small>
                                @endif
                                </h4>
                                <hr></hr>
                                @php
                                    $active_days = explode(",",$shops->active_days);
                                    $today = date('l');
                                    $time = date('H:i');
                                @endphp
                                @foreach ($days as $key=>$item)
                                    @php
                                        $day = substr($item,0,3);
                                        $start_time = $shops[strtolower($day)."_start"];
                                        $end_time = $shops[strtolower($day)."_end"];
                                    @endphp
                                    <p class="p-0">{{ $day }} :
                                        @if (in_array( $key , $active_days))
                                            {{ date('h:i A',strtotime($start_time)) }} - {{ date('h:i A',strtotime($end_time)) }}
                                            @if ($today == $item)
                                                @if ( $time >= $start_time && $time <= $end_time)
                                                    <span class="text-success">Open</span>
                                                @else
                                                    <span class="text-danger"> Closed</span>
                                                @endif
                                            @endif
                                        @else
                                            Closed
                                        @endif
                                    </p>
                                @endforeach
                            </aside>
                            <aside class="widget">
                                <iframe src="https://www.youtube.com/embed/668nUCeBHyY?autoplay=1" width="100%" height="250" allowfullscreen></iframe>
                            </aside>
                        </div>

                    </div>
                </div>
            </div>

        </div>
    </div>
</section>



<div class="modal fade" id="dealdetails">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <h3 class="modal-title"></h3>
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true"> &times; </button>
            </div>
            <div class="modal-body p-4">

            </div>
        </div>
    </div>
</div>


  <script async defer
    src="https://maps.googleapis.com/maps/api/js?key=AIzaSyBHLFZ4C3XB7MDwjywzpBC1ZWMH0u2zuE4&callback=initMap">
</script>
<script>
    function initMap() {
        var myLatLng = {lat: {{$shops->lat}}, lng: {{ $shops->lng }} };

        var map = new google.maps.Map(document.getElementById('map'), {
          zoom: 12,
          center: myLatLng
        });

        var marker = new google.maps.Marker({
          position: myLatLng,
          map: map,
          title: 'Hello World!'
        });
      }

      $('.dealdetails').click(function () {
          let id =  $(this).data('id');

        $.ajax({
          type: "GET",
          url: "{{ url('/dealdetails/') }}"+"/"+id,

          success: function (response) {

            var json_obj = $.parseJSON(response);

            var mymodal = $('#dealdetails');
            mymodal.find('.modal-title').html(`
             `+ json_obj[0]['name'].toUpperCase()+`
             `);

             mymodal.find('.modal-body').html(`
             <div>
                <h4><u>Description:</u></h4>
                <p>`+json_obj[0]['description'] +`</p>
                <hr>
                <h4><u>Active days:</u></h4>
                <p>`+json_obj[0]['days_applicable'].join()+`</p>

                <h4><u>Valid for:</u></h4>
                <p>`+'one Person only'+`</p>

                <h4><u>Valid Dates:</u></h4>
                <p>`+json_obj[0]['valid_from']+' To '+json_obj[0]['valid_from']+`</p>

                <h4><u>Terms & Conditions:</u></h4>
                <p>`+json_obj[0]['terms_conditions'] +`</p>
                <div class="text-center">
                    @if (Auth::guest())
                        <button class="ps-btn" data-toggle="modal" data-target="#myModal1">Claim Now</button>
                    @else
                        <button class="ps-btn claim" data-id= `+id+`>Claim Now</button>
                    @endif
                </div>
             </div>
             `);
            $('#dealdetails').modal('show');
          }
      });

      })


      $(document).on('click', ".unlike", function (e) {
        e.preventDefault();
        var id = $(this).data('id');
        var element = this;

        var data = {
                shop_id:id
            };
        $.ajax({
                type: "post",
                url: '{{ url("removefavouriteshop") }}',
                headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                 },
                data: data,
                cache: false,
                success: function (data)
                {
                    if(data.status == "success"){
                        $(element).removeClass('unlike').addClass('like').attr('data-original-title', 'Like').html('<i class="fa fa-heart-o text-danger"></i>');
                    }
                },
                error: function (data){
                    swal('Fail to run unlike..');
                }
            });
    });

    $(document).on('click', ".like", function (e) {
        e.preventDefault();
        var id = $(this).data('id');
        var element = this;

        var data = {
                shop_id:id
            };
        $.ajax({
            type: "post",
            url: '{{ url("addfavouriteshop") }}',
            headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
            data: data,
            cache: false,
            success: function (data)
            {
                if(data.status == "success"){
                    $(element).removeClass('like').addClass('unlike').attr('data-original-title', 'Unlike').html('<i class="fa fa-heart text-danger"></i>');
                }
            },
            error: function (data){
                swal('Fail to run like..');
            }
        });
    });
</script>
@endsection
