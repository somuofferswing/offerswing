



<?php $__env->startSection('content'); ?>

<style>

    .table-bordered td{

        padding:20px;

        font-weight:bold;

    }

</style>

<div class="ps-breadcrumb">

    <div class="container">

        <ul class="breadcrumb text-capitalize" id="breadcrumb">

            <li><a href="<?php echo e(url('/')); ?>">Home</a></li>

            <li>Wing Coins</li>

        </ul>

    </div>

</div>

<section>

    <div class="ps-page--blog">

        <div class="container">

            <div class="text-center pt-5">

                <img src="<?php echo e(asset('main/images/refer-coin.png')); ?>">

                <div class="ps-page__header">

                    <h1 class="text-center">Wing Coins : <?php echo e($total_balance); ?></h1>

                    <hr class="highlighter">

                </div>

            </div>

            

            <div class="row m-5">

                <div class="col-12 col-md-3 text-center mb-3">

                    <a href="<?php echo e(url('/referafriend')); ?>" class="ps-btn bg-primary text-white">Refer a Friend</a>

                </div>

                <div class="col-12 col-md-5 text-center mb-3">

                    <form action="<?php echo e(url('/token/redeem')); ?>" method="POST">

                        <?php echo csrf_field(); ?>

                        <div class="row">

                            <input type="text" class="form-control col-8" placeholder="Enter your token code"

                                name="token_id" required>

                            <button class="ps-btn col-4" type="submit"> Submit</button>

                        </div>

                    </form>

                </div>

                <div class="col-12 col-md-2 text-center mb-3">

                    <a href="<?php echo e(url('/redeem')); ?>" class="ps-btn bg-success text-white">Redeem</a>

                </div>

            </div>



            <div class="p-5">

                <h2 class="text-center">History</h2>

        

                <table class="table table-bordered">

                    <tbody>

                        <?php $__currentLoopData = $transactions; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $item): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>

                        <?php
                            $meta = $item->meta;
                        ?>

                        <tr>

                            <td><?php echo e($meta['type']); ?></td>

                            <td> <?php if($item['type'] == 'withdraw'): ?> 
                                -                                
                            <?php endif; ?> <?php echo e($item['amount']); ?></td>

                        </tr>

                        <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>

                    </tbody>

                </table>

            </div>

        </div>

    </div>

</section>

<?php $__env->stopSection(); ?>
<?php echo $__env->make('layouts.app', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH /home/offerkrx/public_html/offerswing_test/resources/views/frontend/wingcoins.blade.php ENDPATH**/ ?>