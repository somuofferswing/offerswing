<?php

namespace App\Http\Controllers;

use App\Models\Claimed;
use App\Models\Deal;
use App\Models\Event;
use App\Models\Notification;
use App\Models\OfferNotification;
use App\Models\Order;
use App\Models\Payment;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Mail;
use Softon\Indipay\Facades\Indipay;

class PaymentController extends Controller
{
    public function proceedtopay($id)
    {
        $order = Order::find($id);
        $order_details = $order->orderDetails;

        $type = $order_details['order_type'];
        $type_id = $order_details['order_type_id'];
        $payment_type = $order_details['payment_type'];

        $user_details = User::find($order['user_id']);
        $user = auth()->user();

        if ($payment_type == 'cash') {
            $parameters = [
            'firstname' => $user['first_name'],
            'email' => $user['email'],
            'phone' => $user['mobile'],
            'productinfo' => json_encode(['order_id'=>$order->order_id,'order_type'=>$type,'order_type_id'=> $type_id]),
            'amount' => $order['amount'],
            // 'amount' => 1,
        ];

            $order12 = Indipay::gateway('PayUMoney')->prepare($parameters);

            return Indipay::process($order12);
        } else {
            if ($order['amount'] > auth()->user()->balance) {
                return back()->with('alert', 'Not Enough WingCoins to claim')->with('class', 'warning');
            } else {
                if ($type == 'EVENT') {
                    $user->withdraw($order['amount'], 'withdraw', ['type' => 'Event', 'description' => 'Spent on Event','id'=>$type_id], 'Event');

                    $event = Event::find($type_id);

                    $event->quantity -= $order_details['quantity'];
                    $event->save();

                    $order->payment_status = 'SUCCESS';
                    $order->save();

                    $notifications = new  Notification();

                    $notifications->type = 'Event';
                    $notifications->type_id = $event->id;
                    $notifications->user_id = $user['id'];
                    $notifications->notification_message = "your tickeck for event '".$event->title."' has been booked Successfully!";

                    $notifications->save();

                    $settings = session()->get('settings');

                    $from_email = config('settings.contact_email');
                    $user_email = $user['email'];

                    Mail::send('email.order_event_details', ['order' => $order, 'order_details' => $order_details, 'event' => $event], function ($message) use ($user_email, $from_email) {
                        $message->to($user_email)->from($from_email)->subject('Order Details');
                    });
                    return redirect()->route('eventorder', [$order->order_id]);
                } elseif ($type == 'DEAL') {
                    $user->withdraw($order['amount'], 'withdraw', ['type' => 'Deal', 'description' => 'Spent on deal','id'=>$type_id], 'Deal');

                    $deal = Deal::find($type_id);

                    $deal->quantity -= $order_details['quantity'];
                    $deal->save();

                    $order->payment_status = 'SUCCESS';
                    $order->save();

                   

                    $notifications = new  Notification();

                    $notifications->type = 'DEAL';
                    $notifications->type_id = $deal->id;
                    $notifications->user_id = $user['id'];
                    $notifications->notification_message = "your deal '".$deal->name."' has been claimed Successfully!";
            
                    $notifications->save();
            
                    $settings = session()->get('settings');
            
                    $from_email = config('settings.contact_email');
                    $user_email = $user['email'];
            
                    Mail::send('email.order_deal_details', ['order' => $order, 'order_details' => $order_details, 'deal' => $deal], function ($message) use ($user_email, $from_email) {
                        $message->to($user_email)->from($from_email)->subject('Order Details');
                    });


                    
                    return redirect()->route('dealorder', [$order->order_id]);
                }
            }
        }
    }

    public function indipay_response(Request $request)
    {
        $response = Indipay::response($request);

        
        $product_info = (array)json_decode($response['productinfo']);
        //print_r($product_info);die;
        $order_id = $product_info['order_id'];
        $order_type = $product_info['order_type'];
        $order_type_id = $product_info['order_type_id'];
        $order = Order::where('order_id', $order_id)->first();
        $order_details = $order->orderDetails;

        $order->payment_status = strtoupper($response['status']);

        if ($order->save()) {
            $payment = new Payment();
            $payment->order_id = $order->id;
            $payment->transaction_id = $response['txnid'];
            $payment->payuMoneyId = $response['payuMoneyId'];
            $payment->productinfo = json_encode($product_info);
            $payment->bank_reference_number = $response['bank_ref_num'];
            $payment->mode = $response['mode'];
            $payment->amount = isset($response['net_amount_debit']) ? $response['net_amount_debit'] : '';
            $payment->status = $response['status'];
            $payment->meta = json_encode($response);

            $payment->save();
        }
        $user = User::find($order->user_id);

        if ($response['status'] == 'success') {
            if ($order_type == 'DEAL') {
                $deal = Deal::find($product_info['order_type_id']);
                $deal->quantity -= $order_details->quantity;
                $deal->save();

                $notifications = new  Notification();

                $notifications->type = 'DEAL';
                $notifications->type_id = $deal->id;
                $notifications->user_id = $user['id'];
                $notifications->notification_message = "your deal '".$deal->name."' has been claimed Successfully!";
        
                $notifications->save();
        
                $settings = session()->get('settings');
        
                $from_email = config('settings.contact_email');
                $user_email = $user['email'];
        
                Mail::send('email.order_deal_details', ['order' => $order, 'order_details' => $order_details, 'deal' => $deal], function ($message) use ($user_email, $from_email) {
                    $message->to($user_email)->from($from_email)->subject('Order Details');
                });

                    
                return redirect()->route('dealorder', [$order->order_id]);
            } elseif ($order_type == 'EVENT') {
                $event = Event::find($product_info['order_type_id']);
                $event->quantity -= $order_details->quantity;
                $event->save();

                $notifications = new  Notification();

                $notifications->type = 'Event';
                $notifications->type_id = $event->id;
                $notifications->user_id = $user['id'];
                $notifications->notification_message = "your tickeck for event '".$event->title."' has been booked Successfully!";

                $notifications->save();

                $settings = session()->get('settings');

                $from_email = config('settings.contact_email');
                $user_email = $user['email'];

                Mail::send('email.order_event_details', ['order' => $order, 'order_details' => $order_details, 'event' => $event], function ($message) use ($user_email, $from_email) {
                    $message->to($user_email)->from($from_email)->subject('Order Details');
                });
                return redirect()->route('eventorder', [$order->order_id]);
            }
        } else {
            return redirect()->route('user_order', [$order_id]);
        }
    }

    public function generateRandomString($length = 10)
    {
        $characters = '0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZ';
        $charactersLength = strlen($characters);
        $randomString = '';
        for ($i = 0; $i < $length; ++$i) {
            $randomString .= $characters[rand(0, $charactersLength - 1)];
        }

        return $randomString;
    }
}
