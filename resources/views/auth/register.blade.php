@extends('layouts.app')

@section('content')
<div class="container">
    {{-- <div class="row justify-content-center"> --}}
        <div class="col-md-8 col-md-offset-2">
            <div class="panel panel-default">
                <div class="panel-heading"><h3>{{ __('Register') }}</h3> </div>

                <div class="panel-body">
                    <form method="POST" action="{{ route('register') }}">
                        @csrf
                                <input type="hidden" id="user_type" name="user_type" value="2">
                                <div id="signupalert" style="display:none" class="alert alert-danger">
                                    <p>Error:</p>
                                    <span></span>
                                </div>


                                <div class="form-group">
                                    <label for="firstname" class="col-md-3 control-label">{{ __('First Name') }}</label>

                                    <div class="col-md-9">
                                        <input type="text" id="firstname"
                                            class="form-control @error('first_name') is-invalid @enderror"
                                            name="first_name" value="{{ old('first_name') }}" required
                                            autocomplete="first_name" autofocus>
                                        @error('name')
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $message }}</strong>
                                        </span>
                                        @enderror
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label for="lastname" class="col-md-3 control-label">{{ __('Last Name') }}</label>
                                    <div class="col-md-9">
                                        <input type="text" id="lastname"
                                            class="form-control @error('last_name') is-invalid @enderror"
                                            name="last_name" value="{{ old('last_name') }}" required autocomplete="last_name"
                                            autofocus>

                                        @error('name')
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $message }}</strong>
                                        </span>
                                        @enderror
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label for="mobile" class="col-md-3 control-label">{{ __('Mobile') }}</label>
                                    <div class="col-md-9">
                                        <input type="text" id="mobile"
                                            class="form-control @error('mobile') is-invalid @enderror"
                                            name="mobile" value="{{ old('mobile') }}" required autocomplete="mobile"
                                            autofocus>

                                        @error('name')
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $message }}</strong>
                                        </span>
                                        @enderror
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label for="email" class="col-md-3 control-label">{{ __('E-Mail Address') }}</label>
                                    <div class="col-md-9">
                                        <input type="email" id="email"
                                            class="form-control @error('email') is-invalid @enderror" name="email"
                                            value="{{ old('email') }}" required autocomplete="email">
                                        @error('email')
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $message }}</strong>
                                        </span>
                                        @enderror
                                    </div>
                                </div>


                                <div class="form-group">
                                    <label for="dob" class="col-md-3 control-label">{{ __('DOB') }}</label>
                                    <div class="col-md-9">
                                        <input class="form-control datepicker" type="text" id="dob"name="dob" value="{{ old('dob') }}" required>
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label for="password_confirm"
                                        class="col-md-3 control-label">{{ __('Gender') }}</label>
                                    <div class="col-md-9">
                                        <input type="radio"  class="radio-inline"
                                            name="gender" required value="MALE">Male
                                        <input type="radio"  class="radio-inline"
                                            name="gender" required value="FEMALE">Female
                                        <input type="radio"  class="radio-inline"
                                            name="gender" required value="OTHERS">Other
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label for="password" class="col-md-3 control-label">{{ __('Password') }}</label>
                                    <div class="col-md-9">
                                        <input type="password" id="password"
                                            class="form-control @error('password') is-invalid @enderror" name="password"
                                            required autocomplete="new-password">

                                        @error('password')
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $message }}</strong>
                                        </span>
                                        @enderror
                                    </div>
                                </div>


                                <div class="form-group">
                                    <label for="password_confirm"
                                        class="col-md-3 control-label">{{ __('Confirm Password') }}</label>
                                    <div class="col-md-9">
                                        <input type="password" id="password_confirm" class="form-control"
                                            name="password_confirmation" required autocomplete="new-password">
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label for="referal_code" class="col-md-3 control-label">Referal Code</label>
                                    <div class="col-md-9">
                                        <input type="text" id="referal_code" class="form-control"
                                            name="referal_code" value="{{ old('referal_code') }}">
                                    </div>
                                </div>

                                <div class="form-group">
                                    <!-- Button -->
                                    <div class="col-md-offset-3 col-md-9">
                                        <button id="btn-signup" type="submit" class="btn btn-info"><i
                                                class="icon-hand-right"></i> &nbsp Sign Up</button>
                                        {{-- <span style="margin-left:8px;">or</span> --}}
                                    </div>
                                </div>
                    </form>
                </div>
            </div>
        </div>
    {{-- </div> --}}
</div>
@endsection
