<?php

namespace App\Http\Controllers\Backend;

use App\Http\Controllers\Controller;
use App\Models\Shop;
use App\Models\ShopOwner;
use App\Models\ShopUser;
use App\Models\User;
use App\Models\UserType;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;

class UserController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        // $users = User::all();
        $users = User::where('is_deleted', 0)->get();

        return view('backend.user.allusers', compact('users'));
    }

    public function alldeleted()
    {
        // $users = User::all();
        $users = User::where('is_deleted', 1)->get();

        return view('backend.user.alldeleted', compact('users'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $user_types = UserType::all();

        return view('backend.user.add', compact('user_types'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'first_name' => 'required|string|max:255',
            'last_name' => 'required|string|max:255',
            'email' => 'required|string|email|max:255|unique:users',
            'mobile' => 'required|numeric|digits:10|unique:users',
            'user_type' => 'required',
        ]);

        $email = $request->email;
        $password = 'Offerswing';
        $user_type = $request->user_type;
        $first_name = $request->first_name;
        $last_name = $request->last_name;
        $mobile = $request->mobile;

        $hash_password = Hash::make($password);

        $users = new User();
        $users->email = $email;
        $users->password = $hash_password;
        $users->first_name = $first_name;
        $users->last_name = $last_name;
        $users->user_type = $user_type;
        $users->mobile = $mobile;

        $users->save();

        return redirect('/allusers')->with('alert', 'User created Successfully')->with('class', 'success');
    }

    /**
     * Display the specified resource.
     *
     * @param int $id
     *
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param int $id
     *
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $user = User::find($id);
        $user_types = UserType::all();

        return view('backend.user.edit', compact('user', 'user_types'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param int $id
     *
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->validate($request, [
            'first_name' => 'required|string|max:255',
            'last_name' => 'required|string|max:255',
            'email' => 'required|string|email|max:255|unique:users,email,'.$id,
            'mobile' => 'required|numeric|digits:10|unique:users,mobile,'.$id,
            'user_type' => 'required',
        ]);

        $email = $request->email;
        // $password = 'Offerswing';
        $user_type = $request->user_type;
        $first_name = $request->first_name;
        $last_name = $request->last_name;
        $mobile = $request->mobile;

        // $hash_password = Hash::make($password);

        $users = User::find($id);

        $users->email = $email;
        // $users->password = $hash_password;
        $users->first_name = $first_name;
        $users->last_name = $last_name;
        $users->user_type = $user_type;
        $users->mobile = $mobile;

        $users->save();

        return redirect('/allusers')->with('alert', 'User Updated Successfully')->with('class', 'success');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param int $id
     *
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $user = User::find($id);
        $user->delete();

        return redirect('/allusers')->with('alert', 'User deleted Permenently Successfully')->with('class', 'success');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param int $id
     *
     * @return \Illuminate\Http\Response
     */
    public function delete($id)
    {
        $user = User::find($id);
        $user->is_deleted = 1;
        // $user->save();
        if ($user->save() && 3 == $user->user_type) {
            // $model->pivot->data = $pivot_data; $model->pivot->update();
            $shops_owners = ShopUser::where('user_id', $id)->get();

            foreach ($shops_owners as $key => $value) {
                $shop = Shop::find($value->shop_id);
                $shop->is_deleted = 1;
                $shop->save();
            }
        }

        return redirect('/allusers')->with('alert', 'User deleted Successfully')->with('class', 'success');
    }

    public function deactivateUser($id)
    {
        $user = User::find($id);
        $user->status = 0;
        // $user->save();
        if ($user->save() && 3 == $user->user_type) {
            $shops_owners = ShopOwner::where('retailer_id', $id)->get();

            foreach ($shops_owners as $key => $value) {
                $shop = Shop::find($value->shop_id);
                $shop->active = 0;
                $shop->save();
            }
        }

        return back()->with('alert', 'User deactivated Successfully')->with('class', 'success');
    }

    public function activateUser($id)
    {
        $user = User::find($id);
        $user->status = 1;
        $user->save();

        return back()->with('alert', 'User activated Successfully')->with('class', 'success');
    }

    public function retrive($id)
    {
        $user = User::find($id);
        $user->is_deleted = 0;
        $user->save();

        return redirect('/allusers')->with('alert', 'User Retrived Successfully')->with('class', 'success');
    }

    public function update_password(Request $request)
    {
        if (!(Hash::check($request->get('current_password'), Auth::user()->password))) {
            return response()->json([
                'status' => 'success',
                'message' => 'Your current password does not matches with the password you provided. Please try again.',
                'class_name' => 'alert-danger',
            ]);
        }

        if (0 == strcmp($request->get('current_password'), $request->get('new-password'))) {
            return response()->json([
                'status' => 'success',
                'message' => 'New Password cannot be same as your current password. Please choose a different password.',
                'class_name' => 'alert-danger',
            ]);
        }

        $validation = $this->validate($request, [
            'current_password' => 'required',
            'new-password' => 'required|string|min:8|confirmed',
        ]);
        if ($validation) {
            $user = Auth::user();
            $user->password = bcrypt($request->get('new-password'));
            $user->save();

            return response()->json([
                'status' => 'success',
                'message' => 'Password changed successfully !',
                'class_name' => 'alert-success',
            ]);
        }

        return response()->json([
            'status' => 'success',
            'message' => $validation->errors()->all(),
            'class_name' => 'alert-danger',
        ]);

        die;
    }

    public function sendOtp(Request $request)
    {
    }
}
