@extends('layouts.admin')

@section('content')

<div class="content-wrapper">
    <section class="content-header">
        <h1>
            Users
            <small>Update user</small>
        </h1>
        <!-- <ol class="breadcrumb">
                  <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
                  <li class="active">Dashboard</li>
                </ol> -->
    </section>

    <section class="content">
        {{-- <div class="col-md-12"> --}}
        <div class="box box-info">
            <div class="box-header with-border">
                <h3 class="box-title">Update User</h3>
            </div>
            @if ($errors->any())
            <div class="alert alert-danger">
                <ul>
                    @foreach ($errors->all() as $error)
                    <li>{{ $error }}</li>
                    @endforeach
                </ul>
            </div><br />
            @endif
            <div class="box-body" style="padding: 10px 50px;">
                <form class="form-horizontal" method="POST" role="form" action="{{ url('/updateuser',[$user->id]) }}">

                    @csrf
                    {{-- <input type="hidden" id="register_user_type" name="user_type" value="2"> --}}
                    <div class="form-group">
                        <label for="register_user_type" class="col-md-3 control-label">{{ __('User Type') }}</label>
                        <div class="col-md-9">
                            <select class="form-control" name="user_type">
                                @foreach ($user_types as $item)

                                <option value="{{ $item->id }}"
                                @if ($item->id == $user->user_type)
                                    selected
                                @endif
                                >{{ $item->type_name }}
                                <option>

                                    @endforeach
                            </select>
                            <span id="user_type_error"></span>
                        </div>

                    </div>

                    <div class="form-group">
                        <label for="register_firstname" class="col-md-3 control-label">{{ __('First Name') }}</label>

                        <div class="col-md-9">
                            <input type="text" id="register_firstname" class="form-control" name="first_name"
                                value="{{ $user->first_name }}" required>
                            <span id="firstname_error"></span>
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="register_lastname" class="col-md-3 control-label">{{ __('Last Name') }}</label>
                        <div class="col-md-9">
                            <input type="text" id="register_lastname" class="form-control" name="last_name"
                                value="{{ $user->last_name }}" required>
                            <span id="lastname_error"></span>
                        </div>
                    </div>

                    <div class="form-group">
                        <label for="register_mobile" class="col-md-3 control-label">{{ __('Mobile') }}</label>
                        <div class="col-md-9">
                            <input type="text" id="register_mobile" class="form-control " name="mobile"
                                value="{{ $user->mobile }}" required>
                            <span id="mobile_error"></span>
                        </div>
                    </div>

                    <div class="form-group">
                        <label for="register_email" class="col-md-3 control-label">{{ __('E-Mail Address') }}</label>
                        <div class="col-md-9">
                            <input type="email" id="register_email" class="form-control" name="email"
                                value="{{ $user->email }}" required>
                            <span id="email_error"></span>
                        </div>
                    </div>


                    {{-- <div class="form-group">
                        <label for="register_dob" class="col-md-3 control-label">{{ __('Active') }}</label>
                        <div class="col-md-9">
                            <input class="form-control datepicker" type="text" id="register_dob" name="dob" required>
                            <span id="dob_error"></span>
                        </div>
                    </div> --}}

                    {{-- <div class="form-group">
                <label for="register_password_confirm" class="col-md-3 control-label">{{ __('Gender') }}</label>
                    <div class="col-md-9">
                        <input type="radio" class="radio-inline" name="register_gender" required value="MALE">Male
                        <input type="radio" class="radio-inline" name="register_gender" required value="FEMALE">Female
                        <input type="radio" class="radio-inline" name="register_gender" required value="OTHERS">Other
                        <span id="gender_error"></span>
                    </div>
            </div> --}}


            <div class="form-group">
                <!-- Button -->
                <div class="col-md-offset-3 col-md-9">
                    <button id="btn-signup" type="submit" class="btn btn-info">
                        Update</button>
                    <a href="{{ url()->previous() }}" class="btn btn-danger">{{ __('Cancel') }}</a>
                </div>
            </div>
            </form>
        </div>
</div>
{{-- </div> --}}
</section>
</div>




@endsection
