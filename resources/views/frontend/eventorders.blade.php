@extends('layouts.app')

@section('content')
<style>
    .ps-section--shopping {
        padding: 30px 0;
    }

    .ps-table--shopping-cart tbody tr td {
        text-align: center;
    }

    .ps-table--shopping-cart tbody tr td:first-child {
        text-align: left;
    }
</style>

<section>

    <div class="ps-page--simple">

        <div class="ps-section--shopping ps-shopping-cart">

            <div class="container">

                <div class="ps-section__header pb-5">

                    <h1>Your Order</h1>

                </div>

                <div class="ps-section__content">

                    <div class="table-responsive">

                        <table class="table ps-table--shopping-cart">

                            <thead>

                                <tr>

                                    <th>ITEM NAME</th>

                                    <th>TYPE</th>

                                    <th>PRICE</th>

                                    <th>QUANTITY</th>

                                    <th>TOTAL</th>

                                    <th></th>

                                </tr>

                            </thead>

                            <tbody>

                                <tr>

                                    <td>{{$event['title']}}</td>

                                    <td>{{$order_details['order_type']}}</td>

                                    <td>
                                        @if ($event['payment_type'] == 'wingpoints')
                                        {{ $event['points_to_claim'] }} (wingpoints)
                                        @endif

                                        @if ($event['payment_type'] == 'cash')
                                        ₹ {{ $event['event_price'] }}
                                        @endif

                                    </td>

                                    <td>

                                        <div class="form-group--number">

                                            <button class="up quantity" id="plus">+</button>

                                            <button class="down quantity" id="minus">-</button>

                                            <input class="form-control" type="text" placeholder="1"
                                                value="{{ $order_details['quantity'] }}">

                                        </div>

                                    </td>

                                    <td>
                                        @if ($event['payment_type'] == 'wingpoints')
                                        {{ $event['points_to_claim'] * $order_details['quantity'] }}
                                        @endif

                                        @if ($event['payment_type'] == 'cash')

                                        ₹ {{ $event['event_price'] * $order_details['quantity'] }}
                                        @endif
                                    </td>

                                    <td><a class="delete" href="#"><i class="icon-cross"></i></a></td>

                                </tr>
                            </tbody>

                        </table>

                    </div>

                    <div class="ps-section__cart-actions">

                        <a class="ps-btn bg-dark" href="{{ url('/eventdetails',[$event['id']]) }}">

                            <i class="icon-arrow-left"></i> Back to Event

                        </a>

                        <a class="ps-btn" href="{{ url('/proceedtopay',[$order_details['order_id']])}}">

                            <i class="fa fa-send pr-2"></i>Proceed to Pay

                        </a>

                    </div>

                </div>



            </div>

        </div>

    </div>

</section>

<script>
 

var order_quantity = {{ $order_details['quantity'] }};

if(order_quantity == 1){
    $('#minus').attr("disabled", true);
}

if(order_quantity == 10){
    $('#plus').attr("disabled", true);
}

$('.quantity').on('click',function(){

var quant = $(this).prop('id');

var id = "{{ $order['id'] }}";

if(quant == 'plus'){
quantity = 1
}else{
quantity= -1
}

order_quantity = order_quantity + quantity;

if(order_quantity > 10){
    alert ("Can't book more than 10");
    return false;
}

var data= {
id:id,
quantity:quantity
}

$.ajax({
type: "post",
url: '{{ url("updateorder") }}',
headers: {
'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
},
data: data,
// dataType: "dataType",

success: function (response) {

if(response.status == 'success'){
location.reload();
}else{
var url = response.url;
window.location.href = "{{ url('/') }}"+url;
}
}
});
});



$('.delete').on('click',function(e){

e.preventDefault();

var id = "{{ $order['id'] }}";

var data= {

id:id

}



$.ajax({

type: "post",

url: '{{ url("deleteorder") }}',

headers: {

'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')

},

data: data,

success: function (response) {

if(response.status == 'success'){

window.location.href = "{{ url('/') }}";

}

}

});

});



</script>

@endsection