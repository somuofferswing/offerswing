<!DOCTYPE html>
<html lang="en">

<head>
    <title>Bootstrap Example</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.0/css/bootstrap.min.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.0/js/bootstrap.min.js"></script>
</head>

<body>

    <div class="container">
        <div class="row">
            <div class="col-md-8 col-md-offset-2" style="padding:5px !important;">
                <div calss="row">

                    {{-- <img src="{{ asset(/main/images/newlogo.png) }}" style="height:100px"
                        class="img-responsive" /> --}}



                </div>
                <div class="row">
                    <div class="col-md-6 col-xs-12">
                        <h4>Events receipt</h4>
                        <h5>Order no: {{ $order->order_id }}</h5>
                        <h5>Fri,07 Jun 2019, 21:39:02</h5>
                    </div>
                    <div class="col-md-6 col-xs-12 text-right">
                        <h4>Address</h4>
                        <h5>Srikakulam</h5>

                        <h5>Andhra Pradesh</h5>

                        <div class="details" style="margin-top:20px;">
                            <h5>1234567890</h5>
                            <h5><a href="#">offerswing.com</a></h5>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <h3>Your Event Receipt:</h3>
                        <div class="col-md-6 col-md-offset-3 border my-3">
                            <div class="image">
                                <img src="{{asset('storage/'.$event->event_image)}}" class="img-fluid"
                                    style="width:100%; height:200px;">
                            </div>
                            <hr>
                            <div class="dealbottom">
                                <h5>Event Date : <span>{{ date('d-m-Y',strtotime($event->event_date)) }}</span> <span class="float-right">Event Time :
                                        {{ date('h:i A',strtotime($event->event_time)) }}</span> </h5>
                            </div>
                            <div class="text-center">
                                {!! QrCode::size(250)->generate($order->order_id); !!}
                            </div>

                            <div class="dealdescription ">

                                <table style="margin:0 auto;">
                                    {{-- <tr>
                                        <td>Booking id</td>
                                        <td style="width:20px;">:</td>
                                        <td style="text-align:left;">{{ $claimed->unique_code }}</td>
                                    </tr> --}}
                                    <tr>
                                        <td>About Event</td>
                                        <td style="width:20px;">:</td>
                                        <td style="text-align:left;">{!! $event->description !!}</td>
                                    </tr>
                                    <tr>
                                        <td>Brought on</td>
                                        <td style="width:20px;">:</td>
                                        <td style="text-align:left;">{{ date('d-m-Y h:i A',strtotime($order_details->created_at)) }}</td>
                                    </tr>
                                    <tr>
                                        <td>Event Date</td>
                                        <td style="width:20px;">:</td>
                                        <td style="text-align:left;">{{ date('d-m-Y',strtotime($event->event_date)) }}</td>
                                    </tr>
                                    <tr>
                                        <td>Event Time</td>
                                        <td style="width:20px;">:</td>
                                        <td style="text-align:left;">{{ date('h:i A',strtotime($event->event_time)) }}</td>
                                    </tr>
                                </table>

                            </div>

                            <div class="storeaddress">
                                <h5> <b> Event Address : </b> {!! $event->event_address !!}</h5>


                            </div>
                            <div class="primarydetails ">


                                <table style="margin:0 auto; text-align:left;">
                                    {{-- <tr>
                                        <td>Contact Number</td>
                                        <td style="width:20px;">:</td>
                                        <td>+91 1234567890</td>
                                    </tr> --}}
                                    <tr>
                                        <td>Number of Tickets</td>
                                        <td style="width:20px;">:</td>
                                        <td>{{ $order_details->quantity }}</td>
                                    </tr>
                                    <tr>
                                        <td>Price</td>
                                        <td style="width:20px;">:</td>
                                        <td>
                                            @if ( $order_details->payment_type == 'wingpoints')
                                            {{ $order_details->quantity }} x {{ $event->points_to_claim }} = {{ $order_details->quantity *  $event->points_to_claim }}
                                            @endif
                                            @if ( $order_details->payment_type == 'cash')
                                            {{ $order->quantity }} x {{ $event->price }} = {{ $order->quantity *  $event->price }}
                                            @endif
                                        </td>
                                    </tr>

                                </table>


                            </div>
                            <div class="storecondition">
                                <p><b>Terms & Conditions :</b> {!! $event->terms_conditions !!}</p>


                            </div>

                        </div>

                    </div>

            </div>
        </div>
    </div>

</body>

</html>
