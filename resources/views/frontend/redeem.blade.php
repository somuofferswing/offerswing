@extends('layouts.app')



@section('content')

<style>
    @media (min-width: 992px) {

        .ps-blog--sidebar .ps-blog__left {

            padding-right: 0px;

        }

    }



    .ps-post--small-thumbnail .ps-post__content {

        padding: 0px 20px;

    }
</style>

<div class="ps-breadcrumb">

    <div class="container">

        <ul class="breadcrumb text-capitalize" id="breadcrumb">

            <li><a href="{{ url('/') }}">Home</a></li>

            <li>Redeem Gifts</li>

        </ul>

    </div>

</div>

<section>

    <div class="ps-page--blog">

        <div class="container">

            <div class="ps-page__header">

                <h1>Redeem Gifts</h1>

                <hr class="highlighter">

            </div>

            <div class="ps-blog--sidebar">



                <div class="ps-blog__left">

                    @foreach ($gifts as $item)



                    <div class="ps-post ps-post--small-thumbnail">

                        <div class="ps-post__thumbnail">

                            @if(!empty($item->image))

                            <img src="{{ asset('storage/'.$item->image)  }}" alt="">

                            @endif

                        </div>

                        <div class="ps-post__content">

                            <div class="ps-post__top">

                                <span class="ps-post__title">{{ ucfirst($item->gift_name) }}</span>

                                {!! $item->description !!}

                            </div>

                            <div class="ps-post__bottom">

                                <span>Wings Points to Claim: <b
                                        class="text-danger">{{ $item->points_to_claim }}</b></span>



                                @if (Auth::guest())

                                <a href="#" class="ps-btn btn-xs pull-right" onClick="return false" data-toggle="modal"
                                    data-target="#myModal1">

                                    Claim Now

                                </a>

                                @else

                                @if (Auth::user()->balance >= $item->points_to_claim )

                                <a href="{{ url('/getgift',[ $item->slug]) }}" class="ps-btn btn-xs pull-right">

                                    Claim Now

                                </a>

                                @else

                                <span class="text-danger pull-right">

                                    Not Enough Points

                                </span>

                                @endif

                                @endif



                            </div>



                        </div>

                    </div>

                    @endforeach

                </div>



                <div class="ps-blog__right">

                    <div class="ps-block__slider">

                        <div class="ps-carousel--product-box owl-slider" data-owl-auto="true" data-owl-loop="true"
                            data-owl-speed="7000" data-owl-gap="0" data-owl-nav="true" data-owl-dots="true"
                            data-owl-item="1" data-owl-item-xs="1" data-owl-item-sm="1" data-owl-item-md="1"
                            data-owl-item-lg="1" data-owl-duration="500" data-owl-mousedrag="off">

                            <a href="#">

                                <img src="http://nouthemes.net/html/martfury/img/slider/home-3/clothing-1.jpg" alt="">

                            </a>

                            <a href="#">

                                <img src="http://nouthemes.net/html/martfury/img/slider/home-3/clothing-2.jpg" alt="">

                            </a>

                            <a href="#">

                                <img src="http://nouthemes.net/html/martfury/img/slider/home-3/clothing-3.jpg" alt="">

                            </a>

                        </div>

                    </div>

                </div>

            </div>

        </div>

    </div>

    </div>

</section>



<!-- <a class="btn btn-primary" >Trigger modal</a> -->

<div class="modal fade" id="share">

    <div class="modal-dialog">

        <div class="modal-content">

            <div class="modal-body">

                <div class="a2a_kit a2a_kit_size_32 a2a_default_style">

                    {{-- <a class="a2a_dd" href="https://www.addtoany.com/share"></a> --}}

                    <a class="a2a_button_facebook"></a>

                    <a class="a2a_button_twitter"></a>

                    <a class="a2a_button_email"></a>

                    <a class="a2a_button_whatsapp"></a>

                </div>

                <script async src="https://static.addtoany.com/menu/page.js"></script>

            </div>

        </div>

    </div>

</div>



<script>
    $(document).on('click', ".unlike", function () {

            var id = $(this).data('id');

            var type = "event";

            var element = this;



            var data = {

                    id:id,

                    type:type

                };

            $.ajax({

                    type: "post",

                    url: '{{ url("removelike") }}',

                    headers: {

                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')

                     },

                    data: data,

                    cache: false,

                    success: function (data)

                    {

                        if(data.status == "success"){

                            $(element).removeClass('unlike').addClass('like').html('<i class="far fa-heart" style="color:#e31b23"></i> &nbsp;like');

                        }

                    },

                    error: function (data){

                        alert('Fail to run unlike..');

                    }

                });





        });

        $(document).on('click', ".like", function () {

            var id = $(this).data('id');

            var type = "event";

            var element = this;



            var data = {

                    id:id,

                    type:type

                };

            $.ajax({

                    type: "post",

                    url: '{{ url("addlike") }}',

                    headers: {

                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')

                     },

                    data: data,

                    cache: false,

                    success: function (data)

                    {

                        console.log(data);

                        if(data.status == "success"){

                                $(element).removeClass('like').addClass('unlike').html('<i class="fas fa-heart" style="color:#e31b23"></i> &nbsp;Unlike');



                        }

                    },

                    error: function (data){

                        alert('Fail to run like..');

                    }

                });





        });

</script>

@endsection