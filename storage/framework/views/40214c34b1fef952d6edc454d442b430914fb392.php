<?php $__env->startSection('content'); ?>
<style>
    .subcte .carousel-inner>.item>img {
        height: 400px;
    }

    .card {
        background-color: #fff;
        border: 1px solid transparent;
        border-radius: 6px;
        -moz-box-shadow: 0 0 5px #888;
        -webkit-box-shadow: 0 0 5px#888;
        box-shadow: 0 0 5px #888;
    }

    .mgin1 {
        background: white;
        -moz-box-shadow: 0 0 0px #888;
        -webkit-box-shadow: 0 0 0px#888;
        box-shadow: 0 0 0px #888;
    }





    /* social icons */
    ul.socialIcons {
        padding: 0;
        text-align: center;
    }

    .socialIcons li {
        background: yellow;
        list-style: none;
        display: inline-block;
        /* margin: 15px;
    margin-top: 15%; */
        border-radius: 2em;
        overflow: hidden;
    }

    .socialIcons li a {
        display: block;
        padding: .5em;
        max-width: 2.3em;
        min-width: 2.3em;
        height: 2.3em;
        white-space: nowrap;
        line-height: 1.5em;
        transition: .5s;
        text-decoration: none;
        font-family: arial;
        color: #fff;
    }

    .socialIcons li i {
        margin-right: .5em;
    }

    .socialIcons li:hover a {
        max-width: 200px;
        padding-right: 1em;
    }

    .socialIcons .facebook {
        background: #3b5998;
        box-shadow: 0 0 16px #3b5998;
    }

    .socialIcons .twitter {
        background: #00aced;
        box-shadow: 0 0 16px #00aced;
    }

    .socialIcons .instagram {
        background: #cd486b;
        box-shadow: 0 0 16px #cd486b;
    }

    .socialIcons .pinterest {
        background: #c92228;
        box-shadow: 0 0 16px #c92228;
    }

    .socialIcons .steam {
        background: #666666;
        box-shadow: 0 0 16px #666666;
    }

    .details li {
        list-style-type: none;
    }

    body {

        background-color: #e1e9ec
    }
</style>



<div class="container">
    <div class="row">
        <div class="col-sm-12">
            <div class="subcte">
                <img src="<?php echo e(asset('main/images/32.jpeg')); ?>" alt="" style="width:100%;height:300px;max-height:300px;">
            </div>
        </div>
    </div>

    <?php
    $category_id = request()->route('id');
    $category = \App\Category::find($category_id);
    $parent_category = $category->parentCategories;
    ?>
    <nav aria-label="breadcrumb">
        <ol class="breadcrumb" id="breadcrumb">
            <li class="breadcrumb-item"><a href="/">Home</a></li>

            <li class="breadcrumb-item active" aria-current="page">shops</li>
        </ol>
    </nav>



    <h4 style="font-weight: 800;

text-align: center; ">Shops List</h4>
    <hr style="border-bottom:2px solid #df685d;width:5%;margin:0px auto;">


    <?php if(!empty($shops)): ?>
    <div class="row">
        <?php $__currentLoopData = $shops; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $item): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
        
            <div class="media mgin1">
                <div class="media-left col-md-8" style="padding:10px;">
                    <div class="media-heading">
                        <div class="name"
                            style="font-size:16px;color: #df685d;margin-top:20px;height:56px;max-height:56px;">

                            <a href="<?php echo url('/viewshop',[$item->shopid]);; ?>" style="font-size:20px;">
                                <b><?php echo e($item->shop_name); ?></b>

                            </a>

                            <span class="pull-right">
                                <?php if(Auth::guest()): ?>
                                <a class="btn" data-id="<?php echo e($item->shopid); ?>" data-toggle="modal"
                                    data-target="#myModal1"><i class="far fa-heart" style="color:#e31b23"></i></a>
                                <?php else: ?>
                                <?php if(in_array($item->shopid,$likes)): ?>
                                <a class="btn unlike" data-id="<?php echo e($item->shopid); ?>"><i class="fas fa-heart"
                                        style="color:#e31b23"></i></a>
                                <?php else: ?>
                                <a class="btn like" data-id="<?php echo e($item->shopid); ?>"> <i class="far fa-heart"
                                        style="color:#e31b23"></i></a>
                                <?php endif; ?>

                                <?php endif; ?>
                            </span>
                        </div>
                    </div>
                    <div class="details">


                        <div class="media">
                            <div class="media-left" style="margin-right:10px;">
                                <img src="<?php echo e(asset('main/images/address.png')); ?>" alt="" style="height:26px;">
                            </div>
                            <div class="media-body">
                                <p style="">
                                    <?php echo e($item->address); ?>,<?php echo e($item->city); ?>,<?php echo e($item->state); ?>,<?php echo e($item->state); ?></p>
                            </div>
                        </div>

                        <div class="media" style="margin-top:0;">
                            <div class="media-left" style="margin-right:10px;">
                                <img src="<?php echo e(asset('main/images/phoneicon.png')); ?>" alt="" style="height:20px;">
                            </div>
                            <div class="media-body">
                                <?php echo e($item->phone_number); ?>

                            </div>
                        </div>
                        <br>
                        <p style="padding-left:20px;">
                            <?php for($i = 0; $i < 5; $i++): ?> <?php if($i< (int)$item->shop_rating): ?>
                                <span class="fas fa-star" style="color:#df685d"></span>
                                <?php else: ?>
                                <?php if(($item->shop_rating-(int)$item->shop_rating) == 0.5 &&
                                ((int)$item->shop_rating) ==
                                $i ): ?>
                                <span class="fas fa-star-half-alt" style="color:#df685d"></span>
                                <?php else: ?>
                                <span class="far fa-star" style="color:#df685d"></span>
                                <?php endif; ?>
                                <?php endif; ?>
                                <?php endfor; ?>
                        </p>
                        <?php if(in_array(date('N',time())+1 , explode(",",$item->active_days))): ?>

                        <div class="media" style="margin-top:0;">
                            <div class="media-left" style="margin-right:10px;">
                                <i class="fa fa-chevron-right" style="font-size: 18px;color: #a5a5a5;"></i>
                            </div>
                            <div class="media-body">
                                <p class="">Opened </p>
                            </div>
                        </div>

                        
                        <?php else: ?>
                        <div class="media" style="margin-top:0;">
                            <div class="media-left">
                                <i class="fa fa-chevron-right" style="font-size: 18px;color: #a5a5a5;"></i>
                            </div>
                            <div class="media-body">
                                <p class="">Closed </p>
                            </div>
                        </div>

                        
                        <?php endif; ?>
                    </div>

                </div>

                <div class="media-body col-md-4 " style="width:250px;padding-bottom:0px; height: 236px">
                    <div class="" style="margin:0px 0px 0 0;;">
                        <a href="<?php echo url('/viewshop',[$item->shopid]);; ?>">
                            <img src="<?php echo asset("$item->image_path"); ?>" class="img-responsive"
                                style="width:250px;border-radius:5px;height:236px;max-height:236px;margin-top:20px;">

                        </a>
                    </div>
                </div>

            </div>

        
        <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
    </div>
    <?php else: ?>
    No Shops Found
    <?php endif; ?>
</div>
<?php $__env->stopSection(); ?>
<?php $__env->startSection('scripts'); ?>

<script>
    $(document).on('click', ".unlike", function () {
        var id = $(this).data('id');
        var element = this;

        var data = {
                shop_id:id
            };
        $.ajax({
                type: "post",
                url: '<?php echo e(url("removefavouriteshop")); ?>',
                headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                 },
                data: data,
                cache: false,
                success: function (data)
                {
                    if(data.status == "success"){
                        $(element).removeClass('unlike').addClass('like').html('<i class="far fa-heart" style="color:#e31b23"></i>');
                    }
                },
                error: function (data){
                    alert('Fail to run unlike..');
                }
            });


    });
    $(document).on('click', ".like", function () {
        var id = $(this).data('id');
        var element = this;

        var data = {
                shop_id:id
            };
        $.ajax({
                type: "post",
                url: '<?php echo e(url("addfavouriteshop")); ?>',
                headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                 },
                data: data,
                cache: false,
                success: function (data)
                {
                    console.log(data);
                    if(data.status == "success"){
                            $(element).removeClass('like').addClass('unlike').html('<i class="fas fa-heart" style="color:#e31b23"></i>');

                    }
                },
                error: function (data){
                    alert('Fail to run like..');
                }
            });


    });
</script>

<script>
    $(document).ready(function(){
        var category = JSON.parse('<?php echo json_encode($category); ?>');
        var parent_category = JSON.parse('<?php echo json_encode($parent_category); ?>');
        if(parent_category != null){
           var data =  toarray([parent_category]);
           var da = {
                    'id': category.id,
                    'category_name':category.category_name,
                    'parent_id':category.parent_id
                };

            data.unshift(da);
            data.reverse();
            breadcrumb(data);
        }else{
            var data = [];
            var da = {
                    'id': category.id,
                    'category_name':category.category_name,
                    'parent_id':category.parent_id
                };
            data.unshift(da);
            breadcrumb(data);

        }



        function toarray(data,cat){
            var categories = (cat== null) ? []: cat;
            data.forEach(element => {
                id = element.id;
                category_name = element.category_name;
                parent_id = element.parent_id;

                var data = {
                    'id': id,
                    'category_name':category_name,
                    'parent_id':parent_id
                };

                categories.push(data);
                var cat = categories;
                if(element.parent_categories) { toarray([element.parent_categories],cat); }

            });
            return categories;
        }

        function breadcrumb(data){
            data.forEach((element,key,array)=>{
                var id = element.id;
                var category_name = element.category_name.toLowerCase();
                var parent_id = element.parent_id;
                    var li = `<li class="breadcrumb-item"><a href="<?php echo e(url('subcategories')); ?>/`+id+`" style="text-transform: capitalize;">`+category_name+`</a></li>`;
                    $('#breadcrumb').find(' > li:nth-last-child(1)').before(li);

            });
        }
        });

</script>


<?php $__env->stopSection(); ?>

<?php echo $__env->make('layouts.app', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH C:\wamp64\www\offers\resources\views/shopsbycategory.blade.php ENDPATH**/ ?>