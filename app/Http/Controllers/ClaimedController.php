<?php

namespace App\Models\Http\Controllers;

use App\Http\Controllers\Controller;
use App\Models\Claimed;
use Symfony\Component\HttpFoundation\Request;

class ClaimedController extends Controller
{
    public function claimStatus(Request $request)
    {
        $id = $request->id;

        $claimed = Claimed::where('unique_code', $id)->get();

        if (0 == $claimed->used) {
            $claimed->used = 1;
        } else {
            $claimed->used = 0;
        }

        if ($claimed->save()) {
            return 'Success';
        }

        return 'Failed';
    }
}
