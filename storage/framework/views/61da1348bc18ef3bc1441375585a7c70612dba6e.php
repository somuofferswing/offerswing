<?php $__env->startSection('content'); ?>

<?php
    $deal = $order_details->deal;
    var_dump($deal);die;
?>
<section >
    <div class="ps-page--product">
        <div class="container">

            <div class="row">
                <div class="col-12 col-md-4">
                    <div class="ps-post text-center">
                        <img src="<?php echo asset("$deal->pic"); ?>">
                        <h3 class="pt-4 m-0"><?php echo e($deal->name); ?></h3>
                    </div>
                </div>
                <div class="col-12 col-md-8">
                    <div class="text-center">
                        <?php echo QrCode::size(250)->generate($order->order_id);; ?>

                        <h4>Your Deal Order: <?php echo e($order->order_id); ?></h4>
                        <div class="table-responsive">
                            <table class="table table-bordered ps-table ps-table--specification">
                                <tbody>
                                    <tr>
                                        <td>Booking id</td>
                                        <td><?php echo e($order->order_id); ?></td>
                                    </tr>
                                    <tr>
                                        <td>About Deal</td>
                                        <td><?php echo e((($deal->original_price-$deal->price)/$deal->original_price)*100); ?>% Off</td>
                                    </tr>
                                    <tr>
                                        <td>Brought on</td>
                                        <td><?php echo e(date('d-m-Y h:i A',strtotime($order_details->created_at))); ?></td>
                                    </tr>
                                    <tr>
                                        <td>Expires on</td>
                                        <td><?php echo e(date('d-m-Y h:i A',strtotime($order_details->valid_upto))); ?></td>
                                    </tr>
                                </tbody>
                            </table>
                        </div>
                    </div>
                    <?php
                        $shop = $deal->shop;
                    ?>
                    <div class="ps-product__content pt-3">
                        <h4 class="ps-product__heading">Store Address</h4>
                        <p>
                            <?php echo e($shop->address.",".$shop->pincode); ?>

                        </p>
                        <p>
                            <strong>Contact Number: </strong>
                            +91 <?php echo e($shop->phone_number.",". $shop->phone_number2); ?>

                        </p>
                    

                        <h4 class="ps-product__heading pt-3">Terms & Conditions</h4>
                        <div class="ps-document">
                            <?php echo $deal->terms_conditions; ?>

                        </div>
                    </div>

                </div>
            </div>
            
        </div>
    </div>
</section>
<?php $__env->stopSection(); ?>

<?php echo $__env->make('layouts.app', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH C:\wamp64\www\offerswing_test\resources\views/frontend/deals/orders.blade.php ENDPATH**/ ?>