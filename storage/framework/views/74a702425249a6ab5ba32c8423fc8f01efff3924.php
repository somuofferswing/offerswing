<?php $__env->startSection('content'); ?>
<div class="content-wrapper">
    <section class="content-header">
        <h1>
            Category
            <small>Add Category</small>
        </h1>
        <!-- <ol class="breadcrumb">
              <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
              <li class="active">Dashboard</li>
            </ol> -->
    </section>

    <section class="content">

        <div class="col-md-8 col-md-offset-2">
            <div class="box box-info">
                <div class="box-header with-border">
                    <h3 class="box-title">Add Category</h3>
                </div>
                <form class="form-horizontal" method="POST" action="<?php echo e(route('store-category')); ?>"
                    enctype="multipart/form-data">
                    <?php echo csrf_field(); ?>
                    <div class="box-body">

                        <div class="form-group">
                            <label for="category_name" class="col-sm-2 control-label">Category Name</label>
                            <div class="col-sm-10">
                                <input id="category_name" type="text" class="form-control" name="category_name"
                                    value="<?php echo e(old('category_name')); ?>" required autocomplete="category_name" autofocus>
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="parent_id" class="col-sm-2 control-label">Parent</label>

                            <div class="col-sm-10">
                                <select class="form-control" name="parent_id" id="parent_id">
                                    <option value="">-- Select --</option>

                                </select>
                            </div>

                        </div>
                        <div class="form-group">
                            <label for="category_image" class="col-sm-2 control-label">Image</label>
                            <div class="col-sm-10">
                                <input type="file" class="form-control-file" id="category_image" name="cat_image"
                                    onchange="loadFile(event)">
                                <img id="output" height="100px" width="100px" />
                            </div>
                        </div>
                        <button type="submit" class="btn btn-info pull-right"><?php echo e(__('Create')); ?></button>
                    </div>
                </form>
            </div>
        </div>
    </section>
</div>
<?php $__env->stopSection(); ?>
<?php $__env->startSection('scripts'); ?>

<script>
    var loadFile = function(event) {
    var output = document.getElementById('output');
    output.src = URL.createObjectURL(event.target.files[0]);
  };

  $(document).ready(function(){
        var data = <?php echo json_encode($categories); ?>;

        data.forEach(element => {
            id = element.id;
            value = element.category_name;
            $('#parent_id').append(`<option value="${id}">
                                       ${value}
                                  </option>`);
            if(element.children_recursive) { rloop(element.children_recursive,1); }
        });


        function rloop(data2,i){

            var prefix = '';
            for (let j = 0; j < i; j++) {
                    prefix += "-";
            }
            data2.forEach(element => {
            id = element.id;
            value = element.category_name;
            $('#parent_id').append(`<option value="${id}">
                                      ${prefix} ${value}
                                  </option>`);
            i++;
            if(element.children_recursive) { rloop(element.children_recursive,i); }
            });
        }

    });
</script>

<?php $__env->stopSection(); ?>

<?php echo $__env->make('layouts.admin', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH /home/offerkrx/public_html/offerswing/resources/views/category/create.blade.php ENDPATH**/ ?>