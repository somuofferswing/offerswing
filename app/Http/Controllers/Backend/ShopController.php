<?php

namespace App\Http\Controllers\Backend;

use App\Http\Controllers\Controller;
use App\Models\Amenity;
use App\Models\Category;
use App\Models\ImageUpload;
use App\Models\Shop;
use App\Models\ShopUser;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;

class ShopController extends Controller
{
    public function index()
    {
        $shops = Shop::all();

        return view('backend.shop.index', compact('shops'));
    }

    public function create()
    {
        $retailers = User::getRetailers();
        $amenities = Amenity::all();
        $categories = Category::with('childrenRecursive')->whereNull('parent_id')->get()->toArray();

        return view('backend.shop.create', compact('retailers', 'categories', 'amenities'));
    }

    public function store(Request $request)
    {
        $shop = new Shop();
        $user = new User();
        $cat = (array)$request['category'];

        $this->validate($request, [
            // 'first_name' => 'required',
            // 'last_name' => 'required',
            'owner_id' => 'required',
            'name' => 'required',
            'address' => 'required',
            'category' => 'required',
            'email' => 'required|email',
            'mobile' => 'required',
            'city' => 'required',
            'state' => 'required',
            'lat' => 'required',
            'lng' => 'required',
        ]);

        $shop->sun_start = in_array('0', $request['days']) ? date('H:i', strtotime($request['sun_start'])) : '';
        $shop->sun_end = in_array('0', $request['days']) ? date('H:i', strtotime($request['sun_end'])) : '';

        $shop->mon_start = in_array('1', $request['days']) ? date('H:i', strtotime($request['mon_start'])) : '';
        $shop->mon_end = in_array('1', $request['days']) ? date('H:i', strtotime($request['mon_end'])) : '';

        $shop->tue_start = in_array('2', $request['days']) ? date('H:i', strtotime($request['tue_start'])) : '';
        $shop->tue_end = in_array('2', $request['days']) ? date('H:i', strtotime($request['tue_end'])) : '';

        $shop->wed_start = in_array('3', $request['days']) ? date('H:i', strtotime($request['wed_start'])) : '';
        $shop->wed_end = in_array('3', $request['days']) ? date('H:i', strtotime($request['wed_end'])) : '';

        $shop->thu_start = in_array('4', $request['days']) ? date('H:i', strtotime($request['thu_start'])) : '';
        $shop->thu_end = in_array('4', $request['days']) ? date('H:i', strtotime($request['thu_end'])) : '';

        $shop->fri_start = in_array('5', $request['days']) ? date('H:i', strtotime($request['fri_start'])) : '';
        $shop->fri_end = in_array('5', $request['days']) ? date('H:i', strtotime($request['fri_end'])) : '';

        $shop->sat_start = in_array('6', $request['days']) ? date('H:i', strtotime($request['sat_start'])) : '';
        $shop->sat_end = in_array('6', $request['days']) ? date('H:i', strtotime($request['sat_end'])) : '';

        $shop->name = $request['name'];
//        $shop->categories = implode(',', $request['category']);
        $shop->description = $request['description'];
        $shop->email = $request['email'];
        $shop->phone_number = $request['mobile'];
        $shop->phone_number_2 = $request['phone_number2'];
        $shop->phone_number_3 = $request['phone_number3'];
        $shop->address = $request['address'];
        $shop->city = $request['city'];
        $shop->state = $request['state'];
        $shop->pincode = $request['pincode'];
        $shop->lat = $request['lat'];
        $shop->lng = $request['lng'];
        $shop->active_days = implode(',', $request['days']);
        $shop->shop_rating = $request['shop_rating'];

        $shop->facebook_link = $request['facebook_link'];
        $shop->twitter_link = $request['twitter_link'];
        $shop->instagram_link = $request['instagram_link'];

        $shop->payment_types = implode(',', $request['payment_types']);

        $shop->has_menus = $request['has_menus'];

        $shop->video_link = $request['video_link'];

        $slug = $this->slugify($request['name']);
        $shop->slug = $slug;

        // if ($request->hasFile('ad_image')) {
        //     $image = $request->file('ad_image');
        //     $extension = $image->getClientOriginalExtension();
        //     $timestamp = time();

        //     $image->move('images', $request['name'].'_'.$timestamp.'.'.$extension);
        //     $shop->ad_image = 'images/'.$request['name'].'_'.$timestamp.'.'.$extension;
        // }

        if ($request->hasFile('ad_image')) {
            $image = $request->file('ad_image');
            $imageName = $request['name'].'_ad_'.time().'.'.$image->getClientOriginalExtension();
            $path = $image->storeAs('shopimages', $imageName, 'public');
            $shop->ad_image = $path;
        }

        $shop->save();

        $shop_id = $shop->id;

        $categories = Category::find($cat);
        $shop->categories()->attach($categories);

        $user = User::find($request['owner_id']);
        $shop->users()->attach($user);

        $amenity = Amenity::find($request['amenities']);
        $shop->amenities()->attach($amenity);

        if (!empty($request['imageupload_ids'])) {
            $images = $request['imageupload_ids'];

            $image_ids = explode(',', $images);

            foreach ($image_ids as $id) {
                $image_upload = ImageUpload::find($id);
                $image_upload->shop_id = $shop_id;
                $image_upload->save();
            }
        }

        return redirect('/shop/create')->with('alert', 'Shop created Successfully')->with('class', 'success');
    }

    public function show($id)
    {
        $shops = Shop::select('*')
            ->join('shop_owners as so', 'so.shop_id', '=', 'shops.id')
            ->join('users as owner', 'owner.id', '=', 'so.retailer_id')
            ->where('shops.id', $id)
            ->first();

        $images = ImageUpload::where('shop_id', $id)->get();

        $shop_images = ImageUpload::where('shop_id', $id)->where('type', 'shop_image')->get();
        $shop_menus = ImageUpload::where('shop_id', $id)->where('type', 'shop_menu')->get();

        return view('backend.shop.show', compact('shops', 'shop_images', 'shop_menus', 'images'));
    }

    public function edit($id)
    {
        $retailers = User::getRetailers();
        $amenities = Amenity::all();
        $shop_categories = array();

        $shops = Shop::find($id);
        $cat = $shops->categories;
        $users = $shops->users;
        $user = $users[0];

        $all_amenities = $shops->amenities;

        foreach ($cat as $c) {
            $shop_categories[] = $c['id'];
        }

        foreach ($all_amenities as $c) {
            $shop_amenities[] = $c['id'];
        }

        $categories = Category::with('childrenRecursive')->whereNull('parent_id')->get()->toArray();


        return view('backend.shop.edit', compact('shops', 'categories', 'retailers', 'shop_categories', 'user', 'shop_amenities', 'amenities'));
    }

    public function update(Request $request, $id)
    {
        $user_id = $request['retailer_id'];

        // var_dump($request['shop_rating']);die;

        $shop = Shop::find($id);
        $user = User::find($user_id);
        $owner = ShopUser::where('shop_id', $id)->first();

        $this->validate($request, [
            // 'first_name' => 'required',
            // 'last_name' => 'required',
            'owner_id' => 'required',
            'name' => 'required',
            'address' => 'required',
            'category' => 'required',
            'email' => 'required|email',
            'mobile' => 'required',
            'city' => 'required',
            'state' => 'required',
            'lat' => 'required',
            'lng' => 'required',
        ]);

        $shop->sun_start = in_array('0', $request['days']) ? date('H:i', strtotime($request['sun_start'])) : '';
        $shop->sun_end = in_array('0', $request['days']) ? date('H:i', strtotime($request['sun_end'])) : '';

        $shop->mon_start = in_array('1', $request['days']) ? date('H:i', strtotime($request['mon_start'])) : '';
        $shop->mon_end = in_array('1', $request['days']) ? date('H:i', strtotime($request['mon_end'])) : '';

        $shop->tue_start = in_array('2', $request['days']) ? date('H:i', strtotime($request['tue_start'])) : '';
        $shop->tue_end = in_array('2', $request['days']) ? date('H:i', strtotime($request['tue_end'])) : '';

        $shop->wed_start = in_array('3', $request['days']) ? date('H:i', strtotime($request['wed_start'])) : '';
        $shop->wed_end = in_array('3', $request['days']) ? date('H:i', strtotime($request['wed_end'])) : '';

        $shop->thu_start = in_array('4', $request['days']) ? date('H:i', strtotime($request['thu_start'])) : '';
        $shop->thu_end = in_array('4', $request['days']) ? date('H:i', strtotime($request['thu_end'])) : '';

        $shop->fri_start = in_array('5', $request['days']) ? date('H:i', strtotime($request['fri_start'])) : '';
        $shop->fri_end = in_array('5', $request['days']) ? date('H:i', strtotime($request['fri_end'])) : '';

        $shop->sat_start = in_array('6', $request['days']) ? date('H:i', strtotime($request['sat_start'])) : '';
        $shop->sat_end = in_array('6', $request['days']) ? date('H:i', strtotime($request['sat_end'])) : '';

        $shop->name = $request['name'];
//        $shop->categories = implode(',', $request['category']);
        $shop->description = $request['description'];
        $shop->email = $request['email'];
        $shop->phone_number = $request['mobile'];
        $shop->phone_number_2 = $request['phone_number2'];
        $shop->phone_number_3 = $request['phone_number3'];
        $shop->address = $request['address'];
        $shop->city = $request['city'];
        $shop->state = $request['state'];
        $shop->pincode = $request['pincode'];
        $shop->lat = $request['lat'];
        $shop->lng = $request['lng'];
        $shop->active_days = implode(',', $request['days']);
        $shop->shop_rating = $request['shop_rating'];

        $shop->has_menus = $request['has_menus'];
        $shop->payment_types = implode(',', $request['payment_types']);


        $shop->facebook_link = $request['facebook_link'];
        $shop->twitter_link = $request['twitter_link'];
        $shop->instagram_link = $request['instagram_link'];
        $shop->video_link = $request['video_link'];

        if ($shop->isDirty('name')) {
            $slug = $this->slugify($request['name']);
            $shop->slug = $slug;
        } else {
            $slug = $shop->slug;
        }

        if ($request->hasFile('ad_image')) {
            Storage::disk('public')->delete($shop->ad_image);

            $image = $request->file('ad_image');
            $imageName = $request['name'].'_ad_'.time().'.'.$image->getClientOriginalExtension();
            $path = $image->storeAs('shopimages', $imageName, 'public');

            $shop->ad_image = $path;
        }

        $shop->save();

        $shop_id = $shop->id;

        $categories = Category::find($request['category']);
        $shop->categories()->sync($categories);

        $user = User::find($request['owner_id']);
        $shop->users()->sync($user);

        $amenity = Amenity::find($request['amenities']);
        $shop->amenities()->sync($amenity);


        if (!empty($request['imageupload_ids'])) {
            $images = $request['imageupload_ids'];

            $image_ids = explode(',', $images);

            foreach ($image_ids as $id) {
                $image_upload = ImageUpload::find($id);
                $image_upload->shop_id = $shop_id;
                $image_upload->save();
            }
        }

        return redirect('/shops');
    }

    public function delete($id)
    {
        $shop_owner = ShopOwner::where('shop_id', '=', $id)->first();
        $user_id = $shop_owner['retailer_id'];
        $shop_owner_id = $shop_owner['id'];

        ShopOwner::destroy($shop_owner_id);
        // User::destroy($user_id);
        Shop::destroy($id);

        return json_encode('Shop Deleted Sucessfully');
    }


    public function slugify($str)
    {
        $search = ['Ș', 'Ț', 'ş', 'ţ', 'Ş', 'Ţ', 'ș', 'ț', 'î', 'â', 'ă', 'Î', 'Â', 'Ă', 'ë', 'Ë'];
        $replace = ['s', 't', 's', 't', 's', 't', 's', 't', 'i', 'a', 'a', 'i', 'a', 'a', 'e', 'E'];
        $str = str_ireplace($search, $replace, strtolower(trim($str)));
        $str = preg_replace('/[^\w\d\-\ ]/', '', $str);
        $str = str_replace(' ', '-', $str);

        return $this->getUniqueShopSlug(preg_replace('/\-{2,}/', '-', $str));
    }

    private function getUniqueShopSlug($slug)
    {
        $existingCount = Shop::where('slug', 'like', $slug . '-%')->count();

        if ($existingCount) {
            return $slug . '-' . ($existingCount);
        }

        return $slug;
    }
}
