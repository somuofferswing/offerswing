@extends('layouts.app')

@section('content')

{{-- @php
$deal = $order->deal;
@endphp --}}
<section>
    <div class="ps-page--product">
        <div class="container">

            <div class="row">
                <div class="col-12 col-md-4">
                    <div class="ps-post text-center">
                        <img src="{{ asset('storage/'.$deal->pic) }} ">
                        <h3 class="pt-4 m-0">{{ $deal->name }}</h3>
                    </div>
                </div>
                <div class="col-12 col-md-8">
                    <div class="text-center">
                        {!! QrCode::size(250)->generate($order->order_id); !!}
                        <h4>Your Deal Order: {{ $order->order_id }}</h4>
                        <div class="table-responsive">
                            <table class="table table-bordered ps-table ps-table--specification">
                                <tbody>
                                    {{-- <tr>
                                        <td>Booking id</td>
                                        <td>{{ $claimed->unique_code}}</td>
                                    </tr> --}}
                                    <tr>
                                        <td>About Deal</td>
                                        <td>{{ (($deal->original_price-$deal->price)/$deal->original_price)*100 }}% Off</td>
                                    </tr>
                                    <tr>
                                        <td>Brought on</td>
                                        <td>{{ date('d-m-Y h:i A',strtotime($order_details->created_at)) }}</td>
                                    </tr>
                                    <tr>
                                        <td>Expires on</td>
                                        <td>{{ date('d-m-Y h:i A',strtotime($order_details->valid_upto)) }}</td>
                                    </tr>
                                </tbody>
                            </table>
                        </div>
                    </div>
                    @php
                    $shop = $deal->shop;
                    @endphp
                    <div class="ps-product__content pt-3">
                        <h4 class="ps-product__heading">Store Address</h4>
                        <p>
                            {{ $shop->address.",".$shop->pincode }}
                        </p>
                        <p>
                            <strong>Contact Number: </strong>
                            +91 {{ $shop->phone_number.",". $shop->phone_number2 }}
                        </p>

                        <h4 class="ps-product__heading pt-3">Terms & Conditions</h4>
                        <div class="ps-document">
                            {!! $deal->terms_conditions !!}
                        </div>
                    </div>

                </div>
            </div>

        </div>
    </div>
</section>
@endsection