

<?php $__env->startSection('content'); ?>
<style>
    .ps-faqs{
        padding:40px 0;
    }
    .ps-faqs .ps-section__header{
        padding-bottom:40px;
    }
</style>
<div class="ps-breadcrumb">
    <div class="container">
        <ul class="breadcrumb">
            <li><a href="<?php echo e(url('/')); ?>">Home</a></li>
            <li>FAQ's</li>
        </ul>
    </div>
</div>

<section >
    <div class="ps-faqs">
        <div class="ps-container">
            <div class="ps-section__header">
                <h1>Frequently Asked Questions</h1>
            </div>
            <div class="ps-section__content">
                <div class="row">
                    <div class="col-12 col-md-3">
                        <img src="<?php echo e(asset('main/images/icons/faq.png')); ?>">
                    </div>

                    <div class="col-12 col-md-9">
                        <div class="table-responsive">
                            <table class="table ps-table--faqs">
                                <tbody>
                                 <?php $__currentLoopData = $faqs; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $key=>$item): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                    <tr>
                                        <td class="question"><?php echo e($key+1); ?> . <?php echo e($item->question); ?></td>
                                        <td><?php echo $item->answer; ?></td>
                                    </tr>
                                 <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>

            </div>
        </div>
    </div>
    <div class="ps-call-to-action">
        <div class="ps-container">
            <h3>We’re Here to Help !<a href="<?php echo e(url('/contactus')); ?>"> Contact us</a></h3>
        </div>
    </div>
</section>
<?php $__env->stopSection(); ?>

<?php echo $__env->make('layouts.app', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH /home/offerkrx/public_html/offerswing/resources/views/faq.blade.php ENDPATH**/ ?>