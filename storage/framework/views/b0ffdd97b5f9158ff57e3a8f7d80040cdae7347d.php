<?php $__env->startSection('content'); ?>

<div class="content-wrapper">

    <section class="content-header">

        <h1>

            Wing Deals

            <small>All Wing Deals</small>

        </h1>

        <!-- <ol class="breadcrumb">

              <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>

              <li class="active">Dashboard</li>

            </ol> -->

    </section>

    <section class="content">

        <?php if(session()->get('success')): ?>

        <div class="alert alert-success" id="success">

            <?php echo e(session()->get('success')); ?>


        </div><br />

        <?php endif; ?>

        <?php if(Auth::user()->user_type == 1): ?>



                

                

                

                



                

                

                

                

                



                

                

                
                

                

                

                

                

                

                

                



                

                

                

                

                

                

                

                

                

                

                

                

                

                

                

                

                

                



                

                



        <?php endif; ?>





        <div class="box">

            <div class="box-header with-border">

                <h3 class="box-title">All deals</h3>

            </div>



            <div class="table-responsive">

                <table class="table table-striped">

                    <thead>

                        <tr>

                            <th>DealID</th>

                            <th>Name</th>

                            <th>Description</th>

                            <th>Start Date</th>

                            <th>End Date</th>

                            <th>Price</th>

                            <th colspan="2">Action</th>

                        </tr>

                    </thead>

                    <tbody>

                        <?php $__currentLoopData = $deals; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $deal): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>

                        <tr>

                            <td><?php echo e($deal->deal_id); ?></td>

                            <td><?php echo e($deal->name); ?></td>

                            <td><?php echo $deal->description; ?></td>

                            <td><?php echo e($deal->valid_from); ?></td>

                            <td><?php echo e($deal->valid_upto); ?></td>

                            <td><?php echo e($deal->price); ?></td>

                            <td><?php echo e($deal->discount_percentage); ?></td>

                            <td><?php echo e($deal->max_discount); ?></td>

                            <?php if(Auth::user()->user_type == 3): ?>

                            <td><a class="btn btn-default" href="<?php echo e(url('viewshopdeal', [$deal->deal_id])); ?>">view

                                    details</a>

                            </td>

                            

                            <?php else: ?>

                            <td><a class="btn btn-default" href="<?php echo e(route('edit-deal', [$deal->deal_id])); ?>">Edit</a>

                            </td>



                            <td>

                                <form action="<?php echo e(route('delete-deal', [$deal->deal_id])); ?>" method="post">

                                    <?php echo csrf_field(); ?>

                                    <?php echo method_field('DELETE'); ?>

                                    <button class="btn btn-danger" type="submit">Delete</button>

                                </form>

                            </td>

                            <?php endif; ?>





                        </tr>

                        <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>

                    </tbody>

                </table>

            </div>

        </div>

    </section>

</div>

<?php $__env->stopSection(); ?>

<?php $__env->startSection('scripts'); ?>

<script>

    $('#success').delay(5000).fadeOut('slow');

    $('#reset').on('click',function(){

        // $("#filter")[0].reset();

        $('#shop_id').val('');

        $('#category_id').val('');

        $('#filter')[0].submit();

    });



</script>

<?php $__env->stopSection(); ?>


<?php echo $__env->make('layouts.admin', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH C:\wamp64\www\offerswing_test\resources\views/backend/deals/index.blade.php ENDPATH**/ ?>