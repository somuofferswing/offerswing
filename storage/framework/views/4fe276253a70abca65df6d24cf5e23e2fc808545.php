<?php $__env->startSection('content'); ?>

<style>
    .img-wrap {

        position: relative;

        display: inline-block;

        /* border: 1px red solid; */

        font-size: 0;

    }


    .img-wrap .close {

        position: absolute;

        top: 2px;

        right: 2px;

        z-index: 100;

        background-color: #FFF;

        padding: 5px 2px 2px;

        color: red;

        font-weight: bold;

        cursor: pointer;

        opacity: .2;

        text-align: center;

        font-size: 22px;

        line-height: 10px;

        /* border-radius: 50%; */

        opacity: 1;

    }


    /* .img-wrap:hover .close {

            opacity: 1;

        } */
</style>

<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/dropzone/5.5.1/dropzone.css">

<link rel="stylesheet" href="<?php echo e(asset('adminlte/plugins/timepicker/bootstrap-timepicker.min.css')); ?>">



<div class="content-wrapper">

    <section class="content-header">

        <h1>

            Shop

            <small>Edit Shop</small>

        </h1>

        <!-- <ol class="breadcrumb">

                      <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>

                      <li class="active">Dashboard</li>

                    </ol> -->

    </section>


    <section class="content">

        <div class="col-md-12">

            <div class="box box-info">

                <div class="box-header with-border">
                    <h3 class="box-title">Edit Shop ( <?php echo e($shops->shop_name); ?> )</h3>

                </div>


                <div class="box-body" style="padding: 10px 50px;">

                    <form class="form-horizontal" method="POST" action="<?php echo e(route('update-shop',[$shops->id])); ?>"
                        enctype="multipart/form-data">

                        <?php echo csrf_field(); ?>

                        

                        <input type="hidden" name="retailer_id" value="<?php echo e($user->id); ?>">


                        <h3>Shop Owner Details</h3>
                        <select class="form-control" name="owner_id" id="parent_id" required>

                            <?php $__currentLoopData = $retailers; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $item): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>

                            <option value="<?php echo e($item->id); ?>" <?php if($user->id == $item->id): ?>

                                selected

                                <?php endif; ?>><?php echo e($item->first_name); ?> <?php echo e($item->last_name); ?>(<?php echo e($item->email); ?>)
                            </option>

                            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>

                        </select>


                        <h3>Shop Details</h3>


                        <hr>
                        <div class="form-group">

                            <label for="shop_name" class="col-sm-1 control-label">Shop Name</label>

                            <div class="col-sm-11">

                                <input id="shop_name" type="text" class="form-control" name="name"
                                    value="<?php echo e($shops->name); ?>" required autocomplete="shop_name" autofocus>

                            </div>

                        </div>


                        <div class="form-group">

                            <label for="email" class="col-sm-1  control-label">Email</label>

                            <div class="col-sm-11">

                                <input id="email" type="email" class="form-control" name="email"
                                    value="<?php echo e($shops->email); ?>" required autocomplete="email" autofocus>

                                <?php if ($errors->has('email')) :
if (isset($message)) { $messageCache = $message; }
$message = $errors->first('email'); ?>

                                <span class="text-danger"><?php echo e($message); ?></span>

                                <?php unset($message);
if (isset($messageCache)) { $message = $messageCache; }
endif; ?>

                            </div>


                        </div>

                        <div class="form-group">

                            <label for="mobile" class="col-sm-1  control-label">Phone</label>

                            <div class="col-sm-11">

                                <input id="mobile" type="text" class="form-control" name="mobile"
                                    value="<?php echo e($shops->phone_number); ?>" required autocomplete="mobile" autofocus>

                                <?php if ($errors->has('mobile')) :
if (isset($message)) { $messageCache = $message; }
$message = $errors->first('mobile'); ?>

                                <span class="text-danger"><?php echo e($message); ?></span>

                                <?php unset($message);
if (isset($messageCache)) { $message = $messageCache; }
endif; ?>

                            </div>


                        </div>


                        <div class="form-group">

                            <label for="mobile2" class="col-sm-1  control-label">Phone 2</label>

                            <div class="col-sm-11">

                                <input id="mobile2" type="text" class="form-control" name="phone_number2"
                                    value="<?php echo e($shops->phone_number_2); ?>" autocomplete="Phone 2" autofocus>

                                <?php if ($errors->has('mobile')) :
if (isset($message)) { $messageCache = $message; }
$message = $errors->first('mobile'); ?>

                                <span class="text-danger"><?php echo e($message); ?></span>

                                <?php unset($message);
if (isset($messageCache)) { $message = $messageCache; }
endif; ?>

                            </div>


                        </div>

                        <div class="form-group">

                            <label for="mobile3" class="col-sm-1  control-label">Whatsapp Number</label>

                            <div class="col-sm-11">

                                <input id="mobile3" type="text" class="form-control" name="phone_number3"
                                    value="<?php echo e($shops->phone_number_3); ?>" autocomplete="Whatsapp Number" autofocus>

                                <?php if ($errors->has('mobile')) :
if (isset($message)) { $messageCache = $message; }
$message = $errors->first('mobile'); ?>

                                <span class="text-danger"><?php echo e($message); ?></span>

                                <?php unset($message);
if (isset($messageCache)) { $message = $messageCache; }
endif; ?>

                            </div>


                        </div>


                        <div class="form-group">

                            <label for="shop_name" class="col-sm-1 control-label">Category</label>

                            <div class="col-sm-11">


                                <select class="form-control select2" name="category[]" id="shop_category" multiple
                                    required>

                                    <option value="">-- Select --</option>


                                </select>

                            </div>

                        </div>

                        <div class="form-group">

                            <label for="shop_description" class="col-sm-1 control-label">Shop Description</label>

                            <div class="col-sm-11">

                                <textarea id="shop_description" type="text" class="form-control" name="description"
                                    autofocus rows="6">

                                     <?php echo e($shops->description); ?>


                                    </textarea>

                            </div>

                        </div>

                        <br>
                        <div class="form-group">

                            <label for="facebook" class="col-sm-1  control-label">Facebook page link</label>

                            <div class="col-sm-11">

                                <input id="facebook" type="text" class="form-control" name="facebook_link"
                                    value="<?php echo e($shops->facebook_link); ?>" autocomplete="facebook_link" autofocus>

                                <?php if ($errors->has('facebook_link')) :
if (isset($message)) { $messageCache = $message; }
$message = $errors->first('facebook_link'); ?>

                                <span class="text-danger"><?php echo e($message); ?></span>

                                <?php unset($message);
if (isset($messageCache)) { $message = $messageCache; }
endif; ?>

                            </div>


                        </div>

                        <div class="form-group">

                            <label for="twitter" class="col-sm-1  control-label">Twitter page link</label>

                            <div class="col-sm-11">

                                <input id="twitter" type="text" class="form-control" name="twitter_link"
                                    value="<?php echo e($shops->twitter_link); ?>" autocomplete="twitter_link" autofocus>

                                <?php if ($errors->has('twitter_link')) :
if (isset($message)) { $messageCache = $message; }
$message = $errors->first('twitter_link'); ?>

                                <span class="text-danger"><?php echo e($message); ?></span>

                                <?php unset($message);
if (isset($messageCache)) { $message = $messageCache; }
endif; ?>

                            </div>


                        </div>
                        <div class="form-group">

                            <label for="instagram" class="col-sm-1  control-label">Facebook page link</label>

                            <div class="col-sm-11">

                                <input id="instagram" type="text" class="form-control" name="instagram_link"
                                    value="<?php echo e($shops->instagram_link); ?>" autocomplete="instagram_link" autofocus>

                                <?php if ($errors->has('instagram_link')) :
if (isset($message)) { $messageCache = $message; }
$message = $errors->first('instagram_link'); ?>

                                <span class="text-danger"><?php echo e($message); ?></span>

                                <?php unset($message);
if (isset($messageCache)) { $message = $messageCache; }
endif; ?>

                            </div>


                        </div>

                        <br>

                        <div class="form-group">

                            <label for="amenity" class="col-sm-1 control-label">Amenities</label>

                            <div class="col-sm-11">

                                <select class="form-control select2" name="amenities[]" id="amenity" required multiple>

                                    <option value="">-- Select --</option>
                                    <?php $__currentLoopData = $amenities; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $amenity): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                    <option value="<?php echo e($amenity->id); ?>" <?php if(in_array($amenity->id, $shop_amenities)): ?>
                                        selected <?php endif; ?> ><?php echo e($amenity->name); ?></option>
                                    <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>

                                </select>

                            </div>

                        </div>
                        <?php
                            $payments_types = explode(',',$shops->payment_types);
                            // var_dump($payments_types);die;
                        ?>

                        <div class="form-group">

                            <label for="payment" class="col-sm-1 control-label">Shop Payment Method</label>

                            <div class="col-sm-11">

                                <select class="form-control select2" name="payment_types[]" id="payment" required
                                    multiple>

                                    <option value="">-- Select --</option>
                                    <?php $__currentLoopData = config('global.payment_types'); $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $payment): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                    <option value="<?php echo e($payment['name']); ?>"
                                    <?php if(in_array($payment['name'], $payments_types)): ?>
                                        selected
                                    <?php endif; ?>
                                    ><?php echo e($payment['name']); ?></option>
                                    <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>

                                </select>

                            </div>

                        </div>


                        <div class="row">

                            <div class="col-sm-6">

                                <div class="form-group">

                                    <label for="address" class="col-sm-2 control-label">Address</label>

                                    <div class="col-sm-10">

                                        <textarea id="address" type="text" class="form-control" name="address" required
                                            autocomplete="address" autofocus>

                                            <?php echo e($shops->address); ?>


                                        </textarea>

                                    </div>

                                </div>

                                <div class="form-group">

                                    <label for="city" class="col-sm-2  control-label">City</label>

                                    <div class="col-sm-10">

                                        <input id="city" type="text" class="form-control" name="city"
                                            value="<?php echo e($shops->city); ?>" required autocomplete="city" autofocus>

                                    </div>

                                </div>

                                <div class="form-group">


                                    <label for="state" class="col-sm-2  control-label">State</label>

                                    <div class="col-sm-10">

                                        <input id="state" type="text" class="form-control" name="state"
                                            value="<?php echo e($shops->state); ?>" required autocomplete="state" autofocus>

                                    </div>


                                </div>

                                <div class="form-group">

                                    <label for="pincode" class="col-sm-2  control-label">Pincode</label>

                                    <div class="col-sm-10">

                                        <input id="pincode" type="text" class="form-control" name="pincode"
                                            value="<?php echo e($shops->pincode); ?>" required autocomplete="pincode" autofocus>

                                    </div>

                                </div>

                                <div class="form-group">

                                    <label for="lat" class="col-sm-2  control-label">Latitude</label>

                                    <div class="col-sm-10">

                                        <input id="lat" type="text" class="form-control" name="lat"
                                            value="<?php echo e($shops->lat); ?>" required autocomplete="lat" autofocus>

                                    </div>

                                </div>

                                <div class="form-group">

                                    <label for="lng" class="col-sm-2  control-label">Logitude</label>

                                    <div class="col-sm-10">

                                        <input id="lng" type="text" class="form-control" name="lng"
                                            value="<?php echo e($shops->lng); ?>" required autocomplete="lng" autofocus>

                                    </div>

                                </div>

                            </div>


                            <div class="col-sm-6">

                                <div class="form-group">

                                    <label for="lng" class="col-sm-2  control-label">Active days</label>

                                    <?php

                                    $values = explode(',', $shops->active_days);

                                    ?>

                                    <div class="col-sm-10">

                                        <div class="checkbox">

                                            <label class="col-md-4">

                                                <input type="checkbox" name="days[]" value="1" <?php if(in_array(1,$values)): ?>
                                                    checked <?php endif; ?>>Monday

                                            </label>

                                            <div class="col-md-8">

                                                <input type="text" class="col-md-5 time 1" <?php if(!in_array(1,$values)): ?>
                                                    disabled <?php endif; ?> value="<?php echo e($shops->mon_start); ?>"
                                                    name="mon_start">&nbsp;&nbsp;

                                                <input type="text" class="col-md-5 time 1" <?php if(!in_array(1,$values)): ?>
                                                    disabled <?php endif; ?> value="<?php echo e($shops->mon_end); ?>" name="mon_end">

                                            </div>

                                        </div>

                                        <div class="checkbox">

                                            <label class="col-md-4">

                                                <input type="checkbox" name="days[]" value="2" <?php if(in_array(3,$values)): ?>
                                                    checked <?php endif; ?>>Tuesday

                                            </label>

                                            <div class="col-md-8">

                                                <input type="text" class="col-md-5 time 2" <?php if(!in_array(2,$values)): ?>
                                                    disabled <?php endif; ?> value="<?php echo e($shops->tue_start); ?>"
                                                    name="tue_start">&nbsp;&nbsp;

                                                <input type="text" class="col-md-5 time 2" <?php if(!in_array(2,$values)): ?>
                                                    disabled <?php endif; ?> value="<?php echo e($shops->tue_end); ?>" name="tue_end">

                                            </div>

                                        </div>

                                        <div class="checkbox">

                                            <label class="col-md-4">

                                                <input type="checkbox" name="days[]" value="3" <?php if(in_array(3,$values)): ?>
                                                    checked <?php endif; ?>>Wednesday

                                            </label>

                                            <div class="col-md-8">

                                                <input type="text" class="col-md-5 time 3" <?php if(!in_array(3,$values)): ?>
                                                    disabled <?php endif; ?> value="<?php echo e($shops->wed_start); ?>"
                                                    name="wed_start">&nbsp;&nbsp;

                                                <input type="text" class="col-md-5 time 3" <?php if(!in_array(3,$values)): ?>
                                                    disabled <?php endif; ?> value="<?php echo e($shops->wed_end); ?>" name="wed_end">

                                            </div>

                                        </div>

                                        <div class="checkbox">

                                            <label class="col-md-4">

                                                <input type="checkbox" name="days[]" value="4" <?php if(in_array(4,$values)): ?>
                                                    checked <?php endif; ?>>Thursday

                                            </label>

                                            <div class="col-md-8">

                                                <input type="text" class="col-md-5 time 4" <?php if(!in_array(4,$values)): ?>
                                                    disabled <?php endif; ?> value="<?php echo e($shops->thu_start); ?>"
                                                    name="thu_start">&nbsp;&nbsp;

                                                <input type="text" class="col-md-5 time 4" <?php if(!in_array(4,$values)): ?>
                                                    disabled <?php endif; ?> value="<?php echo e($shops->thu_end); ?>" name="thu_end">

                                            </div>

                                        </div>

                                        <div class="checkbox">

                                            <label class="col-md-4">

                                                <input type="checkbox" name="days[]" value="5" <?php if(in_array(5,$values)): ?>
                                                    checked <?php endif; ?>>Friday

                                            </label>

                                            <div class="col-md-8">

                                                <input type="text" class="col-md-5 time 5" <?php if(!in_array(5,$values)): ?>
                                                    disabled <?php endif; ?> value="<?php echo e($shops->fri_start); ?>"
                                                    name="fri_start">&nbsp;&nbsp;

                                                <input type="text" class="col-md-5 time 5" <?php if(!in_array(5,$values)): ?>
                                                    disabled <?php endif; ?> value="<?php echo e($shops->fri_end); ?>" name="fri_end">

                                            </div>

                                        </div>

                                        <div class="checkbox">

                                            <label class="col-md-4">

                                                <input type="checkbox" name="days[]" value="6" <?php if(in_array(6,$values)): ?>
                                                    checked <?php endif; ?>>Saturday

                                            </label>

                                            <div class="col-md-8">

                                                <input type="text" class="col-md-5 time 6" <?php if(!in_array(6,$values)): ?>
                                                    disabled <?php endif; ?> value="<?php echo e($shops->sat_start); ?>"
                                                    name="sat_start">&nbsp;&nbsp;

                                                <input type="text" class="col-md-5 time 6" <?php if(!in_array(6,$values)): ?>
                                                    disabled <?php endif; ?> value="<?php echo e($shops->sat_end); ?>" name="sat_end">

                                            </div>

                                        </div>

                                        <div class="checkbox">

                                            <label class="col-md-4">

                                                <input type="checkbox" name="days[]" value="0" <?php if(in_array(1,$values)): ?>
                                                    checked <?php endif; ?>>Sunday

                                            </label>

                                            <div class="col-md-8">

                                                <input type="text" class="col-md-5 time 0" <?php if(!in_array(0,$values)): ?>
                                                    disabled <?php endif; ?> value="<?php echo e($shops->sun_start); ?>"
                                                    name="sun_start">&nbsp;&nbsp;

                                                <input type="text" class="col-md-5 time 0" <?php if(!in_array(0,$values)): ?>
                                                    disabled <?php endif; ?> value="<?php echo e($shops->sun_end); ?>" name="sun_end">

                                            </div>

                                        </div>

                                    </div>

                                </div>

                                <div class="form-group">

                                    <label for="lng" class="col-sm-2  control-label">Rating</label>

                                    <div class="col-sm-10">

                                        <input type="number" class="form-control " name="shop_rating" min="1" max=5
                                            value="<?php echo e($shops->shop_rating); ?>" step="0.5">

                                    </div>

                                </div>


                            </div>

                        </div>


                        

                        

                        


                        <hr>

                        <br><br><br>

                        <div class="form-group row" style="overflow-x: auto; white-space: nowrap">

                            <?php if($shops->shop_images): ?>

                            <?php $__currentLoopData = $shops->shop_images; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $image): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>

                            <div class="img-wrap">

                                <span class="close" data-id="<?php echo e($image->id); ?>" data-name="<?php echo e($image->image_name); ?>"
                                    data-url="<?php echo e(route('delete-image', [$image->id])); ?>">&times;</span>

                                <img class="img-thumbnail" src='<?php echo asset("$image->image_path")?>'
                                    alt="Another alt text" style="max-width: 200px">

                            </div>



                            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>

                            <?php endif; ?>

                        </div>

                        <h3>Upload Shop Images</h3>

                        <div class="form-group" style="margin:0px;">

                            <div class="dropzone" id="mydropzone">

                            </div>

                        </div>

                        <hr>


                        <div>

                            <div class="form-group">

                                <label for="bank_name" class="col-sm-2 control-label">Menus Present:</label>

                                <div class="col-sm-10">

                                    <input type="radio" name="has_menus" value="1" <?php if($shops->has_menus == 1): ?>

                                    checked

                                    <?php endif; ?>>Yes

                                    <input type="radio" name="has_menus" value="0" <?php if($shops->has_menus == 0): ?>

                                    checked

                                    <?php endif; ?>>No

                                </div>

                            </div>

                            <div class="form-group row" style="overflow-x: auto; white-space: nowrap">

                                <?php if($shops->shop_menus): ?>

                                <?php $__currentLoopData = $shops->shop_menus; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $image): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>

                                <div class="img-wrap">

                                    <span class="close" data-id="<?php echo e($image->id); ?>" data-name="<?php echo e($image->image_name); ?>"
                                        data-url="<?php echo e(route('delete-image', [$image->id])); ?>">&times;</span>

                                    <img class="img-thumbnail" src='<?php echo asset("$image->image_path")?>'
                                        alt="Another alt text" style="max-width: 200px">

                                </div>



                                <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>



                                <?php endif; ?>


                            </div>


                            <div id="shop_menus" style="display:none">

                                <h3>Upload Shop Menus</h3>

                                <div class="form-group" style="margin:0px;">

                                    <div class="dropzone" id="mydropzone2">

                                    </div>

                                </div>

                            </div>

                        </div>

                        <hr>

                        <div class="form-group">
                            <label for="ad_image" class="col-sm-2 control-label">Ad Image</label>
                            <div class="col-sm-10">
                                <input type="file" class="form-control-file" id="ad_image" name="ad_image"
                                    onchange="loadFile(event)">
                                <img id="output" height="100px" width="100px"
                                    src='<?php echo asset("$shops->ad_image"); ?>' />
                            </div>
                        </div>

                        <div class="form-group">

                            <label for="video_link" class="col-sm-1  control-label">Video Link</label>

                            <div class="col-sm-11">

                                <input id="video_link" type="text" class="form-control" name="video_link"
                                    value="<?php echo e($shops->video_link); ?>" autocomplete="video_link" autofocus>

                                <?php if ($errors->has('video_link')) :
if (isset($message)) { $messageCache = $message; }
$message = $errors->first('video_link'); ?>

                                <span class="text-danger"><?php echo e($message); ?></span>

                                <?php unset($message);
if (isset($messageCache)) { $message = $messageCache; }
endif; ?>

                            </div>


                        </div>

                        <hr>

                        <div class=" pull-right">

                            <button type="submit" class="btn btn-info "><?php echo e(__('Edit Shop')); ?></button>

                            <a href="<?php echo e(url()->previous()); ?>" class="btn btn-danger"><?php echo e(__('Cancel')); ?></a>


                        </div>


                    </form>

                </div>

            </div>

        </div>

    </section>

</div>

<?php $__env->stopSection(); ?>



<?php $__env->startSection('scripts'); ?>
<script>
    var loadFile = function(event) {
            var output = document.getElementById('output');
            output.src = URL.createObjectURL(event.target.files[0]);
        };
</script>

<script src="http://maps.googleapis.com/maps/api/js?key=AIzaSyBHLFZ4C3XB7MDwjywzpBC1ZWMH0u2zuE4&amp;libraries=places">

</script>
<script src="<?php echo e(asset('adminlte/bower_components/select2/dist/js/select2.full.min.js')); ?>"></script>

<script src="https://cdnjs.cloudflare.com/ajax/libs/dropzone/5.5.1/dropzone.js"></script>

<script src="<?php echo e(asset('adminlte/plugins/timepicker/bootstrap-timepicker.min.js')); ?>"></script>

<script src="<?php echo e(asset('adminlte/bower_components/ckeditor/ckeditor.js')); ?>"></script>

<script>
    // function init() {

        //     var input = document.getElementById('address');

        //     var autocomplete = new google.maps.places.Autocomplete(input);

        // }

        // google.maps.event.addDomListener(window, 'load', init);


        $(document).ready(function () {
            $('.select2').select2();

            var data = <?php echo json_encode($categories); ?>;


            var shop_category = '<?php echo e(json_encode($shop_categories)); ?>';


            data.forEach(element => {

                var id = element.id;
                var value = element.category_name;

                if (shop_category.includes(id)) {
                    $('#shop_category').append(`<option value="${id}" selected>
                                     ${value} </option>`);
                } else {
                    $('#shop_category').append(`<option value="${id}">${value} </option>`);

                }


                if (element.children_recursive) {
                    rloop(element.children_recursive, 1);
                }

            });


            function rloop(data2, i) {


                var prefix = '';

                for (let j = 0; j < i; j++) {

                    prefix += "-";

                }

                data2.forEach(element => {

                    var id = element.id;

                    var value = element.category_name;

                    if (shop_category.includes(id)) {

                        $('#shop_category').append(`<option value="${id}" selected>

                                      ${prefix} ${value} </option>`);


                    } else {

                        $('#shop_category').append(`<option value="${id}">

                                      ${prefix} ${value} </option>`);

                    }


                    i++;

                    if (element.children_recursive) {
                        rloop(element.children_recursive, i);
                    }

                });

            }


        });


        $('input[type="checkbox"]').click(function () {

            var checkbox_value = $(this).val();

            if ($(this).prop("checked") == true) {

                $('.' + checkbox_value).attr('disabled', false);

            } else {

                $('.' + checkbox_value).attr('disabled', true);

            }

        });


        $(function () {

            CKEDITOR.replace('shop_description')

        })

</script>

<script type="text/javascript">
    Dropzone.autoDiscover = false;

        var myDropzone = new Dropzone("div#mydropzone", {

            url: "<?php echo e(url('image/upload/store')); ?>",

            params: {'type': 'shop_image'},

            headers: {

                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')

            },

            maxFiles: 10,

            maxFilesize: 12,

            renameFile: function (file) {

                let dt = new Date();

                let time = dt.getTime();

                return time + file.name;

            },

            init: function () {

                this.on("sending", function (file, xhr, formData) {

                    formData.append("shop_id", "<?php echo e($shops->shop_id); ?>");

                    console.log(formData)

                });

            },

            acceptedFiles: ".jpeg,.jpg,.png,.gif",

            addRemoveLinks: true,

            removedfile: function (file) {

                let name = file.upload.filename;

                $.ajax({

                    headers: {

                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')

                    },

                    type: 'POST',

                    url: '<?php echo e(url("image/delete")); ?>',

                    data: {filename: name},

                    success: function (data) {

                        console.log("File has been successfully removed!!");

                    },

                    error: function (e) {

                        console.log(e);

                    }
                });

                let fileRef;

                return (fileRef = file.previewElement) != null ?

                    fileRef.parentNode.removeChild(file.previewElement) : void 0;

            },


            success: function (file, response) {

                console.log(response);

                let val = $('#images_id').val();

                if (val == '') {

                    val = response.id;

                } else {

                    val = val + "," + response.id;

                }

                $('#images_id').val(val);

            },

            error: function (file, response) {

                return false;

            }

        });


        var myDropzone1 = new Dropzone("div#mydropzone2", {

            url: "<?php echo e(url('image/upload/store')); ?>",

            params: {'type': 'shop_menu'},

            headers: {

                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')

            },

            maxFiles: 10,

            maxFilesize: 12,

            renameFile: function (file) {

                let dt = new Date();

                let time = dt.getTime();

                return time + file.name;

            },

            init: function () {

                this.on("sending", function (file, xhr, formData) {

                    formData.append("shop_id", "<?php echo e($shops->shop_id); ?>");

                    console.log(formData)

                });

            },

            acceptedFiles: ".jpeg,.jpg,.png,.gif",

            addRemoveLinks: true,

            removedfile: function (file) {

                let name = file.upload.filename;

                $.ajax({

                    headers: {

                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')

                    },

                    type: 'POST',

                    url: '<?php echo e(url("image/delete")); ?>',

                    data: {filename: name},

                    success: function (data) {

                        console.log("File has been successfully removed!!");

                    },

                    error: function (e) {

                        console.log(e);

                    }
                });

                let fileRef;

                return (fileRef = file.previewElement) != null ?

                    fileRef.parentNode.removeChild(file.previewElement) : void 0;

            },


            success: function (file, response) {

                console.log(response);

                let val = $('#images_id').val();

                if (val == '') {

                    val = response.id;

                } else {

                    val = val + "," + response.id;

                }

                $('#images_id').val(val);

            },

            error: function (file, response) {

                return false;

            }

        });


        $('.img-wrap .close').on('click', function () {

            var id = $(this).data('id');

            var name = $(this).data('name');

            var url = $(this).data('url');

            alert('remove picture: ' + name);

            $.ajax({

                headers: {

                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')

                },

                type: 'POST',

                url: url,

                data: {id: id, filename: name},

                success: function (data) {

                    console.log(data);

                    console.log("File has been successfully removed!!");

                    window.location.reload();

                },

                error: function (e) {

                    console.log(e);

                }
            });

        });

</script>

<script>
    $('.time').timepicker({

            showInputs: false

        });

        $('input:radio[name=has_menus]').change(function () {

            if (this.value == 1) {

                // alert("Allot Thai Gayo Bhai");

                $('#shop_menus').show();

            } else if (this.value == 0) {

                // alert("Transfer Thai Gayo");


                $('#shop_menus').val('');

                $('#shop_menus').hide();

            }

        });


        $(document).ready(function () {

            var radio = $("input:radio[name=has_menus]:checked").val();


            if (radio == 1) {

                // alert("Allot Thai Gayo Bhai");

                $('#shop_menus').show();

            } else if (radio == 0) {

                // alert("Transfer Thai Gayo");


                $('#shop_menus').val('');

                $('#shop_menus').hide();

            }

        });

</script>

<?php $__env->stopSection(); ?>

<?php echo $__env->make('layouts.admin', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH /home/offerkrx/public_html/offerswing_test/resources/views/backend/shop/edit.blade.php ENDPATH**/ ?>