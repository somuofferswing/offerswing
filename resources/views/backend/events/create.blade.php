@extends('layouts.admin')

@section('content')
<link rel="stylesheet"
    href="{{ asset('adminlte/bower_components/bootstrap-datepicker/dist/css/bootstrap-datepicker.min.css') }}">
<link rel="stylesheet" href="{{ asset('adminlte/plugins/timepicker/bootstrap-timepicker.min.css') }}">
<div class="content-wrapper">
    <section class="content-header">
        <h1>
            Events
            <small>Add Event</small>
        </h1>
        <!-- <ol class="breadcrumb">
              <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
              <li class="active">Dashboard</li>
            </ol> -->
    </section>

    <section class="content">

        <div class="box box-info">
            <div class="box-header with-border">
                <h3 class="box-title">Add Event</h3>
            </div>
            <form class="form-horizontal" method="POST" action="{{ route('store-events') }}"
                enctype="multipart/form-data">
                @csrf
                <h3>Event owner </h3>
                <select class="form-control" name="vendor_id" id="parent_id" required>
                    @foreach ($retailers as $item)
                    <option value="{{ $item->id }}">{{ $item->first_name }} {{ $item->last_name }}({{ $item->email }})
                    </option>
                    @endforeach
                </select>
                <hr>
                <h3>Event Details</h3>
                <div class="box-body">
                    <div class="form-group ">
                        <label for="title" class="col-sm-2 control-label">Event Name/Title</label>
                        <div class="col-sm-10">
                            <input id="name" type="text" class="form-control" name="title" value="{{ old('title') }}"
                                required>
                            @if ($errors->has('title'))
                            <div class="danger">{{ $errors->first('title') }}</div>
                            @endif
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="event_image" class="col-sm-2 control-label">Image</label>
                        <div class="col-sm-10">
                            <input type="file" class="form-control-file" id="event_image" name="event_image"
                                onchange="loadFile(event)" required>
                            <img id="output" height="100px" width="100px" />
                        </div>
                    </div>
                    <div class="form-group ">
                        <label for="description" class="col-sm-2 control-label">Description</label>
                        <div class="col-sm-10">
                            <textarea id="description" type="text" class="form-control" name="description" required
                                autocomplete="description" autofocus>{{ old('description') }}</textarea>
                            @if ($errors->has('description'))
                            <div class="danger">{{ $errors->first('description') }}</div>
                            @endif
                        </div>
                    </div>

                    <div class="form-group ">
                        <label for="terms_conditions" class="col-sm-2 control-label">Terms & Conditions</label>
                        <div class="col-sm-10">
                            <textarea id="terms_conditions" class="form-control" name="terms_conditions" required
                                autocomplete="description" autofocus>{{ old('terms_conditions') }}</textarea>
                            @if ($errors->has('terms_conditions'))
                            <div class="danger">{{ $errors->first('terms_conditions') }}</div>
                            @endif
                        </div>
                    </div>

                    <div id="map" style="height:400px;width:100%">
                    </div>

                    <div class="form-group ">
                        <label for="event_address" class="col-sm-2 control-label">Address</label>
                        <div class="col-sm-10">
                            <textarea id="address" class="form-control" name="event_address" required
                                autocomplete="description" autofocus>{{ old('event_description') }}</textarea>
                            @if ($errors->has('event_address'))
                            <div class="danger">{{ $errors->first('event_address') }}</div>
                            @endif
                        </div>
                    </div>



                    <div class="form-group ">
                        <label for="event_lat" class="col-sm-2 control-label">Lat</label>
                        <div class="col-sm-10">
                            <input id="lat" type="text" class="form-control" name="event_lat" required
                                autocomplete="event_lat" value='{{ old('event_lat') }}' autofocus>
                            @if ($errors->has('event_lat'))
                            <div class="danger">{{ $errors->first('event_lat') }}</div>
                            @endif
                        </div>
                    </div>

                    <div class="form-group ">
                        <label for="event_lng" class="col-sm-2 control-label">lng</label>
                        <div class="col-sm-10">
                            <input id="lng" type="text" class="form-control" name="event_lng" required
                                autocomplete="description" value='{{ old('event_lng') }}' autofocus>
                            @if ($errors->has('event_lng'))
                            <div class="danger">{{ $errors->first('event_lng') }}</div>
                            @endif
                        </div>
                    </div>
                    <div class="form-group ">
                        <label for="event_date" class="col-sm-2 control-label">Date</label>
                        <div class="col-sm-10">
                            <input id="event_date" type="text" class="form-control" name="event_date" required
                                autocomplete="description" value='{{ old('event_date') }}' autofocus>
                            @if ($errors->has('event_date'))
                            <div class="danger">{{ $errors->first('event_date') }}</div>
                            @endif
                        </div>
                    </div>
                    <div class="form-group ">
                        <label for="event_time" class="col-sm-2 control-label">time</label>
                        <div class="col-sm-10">
                            <input id="event_time" type="text" class="form-control" name="event_time" required
                                autocomplete="description" value='{{ old('event_date') }}'>
                            @if ($errors->has('event_time'))
                            <div class="danger">{{ $errors->first('event_time') }}</div>
                            @endif
                        </div>
                    </div>
                    <div class="form-group ">
                        <label for="payment_type" class="col-sm-2 control-label">Payment Type</label>
                        <div class="col-sm-10">
                            <select class="form-control" id="payment_type" name="payment_type" required autofocus>
                                <option value="cash">Cash</option>
                                <option value="wingpoints">Wing Points</option>
                            </select>
                            @if ($errors->has('payment_type'))
                            <div class="danger">{{ $errors->first('payment_type') }}</div>
                            @endif
                        </div>
                    </div>
                    <div class="form-group points_to_claim" style="display: none">
                        <label for="points_to_claim" class="col-sm-2 control-label">Wing points to claim</label>
                        <div class="col-sm-10">
                            <input id="points_to_claim" type="text" class="form-control" name="points_to_claim"
                                value="{{ old('points_to_claim') }}" autofocus>
                            @if ($errors->has('points_to_claim'))
                            <div class="danger">{{ $errors->first('points_to_claim') }}</div>
                            @endif
                        </div>
                    </div>
                    <div class="form-group event_price">
                        <label for="event_price" class="col-sm-2 control-label">Price</label>
                        <div class="col-sm-10">
                            <input id="event_price" type="text" class="form-control" name="event_price" 
                                autocomplete="description" value='{{ old('event_price') }}'>
                            @if ($errors->has('event_price'))
                            <div class="danger">{{ $errors->first('event_price') }}</div>
                            @endif
                        </div>
                    </div>
                    <div class="form-group ">
                        <label for="quantity" class="col-sm-2 control-label">Quantity</label>
                        <div class="col-sm-10">
                            <input id="quantity" type="text" class="form-control" name="quantity" required
                                autocomplete="quantity" value='{{ old('quantity') }}' autofocus>
                            @if ($errors->has('quantity'))
                            <div class="danger">{{ $errors->first('quantity') }}</div>
                            @endif
                        </div>
                    </div>
                    <div class="form-group ">
                        <label for="date_time_description" class="col-sm-2 control-label">Price & Time
                            Description</label>
                        <div class="col-sm-10">
                            <textarea id="terms_condidate_time_descriptiontions" class="form-control"
                                name="date_time_description" autocomplete="description"
                                autofocus>{{ old('date_time_description') }}</textarea>
                            @if ($errors->has('terms_conditions'))
                            <div class="danger">{{ $errors->first('date_time_description') }}</div>
                            @endif
                        </div>
                    </div>

                    <div class="pull-right">
                        <button type="submit" class="btn btn-info">{{ __('Create') }}</button>
                        <button type="button" class="btn btn-danger"
                            onclick="window.history.go(-1); return false;">{{ __('Cancel') }}</button>
                    </div>
                </div>
            </form>
        </div>
    </section>
</div>
@endsection
@section('scripts')

<script src="{{ asset('adminlte/bower_components/bootstrap-datepicker/dist/js/bootstrap-datepicker.min.js') }}">
</script>
<script src="{{ asset('adminlte/bower_components/ckeditor/ckeditor.js') }}"></script>
<script src="{{ asset('adminlte/plugins/timepicker/bootstrap-timepicker.min.js') }}"></script>
<script>
    $(function () {
    CKEDITOR.replace('description');
    CKEDITOR.replace('terms_conditions');
    CKEDITOR.replace('date_time_description');
  })
</script>
<script>
    var loadFile = function(event) {
            var output = document.getElementById('output');
            output.src = URL.createObjectURL(event.target.files[0]);
          };
          $(document).ready(function() {
            var date = new Date();
            $('#event_date').datepicker({
                autoclose: true,
                format: 'yyyy-mm-dd',
                startDate: new Date(),
            });

            $('#event_time').timepicker({
            showInputs: false
        });
          });

    function initMap() {
       var  map = new google.maps.Map(document.getElementById('map'), {
          center: {lat: 18.6177, lng: 84.0167},
          zoom: 6
        });
        var input = document.getElementById('address');
        var autocomplete = new google.maps.places.Autocomplete(input);

        autocomplete.bindTo('bounds', map);

        // Set the data fields to return when the user selects a place.
        autocomplete.setFields(
            ['address_components', 'geometry', 'icon', 'name']);

        var infowindow = new google.maps.InfoWindow();
        var infowindowContent = document.getElementById('infowindow-content');
        infowindow.setContent(infowindowContent);
        var marker = new google.maps.Marker({
          map: map,
          anchorPoint: new google.maps.Point(0, -29)
        });

        autocomplete.addListener('place_changed', function() {
          infowindow.close();
          marker.setVisible(false);
          var place = autocomplete.getPlace();
          console.log(place);
          if (!place.geometry) {
            window.alert("No details available for input: '" + place.name + "'");
            return;
          }

          if (place.geometry.viewport) {
            map.fitBounds(place.geometry.viewport);
          } else {
            map.setCenter(place.geometry.location);
            map.setZoom(17);
          }
          marker.setPosition(place.geometry.location);
          marker.setVisible(true);
          $('#lat').val(place.geometry.location.lat());
          $('#lng').val(place.geometry.location.lng());

          var address = '';
          if (place.address_components) {
            address = [
              (place.address_components[0] && place.address_components[0].short_name || ''),
              (place.address_components[1] && place.address_components[1].short_name || ''),
              (place.address_components[2] && place.address_components[2].short_name || '')
            ].join(' ');
          }

          infowindowContent.children['place-icon'].src = place.icon;
          infowindowContent.children['place-name'].textContent = place.name;
          infowindowContent.children['place-address'].textContent = address;
          infowindow.open(map, marker);

          $.each(place.address_components, function (i, address_component) {
            if (address_component.types[0] == "locality"){
                $("#city").val(address_component.long_name);
            }

            if (address_component.types[0] == "administrative_area_level_1"){
                $("#state").val(address_component.long_name);
            }

            if (address_component.types[0] == "postal_code"){
                $("#pincode").val(address_component.long_name);
            }
          });

      });
    }
</script>
<script
    src="http://maps.googleapis.com/maps/api/js?key=AIzaSyBHLFZ4C3XB7MDwjywzpBC1ZWMH0u2zuE4&libraries=places&callback=initMap"
    async defer>
</script>
<script>
    $('#payment_type').on('change',function(){
            var type = $(this).val();
            if(type == 'wingpoints'){
                $('.points_to_claim').show();
                $('.event_price').hide();
            }else{
                $('.points_to_claim').hide();
                $('.event_price').show();
            }

        });
</script>
@endsection
