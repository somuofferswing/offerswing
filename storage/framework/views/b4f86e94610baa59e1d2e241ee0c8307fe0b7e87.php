<?php $__env->startSection('content'); ?>
<style>
.image_profile{
    max-height:300px;
    border-radius:4px;
}

.imageClass {
    position: absolute;
    top: 20px;
    right: 15%;
    z-index: 20;
    width: 40px;
    height: 40px;
    display: -webkit-box;
    display: flex;
    -webkit-box-orient: horizontal;
    -webkit-box-direction: normal;
    flex-flow: row nowrap;
    -webkit-box-pack: center;
    justify-content: center;
    -webkit-box-align: center;
    align-items: center;
    background-color: var(--main-bg-color);
    border-radius:50%;
}

.modalHeader{
    font-size: 26px;
    font-weight: bold;
    text-align:center;
    color:#000;
}

</style>
<div class="ps-breadcrumb">
    <div class="container">
        <ul class="breadcrumb text-capitalize" id="breadcrumb">
            <li><a href="<?php echo e(url('/')); ?>">Home</a></li>
            <li>My Profile</li>
        </ul>
    </div>
</div>

<section>
    <div class="ps-page--blog">
        <div class="container">
            <div class="ps-page__header p-5">
                <h1 class="text-center">My Profile</h1>
                <hr class="highlighter">
            </div>

            <div class="row pb-30">
                <div class="col-12 col-md-4 text-center">
                    <?php if(empty($user->profile_image)): ?>
                        <img class="image_profile shadow" src="<?php echo e(asset('main/images/avatar.png')); ?>" alt="">
                   <?php else: ?>
                        <img class="image_profile shadow" src="<?php echo asset("$user->profile_image"); ?>" alt="">
                   <?php endif; ?>
                   <div class="imageClass">
                        <input type="file" class="d-none" name="profile_image" id="profile_image" accept="image/*" onchange="readURL(this);" />
                        <span class="icon-pencil5 text-white" onclick="chooseFile();" style="cursor:pointer;" data-toggle="tooltip" data-original-title="edit pic"></span>
                   </div>
                </div>
                <div class="col-12 col-md-8">
                    <form id="updateprofile" action="#">
                        <div class="row">
                            
                            <div class="col-xl-6 col-lg-6 col-md-6 col-sm-12 col-12 ">
                                <div class="form-group">
                                    <label>First Name *</label>
                                    <div class="form-group__content">
                                        <input class="form-control" type="text" id="fname" name="first_name" readOnly value="<?php echo e($user->first_name); ?>">
                                    </div>
                                </div>
                            </div>

                            <div class="col-xl-6 col-lg-6 col-md-6 col-sm-12 col-12 ">
                                <div class="form-group">
                                    <label>Last Name *</label>
                                    <div class="form-group__content">
                                        <input class="form-control" type="text" name="last_name" readOnly value="<?php echo e($user->last_name); ?>">
                                    </div>
                                </div>
                            </div>

                            <div class="col-xl-6 col-lg-6 col-md-6 col-sm-12 col-12 ">
                                <div class="form-group">
                                    <label>Email *</label>
                                    <div class="form-group__content">
                                        <input class="form-control" type="email" name="email" readOnly value="<?php echo e($user->email); ?>">
                                    </div>
                                </div>
                            </div>

                            <div class="col-xl-6 col-lg-6 col-md-6 col-sm-12 col-12 ">
                                <div class="form-group">
                                    <label>Mobile *</label>
                                    <div class="form-group__content">
                                        <input class="form-control" type="text" name="mobile" readOnly value="<?php echo e($user->mobile); ?>">
                                    </div>
                                </div>
                            </div>

                            <div class="col-xl-6 col-lg-6 col-md-6 col-sm-12 col-12 ">
                                <div class="form-group">
                                    <label>Date Of Birth *</label>
                                    <div class="form-group__content">
                                        <input class="form-control datepicker" type="text" name="dob" readOnly value="<?php echo e($user->dob); ?>">
                                    </div>
                                </div>                      
                            </div>

                            <div class="col-xl-6 col-lg-6 col-md-6 col-sm-12 col-12 ">
                                <div class="form-group">
                                    <label>Gender *</label>
                                    <div class="form-group__content">
                                        <div class="form-check form-check-inline ps-radio">
                                            <input class="form-check-input" type="radio" id="profile_gender_male" name="gender" value="MALE" <?php echo e(($user->gender == "MALE") ? 'checked' : ''); ?> disabled>
                                            <label class="form-check-label" for="profile_gender_male">Male</label>
                                        </div>
                                
                                        <div class="form-check form-check-inline ps-radio">
                                            <input class="form-check-input" type="radio" name="gender" id="profile_gender_female" value="FEMALE" <?php echo e(($user->gender == "FEMALE") ? 'checked' : ''); ?> disabled>
                                            <label class="form-check-label" for="profile_gender_female">Female</label>
                                        </div>
                                        <div class="form-check form-check-inline ps-radio">
                                            <input class="form-check-input" type="radio" name="gender" id="profile_gender_others" value="OTHERS" <?php echo e(($user->gender == "OTHERS") ? 'checked' : ''); ?> disabled>
                                            <label class="form-check-label" for="profile_gender_others">Other</label>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="col-12 text-center">
                                <div class="form-group">
                                    <button class="ps-btn mb-2" type="button" id="edit">Edit Profile</button>
                                    <button class="ps-btn bg-dark mb-2" type="button" id="changePassBtn" data-toggle="modal" data-target="#updatePassModal">Change Password</button>
                                    <button class="ps-btn bg-success mb-2 d-none" type="submit" id="submitbutton">Update</button>
                                    <button class="ps-btn bg-dark mb-2 d-none" type="button" id="cancelbutton">Cancel</button>
                                </div>

                                <div class="d-flex justify-content-center">
                                    <div class="sk-flow d-none" id="updateProfSpinner">
                                        <div class="sk-flow-dot"></div>
                                        <div class="sk-flow-dot"></div>
                                        <div class="sk-flow-dot"></div>
                                    </div>
                                </div>
                                <div id="updateProfStatus"></div>
                            
                            </div>
                        </div>
                    </form>
                </div>

            </div>
        </div>
    </div>

</section>

<div class="modal" id="updatePassModal" tabindex="-1" role="dialog">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-body">
                
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <i class="fa fa-remove"></i>
                </button>
                <p class="modalHeader">Change Password</p>
                <div class="ps-form--account">
                    <div class="ps-form__content">
                        <form id="updatePassForm" class="form-horizontal">
                            <div class="form-group">
                                <label>Current Password *</label>
                                <input class="form-control" type="password" name="current_password" id="current_password" required>
                            </div>

                            <div class="form-group">
                                <label>New Password *</label>
                                <input class="form-control" type="password" name="new-password" id="new-password" required>
                            </div>

                            <div class="form-group">
                                <label>Confirm New Password *</label>
                                <input class="form-control" type="password" name="new-password_confirmation" id="new-password_confirmation" required>
                            </div>

                            <div class="form-group">
                                <button class="ps-btn ps-btn--fullwidth" type="submit" id="updatepassword">Update Password</button>
                            </div>
                            <div class="d-flex justify-content-center">
                                <div class="sk-flow d-none" id="updatePassSpinner">
                                    <div class="sk-flow-dot"></div>
                                    <div class="sk-flow-dot"></div>
                                    <div class="sk-flow-dot"></div>
                                </div>
                            </div>
                            <div id="updatePassStatus"></div>
                        </form>
                    </div>
                </div>
            </div>

        </div>
    </div>
</div>
<script>
    function chooseFile() {
        $("#profile_image").click();
    }


    function readURL(input) {
        if (input.files && input.files[0]) {
            var reader = new FileReader();

            reader.onload = function (e) {
                $('.image_profile')
                    .attr('src', e.target.result);
            };

            reader.readAsDataURL(input.files[0]);

            var data = new FormData();
            var files = input.files[0]
            data.append('file', files);

            $.ajax({
                type: "POST",
                url: '<?php echo e(url("uploadprofileimage")); ?>',
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
                data: data,
                processData: false,
                contentType: false,
                cache: false,
                success: function (response) {
                    let status = response.class_name;
                    status = status.replace("alert-", "");
                    if(status == 'danger') status = 'error';

                    swal("", response.message, status);
                },
                error: function (data) {
                    swal("", "Fail to run like..", 'error');
                }
            });

        }
    }

    $('#edit').on('click', function (e) {
        e.preventDefault();
        $('#updateprofile input').prop('readOnly', false);
        $('input[name="gender"]').attr('disabled', false);
        $('#updateprofile input[name="mobile"]').prop('readOnly', true);
        $('#submitbutton, #cancelbutton').removeClass('d-none');
        $(this).addClass('d-none');
        $('#changePassBtn').addClass('d-none');

    });

    $('#cancelbutton').on('click', function (e) {
        e.preventDefault();
        $('#updateprofile input').prop('readOnly', true);
        $('input[name="mobile"]').prop('readOnly', true);
        $('input[name="gender"]').attr('disabled', true);
        $('#edit, #changePassBtn').removeClass('d-none');
        $('#submitbutton, #cancelbutton').addClass('d-none');
    });


    $('#updateprofile').on('submit', function (e) {
        e.preventDefault();

        $('#updateProfStatus').removeClass().html('');
        $('#submitbutton,#cancelbutton').addClass('d-none');
        $('#updateProfSpinner').removeClass('d-none');

        var form = $('#updateprofile')[0];
        var formData = new FormData(form);

        $.ajax({
            type: "POST",
            url: '<?php echo e(url("updateprofile")); ?>',
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            },
            data: formData,
            processData: false,
            contentType: false,
            cache: false,
            success: function (response) {
                
                $('#updateProfSpinner').addClass('d-none');
                let message = response.message;
                
                if (response.status == 'success') {

                    $('#updateprofile input').prop('readOnly', true);
                    $('input[name="gender"]').attr('disabled', true);
                    $('#submitbutton,#cancelbutton').addClass('d-none');
                    $('#edit, #changePassBtn').removeClass('d-none');
                    
                    let msg = `<i class="fa fa-check pr-2"></i>${message}`;
                    $('#updateProfStatus').fadeIn().addClass('alert alert-success').html(msg);

                    setTimeout(function(){ 
                        $('#updateProfStatus').removeClass().html('');
                    }, 3000);

                }else{
                    $('#submitbutton,#cancelbutton').removeClass('d-none');
                    $('#updateProfStatus').fadeIn().addClass('alert alert-danger').html(message);
                }
            },
            error: function (data) {
                $('#updateProfSpinner').addClass('d-none');
                $('#submitbutton,#cancelbutton').removeClass('d-none');
                let errors = data.responseJSON.errors;
                let errMsg = '<ul style="list-style:none">';
                if(errors){
                    for(let key in errors){
                        let errList = errors[key];
                        errList.forEach(function(msg){
                            errMsg+=`<li>${msg}</li>`;
                        });
                    }
                }
                errMsg+= '</ul>';
                $('#updateProfStatus').fadeIn().addClass('alert alert-danger').html(errMsg);
            }
        });
    });

    $('#updatePassForm').on('submit', function (e) {
        e.preventDefault();
        $('#updatePassStatus').removeClass().html('');
        $('#updatepassword').addClass('d-none');
        $('#updatePassSpinner').removeClass('d-none');
        var form = $('#updatePassForm')[0];
        var formData = new FormData(form);
       
        $.ajax({
            type: "POST",
            url: '<?php echo e(url("updatepassword")); ?>',
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            },
            data: formData,
            processData: false,
            contentType: false,
            cache: false,
            success: function (response) {
                $('#updatePassSpinner').addClass('d-none');

                let message = response.message;
                if (response.status == 'success') {
                    $('#new-password_confirmation, #new-password, #current_password').val('');

                    let msg = `<i class="fa fa-check pr-2"></i>${message}`;
                    $('#updatePassStatus').fadeIn().addClass('alert alert-success').html(msg);

                    setTimeout(function(){ 
                        $('#updatePassModal').modal('hide'); 
                        $('#updatePassStatus').removeClass().html('');
                    }, 2000);
                }else{
                    $('#updatepassword').removeClass('d-none');
                    $('#updatePassStatus').fadeIn().addClass('alert alert-danger').html(message);
                }
            },
            error: function (data) {
                
                $('#updatePassSpinner').addClass('d-none');
                $('#updatepassword').removeClass('d-none');
                let errors = data.responseJSON.errors;
                let errMsg = '<ul>';
                if(errors){
                    for(let key in errors){
                        let errList = errors[key];
                        errList.forEach(function(msg){
                            errMsg+=`<li>${msg}</li>`;
                        });
                    }
                }
                errMsg+= '</ul>';
                $('#updatePassStatus').fadeIn().addClass('alert alert-danger').html(errMsg);
            }
        });
    });
</script>


<?php $__env->stopSection(); ?>

<?php echo $__env->make('layouts.app', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH C:\wamp64\www\offers\resources\views/frontend/profile.blade.php ENDPATH**/ ?>