<?php $__env->startSection('content'); ?>
<style>
    #map {
        width: 100%;
        height: 350px;
    }

    /* */
    .ps-shop__info p {
        font-size:1.6rem;
        color:#000;
    }
</style>

<?php if(session('alert')): ?>
    <div class="alert alert-<?php echo e(session('class')); ?>">
        <?php echo e(session('alert')); ?>

    </div>
<?php endif; ?>
<?php
    $category = \App\Models\Category::find($category_id);
    $parent_category = $category->parentCategories;
?>

<div class="ps-breadcrumb">
    <div class="container">
        <ul class="breadcrumb text-capitalize" id="breadcrumb">
            <li><a href="<?php echo e(url('/')); ?>">Home</a></li>
            <li><?php echo e($shops->name); ?></li>
        </ul>
    </div>
</div>

<section >
    <div class="ps-page--product">
        <div class="container">

            <div class="ps-page--product">
                <div class="container">
                    <div class="ps-page__container">

                        <div class="ps-page__left">
                            <div class="ps-product--detail">

                                <div class="ps-product__header">
                                    <div class="ps-product__thumbnail" data-vertical="false">
                                        <figure>
                                            <div class="ps-wrapper">
                                                <div class="ps-product__gallery" data-arrow="true">

                                                <?php
                                                    $images = $shops->shop_images;
                                                ?>
                                                <?php if(!empty($images)): ?>
                                                    <?php $__currentLoopData = $images; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $image): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                                        <div class="item">
                                                            <a href="<?php echo asset("$image->image_path"); ?>">
                                                                <img src="<?php echo asset("$image->image_path"); ?>" alt="">
                                                            </a>
                                                        </div>
                                                    <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                                <?php endif; ?>

                                                </div>
                                            </div>
                                        </figure>
                                        <?php if(!empty($images)): ?>
                                        <div class="ps-product__variants" data-item="4" data-md="3" data-sm="3" data-arrow="false">
                                            <?php $__currentLoopData = $images; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $image): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                                <div class="item">
                                                    <img src="<?php echo asset("$image->image_path"); ?>" alt="">
                                                </div>
                                            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                        </div>
                                        <?php endif; ?>
                                    </div>

                                    <div class="ps-product__info">
                                        <h1><?php echo e(ucfirst($shops->shop_name)); ?>

                                            <?php if(Auth::guest()): ?>
                                                <a href="#" class="badge zoom ml-2" data-id="<?php echo e($shops->id); ?>" data-toggle="modal" data-target="#myModal1">
                                                    <span data-toggle="tooltip" data-placement="right" title="" data-original-title="Like">
                                                        <i class="fa fa-heart-o fa-lg text-danger"></i>
                                                    </span>
                                                </a>
                                            <?php else: ?>
                                                <?php if(!empty($likes)): ?>
                                                    <a href="#" class="badge zoom ml-2 unlike" data-id="<?php echo e($shops->id); ?>" data-toggle="tooltip" data-placement="right" title="" data-original-title="Unlike">
                                                        <i class="fa fa-heart fa-lg text-danger"></i>
                                                    </a>
                                                <?php else: ?>
                                                    <a href="#" class="badge zoom ml-2 like" data-id="<?php echo e($shops->id); ?>" data-toggle="tooltip" data-placement="right" title="" data-original-title="Like">
                                                        <i class="fa fa-heart-o fa-lg text-danger"></i>
                                                    </a>
                                                <?php endif; ?>
                                            <?php endif; ?>
                                        </h1>
                                        <div class="ps-product__meta"></div>
                                        <div class="ps-shop__info">
                                            <p>
                                                <i class="fa fa-phone text-success pr-2"></i>
                                                <?php echo e($shops->mobile); ?>

                                                <?php if($shops->phone_number_2): ?>
                                                    <?php echo e(", ".$shops->phone_number_2); ?>

                                                <?php endif; ?>
                                            </p>
                                            <p>
                                                <i class="fa fa-whatsapp text-success pr-2"></i>
                                                <?php echo e($shops->phone_number_3); ?>

                                            </p>
                                            <p>
                                                <i class="fa fa-envelope pr-2"></i><?php echo e($shops->email); ?>

                                            </p>
                                            <p>
                                                <i class="fa fa-map-marker fa-lg pr-2"></i><?php echo e($shops->address); ?>

                                            </p>
                                            <p>
                                            <?php for($i = 0; $i < 5; $i++): ?>
                                                <?php if($i< (int)$shops->shop_rating): ?>
                                                    <span class="fa fa-star text-danger"></span>
                                                <?php else: ?>
                                                    <?php if(($shops->shop_rating-(int)$shops->shop_rating) == 0.5 && ((int)$shops->shop_rating) == $i ): ?>
                                                        <span class="fa fa-star-half-o text-danger"></span>
                                                    <?php else: ?>
                                                        <span class="fa fa-star-o text-danger"></span>
                                                    <?php endif; ?>
                                                <?php endif; ?>
                                            <?php endfor; ?>
                                            </p>

                                        </div>

                                        <div class="ps-product__sharing pt-0">
                                            <p>Share</p>
                                            <a class="facebook" href="#"><i class="fa fa-facebook"></i></a>
                                            <a class="twitter" href="#"><i class="fa fa-twitter"></i></a>
                                            <a class="instagram bg-danger" href="#"><i class="fa fa-instagram"></i></a>
                                            <a class="bg-success" href="#"><i class="fa fa-whatsapp"></i></a>
                                            <a class="bg-info" href="#"><i class="fa fa-share-alt"></i></a>
                                        </div>
                                    </div>
                                </div>

                                <!-- tabs -->
                                <div class="ps-product__content ps-tab-root pt-5">
                                    <ul class="ps-tab-list">
                                        <li class="active"><a href="#tab-aboutus">About us</a></li>
                                        <li><a href="#tab-offers">Offers</a></li>
                                        <li><a href="#tab-wingdeals">Wing Deals</a></li>
                                        <?php if($shops->menus->isNotEmpty()): ?>
                                            <li><a href="#tab-menus">Menus</a></li>
                                        <?php endif; ?>
                                    </ul>
                                    <div class="ps-tabs">

                                        <!-- about us tab -->
                                        <div class="ps-tab active" id="tab-aboutus">
                                            <div class="ps-document">
                                                <h4><?php echo e(strtoupper($shops->name)); ?></h4>
                                                <?php echo $shops->shop_description; ?>

                                                <h5>Phone : <span>+91 <?php echo e($shops->phone_number); ?></span></h5>
                                            </div>
                                        </div>
                                        <!-- end of about us tab -->

                                        <!-- offers tab -->
                                        <div class="ps-tab" id="tab-offers">
                                            <?php if($shops->offersOngoing->isNotEmpty()): ?>
                                                <?php $__currentLoopData = $shops->offersOngoing; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $item): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                                    <h4><?php echo e(strtoupper($item->offer_name)); ?></h4>
                                                    <div class="m-5">
                                                        <?php if(!empty($item->offer_image)): ?>
                                                        <div class="lightgallery">
                                                            <a href="<?php echo asset("$item->offer_image"); ?>">
                                                                <img class="mb-5" src="<?php echo asset("$item->offer_image"); ?>" alt="" height="200px" width="auto">
                                                            </a>
                                                        </div>
                                                        <?php endif; ?>
                                                        <h5>Description</h5>
                                                        <?php echo $item->description; ?>

                                                        <h5>Offer Dates : <span><?php echo e($item->from_date); ?> to <?php echo e($item->to_date); ?></span></h5>

                                                    </div>
                                                    <hr class="mb-5"/>
                                                <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                            <?php else: ?>
                                                <p>Sorry no more offers available</p>
                                            <?php endif; ?>
                                        </div>
                                        <!-- end of offers tab -->

                                        <!-- wing deals tab -->
                                        <div class="ps-tab" id="tab-wingdeals">
                
                                            <?php if($shops->dealsOngoing->isNotEmpty()): ?>
                                                <?php $__currentLoopData = $shops->dealsOngoing; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $item): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                                    <div class="ps-post ps-post--small-thumbnail">
                                                        <div class="ps-post__thumbnail">
                                                            <?php if(!empty($item->pic)): ?>
                                                                <img src="<?php echo asset("{$item->pic}"); ?>" alt="">
                                                            <?php endif; ?>
                                                        </div>
                                                        <div class="ps-post__content">
                                                            <div class="ps-post__top">
                                                                <span class="ps-post__title" ><?php echo e(strtoupper($item->name)); ?></span>
                                                                <span> ₹. <del><?php echo e($item->original_price); ?> </del> </span> &nbsp;&nbsp; <span>  <?php echo e($item->price); ?> </span>

                                                                <?php if($item->payment_type == 'wingpoints'): ?>
                                                                    <p>Pay <?php echo e($item->points_to_claim); ?> (wingpoints) to claim</p>
                                                                <?php else: ?>
                                                                    <p>Pay  ₹.<?php echo e($item->price); ?> to claim</p>
                                                                <?php endif; ?>

                                                                <p>Flat <?php echo e((($item->original_price- $item->price )/$item->original_price) * 100); ?> % off*</p>

                                                                <p>
                                                                    Valid for:
                                                                    <?php
                                                                        $valid_for = explode(',',$item->valid_for);
                                                                        $valid = config('global.valid_for');
                                                                    ?>
                                                                    <?php $__currentLoopData = $valid; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $v): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                                                        <?php if(in_array($v['name'],$valid_for)): ?>
                                                                        <img src="<?php echo e(asset($v['image'])); ?>" height="10px" width="10px"><?php echo e($v['name']); ?>


                                                                        <?php endif; ?>
                                                                    <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                                                </p>
                                                            </div>
                                                            <div class="ps-post__bottom">
                                                                <button class="ps-btn btn-sm bg-warning text-dark dealdetails" data-id="<?php echo e($item->id); ?>">
                                                                    <strong>Details</strong>
                                                                </button>

                                                                <?php if(Auth::guest()): ?>
                                                                    <button class="ps-btn btn-sm pull-right" data-toggle="modal" data-target="#myModal1">Claim Now</button>
                                                                <?php else: ?>
                                                                    <?php if($item->payment_type != 'wingpoints'): ?>
                                                                    <a href="<?php echo e(url('/getdeal',[ $item->id])); ?>" class="ps-btn btn-sm pull-right"data-id="<?php echo e($item->id); ?>">Claim Now</a>
                                                                    <?php else: ?>
                                                                        <?php if(Session::get('total_wingpoints')->points >= $item->price): ?>
                                                                            <a href="<?php echo e(url('/getdeal',[ $item->id])); ?>" class="ps-btn btn-sm pull-right" data-id="<?php echo e($item->id); ?>">Claim Now</a>
                                                                        <?php else: ?>
                                                                            <button class="ps-btn btn-sm bg-dark pull-right" disabled> Not Enough Points </button>
                                                                        <?php endif; ?>
                                                                    <?php endif; ?>
                                                                <?php endif; ?>

                                                            </div>

                                                        </div>
                                                    </div>
                                                <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                            <?php else: ?>
                                                <p>Sorry no deals available </p>
                                            <?php endif; ?>
                                        </div>
                                        <!-- end of wing deals tab -->

                                        <!-- menus tab -->
                                        <?php if(!empty($shops->menus)): ?>
                                        <div class="ps-tab" id="tab-menus">
                                            <?php $__currentLoopData = $shops->menus; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $item): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                            <div class="col-md-4 thumbnail">
                                                <div class="lightgallery">
                                                    <a href="<?php echo asset("$item->image_path"); ?>">
                                                        <img src="<?php echo asset("$item->image_path"); ?>" alt="" style="width:100%;max-height:240px; height:240px;">
                                                    </a>
                                                </div>

                                            </div>
                                            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                        </div>
                                        <?php endif; ?>
                                        <!-- end of menus tab -->
                                    </div>
                                </div>
                                <!-- end of tabs -->

                            </div>
                        </div>

                        <div class="ps-page__right">
                            <aside class="widget">
                                <div id="map"></div>
                            </aside>
                            <aside class="widget widget_product widget_features">
                                <h4 class="text-center">Business Hours</h4>
                                <hr></hr>
                                <?php
                                    $active_days = explode(",",$shops->active_days);
                                    $today = date('l');
                                    $time = date('H:i');
                                ?>
                                <?php $__currentLoopData = $days; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $item): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                    <?php
                                        $day = ($item->id== 5) ? substr($item->name,0,4) : substr($item->name,0,3);
                                        $start_time = $shops[strtolower($day)."_start"];
                                        $end_time = $shops[strtolower($day)."_end"];
                                    ?>
                                    <p class="p-0"><?php echo e($day); ?> :
                                        <?php if(in_array( $item->id , $active_days)): ?>
                                            <?php echo e(date('h:i A',strtotime($start_time))); ?> - <?php echo e(date('h:i A',strtotime($end_time))); ?>

                                            <?php if($today == $item->name): ?>
                                                <?php if( $time >= $start_time && $time <= $end_time): ?>
                                                    <span class="text-success">Open</span>
                                                <?php else: ?>
                                                    <span class="text-danger"> Closed</span>
                                                <?php endif; ?>
                                            <?php endif; ?>
                                        <?php else: ?>
                                            Closed
                                        <?php endif; ?>
                                    </p>
                                <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                            </aside>
                            <aside class="widget">
                                <iframe src="https://www.youtube.com/embed/668nUCeBHyY?autoplay=1" width="100%" height="250" allowfullscreen></iframe>
                            </aside>
                        </div>

                    </div>
                </div>
            </div>

        </div>
    </div>
</section>



<div class="modal fade" id="dealdetails">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <h3 class="modal-title"></h3>
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true"> &times; </button>
            </div>
            <div class="modal-body p-4">

            </div>
        </div>
    </div>
</div>


  <script async defer
    src="https://maps.googleapis.com/maps/api/js?key=AIzaSyBHLFZ4C3XB7MDwjywzpBC1ZWMH0u2zuE4&callback=initMap">
</script>
<script>
    function initMap() {
        var myLatLng = {lat: <?php echo e($shops->lat); ?>, lng: <?php echo e($shops->lng); ?> };

        var map = new google.maps.Map(document.getElementById('map'), {
          zoom: 12,
          center: myLatLng
        });

        var marker = new google.maps.Marker({
          position: myLatLng,
          map: map,
          title: 'Hello World!'
        });
      }

      $('.dealdetails').click(function () {
          let id =  $(this).data('id');

        $.ajax({
          type: "GET",
          url: "<?php echo e(url('/dealdetails/')); ?>"+"/"+id,

          success: function (response) {

            var json_obj = $.parseJSON(response);

            var mymodal = $('#dealdetails');
            mymodal.find('.modal-title').html(`
             `+ json_obj[0]['name'].toUpperCase()+`
             `);

             mymodal.find('.modal-body').html(`
             <div>
                <h4><u>Description:</u></h4>
                <p>`+json_obj[0]['description'] +`</p>
                <hr>
                <h4><u>Active days:</u></h4>
                <p>`+json_obj[0]['days_applicable'].join()+`</p>

                <h4><u>Valid for:</u></h4>
                <p>`+'one Person only'+`</p>

                <h4><u>Valid Dates:</u></h4>
                <p>`+json_obj[0]['valid_from']+' To '+json_obj[0]['valid_from']+`</p>

                <h4><u>Terms & Conditions:</u></h4>
                <p>`+json_obj[0]['terms_conditions'] +`</p>
                <div class="text-center">
                    <?php if(Auth::guest()): ?>
                        <button class="ps-btn" data-toggle="modal" data-target="#myModal1">Claim Now</button>
                    <?php else: ?>
                        <button class="ps-btn claim" data-id= `+id+`>Claim Now</button>
                    <?php endif; ?>
                </div>
             </div>
             `);
            $('#dealdetails').modal('show');
          }
      });

      })


      $(document).on('click', ".unlike", function (e) {
        e.preventDefault();
        var id = $(this).data('id');
        var element = this;

        var data = {
                shop_id:id
            };
        $.ajax({
                type: "post",
                url: '<?php echo e(url("removefavouriteshop")); ?>',
                headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                 },
                data: data,
                cache: false,
                success: function (data)
                {
                    if(data.status == "success"){
                        $(element).removeClass('unlike').addClass('like').attr('data-original-title', 'Like').html('<i class="fa fa-heart-o text-danger"></i>');
                    }
                },
                error: function (data){
                    swal('Fail to run unlike..');
                }
            });
    });

    $(document).on('click', ".like", function (e) {
        e.preventDefault();
        var id = $(this).data('id');
        var element = this;

        var data = {
                shop_id:id
            };
        $.ajax({
            type: "post",
            url: '<?php echo e(url("addfavouriteshop")); ?>',
            headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
            data: data,
            cache: false,
            success: function (data)
            {
                if(data.status == "success"){
                    $(element).removeClass('like').addClass('unlike').attr('data-original-title', 'Unlike').html('<i class="fa fa-heart text-danger"></i>');
                }
            },
            error: function (data){
                swal('Fail to run like..');
            }
        });
    });
</script>
<script>
    $(document).ready(function(){
        var category = JSON.parse('<?php echo json_encode($category); ?>');
        var parent_category = JSON.parse('<?php echo json_encode($parent_category); ?>');
        if(parent_category != null){
            var data =  toarray([parent_category]);
            var da = {
                    'id': category.id,
                    'category_name':category.category_name,
                    'parent_id':category.parent_id
                };

            data.unshift(da);
            data.reverse();
            breadcrumb(data);
        }else{
            var data = [];
            var da = {
                    'id': category.id,
                    'category_name':category.category_name,
                    'parent_id':category.parent_id
                };
            data.unshift(da);
            breadcrumb(data);

        }

        function toarray(data,cat){
            var categories = (cat== null) ? []: cat;
            data.forEach(element => {
                id = element.id;
                category_name = element.category_name;
                parent_id = element.parent_id;

                var data = {
                    'id': id,
                    'category_name':category_name,
                    'parent_id':parent_id
                };

                categories.push(data);
                var cat = categories;
                if(element.parent_categories) { toarray([element.parent_categories],cat); }

            });
            return categories;
        }

        function breadcrumb(data){
            data.forEach((element,key,array)=>{
                var id = element.id;
                var category_name = element.category_name.toLowerCase();
                var parent_id = element.parent_id;
                    var li = `<li><a href="<?php echo e(url('subcategories')); ?>/`+id+`">`+category_name+`</a></li>`;
                    $('#breadcrumb').find(' > li:nth-last-child(1)').before(li);

            });
        }
    });

</script>

<?php $__env->stopSection(); ?>

<?php echo $__env->make('layouts.app', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH C:\wamp64\www\offerswing_test\resources\views/frontend/shopdetails.blade.php ENDPATH**/ ?>