<?php

namespace App\Http\Controllers\Backend;

use App\Http\Controllers\Controller;
use App\Models\Slider;
use App\Models\SliderData;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;

class SliderController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('backend.slider.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $slider = new Slider();

        $slider->slider_name = $request['slider_name'];

        $slider->save();

        if ($request->hasfile('slider_data_image')) {
            foreach ($request->file('slider_data_image') as $key => $file) {
                // $name = $image->getClientOriginalName();

                $fileName = 'imagediv_'.time().'.'.$file->getClientOriginalExtension();
                $path = $file->storeAs('imagediv', $fileName, 'public');

                $data[$key] = $path;

                // $new_name = $slider->id.'_'.$key.'.'.$image->getClientOriginalExtension();

                // $image->move(public_path().'/slider_images/', $new_name);

                // $data[$key] = $new_name;
            }
        }

        foreach ($request['slider_data'] as $key => $value) {
            $slider_data = new SliderData();

            $slider_data->slider_id = $slider->id;

            $slider_data->name = $value['name'];

            $slider_data->image = 'slider_images'.'/'.$data[$key];

            $slider_data->url = $value['url'];

            $slider_data->save();
        }

        return redirect('/admin/home');
    }

    /**
     * Display the specified resource.
     *
     * @param int $id
     *
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param int $id
     *
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $slider = Slider::find($id);

        $slider_data = SliderData::where('slider_id', $id)->get();

        return view('backend.slider.edit', compact('slider', 'slider_data'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param int $id
     *
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $slider = Slider::find($id);

        $slider->slider_name = $request['slider_name'];

        $slider->save();

        if ($request->hasfile('slider_data_image')) {
            //  var_dump("hello");die;

            foreach ($request->file('slider_data_image') as $key => $file) {
                $fileName = 'imagediv_'.time().'.'.$file->getClientOriginalExtension();
                $path = $file->storeAs('imagediv', $fileName, 'public');

                $data[$key] = $path;
            }
        }

        foreach ($request['slider_data'] as $key => $value) {
            if (isset($value['id']) && !empty($value['id'])) {
                $slider_data = SliderData::find($value['id']);

                $slider_data->slider_id = $id;

                $slider_data->name = $value['name'];

                if (!empty($data[$key])) {
                    Storage::disk('public')->delete($slider_data->image);
                    $slider_data->image = 'slider_images'.'/'.$data[$key];
                }

                $slider_data->url = $value['url'];

                $slider_data->save();
            } else {
                $slider_data = new SliderData();

                $slider_data->slider_id = $id;

                $slider_data->name = $value['name'];

                $slider_data->image = 'slider_images'.'/'.$data[$key];

                $slider_data->url = $value['url'];

                $slider_data->save();
            }
        }

        return redirect('/admin/home');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param int $id
     *
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
    }
}
