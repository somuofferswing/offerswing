<?php

namespace App\Http\Controllers\Backend;

use App\Http\Controllers\Controller;
use App\Models\Category;
use App\Models\Days;
use App\Models\Offer;
use App\Models\Shop;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Storage;

class OffersController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index(Request $request)
    {
        if (1 == Auth::user()->user_type) {
            $offer = Offer::select('*', 'offers.id as Offer_id')
                ->join('shops as s', 's.id', '=', 'offers.shop_id');

            if ($request['shop_id'] && !empty($request['shop_id'])) {
                $offer->where('offers.shop_id', $request['shop_id']);
            }

            if ($request['category_id'] && !empty($request['category_id'])) {
                $offer->where('offers.category_id', $request['category_id']);
            }

            $offers = $offer->get();

            $shops = Shop::all();

            $category = Category::getCategories();
            return view('backend.offers.index', compact('offers', 'shops', 'category'));
        }

        if (3 == Auth::user()->user_type) {
            $user = auth()->user();

            $user_id = $user['id'];

            $shopowner = ShopOwner::where('retailer_id', '=', $user_id)->first();

            $shop_id = $shopowner['shop_id'];

            $offers = Offer::select('*', 'offers.id as Offer_id')
                ->leftjoin('shops', 'shops.id', '=', 'offers.shop_id')
                ->join('categories as cat', 'cat.id', '=', 'shops.shop_category')
                ->where('offers.shop_id', '=', $shop_id)->get();
            $category = Category::getCategories();

            return view('backend.offers.index', compact('offers', 'category'));
        }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function create()
    {
        // $user = Auth::user();

        // $user_id = $user->id;

        $shops = Shop::all();
        $days = config('global.days');

        // $category = Category::getCategories();

        return view('backend.offers.create', compact('shops', 'days'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @return Response
     */
    public function store(Request $request)
    {
        $offer = new Offer();

        $this->validate($request, [
            'name' => 'required',
            'description' => 'required',
            'from_date' => 'required',
            'to_date' => 'required',
            'shop_id' => 'required',
        ]);

        $shop_details = Shop::find($request['shop_id']);
        $offer->offer_name = $request['name'];
        $offer->description = $request['description'];
        // $offer->category_id = $shop_details['shop_category'];
        $offer->shop_id = $request['shop_id'];
        // $offer->days_applicable = implode(",",$request['days_applicable']);
        $offer->from_date = $request['from_date'];
        $offer->to_date = $request['to_date'];
        $offer->top = isset($request['top']) ? $request['top'] : 0 ;
        $slug = $this->slugify($request['name']);
        $offer->slug = $slug;

        if ($request->hasFile('offer_image')) {
            $file = $request->file('offer_image');
            $fileName = $slug.'_'.time().'.'.$file->getClientOriginalExtension();
            $path = $file->storeAs('offers', $fileName, 'public');
            $offer->offer_image = $path;
        }
        
        $offer->save();

        return redirect('/offers')->with('success', 'Offer Ceated Successfully');
    }

    /**
     * Display the specified resource.
     *
     * @param int $id
     *
     * @return Response
     */
    public function show($id)
    {
        $offers = Offer::find($id);

        return view('backend.offers.show', compact('offers'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param int $id
     *
     * @return Response
     */
    public function edit($id)
    {
        $shops = Shop::all();
        $days = config('global.days');
        // $category = Category::getCategories();
        $offers = Offer::find($id);

        return view('backend.offers.edit', compact('shops', 'days', 'offers'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param int $id
     *
     * @return Response
     */
    public function update(Request $request, $id)
    {
        $offer = Offer::find($id);

        $this->validate($request, [
            'name' => 'required',
            'description' => 'required',
            'from_date' => 'required',
            'to_date' => 'required',
            'shop_id' => 'required',
        ]);

        $offer->offer_name = $request['name'];
        $offer->description = $request['description'];
        $offer->shop_id = $request['shop_id'];
        $offer->from_date = $request['from_date'];
        $offer->to_date = $request['to_date'];
        $offer->top = isset($request['top']) ? $request['top'] : 0;

        if ($offer->isDirty('offer_name')) {
            $slug = $this->slugify($request['name']);
            $offer->slug = $slug;
        } else {
            $slug = $offer->slug;
        }

        if ($request->hasFile('offer_image')) {
            Storage::disk('public')->delete($offer->offer_image);
            
            $file = $request->file('offer_image');
            $fileName = $slug.'_'.time().'.'.$file->getClientOriginalExtension();
            $path = $file->storeAs('offers', $fileName, 'public');
            $offer->offer_image = $path;
        }

        $offer->save();

        return redirect('/offers')->with('success', 'Offer Updated Successfully');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param int $id
     *
     * @return Response
     */
    public function destroy($id)
    {
        $offer = Offer::find($id);

        $offer->delete();

        return redirect('/offers')->with('success', 'Offer has been deleted Successfully');
    }

    public function shopOffers($id)
    {
        $user = auth()->user();

        // $shop_owners = ShopOwner::where('retailer_id', $user['id'])->where('shop_id', $id)->first();

        // if ($shop_owners->shop_id == $id) {

        // $deals = Deal::where('shop_id', $id)->get();

        $offers = Offer::select('*', 'offers.id as offer_id')
            ->leftjoin('shops', 'shops.id', '=', 'offers.shop_id')
            ->where('offers.shop_id', '=', $shop_owners->shop_id)->get();

        $shops = Shop::all();

        $category = Category::getCategories();

        // var_dump($deals);die;

        return view('backend.offers.index', compact('offers', 'shops', 'category'));
        // }
    }

    public function viewShopOffer($id)
    {
        $user = auth()->user();

        $shop_owners = ShopOwner::where('retailer_id', $user['id'])->where('shop_id', $id)->first();

        $offer = Offer::find($id);

        // $deal_orders = Deal::dealOrders($id);

        // var_dump($deal);die;

        return view('backend.offers.show', compact('offer'));
    }


    public function slugify($str)
    {
        $search = ['Ș', 'Ț', 'ş', 'ţ', 'Ş', 'Ţ', 'ș', 'ț', 'î', 'â', 'ă', 'Î', 'Â', 'Ă', 'ë', 'Ë'];
        $replace = ['s', 't', 's', 't', 's', 't', 's', 't', 'i', 'a', 'a', 'i', 'a', 'a', 'e', 'E'];
        $str = str_ireplace($search, $replace, strtolower(trim($str)));
        $str = preg_replace('/[^\w\d\-\ ]/', '', $str);
        $str = str_replace(' ', '-', $str);

        return $this->getUniqueOfferSlug(preg_replace('/\-{2,}/', '-', $str));
    }

    private function getUniqueOfferSlug($slug)
    {
        $existingCount = Offer::where('slug', 'like', $slug . '-%')->count();

        if ($existingCount) {
            return $slug . '-' . ($existingCount);
        }

        return $slug;
    }
}
