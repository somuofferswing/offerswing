@extends('layouts.admin')



@section('content')
<style>
    .img-wrap {
        position: relative;
    }

    .img-wrap .close {
        position: absolute;
        top: 2px;
        right: 2px;
        z-index: 100;
        color: red;
        background: white;
    }
</style>

<link rel="stylesheet"
    href="{{ asset('adminlte/bower_components/bootstrap-datepicker/dist/css/bootstrap-datepicker.min.css') }}">

<div class="content-wrapper">

    <section class="content-header">

        <h1>

            news

            <small>Add Deal</small>

        </h1>

        <!-- <ol class="breadcrumb">

              <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>

              <li class="active">Dashboard</li>

            </ol> -->

    </section>



    <section class="content">



        <div class="box box-info">

            <div class="box-header with-border">

                <h3 class="box-title">Edit Deal</h3>

            </div>

            <form class="form-horizontal" method="POST" action="{{ route('update-news',[$news->id]) }}"
                enctype="multipart/form-data">

                @csrf

                <div class="box-body">

                    <div class="form-group ">

                        <label for="title" class="col-sm-2 control-label">Title</label>

                        <div class="col-sm-10">

                            <input id="name" type="text" class="form-control" name="title" value="{{ $news->title }}"
                                required>

                            @if ($errors->has('title'))

                            <div class="danger">{{ $errors->first('title') }}</div>

                            @endif

                        </div>

                    </div>

                    <div class="form-group">

                        <label for="news_image" class="col-sm-2 control-label">Image</label>

                        <div class="col-sm-10">

                            <input type="file" class="form-control-file" id="news_image" name="images[]" multiple>
                            @php
                            $images = $news->newsImages;
                            @endphp
                            @foreach ($images as $item)
                            <div class="img-wrap" style="display:inline-block;">
                                <span class="close"><a href="#">&times;</a></span>
                                <img class="img-thumbnail" src='{{ asset('storage/'.$item->image) }}' height="100px"
                                    width="100px">
                            </div>
                            @endforeach


                        </div>

                    </div>

                    <div class="form-group">

                        <label for="video_link" class="col-sm-2 control-label">Video Link </label>

                        <div class="col-sm-10">
                            <input type="text" class="form-control" id="video_link" name="video_link"
                                value="{{ $news->video_link }}">
                        </div>

                    </div>

                    <div class="form-group ">

                        <label for="description" class="col-sm-2 control-label">Description</label>

                        <div class="col-sm-10">

                            <textarea id="description" type="text" class="form-control" name="description" required
                                autocomplete="description" autofocus>{{ $news->description }}</textarea>

                            @if ($errors->has('description'))

                            <div class="danger">{{ $errors->first('description') }}</div>

                            @endif

                        </div>

                    </div>



                    <div class="pull-right">

                        <button type="submit" class="btn btn-info">{{ __('Update') }}</button>

                        <button type="button" class="btn btn-danger"
                            onclick="window.history.go(-1); return false;">{{ __('Cancel') }}</button>

                    </div>



                </div>

            </form>

        </div>

    </section>

</div>

@endsection

@section('scripts')

<script src="{{ asset('adminlte/bower_components/bootstrap-datepicker/dist/js/bootstrap-datepicker.min.js') }}">
</script>

<script src="{{ asset('adminlte/bower_components/select2/dist/js/select2.full.min.js') }}"></script>

<script src="{{ asset('adminlte/bower_components/ckeditor/ckeditor.js') }}"></script>

<script>
    $(function () {



    CKEDITOR.replace('description')

  })

</script>

<script>
    var loadFile = function(event) {

            var output = document.getElementById('output');

            output.src = URL.createObjectURL(event.target.files[0]);

          };

</script>

@endsection