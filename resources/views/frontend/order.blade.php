@extends('layouts.app')

@section('content')
<div class="ps-breadcrumb">
    <div class="container">
        <ul class="breadcrumb text-capitalize" id="breadcrumb">
            <li><a href="{{ url('/') }}">Home</a></li>
            <li>My Orders</li>
        </ul>
    </div>
</div>

<section>
    <div class="ps-page--product">
        <div class="container">

            <div class="ps-product--detail">
                <ul class="ps-tab-list">
                    <li class="active"><a href="#tab-deals">Deals</a></li>
                    <li><a href="#tab-events">Events</a></li>
                    <li><a href="#tab-gifts">Gifts</a></li>
                </ul>
                <div class="ps-tabs">

                    <!-- deals tab -->
                    <div class="ps-tab active" id="tab-deals">
                        @if ($deal_orders->isNotEmpty())
                        <div class="row">
                            @foreach ($deal_orders as $item)

                            @php
                            // $order_details = $item->orderDetails
                            $deal = $item->orderDetails->deal;
                            @endphp
                            <div class="col-xl-4 col-lg-4 col-md-6 col-sm-12 col-12 ">
                                <div class="ps-post">
                                    <div class="ps-post__thumbnail">
                                        <a class="ps-post__overlay"
                                            href="{{ url('/orderdeal',["$item->order_id"])}}"></a>
                                        <img src="{{ asset('storage/'.$deal->pic)}}" alt="">
                                    </div>
                                    <div class="ps-post__content">
                                        <div class="ps-post__meta">
                                            @if ($item->valid_upto >= date('Y-m-d', time()))
                                            <span>Expiry On : {{ $deal->valid_upto }}</span>
                                            @else <span class="text-danger">Expired</span>
                                            @endif

                                            <span class="pull-right">Status :
                                                @if ($item->used == 0) Not Claimed
                                                @else Claimed
                                                @endif
                                            </span>
                                        </div>
                                        <a class="ps-post__title text-truncate"
                                            href="{{ url('/orderdeal',["$item->order_id"])}}">
                                            {{ ucfirst($deal->name) }}
                                        </a>
                                        <div class="text-right">
                                            <a class="font-weight-bold"
                                                href="{{ url('/orderdeal',["$item->order_id"])}}">View Invoice</a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            @endforeach
                        </div>
                        @else
                        <p>No Deal Orders</p>
                        @endif
                    </div>
                    <!-- end of deals tab -->

                    <!-- events tab -->
                    <div class="ps-tab" id="tab-events">
                        @if ($event_orders->isNotEmpty())
                        <div class="row">
                            @foreach ($event_orders as $item)

                            @php $event = $item->orderDetails->event; @endphp
                            <div class="col-xl-4 col-lg-4 col-md-6 col-sm-12 col-12 ">
                                <div class="ps-post">
                                    <div class="ps-post__thumbnail">
                                        <a class="ps-post__overlay"
                                            href="{{ url('/orderevent',["$item->order_id"])}}"></a>
                                        <img src="{{asset('storage/'.$event->event_image)}}" alt="">
                                    </div>
                                    <div class="ps-post__content">
                                        <div class="ps-post__meta">
                                            <span>Event Date : {{ $event->event_date }}</span>
                                            <span class="pull-right">Status :
                                                @if ($item->used == 0) Not Claimed
                                                @else Claimed
                                                @endif
                                            </span>
                                        </div>
                                        <a class="ps-post__title text-truncate"
                                            href="{{ url('/orderevent',["$item->order_id"])}}">{{ $event->title }}</a>
                                        <div class="text-right">
                                            <a class="font-weight-bold"
                                                href="{{ url('/orderevent',["$item->order_id"])}}">View Invoice</a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            @endforeach
                        </div>
                        @else
                        <p>No Events Orders</p>
                        @endif
                    </div>
                    <!-- end of events tab -->

                    <!-- gits tab -->
                    <div class="ps-tab" id="tab-gifts">
                        @if ($gift_orders->isNotEmpty())
                        <div class="row">
                            @foreach ($gift_orders as $item)

                            @php $gift = $item->orderDetails->gift; @endphp
                            <div class="col-xl-4 col-lg-4 col-md-6 col-sm-12 col-12 ">
                                <div class="ps-post">
                                    <div class="ps-post__thumbnail">
                                        <a class="ps-post__overlay"
                                            href="{{ url('/ordergift',["$item->order_id"])}}"></a>
                                        <img src="{{ asset('storage/'.$gift->image)}}" alt="">
                                    </div>
                                    <div class="ps-post__content">
                                        <div class="ps-post__meta">
                                            <span class="pull-right">Status :
                                                @if ($item->used == 0) Not Claimed
                                                @else Claimed
                                                @endif
                                            </span>
                                        </div>
                                        <a class="ps-post__title text-truncate"
                                            href="{{ url('/ordergift',["$item->order_id"])}}">{{ $gift->gift_name }}</a>
                                        <div class="text-right">
                                            <a class="font-weight-bold"
                                                href="{{ url('/ordergift',["$item->order_id"])}}">View Invoice</a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            @endforeach
                        </div>
                        @else
                        <p> No Gifts Orders</p>
                        @endif
                    </div>
                    <!-- end of gits tab -->

                </div>
            </div>
        </div>
    </div>
</section>
@endsection