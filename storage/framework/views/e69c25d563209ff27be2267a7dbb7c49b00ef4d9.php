<?php $__env->startSection('content'); ?>
<div class="content-wrapper">
    <section class="content-header">
        <h1>
            Category
            <small>Add Page</small>
        </h1>
        <!-- <ol class="breadcrumb">
                      <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
                      <li class="active">Dashboard</li>
                    </ol> -->
    </section>

    <section class="content">

        <div class="col-md-8 col-md-offset-2">
            <div class="box box-info">
                <div class="box-header with-border">
                    <h3 class="box-title">Update Page</h3>
                </div>
                <form class="form-horizontal" method="POST" action="<?php echo e(url('/pages/edit',[$page->id])); ?>"
                    enctype="multipart/form-data">
                    <?php echo csrf_field(); ?>
                    <div class="box-body">

                        <div class="form-group">
                            <label for="page_name" class="col-sm-2 control-label">Page Name</label>
                            <div class="col-sm-10">
                                <input id="page_name" type="text" class="form-control" name="page_name"
                                    value="<?php echo e($page->page_name); ?>" required disabled autofocus>
                            </div>
                        </div>

                        <div class="form-group">
                            <label for="content" class="col-sm-2 control-label">Page Content</label>
                            <div class="col-sm-10">
                                <textarea id="content" name="content" required><?php echo $page->content; ?></textarea>
                            </div>
                        </div>

                        <div class="form-group">
                            <label for="style" class="col-sm-2 control-label">Page Style</label>
                            <div class="col-sm-10">
                                <textarea id="style" name="style" required><?php echo $page->style; ?></textarea>
                            </div>
                        </div>

                        <div class="form-group">
                            <label for="script" class="col-sm-2 control-label">Page Script</label>
                            <div class="col-sm-10">
                                <textarea id="script" name="script" required><?php echo $page->script; ?></textarea>
                            </div>
                        </div>


                        <button type="submit" class="btn btn-info pull-right"><?php echo e(__('Update')); ?></button>
                        <button type="button" onclick="window.history.back()"><?php echo e(__('Back')); ?></button>
                    </div>
                </form>
            </div>
        </div>
    </section>
</div>
<?php $__env->stopSection(); ?>
<?php $__env->startSection('scripts'); ?>
<script src="<?php echo e(asset('adminlte/bower_components/ckeditor/ckeditor.js')); ?>"></script>
<script>
    $(function () {

CKEDITOR.replace('content');
CKEDITOR.replace('style');
CKEDITOR.replace('script');
})
</script>

<?php $__env->stopSection(); ?>

<?php echo $__env->make('layouts.admin', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH /home/offerkrx/public_html/offerswing/resources/views/pages/edit.blade.php ENDPATH**/ ?>