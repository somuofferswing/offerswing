@extends('layouts.app')

@section('content')

@php
    $event = $order->event;
@endphp
<section >
    <div class="ps-page--product">
        <div class="container">

            <div class="row">
                <div class="col-12 col-md-4">
                    <div class="ps-post text-center">
                        <img src="<?php echo asset("$event->event_image"); ?>">
                        <h3 class="pt-4 m-0">{{ $event->title }}</h3>
                    </div>
                </div>
                <div class="col-12 col-md-8">
                    <div class="text-center">
                        {!! QrCode::size(250)->generate($claimed->unique_code); !!}
                        <div>
                            <strong>Event Date : {{ date('d-m-Y',strtotime($event->event_date)) }}</strong>
                            <strong class="pl-4">Event Time : {{ date('h:i A',strtotime($event->event_time)) }}</strong>
                        </div>
                        
                        <div class="table-responsive">
                        <table class="table table-bordered ps-table ps-table--specification">
                                <tbody>
                                    <tr>
                                        <td>Booking id</td>
                                        <td>{{ $claimed->unique_code }}</td>
                                    </tr>
                                    <tr>
                                        <td>About Event</td>
                                        <td>{!! $event->description !!}</td>
                                    </tr>
                                    <tr>
                                        <td>Brought on</td>
                                        <td>{{ date('d-m-Y h:i A',strtotime($claimed->created_at)) }}</td>
                                    </tr>
                                    <tr>
                                        <td>Event Date</td>
                                        <td>{{ date('d-m-Y',strtotime($event->event_date)) }}</td>
                                    </tr>
                                    <tr>
                                        <td>Event Time</td>
                                        <td>{{ date('h:i A',strtotime($event->event_time)) }}</td>
                                    </tr>
                                    <tr>
                                        <td>Number of Tickets</td>
                                        <td>{{ $order->quantity }}</td>
                                    </tr>
                                    <tr>
                                        <td>Price</td>
                                        <td>{{ $order->quantity }} x {{ $event->price }} = {{ $order->quantity *  $event->price }}</td>
                                    </tr>
                                </tbody>
                            
                            </table>
                        </div>
                    </div>
                    <div class="ps-product__content pt-3">
                        <h4 class="ps-product__heading">Event Address</h4>
                        <p>{!! $event->event_address !!}</p>
  
                        <h4 class="ps-product__heading pt-3">Terms & Conditions</h4>
                        <div class="ps-document">
                            {!! $event->terms_conditions !!}
                        </div>
                    </div>

                </div>
            </div>
            
        </div>
    </div>
</section>
    <!-- @php
        $event = $order->event;
    @endphp
<section >
    <div class="ps-page--product">
        <div class="container">

            <div class="ps-product--detail ps-product--full-content">
                <div class="ps-product__top">
                    <div class="ps-product__header">
                        <div class="ps-product__thumbnail" data-vertical="false">
                            <img src="<?php echo asset("$event->event_image"); ?>">
                        </div>
                        <div class="ps-product__info">
                            <h1>{{ $event->title }}</h1>
                            <div class="ps-product__meta"></div>
                            <div class="ps-shop__info">
                                <p>Event Date : {{ date('d-m-Y',strtotime($event->event_date)) }}</p>
                                <p>Event Time : {{ date('h:i A',strtotime($event->event_time)) }}</p>
                                <p>Address : {!! $event->event_address !!}</p>
                            </div>
                            
                        </div>
                        
                    </div>

                    <div class="ps-product__price-right bg-white p-0">
                        {!! QrCode::size(250)->generate($claimed->unique_code); !!}
                    </div>
                </div>
       

                <div class="ps-product__content pt-5">
                    <h3 class="ps-product__heading">Info</h3>
                    <div class="ps-product__specification">
                        <div class="table-responsive">
                            <table class="table table-bordered ps-table ps-table--specification">
                                <tbody>
                                    <tr>
                                        <td>Booking id</td>
                                        <td>{{ $claimed->unique_code }}</td>
                                    </tr>
                                    <tr>
                                        <td>About Event</td>
                                        <td>{!! $event->description !!}</td>
                                    </tr>
                                    <tr>
                                        <td>Brought on</td>
                                        <td>{{ date('d-m-Y h:i A',strtotime($claimed->created_at)) }}</td>
                                    </tr>
                                    <tr>
                                        <td>Event Date</td>
                                        <td>{{ date('d-m-Y',strtotime($event->event_date)) }}</td>
                                    </tr>
                                    <tr>
                                        <td>Event Time</td>
                                        <td>{{ date('h:i A',strtotime($event->event_time)) }}</td>
                                    </tr>
                                    <tr>
                                        <td>Number of Tickets</td>
                                        <td>{{ $order->quantity }}</td>
                                    </tr>
                                    <tr>
                                        <td>Price</td>
                                        <td>{{ $order->quantity }} x {{ $event->price }} = {{ $order->quantity *  $event->price }}</td>
                                    </tr>
                                </tbody>
                            
                            </table>
                        </div>
                    </div>
                </div>

                <h3 class="ps-product__heading pt-5">Terms & Conditions</h3>
                <div class="ps-document">
                {!! $event->terms_conditions !!}
                </div>
            </div>

        </div>
    </div> -->
@endsection
