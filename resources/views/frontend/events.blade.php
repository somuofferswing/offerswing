@extends('layouts.app')



@section('content')



<style>
    /* card */

    .ps-post .ps-post__thumbnail .ps-post__badge {

        border-radius: 50%;

        background-color: white;

        color: #dc3545;

        cursor: pointer;

    }



    .ps-post .ps-post__badge i {

        color: #dc3545;

    }
</style>



<div class="ps-breadcrumb">

    <div class="container">

        <ul class="breadcrumb text-capitalize" id="breadcrumb">

            <li><a href="{{ url('/') }}">Home</a></li>

            <li>Events</li>

        </ul>

    </div>

</div>

<section>

    <div class="ps-page--blog">

        <div class="container">



            <div class="ps-page__header">

                <h1>Events</h1>

                <hr class="highlighter">

            </div>



            <div class="ps-blog">

                <div class="ps-blog__content">

                    <div class="row">

                        @foreach ($events as $item)

                        <div class="col-xl-4 col-lg-4 col-md-6 col-sm-12 col-12 ">

                            <div class="ps-post">

                                <div class="ps-post__thumbnail">

                                    <a class="ps-post__overlay" href="{{ url('eventdetails',[$item->slug])}}"></a>

                                    <img src="{{ asset('storage/'.$item->event_image)  }}" alt="">

                                    <!-- <div class="ps-post__badge">

                                        @if(Auth::guest())

                                            <span class="zoom" onClick="return false;" data-id="{{ $item->id }}"  data-toggle="modal" data-target="#myModal1">

                                                <span data-toggle="tooltip" data-placement="right" title="" data-original-title="Like">

                                                <i class="fa fa-heart-o fa-lg"></i>

                                                </span>

                                            </span>

                                        @else

                                            @if (in_array($item->id,$likes))

                                                <span class="zoom  unlike" data-id="{{ $item->id }}" data-toggle="tooltip" data-placement="right" title="" data-original-title="Unlike">

                                                    <i class="fa fa-heart fa-lg"></i>

                                                </span>

                                            @else

                                                <span class="zoom like" data-id="{{ $item->id }}" data-toggle="tooltip" data-placement="right" title="" data-original-title="Like">

                                                    <i class="fa fa-heart-o fa-lg"></i>

                                                </span>

                                            @endif

                                        @endif

                                    </div> -->

                                </div>

                                <div class="ps-post__content">

                                    <div class="ps-post__meta">

                                        <span>

                                            <i class="fa fa-calendar pr-2"></i>{{ $item->event_date}}

                                            <i class="fa fa-clock-o pr-2"></i>{{ $item->event_time}}

                                        </span>

                                        <span class="pull-right text-danger">

                                            @if ($item->payment_type == 'cash')
                                                &#x20b9 {{ $item->event_price }} per each
                                            @endif

                                            @if ($item->payment_type == 'wingpoints')
                                                 {{ $item->points_to_claim }} wingpoints per each
                                            @endif


                                        </span>

                                    </div>

                                    <a class="ps-post__title text-truncate" href="{{ url('eventdetails',[$item->id])}}">

                                        {{ ucfirst($item->title) }}



                                    </a>

                                    <div class="text-center">

                                        <a href="{{ url('eventdetails',[$item->slug])}}" class="ps-btn btn-sm"> Book
                                            Now</a>

                                    </div>



                                </div>

                            </div>

                        </div>

                        @endforeach

                    </div>

                </div>

            </div>



        </div>

    </div>

</section>





<!-- <a class="btn btn-primary" >Trigger modal</a> -->

<div class="modal fade" id="share">

    <div class="modal-dialog">

        <div class="modal-content">

            <div class="modal-body">

                <div class="a2a_kit a2a_kit_size_32 a2a_default_style">

                    {{-- <a class="a2a_dd" href="https://www.addtoany.com/share"></a> --}}

                    <a class="a2a_button_facebook"></a>

                    <a class="a2a_button_twitter"></a>

                    <a class="a2a_button_email"></a>

                    <a class="a2a_button_whatsapp"></a>

                </div>

                <script async src="https://static.addtoany.com/menu/page.js"></script>

            </div>

        </div>

    </div>

</div>



<script>
    $(document).on('click', ".unlike", function (e) {

        e.preventDefault();

        var id = $(this).data('id');

        var type = "event";

        var element = this;



        var data = {

                id:id,

                type:type

            };

        $.ajax({

                type: "post",

                url: '{{ url("removelike") }}',

                headers: {

                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')

                    },

                data: data,

                cache: false,

                success: function (data)

                {

                    if(data.status == "success"){

                        $(element).removeClass('unlike').addClass('like').attr('data-original-title', 'Like').html('<i class="fa fa-heart-o"></i>');

                    }

                },

                error: function (data){

                    swal('Fail to run unlike..');

                }

            });

        });



        $(document).on('click', ".like", function (e) {

            e.preventDefault();

            var id = $(this).data('id');

            var type = "event";

            var element = this;



            var data = {

                    id:id,

                    type:type

                };

            $.ajax({

                    type: "post",

                    url: '{{ url("addlike") }}',

                    headers: {

                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')

                     },

                    data: data,

                    cache: false,

                    success: function (data)

                    {

                        if(data.status == "success"){

                                $(element).removeClass('like').addClass('unlike').attr('data-original-title', 'Unlike').html('<i class="fa fa-heart"></i>');



                        }

                    },

                    error: function (data){

                        swal('Fail to run like..');

                    }

                });





        });

</script>





@endsection