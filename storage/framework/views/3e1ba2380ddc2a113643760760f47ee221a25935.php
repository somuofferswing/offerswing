<?php $__env->startSection('content'); ?>
<div class="container">
    
        <div class="col-md-8 col-md-offset-2">
            <div class="panel panel-default">
                <div class="panel-heading"><h3><?php echo e(__('Register')); ?></h3> </div>

                <div class="panel-body">
                    <form method="POST" action="<?php echo e(route('register')); ?>">
                        <?php echo csrf_field(); ?>
                                <input type="hidden" id="user_type" name="user_type" value="2">
                                <div id="signupalert" style="display:none" class="alert alert-danger">
                                    <p>Error:</p>
                                    <span></span>
                                </div>


                                <div class="form-group">
                                    <label for="firstname" class="col-md-3 control-label"><?php echo e(__('First Name')); ?></label>

                                    <div class="col-md-9">
                                        <input type="text" id="firstname"
                                            class="form-control <?php if ($errors->has('first_name')) :
if (isset($message)) { $messageCache = $message; }
$message = $errors->first('first_name'); ?> is-invalid <?php unset($message);
if (isset($messageCache)) { $message = $messageCache; }
endif; ?>"
                                            name="first_name" value="<?php echo e(old('first_name')); ?>" required
                                            autocomplete="first_name" autofocus>
                                        <?php if ($errors->has('name')) :
if (isset($message)) { $messageCache = $message; }
$message = $errors->first('name'); ?>
                                        <span class="invalid-feedback" role="alert">
                                            <strong><?php echo e($message); ?></strong>
                                        </span>
                                        <?php unset($message);
if (isset($messageCache)) { $message = $messageCache; }
endif; ?>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label for="lastname" class="col-md-3 control-label"><?php echo e(__('Last Name')); ?></label>
                                    <div class="col-md-9">
                                        <input type="text" id="lastname"
                                            class="form-control <?php if ($errors->has('last_name')) :
if (isset($message)) { $messageCache = $message; }
$message = $errors->first('last_name'); ?> is-invalid <?php unset($message);
if (isset($messageCache)) { $message = $messageCache; }
endif; ?>"
                                            name="last_name" value="<?php echo e(old('last_name')); ?>" required autocomplete="last_name"
                                            autofocus>

                                        <?php if ($errors->has('name')) :
if (isset($message)) { $messageCache = $message; }
$message = $errors->first('name'); ?>
                                        <span class="invalid-feedback" role="alert">
                                            <strong><?php echo e($message); ?></strong>
                                        </span>
                                        <?php unset($message);
if (isset($messageCache)) { $message = $messageCache; }
endif; ?>
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label for="mobile" class="col-md-3 control-label"><?php echo e(__('Mobile')); ?></label>
                                    <div class="col-md-9">
                                        <input type="text" id="mobile"
                                            class="form-control <?php if ($errors->has('mobile')) :
if (isset($message)) { $messageCache = $message; }
$message = $errors->first('mobile'); ?> is-invalid <?php unset($message);
if (isset($messageCache)) { $message = $messageCache; }
endif; ?>"
                                            name="mobile" value="<?php echo e(old('mobile')); ?>" required autocomplete="mobile"
                                            autofocus>

                                        <?php if ($errors->has('name')) :
if (isset($message)) { $messageCache = $message; }
$message = $errors->first('name'); ?>
                                        <span class="invalid-feedback" role="alert">
                                            <strong><?php echo e($message); ?></strong>
                                        </span>
                                        <?php unset($message);
if (isset($messageCache)) { $message = $messageCache; }
endif; ?>
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label for="email" class="col-md-3 control-label"><?php echo e(__('E-Mail Address')); ?></label>
                                    <div class="col-md-9">
                                        <input type="email" id="email"
                                            class="form-control <?php if ($errors->has('email')) :
if (isset($message)) { $messageCache = $message; }
$message = $errors->first('email'); ?> is-invalid <?php unset($message);
if (isset($messageCache)) { $message = $messageCache; }
endif; ?>" name="email"
                                            value="<?php echo e(old('email')); ?>" required autocomplete="email">
                                        <?php if ($errors->has('email')) :
if (isset($message)) { $messageCache = $message; }
$message = $errors->first('email'); ?>
                                        <span class="invalid-feedback" role="alert">
                                            <strong><?php echo e($message); ?></strong>
                                        </span>
                                        <?php unset($message);
if (isset($messageCache)) { $message = $messageCache; }
endif; ?>
                                    </div>
                                </div>


                                <div class="form-group">
                                    <label for="dob" class="col-md-3 control-label"><?php echo e(__('DOB')); ?></label>
                                    <div class="col-md-9">
                                        <input class="form-control datepicker" type="text" id="dob"name="dob" value="<?php echo e(old('dob')); ?>" required>
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label for="password_confirm"
                                        class="col-md-3 control-label"><?php echo e(__('Gender')); ?></label>
                                    <div class="col-md-9">
                                        <input type="radio"  class="radio-inline"
                                            name="gender" required value="MALE">Male
                                        <input type="radio"  class="radio-inline"
                                            name="gender" required value="FEMALE">Female
                                        <input type="radio"  class="radio-inline"
                                            name="gender" required value="OTHERS">Other
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label for="password" class="col-md-3 control-label"><?php echo e(__('Password')); ?></label>
                                    <div class="col-md-9">
                                        <input type="password" id="password"
                                            class="form-control <?php if ($errors->has('password')) :
if (isset($message)) { $messageCache = $message; }
$message = $errors->first('password'); ?> is-invalid <?php unset($message);
if (isset($messageCache)) { $message = $messageCache; }
endif; ?>" name="password"
                                            required autocomplete="new-password">

                                        <?php if ($errors->has('password')) :
if (isset($message)) { $messageCache = $message; }
$message = $errors->first('password'); ?>
                                        <span class="invalid-feedback" role="alert">
                                            <strong><?php echo e($message); ?></strong>
                                        </span>
                                        <?php unset($message);
if (isset($messageCache)) { $message = $messageCache; }
endif; ?>
                                    </div>
                                </div>


                                <div class="form-group">
                                    <label for="password_confirm"
                                        class="col-md-3 control-label"><?php echo e(__('Confirm Password')); ?></label>
                                    <div class="col-md-9">
                                        <input type="password" id="password_confirm" class="form-control"
                                            name="password_confirmation" required autocomplete="new-password">
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label for="referal_code" class="col-md-3 control-label">Referal Code</label>
                                    <div class="col-md-9">
                                        <input type="text" id="referal_code" class="form-control"
                                            name="referal_code" value="<?php echo e(old('referal_code')); ?>">
                                    </div>
                                </div>

                                <div class="form-group">
                                    <!-- Button -->
                                    <div class="col-md-offset-3 col-md-9">
                                        <button id="btn-signup" type="submit" class="btn btn-info"><i
                                                class="icon-hand-right"></i> &nbsp Sign Up</button>
                                        
                                    </div>
                                </div>
                    </form>
                </div>
            </div>
        </div>
    
</div>
<?php $__env->stopSection(); ?>

<?php echo $__env->make('layouts.app', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH C:\wamp64\www\offers\resources\views/auth/register.blade.php ENDPATH**/ ?>