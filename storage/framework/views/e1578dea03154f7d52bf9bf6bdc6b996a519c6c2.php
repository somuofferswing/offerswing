<?php $__env->startSection('content'); ?>
<style>
    #contactus {
        background-image: url("https://blueskyitconsulting.com.au/wp-content/uploads/2013/05/bg-02.jpg");
        padding-top: 20px;
    }

    .bck {
        background: #ffffff7a;
        -moz-box-shadow: 0 0 0px #888;
        -webkit-box-shadow: 0 0 0px#888;
        box-shadow: 0 0 0px #888;
    }
</style>


<div class="container-fluid" id="contactus">
    <nav aria-label="breadcrumb">
        <ol class="breadcrumb">
            <li class="breadcrumb-item"><a href="#">Home</a></li>
            <li class="breadcrumb-item active" aria-current="page">Contact Us</li>
        </ol>
    </nav>
    <div class="container">
        <div class="row bck" style="padding:10px;border-radius: 15px;">

            

            <!-- Section Titile -->
            <div class="col-md-5 mt-3 contact-widget-section2 wow animated fadeInLeft" data-wow-delay=".2s">
                <p style="font-size:25px;color:#d73e44">
                    Get in Touch with us
                </p>
                <p style="font-size:20px;">Meet Us here</p>
                <div class="find-widget">
                    <b>Company : </b> <a href="#">Easywing Pvt. Ltd.</a>
                </div>

                <div class="find-widget">
                    
                    <address><b><i class="fa fa-address-card-o" aria-hidden="true"></i> Address :</b> D No:6-8-4 Jabba
                        Street, Near Krishna Park, <br>
                        Oppo: KVB bank, Krishna Park Area,Ranasthali, New Colony, <br>
                        Srikakulam, Andhra Pradesh 532001
                    </address>
                </div>

                <p style="font-size:20px;"> Contact us on</p>
                <div class="find-widget">
                    <b><i class="fa fa-phone" aria-hidden="true"> +91 97049 89697</i> </b>
                </div>

                <div class="find-widget">
                    <b><i class="fa fa-whatsapp" aria-hidden="true"> +91 9704 04 9596</i> </b>
                </div>

                <p style="font-size:20px;">Mail us on</p>
                <div class="find-widget">
                    <b><i class="fa fa-envelope" aria-hidden="true"> info@offerswing.com</i></b>
                </div>


            </div>
            <!-- contact form -->
            <div class="col-md-7 wow animated fadeInRight" data-wow-delay=".2s">
                <p style="font-size:25px;color:#d73e44">
                    Please fill out the following from to get queries.
                </p>
                <form class="shake" role="form" action="<?php echo e(url('contact_post')); ?>" method="post" id="contactForm"
                    name="contact-form" data-toggle="validator">
                    <?php echo csrf_field(); ?>
                    <!-- Name -->
                    <input type="hidden" name="w_email" value="offerswing@gmail.com">
                    <div class="form-group label-floating">
                        <label class="control-label" for="name"></label>
                        <input class="form-control" id="name" type="text" name="name" required
                            data-error="Please enter your name" placeholder=" Your Name*">
                        <div class="help-block with-errors"></div>
                    </div>
                    <!-- Phone -->
                    <div class="form-group label-floating">
                        <label class="control-label" for="phone"></label>
                        <input class="form-control" id="phone" type="phone" name="phone" required
                            data-error="Please enter your Phone" placeholder="Your Phone Number*">
                        <div class="help-block with-errors"></div>
                    </div>
                    <!-- email -->
                    <div class="form-group label-floating">
                        <label class="control-label" for="email"></label>
                        <input class="form-control" id="email" type="email" name="email" required
                            data-error="Please enter your Email" placeholder="Your Email*">
                        <div class="help-block with-errors"></div>
                    </div>
                    <!-- Subject -->
                    <div class="form-group label-floating">
                        <label class="control-label"></label>
                        <input class="form-control" id="msg_subject" type="text" name="subject" required
                            data-error="Please enter your message subject" placeholder="Subject">
                        <div class="help-block with-errors"></div>
                    </div>
                    <!-- Message -->
                    <div class="form-group label-floating">
                        <label for="message" class="control-label"></label>
                        <textarea class="form-control" rows="3" id="message" name="message" required
                            data-error="Write your message" placeholder="Message*"></textarea>
                        <div class="help-block with-errors"></div>
                    </div>
                    <!-- Form Submit -->
                    <div class="col-md-6"></div>
                    <div class="col-md-3"></div>
                    <div class="col-md-3">
                        <div class="form-submit mt-5">
                            <button class="btn btn-danger" type="submit" id="form-submit"><i
                                    class="material-icons mdi mdi-message-outline"></i> Send Message</button>
                            <div id="msgSubmit" class="h3 text-center hidden"></div>
                            <div class="clearfix"></div>
                        </div>
                    </div>
                </form>
            </div>
        </div>
        <h3><strong>
                <font color="#ffffff">Locate us on</font>
            </strong></h3>

        <div class="row" style="padding:10px;border-radius: 15px;">
            <iframe class="mgin1"
                src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3788.0543204118726!2d83.89259651489675!3d18.29912388068506!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x3a3c156b1b0f18e9%3A0xfb3a50c82c88f058!2sEasywing+Private+Limited!5e0!3m2!1sen!2sin!4v1562417480601!5m2!1sen!2sin"
                width="100%" height="450" frameborder="0" style="border:0;margin-top:20px; border-radius: 15px;"
                allowfullscreen></iframe>
        </div>
    </div>
</div>


<?php $__env->stopSection(); ?>

<?php echo $__env->make('layouts.app', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH C:\wamp64\www\offers\resources\views/contactus.blade.php ENDPATH**/ ?>