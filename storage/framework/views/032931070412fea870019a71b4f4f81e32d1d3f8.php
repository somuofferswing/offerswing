<style>
    sup {
        color: #bd2130;
        font-size: 0.4rem;
        top: -0.8rem;
    }
</style>
<div class="container-fluid" style="background-color: #66696b;color:white">
    <div class="row py-4 align-items-center text-center">
        <div class="col-md-4 border-right">
            <h4>Create Event / Deal</h4>
            <h5>+91 9704 98 96 97</h5>
        </div>
        <div class="col-md-4 border-right ">
            <h4>Follow us</h4>
            <div class="icons" style="display:inline-flex;">
                <ul class="list-inline social-nav">
                    <li class="list-inline-item">
                        <a href="https://www.facebook.com/Offerswing-378561272765031/" target="_blank" title="Facebook"
                            rel="nofollow" class="facebook"><i class="fab fa-facebook"
                                style="padding-top: 5px;"></i></a>
                    </li>
                    <li class="list-inline-item">
                        <a href="https://twitter.com/offerswing2" target="_blank" title="Twitter" rel="nofollow"
                            class="twitter"><i class="fab fa-twitter" style="padding-top: 5px;"></i></a></li>

                    <li class="list-inline-item">
                        <a href="https://www.instagram.com/offerswing2019/" target="_blank" title="instagram"
                            rel="nofollow" class="google"><i class="fab fa-instagram" style="padding-top: 5px;"></i></a>
                    </li>

                </ul>
            </div>
        </div>
        <div class="col-md-4 col-xs-12 ">

            <h4 style="text-align:center;padding-left: 13px;">Stay connected with us</h4>
            <img src="<?php echo e(asset('main/images/whats.png')); ?>" class="img-fluid" style="height:30px;margin:0 auto;" />
        </div>
    </div>
</div>
<div class="container-fluid" style="background-color: #000 !important;">
    <div class="row py-4 align-items-center text-center">
        <div class="col-md-4">

            <h4 style="color:white;">Company</h4>
            <ul class="nav flex-column border-right footer_link" >
                <li class="nav-item"><a class="nav-link" href="<?php echo e(url("aboutus")); ?>">About us</a></li>
                <li class="nav-item"><a class="nav-link" href="<?php echo e(url("page",['terms_conditions'])); ?>">Terms &
                        Conditions</a></li>
                <li class="nav-item"><a class="nav-link" href="<?php echo e(url("page",['privacy_policy'])); ?>">Privacy Policy</a>
                </li>
            </ul>
        </div>
        <div class="col-md-4 border-right">
            <h4 style="color:white;">Help</h4>
            <ul class="nav flex-column footer_link">
                <li class="nav-item"><a class="nav-link" href="<?php echo e(url("contactus")); ?>">Contact us</a></li>
                <li class="nav-item"><a class="nav-link" href="#">Give Us Feedback</a></li>
            </ul>
        </div>

        <div class="col-md-4">

            <h4 style="color:white;">Download app</h4>
            <ul class="nav nav-fill footer_link">
                <li class="nav-item">
                    <a href="" class="nav-link"> <img src="<?php echo e(asset('main/images/PlayStore.png')); ?>" class="img-fluid"
                            style="height:30px"></a>
                    <span style="font-size: 10px;color:white;">Google Play<span>
                </li>
                <li class="nav-item">
                    <a href="" class="nav-link"><img src="<?php echo e(asset('main/images/apple-app-store-icon.png')); ?>"
                            class="img-fluid" style="height:30px"></a>
                    <span style="font-size: 10px;color:white;">App Store<span>
                </li>

            </ul>
            <br>

        </div>

    </div>
    <div class="col-md-4 text-left" style="color:white;">
        <p class="mb-0">Copyright@ Offerswing</p>
    </div>

</div>

<div id="stop" class="scrollTop">
    <span><a href=""><i class="fa fa-arrow-up"></i></a></span>
</div>


<!-- loginmodel-->

<div class="modal fade" id="myModal1" role="dialog">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title" id="modal-title">Login</h4>
                <span class="ml-auto">
                    <sup><i class="fas fa-star"></i></sup> indicates Required
                </span>

                <button type="button" class="close" data-dismiss="modal">&times;</button>
            </div>

            <div class="modal-body">
                <div id="loginbox">
                    <div style="display:none" id="login-alert" class="alert alert-danger col-sm-12"></div>

                    <form id="loginform" class="form-horizontal" onsubmit="return LoginUser()" role="form">

                        <?php echo csrf_field(); ?>
                        <input type="hidden" id="login_user_type" name="user_type" value="2">
                        <div class="form-group row">
                            <label for="login_email" class="col-md-3 col-form-label"><?php echo e(__('Email')); ?> <sup><i
                                        class="fas fa-star"></i></sup></label>
                            <div class="col-md-9">
                                <input id="login_email" type="email" class="form-control" name="email"
                                    placeholder="Email">
                            </div>
                            <span id="login_email_error"></span>
                        </div>

                        <div class="form-group row">
                            <label for="login_password" class="col-md-3 col-form-label"><?php echo e(__('Password')); ?> <sup><i
                                        class="fas fa-star"></i></sup></label>
                            <div class="col-md-9">
                                <input id="login_password" type="password" class="form-control" name="password"
                                    placeholder="Password">
                                <span id="login_password_error"></span>
                            </div>
                        </div>

                        <div class="form-group row">
                            <div class="col-sm-3">
                            </div>
                            <div class="col-sm-9">
                                <div class="input-group">
                                    <div class="checkbox">
                                        <label>
                                            <input id="login-remember" type="checkbox" name="remember" value="1">
                                            Remember me
                                        </label>
                                    </div>
                                </div>

                                <div class="form-group row pt-3">
                                    <div class="col-sm-12">
                                        <button type="submit" class="btn btn-success">Signin </button>
                                        <button class="btn btn-primary login" type="button" id="show_otpbox">Signin with
                                            OTP
                                        </button>
                                        <button id="show_forgot" type="button" class="btn btn-default login">Forgot
                                            Password?</button>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <hr>
                        Don't have an account! <a href="#" id="show_signup" class="login"> Sign Up Here </a>
                    </form>
                </div>

                <div id="forgotpassword" style="display:none;">

                    <div id="reset_status" class="alert"> </div>
                    <form id="forgotpwd" class="form-horizontal" onsubmit="return forgotPassword()" role="form">
                        <div class="form-group row">
                            <label for="reset_email" class="col-sm-3 col-form-label">Email Address <sup><i
                                        class="fas fa-star"></i></sup> </label>
                            <div class="col-sm-9">
                                <input id="reset_email" type="text" class="form-control" name="reset_email" required
                                    placeholder="Please Enter your Email">
                            </div>
                        </div>


                        <div class="form-group row">
                            <div class="col-sm-3">
                            </div>
                            <div class="col-sm-9">
                                <button type="submit" class="btn btn-info">Send Password Reset Link</button>
                            </div>
                        </div>
                        <hr>
                        Need to Sign in ?
                        <a id="show_login" href="#" class="login">Sign In</a>

                    </form>
                </div>

                <div id="signupbox" style="display:none;">
                    <div id="sigupstatus" class="alert"></div>
                    <form id="signupform" class="form-horizontal" method="POST" onsubmit="return SignupUser()"
                        role="form">
                        <?php echo csrf_field(); ?>
                        <input type="hidden" id="register_user_type" name="user_type" value="2">
                        <div class="form-group row">
                            <label for="register_firstname" class="col-md-3 col-form-label"><?php echo e(__('First Name')); ?>

                                <sup><i class="fas fa-star"></i></sup></label>

                            <div class="col-md-9">
                                <input type="text" id="register_firstname" class="form-control" name="first_name"
                                    placeholder="First Name" required>
                                <span id="firstname_error"></span>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="register_lastname" class="col-md-3 col-form-label"><?php echo e(__('Last Name')); ?> <sup><i
                                        class="fas fa-star"></i></sup></label>
                            <div class="col-md-9">
                                <input type="text" id="register_lastname" class="form-control" name="last_name"
                                    placeholder="Last Name" required>
                                <span id="lastname_error"></span>
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="register_mobile" class="col-md-3 col-form-label"><?php echo e(__('Mobile')); ?> <sup><i
                                        class="fas fa-star"></i></sup></label>
                            <div class="col-md-9">
                                <input type="text" id="register_mobile" class="form-control " name="mobile" required
                                    placeholder="Mobile Number">
                                <span id="mobile_error"></span>
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="register_email" class="col-md-3 col-form-label"><?php echo e(__('E-Mail Address')); ?>

                                <sup><i class="fas fa-star"></i></sup></label>
                            <div class="col-md-9">
                                <input type="email" id="register_email" class="form-control" name="email" required
                                    placeholder="Email">
                                <span id="email_error"></span>
                            </div>
                        </div>


                        <div class="form-group row">
                            <label for="register_dob" class="col-md-3 col-form-label"><?php echo e(__('DOB')); ?> <sup><i
                                        class="fas fa-star"></i></sup></label>
                            <div class="col-md-9">
                                <input class="form-control datepicker" type="text" id="register_dob" name="dob" required
                                    placeholder="DD-MM-YYYY">
                                <span id="dob_error"></span>
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="register_password_confirm" class="col-md-3 col-form-label"><?php echo e(__('Gender')); ?>

                                <sup><i class="fas fa-star"></i></sup></label>
                            <div class="col-md-9">
                                <input type="radio" class="radio-inline" name="register_gender" required value="MALE">
                                Male
                                <input type="radio" class="radio-inline" name="register_gender" required value="FEMALE">
                                Female
                                <input type="radio" class="radio-inline" name="register_gender" required value="OTHERS">
                                Other
                                <span id="gender_error"></span>
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="register_password" class="col-md-3 col-form-label"><?php echo e(__('Password')); ?> <sup><i
                                        class="fas fa-star"></i></sup></label>
                            <div class="col-md-9">
                                <input type="password" id="register_password" class="form-control" name="password"
                                    required placeholder="Password">
                                <span id="password_error"></span>
                            </div>
                        </div>


                        <div class="form-group row">
                            <label for="register_password_confirm"
                                class="col-md-3 col-form-label"><?php echo e(__('Confirm Password')); ?> <sup><i
                                        class="fas fa-star"></i></sup></label>
                            <div class="col-md-9">
                                <input type="password" id="register_password_confirm" class="form-control"
                                    name="password_confirmation" required placeholder="Confirm Password">
                                <span id="password_error"></span>
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="register_referal_code" class="col-md-3 col-form-label">Referal
                                Code</label>
                            <div class="col-md-9">
                                <input type="text" id="register_referal_code" class="form-control" name="referal_code"
                                    placeholder="Referal Code (Optional)">
                            </div>
                        </div>

                        <div class="form-group row">
                            <div class="col-sm-3">

                            </div>
                            <div class="col-sm-3">
                                <button id="btn-signup" type="submit" class="btn btn-success"> Sign Up</button>
                            </div>
                        </div>
                        <hr>
                        Already had an Account? <a href="#" class="login" id="show_login">Sign In</a>
                    </form>
                </div>

                <div id="otpbox" style="display:none;">

                    <div id="otpStatus" class="alert"></div>

                    <form id="otpform" class="form-horizontal" onsubmit="return SendOtp()" role="form">

                        <?php echo csrf_field(); ?>
                        <div class="form-group row">
                            <label for="otp_mobile" class="col-md-3 col-form-label">Mobile Number <sup><i
                                        class="fas fa-star"></i></sup></label>
                            <div class="col-md-9">
                                <input id="otp_mobile" type="number" class="form-control" name="otp_mobile"
                                    placeholder="Please Enter Mobile Number">
                                <span id="otp_mobile_error"></span>
                            </div>
                        </div>

                        <div class="form-group row" id="sendotptomobile">
                            <div class="col-sm-3">

                            </div>
                            <div class="col-sm-9">
                                <button type="submit" class="btn btn-info">Send Otp</button>

                                <button type="button" class=" btn btn-primary login" id="show_login">Signin with
                                    Password</button>
                            </div>

                        </div>
                    </form>


                    <form id="verifyotp" class="form-horizontal" onsubmit="return Otpverify()" role="form"
                        style="display:none">

                        <div class="form-group row">
                            <label for="otp" class="col-md-3 col-form-label">OTP <sup><i
                                        class="fas fa-star"></i></sup></label>
                            <div class="col-md-9">
                                <input id="otp" type="number" class="form-control" name="otp"
                                    placeholder="PLease Enter OTP">
                                <span id="login_password_error"></span>
                            </div>
                        </div>

                        <div class="form-group row">
                            <div class="col-sm-3">

                            </div>
                            <div class="col-sm-9">
                                <button type="submit" class="btn btn-success">Verify </button>
                                <button type="button" id="resendotp" class="btn btn-warning" disabled
                                    onClick="resendOTP()">Resend OTP</button>
                                <span id="myTimer"></span>
                            </div>

                        </div>

                        <hr>
                        Don't have an account!
                        <a href="#" id="show_signup" class="login">
                            Sign Up Here
                        </a>
                    </form>
                </div>
            </div>

        </div>
    </div>
</div>
<!-- loginmodel-->

<!-- alertmodel -->
<!-- Button trigger modal -->


<!-- Modal -->
<div class="modal fade" id="modal-alert" tabindex="-1" role="dialog" aria-labelledby="modelTitleId" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title">Modal title</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
            </div>
            <div class="modal-body">
                <p id="alert_message" class="alert alert-<?php echo e(session('class')); ?>">
                    <?php echo e(session('alert')); ?>

                </p>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                <button type="button" class="btn btn-primary">Save</button>
            </div>
        </div>
    </div>
</div>
<!-- alertmodel -->
<script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-3-typeahead/4.0.1/bootstrap3-typeahead.min.js"></script>
<script>
    $(document).ready(function() {
        var scrollTop = $(".scrollTop");

        $(window).scroll(function() {
            var topPos = $(this).scrollTop();
            if (topPos > 100) {
                $(scrollTop).css("opacity", "1");
            } else {
                $(scrollTop).css("opacity", "0");
            }
        });
    $(scrollTop).click(function() {
        $('html, body').animate({
        scrollTop: 0
        }, 800);
        return false;
    });
});
</script>
<script>
    $(document).ready(function(){
        var now = new Date();
        var hrs = now.getHours();

        if (hrs >  6) { $('#greetings').text("").text("Morning") };
        if (hrs > 12) { $('#greetings').text("").text("Afternoon") };
        if (hrs > 17) { $('#greetings').text("").text("Evening") };

        var alert1 = <?php echo e(Session::has('alert') ? true : 0); ?>;
        if(alert1){
            $('#modal-alert').modal('show');
        }
    });

    $('.login').on('click',function(){

            var id = $(this).attr('id');

            if(id=="show_signup"){
                $('#loginbox, #otpbox, #forgotpassword').hide();
                $('#signupbox').show();
                $('#modal-title').html('').html('Sign up');
            }

            if(id=="show_login"){
                $('#signupbox,#otpbox,#forgotpassword').hide();
                $('#loginbox').show();
                $('#modal-title').html('').html('Sign In');
            }

            if(id=="show_forgot"){
                $('#signupbox,#otpbox,#loginbox').hide();
                $('#forgotpassword').show();
                $('#modal-title').html('').html('Reset Password');
            }

            if(id=="show_otpbox"){
                $('#signupbox,#forgotpassword,#loginbox').hide();
                $('#otpbox').show();
                $('#modal-title').html('').html('Sign in Using OTP');
            }

    });
</script>
<script>
    function LoginUser()
        {
            var email    = $("#login_email").val();
            var user_type    = $("#login_user_type").val();
            var password = $("#login_password").val();
            var data = {
                email:email,
                password:password,
                user_type: user_type
            };
            // Ajax Post
            $.ajax({
                type: "post",
                url: '<?php echo e(url("customlogin")); ?>',
                headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                 },
                data: data,
                cache: false,
                success: function (data)
                {
                    if(data.status == "success"){
                        console.log(data);
                      location.reload();

                    }else{
                        console.log(data);
                        alert('Please!check Your credentials');
                        console.log('login request sent !');
                        console.log('status: ' +data.status);
                        console.log('message: ' +data.message);
                    }
                },
                error: function (data){

                    console.log(data);
                    console.log(data.responseJSON.errors);
                    var errors = data.responseJSON.errors;

                    // if(errors.mobile){
                    //     $('#mobile_error').addClass('text-danger');
                    //     $('#mobile_error').html(errors.mobile[0]);
                    // }

                    // if(errors.email){
                    //     $('#login_email_error').addClass('text-danger');
                    //     $('#login_email_error').html(errors.email[0]);
                    // }

                    // if(errors.password){
                    //     $('#login_password_error').addClass('text-danger');
                    //     $('#login_password_error').html(errors.password[0]);
                    // }
                }
            });
            return false;
        }

        function SignupUser()
        {
            // next code goes here...
            var first_name       = $("#register_firstname").val();
            var last_name       = $("#register_lastname").val();
            var email       = $("#register_email").val();
            var password       = $("#register_password").val();
            var mobile       = $("#register_mobile").val();
            var dob       = $("#register_dob").val();
            var gender       = $("input[name='register_gender']:checked").val();
            var referal_code       = $("#register_referal_code").val();
            var user_type       = $("#register_user_type").val();
            var password_confirmation       = $("#register_password_confirm").val();
            // garnish the data
            var data = {
                first_name:first_name,
                last_name:last_name,
                email:email,
                password:password,
                referal_code:referal_code,
                user_type:user_type,
                password_confirmation:password_confirmation,
                mobile:mobile,
                dob:dob,
                gender:gender
            };
            // ajax post
            $.ajax({
                type: "post",
                url: '<?php echo e(url("customregister")); ?>',
                headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                 },
                data: data,
                cache: false,
                success: function (data){
                    console.log(data);

                    message = data.message;
                    if(data.status == "Success"){
                        $('#sigupstatus').addClass("alert-success").text(message);
                        location.reload();
                    }else{
                        $('#otpStatus').addClass("alert-danger").text(message);
                        console.log(data);
                    }

                    // $('#otpverify').show();
                    // $('#signupbox').hide();
                    // $('#loginbox').hide();

                    // location.reload();
                },
                error: function (data){
                    // console.log(data);
                    // console.log(data.responseJSON.errors);

                    var errors = data.responseJSON.errors;

                    if(errors.mobile){
                        $('#mobile_error').addClass('text-danger');
                        $('#mobile_error').html(errors.mobile[0]);
                    }

                    if(errors.email){
                        $('#email_error').addClass('text-danger');
                        $('#email_error').html(errors.email[0]);
                    }

                    if(errors.password){
                        $('#password_error').addClass('text-danger');
                        $('#password_error').html(errors.password[0]);
                    }
                }
            });
            return false;
        }


            var sec = 59;
            var myTimer = document.getElementById('myTimer');
            var myBtn = document.getElementById('resendotp');

            function countDown() {
            if (sec < 10) {
                myTimer.innerHTML = "00:0" + sec;
            } else {
                myTimer.innerHTML = "00:"+sec;
            }
            if (sec <= 0) {
                $("#resendotp").removeAttr("disabled");
                $("#myTimer").fadeTo(2500, 0);
                return;
            }
            sec -= 1;
            window.setTimeout(countDown, 1000);
            }



            function resendOTP()
        {
            var mobile    = $("#otp_mobile").val();

            var data = {
                mobile:mobile
            };
            // Ajax Post
            $.ajax({
                type: "post",
                url: '<?php echo e(url("resendOTP")); ?>',
                headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                 },
                data: data,
                cache: false,
                success: function (data)
                {
                    message = data.message;
                    if(data.status == "Success"){
                        $('#otpStatus').addClass("alert-success").text(message);
                        $('#otp_mobile').attr('disabled','disabled');

                        $('#sendotptomobile').hide();
                        $('#verifyotp').show();
                        countDown();

                    }else{
                        $('#otpStatus').addClass("alert-danger").text(message);
                        console.log(data);
                    }
                },
                error: function (data){

                    console.log(data);
                    console.log(data.responseJSON.errors);
                    var errors = data.responseJSON.errors;

                    // if(errors.mobile){
                    //     $('#mobile_error').addClass('text-danger');
                    //     $('#mobile_error').html(errors.mobile[0]);
                    // }

                    if(errors.email){
                        $('#login_email_error').addClass('text-danger');
                        $('#login_email_error').html(errors.email[0]);
                    }

                    if(errors.password){
                        $('#login_password_error').addClass('text-danger');
                        $('#login_password_error').html(errors.password[0]);
                    }
                }
            });
            return false;
        }



        function SendOtp()
        {
            var mobile    = $("#otp_mobile").val();

            var data = {
                mobile:mobile
            };
            // Ajax Post
            $.ajax({
                type: "post",
                url: '<?php echo e(url("loginSendOtp")); ?>',
                headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                 },
                data: data,
                cache: false,
                success: function (data)
                {
                    message = data.message;
                    if(data.status == "Success"){
                        console.log(data);
                        $('#otpStatus').addClass("alert-success").text(message);
                        var mobile = $('#otp_mobile').val();
                        $('#otp_mobile').attr('disabled','disabled');

                        $('#sendotptomobile').hide();
                        $('#verifyotp').show();
                        countDown();

                    }else{
                        $('#otpStatus').addClass("alert-danger").text(message);
                        console.log(data);
                    }
                },
                error: function (data){

                    console.log(data);
                    console.log(data.responseJSON.errors);
                    var errors = data.responseJSON.errors;

                    // if(errors.mobile){
                    //     $('#mobile_error').addClass('text-danger');
                    //     $('#mobile_error').html(errors.mobile[0]);
                    // }

                    if(errors.email){
                        $('#login_email_error').addClass('text-danger');
                        $('#login_email_error').html(errors.email[0]);
                    }

                    if(errors.password){
                        $('#login_password_error').addClass('text-danger');
                        $('#login_password_error').html(errors.password[0]);
                    }
                }
            });
            return false;
        }


        function Otpverify()
        {
            // next code goes here...
            var otp  = $("#otp").val();

            var data = {
                code:otp,
            };
            // ajax post
            $.ajax({
                type: "post",
                url: '<?php echo e(url("verifyotp")); ?>',
                headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                 },
                data: data,
                cache: false,
                success: function (data){

                    console.log(data);
                    var message = data.message;
                    if(data.status == "Success"){
                    $('#otpStatus').addClass("alert-success").text(message);
                    location.reload();
                    }else{
                    $('#otpStatus').addClass("alert-danger").text(message);
                    }
                },
                error: function (data){
                    console.log(data);
                    console.log(data.responseJSON.errors);

                    // var errors = data.responseJSON.errors;
                }
            });
            return false;
        }

        function forgotPassword()
        {
            // next code goes here...
            var email  = $("#reset_email").val();

            var data = {
                email:email
            };
            $.ajax({
                type: "post",
                url: '<?php echo e(url("customforgotpassword")); ?>',
                headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                 },
                data: data,
                cache: false,
                success: function (data){
                    console.log(data);
                    var message = data.msg;
                    if(data.status == "Success"){

                    $('#reset_status').addClass("alert-success").text(message);

                    }else{
                        $('#reset_status').addClass("alert-danger").text(message);
                    }
                },
                error: function (data){
                    console.log(data);
                    console.log(data.responseJSON.errors);

                    // var errors = data.responseJSON.errors;
                }
            });
            return false;
        }
</script>

<script type="text/javascript">
    var path = "<?php echo e(route('autocomplete')); ?>";
    $('input.typeahead').typeahead({
        source:  function (query, process) {
        return $.get(path, { query: query }, function (data) {
            console.log(data);
                return process(data);
            });
        }
    });
</script>
<?php /**PATH C:\wamp64\www\offers\resources\views/layouts/appfooter.blade.php ENDPATH**/ ?>