<!DOCTYPE html>
<html lang="en-US">
    <head>
        <meta charset="utf-8">
    </head>
    <body>
        <h2>Request for Business:</h2>

        <div>
            <ul>
                <li>Name: <?php echo e($input['name']); ?></li>
                <li>Email: <?php echo e($input['email']); ?></li>
                <li>Mobile: <?php echo e($input['mobile']); ?></li>
                <li>Business Name: <?php echo e($input['business_name']); ?></li>
                <li>Address: <?php echo e($input['address']); ?></li>
                <li>City: <?php echo e($input['city']); ?></li>
                <li>Category: <?php echo e($input['category']); ?></li>
            </ul>
        </div>

    </body>
</html>
<?php /**PATH /home/offerkrx/public_html/offerswing_test/resources/views/email/addyourbusiness.blade.php ENDPATH**/ ?>