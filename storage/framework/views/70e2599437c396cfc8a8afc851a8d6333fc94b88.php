    <style>
    .showdropLeft{
        min-width:180px;
    }

    @media (max-width: 1199px){
        .showdropLeft{
            transform: translate3d(-90px, 0px, 0px);
        }
    }

    .categoryImg{
        margin-right: 10px;
        width: 20px;
    }
    .categoryTitle{
        position:relative;
        top:5px;
    }
    </style>
    <header class="header header--standard header--autopart shadow" data-sticky="true">

      <div class="header__content">
          <div class="container">
            
            <div class="header__content-left">
              <a class="ps-logo" href="<?php echo e(url('/')); ?>">
                <img src="<?php echo e(asset('main/images/newlogo.png')); ?>" alt="">
              </a>
              <div class="menu--product-categories">
                <div class="menu__toggle"><i class="icon-menu"></i><span>Shop by Category</span></div>
                <div class="menu__content">
                  <ul class="menu--dropdown menuHeight2" style="overflow-y: scroll">
                  <?php if($categories->isNotEmpty()): ?>
                    <?php $__currentLoopData = $categories; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $key=>$item): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                        <li>
                            <a href="<?php echo url('/subcategories',[$item->slug]);; ?>">
                                <img class="categoryImg" src="<?php echo e(asset('storage/'.$item->cat_image)); ?>" alt="">
                                <span class="categoryTitle"><?php echo e(ucwords(strtolower($item->category_name))); ?></span>
                            </a>
                        </li>
                    <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                    <?php else: ?>
                        <li><a href="#">No Categories Found</a></li>
                    <?php endif; ?>  
                  </ul>
                </div>
              </div>
            </div>
            
            <div class="header__content-center">
              <form class="ps-form--quick-search" action="<?php echo e(url('/search')); ?>" method="get">
                <div class="form-group--icon locationInput"><i class="icon-chevron-down"></i>
                    <select class="form-control">
                        <option>Location</option>
                        <option value="Srikakaulam">Srikakaulam</option>
                        <option value="Vishakapatnam">Vishakapatnam</option>
                    </select>
                </div>
                <input class="form-control" name="query" type="text" placeholder="Search for local offers, Deals & Near by Shops.....">
                <button>Search</button>
              </form>
            </div>
            
            <div class="header__content-right">
            
              <div class="header__actions">
    
                <?php if(auth()->guard()->guest()): ?>
                <div class="ps-block--user-header">
                    <div class="ps-block__left"><i class="icon-user"></i></div>
                    <div class="ps-block__right">
                        <a href="#" data-toggle="modal" data-target="#myModal1" id="login_modal">Login</a>
                        <a href="#" data-toggle="modal" data-target="#myModal1" id="register_modal">Register</a>
                    </div>
                </div>
                <?php else: ?>
                <a class="header__extra" href="<?php echo e(url('wingpoints')); ?>" data-toggle="tooltip" data-original-title="Wing Coins">
                    <i class="icon-coin-dollar"></i>
                </a>
                <a class="header__extra" href="<?php echo e(url('orders')); ?>" data-toggle="tooltip"  data-original-title="My Orders">
                    <i class="icon-cart" style="-webkit-text-stroke: .5px #000;"></i>
                </a>
                <ul class="menu">
                    <li class="current-menu-item menu-item-has-children">
                        <a href="#" onClick="return false;"><?php echo e(Auth::user()->first_name." ".Auth::user()->last_name); ?></a>
                        <span class="sub-toggle"></span>
                        <ul class="sub-menu showdropLeft">
                            <li><a href="<?php echo e(url('userprofile')); ?>">Profile</a></li>
                            <li><a href="<?php echo e(url('favouriteshops')); ?>">Favourite Shops</a></li>
                            <li><a href="<?php echo e(url('/referafriend')); ?>">Refer a Friend</a></li>
                            <li><a href="#">Notifications<sup><?php echo e(count($notifications)); ?></sup></a></li>
                            <li>
                                <a href="<?php echo e(route('logout')); ?>" onclick="event.preventDefault(); document.getElementById('logout-form').submit();">Logout</a>
                            </li>
                        </ul>
                    </li>
                </ul>
                <?php endif; ?>
               
              </div>
            </div>
          </div>
        </div>
        
        <nav class="navigation">
          <div class="container">
              <div class="navigation__left">
                  <div class="menu--product-categories">
                      <div class="menu__toggle"><i class="icon-menu"></i><span class="text-white"> Shop by Category</span></div>
                      <div class="menu__content">
                        <ul class="menu--dropdown menuHeight" style="overflow-y:scroll">
                            
                          <?php if($categories->isNotEmpty()): ?>
                            <?php $__currentLoopData = $categories; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $key=>$item): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                <li>
                                    <a href="<?php echo url('/subcategories',[$item->slug]);; ?>">
                                        <img class="categoryImg" src="<?php echo e(asset('storage/'.$item->cat_image)); ?>" alt="">
                                        <span class="categoryTitle"><?php echo e(ucwords(strtolower($item->category_name))); ?></span>
                                    </a>
                                </li>
                            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                          <?php else: ?>
                              <li><a href="#">No Categories Found</a></li>
                          <?php endif; ?>  
                        </ul>
                    </div>
                  </div>
              </div>
              <div class="navigation__right">
                  <span class="text-white">
                        Hi, <strong>Good <strong id="greetings">Morning</strong></strong>
                    </span>
                    <ul class="navigation__extra justify-content-end p-3">
                        <li><a href="<?php echo e(url('/addyourbusiness')); ?>">Add Your Business</a></li>
                        <li><a href="<?php echo e(url('/contactus')); ?>">Contact Us</a></li>
                        <li><a href="<?php echo e(url('/aboutus')); ?>">About Us</a></li>
                        <li><a href="<?php echo e(url('/faq')); ?>">FAQ's</a></li>
                    </ul>
              </div>
          </div>
        </nav>
      </header>
  
      <header class="header header--mobile autopart shadow" data-sticky="true">
          <div class="navigation--mobile">
              <div class="navigation__left">
                  <a class="ps-logo" href="index.html">
                      <img src="<?php echo e(asset('main/images/newlogo.png')); ?>" width="120px" alt="">
                  </a>
              </div>
              <div class="navigation__right">
                  <div class="header__actions pt-3">
                      <?php if(auth()->guard()->guest()): ?>
                          <div class="ps-block--user-header">
                              <div class="ps-block__left">
                                  <a href="#" data-toggle="modal" data-target="#myModal1">
                                      <i class="icon-user"></i>
                                  </a>
                              </div>
                              <div class="ps-block__right">
                                  <a href="#" data-toggle="modal" data-target="#myModal1" id="login_modal">Login</a>
                                  <a href="#" data-toggle="modal" data-target="#myModal1" id="register_modal">Register</a>
                              </div>
                          </div>
                      <?php else: ?>
                          <ul class="menu">
                              <li class="current-menu-item menu-item-has-children">
                                  <i class="icon-user"></i><i class="icon-chevron-down" style="font-size:12px"></i>
                                  <span class="sub-toggle"></span>
                                  <ul class="sub-menu showdropLeft">
                                      <li><a href="<?php echo e(url('userprofile')); ?>">Profile</a></li>
                                      <li><a href="<?php echo e(url('favouriteshops')); ?>">Favourite Shops</a></li>
                                      <li><a href="<?php echo e(url('/referafriend')); ?>">Refer a Friend</a></li>
                                      <li><a href="<?php echo e(url('notifications')); ?>">Notifications<sup><?php echo e(count($notifications)); ?></sup></a></li>
                                      <li>
                                          <a href="<?php echo e(route('logout')); ?>" onclick="event.preventDefault(); document.getElementById('logout-form').submit();">Logout</a>
                                      </li>
                                  </ul>
                              </li>
                          </ul>
                      <?php endif; ?>
                  </div>
              </div>
          </div>
      </header>
  
      <div class="ps-panel--sidebar" id="navigation-mobile">
          <div class="ps-panel__header">
              <h3>Categories</h3>
          </div>
          <div class="ps-panel__content">
              <ul class="menu--mobile">
              <?php if($categories->isNotEmpty()): ?>
                  <?php $__currentLoopData = $categories; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $item): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                  <li><a href="<?php echo url('/subcategories',[$item->id]);; ?>"><?php echo e(ucwords(strtolower($item->category_name))); ?></a></li>
                  <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
              <?php else: ?>
                  <li><a href="#">No Categories Found</a></li>
              <?php endif; ?>   
              </ul>
          </div>
      </div>
      <div class="navigation--list">
          <div class="navigation__content">
              <a class="navigation__item ps-toggle--sidebar" href="#menu-mobile">
                  <i class="icon-menu"></i><span> Menu</span>
              </a>
              <a class="navigation__item ps-toggle--sidebar" href="#navigation-mobile">
                  <i class="icon-list4"></i><span> Categories</span>
              </a>
              <a class="navigation__item ps-toggle--sidebar" href="#search-sidebar">
                  <i class="icon-magnifier"></i><span> Search</span>
              </a>
              <?php if(auth()->guard()->check()): ?>
              <a class="navigation__item ps-toggle--sidebar" href="<?php echo e(url('wingpoints')); ?>">
                  <i class="icon-coin-dollar"></i><span> Coins</span>
              </a>
              <a class="navigation__item ps-toggle--sidebar" href="<?php echo e(url('orders')); ?>">
                  <i class="icon-cart" style="-webkit-text-stroke: .5px #000;"></i><span> Orders</span>
              </a>
              <?php endif; ?>
          </div>
      </div>
      <div class="ps-panel--sidebar" id="search-sidebar">
          <div class="ps-panel__header">
              <form class="ps-form--quick-search" action="<?php echo e(url('/search')); ?>" method="get">
                <div class="form-group--icon locationInput"><i class="icon-chevron-down"></i>
                    <select class="form-control">
                        <option>Location</option>
                        <option value="Srikakaulam">Srikakaulam</option>
                        <option value="Vishakapatnam">Vishakapatnam</option>
                    </select>
                </div>
                <input class="form-control" name="query" type="text" placeholder="Search for local offers, Deals & Near by Shops.....">
                <button><i class="icon-magnifier"></i></button>
              </form>
          </div>
          <div class="navigation__content"></div>
      </div>
      <div class="ps-panel--sidebar" id="menu-mobile">
          <div class="ps-panel__header">
              <h3>Menu</h3>
          </div>
          <div class="ps-panel__content">
              <ul class="menu--mobile">
                  <li class="current-menu-item"><a href="index.html">Add Your Business</a></li>
                  <li class="current-menu-item"><a href="index.html">Contact Us</a></li>
                  <li class="current-menu-item"><a href="index.html">About Us</a></li>
                  <li class="current-menu-item"><a href="index.html">FAQ's</a></li>
              </ul>
          </div>
      </div>

    <!-- log out -->
    <form id="logout-form" action="<?php echo e(route('logout')); ?>" method="POST" style="display: none;">
         <?php echo csrf_field(); ?>
    </form><?php /**PATH D:\wamp64\www\offerswing_test\resources\views/layouts/mainheader.blade.php ENDPATH**/ ?>