<?php

namespace App\Http\Controllers\Backend;

use App\Http\Controllers\Controller;
use App\Models\Claimed;
use App\Models\Order;
use Symfony\Component\HttpFoundation\Request;

class ClaimedController extends Controller
{
    public function claimStatus(Request $request)
    {
        $id = $request['order_id'];
        // return $id;

        $order = Order::where(['order_id'=> $id])->first();
        $order_details = $order->orderDetails;

        if (0 == $order_details->claim_status) {
            $claimed = new Claimed();

            $claimed->user_id = $order->user_id;
            $claimed->order_id = $order->order_id;
            $claimed->claimed_type = $order_details->order_type;
            $claimed->claimed_type_id = $order_details->order_type_id;

            if ($claimed->save()) {
                $order_details->claim_status = 1;
            }
        } else {
            $claimed = Claimed::where(['order_id'=>$order->order_id,'user_id'=>$order->user_id])->first();
            
            if ($claimed->delete()) {
                $order_details->claim_status = 0;
            }
        }

        if ($order_details->save()) {
            return 'Success';
        }

        return 'Failed';
    }
}
