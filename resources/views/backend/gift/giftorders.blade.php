@extends('layouts.app')

@section('content')

@php
    $gift = $order->gift;
@endphp
<section >
    <div class="ps-page--product">
        <div class="container">

            <div class="row">
                <div class="col-12 col-md-4">
                    <div class="ps-post text-center">
                        <img src="<?php echo asset("$gift->image"); ?>">
                        <h3 class="pt-4 m-0">{{ $gift->gift_name }}</h3>
                    </div>
                </div>
                <div class="col-12 col-md-8 text-center pb-5">
                    {!! QrCode::size(250)->generate($claimed->unique_code); !!}
                    <div class="table-responsive">
                        <table class="table table-bordered ps-table ps-table--specification">
                            <tbody>
                                <tr>
                                    <td>Booking id</td>
                                    <td>{{ $claimed->unique_code }}</td>
                                </tr>
                                <tr>
                                    <td>Claimed item</td>
                                    <td>{{ $gift->gift_name }}</td>
                                </tr>
                                <tr>
                                    <td>Claimed on</td>
                                    <td>{{ date('d-m-Y h:i A',strtotime($claimed->created_at)) }}</td>
                                </tr>
                            </tbody>
                        
                        </table>
                    </div>
                    <strong>Our team will contact you soon</strong>
                </div>
            </div>
            
        </div>
    </div>
</section>
@endsection