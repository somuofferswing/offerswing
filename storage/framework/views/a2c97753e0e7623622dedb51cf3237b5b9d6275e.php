

<?php $__env->startSection('content'); ?>
    <div class="ps-breadcrumb">
        <div class="container">
            <ul class="breadcrumb">
                <li><a href="<?php echo e(url('/')); ?>">Home</a></li>
                <li>Add your Business</li>
            </ul>
        </div>
    </div>
    <section class="ps-contact-info">
        <div class="container">
            <form class="ps-form--contact-us" id="businessForm" action="<?php echo e(url('addyourbusiness_post')); ?>" method="post">
                <h3>Self Register Your Business in Offerswing and get 10% off</h3>
                <?php echo csrf_field(); ?>
                <div class="row">
                    <input type="hidden" name="w_email" value="srikanthpachipulusu@gmail.com">
                    <div class="col-xl-6 col-lg-6 col-md-6 col-sm-12 col-12 ">
                        <div class="form-group">
                            <input class="form-control" type="text" id="name" name="name" placeholder="Name *" required>
                        </div>
                    </div>
                    <div class="col-xl-6 col-lg-6 col-md-6 col-sm-12 col-12 ">
                        <div class="form-group">
                            <input class="form-control" type="text" id="business_name" name="business_name" placeholder="Business Name *" required>
                        </div>
                    </div>
                    <div class="col-xl-6 col-lg-6 col-md-6 col-sm-12 col-12 ">
                        <div class="form-group">
                            <input class="form-control" type="text" placeholder="Phone Number *" id="phonenumber" name="mobile" required>
                        </div>
                    </div>
                    <div class="col-xl-6 col-lg-6 col-md-6 col-sm-12 col-12 ">
                        <div class="form-group">
                            <input class="form-control" type="email" id="email" name="email" placeholder="Email" >
                        </div>
                    </div>
                    <div class="col-xl-6 col-lg-6 col-md-6 col-sm-12 col-12 ">
                        <div class="form-group">
                            <input class="form-control" type="text" id="City" name="city" placeholder="City *" required>
                        </div>
                    </div>
                    <div class="col-xl-6 col-lg-6 col-md-6 col-sm-12 col-12 ">
                        <div class="form-group">
                            <select id="sel1" name="category" class="form-control" required>
                                <?php $__currentLoopData = $categories; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $category): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                    <option value="<?php echo e($category->category_name); ?>"><?php echo e($category->category_name); ?></option>       
                                <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                <option value="other">other</option>
                            </select>
                        </div>
                    </div>
                
                    <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12 ">
                        <div class="form-group">
                            <textarea class="form-control" rows="5" placeholder="Complete Adress *" id="comment" name="address" required></textarea>
                        </div>
                    </div>
                </div>
                <div class="form-group submit">
                    <button class="ps-btn" id="form-submit">Submit</button>
                </div>
                <div class="d-flex justify-content-center">
                    <div class="sk-flow d-none" id="businessSpinner">
                        <div class="sk-flow-dot"></div>
                        <div class="sk-flow-dot"></div>
                        <div class="sk-flow-dot"></div>
                    </div>
                </div>
            </form>
        </div>
    </section>

    <script>
        $('#businessForm').submit(function(){
            $('#form-submit').hide();
            $('#businessSpinner').removeClass('d-none');
        });
    </script>
<?php $__env->stopSection(); ?>

<?php echo $__env->make('layouts.app', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH /home/offerkrx/public_html/offerswing_test/resources/views/frontend/addyourbusiness.blade.php ENDPATH**/ ?>