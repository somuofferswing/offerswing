<?php $__env->startSection('content'); ?>
<style>
    .ps-page--blog .ps-page__header{
		padding:30px 0;
	}
    .highlighter{
        border-bottom:2px solid #df685d;
        width:20%;
        margin:0px auto;
    }
    .table-bordered td{
        padding:20px;
        font-weight:bold;
    }
</style>
<div class="ps-breadcrumb">
    <div class="ps-container">
        <ul class="breadcrumb text-capitalize" id="breadcrumb">
            <li><a href="<?php echo e(url('/')); ?>">Home</a></li>
            <li>Wing Coins</li>
        </ul>
    </div>
</div>
<section>
    <div class="ps-page--blog">
        <div class="container">
            <div class="ps-page__header p-5">
                <h1 class="text-center">Wing Coins : <?php echo e($total_wing_points->points); ?></h1>
                <hr class="highlighter">
            </div>
            
            <div class="row m-5">
                <div class="col-12 col-md-6 text-center mb-3">
                    <a href="<?php echo e(url('/referafriend')); ?>" class="ps-btn bg-primary text-white">Refer a Friend</a>
                    <a href="<?php echo e(url('/redeem')); ?>" class="ps-btn bg-success text-white">Redeem</a>
                </div>
                <div class="col-12 col-md-6 text-center mb-3">
                    <form action="<?php echo e(url('/token/redeem')); ?>" method="POST">
                        <?php echo csrf_field(); ?>
                        <div class="row">
                            <input type="text" class="form-control col-8" placeholder="Enter your token code"
                                name="token_id">
                            <button class="ps-btn col-4" type="submit"> Submit</button>
                        </div>
                    </form>
                </div>
            </div>

            <div class="pt-5">
                <h2 class="text-center">History</h2>
            </div>
            <table class="table table-bordered">
                <tbody>
                    <?php $__currentLoopData = $wing_points; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $item): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                    <tr>
                        <td><?php echo e($item->type); ?></td>
                        <td><?php echo e($item->points); ?></td>
                    </tr>
                    <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                </tbody>
            </table>
        </div>
    </div>
</section>
<?php $__env->stopSection(); ?>
<?php echo $__env->make('layouts.app', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH C:\xampp\htdocs\offerswing\resources\views/wingcoins.blade.php ENDPATH**/ ?>