<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;

/**
 * Post.
 *
 * @mixin Builder
 */
class ImageUpload extends Model
{
    protected $fillable = [
        'image_name', 'image_path', 'shop_id',
    ];

    public function shop()
    {
        return $this->belongsTo(Shop::class, 'shop_id', 'id');
    }
}
