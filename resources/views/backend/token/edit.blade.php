@extends('layouts.admin')

@section('content')
<div class="content-wrapper">
    <section class="content-header">
        <h1>
            Token
            <small>Edit Token</small>
        </h1>
        <!-- <ol class="breadcrumb">
              <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
              <li class="active">Dashboard</li>
            </ol> -->
    </section>

    <section class="content">
        {{-- <div class="col-md-12"> --}}
        <div class="box box-info">
            <div class="box-header with-border">
                <h3 class="box-title">Edit Token</h3>
            </div>
            <form class="form-horizontal" method="POST" action="{{ url('/token/update',[$token->id]) }}"
                enctype="multipart/form-data">
                @csrf

                <div class="form-group">
                    <label for="token_id" class="col-sm-1 control-label">Token Id</label>
                    <div class="col-sm-11">
                        <input id="token_id" type="text" class="form-control" name="token_id"
                            value="{{ $token->token_id }}" required >
                    </div>
                </div>

                <div class="form-group">
                    <label for="points_rewarded" class="col-sm-1 control-label">Points Awarded</label>
                    <div class="col-sm-11">
                        <input id="points_rewarded" type="text" class="form-control" name="points_rewarded"
                            value="{{ $token->points_rewarded }}" required>
                    </div>
                </div>

                <div class=" pull-right">
                    <button type="submit" class="btn btn-info ">{{ __('Add Token') }}</button>
                    <button type="button" class="btn btn-danger ">{{ __('Cancel') }}</button>

                </div>

            </form>
        </div>
</div>
{{-- </div> --}}
</section>
</div>
@endsection
