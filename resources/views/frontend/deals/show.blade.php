@extends('layouts.admin')

@section('content')
<style>
    td {
        /* overflow: hidden; */
        max-width: 400px;
        word-wrap: break-word;
    }
</style>
<div class="content-wrapper">
    <section class="content-header">
        <h1>
            Wing Deals
            <small>View Wing Deal({{$deal->name }})</small>
        </h1>
    </section>

    <section class="content">
        <img src="<?php echo asset("$deal->pic"); ?>" alt="{{$deal->name }}" class="img-responsive" width="100%">
        <hr>

        <div class="table-responsive">
            <table class="table table-striped">
                <tr>
                    <td>Id</td>
                    <td>{{ $deal->id }}</td>
                </tr>

                <tr>
                    <td>Name</td>
                    <td>{{ $deal->name }}</td>
                </tr>

                <tr>
                    <td>Description</td>
                    <td>{!! $deal->description !!}</td>
                </tr>

                <tr>
                    <td>Payment Type</td>
                    <td>{{ $deal->payment_type }}</td>
                </tr>
                <tr>
                    <td>Price/Wingpoints</td>
                    <td>{{ $deal->price }}</td>
                </tr>
                <tr>
                    <td>Deals Quantity</td>
                    <td>{{ $deal->quantity }}</td>
                </tr>
                <tr>
                    <td>Claims Per user</td>
                    <td>{{ $deal->max_count_for_user }}</td>
                </tr>
                <tr>
                    <td>Valid From</td>
                    <td>{{ $deal->valid_from }}</td>
                </tr>
                <tr>
                    <td>valid To</td>
                    <td>{{ $deal->valid_upto }}</td>
                </tr>
                <tr>
                    <td>Discount Percentage</td>
                    <td>{{ $deal->discount_percentage }}</td>
                </tr>
                <tr>
                    <td>Max Discount</td>
                    <td>{{ $deal->max_discount }}</td>
                </tr>
                <tr>
                    <td>Wing Points Rewarded</td>
                    <td>{{ $deal->points_rewarded }}</td>
                </tr>

                <tr>
                    <td>Terms & Conditions</td>
                    <td>{!! $deal->terms_conditions !!}</td>
                </tr>

            </table>
        </div>


        <hr>

        Deal Orders

        <table class="table">
            <tr>
                <th>Unique Claim Code</th>
                <th>User Name</th>
                <th>User Mobile</th>
                <th>User Email</th>
                <th>Bought On</th>
                <th>Claim Status</th>
                <th>Actions</th>
            </tr>

            @foreach ($ $claimed_deals as $item)
            <tr>
                <td>{{ $item->unique_code }}</td>
                <td>{{ $item->first_name. .$item->last_name }}</td>
                <td>{{ $item->mobile }}</td>
                <td>{{ $item->email }}</td>
                <td>{{ $item->bought_on }}</td>
                <td>@if ($item->used ==0)
                    Not Used
                    @else
                    Used
                    @endif</td>
                <td>
                    @if ($item->used ==0)
                <div class="btn btn-info changestatus" data-id="{{$item->unique_code }}">
                        Change to used
                    </div>
                    @else
                    <div class="btn btn-danger changestatus" data-id="{{$item->unique_code }}">
                            Change to not used
                        </div>
                    @endif
                </td>


            </tr>


            @endforeach

        </table>
    </section>
</div>
@endsection
@section('scripts')
<script>
    $('.changestatus').on('click',function(){
        var id = $(this).data('id');

        data = {
            'id':id
        }

        $.ajax({
        type: "POST",
        url: "{{ url('/claimStatus') }}",
        data: "data",
        // dataType: "dataType",
        success: function (response) {
            if(response == 'Success'){
                window.location.reload();
            }else{
                alert(response);
            }
        }
    });

    });
    
</script>

@endsection