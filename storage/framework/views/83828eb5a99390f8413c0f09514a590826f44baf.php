<?php $__env->startSection('content'); ?>
<style>
/* card */
.ps-post .ps-post__title{
        margin:10px 0 15px 0;
    }

    .ps-post .ps-post__content p{
        color: inherit;
    } 

    @media (max-width: 767px){
        .ps-post--small-thumbnail .ps-post__thumbnail {
            margin:0px;
        }
        .ps-post--small-thumbnail .ps-post__content {
            padding-left: 1.5rem;
        }
    }

    .ps-post .ps-post__thumbnail .ps-post__badge {
        top:inherit;
        bottom: 20px;
        border-radius:50%;
    }
	
	.ps-post--small-thumbnail .ps-post__content{
		padding: 0px 20px;
	}

    .zoom:hover {
        transform: scale(1.5);
        -webkit-transition: transform .2s ease-in-out;
    }
</style>
<div class="ps-breadcrumb">
    <div class="ps-container">
        <ul class="breadcrumb text-capitalize" id="breadcrumb">
            <li><a href="<?php echo e(url('/')); ?>">Home</a></li>
            <li>Search</li>
        </ul>
    </div>
</div>

<section>
    <div class="ps-page--product">
        <div class="ps-container">

            <div class="ps-product--detail">
                <ul class="ps-tab-list">
                    <li class="active"><a href="#tab-shops">Shops</a></li>
                    <li><a href="#tab-deals">Deals</a></li>
                </ul>
                <div class="ps-tabs">

                <!-- shops tab -->
                <div class="ps-tab active" id="tab-shops">
                <?php if($shops->isEmpty()): ?>
                <h3 class="text-danger text-uppercase">No Shops Found</h3>
                <?php else: ?>
                    <?php $__currentLoopData = $shops; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $item): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                        <?php
                        $image = $item->shop_images;
                        $firstimage = $image[0];
                        ?>

                        <div class="ps-post ps-post--small-thumbnail ps-highlight bg-dark text-white">
                            <div class="ps-post__thumbnail">
                                <a class="ps-post__overlay" href="<?php echo url('/viewshop',[$item->shopid]);; ?>"></a>
        
                                <?php if(!empty($firstimage->image_path)): ?>
                                    <img src="<?php echo asset("$firstimage->image_path"); ?>" alt="">
                                 <?php endif; ?> 
                                
                                <?php if(in_array(date('N',time())+1 , explode(",",$item->active_days))): ?>
                                    <div class="ps-post__badge bg-warning text-dark">Open</div>
                                <?php else: ?>
                                    <div class="ps-post__badge ps-close">Close</div>
                                <?php endif; ?>
                            </div>
                            <div class="ps-post__content">
                                <div class="ps-post__top">
                                    <a class="ps-post__title" href="<?php echo url('/viewshop',[$item->shopid]);; ?>">
                                        <?php echo e(ucfirst($item->shop_name)); ?>

                                    </a>
                
                                    <div class="ps-post__desc">
                                        <p>
                                            <i class="fa fa-map-marker fa-lg text-warning pr-2"></i>
                                            <?php echo e($item->address); ?>

                                        </p>
                                        <p><i class="fa fa-phone text-success pr-2"></i> 
                                            <?php echo e($item->phone_number); ?>

                                        </p>
                                        <p class="mb-3">
                                            <?php for($i = 0; $i < 5; $i++): ?> 
                                                <?php if($i< (int)$item->shop_rating): ?>
                                                <span class="fa fa-star text-danger"></span>
                                                <?php else: ?>
                                                    <?php if(($item->shop_rating-(int)$item->shop_rating) == 0.5 &&
                                                    ((int)$item->shop_rating) ==
                                                    $i ): ?>
                                                        <span class="fa fa-star-half-o text-danger"></span>
                                                    <?php else: ?>
                                                        <span class="fa fa-star text-danger"></span>
                                                    <?php endif; ?>
                                                <?php endif; ?>
                                            <?php endfor; ?>
                                        </p>
                                    </div>
                                </div>
                            </div>
                        </div>
                    <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                    <?php endif; ?>
                </div>
                <!-- end of shops tab -->

                <!-- deals tab -->
                <div class="ps-tab" id="tab-deals">
                    <?php if($deals->isNotEmpty()): ?>
                        <?php $__currentLoopData = $deals; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $key=>$item): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                            <h4><?php echo e(strtoupper($item->name)); ?></h4>
                            <div class="m-3">
                                <?php if(!empty($item->pic)): ?>
                                    <img class="mb-5" src="<?php echo asset("{$item->pic}"); ?>" alt="" height="200px" width="auto">
                                <?php endif; ?>
                                <p><strong>Valid Upto:</strong> <?php echo e($item->valid_upto); ?></p>
                                <p><button class="btn btn-warning dealdetails" data-id="<?php echo e($item->id); ?>">
                                    <strong>Details</strong></button>
                                </p>
                                <p>Flat <?php echo e($item->discount_percentage); ?> % off*</p>
                                
                                <?php if(Auth::guest()): ?>
                                    <button class="btn btn-primary" data-toggle="modal" data-target="#myModal1">Claim Now</button>
                                <?php else: ?>
                                    <?php if($item->payment_type != 'wingpoints'): ?>
                                    <a href="<?php echo e(url('/getdeal',[ $item->id])); ?>" class="btn btn-danger"
                                        data-id="<?php echo e($item->id); ?>">Claim Now</a>

                                    <?php else: ?>
                                        <?php if(Session::get('total_wingpoints')->points >= $item->price): ?>
                                            <a href="<?php echo e(url('/getdeal',[ $item->id])); ?>" class="btn btn-danger" data-id="<?php echo e($item->id); ?>">Claim Now</a>
                                        <?php else: ?>
                                            <button class="btn btn-default" disabled> Not Enough Points </button>
                                        <?php endif; ?>
                                    <?php endif; ?>
                                <?php endif; ?>
                            </div>
                            <hr class="mb-5"></hr>
                        <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                    <?php else: ?>
                        <p>Sorry no deals available </p>
                    <?php endif; ?>
                </div>
                <!-- end of deals tab -->

            </div></div>
        </div>
    </div>
</section>
    
<script>
    $(document).on('click', ".unlike", function() {
        var id = $(this).data('id');
        var element = this;

        var data = {
            shop_id: id
        };
        $.ajax({
            type: "post",
            url: '<?php echo e(url("removefavouriteshop")); ?>',
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            },
            data: data,
            cache: false,
            success: function(data) {
                if (data.status == "success") {
                    $(element).removeClass('unlike').addClass('like').html('<i class="far fa-heart" style="color:#e31b23"></i>');
                }
            },
            error: function(data) {
                alert('Fail to run unlike..');
            }
        });


    });
    $(document).on('click', ".like", function() {
        var id = $(this).data('id');
        var element = this;

        var data = {
            shop_id: id
        };
        $.ajax({
            type: "post",
            url: '<?php echo e(url("addfavouriteshop")); ?>',
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            },
            data: data,
            cache: false,
            success: function(data) {
                console.log(data);
                if (data.status == "success") {
                    $(element).removeClass('like').addClass('unlike').html('<i class="fas fa-heart" style="color:#e31b23"></i>');

                }
            },
            error: function(data) {
                alert('Fail to run like..');
            }
        });


    });
</script>

<?php $__env->stopSection(); ?>
<?php echo $__env->make('layouts.app', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH C:\xampp\htdocs\offerswing\resources\views/search.blade.php ENDPATH**/ ?>