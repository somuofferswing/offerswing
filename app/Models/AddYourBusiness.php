<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class AddYourBusiness extends Model
{
    protected $table = 'add_your_business';
}
