<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class OrderDetail extends Model
{
    public function order()
    {
        return $this->belongsTo(Order::class, 'order_id', 'id');
    }

    public function user()
    {
        return $this->belongsTo(User::class, 'user_id', 'id');
    }

    public function deal()
    {
        return $this->belongsTo(Deal::class, 'order_type_id', 'id');
    }

    public function event()
    {
        return $this->belongsTo(Event::class, 'order_type_id', 'id');
    }

    public function gift()
    {
        return $this->belongsTo(Gift::class, 'order_type_id', 'id');
    }
}
