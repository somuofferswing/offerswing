<?php $__env->startSection('content'); ?>
    <div class="content-wrapper">
        <section class="content-header">
            <h1>
                Payment Types
                <small>Add Payment Types</small>
            </h1>
            <!-- <ol class="breadcrumb">
                  <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
                  <li class="active">Dashboard</li>
                </ol> -->
        </section>

        <section class="content">

            <div class="col-md-8 col-md-offset-2">
                <div class="box box-info">
                    <div class="box-header with-border">
                        <h3 class="box-title">Add Category</h3>
                    </div>
                    <form class="form-horizontal" method="POST" action="<?php echo e(route('store-payment')); ?>"
                          enctype="multipart/form-data">
                        <?php echo csrf_field(); ?>
                        <div class="box-body">

                            <div class="form-group">
                                <label for="name" class="col-sm-2 control-label">PaymentType Name</label>
                                <div class="col-sm-10">
                                    <input id="name" type="text" class="form-control" name="name"
                                           value="<?php echo e(old('name')); ?>" required autocomplete="name" autofocus>
                                </div>
                            </div>
                            <div class="form-group ">
                                <label for="description" class="col-sm-2 control-label">Description</label>
                                <div class="col-sm-10">
                            <textarea id="description" type="text" class="form-control" name="description" required
                                      autocomplete="description" autofocus><?php echo e(old('description')); ?></textarea>
                                    <?php if($errors->has('description')): ?>
                                        <div class="danger"><?php echo e($errors->first('description')); ?></div>
                                    <?php endif; ?>
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="category_image" class="col-sm-2 control-label">Image</label>
                                <div class="col-sm-10">
                                    <input type="file" class="form-control-file" id="category_image" name="image"
                                           onchange="loadFile(event)">
                                    <img id="output" height="100px" width="100px"/>
                                </div>
                            </div>
                            <button type="submit" class="btn btn-info pull-right"><?php echo e(__('Create')); ?></button>
                        </div>
                    </form>
                </div>
            </div>
        </section>
    </div>
<?php $__env->stopSection(); ?>
<?php $__env->startSection('scripts'); ?>
    <script src="<?php echo e(asset('adminlte/bower_components/ckeditor/ckeditor.js')); ?>"></script>
    <script>
        var loadFile = function (event) {
            var output = document.getElementById('output');
            output.src = URL.createObjectURL(event.target.files[0]);
        };

        $(function () {
            CKEDITOR.replace('description')
        })
    </script>

<?php $__env->stopSection(); ?>

<?php echo $__env->make('layouts.admin', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH C:\wamp64\www\offerswing_test\resources\views/backend/payments/create.blade.php ENDPATH**/ ?>