@extends('layouts.admin')

@section('content')
    <div class="content-wrapper">
        <section class="content-header">
            <h1>
                Amenity
                <small>Add Amenity</small>
            </h1>
            <!-- <ol class="breadcrumb">
              <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
              <li class="active">Dashboard</li>
            </ol> -->
        </section>

        <section class="content">

            <div class="col-md-8 col-md-offset-2">
                <div class="box box-info">
                    <div class="box-header with-border">
                        <h3 class="box-title">Add Amenity</h3>
                    </div>
                    <form class="form-horizontal" method="POST" action="{{ route('update-amenity',[$amenity->id]) }}"  enctype="multipart/form-data">
                        @csrf
                        <div class="box-body">

                            <div class="form-group">
                                <label for="name" class="col-sm-2 control-label">AmenityName</label>
                                <div class="col-sm-10">
                                    <input id="category_name" type="text" class="form-control" name="name"
                                           value="{{ $amenity->name }}" required autocomplete="name"
                                           autofocus >
                                </div>
                            </div>
                            <div class="form-group ">
                                <label for="description" class="col-sm-2 control-label">Description</label>
                                <div class="col-sm-10">
                                        <textarea id="description" type="text" class="form-control" name="description"
                                                  required autocomplete="description"
                                                  autofocus>{{ $amenity->description }}</textarea>
                                    @if ($errors->has('description'))
                                        <div class="danger">{{ $errors->first('description') }}</div>
                                    @endif
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="category_image" class="col-sm-2 control-label">Image</label>
                                <div class="col-sm-10">
                                    <input type="file" class="form-control-file" id="category_image" name="image" onchange="loadFile(event)">
                                    {{-- <img  height="100px" width="100px"/> --}}
                                    <img class="img-thumbnail" src='{{ asset('storage/'.$amenity->image) }}' height="100px" width="100px" id="output">
                                </div>
                            </div>
                            <button type="submit" class="btn btn-info pull-right">{{ __('Update') }}</button>
                        </div>
                    </form>
                </div>
            </div>
        </section>
    </div>
@endsection

@section('scripts')
    <script src="{{ asset('adminlte/bower_components/ckeditor/ckeditor.js') }}"></script>

    <script>
        var loadFile = function(event) {
            var output = document.getElementById('output');
            output.src = URL.createObjectURL(event.target.files[0]);
        };

        $(function () {
            CKEDITOR.replace('description')
        })
    </script>

@endsection
