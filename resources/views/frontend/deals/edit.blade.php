@extends('layouts.admin')

@section('content')
<link rel="stylesheet"
    href="{{ asset('adminlte/bower_components/bootstrap-datepicker/dist/css/bootstrap-datepicker.min.css') }}">
<div class="content-wrapper">
    <section class="content-header">
        <h1>
            Deals
            <small>Add Deal</small>
        </h1>
        <!-- <ol class="breadcrumb">
              <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
              <li class="active">Dashboard</li>
            </ol> -->
    </section>

    <section class="content">

        <div class="box box-info">
            <div class="box-header with-border">
                <h3 class="box-title">Add Deal</h3>
            </div>
            <form class="form-horizontal" method="POST" action="{{ route('update-deal',[$deals->id]) }}"
                enctype="multipart/form-data">
                @csrf
                <input type="hidden" value="{{$user_id}}" name="created_by">
                <div class="box-body">
                    <div class="form-group ">
                        <label for="name" class="col-sm-2 control-label">Name</label>
                        <div class="col-sm-10">
                            <input id="name" type="text" class="form-control" name="name" value="{{ $deals->name }}"
                                required autocomplete="name" autofocus>
                            @if ($errors->has('name'))
                            <div class="danger">{{ $errors->first('name') }}</div>
                            @endif
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="pic" class="col-sm-2 control-label">Image</label>
                        <div class="col-sm-10">
                            <input type="file" class="form-control-file" id="pic" name="pic" onchange="loadFile(event)">
                            {{-- <img  height="100px" width="100px"/> --}}
                            <img class="img-thumbnail" src='<?php echo asset("$deals->pic"); ?>' height="100px"
                                width="100px" id="output">
                        </div>
                    </div>
                    <div class="form-group ">
                        <label for="description" class="col-sm-2 control-label">Description</label>
                        <div class="col-sm-10">
                            <textarea id="description" type="text" class="form-control" name="description" required
                                autocomplete="description" autofocus>{{ $deals->description }}</textarea>
                            @if ($errors->has('description'))
                            <div class="danger">{{ $errors->first('description') }}</div>
                            @endif
                        </div>
                    </div>

                    <div class="form-group ">
                        <label for="original_price" class="col-sm-2 control-label">Item Original Price</label>
                        <div class="col-sm-10">
                            <input id="original_price" type="text" class="form-control" name="original_price"
                                value="{{ $deals->original_price }}" required autocomplete="name" autofocus>

                            @if ($errors->has('description'))
                            <div class="danger">{{ $errors->first('description') }}</div>
                            @endif
                        </div>
                    </div>
                    <div class="form-group ">
                        <label for="payment_type" class="col-sm-2 control-label">Payment Type</label>
                        <div class="col-sm-10">
                            <select class="form-control" id="payment_type" name="payment_type" required autofocus>
                                {{-- <option>--Select--</option> --}}
                                <option value="cash" @if ($deals->payment_type =="cash" )
                                    selected
                                    @endif>Cash</option>
                                <option value="wingpoints" @if ($deals->payment_type =="wingpoints" )
                                    selected
                                    @endif>Wing Points</option>
                                <option value="both" @if ($deals->payment_type =="both" )
                                    selected
                                    @endif>Both</option>
                            </select>
                            @if ($errors->has('payment_type'))
                            <div class="danger">{{ $errors->first('payment_type') }}</div>
                            @endif
                        </div>
                    </div>
                    <div class="form-group ">
                        <label for="price" class="col-sm-2 control-label">Deal Price</label>
                        <div class="col-sm-10">
                            <input id="price" type="text" class="form-control" name="price" value="{{ $deals->price }}"
                                required autofocus>
                            @if ($errors->has('price'))
                            <div class="danger">{{ $errors->first('price') }}</div>
                            @endif
                        </div>
                    </div>
                    <div class="form-group ">
                        <label for="shop_id" class="col-sm-2 control-label">Shop</label>
                        <div class="col-sm-10">

                            <select class="form-control" id="shop_id" name="shop_id" value="{{ old('shop_id') }}"
                                required autocomplete="shop_id" autofocus>
                                <option>--Select--</option>
                                @foreach($shops as $shop)
                                <option value="{{ $shop->id }}" @if ($deals->shop_id == $shop->id )
                                    selected
                                    @endif>{{ $shop->shop_name }}</option>
                                @endforeach
                            </select>
                            @if ($errors->has('shop_id'))
                            <div class="danger">{{ $errors->first('shop_id') }}</div>
                            @endif
                        </div>
                    </div>
                    {{-- <div class="form-group ">
                                <label for="category_id" class="col-sm-2 control-label">Category</label>
                                <div class="col-sm-10">

                                    <select class="form-control" id="category_id" name="category_id" value="{{ old('category_id') }}"
                    required autocomplete="category_id"
                    autofocus>
                    <option>--Select--</option>
                    @foreach($category as $key =>$cat)
                    <option value="{{ $key }}" @if ($deals->category_id == $key)
                        selected
                        @endif>{{ $cat }}</option>
                    @endforeach
                    </select>
                    @if ($errors->has('category_id'))
                    <div class="danger">{{ $errors->first('category_id') }}</div>
                    @endif
                </div>
        </div> --}}
        {{-- <div class="form-group ">
                                <label for="times_applicable" class="col-sm-2 control-label">Times Applicable</label>
                                <div class="col-sm-10">
                                    <input id="times_applicable" type="number" class="form-control" name="times_applicable"
                                           value="{{ old('times_applicable') }}" required
        autocomplete="times_applicable"
        autofocus min="1">
        @if ($errors->has('times_applicable'))
        <div class="danger">{{ $errors->first('times_applicable') }}</div>
        @endif
</div>
</div> --}}
<div class="form-group ">
    <label for="times_applicable" class="col-sm-2 control-label">Days Applicable</label>
    <div class="col-sm-10">
        <select name="days_applicable[]" id="days_applicable" class="form-control select2"
            data-placeholder="Select days applicable" multiple="multiple">
            @foreach ($days as $item)
            <option value="{{$item->id}}" @if (in_array($item->id, explode(',',$deals->days_applicable)))
                selected
                @endif>{{$item->name}}</option>
            @endforeach
        </select>
        @if ($errors->has('days_applicable'))
        <div class="danger">{{ $errors->first('days_applicable') }}</div>
        @endif
    </div>
</div>
<div class="form-group ">
    <label for="valid_from" class="col-sm-2 control-label">From Date</label>
    <div class="col-sm-10">
        <input type="text" class="form-control pull-right" id="valid_from" name="valid_from"
            value="{{ $deals->valid_from }}" required autocomplete="valid_from" autofocus>
        {{--                                    <input id="valid_from" type="text" class="form-control" name="valid_from"--}}
        {{--                                           value="{{ old('valid_from') }}" required
        autocomplete="valid_from"--}}
        {{--                                           autofocus>--}}
        @if ($errors->has('valid_from'))
        <div class="danger">{{ $errors->first('valid_from') }}</div>
        @endif
    </div>
</div>
<div class="form-group ">
    <label for="valid_upto" class="col-sm-2 control-label">To Date</label>
    <div class="col-sm-10">
        <input type="text" class="form-control pull-right" id="valid_upto" name="valid_upto"
            value="{{ $deals->valid_upto }}" required autocomplete="valid_upto" autofocus>
        {{--                                    <input id="valid_upto" type="text" class="form-control" name="valid_upto"--}}
        {{--                                           value="{{ old('valid_upto') }}" required
        autocomplete="valid_upto"--}}
        {{--                                           autofocus>--}}
        @if ($errors->has('valid_upto'))
        <div class="danger">{{ $errors->first('valid_upto') }}</div>
        @endif
    </div>
</div>
<div class="form-group ">
    <label for="max_discount" class="col-sm-2 control-label">Max Discount</label>
    <div class="col-sm-10">
        <input id="max_discount" type="text" class="form-control" name="max_discount" value="{{ $deals->max_discount}}"
            required autocomplete="max_discount" autofocus>
        @if ($errors->has('max_discount'))
        <div class="danger">{{ $errors->first('max_discount') }}</div>
        @endif
    </div>
</div>

<div class="form-group ">
    <label for="discount_percentage" class="col-sm-2 control-label">Discount (%)</label>
    <div class="col-sm-10">
        <input id="discount_percentage" type="text" class="form-control" name="discount_percentage"
            value="{{ $deals->discount_percentage }}" required autocomplete="discount_percentage" autofocus>
        @if ($errors->has('discount_percentage'))
        <div class="danger">{{ $errors->first('discount_percentage') }}</div>
        @endif
    </div>
</div>

<div class="form-group ">
    <label for="quantity" class="col-sm-2 control-label">Quantity</label>
    <div class="col-sm-10">
        <input id="quantity" type="text" class="form-control" name="quantity" value="{{ $deals->quantity }}" autofocus>
        @if ($errors->has('quantity'))
        <div class="danger">{{ $errors->first('quantity') }}</div>
        @endif
    </div>
</div>
<div class="form-group ">
    <label for="max_count_for_user" class="col-sm-2 control-label">Max per User</label>
    <div class="col-sm-10">
        <input id="max_count_for_user" type="text" class="form-control" name="max_count_for_user"
            value="{{ $deals->max_count_for_user }}" required>
        @if ($errors->has('max_count_for_user'))
        <div class="danger">{{ $errors->first('max_count_for_user') }}</div>
        @endif
    </div>
</div>
<div class="form-group ">
    <label for="points_rewarded" class="col-sm-2 control-label">Wing points rewarded </label>
    <div class="col-sm-10">
        <input id="points_rewarded" type="text" class="form-control" name="points_rewarded"
            value="{{ $deals->points_rewarded }}" autofocus>
        @if ($errors->has('points_rewarded'))
        <div class="danger">{{ $errors->first('points_rewarded') }}</div>
        @endif
    </div>
</div>

<div class="form-group ">
    <label for="max_wing_points_assured" class="col-sm-2 control-label">Top Deal</label>
    <div class="col-sm-10">
        <input type="checkbox" class="" @if ($deals->top == 1)
        checked
        @endif
        value="1"
        name="top" >
        @if ($errors->has('top'))
        <div class="danger">{{ $errors->first('top') }}</div>
        @endif
    </div>
</div>

<div class="form-group ">
    <label for="max_wing_points_assured" class="col-sm-2 control-label">Active</label>
    <div class="col-sm-10">
        <input type="checkbox" class="" @if ($deals->active == 1)
        checked
        @endif
        value="1"
        name="active" >
        @if ($errors->has('active'))
        <div class="danger">{{ $errors->first('active') }}</div>
        @endif
    </div>
</div>

<div class="form-group ">
    <label for="terms_conditions" class="col-sm-2 control-label">Terms & Conditions</label>
    <div class="col-sm-10">
        <textarea id="terms_conditions" type="text" class="form-control" name="terms_conditions" rows="10"
            cols="80">{{ $deals->terms_conditions }}</textarea>
        @if ($errors->has('terms_conditions'))
        <div class="danger">{{ $errors->first('terms_conditions') }}</div>
        @endif
    </div>
</div>
<div class="pull-right">
    <button type="submit" class="btn btn-info">{{ __('Update') }}</button>
    <button type="button" class="btn btn-danger"
        onclick="window.history.go(-1); return false;">{{ __('Cancel') }}</button>
</div>

</div>
</form>
</div>
</section>
</div>
@endsection
@section('scripts')
<script src="{{ asset('adminlte/bower_components/bootstrap-datepicker/dist/js/bootstrap-datepicker.min.js') }}">
</script>
<script src="{{ asset('adminlte/bower_components/select2/dist/js/select2.full.min.js') }}"></script>
<script src="{{ asset('adminlte/bower_components/ckeditor/ckeditor.js') }}"></script>
<script>
    $(function () {
    CKEDITOR.replace('terms_conditions')
    CKEDITOR.replace('description')
  })
        $('.select2').select2()
        $(document).ready(function() {
            $('#valid_from').datepicker({
                autoclose: true,
                format: 'yyyy-mm-dd',
                startDate: new Date(),
            });

            $('#valid_upto').datepicker({
                autoclose: true,
                format: 'yyyy-mm-dd',
                startDate: new Date(),
            }).change(function () {
                var from = $('#valid_from').val();
                var to = $('#valid_upto').val();
                if(to < from){
                    $('#valid_upto').val("");
                    alert('to date should be grater than from date');
                }
            });
        })

</script>
<script>
    var loadFile = function(event) {
            var output = document.getElementById('output');
            output.src = URL.createObjectURL(event.target.files[0]);
          };
</script>
@endsection
