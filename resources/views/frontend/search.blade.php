@extends('layouts.app')

@section('content')
<style>

    /* card */

    .ps-post{
        padding:0px;
        border-radius: 25px;
    }
    .ps-post .ps-post__title{
        margin:10px 0 15px 0;
    }

    .ps-post .ps-post__content p{
        color: inherit;
    } 

    @media (max-width: 767px){
        .ps-post--small-thumbnail .ps-post__thumbnail {
            margin:0px;
        }
        .ps-post--small-thumbnail .ps-post__content {
            padding-left: 1.5rem;
        }
    }

    .ps-post .ps-post__thumbnail .ps-post__badge {
        top:inherit;
        bottom: 20px;
        border-radius:50%;
    }

    .ps-post .ps-post__thumbnail img{
        border-top-right-radius: 25px;
        border-bottom-right-radius: 25px;
    }
</style>
<div class="ps-breadcrumb">
    <div class="container">
        <ul class="breadcrumb text-capitalize" id="breadcrumb">
            <li><a href="{{ url('/') }}">Home</a></li>
            <li>Search</li>
        </ul>
    </div>
</div>

<section>
    <div class="ps-page--product">
        <div class="ps-container">

            <div class="ps-product--detail">
                <ul class="ps-tab-list">
                    <li class="active"><a href="#tab-shops">Shops</a></li>
                    <li><a href="#tab-deals">Deals</a></li>
                </ul>
                <div class="ps-tabs">

                <!-- shops tab -->
                <div class="ps-tab active" id="tab-shops">
                @if ($shops->isEmpty())
                <h3 class="text-danger text-uppercase">No Shops Found</h3>
                @else
                    @foreach ($shops as $item)
                        @php
                        $image = $item->shop_images;
                        $firstimage = $image[0];
                        @endphp

                        <div class="ps-post ps-post--small-thumbnail ps-highlight bg-dark text-white">
                            <div class="ps-post__content">
                                <div class="ps-post__top">
                                    <a class="ps-post__title" href="{!! url('/viewshop',[$item->shopid]); !!}">{{ $item->shop_name }}</a>
                
                                    <div class="ps-post__desc">
                                        <p><i class="fa fa-map-marker fa-lg text-warning pr-2"></i>{{ $item->address }}</p>
                                        <p><i class="fa fa-phone text-success pr-2"></i>{{ $item->phone_number }}</p>
                                        <p>
                                            @for ($i = 0; $i < 5; $i++) 
                                                @if ($i< (int)$item->shop_rating)
                                                <span class="fa fa-star text-danger"></span>
                                                @else
                                                    @if (($item->shop_rating-(int)$item->shop_rating) == 0.5 &&
                                                    ((int)$item->shop_rating) ==
                                                    $i )
                                                        <span class="fa fa-star-half-o text-danger"></span>
                                                    @else
                                                        <span class="fa fa-star text-danger"></span>
                                                    @endif
                                                @endif
                                            @endfor
                                        </p>

                                    </div>
                                </div>
                            </div>

                            <div class="ps-post__thumbnail">
                                <a class="ps-post__overlay" href="{!! url('/viewshop',[$item->shopid]); !!}"></a>
                                @if(!empty($firstimage->image_path))
                                    <img src="<?php echo asset("$firstimage->image_path"); ?>" alt="">
                                @endif 
                                
                                @if (in_array(date('N',time())+1 , explode(",",$item->active_days)))
                                    <div class="ps-post__badge bg-warning text-dark">Open</div>
                                @else
                                    <div class="ps-post__badge ps-close">Close</div>
                                @endif
                            </div>
                        </div>
                    @endforeach
                    @endif
                </div>
                <!-- end of shops tab -->

                <!-- deals tab -->
                <div class="ps-tab" id="tab-deals">
                    @if ($deals->isNotEmpty())
                        @foreach ($deals as $key=>$item)
                            <h4>{{ strtoupper($item->name) }}</h4>
                            <div class="m-3">
                                @if(!empty($item->pic))
                                    <img class="mb-5" src="<?php echo asset("{$item->pic}"); ?>" alt="" height="200px" width="auto">
                                @endif
                                <p><strong>Valid Upto:</strong> {{ $item->valid_upto }}</p>
                                <p><button class="btn btn-warning dealdetails" data-id="{{ $item->id }}">
                                    <strong>Details</strong></button>
                                </p>
                                <p>Flat {{ $item->discount_percentage }} % off*</p>
                                
                                @if (Auth::guest())
                                    <button class="btn btn-primary" data-toggle="modal" data-target="#myModal1">Claim Now</button>
                                @else
                                    @if ($item->payment_type != 'wingpoints')
                                    <a href="{{ url('/getdeal',[ $item->id]) }}" class="btn btn-danger"
                                        data-id="{{ $item->id }}">Claim Now</a>

                                    @else
                                        @if (Session::get('total_wingpoints')->points >= $item->price)
                                            <a href="{{ url('/getdeal',[ $item->id]) }}" class="btn btn-danger" data-id="{{ $item->id }}">Claim Now</a>
                                        @else
                                            <button class="btn btn-default" disabled> Not Enough Points </button>
                                        @endif
                                    @endif
                                @endif
                            </div>
                            <hr class="mb-5"></hr>
                        @endforeach
                    @else
                        <p>Sorry no deals available </p>
                    @endif
                </div>
                <!-- end of deals tab -->

            </div></div>
        </div>
    </div>
</section>
    
<script>
    $(document).on('click', ".unlike", function() {
        var id = $(this).data('id');
        var element = this;

        var data = {
            shop_id: id
        };
        $.ajax({
            type: "post",
            url: '{{ url("removefavouriteshop") }}',
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            },
            data: data,
            cache: false,
            success: function(data) {
                if (data.status == "success") {
                    $(element).removeClass('unlike').addClass('like').html('<i class="far fa-heart" style="color:#e31b23"></i>');
                }
            },
            error: function(data) {
                alert('Fail to run unlike..');
            }
        });


    });
    $(document).on('click', ".like", function() {
        var id = $(this).data('id');
        var element = this;

        var data = {
            shop_id: id
        };
        $.ajax({
            type: "post",
            url: '{{ url("addfavouriteshop") }}',
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            },
            data: data,
            cache: false,
            success: function(data) {
                console.log(data);
                if (data.status == "success") {
                    $(element).removeClass('like').addClass('unlike').html('<i class="fas fa-heart" style="color:#e31b23"></i>');

                }
            },
            error: function(data) {
                alert('Fail to run like..');
            }
        });


    });
</script>

@endsection