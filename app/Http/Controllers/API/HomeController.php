<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use App\Models\ImageDivData;
use App\Models\SliderData;

class HomeController extends Controller
{
    public $successStatus = 200;

    public function index()
    {
        $data['slider_1'] = SliderData::where('slider_id', 1)->get();
        $data['slider_2'] = SliderData::where('slider_id', 2)->get();
        $data['imagediv2_1'] = ImageDivData::where('image_div_id', 1)->get();
        $data['imagediv2_2'] = ImageDivData::where('image_div_id', 2)->get();
        $data['imagediv2_3'] = ImageDivData::where('image_div_id', 3)->get();
        $data['imagediv2_4'] = ImageDivData::where('image_div_id', 7)->get();
        $data['imagediv2_5'] = ImageDivData::where('image_div_id', 8)->get();
        $data['imagediv2_6'] = ImageDivData::where('image_div_id', 9)->get();
        $data['imagediv3_1'] = ImageDivData::where('image_div_id', 4)->get();
        $data['imagediv3_2'] = ImageDivData::where('image_div_id', 5)->get();
        $data['imagediv3_3'] = ImageDivData::where('image_div_id', 6)->get();

        return response()->json(['status' => 'success', 'data' => $data], $this->successStatus);
    }
}
