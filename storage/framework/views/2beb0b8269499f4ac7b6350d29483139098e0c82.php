

<?php $__env->startSection('content'); ?>
<div class="content-wrapper">
    <section class="content-header">
        <h1>
           Gifts
            <small>All Gifts</small>
        </h1>
        <!-- <ol class="breadcrumb">
              <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
              <li class="active">Dashboard</li>
            </ol> -->
    </section>
    <section class="content">
        <?php if(session()->get('success')): ?>
        <div class="alert alert-success" id="success">
            <?php echo e(session()->get('success')); ?>

        </div><br />
        <?php endif; ?>

        <div class="box box-primary">
            <div class="box-header with-border">
                <h3 class="box-title">All Gifts</h3>
                
            </div>

            <div class="table-responsive">
                <table class="table table-striped">
                    <thead>

                        <tr>
                            <th>ID</th>
                            <th>Name</th>
                            <th>Description</th>
                            <th>Image</th>
                            <th>Points To claim</th>
                            <th>Quantity</th>
                            <th>Claims per user</th>
                            <th>Start Date</th>
                            <th>End Date</th>
                            <th colspan="2">Action</th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php $__currentLoopData = $gifts; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $gift): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                        <tr>
                            <td><?php echo e($gift->id); ?></td>
                            <td><?php echo e($gift->gift_name); ?></td>
                            <td><?php echo $gift->description; ?></td>
                            <td><img src="<?php echo asset("$gift->image"); ?>" alt="image" height="50px" width="50px"></td>
                            <td><?php echo e($gift->points_to_claim); ?></td>
                            <td><?php echo e($gift->quantity); ?></td>
                            <td><?php echo e($gift->claims_per_user); ?></td>
                            <td><?php echo e($gift->start_date); ?></td>
                            <td><?php echo e($gift->end_date); ?></td>

                            <td><a class="btn btn-default" href="<?php echo e(route('edit-gift', [$gift->id])); ?>">Edit</a></td>

                            <td>
                                <form action="<?php echo e(url('/gift/delete', [$gift->id])); ?>" method="post">
                                    <?php echo csrf_field(); ?>
                                    <?php echo method_field('POST'); ?>
                                    <button class="btn btn-danger" type="submit">Delete</button>
                                </form>
                            </td>
                        </tr>
                        <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                    </tbody>
                </table>
            </div>
        </div>
    </section>
</div>
<?php $__env->stopSection(); ?>
<?php $__env->startSection('scripts'); ?>
<script>
    $('#success').delay(5000).fadeOut('slow');
    $('#reset').on('click',function(){
        // $("#filter")[0].reset();
        $('#shop_id').val('');
        $('#category_id').val('');
        $('#filter')[0].submit();
    });

</script>
<?php $__env->stopSection(); ?>

<?php echo $__env->make('layouts.admin', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH /home/offerkrx/public_html/offerswing/resources/views/gift/index.blade.php ENDPATH**/ ?>