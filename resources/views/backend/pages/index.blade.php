@extends('layouts.admin')

@section('content')
    <div class="content-wrapper">
        <section class="content-header">
            <h1>
                Shops
                <small>All Shops</small>
            </h1>
            <!-- <ol class="breadcrumb">
              <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
              <li class="active">Dashboard</li>
            </ol> -->
        </section>
        <section class="content">

            <div class="box box-primary">
                <div class="box-header with-border">
                    <h3 class="box-title">All Pages</h3>
                </div>
                <div class="table-responsive">
                    <table class="table table-striped">
                        <thead>
                        <tr>
                            <th>Page Name</th>
                            <th>Content</th>
                            <th colspan="3">Action</th>
                        </tr>
                        </thead>
                        <tbody>
                        @foreach($pages as $page)
                            <tr>
                                <td>{{ $page->page_name }}</td>
                                <td>{!! $page->content !!}</td>
                                {{-- <td><a href="{{ url('/pages/edit', [$page->id]) }}" class="btn btn-primary">View</a></td> --}}
                                <td><a href="{{ url('/pages/edit', [$page->id]) }}" class="btn btn-default">Edit</a></td>
                                <td><a class="btn btn-danger del" data-sub="false" data-id="{{$page->id}}">Delete</a></td>

                                {{-- <td><a href="{{ route('delete-shop', [$shop->id]) }}" class="btn btn-danger">Delete</a></td> --}}
{{--                                <td>Delete</td>--}}
                            </tr>
                        @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
    </section>
    </div>
@endsection
@section('scripts')

<script>
$('.del').on('click',function(){
        var id = $(this).data('id');
        if (confirm('Do you want to delete page?')) {
            // alert('Thanks for confirming');
            $.ajax({
                type: "POST",
                url: "{{  url('pages/delete/') }}"+"/"+id,
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
                success: function (response) {
                    // console.log(response);
                    // alert(response);
                    window.location.reload();
                }
            });
        } else {

        }

});
</script>

@endsection
