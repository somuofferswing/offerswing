<?php

namespace App\Http\Controllers\Backend;

use App\Http\Controllers\Controller;
use App\Models\NewsFeed;
use App\Models\NewsImage;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Storage;

class NewsfeedController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index()
    {
        $news = NewsFeed::all();

        return view('backend.news.index', compact('news'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function create()
    {
        // $shops = Shop::all();

        // $days = Days::all();

        // $category = Category::getCategories();

        return view('backend.news.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @return Response
     */
    public function store(Request $request)
    {
        $news = new NewsFeed();
        

        $this->validate($request, [
            'title' => 'required',
            'description' => 'required',
        ]);

        $news->title = $request['title'];

        $news->description = $request['description'];

        $news->video_link = $request['video_link'];

        $slug = $this->slugify($request['title']);

        $news->slug = $slug;
        
        $news->save();



        if ($request->hasFile('images')) {
            $images = $request->file('images');
            foreach ($images as $key=>$file) {
                $news_image = new NewsImage();
                $fileName = $slug.'_'.time().'.'.$file->getClientOriginalExtension();
                $path = $file->storeAs('news', $fileName, 'public');
                $news_image->image = $path;
                $news->newsImages()->save($news_image);
            }
        }

        return redirect('/news')->with('success', 'NewsFeed Ceated Successfully');
    }

    /**
     * Display the specified resource.
     *
     * @param int $id
     *
     * @return Response
     */
    public function show($id)
    {
        $news = NewsFeed::find($id);

        return view('backend.news.show', compact('news'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param int $id
     *
     * @return Response
     */
    public function edit($id)
    {
        // $shops = Shop::all();

        // $days = Days::all();

        // $category = Category::getCategories();

        $news = NewsFeed::find($id);

        return view('backend.news.edit', compact('news'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param int $id
     *
     * @return Response
     */
    public function update(Request $request, $id)
    {
        $news = NewsFeed::find($id);
        // $news_image = new NewsImage();

        $this->validate(
            $request,
            [
            'title' => 'required',
            'description' => 'required',
        ]
        );

        $news->title = $request['title'];

        $news->description = $request['description'];

        $news->video_link = $request['video_link'];

        if ($news->isDirty('title')) {
            $slug = $this->slugify($request['title']);
            $news->slug = $slug;
        } else {
            $slug = $news->slug;
        }

        $news->save();

        if ($request->hasFile('images')) {
            $images = $request->file('images');
            foreach ($images as $key=>$file) {
                $news_image = new NewsImage();
                $fileName = $slug.'_'.time().'.'.$file->getClientOriginalExtension();
                $path = $file->storeAs('news', $fileName, 'public');
                $news_image->image = $path;
                $news->newsImages()->save($news_image);
            }
        }


        return redirect('/news')->with('success', 'NewsFeed Updated Successfully');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param int $id
     *
     * @return Response
     */
    public function destroy($id)
    {
        $news = NewsFeed::find($id);

        $news->delete();

        return redirect('/news')->with('success', 'NewsFeed has been deleted Successfully');
    }

    public function deleteNewsImage($id)
    {
        $news = NewsImage::find($id);

        Storage::disk('public')->delete($news->image);

        $news->delete();

        return "Success";

        // return redirect('/news')->with('success', 'NewsFeed has been deleted Successfully');
    }

    public function slugify($str)
    {
        $search = ['Ș', 'Ț', 'ş', 'ţ', 'Ş', 'Ţ', 'ș', 'ț', 'î', 'â', 'ă', 'Î', 'Â', 'Ă', 'ë', 'Ë'];
        $replace = ['s', 't', 's', 't', 's', 't', 's', 't', 'i', 'a', 'a', 'i', 'a', 'a', 'e', 'E'];
        $str = str_ireplace($search, $replace, strtolower(trim($str)));
        $str = preg_replace('/[^\w\d\-\ ]/', '', $str);
        $str = str_replace(' ', '-', $str);

        return $this->getUniqueNewsSlug(preg_replace('/\-{2,}/', '-', $str));
    }

    private function getUniqueNewsSlug($slug)
    {
        $existingCount = NewsFeed::where('slug', 'like', $slug . '-%')->count();

        if ($existingCount) {
            return $slug . '-' . ($existingCount);
        }

        return $slug;
    }
}
