<?php $__env->startSection('content'); ?>
<style>

    /* card */
    .ps-post .ps-post__title{
        margin:10px 0 15px 0;
    }

    .ps-post .ps-post__content p{
        color: inherit;
    } 

    @media (max-width: 767px){
        .ps-post--small-thumbnail .ps-post__thumbnail {
            margin:0px;
        }
        .ps-post--small-thumbnail .ps-post__content {
            padding-left: 1.5rem;
        }
    }

    .ps-post .ps-post__thumbnail .ps-post__badge {
        top:inherit;
        bottom: 20px;
        border-radius:50%;
    }
	
	.ps-post--small-thumbnail .ps-post__content{
		padding: 0px 20px;
	}

    .zoom:hover {
        transform: scale(1.5);
        -webkit-transition: transform .2s ease-in-out;
    }

</style>
<div class="ps-breadcrumb">
    <div class="ps-container">
        <ul class="breadcrumb text-capitalize" id="breadcrumb">
            <li><a href="<?php echo e(url('/')); ?>">Home</a></li>
            <li>Shops</li>
        </ul>
    </div>
</div>
<section>
    <div class="ps-page--blog">
        <div class="container">
            <div class="ps-page__header">
                <h1>Shops List</h1>
                <hr class="highlighter">
            </div>
            <div class="ps-blog--sidebar">
                <div class="ps-blog__left pr-0">
    <?php
    $category_id = request()->route('id');
    $category = \App\Category::find($category_id);
    $parent_category = $category->parentCategories;
    ?>
  
    <?php if($shops->isNotEmpty()): ?>
        <?php $__currentLoopData = $shops; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $item): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
            <div class="ps-post ps-post--small-thumbnail ps-highlight bg-dark text-white">
                <div class="ps-post__thumbnail">
                    <a class="ps-post__overlay" href="<?php echo url('/viewshop',[$item->shopid]);; ?>"></a>
                    <img src="<?php echo e(asset('images/1572594588258leo-medical-centre-srikakulam-ho-srikakulam-pathology-labs-etq71oy.jpg')); ?>" alt="">
                    
                    <?php if(in_array(date('N',time())+1 , explode(",",$item->active_days))): ?>
                        <div class="ps-post__badge bg-warning text-dark">Open</div>
                    <?php else: ?>
                        <div class="ps-post__badge ps-close">Close</div>
                    <?php endif; ?>
                </div>
                <div class="ps-post__content">
                    <div class="ps-post__top">
                        <a class="ps-post__title" href="<?php echo url('/viewshop',[$item->shopid]);; ?>"><?php echo e($item->shop_name); ?>


                            <?php if(Auth::guest()): ?>
                                 <span class="badge zoom ml-2" onClick="return false;" data-id="<?php echo e($item->shopid); ?>" data-toggle="modal" data-target="#myModal1">
                                    <span data-toggle="tooltip" data-placement="right" title="" data-original-title="Like">
                                        <i class="fa fa-heart-o fa-lg text-danger"></i>
                                    </span>
                                </span>
                            <?php else: ?>
                                <?php if(in_array($item->shopid,$likes)): ?>
                                    <span class="badge zoom ml-2 unlike" data-id="<?php echo e($item->shopid); ?>" data-toggle="tooltip" data-placement="right" title="" data-original-title="Unlike">
                                        <i class="fa fa-heart fa-lg text-danger"></i>
                                    </span>
                                <?php else: ?>
                                    <span class="badge zoom ml-2 like" data-id="<?php echo e($item->shopid); ?>" data-toggle="tooltip" data-placement="right" title="" data-original-title="Like">
                                        <i class="fa fa-heart-o fa-lg text-danger"></i>
                                    </span>
                                <?php endif; ?>
                            <?php endif; ?>
                            
                        </a>
    
                        <div class="ps-post__desc">
                            <p>
                                <i class="fa fa-map-marker fa-lg text-warning pr-2"></i>
                                <?php echo e($item->address); ?>

                            </p>
                            <p><i class="fa fa-phone text-success pr-2"></i> 
                                <?php echo e($item->phone_number); ?>

                            </p>
                            <p class="mb-3">
                                <?php for($i = 0; $i < 5; $i++): ?> 
                                    <?php if($i< (int)$item->shop_rating): ?>
                                    <span class="fa fa-star text-danger"></span>
                                    <?php else: ?>
                                        <?php if(($item->shop_rating-(int)$item->shop_rating) == 0.5 &&
                                        ((int)$item->shop_rating) ==
                                        $i ): ?>
                                            <span class="fa fa-star-half-o text-danger"></span>
                                        <?php else: ?>
                                            <span class="fa fa-star text-danger"></span>
                                        <?php endif; ?>
                                    <?php endif; ?>
                                <?php endfor; ?>
                            </p>
                        </div>
                    </div>
                </div>
            </div>

        <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
    <?php else: ?>
        <h3 class="text-danger text-uppercase">No Shops Found</h3>
    <?php endif; ?>
            </div>
            
            <div class="ps-blog__right d-none d-md-block">
                <div class="ps-block__slider">
                    <div class="ps-carousel--product-box owl-slider" data-owl-auto="true" data-owl-loop="true" data-owl-speed="7000" data-owl-gap="0" data-owl-nav="true" data-owl-dots="true" data-owl-item="1" data-owl-item-xs="1" data-owl-item-sm="1" data-owl-item-md="1" data-owl-item-lg="1" data-owl-duration="500" data-owl-mousedrag="off">
                        <a href="#">
                            <img src="http://nouthemes.net/html/martfury/img/slider/home-3/clothing-1.jpg" alt="">
                        </a>
                        <a href="#">
                            <img src="http://nouthemes.net/html/martfury/img/slider/home-3/clothing-2.jpg" alt="">
                        </a>
                        <a href="#">
                            <img src="http://nouthemes.net/html/martfury/img/slider/home-3/clothing-3.jpg" alt="">
                        </a>
                    </div>
                </div>
            </div>

        </div>
    </div>
</div>
</section>

<script>
    $(document).on('click', ".unlike", function () {
        var id = $(this).data('id');
        var element = this;

        var data = {
                shop_id:id
            };
        $.ajax({
                type: "post",
                url: '<?php echo e(url("removefavouriteshop")); ?>',
                headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                 },
                data: data,
                cache: false,
                success: function (data)
                {
                    if(data.status == "success"){
                        $(element).removeClass('unlike').addClass('like').html('<i class="far fa-heart" style="color:#e31b23"></i>');
                    }
                },
                error: function (data){
                    alert('Fail to run unlike..');
                }
            });


    });
    $(document).on('click', ".like", function () {
        var id = $(this).data('id');
        var element = this;

        var data = {
                shop_id:id
            };
        $.ajax({
                type: "post",
                url: '<?php echo e(url("addfavouriteshop")); ?>',
                headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                 },
                data: data,
                cache: false,
                success: function (data)
                {
                    console.log(data);
                    if(data.status == "success"){
                            $(element).removeClass('like').addClass('unlike').html('<i class="fas fa-heart" style="color:#e31b23"></i>');

                    }
                },
                error: function (data){
                    alert('Fail to run like..');
                }
            });


    });
</script>

<script>
    $(document).ready(function(){
        var category = JSON.parse('<?php echo json_encode($category); ?>');
        var parent_category = JSON.parse('<?php echo json_encode($parent_category); ?>');
        if(parent_category != null){
           var data =  toarray([parent_category]);
           var da = {
                    'id': category.id,
                    'category_name':category.category_name,
                    'parent_id':category.parent_id
                };
    
            data.unshift(da);
            data.reverse();
            breadcrumb(data);
        }else{
            var data = [];
            var da = {
                    'id': category.id,
                    'category_name':category.category_name,
                    'parent_id':category.parent_id
                };
            data.unshift(da);
            breadcrumb(data);
    
        }
    
        
        
        function toarray(data,cat){
            var categories = (cat== null) ? []: cat;
            data.forEach(element => {
                id = element.id;
                category_name = element.category_name;
                parent_id = element.parent_id;
    
                var data = {
                    'id': id,
                    'category_name':category_name,
                    'parent_id':parent_id
                };
    
                categories.push(data);
                var cat = categories;
                if(element.parent_categories) { toarray([element.parent_categories],cat); }
                
            });
            return categories;
        }
    
        function breadcrumb(data){
            data.forEach((element,key,array)=>{
                var id = element.id;
                var category_name = element.category_name;
                    category_name = category_name.toLowerCase();
                var parent_id = element.parent_id;
                    var li = `<li><a href="<?php echo e(url('subcategories')); ?>/`+id+`">`+category_name+`</a></li>`;
                    $('#breadcrumb').find(' > li:nth-last-child(1)').before(li);

            });
        }
        });
    
</script>
<?php $__env->stopSection(); ?>
<?php echo $__env->make('layouts.app', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH C:\xampp\htdocs\offerswing\resources\views/shopsbycategory.blade.php ENDPATH**/ ?>