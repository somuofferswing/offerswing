<?php

namespace App\Http\Controllers\API;

// use Illuminate\Http\Request;

use App\Http\Controllers\Controller;
use App\Models\Deal;

class DealsController extends Controller
{
    public $successStatus = 200;

    public function index()
    {
        return Deal::all();
    }

    public function getDealDetails($id)
    {
        $deal = Deal::find($id);

        if (!empty($deal)) {
            return response()->json(['status' => 'success', 'data' => $deal], $this->successStatus);
        }

        return response()->json(['status' => 'success', 'data' => 'No Data found'], $this->successStatus);
    }

    public function getShopDeals($id)
    {
        $offer = Deal::where('shop_id', $id)->get();

        if (!empty($offer)) {
            $deals[]['deals'] = $offer;

            $deals[]['shop'] = $offer->shop;

            return response()->json(['status' => 'success', 'data' => $deals], $this->successStatus);
        }

        return response()->json(['status' => 'success', 'data' => 'No Data found'], $this->successStatus);
    }

    public function getCategoryDeals($id)
    {
        $deals = Deal::where('category_id', $id)->get();

        if (!empty($offer)) {
            return response()->json(['status' => 'success', 'data' => $deals], $this->successStatus);
        }

        return response()->json(['status' => 'success', 'data' => 'No Data found'], $this->successStatus);
    }
}
