@extends('layouts.admin')



@section('content')
    {{-- <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/dropzone/5.5.1/dropzone.css"> --}}

    <link rel="stylesheet"
          href="{{ asset('adminlte/bower_components/bootstrap-datepicker/dist/css/bootstrap-datepicker.min.css') }}">

    <div class="content-wrapper">

        <section class="content-header">

            <h1>

                NewsFeed

                <small>Add News</small>

            </h1>

            <!-- <ol class="breadcrumb">

                  <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>

                  <li class="active">Dashboard</li>

                </ol> -->

        </section>


        <section class="content">


            <div class="box box-info">

                <div class="box-header with-border">

                    <h3 class="box-title">Add News</h3>

                </div>

                <form class="form-horizontal" method="POST" action="{{ route('store-news') }}"
                      enctype="multipart/form-data">

                    @csrf

                    <div class="box-body">

                        <div class="form-group ">

                            <label for="title" class="col-sm-2 control-label">Title *</label>

                            <div class="col-sm-10">

                                <input id="name" type="text" class="form-control" name="title"
                                       value="{{ old('title') }}"
                                       required>

                                @if ($errors->has('title'))

                                    <div class="danger">{{ $errors->first('title') }}</div>

                                @endif

                            </div>

                        </div>

                        <div class="form-group">

                            <label for="news_image" class="col-sm-2 control-label">Images * </label>

                            <div class="col-sm-10">

                                <div class="dropzone" id="my-dropzone" name="mainFileUploader">
                                    <div class="fallback">
{{--                                        <input name="file" type="file" multiple />--}}
                                        <input type="file" name="images[]" id="news_images" multiple
                                               accept="image/jpeg, image/png, image/gif,"><br/>
                                    </div>
                                </div>


                            </div>

                        </div>

                        <div class="form-group">

                            <label for="video_link" class="col-sm-2 control-label">Video Link </label>

                            <div class="col-sm-10">
                                <input type="text" class="form-control" id="video_link" name="video_link">
                            </div>

                        </div>

                        <div class="form-group ">

                            <label for="description" class="col-sm-2 control-label">Description *</label>

                            <div class="col-sm-10">

                            <textarea id="description" type="text" class="form-control" name="description" required
                                      autocomplete="description" autofocus>{{ old('description') }}</textarea>

                                @if ($errors->has('description'))

                                    <div class="danger">{{ $errors->first('description') }}</div>

                                @endif

                            </div>

                        </div>


                        <div class="pull-right">

                            <button type="submit" class="btn btn-info" id="submit">{{ __('Create') }}</button>

                            <button type="button" class="btn btn-danger"
                                    onclick="window.history.go(-1); return false;">{{ __('Cancel') }}</button>

                        </div>

                    </div>

                </form>

            </div>

        </section>

    </div>

@endsection

@section('scripts')
    {{-- <script src="https://cdnjs.cloudflare.com/ajax/libs/dropzone/5.5.1/dropzone.js"></script> --}}

    <script src="{{ asset('adminlte/bower_components/bootstrap-datepicker/dist/js/bootstrap-datepicker.min.js') }}">

    </script>

    <script src="{{ asset('adminlte/bower_components/ckeditor/ckeditor.js') }}"></script>

    <script>
        $(function () {


            CKEDITOR.replace('description')

        })

    </script>

    {{-- <script>
        Dropzone.options.myDropzone = {
            url: "{{ route('store-news') }}",
            autoProcessQueue: false,
            uploadMultiple: true,
            parallelUploads: 100,
            maxFiles: 100,
            acceptedFiles: "image/*",

            init: function () {

                var submitButton = document.querySelector("#submit");
                var wrapperThis = this;

                submitButton.addEventListener("click", function () {
                    wrapperThis.processQueue();
                });

                this.on("addedfile", function (file) {

                    // Create the remove button
                    var removeButton = Dropzone.createElement("<button class='btn btn-lg dark'>Remove File</button>");

                    // Listen to the click event
                    removeButton.addEventListener("click", function (e) {
                        // Make sure the button click doesn't submit the form:
                        e.preventDefault();
                        e.stopPropagation();

                        // Remove the file preview.
                        wrapperThis.removeFile(file);
                        // If you want to the delete the file on the server as well,
                        // you can do the AJAX request here.
                    });

                    // Add the button to the file preview element.
                    file.previewElement.appendChild(removeButton);
                });

                this.on('sendingmultiple', function (data, xhr, formData) {
                    formData.append("title", $("#title").val());
                    formData.append("video_link", $("#video_link").val());
                    formData.append("description", $("#description").val());
                });
            }
        };
    </script> --}}

@endsection
