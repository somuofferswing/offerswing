<?php $__env->startSection('content'); ?>
<style>
        .mgin1 {
            background: white;
            -moz-box-shadow: 0 0 5px #888;
            -webkit-box-shadow: 0 0 5px#888;
            box-shadow: 0 0 5px #888;
        }

        .git {
            padding: 10px;
        }

        input[type=text]:disabled {
            border: none;
            background-color: inherit;
            /* border-bottom: 2px solid red; */
        }

        .img-wrap {
            position: relative;
            display: inline-block;
            border: 1px red solid;
            font-size: 0;
        }

        .img-wrap .edit {
            position: absolute;
            top: 2px;
            right: 2px;
            z-index: 100;
            background-color: #FFF;
            padding: 5px 2px 2px;
            color: #000;
            font-weight: bold;
            cursor: pointer;
            opacity: .2;
            text-align: center;
            font-size: 22px;
            line-height: 10px;
            border-radius: 50%;
        }

        .img-wrap:hover .edit {
            opacity: 1;
        }
    </style>
<div class="content-wrapper">
    <section class="content-header">
        <h1>
            Dashboard
            <small>Control panel</small>
        </h1>
        <!-- <ol class="breadcrumb">
                  <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
                  <li class="active">Dashboard</li>
                </ol> -->
    </section>


    <section class="content">

        <h3>Profile</h3>
        <div class="row mgin1" style="padding:10px;">

            <!-- contact form -->
            <div class="col-md-6 col-md-offset-3 profile wow animated fadeInRight" data-wow-delay=".2s">
                <div class="form-group text-center">

                    <?php if(empty($user->profile_image)): ?>
                    <div class="img-wrap">
                        <input type="file" name="profile_image" id="profile_image" accept="image/*"
                            style="display:none;" onchange="readURL(this);" />
                        <span class="edit" onclick="chooseFile();"><i class="fa fa-pencil-alt"></i></span>
                        <img src="<?php echo e(asset('main/images/avatar.png')); ?>" width="300px"" class=" image_profile">
                    </div>
                    <?php else: ?>
                    <div class="img-wrap">
                        <input type="file" name="profile_image" id="profile_image" accept="image/*"
                            style="display:none;" onchange="readURL(this);" />
                        <span class="edit" onclick="chooseFile();"><i class="fas fa-pencil-alt"></i></span>
                        <img src="<?php echo asset("$user->profile_image"); ?>" alt="" width="300px"
                            style="border-radius:50%;" class="image_profile">

                    </div>
                    <?php endif; ?>

                </div>

                <form id="updateprofile" class="form-horizontal">
                    
                    <div class="form-group" style="margin-top:50px;">
                        <label class="control-label col-sm-3" for="email">Email:</label>
                        <div class="col-sm-9">
                            <input class="form-control" type="text" name="email" disabled value="<?php echo e($user->email); ?>">
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="control-label col-sm-3" for="fname">First Name:</label>
                        <div class="col-sm-9">
                            <input class="form-control" type="text" id="fname" name="first_name" disabled
                                value="<?php echo e($user->first_name); ?>">
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="control-label col-sm-3" for="fname">Last Name:</label>
                        <div class="col-sm-9">
                            <input class="form-control" type="text" name="last_name" disabled
                                value="<?php echo e($user->last_name); ?>">
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="control-label col-sm-3">Mobile:</label>
                        <div class="col-sm-9">
                            <input class="form-control" type="text" name="mobile" disabled value="<?php echo e($user->mobile); ?>">
                        </div>
                    </div>


                    <div class="form-group">
                        <label class="control-label col-sm-3">DOB:</label>
                        <div class="col-sm-9">
                            <input class="form-control datepicker" type="text" name="dob" disabled
                                value="<?php echo e($user->dob); ?>">
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="control-label col-sm-3">Gender:</label>
                        <div class="col-sm-9">
                            <label class="radio-inline"><input type="radio" name="gender" value="MALE"
                                    <?php echo e(($user->gender == "MALE") ? 'checked' : ''); ?> disabled>Male</label>
                            <label class="radio-inline"><input type="radio" name="gender" value="FEMALE"
                                    <?php echo e(($user->gender == "FEMALE") ? 'checked' : ''); ?> disabled>Female</label>
                            <label class="radio-inline"><input type="radio" name="gender" value="OTHERS"
                                    <?php echo e(($user->gender == "OTHERS") ? 'checked' : ''); ?> disabled>Other</label>
                        </div>
                    </div>





                    <div class="form-group">
                        <div class="col-sm-offset-3 col-sm-9">
                            <button type="submit" class="btn btn-primary" style="display:none"
                                id="submitbutton">Update</button>
                            <button type="button" class="btn btn-danger" style="display:none"
                                id="cancelbutton">Cancel</button>
                        </div>
                    </div>



                </form>
                <br>
                <br>
                <div class="form-group">
                    <div class="col-sm-offset-3 col-sm-9">
                        <button id="edit" class="btn btn-info"> Edit Profile </button>
                    </div>
                </div>

            </div>
        </div>
        <br><br>
        <h3>Change Password</h3>
        <div class="row mgin1" style="padding:20px;">
            <div class="profile wow animated fadeInRight" data-wow-delay=".2s">
                <form id="updateprofile_password" class="form-horizontal">

                    <div class="form-group">
                        <label class="control-label col-sm-2" for="email">Current Password:</label>
                        <div class="col-sm-10">
                            <input class="form-control" type="password" name="current_password" id="current_password"
                                value="">
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="control-label col-sm-2" for="email">New Password</label>
                        <div class="col-sm-10">
                            <input class="form-control" type="password" name="new-password" id="new-password" value="">
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="control-label col-sm-2" for="email">Confirm New Password</label>
                        <div class="col-sm-10">
                            <input class="form-control" type="password" name="new-password_confirmation"
                                id="new-password_confirmation" value="">
                        </div>
                    </div>

                    <div class="form-group">
                        <div class="col-sm-offset-2 col-sm-10">
                            <button type="submit" id="updatepassword" class="btn btn-primary " disabled>Update Password
                            </button>
                        </div>
                    </div>






                </form>

            </div>
        </div>
    </section>
</div>
<?php $__env->stopSection(); ?>
<?php $__env->startSection('scripts'); ?>

<script>
    function chooseFile() {
        $("#profile_image").click();
    }


    function readURL(input) {
        if (input.files && input.files[0]) {
            var reader = new FileReader();

            reader.onload = function (e) {
                $('.image_profile')
                    .attr('src', e.target.result)
                    .width(300);
            };

            reader.readAsDataURL(input.files[0]);

            var data = new FormData();
            var files = input.files[0]
            data.append('file', files);


            $.ajax({
                type: "POST",
                url: '<?php echo e(url("uploadprofileimage")); ?>',
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
                data: data,
                processData: false,
                contentType: false,
                cache: false,
                success: function (response) {
                    console.log(response);

                },
                error: function (data) {
                    alert('Fail to run like..');
                }
            });

        }
    }

    $('#edit').on('click', function () {
        $('#updateprofile input').prop('disabled', false);
        $('#updateprofile input[name="mobile"]').prop('disabled', true);
        $('#edit').hide();
        $('#submitbutton').show();
        $('#cancelbutton').show();

    });

    $('#cancelbutton').on('click', function () {
        $('#updateprofile input').prop('disabled', true);
        $('input[name="mobile"]').prop('disabled', true);
        $('#edit').show();
        $('#submitbutton').hide();
        $('#cancelbutton').hide();

    });

    $('.datepicker').datepicker({
        format: "yyyy-mm-dd",
        endDate: '-1d',
        autoclose: true,
        todayHighlight: true
    });

    $('#submitbutton').on('click', function (e) {
        e.preventDefault();

        var form = $('#updateprofile')[0];
        var formData = new FormData(form);
        var edit = $('#edit');
        var submitbutton = $('#submitbutton');
        $.ajax({
            type: "POST",
            url: '<?php echo e(url("updateprofile")); ?>',
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            },
            data: formData,
            processData: false,
            contentType: false,
            cache: false,
            success: function (response) {
                // console.log(response);
                if (response.status == 'success') {

                    $('#updateprofile input').prop('disabled', true);
                    $('#edit').show();
                    $('#submitbutton').hide();
                    $('#cancelbutton').hide();
                    alert(response.message);
                }else{
                    alert(response.message);
                }
            },
            error: function (data) {
                alert('Fail to run');
            }
        });
    });

    $('#updatepassword').on('click', function (e) {
        e.preventDefault();

        var form = $('#updateprofile_password')[0];
        var formData = new FormData(form);
        console.log(formData);
        for (var pair of formData.entries())
{
 console.log(pair[0]+ ', '+ pair[1]);
}
        $.ajax({
            type: "POST",
            url: '<?php echo e(url("updatepassword")); ?>',
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            },
            data: formData,
            processData: false,
            contentType: false,
            cache: false,
            success: function (response) {
                console.log(response);
                if (response.status == 'success') {

                    $('#new-password_confirmation').val('');
                    $('#new-password').val('');
                    $('#current_password').val('');

                    // $('#updateprofile input').prop('disabled', true);
                    // $('#edit').show();
                    // $('#submitbutton').hide();
                    // $('#cancelbutton').hide();
                    alert(response.message);
                }else{
                    alert(response.message);
                }
            },
            error: function (data) {
                alert('Fail to run');
            }
        });
    });


</script>


<?php $__env->stopSection(); ?>

<?php echo $__env->make('layouts.admin', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH /home/offerkrx/public_html/offerswing/resources/views/admin/profile.blade.php ENDPATH**/ ?>