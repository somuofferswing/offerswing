

<?php $__env->startSection('content'); ?>
<section >
    <div class="ps-page--product">
        <div class="container">

            <div class="row">
                <div class="col-12 col-md-4">
                    <div class="ps-post text-center">
                        <img src="<?php echo asset("$gift->image"); ?>">
                        <h3 class="pt-4 m-0"><?php echo e($gift->gift_name); ?></h3>
                    </div>
                </div>
                <div class="col-12 col-md-8 text-center pb-5">
                    <?php echo QrCode::size(250)->generate($order->order_id);; ?>

                    <div class="table-responsive">
                        <table class="table table-bordered ps-table ps-table--specification">
                            <tbody>
                                
                                <tr>
                                    <td>Claimed item</td>
                                    <td><?php echo e($gift->gift_name); ?></td>
                                </tr>
                                <tr>
                                    <td>Claimed on</td>
                                    <td><?php echo e(date('d-m-Y h:i A',strtotime($order_details->created_at))); ?></td>
                                </tr>
                            </tbody>
                        
                        </table>
                    </div>
                    <strong>Our team will contact you soon</strong>
                </div>
            </div>
            
        </div>
    </div>
</section>
<?php $__env->stopSection(); ?>
<?php echo $__env->make('layouts.app', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH /home/offerkrx/public_html/offerswing_test/resources/views/frontend/giftorders.blade.php ENDPATH**/ ?>