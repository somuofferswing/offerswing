

<?php $__env->startSection('content'); ?>
    <div class="content-wrapper">
        <section class="content-header">
            <h1>
                Tokens
                <small>All Tokens</small>
            </h1>
            <!-- <ol class="breadcrumb">
              <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
              <li class="active">Dashboard</li>
            </ol> -->
        </section>
        <section class="content">

            <div class="box box-primary">
                <div class="box-header with-border">
                    <h3 class="box-title">All Shops</h3>
                </div>
                <div class="table-responsive">
                    <table class="table table-striped">
                        <thead>
                        <tr>
                            <th>Token Id</th>
                            <th>Points Rewarded</th>

                            <th colspan="2">Action</th>
                        </tr>
                        </thead>
                        <tbody>
                        <?php $__currentLoopData = $tokens; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $token): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                            <tr>
                                <td><?php echo e($token->token_id); ?></td>
                                <td><?php echo e($token->points_rewarded); ?></td>

                                
                                <td><a href="<?php echo e(url('/token/edit', [$token->id])); ?>" class="btn btn-default">Edit</a></td>
                                <td>
                                    <form action="<?php echo e(url('/token/delete', [$token->id])); ?>" method="post">
                                        <?php echo csrf_field(); ?>
                                        <?php echo method_field('POST'); ?>
                                        <button class="btn btn-danger" type="submit">Delete</button>
                                    </form>
                                </td>

                                

                            </tr>
                        <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                        </tbody>
                    </table>
                </div>
            </div>
    </section>
    </div>
<?php $__env->stopSection(); ?>
<?php $__env->startSection('scripts'); ?>

<script>
$('.del').on('click',function(){
        var id = $(this).data('id');
        if (confirm('Do you want to delete shop?')) {
            // alert('Thanks for confirming');
            $.ajax({
                type: "POST",
                url: "<?php echo e(url('shop/delete/')); ?>"+"/"+id,
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
                success: function (response) {
                    // console.log(response);
                    alert(response);
                    window.location.reload();
                }
            });
        } else {

        }

});
</script>

<?php $__env->stopSection(); ?>

<?php echo $__env->make('layouts.admin', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH /home/offerkrx/public_html/offerswing_test/resources/views/backend/token/index.blade.php ENDPATH**/ ?>