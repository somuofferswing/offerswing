<?php $__env->startSection('content'); ?>
<div class="content-wrapper">
    <section class="content-header">
        
        <!-- <ol class="breadcrumb">
              <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
              <li class="active">Dashboard</li>
            </ol> -->
    </section>

    <section class="content">
        <div class="box">
            <div class="box-header with-border">
                <h3 class="box-title">Deal Info</h3>
            </div>
            <div class="box-body">
                <h2><?php echo e(strtoupper($deal->name)); ?></h2>
                <h4><u> Description</u></h4>
                <p><?php echo $deal->description; ?></p>
                <h4> <u>Terms & Conditions</u> </h4>
                <p><?php echo $deal->terms_conditions; ?></p>

                <h4> <u>Deal period</u> </h4>
                <p><?php echo e($deal->valid_from); ?> to <?php echo e($deal->valid_upto); ?></p>

                <h4> <u>Discount</u> </h4>
                <p><?php echo e($deal->discount_percentage); ?> percentage ( upto ₹.<?php echo e($deal->max_discount); ?>)</p>

            </div>
        </div>


        <div class="box">
            <div class="box-header with-border">
                <h3 class="box-title">All Claimed for this deal</h3>
            </div>
            <div class="box-body">
                <div class="table-responsive">
                    <table class="table table-striped">
                        <thead>
                            <tr>
                                <th>Claim Id</th>
                                <th>UserName</th>
                                <th>User Email</th>
                                <th>User mobile</th>
                                <th>Used at Shop</th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php $__currentLoopData = $claims; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $item): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                            <tr>
                                <td><?php echo e($item->unique_code); ?></td>
                                <td><?php echo e($item->first_name); ?></td>
                                <td><?php echo e($item->email); ?></td>
                                <td><?php echo e($item->mobile); ?></td>
                                <td>
                                    <?php if($item->used == 1): ?>
                                        yes
                                    <?php else: ?>
                                        No
                                    <?php endif; ?>

                                </td>
                            </tr>

                            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>

                        </tbody>


                    </table>
                </div>
            </div>
        </div>
    </section>
<?php $__env->stopSection(); ?>

<?php echo $__env->make('layouts.admin', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH C:\wamp64\www\offers\resources\views/deals/claimed.blade.php ENDPATH**/ ?>