@extends('layouts.app')

@section('content')
@include('layouts.appmainsidebar')
<div id='rightone'>
    <div class="col-md-12">

        <div class="row">
            <div class="col-md-8">

                @component('components.carousel',['slider'=> $slider_1])
                @slot('coursel_id')
                coursel
                @endslot
                @endcomponent

            </div>
            <div class="col-md-4">
                @component('components.carousel',['slider'=> $slider_2])
                @slot('coursel_id')
                coursel1
                @endslot
                @endcomponent
            </div>



        </div>
    </div>
    <div class="col-md-12 bs">



        <div class="row sw" style="margin-top:20px;">

            @foreach ($imagediv2_1 as $item)
            <div class="col-md-6">
                <div class="  ">
                    <img src="{{ asset("$item->image") }}" style="width:100%;" class="img-responsive " />
                </div>
            </div>
            @endforeach


        </div>

        <div class="row" style="margin-top: 20px;">

            @foreach ($imagediv3_1 as $item)
            <div class="col-md-4">
                <div class="  ">
                    <img src="{{ asset("$item->image") }}" style="width:100%;" class="img-responsive " />
                </div>
            </div>
            @endforeach


        </div>

        <div class="row" style="margin-top: 20px;">
            @foreach ($imagediv2_2 as $item)
            <div class="col-md-6">
                <div class="  ">
                    <img src="{{ asset("$item->image") }}" style="width:100%;" class="img-responsive " />
                </div>
            </div>
            @endforeach

        </div>

        <div class="row" style="margin-top: 20px;">
            @foreach ($imagediv3_2 as $item)
            <div class="col-md-4">
                <div class="  ">
                    <img src="{{ asset("$item->image") }}" style="width:100%;" class="img-responsive " />
                </div>
            </div>
            @endforeach
        </div>


        <div class="row" style="margin-top: 20px;">
            @foreach ($imagediv2_3 as $item)
            <div class="col-md-6">
                <div class="  ">
                    <img src="{{ asset("$item->image") }}" style="width:100%;" class="img-responsive " />
                </div>
            </div>
            @endforeach

        </div>

        <div class="row" style="margin-top: 20px;">
            @foreach ($imagediv3_3 as $item)
            <div class="col-md-4">
                <div class="  ">
                    <img src="{{ asset("$item->image") }}" style="width:100%;" class="img-responsive " />
                </div>
            </div>
            @endforeach

        </div>

        <div class="row" style="margin-top: 20px;">
            @foreach ($imagediv2_4 as $item)
            <div class="col-md-6">
                <div class="  ">
                    <img src="{{ asset("$item->image") }}" style="width:100%;" class="img-responsive " />
                </div>
            </div>
            @endforeach

        </div>
        <div class="row" style="margin-top: 20px;">
            @foreach ($imagediv2_5 as $item)
            <div class="col-md-6">
                <div class="  ">
                    <img src="{{ asset("$item->image") }}" style="width:100%;" class="img-responsive " />
                </div>
            </div>
            @endforeach

        </div>
        <div class="row" style="margin-top: 20px;">
            @foreach ($imagediv2_6 as $item)
            <div class="col-md-6">
                <div class="  ">
                    <img src="{{ asset("$item->image") }}" style="width:100%;" class="img-responsive " />
                </div>
            </div>
            @endforeach

        </div>

    </div>
</div>
<!-- loginmodel-->

<!--giveus feedback-->
<div class="modal fade  " tabindex='-1' id="myModal4" role="dialog">
    <div class="modal-dialog">

        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header">
                <!-- <button type="button" class="close" data-dismiss="modal">&times;</button> -->
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title">Give us Feedback</h4>
            </div>
            <div class="modal-body">
                <form action="/action_page.php">
                    <div class="form-group">
                        <label for="name">Name</label>
                        <input type="text" class="form-control" id="name">
                    </div>

                    <div class="form-group">
                        <label for="comment">Comment:</label>
                        <textarea class="form-control" rows="5" id="comment"></textarea>
                    </div>
                    <button type="submit" class="btn btn-default">Submit</button>
                </form>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
            </div>
        </div>

    </div>
</div>
@endsection
