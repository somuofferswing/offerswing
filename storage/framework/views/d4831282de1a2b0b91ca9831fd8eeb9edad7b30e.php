<!DOCTYPE html>
<html lang="en">

<head>
    <title>Bootstrap Example</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.0/css/bootstrap.min.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.0/js/bootstrap.min.js"></script>
</head>

<body>

    <div class="container">
        <div class="row">
            <div class="col-md-8 col-md-offset-2" style="padding:5px !important;">
                <div calss="row">

                    <img src="http://www.offerswing.com/main/images/newlogo.png" style="height:100px"
                        class="img-responsive" />



                </div>
                <div class="row">
                    <div class="col-md-6 col-xs-12">
                        <h4>Deals receipt</h4>
                        <h5>Order no: <?php echo e($order->order_id); ?></h5>
                        <h5>Fri,07 Jun 2019, 21:39:02</h5>
                    </div>
                    <div class="col-md-6 col-xs-12 text-right">
                        <h4>Address</h4>
                        <h5>Srikakulam</h5>

                        <h5>Andhra Pradesh</h5>

                        <div class="details" style="margin-top:20px;">
                            <h5>1234567890</h5>
                            <h5><a href="#">offerswing.com</a></h5>
                        </div>
                    </div>
                </div>
                <div class="row ">
                        <h4 class="text-center">Your Deal Order: <?php echo e($order->order_id); ?></h4>
                        <hr>
                        <div class="col-md-8 offset-2 border my-3">
                            <div class="image">
                                <img src="<?php echo e(asset('storage/'.$deal->pic)); ?>" class="img-fluid" style="width:100%; height:200px;">
                            </div>
                            <hr>
                            <div class="dealbottom">
                                <h5>
                                    Claimed Date : <span><?php echo e(date('d-m-Y',strtotime($order->created_at))); ?></span>

                                    <span class="float-right">Expires on : <?php echo e(date('d-m-Y',strtotime($order->valid_upto))); ?></span>
                                </h5>
                            </div>
                            <div class="text-center">
                                <?php echo QrCode::size(250)->generate($order->order_id);; ?>

                            </div>

                            <div class="dealdescription py-3">

                                <table style="margin:0 auto;">
                                    <tr>
                                        <td>Booking id</td>
                                        <td style="width:20px;">:</td>
                                        <td style="text-align:left;"><?php echo e($order->order_id); ?></td>
                                    </tr>
                                    <tr>
                                        <td>About Deal</td>
                                        <td style="width:20px;">:</td>
                                        <td style="text-align:left;"><?php echo e((($deal->original_price-$deal->price)/$deal->original_price)*100); ?>% Off</td>
                                    </tr>
                                    <tr>
                                        <td>Brought on</td>
                                        <td style="width:20px;">:</td>
                                        <td style="text-align:left;"><?php echo e(date('d-m-Y h:i A',strtotime($order->created_at))); ?></td>
                                    </tr>
                                    <tr>
                                        <td>Expires on</td>
                                        <td style="width:20px;">:</td>
                                        <td style="text-align:left;"><?php echo e(date('d-m-Y h:i A',strtotime($order->valid_upto))); ?></td>
                                    </tr>
                                </table>

                            </div>

                            <div class="storeaddress">
                                <?php
                                    $shop = $deal->shop;
                                ?>
                                <p> <b> Store Address : </b> <?php echo e($shop->address.",".$shop->city.",".$shop->state.",".$shop->pincode); ?></p>
                                <p> <b> Contact Number : </b> +91 <?php echo e($shop->phone_number.",". $shop->phone_number2); ?></p>

                            </div>
                            
                            <div class="storecondition">
                            <p><b>Terms & Conditions :</b> <?php echo $deal->terms_conditions; ?></p>
                                
                            </div>

                        </div>





                    </div>

            </div>
        </div>
    </div>

</body>

</html>
<?php /**PATH /home/offerkrx/public_html/offerswing_test/resources/views/email/order_deal_details.blade.php ENDPATH**/ ?>