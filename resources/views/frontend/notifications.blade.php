@extends('layouts.app')

@section('content')

<div class="container">
  <h4>Notifications</h4>
  @foreach ($notifications as $item)
  <div class="card text-white bg-primary">
    <div class="card-body">
      <p class="card-text">{{ $item->notification_message }}</p>
    </div>
  </div>
  @endforeach

</div>

@endsection
@section('scripts')

@endsection