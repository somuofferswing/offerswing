<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class Category extends Model
{
    protected $fillable = [
        'category_id', 'parent_id', 'is_active',
    ];

    public function parent()
    {
        return $this->belongsTo(Category::class, 'parent_id');
    }

    public function children()
    {
        return $this->hasMany(Category::class, 'parent_id');
    }

    public function childrenRecursive()
    {
        return $this->children()->with('childrenRecursive');
    }

    public function shops()
    {
        return $this->belongsToMany(Shop::class);
    }

    // public function offers()
    // {
    //     return $this->belongsToMany(Offer::class);
    // }

    // public function deals()
    // {
    //     return $this->belongsToMany(Deal::class);
    // }

    public function parentCategories()
    {
        return $this->parent()->with('parentCategories');
    }

    public static function getCategories()
    {
        $categories = DB::table('categories as c1')
            ->select('c1.id', 'c1.category_name', 'c2.id as sub_id', 'c2.category_name as sub_category_name', 'c3.id as sub_sub_id', 'c3.category_name as sub_sub_category_name')
            ->leftJoin('categories as c2', 'c2.parent_id', '=', 'c1.id')
            ->leftJoin('categories as c3', 'c3.parent_id', '=', 'c2.id')
            ->whereNull('c1.parent_id')
            ->get()
        ;
        foreach ($categories as $cat) {
            $category[$cat->id] = $cat->category_name;
            if ('' != $cat->sub_id) {
                $category[$cat->sub_id] = '-'.$cat->sub_category_name;
                if ('' != $cat->sub_sub_id) {
                    $category[$cat->sub_sub_id] = '--'.$cat->sub_sub_category_name;
                }
            }
        }
//        var_dump($category);die;
        return $category;
    }
}
