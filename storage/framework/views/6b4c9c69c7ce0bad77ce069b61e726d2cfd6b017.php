<?php $__env->startSection('content'); ?>

<style>
    {
            {
            $page->style
        }
    }
</style>

<div class="container">
    <nav aria-label="breadcrumb">
        <ol class="breadcrumb">
            <li class="breadcrumb-item"><a href="#">Home</a></li>
            <li class="breadcrumb-item active" aria-current="page"><?php echo e($page->page_name); ?></li>
        </ol>
    </nav>

    <?php echo $page->content; ?>

</div>


<?php $__env->stopSection(); ?>

<?php $__env->startSection('scripts'); ?>

<script>
    <?php echo e($page->script); ?>

</script>

<?php $__env->stopSection(); ?>

<?php echo $__env->make('layouts.app', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH C:\wamp64\www\offers\resources\views/page.blade.php ENDPATH**/ ?>