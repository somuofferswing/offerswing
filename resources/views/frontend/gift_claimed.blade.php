@extends('layouts.app')

@section('content')

<div class="container">
    <div class="row">
        @php
        $gift = $order->gift;
        @endphp
        <div class="col-md-6 col-md-offset-3 border my-3">
            <div class="image">
                <img src="<?php echo asset("$gift->image"); ?>" class="img-fluid" style="width:100%; height:200px;">
            </div>
            <hr>
            <div class="dealbottom">
                <h5>Gift Title : <span>{{ $gift->gift_name }}</span> </h5>
            </div>
            <div class="text-center">
                {!! QrCode::size(250)->generate($claimed->unique_code); !!}

            </div>

            <div class="dealdescription ">

                <table style="margin:0 auto;">
                    <tr>
                        <td>Booking id</td>
                        <td style="width:20px;">:</td>
                        <td style="text-align:left;">{{ $claimed->unique_code }}</td>
                    </tr>
                    <tr>
                        <td>Claimed item</td>
                        <td style="width:20px;">:</td>
                        <td style="text-align:left;">{{ $gift->gift_name }}</td>
                    </tr>
                    <tr>
                        <td>Claimed on</td>
                        <td style="width:20px;">:</td>
                        <td style="text-align:left;">{{ date('d-m-Y h:i A',strtotime($claimed->created_at)) }}</td>
                    </tr>
                    {{-- <tr>
                        <td>Gift Collecting Date</td>
                        <td style="width:20px;">:</td>
                        <td style="text-align:left;">26/10/2019</td>
                    </tr> --}}
                </table>

            </div>

            <div class="storeaddress text-center">
                <h5>Our team will contact you soon</h5>


            </div>

            {{-- <div class="storecondition">
                 <p><b>Terms & Conditions :</b> {!! $event->terms_conditions !!}</p>


            </div> --}}

        </div>

    </div>
</div>
</div>
@endsection

@section('scripts')


@endsection
