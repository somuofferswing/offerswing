<?php

return [
    'country_code' => '91',
    'default' => env('SMS_API_DEFAULT_GATEWAY', 'OTP'),

    'OTP' => [
        'method' => 'POST',
        'url' => 'https://sms.wegusinfotech.in/smsapi/user/send/transactional/',
        'params' => [
            'send_to_param_name' => 'Phone',
            'msg_param_name' => 'otp',
            'others' => [
                'authKey' => '$2b$10$HVKtPJmWxSlNXzbznfNCOudKxwhxYJUGcJdgNfMrcZrwwEccwzG.2',
                'senderId' => 'APOLSK',
                'tempId' => '2546',
            ],
        ],
        'headers' => [
            'Content-type' => 'application/json',
        ],
        'json' => true,
        'jsonToArray' => false,
        'wrapper' => 'data',
        // 'wrapperParams' => [
        //     'wrapperParam1' => '', //Wrapper Param
        // ],
        'add_code' => true,
    ],
];
