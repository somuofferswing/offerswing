<!DOCTYPE html>
<html lang="en-US">
    <head>
        <meta charset="utf-8">
    </head>
    <body>
        <h2>Contact:</h2>

        <div>
            <ul>
                <li>Name: {{$input['name']}}</li>
                <li>Email: {{$input['email']}}</li>
                <li>Subject: {{$input['subject']}}</li>
                <li>Message: {{$input['message']}}</li>
            </ul>
        </div>

    </body>
</html>
