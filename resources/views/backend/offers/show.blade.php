@extends('layouts.admin')

@section('content')
<div class="content-wrapper">
    <section class="content-header">
        <h1>
            Offers
            <small>View Offer ({{$offer->offer_name }})</small>
        </h1>
        <!-- <ol class="breadcrumb">
              <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
              <li class="active">Dashboard</li>
            </ol> -->
    </section>

    <section class="content">

        <img src="<?php echo asset("$offer->offer_image"); ?>" alt="{{$offer->offer_name }}" class="img-responsive"
            width="100%">

        <hr>
        <div class="table-responsive">
            <table class="table table-striped">
                <tr>
                    <td>Name</td>
                    <td>{{$offer->offer_name}}</td>
                </tr>
                <tr>
                    <td>Description</td>
                    <td>{{$offer->description}}</td>
                </tr>
                <tr>
                    <td>From Date</td>
                    <td>{{$offer->from_date}}</td>
                </tr>
                <tr>
                    <td>To Date</td>
                    <td>{{$offer->to_date}}</td>
                </tr>
            </table>

        </div>

    </section>
</div>
@endsection