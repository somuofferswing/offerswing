@extends('layouts.admin')

@section('content')
<div class="content-wrapper">
    <section class="content-header">
        <h1>
            Category
            <small>Add Page</small>
        </h1>
        <!-- <ol class="breadcrumb">
                      <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
                      <li class="active">Dashboard</li>
                    </ol> -->
    </section>

    <section class="content">

        <div class="col-md-8 col-md-offset-2">
            <div class="box box-info">
                <div class="box-header with-border">
                    <h3 class="box-title">Update Page</h3>
                </div>
                <form class="form-horizontal" method="POST" action="{{ url('/pages/edit',[$page->id]) }}"
                    enctype="multipart/form-data">
                    @csrf
                    <div class="box-body">

                        <div class="form-group">
                            <label for="page_name" class="col-sm-2 control-label">Page Name</label>
                            <div class="col-sm-10">
                                <input id="page_name" type="text" class="form-control" name="page_name"
                                    value="{{ $page->page_name }}" required disabled autofocus>
                            </div>
                        </div>

                        <div class="form-group">
                            <label for="content" class="col-sm-2 control-label">Page Content</label>
                            <div class="col-sm-10">
                                <textarea id="content" name="content" required>{!! $page->content  !!}</textarea>
                            </div>
                        </div>

                        <div class="form-group">
                            <label for="style" class="col-sm-2 control-label">Page Style</label>
                            <div class="col-sm-10">
                                <textarea id="style" name="style" required>{!! $page->style  !!}</textarea>
                            </div>
                        </div>

                        <div class="form-group">
                            <label for="script" class="col-sm-2 control-label">Page Script</label>
                            <div class="col-sm-10">
                                <textarea id="script" name="script" required>{!! $page->script  !!}</textarea>
                            </div>
                        </div>


                        <button type="submit" class="btn btn-info pull-right">{{ __('Update') }}</button>
                        <button type="button" onclick="window.history.back()">{{ __('Back') }}</button>
                    </div>
                </form>
            </div>
        </div>
    </section>
</div>
@endsection
@section('scripts')
<script src="{{ asset('adminlte/bower_components/ckeditor/ckeditor.js') }}"></script>
<script>
    $(function () {

CKEDITOR.replace('content');
CKEDITOR.replace('style');
CKEDITOR.replace('script');
})
</script>

@endsection
