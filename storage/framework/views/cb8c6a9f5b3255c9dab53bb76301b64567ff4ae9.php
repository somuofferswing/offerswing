<?php $__env->startSection('content'); ?>

<div class="content-wrapper">
    <section class="content-header">
        <h1>
            Users
            <small>Add user</small>
        </h1>
        <!-- <ol class="breadcrumb">
                  <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
                  <li class="active">Dashboard</li>
                </ol> -->
    </section>

    <section class="content">
        
        <div class="box box-info">
            <div class="box-header with-border">
                <h3 class="box-title">Add Shop</h3>
            </div>
            <?php if($errors->any()): ?>
            <div class="alert alert-danger">
                <ul>
                    <?php $__currentLoopData = $errors->all(); $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $error): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                    <li><?php echo e($error); ?></li>
                    <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                </ul>
            </div><br />
            <?php endif; ?>
            <div class="box-body" style="padding: 10px 50px;">
                <form class="form-horizontal" method="POST" role="form" action="<?php echo e(url("/storeuser")); ?>">

                    <?php echo csrf_field(); ?>
                    
                    <div class="form-group">
                        <label for="register_user_type" class="col-md-3 control-label"><?php echo e(__('User Type')); ?></label>
                        <div class="col-md-9">
                            <select class="form-control" name="user_type">
                                <?php $__currentLoopData = $user_types; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $item): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>

                                <option value="<?php echo e($item->id); ?>"><?php echo e($item->type_name); ?>

                                <option>

                                    <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                            </select>
                            <span id="user_type_error"></span>
                        </div>

                    </div>

                    <div class="form-group">
                        <label for="register_firstname" class="col-md-3 control-label"><?php echo e(__('First Name')); ?></label>

                        <div class="col-md-9">
                            <input type="text" id="register_firstname" class="form-control" name="first_name" required>
                            <span id="firstname_error"></span>
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="register_lastname" class="col-md-3 control-label"><?php echo e(__('Last Name')); ?></label>
                        <div class="col-md-9">
                            <input type="text" id="register_lastname" class="form-control" name="last_name" required>
                            <span id="lastname_error"></span>
                        </div>
                    </div>

                    <div class="form-group">
                        <label for="register_mobile" class="col-md-3 control-label"><?php echo e(__('Mobile')); ?></label>
                        <div class="col-md-9">
                            <input type="text" id="register_mobile" class="form-control " name="mobile" required>
                            <span id="mobile_error"></span>
                        </div>
                    </div>

                    <div class="form-group">
                        <label for="register_email" class="col-md-3 control-label"><?php echo e(__('E-Mail Address')); ?></label>
                        <div class="col-md-9">
                            <input type="email" id="register_email" class="form-control" name="email" required>
                            <span id="email_error"></span>
                        </div>
                    </div>


                    


            <div class="form-group">
                <!-- Button -->
                <div class="col-md-offset-3 col-md-9">
                    <button id="btn-signup" type="submit" class="btn btn-info"><i class="icon-hand-right"></i>
                        &nbsp
                        create</button>
                </div>
            </div>
            </form>
        </div>
</div>

</section>
</div>




<?php $__env->stopSection(); ?>

<?php echo $__env->make('layouts.admin', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH C:\wamp64\www\offerswing_test\resources\views/backend/user/add.blade.php ENDPATH**/ ?>