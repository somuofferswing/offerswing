@extends('layouts.admin')

@section('content')
<div class="content-wrapper">
    <section class="content-header">
        <h1>
            Users
            <small>All Users</small>
        </h1>
        <!-- <ol class="breadcrumb">
              <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
              <li class="active">Dashboard</li>
            </ol> -->
    </section>
    <section class="content">

        <div class="box box-primary">
            <div class="box-header with-border">
                <h3 class="box-title">All Users</h3>
            </div>
            <div class="table-responsive">
                <table class="table table-striped">
                    <thead>
                        <tr>
                            <th>ID</th>
                            <th>User type</th>
                            <th>First Name</th>
                            <th>Last Name</th>
                            <th>Mobile</th>
                            <th>Email</th>
                            <th colspan="2">Action</th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach($users as $user)
                        <tr>
                            <td>{{$user->id}}</td>
                            <td>
                                @if ($user->user_type == 1)
                                <b>Admin</b>
                                @endif

                                @if ($user->user_type == 2)
                                <b>User</b>
                                @endif

                                @if ($user->user_type == 3)
                                <b>Retailer</b>
                                @endif
                            </td>
                            <td>{{$user->first_name}}</td>
                            <td>{{$user->last_name}}</td>
                            <td>{{$user->mobile}}</td>
                            <td>{{$user->email}}</td>

                            @if ($user->id != 1 || ($user->user_type == 'Admin' &&  $user->id != Auth::id()))
                            <td><button class="btn btn-default"><a
                                        href="{{ url('/user/edit', [$user->id]) }}">Edit</a></button></td>
                            {{-- <td><button class="btn btn-danger"><a href="#">Delete</a></button></td> --}}
                            <td>
                                @if ($user->status == 1)
                                <form action="{{ url('/user/deactivateUser', [$user->id]) }}" method="POST">
                                    @csrf
                                    {{-- @method('DELETE') --}}
                                    <button class="btn btn-warning" type="submit">Deactivate</button>
                                </form>

                                @else

                                <form action="{{ url('/user/activateUser', [$user->id]) }}" method="POST">
                                    @csrf
                                    {{-- @method('DELETE') --}}
                                    <button class="btn btn-success" type="submit">Activate</button>
                                </form>

                                @endif

                            </td>
                            <td>
                                <form action="{{ url('/user/deleteuser', [$user->id]) }}" method="POST"
                                    onSubmit="if(!confirm('Are you sure you want to delete User?')){return false;}">
                                    @csrf
                                    {{-- @method('DELETE') --}}
                                    <button class="btn btn-danger" type="submit">Delete</button>
                                </form>
                            </td>
                            @endif
                        </tr>
                        @endforeach
                    </tbody>
                </table>
            </div>
        </div>
    </section>
</div>
@endsection
