<!DOCTYPE html>
<html>

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <!-- CSRF Token -->
    <meta name="csrf-token" content="<?php echo e(csrf_token()); ?>">
    <title>OffersWing | Dashboard</title>

    <!-- Tell the browser to be responsive to screen width -->
    <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
    <!-- Bootstrap 3.3.7 -->
    <link href="<?php echo e(asset('adminlte/bower_components/bootstrap/dist/css/bootstrap.min.css')); ?>" rel="stylesheet">
    <!-- Font Awesome -->
    <link href="<?php echo e(asset('adminlte/bower_components/font-awesome/css/font-awesome.min.css')); ?>" rel="stylesheet">
    <!-- Ionicons -->
    <link href="<?php echo e(asset('adminlte/bower_components/Ionicons/css/ionicons.min.css')); ?>" rel="stylesheet">
    <!-- Select2 -->
    <link rel="stylesheet" href="<?php echo e(asset('adminlte/bower_components/select2/dist/css/select2.min.css')); ?>">
    <!-- Theme style -->
    <link href="<?php echo e(asset('adminlte/dist/css/AdminLTE.min.css')); ?>" rel="stylesheet">

    <!-- AdminLTE Skins. Choose a skin from the css/skins
         folder instead of downloading all of them to reduce the load. -->
    <link href="<?php echo e(asset('adminlte/dist/css/skins/_all-skins.min.css')); ?>" rel="stylesheet">
    <!-- Morris chart -->
    <link href="<?php echo e(asset('adminlte/bower_components/morris.js/morris.css')); ?>" rel="stylesheet">
    <!-- jvectormap -->
    <link href="<?php echo e(asset('adminlte/bower_components/jvectormap/jquery-jvectormap.css')); ?>" rel="stylesheet">
    <!-- Date Picker -->
    <link href="<?php echo e(asset('adminlte/bower_components/bootstrap-datepicker/dist/css/bootstrap-datepicker.min.css')); ?>"
        rel="stylesheet">
    <!-- Daterange picker -->
    <link href="<?php echo e(asset('adminlte/bower_components/bootstrap-daterangepicker/daterangepicker.css')); ?>"
        rel="stylesheet">
    <!-- bootstrap wysihtml5 - text editor -->
    <link href="<?php echo e(asset('adminlte/plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.min.css')); ?>" rel="stylesheet">

    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->

    <!-- Google Font -->
    <link rel="stylesheet"
        href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,600,700,300italic,400italic,600italic">
</head>

<body class="hold-transition skin-blue sidebar-mini">

    <?php if(session('alert')): ?>
    <div class="alert alert-<?php echo e(session('class')); ?>" id="alert">
        <?php echo e(session('alert')); ?>

    </div>
    <?php endif; ?>

    <?php if(auth()->guard()->guest()): ?>
    <?php echo $__env->yieldContent('content'); ?>
    <?php else: ?>
    <div class="wrapper">
        <?php if(Auth::user()->user_type == 1): ?>
            <?php echo $__env->make('layouts.adminheader', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>
            <?php echo $__env->make('layouts.adminsidebar', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>
            <?php echo $__env->yieldContent('content'); ?>
            <?php echo $__env->make('layouts.adminfooter', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>
        <?php else: ?>

        <?php echo $__env->make('layouts.adminheader', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>
        
        <?php echo $__env->yieldContent('content'); ?>
        <?php echo $__env->make('layouts.adminfooter', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>

        <?php endif; ?>

    </div>
    <?php endif; ?>

    <!-- ./wrapper -->

    <!-- jQuery 3 -->
    <script src="<?php echo e(asset('adminlte/bower_components/jquery/dist/jquery.min.js')); ?>"></script>
    <!-- jQuery UI 1.11.4 -->
    <script src="<?php echo e(asset('adminlte/bower_components/jquery-ui/jquery-ui.min.js')); ?>"></script>
    <!-- Resolve conflict in jQuery UI tooltip with Bootstrap tooltip -->
    <!-- <script>
  $.widget.bridge('uibutton', $.ui.button);
</script> -->
    <!-- Bootstrap 3.3.7 -->
    <script src="<?php echo e(asset('adminlte/bower_components/bootstrap/dist/js/bootstrap.min.js')); ?>"></script>
    <!-- Morris.js charts -->
    <script src="<?php echo e(asset('adminlte/bower_components/raphael/raphael.min.js')); ?>"></script>
    <script src="<?php echo e(asset('adminlte/bower_components/morris.js/morris.min.js')); ?>"></script>
    <!-- Sparkline -->
    <script src="<?php echo e(asset('adminlte/bower_components/jquery-sparkline/dist/jquery.sparkline.min.js')); ?>"></script>
    <!-- jvectormap -->
    <script src="<?php echo e(asset('adminlte/plugins/jvectormap/jquery-jvectormap-1.2.2.min.js')); ?>"></script>
    <script src="<?php echo e(asset('adminlte/plugins/jvectormap/jquery-jvectormap-world-mill-en.js')); ?>"></script>
    <!-- jQuery Knob Chart -->
    <script src="<?php echo e(asset('adminlte/bower_components/jquery-knob/dist/jquery.knob.min.js')); ?>"></script>
    <!-- daterangepicker -->
    <script src="<?php echo e(asset('adminlte/bower_components/moment/min/moment.min.js')); ?>"></script>
    <script src="<?php echo e(asset('adminlte/bower_components/bootstrap-daterangepicker/daterangepicker.js')); ?>"></script>
    <!-- datepicker -->
    <script src="<?php echo e(asset('adminlte/bower_components/bootstrap-datepicker/dist/js/bootstrap-datepicker.min.js')); ?>">
    </script>
    <!-- Bootstrap WYSIHTML5 -->
    <script src="<?php echo e(asset('adminlte/plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.all.min.js')); ?>"></script>
    <!-- Slimscroll -->
    <script src="<?php echo e(asset('adminlte/bower_components/jquery-slimscroll/jquery.slimscroll.min.js')); ?>"></script>
    <!-- FastClick -->
    <script src="<?php echo e(asset('adminlte/bower_components/fastclick/lib/fastclick.js')); ?>"></script>
    <!-- AdminLTE App -->
    <script src="<?php echo e(asset('adminlte/dist/js/adminlte.min.js')); ?>"></script>
    <script>
         $('#alert').delay(5000).fadeOut('slow');
    </script>
    <?php echo $__env->yieldContent('scripts'); ?>
</body>

</html>
<?php /**PATH /home/offerkrx/public_html/offerswing_test/resources/views/layouts/admin.blade.php ENDPATH**/ ?>