

<?php $__env->startSection('content'); ?>
<link rel="stylesheet"
    href="<?php echo e(asset('adminlte/bower_components/bootstrap-datepicker/dist/css/bootstrap-datepicker.min.css')); ?>">
<div class="content-wrapper">
    <section class="content-header">
        <h1>
            NewsFeed
            <small>Add News</small>
        </h1>
        <!-- <ol class="breadcrumb">
              <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
              <li class="active">Dashboard</li>
            </ol> -->
    </section>

    <section class="content">

        <div class="box box-info">
            <div class="box-header with-border">
                <h3 class="box-title">Add News</h3>
            </div>
            <form class="form-horizontal" method="POST" action="<?php echo e(url('/faq/store')); ?>" >
                <?php echo csrf_field(); ?>
                <div class="box-body">
                    <div class="form-group ">
                        <label for="question" class="col-sm-2 control-label">Question</label>
                        <div class="col-sm-10">
                            <input id="question" type="text" class="form-control" name="question" value="<?php echo e(old('question')); ?>"
                                required>
                            <?php if($errors->has('question')): ?>
                            <div class="danger"><?php echo e($errors->first('question')); ?></div>
                            <?php endif; ?>
                        </div>
                    </div>
                    <div class="form-group ">
                        <label for="answer" class="col-sm-2 control-label">Answer</label>
                        <div class="col-sm-10">
                            <textarea id="answer" type="text" class="form-control" name="answer" required
                                ><?php echo e(old('answer')); ?></textarea>
                            <?php if($errors->has('answer')): ?>
                            <div class="danger"><?php echo e($errors->first('description')); ?></div>
                            <?php endif; ?>
                        </div>
                    </div>

                    <div class="pull-right">
                            <button type="submit" class="btn btn-info"><?php echo e(__('Create')); ?></button>
                                <button type="button" class="btn btn-danger" onclick="window.history.go(-1); return false;"><?php echo e(__('Cancel')); ?></button>
                        </div>
</div>
</form>
</div>
</section>
</div>
<?php $__env->stopSection(); ?>
<?php $__env->startSection('scripts'); ?>
<script src="<?php echo e(asset('adminlte/bower_components/ckeditor/ckeditor.js')); ?>"></script>
<script>
    $(function () {

    CKEDITOR.replace('answer')
  })
</script>

<?php $__env->stopSection(); ?>

<?php echo $__env->make('layouts.admin', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH /home/offerkrx/public_html/offerswing/resources/views/faq/create.blade.php ENDPATH**/ ?>