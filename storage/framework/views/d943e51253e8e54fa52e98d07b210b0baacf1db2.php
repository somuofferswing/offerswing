<?php $__env->startSection('content'); ?>
    

    <link rel="stylesheet"
          href="<?php echo e(asset('adminlte/bower_components/bootstrap-datepicker/dist/css/bootstrap-datepicker.min.css')); ?>">

    <div class="content-wrapper">

        <section class="content-header">

            <h1>

                NewsFeed

                <small>Add News</small>

            </h1>

            <!-- <ol class="breadcrumb">

                  <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>

                  <li class="active">Dashboard</li>

                </ol> -->

        </section>


        <section class="content">


            <div class="box box-info">

                <div class="box-header with-border">

                    <h3 class="box-title">Add News</h3>

                </div>

                <form class="form-horizontal" method="POST" action="<?php echo e(route('store-news')); ?>"
                      enctype="multipart/form-data">

                    <?php echo csrf_field(); ?>

                    <div class="box-body">

                        <div class="form-group ">

                            <label for="title" class="col-sm-2 control-label">Title *</label>

                            <div class="col-sm-10">

                                <input id="name" type="text" class="form-control" name="title"
                                       value="<?php echo e(old('title')); ?>"
                                       required>

                                <?php if($errors->has('title')): ?>

                                    <div class="danger"><?php echo e($errors->first('title')); ?></div>

                                <?php endif; ?>

                            </div>

                        </div>

                        <div class="form-group">

                            <label for="news_image" class="col-sm-2 control-label">Images * </label>

                            <div class="col-sm-10">

                                <div class="dropzone" id="my-dropzone" name="mainFileUploader">
                                    <div class="fallback">

                                        <input type="file" name="images[]" id="news_images" multiple
                                               accept="image/jpeg, image/png, image/gif,"><br/>
                                    </div>
                                </div>


                            </div>

                        </div>

                        <div class="form-group">

                            <label for="video_link" class="col-sm-2 control-label">Video Link </label>

                            <div class="col-sm-10">
                                <input type="text" class="form-control" id="video_link" name="video_link">
                            </div>

                        </div>

                        <div class="form-group ">

                            <label for="description" class="col-sm-2 control-label">Description *</label>

                            <div class="col-sm-10">

                            <textarea id="description" type="text" class="form-control" name="description" required
                                      autocomplete="description" autofocus><?php echo e(old('description')); ?></textarea>

                                <?php if($errors->has('description')): ?>

                                    <div class="danger"><?php echo e($errors->first('description')); ?></div>

                                <?php endif; ?>

                            </div>

                        </div>


                        <div class="pull-right">

                            <button type="submit" class="btn btn-info" id="submit"><?php echo e(__('Create')); ?></button>

                            <button type="button" class="btn btn-danger"
                                    onclick="window.history.go(-1); return false;"><?php echo e(__('Cancel')); ?></button>

                        </div>

                    </div>

                </form>

            </div>

        </section>

    </div>

<?php $__env->stopSection(); ?>

<?php $__env->startSection('scripts'); ?>
    

    <script src="<?php echo e(asset('adminlte/bower_components/bootstrap-datepicker/dist/js/bootstrap-datepicker.min.js')); ?>">

    </script>

    <script src="<?php echo e(asset('adminlte/bower_components/ckeditor/ckeditor.js')); ?>"></script>

    <script>
        $(function () {


            CKEDITOR.replace('description')

        })

    </script>

    

<?php $__env->stopSection(); ?>

<?php echo $__env->make('layouts.admin', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH C:\wamp64\www\offerswing_test\resources\views/backend/news/create.blade.php ENDPATH**/ ?>