<?php $__env->startSection('content'); ?>

<style>
    ul.socialIcons {
        padding: 0;
        text-align: center;
    }

    .socialIcons li {
        background: yellow;
        list-style: none;
        display: inline-block;
        /* margin: 15px;
    margin-top: 15%; */
        border-radius: 2em;
        overflow: hidden;
    }

    .socialIcons li a {
        display: block;
        padding: .5em;
        max-width: 2.3em;
        min-width: 2.3em;
        height: 2.3em;
        white-space: nowrap;
        line-height: 1.5em;
        transition: .5s;
        text-decoration: none;
        font-family: arial;
        color: #fff;
    }

    .socialIcons li i {
        margin-right: .5em;
    }

    .socialIcons li:hover a {
        max-width: 200px;
        padding-right: 1em;
    }

    .socialIcons .facebook {
        background: #3b5998;
        box-shadow: 0 0 16px #3b5998;
    }

    .socialIcons .twitter {
        background: #00aced;
        box-shadow: 0 0 16px #00aced;
    }

    .socialIcons .instagram {
        background: #cd486b;
        box-shadow: 0 0 16px #cd486b;
    }

    .socialIcons .pinterest {
        background: #c92228;
        box-shadow: 0 0 16px #c92228;
    }

    .socialIcons .steam {
        background: #666666;
        box-shadow: 0 0 16px #666666;
    }
</style>

<div class="container">

    <nav aria-label="breadcrumb">
        <ol class="breadcrumb">
            <li class="breadcrumb-item"><a href="#">Home</a></li>
            <li class="breadcrumb-item active" aria-current="page">News Feed</li>
        </ol>
    </nav>
    <h4 style="font-weight: 800;text-align: center; " class="wng">News Feed</h4>
    <hr style="border-bottom:2px solid #df685d;width:5%;margin:0px auto;">
    <div class="row py-5">
        <?php $__currentLoopData = $newsfeed; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $item): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
        <div class="col-md-4">


            <div class="card">
                <img class="card-img-top border-bottom img-fluid" src="<?php echo asset("$item->news_image"); ?>"
                    alt="Card image cap" style="height:200px;">
                <div class="card-body">
                    <h5 class="card-title"><?php echo e(ucfirst($item->title)); ?></h5>
                    <p class="card-text"><?php echo $item->description; ?></p>
                    <div class="row border-top py-1">
                        <div class="col-md-2">
                            <?php if(Auth::guest()): ?>
                            <a class="btn" data-id="<?php echo e($item->id); ?>" data-toggle="modal" data-target="#myModal1"><i
                                    class="far fa-heart" style="color:#e31b23"></i>&nbsp;Like</a>
                            <?php else: ?>
                            <?php if(in_array($item->id,$likes)): ?>
                            <a class="btn unlike" data-id="<?php echo e($item->id); ?>"><i class="fas fa-heart"
                                    style="color:#e31b23"></i>&nbsp;Unlike</a>
                            <?php else: ?>
                            <a class="btn like" data-id="<?php echo e($item->id); ?>"> <i class="far fa-heart"
                                    style="color:#e31b23"></i>&nbsp;like</a>
                            <?php endif; ?>

                            <?php endif; ?>
                        </div>
                        <div class="col-md-10">
                            <ul class="socialIcons">
                                <li class="facebook"><a href="#"><i class="fab fa-fw fa-facebook"></i>Facebook</a></li>
                                <li class="twitter"><a href="#"><i class="fab fa-fw fa-twitter"></i>Twitter</a></li>
                                <li class="instagram"><a href="#"><i class="fab fa-fw fa-instagram"></i>Instagram</a>
                                </li>
                            </ul>
                        </div>
                    </div>
                    
                </div>
            </div>
        </div>
        <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
    </div>
</div>

</div>

<!-- <a class="btn btn-primary" >Trigger modal</a> -->
<div class="modal fade" id="share">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-body">
                <div class="a2a_kit a2a_kit_size_32 a2a_default_style">
                    <a class="a2a_button_facebook"></a>
                    <a class="a2a_button_twitter"></a>
                    <a class="a2a_button_whatsapp"></a>
                </div>
                <script async src="https://static.addtoany.com/menu/page.js"></script>
            </div>
        </div>
    </div>
</div>



<?php $__env->stopSection(); ?>

<?php $__env->startSection('scripts'); ?>
<script>
    $(document).on('click', ".unlike", function () {
        var id = $(this).data('id');
        var type = "news_feed";
        var element = this;

        var data = {
                id:id,
                type:type
            };
        $.ajax({
                type: "post",
                url: '<?php echo e(url("removelike")); ?>',
                headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                 },
                data: data,
                cache: false,
                success: function (data)
                {
                    if(data.status == "success"){
                        $(element).removeClass('unlike').addClass('like').html('<i class="far fa-heart" style="color:#e31b23"></i> &nbsp;like');
                    }
                },
                error: function (data){
                    alert('Fail to run unlike..');
                }
            });


    });
    $(document).on('click', ".like", function () {
        var id = $(this).data('id');
        var type = "news_feed";
        var element = this;

        var data = {
                id:id,
                type:type
            };
        $.ajax({
                type: "post",
                url: '<?php echo e(url("addlike")); ?>',
                headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                 },
                data: data,
                cache: false,
                success: function (data)
                {
                    console.log(data);
                    if(data.status == "success"){
                            $(element).removeClass('like').addClass('unlike').html('<i class="fas fa-heart" style="color:#e31b23"></i> &nbsp;Unlike');

                    }
                },
                error: function (data){
                    alert('Fail to run like..');
                }
            });


    });
</script>


<?php $__env->stopSection(); ?>
<?php echo $__env->make('layouts.app', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH C:\wamp64\www\offers\resources\views/newsfeed.blade.php ENDPATH**/ ?>