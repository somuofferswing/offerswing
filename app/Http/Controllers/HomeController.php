<?php

namespace App\Http\Controllers;

use App\Models\AddYourBusiness;
use App\Models\CartItem;
use App\Models\Category;
use App\Models\Claimed;
use App\Models\Contactus;
use App\Models\Days;
use App\Models\Deal;
use App\Models\Event;
use App\Models\Faq;
use App\Models\Gift;
use App\Models\ImageDivData;
use App\Models\ImageUpload;
use App\Models\Like;
use App\Models\NewsFeed;
use App\Models\Notification;
use App\Models\Offer;
use App\Models\Order;
use App\Models\Page;
use App\Models\Setting;
use App\Models\Shop;
use App\Models\SliderData;
use App\Models\User;
use App\Models\UserFavouriteShop;
use App\Models\WingPoints;
use Auth;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Crypt;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Validator;
use Mail;
use Session;
use Softon\Indipay\Facades\Indipay;

// use Illuminate\Support\Facades\Redirect;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     */
    // public function __construct()
    // {
    //      $this->middleware('auth');
    // }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        $slider_1 = SliderData::where('slider_id', 1)->get();
        $slider_2 = SliderData::where('slider_id', 2)->get();
        $imagediv2_1 = ImageDivData::where('image_div_id', 1)->get();
        $imagediv2_2 = ImageDivData::where('image_div_id', 2)->get();
        $imagediv2_3 = ImageDivData::where('image_div_id', 3)->get();
        $imagediv2_4 = ImageDivData::where('image_div_id', 7)->get();
        $imagediv2_5 = ImageDivData::where('image_div_id', 8)->get();
        $imagediv2_6 = ImageDivData::where('image_div_id', 9)->get();
        $imagediv3_1 = ImageDivData::where('image_div_id', 4)->get();
        $imagediv3_2 = ImageDivData::where('image_div_id', 5)->get();
        $imagediv3_3 = ImageDivData::where('image_div_id', 6)->get();

        return view('frontend.welcome', compact(
            'slider_1',
            'slider_2',
            'imagediv2_1',
            'imagediv2_2',
            'imagediv2_3',
            'imagediv2_4',
            'imagediv2_5',
            'imagediv2_6',
            'imagediv3_1',
            'imagediv3_2',
            'imagediv3_3'
        ));
    }

    public function subCategories($slug)
    {
        $category = Category::where('categories.slug', $slug)->first();
        $sub_categories = Category::where('categories.parent_id', $category['id'])->get();
        if ($sub_categories->isEmpty()) {
            return $this->shopsByCategory($slug);
        }

        return view('frontend.subcategories', compact('sub_categories', 'slug'));
    }

    public function shopsByCategory($slug)
    {
        $category = Category::where('categories.slug', $slug)->first();
        $id = $category['id'];
        $shops = Shop::whereHas('categories', function ($q) use ($id) {
            $q->where('categories.id', '=', $id);
        })->get();

        $days = config('global.days');
        $likes = '';
        if (Auth::check()) {
            $user = auth()->user();
            $likes = UserFavouriteShop::where('user_id', '=', $user['id'])
                ->pluck('shop_id')->toArray();
        }

        return view('frontend.shopsbycategory', compact('shops', 'likes', 'days'));
    }

    public function shopDetails($slug)
    {
        $shop = Shop::where('slug', $slug)->first();
        $id = $shop['id'];
        $date = date('Y-m-d', time());

        $shops = Shop::whereHas('users', function ($q) use ($id) {
            $q->where('shops.id', '=', $id);
        })->first();


        $images = ImageUpload::where('shop_id', $id)->get();
        $days = config('global.days');
        $offers = $shops->offersOngoing;
        $deals = $shops->dealsOngoing;

        $likes = '';
        if (Auth::check()) {
            $user = auth()->user();
            $likes = UserFavouriteShop::where('user_id', '=', $user['id'])->where('shop_id', $id)->first();
        }
        //pr($shops, 1);exit;
        return view('frontend.shopdetails', compact('shops', 'images', 'days', 'offers', 'deals', 'likes'));
    }

    public function aboutus()
    {
        return view('frontend.aboutus');
    }

    public function contactus()
    {
        return view('frontend.contactus');
    }

    public function addyourbusiness()
    {
        $categories = Category::whereNull('parent_id')->get();

        return view('frontend.addyourbusiness', compact('categories'));
    }

    public function wingdeals()
    {
        $date = date('Y-m-d', time());
        $deals = Deal::where('deal_end_date', '>=', $date)
                ->get();
        return view('frontend.wingdeals', compact('deals'));
    }

    public function faq()
    {
        $faqs = Faq::all();

        return view('frontend.faq', compact('faqs'));
    }

    public function getDealDetails($id)
    {
        $deals = Deal::leftjoin('shops', 'shops.id', '=', 'deals.shop_id')
            ->where('deals.id', $id)->get()->toArray();
        $days = Days::all();

        $days_applicable = explode(',', $deals[0]['days_applicable']);
        $deals[0]['days_applicable'] = [];

        foreach ($days as $value) {
            if (in_array($value['id'], $days_applicable)) {
                $deals[0]['days_applicable'][] = '<span class="badge badge-pill defaultBg text-white" style="padding-top:.4rem;padding-bottom:.4rem;border-radius:50%">'.substr($value['name'], 0, 3).'</span>';
            }
        }
        // echo json_encode($deals[0]['days_applicable']);die;
        echo json_encode($deals);
        exit;
    }

    // public function addtoCart($deal_id)
    // {
    //     $deal = Deal::find($deal_id)->toArray();
    //     $user = auth()->user();

    //     $cart_deal = CartItem::where('user_id', '=', $user['id'])
    //         ->where('deal_id', '=', $deal['id'])->first();
    //     if (empty($cart_deal)) {
    //         $cart = new CartItem();

    //         $cart->user_id = $user['id'];
    //         $cart->deal_id = $deal['id'];
    //         $cart->price = $deal['price'];
    //         $cart->quantity = 1;
    //         $cart->save();
    //     } else {
    //         $cart_deal = $cart_deal->toArray();
    //         $cart_id = $cart_deal['id'];
    //         $quantity = $cart_deal['quantity'] + 1;
    //         $cart = CartItem::find($cart_id);
    //         $cart->quantity = $quantity;
    //         $cart->price = $deal['price'] * $quantity;
    //         $cart->save();
    //     }
    //     $cart_items = CartItem::where('user_id', '=', $user['id'])
    //         ->get()->toArray();

    //     // echo json_encode($cart_items);
    //     Session::put('cart_items', $cart_items);
    //     exit;
    // }

    public function newsfeed()
    {
        $newsfeed = NewsFeed::orderBy('created_at', 'desc')->get();
        $likes = '';
        if (Auth::check()) {
            $user = auth()->user();
            $likes = Like::where('user_id', '=', $user['id'])
                ->where('type', '=', 'news_feed')->pluck('type_id')->toArray();
        }
        return view('frontend.newsfeed', compact('newsfeed', 'likes'));
    }

    public function events()
    {
        $events = Event::where('event_date', '>=', date('Y-m-d'))->orderBy('created_at', 'desc')->get();
        $likes = '';
        if (Auth::check()) {
            $user = auth()->user();
            $likes = Like::where('user_id', '=', $user['id'])
                ->where('type', '=', 'event')->pluck('type_id')->toArray();
        }

        return view('frontend.events', compact('events', 'likes'));
    }

    public function redeem()
    {
        $gifts = Gift::all();

        return view('frontend.redeem', compact('gifts'));
    }

    public function addfavouriteshop(Request $request)
    {
        $user = auth()->user();
        $shop_id = $request->shop_id;

        $favouriteShop = new UserFavouriteShop();

        $favouriteShop->user_id = $user['id'];
        $favouriteShop->shop_id = $shop_id;

        $favouriteShop->save();

        $msg = [
            'status' => 'success',
        ];

        return response()->json($msg);
    }

    public function removefavouriteshop(Request $request)
    {
        $user = auth()->user();
        $shop_id = $request->shop_id;

        // $like = new Like();

        $favouriteShop = UserFavouriteShop::where('user_id', '=', $user['id'])
            ->where('shop_id', '=', $shop_id)->first();

        $favouriteShop->delete();
        $msg = [
            'status' => 'success',
        ];

        return response()->json($msg);
    }

    public function allfavouriteshops()
    {
        $user = auth()->user();
        $user_id = $user['id'];

        $favouriteshops_ids = UserFavouriteShop::where('user_id', '=', $user['id'])
            ->pluck('shop_id')->toArray();

        $shops = Shop::whereIn('id', $favouriteshops_ids)->get();

        // $offers_shops = Offer::pluck('shop_id')->distinct()->toArray();
        $offers_shops = Offer::distinct()->pluck('shop_id')->toArray();
        $deals_shops = Deal::distinct()->pluck('shop_id')->toArray();

        return view('frontend.favouriteshops', compact('shops', 'favouriteshops_ids', 'offers_shops', 'deals_shops'));
    }

    public function addlike(Request $request)
    {
        $user = auth()->user();
        $type = $request->type;
        $type_id = $request->id;

        $like = new Like();

        $like->user_id = $user['id'];
        $like->type = $type;
        $like->type_id = $type_id;

        $like->save();

        $msg = [
            'status' => 'success',
        ];

        return response()->json($msg);
    }

    public function removelike(Request $request)
    {
        $user = auth()->user();
        $type = $request->type;
        $type_id = $request->id;

        // $like = new Like();

        $like = Like::where('user_id', '=', $user['id'])
            ->where('type', '=', $type)
            ->where('type_id', '=', $type_id)->first();

        $like->delete();
        $msg = [
            'status' => 'success',
        ];

        return response()->json($msg);
    }

    public function userprofile()
    {
        $user = auth()->user();

        return view('frontend.profile', compact('user'));
    }

    // public function getgift($id)
    // {
    //     $user = auth()->user();
    //     $gift = Gift::find($id);

    //     if ($gift->quantity > 0) {
    //         $is_exists = Order::where('user_id', $user['id'])->where('type', 'gift')->where('type_id', $id)->first();

    //         if (empty($is_exists)) {
    //             $order = new Order();

    //             $order_id = $this->generateRandomString(10);

    //             $order->order_id = $order_id;
    //             $order->user_id = $user['id'];
    //             $order->type = 'gift';
    //             $order->type_id = $id;
    //             $order->quantity = 1;
    //             $order->price = 0;
    //             $order->wing_points = $gift['points_to_claim'];
    //             // $order->wing_coins_max = $deal['wing_points_max'];

    //             $order->save();

    //             $idi = $order->id;
    //             $claim = new Claimed();

    //             $claim->claimed_type = $order['type'];
    //             $claim->claimed_type_id = $order['type_id'];
    //             $claim->order_id = $id;
    //             $claim->user_id = $order['user_id'];
    //             $claim->unique_code = $this->generateRandomString(5);

    //             $claim->save();

    //             $wing_points = new WingPoints();

    //             $wing_points->type = 'GIFT';
    //             $wing_points->type_id = $id;
    //             $wing_points->points = -$gift['points_to_claim'];

    //             $wing_points->save();

    //             $order->payment_status = 'SUCCESS';
    //             $order->save();

    //             // $event = Event::find($type_id);

    //             $count = $gift['quantity'] - $order['quantity'];

    //             $gift->quantity = $count;
    //             $gift->save();

    //             return redirect()->route('gift_claimed', [$order_id]);
    //             // return redirect()->route('user_order', [$order->id]);
    //         }

    //         return back()->with('alert', 'Only once per user')->with('class', 'danger');
    //     }

    //     return back()->with('alert', 'Sorry All claimed!')->with('class', 'danger');
    // }

    // public function getthisdeal($id)
    // {
    //     $user = auth()->user();
    //     $deal = Deal::find($id)->toArray();

    //     if ($deal['quantity'] > 0) {
    //         $orders = Order::where('user_id', $user['id'])->where('type', 'deal')->where('type_id', $id)->get();

    //         $orderscount = $orders->count();

    //         if ($orderscount <= $deal['max_count_for_user']) {
    //             $order = new Order();

    //             $order_id = $this->generateRandomString(10);

    //             $order->order_id = $order_id;
    //             $order->user_id = $user['id'];
    //             $order->type = 'deal';
    //             $order->type_id = $deal['id'];
    //             $order->quantity = 1;
    //             $order->price = $deal['price'];

    //             $order->save();

    //             return redirect()->route('user_order', [$order->id]);
    //         }

    //         return back()->with('alert', 'You Claimed Max This Deal')->with('class', 'danger');
    //     }

    //     return back()->with('alert', 'Sorry All Claimed!')->with('class', 'danger');
    // }

    // public function geteventtickets($id)
    // {
    //     $user = auth()->user();
    //     $event = Event::find($id)->toArray();

    //     if ($event->quantity > 0) {
    //         $order = new Order();

    //         $order_id = $this->generateRandomString(10);

    //         $order->order_id = $order_id;
    //         $order->user_id = $user['id'];
    //         $order->type = 'event';
    //         $order->type_id = $event['id'];
    //         $order->quantity = 1;
    //         $order->price = $event['event_price'];
    //         // $order->wing_coins_max = $deal['wing_points_max'];

    //         $order->save();

    //         return redirect()->route('user_order', [$order->id]);
    //     }
    // }

    public function order($id)
    {
        $id = Crypt::decrypt($id);
        $order = Order::find($id);
        
        if ('SUCCESS' != $order->payment_status) {
            $order_details = $order->orderDetails;
            $settings = Setting::where('name', 'referral_points_per_rupee')->first()->toArray();

            if ('DEAL' == $order_details['order_type']) {
                $deal = Deal::find($order_details['order_type_id'])->toArray();
                return view('frontend.orders', compact('order', 'order_details', 'deal', 'settings'));
            } elseif ('EVENT' == $order_details['order_type']) {
                $event = Event::find($order_details['order_type_id'])->toArray();
                return view('frontend.eventorders', compact('order', 'order_details', 'event', 'settings'));
            }
        }

        return redirect()->route('home');
    }

    public function updateorder(Request $request)
    {
        $id = $request['id'];
        $order = Order::find($id);
        $order_details = $order->orderDetails;

        if (isset($request['quantity'])) {
            $quantity = $order_details['quantity'] + $request['quantity'];
            $type = $order['type'];
            $type_id = $order['type_id'];

            if (0 != $quantity) {
                $order_details->quantity = $quantity;
                $order_details->final_price = $quantity * $order_details['price'];
                $order_details->save();

                $order->amount = $order_details->final_price;
                $order->save();

                $msg = [
                    'status' => 'success',
                ];

                return response()->json($msg);
            }
            $order->delete();
            $msg = [
                'status' => 'failed',
                'url' => '/eventdetails/'.$type_id,
            ];

            return response()->json($msg);
        }
    }

    public function deleteorder(Request $request)
    {
        $id = $request['id'];
        $order = Order::find($id);

        $type = $order['type'];
        $type_id = $order['type_id'];

        $order->delete();

        $msg = [
            'status' => 'success',
        ];

        return response()->json($msg);
    }

    public function proceedtopay($id)
    {
        $order = Order::where('order_id', $id)->first();

        // var_dump($order['user_id']);
        // die;

        $type = $order['type'];
        $type_id = $order['type_id'];
        $user_details = User::find($order['user_id']);

        // print_r($user_details);
        // die;

        $parameters = [
            'firstname' => $user_details['first_name'],
            'email' => $user_details['email'],
            'phone' => $user_details['mobile'],
            'productinfo' => json_encode(['order_id'=>$id,'order_type'=>$type,'order_type_id'=> $type_id]),
            // 'tid' => 1,
            // 'order_id' => $id,
            // 'amount' => $order['price'],
            'amount' => 1,
        ];

        $order12 = Indipay::prepare($parameters);

        return Indipay::process($order12);
    }

    public function indipay_response(Request $request)
    {
        $response = Indipay::response($request);

        if ($response['status'] == 'success') {
            $product_info = json_decode($response['productinfo']);

            $order_id = $product_info['order_id'];
            $order = Order::where('order_id', $order_id)->first();

            $order->status = "SUCCESS";
        }
    }

    public function claimed($id)
    {
        $user = auth()->user();
        $order = Order::where('order_id', $id)->first();
        // if ('deal' == $order['type']) {
        //     $deal = Deal::find($order['type_id']);
        // } else {
        //     $deal = Event::find($order['type_id']);
        // }

        // $deal = Deal::find($order['type_id']);

        $claim = Claimed::where('order_id', $id)->first();

        return view('frontend.claimed', compact('order', 'user', 'claim'));
    }

    public function giftclaimed($id)
    {
        $user = auth()->user();
        $order = Order::where('order_id', $id)->first();

        $gift = Gift::find($order['type_id']);

        // $deal = Deal::find($order['type_id']);

        $claimed = Claimed::where('order_id', $order['order_id'])->first();

        return view('frontend.gift_claimed', compact('order', 'gift', 'user', 'claimed'));
    }

    public function orders()
    {
        DB::enableQueryLog();
        $user = auth()->user();
        $orders = Order::where('user_id', $user['id'])->get();
        $deal_orders = Order::whereHas(
            'orderDetails',
            function ($query) {
                $query->where('order_type', 'deal');
            }
        )->where(['orders.user_id'=> $user['id']])->get();
        $event_orders = Order::whereHas(
            'orderDetails',
            function ($query) {
                $query->where('order_type', 'event');
            }
        )->where(['orders.user_id'=> $user['id']])->get();
        $gift_orders = Order::whereHas(
            'orderDetails',
            function ($query) {
                $query->where('order_type', 'gift');
            }
        )->where(['orders.user_id'=> $user['id']])->get();
        // $event_orders = Order::with('order_details')->where(['orders.user_id'=> $user['id'],'order_details.order_type'=> 'event'])->get();
        // $gift_orders = Order::with('order_details')->where(['orders.user_id'=> $user['id'],'order_details.order_type'=> 'gift'])->get();

        return view('frontend.order', compact('orders', 'deal_orders', 'gift_orders', 'event_orders'));
    }

    public function referafriend()
    {
        $user = auth()->user();

        $referral_route = route('home', ['ref' => $user->referral_token]);

        return view('frontend.referafriend', compact('user', 'referral_route'));
    }

    public function generateRandomString($length = 10)
    {
        $characters = '0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZ';
        $charactersLength = strlen($characters);
        $randomString = '';
        for ($i = 0; $i < $length; ++$i) {
            $randomString .= $characters[rand(0, $charactersLength - 1)];
        }

        return $randomString;
    }

    public function ajaximageupload(Request $request)
    {
        $validation = Validator::make($request->all(), [
            'file' => 'required|image|mimes:jpeg,png,jpg,gif',
        ]);
        $user = auth()->user();
        if ($validation->passes()) {
            $file = $request->file('file');
            $fileName = 'profile_pic_'.time().'.'.$file->getClientOriginalExtension();
            $path = $file->storeAs('profiles', $fileName, 'public');

            User::where('id', $user['id'])
                ->limit(1)
                ->update(['profile_image' => $path])
            ;

            return response()->json([
                'message' => 'Image Upload Successfully',
                'uploaded_image' => '<img src="/storage/'.$path.'" class="img-thumbnail" width="300" />',
                'class_name' => 'alert-success',
            ]);
        }

        return response()->json([
            'message' => $validation->errors()->all(),
            'uploaded_image' => '',
            'class_name' => 'alert-danger',
        ]);

        die;
    }

    public function updateprofile(Request $request)
    {
        // var_dump($request);die;
        $user = auth()->user();
        $id = $user['id'];

        $validation = $this->validate($request, [
            'first_name' => 'required',
            'last_name' => 'required',
            'email' => 'required|email|unique:users,email,'.$id,
            // 'mobile' => 'required|unique:users,mobile,'.$id,
        ]);

        if ($validation) {
            $user_details = User::find($user['id']);

            $user_details->first_name = $request['first_name'];
            $user_details->last_name = $request['last_name'];
            $user_details->email = $request['email'];
            // $user_details->mobile = $request['mobile'];
            $user_details->dob = $request['dob'];
            $user_details->gender = $request['gender'];

            $user_details->save();

            return response()->json([
                'status' => 'success',
                'message' => 'profile Updated Successful',
            ]);
        }

        return response()->json([
            'status' => 'Success',
            'message' => $validation->errors()->all(),
            'class_name' => 'alert-danger',
        ]);

        die;
    }

    public function update_password(Request $request)
    {
        if (!(Hash::check($request->get('current_password'), Auth::user()->password))) {
            // The passwords matches
            return response()->json([
                'status' => 'fail',
                'message' => 'Your current password does not matches with the password you provided. Please try again.',
                'class_name' => 'alert-danger',
            ]);
            // return redirect()->back()->with("error","Your current password does not matches with the password you provided. Please try again.");
        }

        if (0 == strcmp($request->get('current_password'), $request->get('new-password'))) {
            //Current password and new password are same
            return response()->json([
                'status' => 'fail',
                'message' => 'New Password cannot be same as your current password. Please choose a different password.',
                'class_name' => 'alert-danger',
            ]);
            // return redirect()->back()->with("error","New Password cannot be same as your current password. Please choose a different password.");
        }

        $validation = $this->validate($request, [
            'current_password' => 'required',
            'new-password' => 'required|string|min:8|confirmed',
        ]);
        if ($validation) {
            $user = Auth::user();
            $user->password = bcrypt($request->get('new-password'));
            $user->save();

            // return redirect()->back()->with("success","Password changed successfully !");
            return response()->json([
                'status' => 'success',
                'message' => 'Password changed successfully !',
                'class_name' => 'alert-success',
            ]);
        }

        return response()->json([
            'status' => 'fail',
            'message' => $validation->errors()->all(),
            'class_name' => 'alert-danger',
        ]);

        die;
    }

    public function contact_post(Request $request)
    {
        $request->validate([
            'name' => 'required',
            'email' => 'required',
            'subject' => 'required',
            'message' => 'required',
        ]);

        $input = $request->all();

        $contact = new Contactus();
        $contact->name = $request['name'];
        $contact->email = $request['email'];
        $contact->subject = $request['subject'];
        $contact->message = $request['message'];

        $contact->save();

        $settings = session()->get('settings');

        foreach ($settings as $key => $value) {
            if ('contact_email' == $value['name']) {
                $from_email = $value['value'];
            }
        }

        Mail::send('email.contact', ['input' => $input], function ($message) use ($from_email) {
            $message->to($from_email)->from(Input::get('email'))->subject('Contact us');
        });

        Mail::send('email.contactus_cust', ['input' => $input], function ($message) use ($from_email) {
            $message->to(Input::get('email'))->from($from_email)->subject('Contact us');
        });

        //   flash('Mail has been sent.')->success()->important();
        //   return back()->with('updated', 'Mail has been sent');
        return back()->with('alert', 'Submitted Sucessfully')->with('class', 'success');
    }

    public function addyourbusiness_post(Request $request)
    {
        $request->validate([
            'name' => 'required',
            'email' => 'required',
            'business_name' => 'required',
            'address' => 'required',
            'mobile' => 'required',
            'city' => 'required',
            'category' => 'required',
        ]);

        $add_business = new AddYourBusiness();

        $add_business->name = $request['name'];
        // $add_business->email = $request['email'];
        $add_business->business_name = $request['business_name'];
        $add_business->address = $request['address'];
        $add_business->mobile = $request['mobile'];
        $add_business->city = $request['city'];
        $add_business->category = $request['category'];

        // var_dump($add_business);die;

        $add_business->save();

        $input = $request->all();

        $settings = session()->get('settings');

        foreach ($settings as $key => $value) {
            if ('contact_email' == $value['name']) {
                $from_email = $value['value'];
            }
        }

        Mail::send('email.addyourbusiness', ['input' => $input], function ($message) use ($from_email) {
            $message->to($from_email)->from(Input::get('email'))->subject('Add Your Business');
        });

        Mail::send('email.addyourbusiness_cust', ['input' => $input], function ($message) use ($from_email) {
            $message->to(Input::get('email'))->from($from_email)->subject('Add Your Business');
        });

        //   flash('Mail has been sent.')->success()->important();
        //   return back()->with('updated', 'Mail has been sent');
        return back()->with('alert', 'Submitted Sucessfully')->with('class', 'success');
    }

    public function eventdetails($slug)
    {
        //$event = Event::where(['slug'=>$slug])->first();
        $event = Event::findOrFail($slug);
        return view('frontend.single_event', compact('event'));
    }

    public function search(Request $request)
    {
        $shops = Shop::select('shops.*', 'shops.id as shopid')
            ->where('shop_name', 'LIKE', "%{$request->input('query')}%")
            // ->where("city", $city)
            ->get()
            // ->toArray()
        ;

        $deals = Deal::select('*')
            ->where('name', 'LIKE', "%{$request->input('query')}%")
            // ->where("shops.city", $city)
            ->get()
        ;

        return view('frontend.search', compact('shops', 'deals'));
        // return view('frontend.search', compact('shops', 'deals', 'offers'));
    }

    public function wingpoints()
    {
        $user = auth()->user();
        $total_balance = $user->balance;
        $transactions = $user->transactions;

        return view('frontend.wingcoins', compact('total_balance', 'transactions'));
    }

    public function getPage($page)
    {
        $page = Page::where('page_name', $page)->first();

        return view('frontend.page', compact('page'));
    }


    public function getNotifications()
    {
        $user = auth()->user();
        $notifications = Notification::where(['user_id'=>$user['id'],'is_read'=>0])->get();
        return view('frontend.notifications', compact('notifications'));
    }


    public function markreadnotification(Request $request)
    {
        $user = auth()->user();

        if (!empty($request['all']) && $request[all] !=1) {
            $id = $request['id'];
            $notification = Notification::find($id);
            $notification->is_read = 1;
            $notification->save();
        } else {
            Notification::where('user_id', $user['id'])->update(['is_read' => 1]);
        }

        return "Success";
    }
}
