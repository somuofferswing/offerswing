<?php $__env->startSection('content'); ?>

<style>
    .imgrow2 {

        width: 437;

        height: 234px
    }

    .imgrow3 {

        width: 400;

        height: 240px
    }
</style>

<div id="homepage-2">

    <div class="container">



        <!-- main slider -->

        <div class="row mb-5">

            <div class="col-12 col-md-9 flex-grow-1">

                <?php $__env->startComponent('components.carousel',['slider'=> $slider_1]); ?>

                <?php $__env->slot('coursel_id'); ?>

                coursel

                <?php $__env->endSlot(); ?>

                <?php echo $__env->renderComponent(); ?>

            </div>

            <div class="col-md-3 flex-grow-1">

                <?php $__env->startComponent('components.carousel',['slider'=> $slider_2]); ?>

                <?php $__env->slot('coursel_id'); ?>

                coursel1

                <?php $__env->endSlot(); ?>

                <?php echo $__env->renderComponent(); ?>

            </div>

        </div>



        <!-- deals block -->

        <div class="ps-site-features bg-white mb-5">

            <div class="ps-block--site-features ps-block--site-features-2">

                <div class="ps-block__item justify-content-center">

                    <a href="<?php echo e(url('/wingdeals')); ?>">

                        <img src="<?php echo e(asset('main/images/icons/deals.png')); ?>" alt="" class="img-fluid"
                            style="height: 60px;">

                    </a>

                </div>

                <div class="ps-block__item justify-content-center">

                    <a href="<?php echo e(url('/event')); ?>">

                        <img src="<?php echo e(asset('main/images/icons/events.png')); ?>" alt="" class="img-fluid"
                            style="height: 50px;">

                    </a>

                </div>

                <div class="ps-block__item justify-content-center">

                    <a href="<?php echo e(url('/newsfeeds')); ?>">

                        <img src="<?php echo e(asset('main/images/icons/news.png')); ?>" alt="" class="img-fluid"
                            style="height: 50px;">

                    </a>

                </div>

                <div class="ps-block__item justify-content-center">

                    <div class="ps-block__left m-0">

                        <i class="icon-gift"></i>

                        <!-- <img src="<?php echo e(asset('main/images/icons/wing.png')); ?>" alt="" class="img-fluid"

                            style="height: 50px;"> -->

                    </div>

                    <div class="ps-block__right">

                        <a href="<?php echo e(url('/redeem')); ?>">

                            <strong style="font-size:18px;font-weight:500">Redeem Gifts</strong>

                        </a>

                    </div>

                </div>



                <div class="ps-block__item">

                    <div class="ps-block__left m-0"><i class="fa fa-download"></i></div>

                    <div class="ps-block__right">

                        <a href="https://play.google.com/store?hl=en" target="_blank">

                            <strong style="font-size:18px;font-weight:500">Download APP</strong>

                        </a>

                    </div>

                </div>

            </div>

        </div>

    </div>



    <div class="ps-home-banner">

        <div class="container syd-container">

            <div class="ps-section__left">

                <?php echo $__env->make('layouts.mainSidebar', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>

            </div>

            <div class="ps-section__right">



                <div class="row">

                    <?php $__currentLoopData = $imagediv2_1; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $item): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>

                    <div class="col-12 col-md-6">

                        <a class="ps-collection" href="#">

                            <img class="imgrow2" src="<?php echo e(asset("$item->image")); ?>" alt="">

                        </a>

                    </div>

                    <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>

                </div>



                <div class="row mt-5">

                    <?php $__currentLoopData = $imagediv3_1; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $item): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>

                    <div class="col-12 col-md-4">

                        <a class="ps-collection" href="#">

                            <img class="imgrow3" src="<?php echo e(asset("$item->image")); ?>" alt="">

                        </a>

                    </div>

                    <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>

                </div>



                <div class="row mt-5">

                    <?php $__currentLoopData = $imagediv2_2; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $item): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>

                    <div class="col-12 col-md-6">

                        <a class="ps-collection" href="#">

                            <img class="imgrow2" src="<?php echo e(asset("$item->image")); ?>" alt="">

                        </a>

                    </div>

                    <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>

                </div>



                <div class="row mt-5">

                    <?php $__currentLoopData = $imagediv3_2; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $item): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>

                    <div class="col-12 col-md-4">

                        <a class="ps-collection" href="#">

                            <img class="imgrow3" src="<?php echo e(asset("$item->image")); ?>" alt="">

                        </a>

                    </div>

                    <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>

                </div>



                <div class="row mt-5">

                    <?php $__currentLoopData = $imagediv2_3; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $item): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>

                    <div class="col-12 col-md-6">

                        <a class="ps-collection" href="#">

                            <img class="imgrow2" src="<?php echo e(asset("$item->image")); ?>" alt="">

                        </a>

                    </div>

                    <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>

                </div>



                <div class="row mt-5">

                    <?php $__currentLoopData = $imagediv3_3; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $item): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>

                    <div class="col-12 col-md-4">

                        <a class="ps-collection" href="#">

                            <img class="imgrow3" src="<?php echo e(asset("$item->image")); ?>" alt="">

                        </a>

                    </div>

                    <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>

                </div>



                <div class="row mt-5">

                    <?php $__currentLoopData = $imagediv2_4; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $item): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>

                    <div class="col-12 col-md-6">

                        <a class="ps-collection" href="#">

                            <img class="imgrow2" src="<?php echo e(asset("$item->image")); ?>" alt="">

                        </a>

                    </div>

                    <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>

                </div>



                <!-- <div class="row mt-5">

                <?php $__currentLoopData = $imagediv2_5; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $item): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>

                <div class="col-12 col-md-6">

                    <a class="ps-collection" href="#">

                        <img class="imgrow2" src="<?php echo e(asset("$item->image")); ?>" alt="">

                    </a>

                </div>

                <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>

            </div>



            <div class="row mt-5">

                <?php $__currentLoopData = $imagediv3_3; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $item): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>

                <div class="col-12 col-md-4">

                    <a class="ps-collection" href="#">

                        <img class="imgrow3" src="<?php echo e(asset("$item->image")); ?>" alt="">

                    </a>

                </div>

                <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>

            </div> -->



            </div>

        </div>

    </div>

</div>

<?php $__env->stopSection(); ?>

<?php echo $__env->make('layouts.app', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH C:\wamp64\www\offerswing\resources\views/frontend/welcome.blade.php ENDPATH**/ ?>