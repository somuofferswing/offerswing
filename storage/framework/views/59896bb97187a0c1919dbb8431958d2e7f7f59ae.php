<?php $__env->startSection('content'); ?>
<link rel="stylesheet"
    href="<?php echo e(asset('adminlte/bower_components/bootstrap-datepicker/dist/css/bootstrap-datepicker.min.css')); ?>">
<div class="content-wrapper">
    <section class="content-header">
        <h1>
            Deals
            <small>Add Deal</small>
        </h1>
        <!-- <ol class="breadcrumb">
              <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
              <li class="active">Dashboard</li>
            </ol> -->
    </section>

    <section class="content">

        <div class="box box-info">
            <div class="box-header with-border">
                <h3 class="box-title">Add Deal</h3>
                <a href="<?php echo url('/deal/create');; ?>" class="pull-right btn btn-info">Add Deal</a>
            </div>
            <form class="form-horizontal" method="POST" action="<?php echo e(route('store-deal')); ?>"
                enctype="multipart/form-data">
                <?php echo csrf_field(); ?>
                <input type="hidden" value="<?php echo e($user_id); ?>" name="created_by">
                <div class="box-body">
                    <div class="form-group ">
                        <label for="name" class="col-sm-2 control-label">Name</label>
                        <div class="col-sm-10">
                            <input id="name" type="text" class="form-control" name="name" value="<?php echo e(old('name')); ?>"
                                required autocomplete="name" autofocus>
                            <?php if($errors->has('name')): ?>
                            <div class="danger"><?php echo e($errors->first('name')); ?></div>
                            <?php endif; ?>
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="pic" class="col-sm-2 control-label">Image</label>
                        <div class="col-sm-10">
                            <input type="file" class="form-control-file" id="pic" name="pic" onchange="loadFile(event)">
                            <img id="output" height="100px" width="100px" />
                        </div>
                    </div>
                    <div class="form-group ">
                        <label for="description" class="col-sm-2 control-label">Description</label>
                        <div class="col-sm-10">
                            <textarea id="description" type="text" class="form-control" name="description" required
                                autocomplete="description" autofocus><?php echo e(old('description')); ?></textarea>
                            <?php if($errors->has('description')): ?>
                            <div class="danger"><?php echo e($errors->first('description')); ?></div>
                            <?php endif; ?>
                        </div>
                    </div>
                    <div class="form-group ">
                        <label for="payment_type" class="col-sm-2 control-label">Payment Type</label>
                        <div class="col-sm-10">
                            <select class="form-control" id="payment_type" name="payment_type" required autofocus>
                                
                                <option value="cash">Cash</option>
                                <option value="wingpoints">Wing Points</option>
                                <option value="both">Both</option>
                            </select>
                            <?php if($errors->has('payment_type')): ?>
                            <div class="danger"><?php echo e($errors->first('payment_type')); ?></div>
                            <?php endif; ?>
                        </div>
                    </div>
                    <div class="form-group ">
                        <label for="price" class="col-sm-2 control-label">Deal Price</label>
                        <div class="col-sm-10">
                            <input id="price" type="text" class="form-control" name="price" value="<?php echo e(old('price')); ?>"
                                required autofocus>
                            <?php if($errors->has('price')): ?>
                            <div class="danger"><?php echo e($errors->first('price')); ?></div>
                            <?php endif; ?>
                        </div>
                    </div>
                    <div class="form-group ">
                        <label for="shop_id" class="col-sm-2 control-label">Shop</label>
                        <div class="col-sm-10">

                            <select class="form-control" id="shop_id" name="shop_id" value="<?php echo e(old('shop_id')); ?>"
                                required autocomplete="shop_id" autofocus>
                                <option>--Select--</option>
                                <?php $__currentLoopData = $shops; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $shop): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                <option value="<?php echo e($shop->id); ?>"><?php echo e($shop->shop_name); ?></option>
                                <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                            </select>
                            <?php if($errors->has('shop_id')): ?>
                            <div class="danger"><?php echo e($errors->first('shop_id')); ?></div>
                            <?php endif; ?>
                        </div>
                    </div>
                    
        
<div class="form-group ">
    <label for="times_applicable" class="col-sm-2 control-label">Days Applicable</label>
    <div class="col-sm-10">
        <select name="days_applicable[]" id="days_applicable" class="form-control select2"
            data-placeholder="Select days applicable" multiple="multiple">
            <?php $__currentLoopData = $days; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $item): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
            <option value="<?php echo e($item->id); ?>"><?php echo e($item->name); ?></option>
            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
        </select>
        <?php if($errors->has('days_applicable')): ?>
        <div class="danger"><?php echo e($errors->first('days_applicable')); ?></div>
        <?php endif; ?>
    </div>
</div>
<div class="form-group ">
    <label for="valid_from" class="col-sm-2 control-label">From Date</label>
    <div class="col-sm-10">
        <input type="text" class="form-control pull-right" id="valid_from" name="valid_from"
            value="<?php echo e(old('valid_from')); ?>" required autocomplete="valid_from" autofocus>
        
        
        
        <?php if($errors->has('valid_from')): ?>
        <div class="danger"><?php echo e($errors->first('valid_from')); ?></div>
        <?php endif; ?>
    </div>
</div>
<div class="form-group ">
    <label for="valid_upto" class="col-sm-2 control-label">To Date</label>
    <div class="col-sm-10">
        <input type="text" class="form-control pull-right" id="valid_upto" name="valid_upto"
            value="<?php echo e(old('valid_upto')); ?>" required autocomplete="valid_upto" autofocus>
        
        
        
        <?php if($errors->has('valid_upto')): ?>
        <div class="danger"><?php echo e($errors->first('valid_upto')); ?></div>
        <?php endif; ?>
    </div>
</div>
<div class="form-group ">
    <label for="max_discount" class="col-sm-2 control-label">Max Discount</label>
    <div class="col-sm-10">
        <input id="max_discount" type="text" class="form-control" name="max_discount" value="<?php echo e(old('max_discount')); ?>"
            required autocomplete="max_discount" autofocus>
        <?php if($errors->has('max_discount')): ?>
        <div class="danger"><?php echo e($errors->first('max_discount')); ?></div>
        <?php endif; ?>
    </div>
</div>

<div class="form-group ">
    <label for="discount_percentage" class="col-sm-2 control-label">Discount (%)</label>
    <div class="col-sm-10">
        <input id="discount_percentage" type="text" class="form-control" name="discount_percentage"
            value="<?php echo e(old('discount_percentage')); ?>" required autocomplete="discount_percentage" autofocus>
        <?php if($errors->has('discount_percentage')): ?>
        <div class="danger"><?php echo e($errors->first('discount_percentage')); ?></div>
        <?php endif; ?>
    </div>
</div>
<div class="form-group ">
    <label for="quantity" class="col-sm-2 control-label">Quantity</label>
    <div class="col-sm-10">
        <input id="quantity" type="text" class="form-control" name="quantity" value="<?php echo e(old('quantity')); ?>" autofocus>
        <?php if($errors->has('quantity')): ?>
        <div class="danger"><?php echo e($errors->first('quantity')); ?></div>
        <?php endif; ?>
    </div>
</div>
<div class="form-group ">
    <label for="max_count_for_user" class="col-sm-2 control-label">Max per User</label>
    <div class="col-sm-10">
        <input id="max_count_for_user" type="text" class="form-control" name="max_count_for_user"
            value="<?php echo e(old('max_count_for_user')); ?>" required autocomplete="max_count_for_user" autofocus>
        <?php if($errors->has('max_count_for_user')): ?>
        <div class="danger"><?php echo e($errors->first('max_count_for_user')); ?></div>
        <?php endif; ?>
    </div>
</div>
<div class="form-group ">
    <label for="points_rewarded" class="col-sm-2 control-label">Wing points receive </label>
    <div class="col-sm-10">
        <input id="points_rewarded" type="text" class="form-control" name="points_rewarded"
            value="<?php echo e(old('points_rewarded')); ?>" autofocus>
        <?php if($errors->has('points_rewarded')): ?>
        <div class="danger"><?php echo e($errors->first('points_rewarded')); ?></div>
        <?php endif; ?>
    </div>
</div>

<div class="form-group ">
    <label for="max_wing_points_assured" class="col-sm-2 control-label">Top Deal</label>
    <div class="col-sm-10">
        <input type="checkbox" class="" name="top" value="1">
        <?php if($errors->has('top')): ?>
        <div class="danger"><?php echo e($errors->first('top')); ?></div>
        <?php endif; ?>
    </div>
</div>

<div class="form-group ">
        <label for="max_wing_points_assured" class="col-sm-2 control-label">Active</label>
        <div class="col-sm-10">
            <input type="checkbox" class="" name="active" value="1">
            <?php if($errors->has('active')): ?>
            <div class="danger"><?php echo e($errors->first('active')); ?></div>
            <?php endif; ?>
        </div>
    </div>

<div class="form-group ">
    <label for="terms_conditions" class="col-sm-2 control-label">Terms & Conditions</label>
    <div class="col-sm-10">
        <textarea id="terms_conditions" type="text" class="form-control" name="terms_conditions" rows="10"
            cols="80"><?php echo e(old('terms_conditions')); ?></textarea>
        <?php if($errors->has('terms_conditions')): ?>
        <div class="danger"><?php echo e($errors->first('terms_conditions')); ?></div>
        <?php endif; ?>
    </div>
</div>
<button type="button" class="btn btn-info pull-right"><?php echo e(__('Cancel')); ?></button>
<button type="submit" class="btn btn-info pull-right"><?php echo e(__('Add Deal')); ?></button>
</div>
</form>
</div>
</section>
</div>
<?php $__env->stopSection(); ?>
<?php $__env->startSection('scripts'); ?>
<script src="<?php echo e(asset('adminlte/bower_components/bootstrap-datepicker/dist/js/bootstrap-datepicker.min.js')); ?>">
</script>
<script src="<?php echo e(asset('adminlte/bower_components/select2/dist/js/select2.full.min.js')); ?>"></script>
<script src="<?php echo e(asset('adminlte/bower_components/ckeditor/ckeditor.js')); ?>"></script>
<script>
    $(function () {
    CKEDITOR.replace('terms_conditions')
    CKEDITOR.replace('description')
  })
        $('.select2').select2()
        $(document).ready(function() {
            $('#valid_from').datepicker({
                autoclose: true,
                format: 'yyyy-mm-dd',
                startDate: new Date(),
            });

            $('#valid_upto').datepicker({
                autoclose: true,
                format: 'yyyy-mm-dd',
                startDate: new Date(),
            }).change(function () {
                var from = $('#valid_from').val();
                var to = $('#valid_upto').val();
                if(to < from){
                    $('#valid_upto').val("");
                    alert('to date should be grater than from date');
                }
            });
        })

</script>
<script>
    var loadFile = function(event) {
            var output = document.getElementById('output');
            output.src = URL.createObjectURL(event.target.files[0]);
          };
</script>
<?php $__env->stopSection(); ?>

<?php echo $__env->make('layouts.admin', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH C:\wamp64\www\offers\resources\views/deals/create.blade.php ENDPATH**/ ?>