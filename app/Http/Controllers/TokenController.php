<?php

namespace App\Http\Controllers;

use App\Models\Token;
use App\Models\WingPoints;
use App\Models\User;
use Illuminate\Http\Request;

class TokenController extends Controller
{
    public function redeemToken(Request $request)
    {
        $user = auth()->user();

        $this->validate($request, [
            'token_id' => 'required',
        ]);

        $tid = $request['token_id'];

        $token = Token::where('token_id', $tid)->first();

        $user_id = $user['id'];

        if (!empty($token)) {
            $token_status = $token->claim_status;

            if (0 == $token_status) {
                $user->deposit($token->points_rewarded, 'deposit', ['type' => 'Token Redeemed', 'description' => 'Rewarded on Redmestion of Token'], 'Token');

                $token->claim_status = 1;
                $token->save();

                return back()->with('alert', $token->points_rewarded. 'Wing Points added Successfully')->with('class', 'success');
            }

            return back()->with('alert', 'Token Claimed Already')->with('class', 'danger');
        }

        return back()->with('alert', 'Please check the Token Code')->with('class', 'danger');
    }
}
