<?php

namespace App\Models;

use Gr8Shivam\SmsApi\SmsApi;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Carbon;

class Otp extends Model
{
    const EXPIRATION_TIME = 15; // minutes

    protected $fillable = [
        'code',
        'user_id',
        'used',
    ];

    public function __construct(array $attributes = [])
    {
        if (!isset($attributes['code'])) {
            $attributes['code'] = $this->generateCode();
        }

        parent::__construct($attributes);
    }

    /**
     * Generate a six digits code.
     *
     * @param int $codeLength
     *
     * @return string
     */
    public function generateCode($codeLength = 4)
    {
        $min = pow(10, $codeLength);
        $max = $min * 10 - 1;

        return mt_rand($min, $max);
    }

    /**
     * User tokens relation.
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function user()
    {
        return $this->belongsTo(User::class);
    }

    /**
     * True if the token is not used nor expired.
     *
     * @return bool
     */
    public function isValid()
    {
        return !$this->isUsed() && !$this->isExpired();
    }

    /**
     * Is the current token used.
     *
     * @return bool
     */
    public function isUsed()
    {
        return $this->used;
    }

    /**
     * Is the current token expired.
     *
     * @return bool
     */
    public function isExpired()
    {
        return $this->created_at->diffInMinutes(Carbon::now()) > static::EXPIRATION_TIME;
    }

    public function sendCode()
    {
        if (!$this->user) {
            throw new \Exception('No user attached to this token.');
        }

        if (!$this->code) {
            $this->code = $this->generateCode();
        }

        try {
            $mobile = $this->user->getPhoneNumber();
            $m_otp = (string) $this->code;

            $smsapi = new SmsApi();
            $smsapi->sendMessage($mobile, $m_otp)->getResponseCode();
        } catch (\Exception $ex) {
            return false; //enable to send SMS
        }

        return true;
    }
}
