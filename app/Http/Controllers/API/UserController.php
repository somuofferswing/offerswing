<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use App\Models\Otp;
use App\Models\Setting;
use App\Models\User;
use App\Models\WingPoints;
use Gr8Shivam\SmsApi\SmsApi;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Validator;

class UserController extends Controller
{
    public $successStatus = 200;

    public function login()
    {
        if (Auth::attempt(['email' => request('email'), 'password' => request('password')])) {
            $user = Auth::user();
            $success['token'] = $user->createToken('MyApp')->accessToken;

            // $otp = new Otp();
            // $mobile_otp = $otp->generateCode();
            // $m_otp = (string) $mobile_otp;
            // $mobile = (string) $user->mobile;

            // $smsapi = new SmsApi();
            // $result = $smsapi->sendMessage($mobile, $m_otp)->getResponseCode();

            return response()->json(['success' => $success], $this->successStatus);
        }

        return response()->json(['error' => 'Unauthorised'], 401);
    }

    public function register(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'first_name' => 'required|string|max:255',
            'last_name' => 'required|string|max:255',
            'email' => 'required|string|email|max:255|unique:users',
            'password' => 'required|string|min:8',
            'c_password' => 'required|same:password',
            'mobile' => 'required|numeric|digits:10|unique:users',
            'user_type' => 'required',
        ]);

        if ($validator->fails()) {
            return response()->json(['error' => $validator->errors()], 401);
        }

        $email = $request->email;
        $password = $request->password;
        $user_type = $request->user_type;
        $first_name = $request->first_name;
        $last_name = $request->last_name;
        $mobile = $request->mobile;
        $dob = $request->dob;
        $gender = $request->gender;
        $referal_code = $request->referal_code;

        $hash_password = Hash::make($password);

        $users = new User();
        $users->email = $email;
        $users->password = $hash_password;
        $users->first_name = $first_name;
        $users->last_name = $last_name;
        $users->user_type = $user_type;
        $users->mobile = $mobile;
        $users->dob = $dob;
        $users->gender = $gender;

        if (2 == $user_type) {
            $users->refid = $this->generateReferralcode($first_name);
        }

        $users->save();

        $welcome_points = Setting::where('name', 'welcome_points')->where('status', '1')->first()->toArray();
        $points = $welcome_points['value'];

        $wingpoints = new WingPoints();
        $wingpoints->user_id = $users->id;
        $wingpoints->type = 'WELCOME';
        $wingpoints->points = $points;

        $wingpoints->save();

        if (!empty($referal_code)) {
            $res = $this->rewardpointsforreferral($referal_code, $users->id);
        }

        $users->sendEmailVerificationNotification();

        $success['token'] = $users->createToken('MyApp')->accessToken;
        $success['name'] = $users->first_name.' '.$users->last_name;

        return response()->json(['result' => $success], $this->successStatus);
    }

    public function logout(Request $request)
    {
        $accessToken = Auth::user()->token();
        DB::table('oauth_refresh_tokens')
            ->where('access_token_id', $accessToken->id)
            ->update([
                'revoked' => true,
            ])
        ;

        $accessToken->revoke();

        return response()->json(null, 204);
    }

    public function generateRandomString($length = 10)
    {
        // $characters = '0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZ';
        $characters = '0123456789';
        $charactersLength = strlen($characters);
        $randomString = '';
        for ($i = 0; $i < $length; ++$i) {
            $randomString .= $characters[rand(0, $charactersLength - 1)];
        }

        return $randomString;
    }

    public function generateReferralcode($name)
    {
        $refid = strtoupper(substr($name, 0, 4).$this->generateRandomString(4));
        $reff = User::where('refid', $refid)->get()->toArray();
        // var_dump($reff);die;
        if (!empty($reff)) {
            var_dump('Hai');
            die;
            $this->generateReferralcode($name);
        } else {
            return $refid;
        }
    }

    public function rewardpointsforreferral($referral, $user_id)
    {
        $reff = User::where('refid', $referral)->first()->toArray();

        $referral_points = Setting::where('name', 'referral_points')->where('status', '1')->first()->toArray();
        $points = $referral_points['value'];

        $wingpoints = new WingPoints();
        $wingpoints->user_id = $reff['id'];
        $wingpoints->type = 'REFERRAL';
        $wingpoints->points = $points;
        $wingpoints->referral_used_user_id = $user_id;

        $wingpoints->save();

        return 'success';
    }
}
