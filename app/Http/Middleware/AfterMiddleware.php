<?php

namespace App\Http\Middleware;

use App\Models\Category;
use App\Models\City;
use App\Models\Notification;
use App\Models\Order;
use App\Models\Setting;
use App\Models\WingPoints;
use Auth;
use Closure;
use Illuminate\Support\Facades\DB;
use Session;

class AfterMiddleware
{
    /**
     * Handle an incoming request.
     *
     * @param \Illuminate\Http\Request $request
     *
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $categories = Category::where('parent_id', null)->get();
        $cities = City::all();

        $settings = Setting::all();

        view()->share('categories', $categories);
        view()->share('cities', $cities);
        view()->share('settings', $settings);

        Session::put('city', $cities[0]['city']);
        Session::put('settings', $settings);

        if (Auth::user()) {
            $user = auth()->user();

            $notifications = Notification::where('user_id', $user['id'])->where('is_read', 0)->get();          
            view()->share('notifications', $notifications);
        }

        return $next($request);
    }
}
