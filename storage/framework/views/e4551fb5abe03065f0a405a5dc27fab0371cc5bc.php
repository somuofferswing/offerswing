<?php $__env->startSection('content'); ?>

<div class="container">

    <div class="row">
        <h3>Favourite Shops</h3>
        <div class="col-md-6">



            <?php $__currentLoopData = $shops; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $key=>$item): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
            <div class="row" style="margin-top:20px;">

                <div class="panel panel-danger">
                    <!-- <div class="panel-heading">
                            <h3 class="panel-title">Panel title</h3>
                      </div> -->
                    <div class="panel-body">

                        <div class="name" style="font-size:22px;color: #df685d;">
                            <?php echo e($item->shop_name); ?>


                            <span style="padding-left:20px;">
                                <?php if(Auth::guest()): ?>
                                <a class="btn" data-id="<?php echo e($item->shopid); ?>" data-toggle="modal"
                                    data-target="#myModal1"><i class="far fa-heart" style="color:#e31b23"></i></a>
                                <?php else: ?>
                                <?php if(in_array($item->id,$favouriteshops_ids)): ?>
                                <a class="btn unlike" data-id="<?php echo e($item->id); ?>"><i class="fas fa-heart"
                                        style="color:#e31b23"></i></a>
                                <?php else: ?>
                                <a class="btn like" data-id="<?php echo e($item->id); ?>"> <i class="far fa-heart"
                                        style="color:#e31b23"></i></a>
                                <?php endif; ?>

                                <?php endif; ?>
                            </span>
                        </div>

                        
                        <ul class="details">
                            <li><?php echo e($item->address); ?></li>
                            <li><?php echo e($item->city); ?></li>
                            <li><?php echo e($item->state); ?></li>
                            <li><b>Rating </b> :
                                <?php for($i = 0; $i < 5; $i++): ?> <?php if($i< (int)$item->shop_rating): ?>
                                    <span class="fas fa-star" style="color:#df685d"></span>
                                    <?php else: ?>
                                    <span class="far fa-star" style="color:#df685d"></span>
                                    <?php endif; ?>
                                    <?php endfor; ?>
                            </li>
                        </ul>

                        <a href="<?php echo url('/viewshop',[$item->id]);; ?>" class="btn btn-primary btn-lg  buy-now">
                            Details <span class="glyphicon glyphicon-triangle-right"></span>
                        </a>
                        <div class="pull-right">
                            <div class="card-img" style="width:200px !important;">
                                <a href="<?php echo url('/viewshop',[$item->id]);; ?>">
                                    <img src="<?php echo asset("$item->image_path"); ?>" class="img-responsive">

                                </a>

                            </div>
                        </div>
                        <!-- <ul class="socialIcons">
                            <li class="facebook"><a href="#"><i class="fab fa-fw fa-facebook"></i>Facebook</a></li>
                            <li class="twitter"><a href="#"><i class="fab fa-fw fa-twitter"></i>Twitter</a></li>
                            <li class="instagram"><a href="#"><i class="fab fa-fw fa-instagram"></i>Instagram</a></li>
                        </ul> -->
                        <!-- <h5
                            style="position:absolute;top:-12px;left:9px;transform:rotate(-30deg);text-align:center;    text-shadow: 4px 4px 8px #df685d;border-bottom:1px dashed #df685d;font-weight:800;">
                            Offers<br>Wing Offers</h5> -->
                    </div>
                    <div class="panel-footer">
                        <?php if(in_array($item->id,$offers_shops)): ?>
                        <span><b>Offers</b></span>
                        <?php endif; ?>

                        <?php if(in_array($item->id,$deals_shops)): ?>
                        <span><b>WingDeals</b> </span>
                        <?php endif; ?>

                    </div>
                </div>



            </div>

            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>

        </div>

    </div>

</div>

<?php $__env->stopSection(); ?>

<?php $__env->startSection('scripts'); ?>

<script>
    $(document).on('click', ".unlike", function () {
        var id = $(this).data('id');
        var element = this;

        var data = {
            shop_id: id
        };
        $.ajax({
            type: "post",
            url: '<?php echo e(url("removefavouriteshop")); ?>',
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            },
            data: data,
            cache: false,
            success: function (data) {
                if (data.status == "success") {
                    $(element).removeClass('unlike').addClass('like').html('<i class="far fa-heart" style="color:#e31b23"></i>');
                }
            },
            error: function (data) {
                alert('Fail to run unlike..');
            }
        });


    });
    $(document).on('click', ".like", function () {
        var id = $(this).data('id');
        var element = this;

        var data = {
            shop_id: id
        };
        $.ajax({
            type: "post",
            url: '<?php echo e(url("addfavouriteshop")); ?>',
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            },
            data: data,
            cache: false,
            success: function (data) {
                console.log(data);
                if (data.status == "success") {
                    $(element).removeClass('like').addClass('unlike').html('<i class="fas fa-heart" style="color:#e31b23"></i>');

                }
            },
            error: function (data) {
                alert('Fail to run like..');
            }
        });


    });
</script>

<?php $__env->stopSection(); ?>

<?php echo $__env->make('layouts.app', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH C:\wamp64\www\offers\resources\views/favouriteshops.blade.php ENDPATH**/ ?>