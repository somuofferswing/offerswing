@extends('layouts.admin')



@section('content')

<div class="content-wrapper">

    <section class="content-header">

        <h1>

            News Feed

            <small>All NewsFeed</small>

        </h1>

        <!-- <ol class="breadcrumb">

              <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>

              <li class="active">Dashboard</li>

            </ol> -->

    </section>

    <section class="content">

        @if(session()->get('success'))

        <div class="alert alert-success" id="success">

            {{ session()->get('success') }}

        </div><br />

        @endif



        <div class="box box-primary">

            <div class="box-header with-border">

                <h3 class="box-title">All NewsFeed</h3>

                <a href="{!! url('/news/create'); !!}" class="pull-right btn btn-info">Add Newsfeed</a>

            </div>



            <div class="table-responsive">

                <table class="table table-striped">

                    <thead>

                        <tr>

                            <th>ID</th>

                            <th>Title</th>

                            <th>Description</th>


                            <th colspan="2">Action</th>

                        </tr>

                    </thead>

                    <tbody>

                        @foreach($news as $newsfeed)

                        <tr>

                            <td>{{$newsfeed->id}}</td>

                            <td>{{$newsfeed->title}}</td>

                            <td>{!! $newsfeed->description !!}</td>

                            <td><a class="btn btn-default" href="{{ route('edit-news', [$newsfeed->id]) }}">Edit</a>
                            </td>



                            <td>

                                <form action="{{ route('delete-news', [$newsfeed->id])}}" method="post">

                                    @csrf

                                    @method('DELETE')

                                    <button class="btn btn-danger" type="submit">Delete</button>

                                </form>

                            </td>

                        </tr>

                        @endforeach

                    </tbody>

                </table>

            </div>

        </div>

    </section>

</div>

@endsection

@section('scripts')

<script>
    $('#success').delay(5000).fadeOut('slow');

    $('#reset').on('click',function(){

        // $("#filter")[0].reset();

        $('#shop_id').val('');

        $('#category_id').val('');

        $('#filter')[0].submit();

    });



</script>

@endsection
