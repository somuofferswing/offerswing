<?php $__env->startSection('content'); ?>
<style>
    #faq {
        background-color: #f7f7f7;
    }
</style>
<div class="container-fluid" id="faq">

    <nav aria-label="breadcrumb">
        <ol class="breadcrumb">
            <li class="breadcrumb-item"><a href="#">Home</a></li>
            <li class="breadcrumb-item active" aria-current="page">FAQ's</li>
        </ol>
    </nav>
    <div class="row pt-4">
        <div class="col-md-3">
            <img src="<?php echo e(asset('main/images/icons/faq.png')); ?>" height="auto" width="100%" class="mgin1"
                style=" border-radius: 10px">

        </div>
        <div class="col-md-6 ">
            <h3 class="text-center">Frequently Asked Questions(FAQs)</h3>

            <hr>

            <?php $__currentLoopData = $faqs; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $key=>$item): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
            <div>
                <h4>
                    <p><?php echo e($key+1); ?> . <?php echo e($item->question); ?></p>
                </h4>

                <p> Answer: <?php echo $item->answer; ?> </p>
            </div>
            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>

        </div>

        <div class="col-md-3">
            
        </div>
    </div>

</div>

<?php $__env->stopSection(); ?>

<?php echo $__env->make('layouts.app', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH C:\wamp64\www\offers\resources\views/faq.blade.php ENDPATH**/ ?>