@extends('layouts.admin')

@section('content')
<div class="content-wrapper">
    <section class="content-header">
        {{-- <h1>
                Wing Deals
                <small>View Wing Deal({{$deals->name }})</small>
        </h1> --}}
        <!-- <ol class="breadcrumb">
              <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
              <li class="active">Dashboard</li>
            </ol> -->
    </section>

    <section class="content">
        <div class="box">
            <div class="box-header with-border">
                <h3 class="box-title">Deal Info</h3>
            </div>
            <div class="box-body">
                <h2>{{ strtoupper($deal->name) }}</h2>
                <h4><u> Description</u></h4>
                <p>{!! $deal->description !!}</p>
                <h4> <u>Terms & Conditions</u> </h4>
                <p>{!! $deal->terms_conditions !!}</p>

                <h4> <u>Deal period</u> </h4>
                <p>{{ $deal->valid_from}} to {{ $deal->valid_upto }}</p>

                <h4> <u>Discount</u> </h4>
                <p>{{ $deal->discount_percentage }} percentage ( upto ₹.{{ $deal->max_discount }})</p>

            </div>
        </div>


        <div class="box">
            <div class="box-header with-border">
                <h3 class="box-title">All Claimed for this deal</h3>
            </div>
            <div class="box-body">
                <div class="table-responsive">
                    <table class="table table-striped">
                        <thead>
                            <tr>
                                <th>Claim Id</th>
                                <th>UserName</th>
                                <th>User Email</th>
                                <th>User mobile</th>
                                <th>Used at Shop</th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach ($claims as $item)
                            <tr>
                                <td>{{$item->unique_code}}</td>
                                <td>{{$item->first_name}}</td>
                                <td>{{$item->email}}</td>
                                <td>{{$item->mobile}}</td>
                                <td>
                                    @if ($item->used == 1)
                                        yes
                                    @else
                                        No
                                    @endif

                                </td>
                            </tr>

                            @endforeach

                        </tbody>


                    </table>
                </div>
            </div>
        </div>
    </section>
@endsection
