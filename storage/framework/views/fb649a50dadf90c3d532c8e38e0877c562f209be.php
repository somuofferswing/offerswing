

<?php $__env->startSection('content'); ?>
<style>
.mgin1 {
        background: white;
        -moz-box-shadow: 0 0 5px #888;
        -webkit-box-shadow: 0 0 5px#888;
        box-shadow: 0 0 5px #888;
    }
    .llogin{
        padding:20px;
    }
</style>
<div class="container">
    <div class="row justify-content-center mgin1">
        <div class="col-md-6 col-md-offset-3 " >
            <div class="card">
                <div class="card-header text-center llogin"><h4><?php echo e(__('Login')); ?></h4><hr></div>


                <div class="card-body">
                    <form method="POST" action="<?php echo e(route('login')); ?>" class="form-horizontal">
                        <?php echo csrf_field(); ?>

                        <div class="form-group">
                            <label class="control-label col-sm-3" for="email"><?php echo e(__('E-Mail Address')); ?></label>
                            <div class="col-sm-9">
                                <input id="email" type="email" class="form-control <?php if ($errors->has('email')) :
if (isset($message)) { $messageCache = $message; }
$message = $errors->first('email'); ?> is-invalid <?php unset($message);
if (isset($messageCache)) { $message = $messageCache; }
endif; ?>" name="email" value="<?php echo e(old('email')); ?>" required autocomplete="email" autofocus>

                                <?php if ($errors->has('email')) :
if (isset($message)) { $messageCache = $message; }
$message = $errors->first('email'); ?>
                                <span class="invalid-feedback" role="alert">
                                    <strong><?php echo e($message); ?></strong>
                                </span>
                            <?php unset($message);
if (isset($messageCache)) { $message = $messageCache; }
endif; ?>
                            </div>
                          </div>




                        

                        <div class="form-group">
                            <label class="control-label col-sm-3" for="password"><?php echo e(__('Password')); ?></label>
                            <div class="col-sm-9">
                                <input id="password" type="password" class="form-control <?php if ($errors->has('password')) :
if (isset($message)) { $messageCache = $message; }
$message = $errors->first('password'); ?> is-invalid <?php unset($message);
if (isset($messageCache)) { $message = $messageCache; }
endif; ?>" name="password" required autocomplete="current-password">

                                <?php if ($errors->has('password')) :
if (isset($message)) { $messageCache = $message; }
$message = $errors->first('password'); ?>
                                <span class="invalid-feedback" role="alert">
                                    <strong><?php echo e($message); ?></strong>
                                </span>
                            <?php unset($message);
if (isset($messageCache)) { $message = $messageCache; }
endif; ?>
                            </div>
                          </div>



                        


                        <div class="form-group">
                            <div class="col-sm-offset-3 col-sm-9">
                              <div class="checkbox">
                                <label> <input class="form-check-input" type="checkbox" name="remember" id="remember" <?php echo e(old('remember') ? 'checked' : ''); ?>> <?php echo e(__('Remember Me')); ?></label>
                              </div>
                            </div>
                          </div>

                        

                        <div class="form-group">
                            <div class="col-sm-offset-3 col-sm-9">
                                <button type="submit" class="btn btn-primary">
                                    <?php echo e(__('Login')); ?>

                                </button>
                                <?php if(Route::has('password.request')): ?>
                                    <a class="btn btn-link" href="<?php echo e(route('password.request')); ?>">
                                        <?php echo e(__('Forgot Your Password?')); ?>

                                    </a>
                                <?php endif; ?>
                            </div>
                          </div>



                        
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
<?php $__env->stopSection(); ?>

<?php echo $__env->make('layouts.app', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH /home/offerkrx/public_html/offerswing/resources/views/auth/login.blade.php ENDPATH**/ ?>