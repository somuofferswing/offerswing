<?php $__env->startSection('content'); ?>

<div class="container">


    <div class="panel panel-info">
          <div class="panel-heading">
                <h3 class="panel-title">Your Claim</h3>
          </div>
          <div class="panel-body">
            <div class="table-responsive">
                <table class="table">
                    <tr>
                        <td>Order Id</td>
                        <td><?php echo e($order['order_id']); ?></td>
                    </tr>
                    <tr>
                        <td>Type</td>
                        <td><?php echo e($order['type']); ?></td>
                    </tr>
                    <tr>
                            <td>Gift Name</td>
                            <td><?php echo e($gift['gift_name']); ?></td>

                    </tr>
                    <tr>
                            <td>Gift Code</td>
                            <td><?php echo e($claim['unique_code']); ?></td>

                    </tr>



                </table>
            </div>

            <a href="<?php echo e(url('/')); ?>" class="btn btn-primary">Go Home</a>
          </div>
    </div>


</div>
<?php $__env->stopSection(); ?>

<?php $__env->startSection('scripts'); ?>


<?php $__env->stopSection(); ?>

<?php echo $__env->make('layouts.app', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH C:\wamp64\www\offers\resources\views/gift_claimed.blade.php ENDPATH**/ ?>