<?php $__env->startSection('content'); ?>

<div class="container">

    <div class="row" style="padding:20px;">
        <?php
        $event = $order->event;
        ?>


        <h4 class="text-center">Your Event Order:</h4>
        <hr>
        <h5>Your Orders :<span style="font-weight:bold;"> <?php echo e($order->order_id); ?></span></h5>
        <?php if($event->payment_type == 'wingpoints'): ?>
            <h5>Event Price (Wing Points):<span style="font-weight:bold;"> <?php echo e($event->price); ?></span></h5>
        <?php else: ?>
            <h5>Event Price :<span style="font-weight:bold;"> Rs.<?php echo e($event->price); ?></span></h5>
        <?php endif; ?>
        <h5>Event Date :<span style="font-weight:bold;"> <?php echo e($event->event_date); ?> </span></h5>
        <h5>Event Time :<span style="font-weight:bold;"> <?php echo e($event->event_time); ?> </span></h5>
        <h5>Event Description :<span>
                <?php echo $event->description; ?>

            </span></h5>
        <hr>

    </div>

</div>
</div>
</div>

<?php $__env->stopSection(); ?>

<?php echo $__env->make('layouts.app', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH C:\wamp64\www\offers\resources\views/events/orders.blade.php ENDPATH**/ ?>