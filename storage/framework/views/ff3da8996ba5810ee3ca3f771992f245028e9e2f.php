<?php $__env->startSection('content'); ?>
<section >
    <div class="ps-page--product">
        <div class="container">

            <div class="row">
                <div class="col-12 col-md-4">
                    <div class="ps-post text-center">
                        <img src="<?php echo e(asset('storage/'.$event->event_image)); ?>">
                        <h3 class="pt-4 m-0"><?php echo e($event->title); ?></h3>
                    </div>
                </div>
                <div class="col-12 col-md-8">
                    <div class="text-center">
                        <?php echo QrCode::size(250)->generate($order->order_id);; ?>

                        <div>
                            <strong>Event Date : <?php echo e(date('d-m-Y',strtotime($event->event_date))); ?></strong>
                            <strong class="pl-4">Event Time : <?php echo e(date('h:i A',strtotime($event->event_time))); ?></strong>
                        </div>
                        
                        <div class="table-responsive">
                        <table class="table table-bordered ps-table ps-table--specification">
                                <tbody>
                                    
                                    <tr>
                                        <td>About Event</td>
                                        <td><?php echo $event->description; ?></td>
                                    </tr>
                                    <tr>
                                        <td>Brought on</td>
                                        <td><?php echo e(date('d-m-Y h:i A',strtotime($order_details->created_at))); ?></td>
                                    </tr>
                                    <tr>
                                        <td>Event Date</td>
                                        <td><?php echo e(date('d-m-Y',strtotime($event->event_date))); ?></td>
                                    </tr>
                                    <tr>
                                        <td>Event Time</td>
                                        <td><?php echo e(date('h:i A',strtotime($event->event_time))); ?></td>
                                    </tr>
                                    <tr>
                                        <td>Number of Tickets</td>
                                        <td><?php echo e($order->quantity); ?></td>
                                    </tr>
                                    <tr>
                                        <td>Price</td>
                                        <td>
                                        <?php if( $order_details->payment_type = 'wingpoints'): ?>
                                        <?php echo e($order_details->quantity); ?> x <?php echo e($event->points_to_claim); ?> = <?php echo e($order_details->quantity *  $event->points_to_claim); ?>

                                        <?php endif; ?>
                                        <?php if( $order_details->payment_type = 'cash'): ?>
                                        <?php echo e($order->quantity); ?> x <?php echo e($event->price); ?> = <?php echo e($order->quantity *  $event->price); ?>

                                        <?php endif; ?>
                                        </td>
                                        
                                    </tr>
                                </tbody>
                            
                            </table>
                        </div>
                    </div>
                    <div class="ps-product__content pt-3">
                        <h4 class="ps-product__heading">Event Address</h4>
                        <p><?php echo $event->event_address; ?></p>
  
                        <h4 class="ps-product__heading pt-3">Terms & Conditions</h4>
                        <div class="ps-document">
                            <?php echo $event->terms_conditions; ?>

                        </div>
                    </div>

                </div>
            </div>
            
        </div>
    </div>
</section>
<?php $__env->stopSection(); ?>

<?php echo $__env->make('layouts.app', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH C:\wamp64\www\offers\resources\views/frontend/eventorder.blade.php ENDPATH**/ ?>