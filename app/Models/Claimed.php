<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Claimed extends Model
{
    protected $table = 'claimed';

    public function order()
    {
        return $this->belongsTo(Order::class, 'order_id', 'order_id');
    }
}
