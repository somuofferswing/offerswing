<?php

namespace App\Http\Controllers\Backend;

use App\Http\Controllers\Controller;
use App\Models\Category;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;

class CategoriesController extends Controller
{
    public function index()
    {
        $allcategories = Category::select('categories.*', 'c1.id as parent_category_id', 'c1.category_name as parent_category_name')
            ->leftjoin('categories as c1', 'categories.parent_id', '=', 'c1.id')
            ->paginate(15)
        ;

        return view('backend.category.index', compact('allcategories'));
    }

    // public function show($id)
    // {

    //     $category = Category::select('categories.*','p1.category_name as p1_category_name','p2.category_name as p2_category_name')
    //         ->leftjoin('categories as p1', 'p1.id', '=', 'categories.parent_id')
    //         ->leftjoin('categories as p2', 'p2.id', '=', 'p1.parent_id')
    //         ->where('categories.id', $id)
    //         ->first();

    //     return view('backend.category.show', compact('category'));
    // }

    public function create()
    {
        $categories = Category::with('childrenRecursive')->whereNull('parent_id')->get()->toArray();

        return view('backend.category.create', compact('categories'));
    }

    public function store(Request $request)
    {
        $category = new Category();
        $this->validate($request, [
            'category_name' => 'required',
        ]);
        $category->category_name = $request['category_name'];
        $category->parent_id = $request['parent_id'];
        $slug = $this->slugify($request['category_name']);
        $category->slug = $slug;

        if ($request->hasFile('cat_image')) {
            $file = $request->file('cat_image');
            $fileName = $slug.'_'.time().'.'.$file->getClientOriginalExtension();
            $path = $file->storeAs('categoryimages', $fileName, 'public');
            $category->cat_image = $path;
        }

        $category->save();

        return redirect('/categories');
    }

    public function edit($id)
    {
        $category = Category::select('*')
            ->where('id', $id)
            ->first()
        ;
        $categories = Category::with('childrenRecursive')->whereNull('parent_id')->get()->toArray();

        return view('backend.category.edit', compact('categories', 'category'));
    }

    public function update(Request $request, $id)
    {
        $category = Category::find($id);

        $this->validate($request, [
            'category_name' => 'required',
        ]);
        $category->category_name = $request['category_name'];
        $category->parent_id = $request['parent_id'];
        if ($category->isDirty('category_name')) {
            $slug = $this->slugify($request['category_name']);
            $category->slug = $slug;
        } else {
            $slug = $category->slug;
        }

        if ($request->hasFile('cat_image')) {
            Storage::disk('public')->delete($category->cat_image);

            $file = $request->file('cat_image');
            $fileName = $slug.'_'.time().'.'.$file->getClientOriginalExtension();
            $path = $file->storeAs('categoryimages', $fileName, 'public');
            $category->cat_image = $path;
        }

        $category->save();

        return redirect('/categories');
    }

    public function delete($id)
    {
        $sub_category = Category::where('parent_id', $id)->pluck('id')->toArray();

        if (!empty($sub_category)) {
            foreach ($sub_category as $sub) {
                Category::destroy($sub);
            }
        }

        Category::destroy($id);

        return json_encode('Category Deleted Sucessfully');
    }

    public function slugify($str)
    {
        $search = ['Ș', 'Ț', 'ş', 'ţ', 'Ş', 'Ţ', 'ș', 'ț', 'î', 'â', 'ă', 'Î', 'Â', 'Ă', 'ë', 'Ë'];
        $replace = ['s', 't', 's', 't', 's', 't', 's', 't', 'i', 'a', 'a', 'i', 'a', 'a', 'e', 'E'];
        $str = str_ireplace($search, $replace, strtolower(trim($str)));
        $str = preg_replace('/[^\w\d\-\ ]/', '', $str);
        $str = str_replace(' ', '-', $str);

        return $this->getUniqueCategorySlug(preg_replace('/\-{2,}/', '-', $str));
    }

    private function getUniqueCategorySlug($slug)
    {
        $existingCount = Category::where('slug', 'like', $slug . '-%')->count();

        if ($existingCount) {
            return $slug . '-' . ($existingCount);
        }

        return $slug;
    }
}
