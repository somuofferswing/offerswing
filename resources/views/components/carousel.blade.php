<div id="{{ $coursel_id }}" class="carousel slide" data-ride="carousel">

    <ul class="carousel-indicators">

        @for ($i = 0; $i < count($slider) ; $i++) <li data-target="#{{ $coursel_id }}" data-slide-to="{{ $i }}"

            class="{{ $i==0 ? 'active' : '' }}">

            </li>

            @endfor

    </ul>

    <div class="carousel-inner">

        @foreach ($slider as $index=>$item)

        <div class="carousel-item {{ $index==0 ? 'active' : '' }}">

            @if ($item->image_path)

            <img class="mx-auto d-block img-fluid" alt="{{ $item->name }}" src="{{ asset('storage/'.$item->image_path)}}"

                width="100%" style="min-height:300px;max-height:300px;">

            @else

            <img class="mx-auto d-block img-fluid" alt="{{ $item->name }}" src="{{ asset('storage/'.$item->image)}}"

                width="100%" style="min-height:300px;max-height:300px;">



            @endif





        </div>

        @endforeach

    </div>

    <a class="carousel-control-prev" href="#{{ $coursel_id }}" data-slide="prev"><span

            class="carousel-control-prev-icon"></span></a>

    <a class="carousel-control-next" href="#{{ $coursel_id }}" data-slide="next"><span

            class="carousel-control-next-icon"></span></a>

</div>