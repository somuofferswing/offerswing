<?php

namespace App\Http\Controllers;

use App\Models\Claimed;
use App\Models\Event;
use App\Models\Notification;
use App\Models\OfferNotification;
use App\Models\Order;
use App\Models\OrderDetail;
use App\Models\User;
use App\Models\WingPoints;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Crypt;
use Illuminate\Support\Facades\Mail;

class EventsController extends Controller
{
    public function getEvent($slug)
    {
        // \print_r($slug);
        // die;
        $user = auth()->user();
        $user_id = $user['id'];
        $user_email = $user['email'];

        $event = Event::where(['slug'=>$slug])->first();
        $event_id = $event['id'];
 
        if ('wingpoints' == $event->payment_type) {
            $order = new Order();
            $order_id = $this->generateRandomString(15);
            $order->order_id = $order_id;
            $order->user_id = $user_id;
            $order->payment_type = $event->payment_type;
            $order->payment_status = 'PENDING';
            $order->amount = $event->points_to_claim;

            $order_details = new OrderDetail();
            $order_details->order_type = 'EVENT';
            $order_details->user_id = $user_id;
            $order_details->order_type_id = $event_id;
            $order_details->quantity = 1;
            $order_details->price = $event->points_to_claim;
            $order_details->payment_type = $event->payment_type;
            $order_details->final_price = $event->points_to_claim;
            $order_details->valid_upto = '';

        // $order->save();
            // $order->orderDetails()->save($order_details);
        } elseif ('cash' == $event->payment_type || 'both' == $event->payment_type) {
            $order = new Order();
            $order_id = $this->generateRandomString(15);
            $order->order_id = $order_id;
            $order->user_id = $user_id;
            $order->payment_type = $event->payment_type;
            $order->payment_status = 'PENDING';
            $order->amount = $event->event_price;

            $order_details = new OrderDetail();
            $order_details->order_type = 'EVENT';
            $order_details->user_id = $user_id;
            $order_details->order_type_id = $event_id;
            $order_details->quantity = 1;
            $order_details->price = $event->event_price;
            $order_details->payment_type = $event->payment_type;
            $order_details->final_price = $event->event_price;
            $order_details->valid_upto = '';
        }
        $order->save();
        $order->orderDetails()->save($order_details);
        
        return redirect()->route('user_order', Crypt::encrypt($order->id));
    }

    public function eventOrder($order_id)
    {
        $user = auth()->user();
        $order = Order::where(['order_id' => $order_id])->first();
        $order_details = $order->orderDetails;
        $event_id = $order_details->order_type_id;
        $event = Event::find($event_id);
        $user_id = $order_details->user_id;

        
        return view('frontend.eventorder', compact('order', 'order_details', 'event'));
    }

    public function generateRandomString($length = 10)
    {
        $characters = '0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZ';
        $charactersLength = strlen($characters);
        $randomString = '';
        for ($i = 0; $i < $length; ++$i) {
            $randomString .= $characters[rand(0, $charactersLength - 1)];
        }

        return $randomString;
    }
}
