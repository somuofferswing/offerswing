<?php

namespace App\Http\Controllers;

use App\Models\ImageDiv;
use App\Models\ImageDivData;
use Illuminate\Http\Request;

class ImageDivController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('frontend.imagediv.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $imagediv = new ImageDiv();

        $imagediv->imagediv_name = $request['imagediv_name'];

        $imagediv->save();

        if ($request->hasfile('imagediv_data_image')) {
            foreach ($request->file('imagediv_data_image') as $key => $image) {
                // $name = $image->getClientOriginalName();

                $new_name = $imagediv->id.'_'.$key.'.'.$image->getClientOriginalExtension();

                $image->move(public_path().'/slider_images/', $new_name);

                $data[$key] = $new_name;
            }
        }

        foreach ($request['imagediv_data'] as $key => $value) {
            $imagediv_data = new ImageDivData();

            $imagediv_data->image_div_id = $imagediv->id;

            $imagediv_data->name = $value['name'];

            $imagediv_data->image = 'slider_images'.'/'.$data[$key];

            // $imagediv_data->url = $value['url'];

            $imagediv_data->save();
        }

        return redirect('/admin/home');
    }

    /**
     * Display the specified resource.
     *
     * @param int $id
     *
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param int $id
     *
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $imagediv = ImageDiv::find($id);

        $imagediv_data = ImageDivData::where('image_div_id', $id)->get();

        return view('frontend.imagediv.edit', compact('imagediv', 'imagediv_data'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param int $id
     *
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $imagediv = ImageDiv::find($id);

        $imagediv->imagediv_name = $request['imagediv_name'];

        $imagediv->save();

        if ($request->hasfile('imagediv_data_image')) {
            //  var_dump("hello");die;

            foreach ($request->file('imagediv_data_image') as $key => $image) {
                $name = $image->getClientOriginalName();

                $new_name = $id.'_'.$key.'.'.$image->getClientOriginalExtension();

                $image->move(public_path().'/imagediv/', $new_name);

                $data[$key] = $new_name;
            }
        }

        foreach ($request['imagediv_data'] as $key => $value) {
            if (isset($value['id']) && !empty($value['id'])) {
                $imagediv_data = ImageDivData::find($value['id']);

                $imagediv_data->image_div_id = $id;

                $imagediv_data->name = $value['name'];

                if (!empty($data[$key])) {
                    $imagediv_data->image = 'imagediv'.'/'.$data[$key];
                }

                // $imagediv_data->url = $value['url'];

                $imagediv_data->save();
            } else {
                $imagediv_data = new ImageDivData();

                $imagediv_data->image_div_id = $id;

                $imagediv_data->name = $value['name'];

                $imagediv_data->image = 'imagediv'.'/'.$data[$key];

                // $imagediv_data->url = $value['url'];

                $imagediv_data->save();
            }
        }

        return redirect('/admin/home');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param int $id
     *
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
    }
}
