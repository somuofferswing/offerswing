<?php $__env->startSection('content'); ?>

<div class="container">
        <nav aria-label="breadcrumb">
                <ol class="breadcrumb">
                    <li class="breadcrumb-item"><a href="/">Home</a></li>
                    <li class="breadcrumb-item active" aria-current="page">Favourite Shops</li>
                </ol>
            </nav>

    <h3 class="text-center border-bottom">Favourite Shops</h3>
    <div class="row">

        <?php $__currentLoopData = $shops; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $key=>$item): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
        <?php
        $image = $item->shop_images;
        $firstimage = $image[0];
        ?>
        <div class="col-md-12">
            <div class="card my-2">
                <div class="card-body">
                    <div class="row">
                        <div class="col-md-8" style="padding:1rem">
                            <div>
                                <a href="<?php echo url('/viewshop',[$item->id]);; ?>">
                                    <b style="color:#df685d;font-size:1.4rem;"><?php echo e(ucfirst($item->shop_name)); ?></b>
                                </a>

                                <span class="float-right">
                                    <?php if(Auth::guest()): ?>
                                    <a class="btn" data-id="<?php echo e($item->shopid); ?>" data-toggle="modal"
                                        data-target="#myModal1"><i class="far fa-heart" style="color:#e31b23"></i></a>
                                    <?php else: ?>
                                    <?php if(in_array($item->id,$favouriteshops_ids)): ?>
                                    <a class="btn unlike" data-id="<?php echo e($item->id); ?>"><i class="fas fa-heart"
                                            style="color:#e31b23"></i></a>
                                    <?php else: ?>
                                    <a class="btn like" data-id="<?php echo e($item->id); ?>"> <i class="far fa-heart"
                                            style="color:#e31b23"></i></a>
                                    <?php endif; ?>

                                    <?php endif; ?>
                                </span>

                            </div>
                            <div class="py-3">

                                <div class="media">
                                    <div class="media-left">
                                        <i class="fas fa-flag" style="color:#df685d;"></i>
                                    </div>
                                    <div class="media-body pl-2">
                                        <?php echo e($item->address); ?>,<?php echo e($item->city); ?>,<?php echo e($item->state); ?>,<?php echo e($item->state); ?>

                                    </div>
                                </div>
                                <div class="media">
                                    <div class="media-left">
                                        <i class="fas fa-phone-volume" style="color:#13ca42"></i>
                                    </div>
                                    <div class="media-body pl-2">
                                        <?php echo e($item->phone_number); ?>

                                    </div>
                                </div>

                                <p style="padding-left:20px;">
                                    <?php for($i = 0; $i < 5; $i++): ?> <?php if($i< (int)$item->shop_rating): ?>
                                        <span class="fas fa-star" style="color:#df685d"></span>
                                        <?php else: ?>
                                        <?php if(($item->shop_rating-(int)$item->shop_rating) == 0.5 &&
                                        ((int)$item->shop_rating) ==
                                        $i ): ?>
                                        <span class="fas fa-star-half-alt" style="color:#df685d"></span>
                                        <?php else: ?>
                                        <span class="far fa-star" style="color:#df685d"></span>
                                        <?php endif; ?>
                                        <?php endif; ?>
                                        <?php endfor; ?>
                                </p>

                                <?php if(in_array(date('N',time())+1 , explode(",",$item->active_days))): ?>

                                <div class="media" style="margin-top:0;">
                                    <div class="media-left" style="margin-right:10px;">
                                        <i class="fa fa-chevron-right" style="font-size: 18px;color: #a5a5a5;"></i>
                                    </div>
                                    <div class="media-body">
                                        <p class="">Opened </p>
                                    </div>
                                </div>
                                <?php else: ?>
                                <div class="media" style="margin-top:0;">
                                    <div class="media-left">
                                        <i class="fa fa-chevron-right" style="font-size: 18px;color: #a5a5a5;"></i>
                                    </div>
                                    <div class="media-body">
                                        <p class="">Closed </p>
                                    </div>
                                </div>
                                <?php endif; ?>
                            </div>
                        </div>
                        <div class="col-md-4">

                            <a href="<?php echo url('/viewshop',[$item->id]);; ?>">
                                <img src="<?php echo asset("$firstimage->image_path"); ?>" class="img-fluid"
                                    style="width:100%;height:100%">

                            </a>

                        </div>
                    </div>
                </div>

            </div>
        </div>
        <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>



    </div>

</div>

<?php $__env->stopSection(); ?>

<?php $__env->startSection('scripts'); ?>

<script>
    $(document).on('click', ".unlike", function () {
        var id = $(this).data('id');
        var element = this;

        var data = {
            shop_id: id
        };
        $.ajax({
            type: "post",
            url: '<?php echo e(url("removefavouriteshop")); ?>',
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            },
            data: data,
            cache: false,
            success: function (data) {
                if (data.status == "success") {
                    $(element).removeClass('unlike').addClass('like').html('<i class="far fa-heart" style="color:#e31b23"></i>');
                }
            },
            error: function (data) {
                alert('Fail to run unlike..');
            }
        });


    });
    $(document).on('click', ".like", function () {
        var id = $(this).data('id');
        var element = this;

        var data = {
            shop_id: id
        };
        $.ajax({
            type: "post",
            url: '<?php echo e(url("addfavouriteshop")); ?>',
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            },
            data: data,
            cache: false,
            success: function (data) {
                console.log(data);
                if (data.status == "success") {
                    $(element).removeClass('like').addClass('unlike').html('<i class="fas fa-heart" style="color:#e31b23"></i>');

                }
            },
            error: function (data) {
                alert('Fail to run like..');
            }
        });


    });
</script>

<?php $__env->stopSection(); ?>
<?php echo $__env->make('layouts.app', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH C:\xampp\htdocs\offerswing\resources\views/favouriteshops.blade.php ENDPATH**/ ?>