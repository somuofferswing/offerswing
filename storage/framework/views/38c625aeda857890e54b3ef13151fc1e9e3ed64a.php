

<?php $__env->startSection('content'); ?>
<div class="content-wrapper">
    <section class="content-header">
        <h1>
            Slider
            <small>Edit Slider</small>
        </h1>
        <!-- <ol class="breadcrumb">
              <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
              <li class="active">Dashboard</li>
            </ol> -->
    </section>

    <section class="content">
        <div class="box-body" style="padding: 10px 50px;">
            <form class="form-horizontal" method="POST" action="<?php echo e(url('/imagediv/update',[$imagediv->id])); ?>"
                enctype="multipart/form-data">
                <?php echo csrf_field(); ?>
                <div class="form-group">
                    <label for="imagediv_name" class="col-sm-2  control-label">Name</label>
                    <div class="col-sm-10">
                        <input id="imagediv_name" type="text" class="form-control" name="imagediv_name"
                            value="<?php echo e($imagediv->imagediv_name); ?>" required autocomplete="imagediv_name" autofocus>
                    </div>
                </div>

                <hr>
                
                <div class="table-responsive" >

                    <table class="table" style="width:100%">
                        <tr>
                            <th>Name</th>
                            <th>Image</th>
                            <th>url(optional)</th>
                        </tr>
                        <?php $__currentLoopData = $imagediv_data; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $key=>$item): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>

                        <tr>
                            <input type="hidden" name="imagediv_data[<?php echo e($key); ?>][id]" value="<?php echo e($item->id); ?>">
                            <td><input type="text" name="imagediv_data[<?php echo e($key); ?>][name]" value="<?php echo e($item->name); ?>"></td>
                            <td>

                                <input type="file" name="imagediv_data_image[]"
                                    onchange="document.getElementById('image'+<?php echo e($key); ?>).src = window.URL.createObjectURL(this.files[0])">
                                <img src="<?php echo e(asset("$item->image")); ?>" alt="" id="image<?php echo e($key); ?>" height="100px">
                            </td>
                            
                        </tr>
                        <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                    </table>
                </div>
                <div class="pull-right">

                    <button class="btn btn-info" type="submit">Update</button>
                    <button class="btn btn-danger" type="button">Cancel</button>
                </div>
            </form>
        </div>

    </section>
</div>
<?php $__env->stopSection(); ?>
<?php $__env->startSection('scripts'); ?>

<script>
    var loadFile = function(event) {
    var reader = new FileReader();
    reader.onload = function(){
      var output = document.getElementById('output');
      output.src = reader.result;
    };
    reader.readAsDataURL(event.target.files[0]);
  };

  $('#addnew').on('click',function(){

    var count = $('table tr').length;
    var len = count-1;

            var markup = `<tr></tr><td><input type="text" name="imagediv_data[`+len+`][name]" value="" required></td>
                        <td>
                            <input type="file" name="imagediv_data_image[]" onchange="document.getElementById('image`+len+`').src = window.URL.createObjectURL(this.files[0])">
                                <img id="image`+len+`" height="100px" required>
                            </td>
                            </tr>`;
            $("table tbody").append(markup);
  });
</script>

<?php $__env->stopSection(); ?>

<?php echo $__env->make('layouts.admin', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH /home/offerkrx/public_html/offerswing/resources/views/imagediv/edit.blade.php ENDPATH**/ ?>