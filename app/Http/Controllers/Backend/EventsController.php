<?php

namespace App\Http\Controllers\Backend;

use App\Http\Controllers\Controller;
use App\Models\Claimed;
use App\Models\Event;
use App\Models\OfferNotification;
use App\Models\Order;
use App\Models\User;
use App\Models\WingPoints;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Storage;

class EventsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $events = Event::all();

        return view('backend.events.index', compact('events'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $retailers = User::getRetailers();

        return view('backend.events.create', compact('retailers'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $events = new Event();
        $this->validate($request, [
            'title' => 'required',
            'description' => 'required',
            'event_date' => 'required',
            'event_time' => 'required',
            'event_address' => 'required',
            'payment_type' => 'required',
            'quantity' => 'required',
            'vendor_id' => 'required',
        ]);

        $events->vendor_id = $request['vendor_id'];
        $events->title = $request['title'];
        $events->description = $request['description'];
        $events->event_date = $request['event_date'];
        $events->event_time = $request['event_time'];
        $events->event_address = $request['event_address'];
        $events->event_price = $request['event_price'];
        $events->event_lat = $request['event_lat'];
        $events->event_lng = $request['event_lng'];
        $events->payment_type = $request['payment_type'];
        $events->quantity = $request['quantity'];
        $events->terms_conditions = $request['terms_conditions'];
        $events->date_time_description = $request['date_time_description'];
        $events->points_to_claim = $request['points_to_claim'];

        $slug = $this->slugify($request['title']);
        $events->slug = $slug;

        if ($request->hasFile('event_image')) {
            $file = $request->file('event_image');
            $fileName = $slug.'_'.time().'.'.$file->getClientOriginalExtension();
            $path = $file->storeAs('events', $fileName, 'public');
            $events->event_image = $path;
        }

        // if ($request->hasFile('event_image')) {
        //     $image = $request->file('event_image');
        //     $extension = $image->getClientOriginalExtension();
        //     $timestamp = time();
        //     $image->move('event_image', $timestamp.'.'.$extension);
        //     $events->event_image = 'event_image/'.$timestamp.'.'.$extension;
        // }

        $events->save();

        return redirect('/events')->with('success', 'Events Ceated Successfully');
    }

    /**
     * Display the specified resource.
     *
     * @param int $id
     *
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $events = Event::find($id);

        return view('backend.events.show', compact('Events'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param int $id
     *
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $events = Event::find($id);
        $retailers = User::getRetailers();

        return view('backend.events.edit', compact('events', 'retailers'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param int $id
     *
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $events = Event::find($id);
        $this->validate($request, [
            'title' => 'required',
            'description' => 'required',
            'event_date' => 'required',
            'event_time' => 'required',
            'event_address' => 'required',
            'payment_type' => 'required',
            'quantity' => 'required',
            'vendor_id' => 'required',
        ]);

        $events->vendor_id = $request['vendor_id'];
        $events->title = $request['title'];
        $events->description = $request['description'];
        $events->event_date = $request['event_date'];
        $events->event_time = $request['event_time'];
        $events->event_address = $request['event_address'];
        $events->event_price = $request['event_price'];
        $events->event_lat = $request['event_lat'];
        $events->event_lng = $request['event_lng'];
        $events->payment_type = $request['payment_type'];
        $events->quantity = $request['quantity'];
        $events->terms_conditions = $request['terms_conditions'];
        $events->date_time_description = $request['date_time_description'];
        $events->points_to_claim = $request['points_to_claim'];


        if ($events->isDirty('title')) {
            $slug = $this->slugify($request['title']);
            $events->slug = $slug;
        } else {
            $slug = $events->slug;
        }

        if ($request->hasFile('event_image')) {
            Storage::disk('public')->delete($events->event_image);

            $file = $request->file('event_image');
            $fileName = $slug.'_'.time().'.'.$file->getClientOriginalExtension();
            $path = $file->storeAs('events', $fileName, 'public');
            $events->event_image = $path;
        }


        $events->save();

        return redirect('/events')->with('success', 'Events Updated Successfully');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param int $id
     *
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $events = Event::find($id);
        $events->delete();

        return redirect('/events')->with('success', 'Events has been deleted Successfully');
    }

    public function getEvent($event_id)
    {
        $user = auth()->user();
        $user_id = $user['id'];
        $user_email = $user['email'];

        $event = Event::find($event_id);

        if ('wingpoints' == $event->payment_type) {
            $order = new Order();
            $order_id = $this->generateRandomString(15);
            $order->order_id = $order_id;
            $order->type = 'event';
            $order->type_id = $event_id;
            $order->user_id = $user_id;
            $order->wing_points = $event->event_price;
            $order->quantity = 1;
            $order->price = 0;
            $order->final_price = 0;
            $order->payment_status = 'SUCCESS';

            if ($order->save()) {
                $unique_code = $this->generateRandomString(20);
                $claim = new Claimed();

                $claim->claimed_type = $order['type'];
                $claim->claimed_type_id = $order['type_id'];
                $claim->order_id = $order_id;
                $claim->user_id = $order['user_id'];
                $claim->unique_code = $unique_code;

                $claim->save();

                $wing_points = new WingPoints();

                $wing_points->type = 'EVENT';
                $wing_points->type_id = $event_id;
                $wing_points->user_id = $user_id;
                $wing_points->points = -$event->event_price;

                $wing_points->save();

                $notifications = new  OfferNotification();

                $notifications->type = 'EVENT';
                $notifications->type_id = $event_id;
                $notifications->user_id = $user_id;
                $notifications->notification_message = "your tickets for event '".$event->title."' has been booked Successfully!";

                $notifications->save();

                $settings = session()->get('settings');

                foreach ($settings as $key => $value) {
                    if ('contact_email' == $value['name']) {
                        $from_email = $value['value'];
                    }
                }

                $event = $order->event;

                Mail::send('email.order_event_details', ['order' => $order, 'claimed' => $claim, 'event' => $event], function ($message) use ($user_email,$from_email) {
                    $message->to($user_email)->from($from_email)->subject('Order Details');
                });

                return redirect()->route('eventorder', [$order_id]);
            }
        } elseif ('cash' == $event->payment_type || 'both' == $event->payment_type) {
            $order = new Order();

            $order->order_id = $this->generateRandomString(15);
            $order->type = 'event';
            $order->type_id = $event_id;
            $order->user_id = $user_id;
            $order->wing_points = 0;
            $order->quantity = 1;
            $order->price = $event->price;
            $order->final_price = $event->price;
            $order->payment_status = 'PENDING';

            if ($order->save()) {
                return redirect()->route('user_order', [$order->id]);
            }
        }
    }

    public function eventOrder($order_id)
    {
        $order = Order::where('order_id', $order_id)->first();
        $claimed = Claimed::where('order_id', $order_id)->first();

        return view('backend.events.orders', compact('order', 'claimed'));
    }

    public function generateRandomString($length = 10)
    {
        $characters = '0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZ';
        $charactersLength = strlen($characters);
        $randomString = '';
        for ($i = 0; $i < $length; ++$i) {
            $randomString .= $characters[rand(0, $charactersLength - 1)];
        }

        return $randomString;
    }

    public function slugify($str)
    {
        $search = ['Ș', 'Ț', 'ş', 'ţ', 'Ş', 'Ţ', 'ș', 'ț', 'î', 'â', 'ă', 'Î', 'Â', 'Ă', 'ë', 'Ë'];
        $replace = ['s', 't', 's', 't', 's', 't', 's', 't', 'i', 'a', 'a', 'i', 'a', 'a', 'e', 'E'];
        $str = str_ireplace($search, $replace, strtolower(trim($str)));
        $str = preg_replace('/[^\w\d\-\ ]/', '', $str);
        $str = str_replace(' ', '-', $str);

        return $this->getUniqueEventSlug(preg_replace('/\-{2,}/', '-', $str));
    }

    private function getUniqueEventSlug($slug)
    {
        $existingCount = Event::where('slug', 'like', $slug . '-%')->count();

        if ($existingCount) {
            return $slug . '-' . ($existingCount);
        }

        return $slug;
    }
}
