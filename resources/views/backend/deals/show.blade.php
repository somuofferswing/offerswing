@extends('layouts.admin')



@section('content')

<style>
    td {

        /* overflow: hidden; */

        max-width: 400px;

        word-wrap: break-word;

    }
</style>

<div class="content-wrapper">

    <section class="content-header">

        <h1>

            Wing Deals

            <small>View Wing Deal({{$deal->name }})</small>

        </h1>

    </section>



    <section class="content">

        <img src="{{ asset('storage/'.$deal->pic) }}" alt="{{$deal->name }}" class="img-responsive" width="100%">

        <hr>



        <div class="table-responsive">

            <table class="table table-striped">

                <tr>

                    <td>Id</td>

                    <td>{{ $deal->id }}</td>

                </tr>



                <tr>

                    <td>Name</td>

                    <td>{{ $deal->name }}</td>

                </tr>



                <tr>

                    <td>Description</td>

                    <td>{!! $deal->description !!}</td>

                </tr>



                <tr>

                    <td>Payment Type</td>

                    <td>{{ $deal->payment_type }}</td>

                </tr>

                <tr>

                    <td>Price</td>

                    <td>
                        {{ $deal->price }}
                    </td>

                </tr>

                <tr>

                    <td>Deals Quantity</td>

                    <td>{{ $deal->quantity }}</td>

                </tr>

                <tr>

                    <td>Claims Per user</td>

                    <td>{{ $deal->max_count_for_user }}</td>

                </tr>

                <tr>

                    <td>Deal Show From</td>

                    <td>{{ $deal->deal_start_date }}</td>

                </tr>

                <tr>

                    <td>Deal show To</td>

                    <td>{{ $deal->deal_end_date }}</td>

                </tr>

                <tr>

                    <td>Discount Percentage</td>

                    <td>{{ (($deal->original_price- $deal->price )/$deal->original_price) * 100 }}</td>

                </tr>
                {{-- 
                <tr>

                    <td>Wing Points Rewarded</td>

                    <td>{{ $deal->points_rewarded }}</td>

                </tr> --}}



                <tr>

                    <td>Terms & Conditions</td>

                    <td>{!! $deal->terms_conditions !!}</td>

                </tr>



            </table>

        </div>





        <hr>



        Deal Orders



        <table class="table">

            <tr>

                <th>Unique Claim Code</th>

                <th>User Name</th>

                <th>User Mobile</th>

                <th>User Email</th>

                <th>Bought On</th>

                <th>Claim Status</th>

                <th>Actions</th>

            </tr>



            @foreach ($OrderDetail as $item)

            @php
            $order = $item->order;
            $user = $order->user;
            @endphp


            <tr>

                <td>{{ $order->order_id }}</td>

                <td>{{ $user->first_name.' '.$user->last_name }}</td>

                <td>{{ $user->mobile }}</td>

                <td>{{ $user->email }}</td>

                <td>{{ $item->created_at }}</td>

                <td>@if ($item->claim_status ==0)

                    Not Used

                    @else

                    Used

                    @endif</td>

                <td>

                    @if ($item->claim_status ==0)

                    <div class="btn btn-info changestatus" data-id="{{$order->order_id }}">

                        Change to used

                    </div>

                    @else

                    <div class="btn btn-danger changestatus" data-id="{{$order->order_id }}">

                        Change to not used

                    </div>

                    @endif

                </td>





            </tr>





            @endforeach



        </table>

    </section>

</div>

@endsection

@section('scripts')

<script>
    $('.changestatus').on('click',function(){

        var id = $(this).data('id');
        // alert(id);
        data = {
            order_id:id
        }
        console.log(data);

        $.ajax({

        type: "POST",

        url: "{{ url('/claimStatus') }}",
        headers: {

            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')

            },

        data: data,

        success: function (response) {
            console.log(response);

            if(response == 'Success'){

                window.location.reload();

            }else{
                console.log(response);

                alert(response);

            }

        }

    });



    });

    

</script>



@endsection