<?php $__env->startSection('content'); ?>
    <link rel="stylesheet"
          href="<?php echo e(asset('adminlte/bower_components/bootstrap-datepicker/dist/css/bootstrap-datepicker.min.css')); ?>">
    <div class="content-wrapper">
        <section class="content-header">
            <h1>
                Offers
                <small>Add Offer</small>
            </h1>
            <!-- <ol class="breadcrumb">
                  <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
                  <li class="active">Dashboard</li>
                </ol> -->
        </section>

        <section class="content">

            <div class="box box-info">
                <div class="box-header with-border">
                    <h3 class="box-title">Add Offer</h3>
                </div>
                <form class="form-horizontal" method="POST" action="<?php echo e(route('store-offer')); ?>"
                      enctype="multipart/form-data">
                    <?php echo csrf_field(); ?>
                    
                    <div class="box-body">
                        <div class="form-group ">
                            <label for="name" class="col-sm-2 control-label">Name</label>
                            <div class="col-sm-10">
                                <input id="name" type="text" class="form-control" name="name" value="<?php echo e(old('name')); ?>"
                                       required autocomplete="name" autofocus>
                                <?php if($errors->has('name')): ?>
                                    <div class="danger"><?php echo e($errors->first('name')); ?></div>
                                <?php endif; ?>
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="offer_image" class="col-sm-2 control-label">Image</label>
                            <div class="col-sm-10">
                                <input type="file" class="form-control-file" id="offer_image" name="offer_image"
                                       onchange="loadFile(event)">
                                <img id="output" height="100px" width="100px"/>
                            </div>
                        </div>
                        <div class="form-group ">
                            <label for="description" class="col-sm-2 control-label">Description</label>
                            <div class="col-sm-10">
                            <textarea id="description" type="text" class="form-control" name="description" required
                                      autocomplete="description" autofocus><?php echo e(old('description')); ?></textarea>
                                <?php if($errors->has('description')): ?>
                                    <div class="danger"><?php echo e($errors->first('description')); ?></div>
                                <?php endif; ?>
                            </div>
                        </div>
                        <div class="form-group ">
                            <label for="shop_id" class="col-sm-2 control-label">Shop</label>
                            <div class="col-sm-10">

                                <select class="form-control" id="shop_id" name="shop_id" value="<?php echo e(old('shop_id')); ?>"
                                        required autocomplete="shop_id" autofocus>
                                    <option>--Select--</option>
                                    <?php $__currentLoopData = $shops; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $shop): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                        <option value="<?php echo e($shop->id); ?>"><?php echo e($shop->name); ?></option>
                                    <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                </select>
                                <?php if($errors->has('shop_id')): ?>
                                    <div class="danger"><?php echo e($errors->first('shop_id')); ?></div>
                                <?php endif; ?>
                            </div>
                        </div>














                        <div class="form-group ">
                            <label for="from_date" class="col-sm-2 control-label">From Date</label>
                            <div class="col-sm-10">
                                <input type="text" class="form-control pull-right" id="from_date" name="from_date"
                                       value="<?php echo e(old('from_date')); ?>" required autocomplete="from_date" autofocus>

                                <?php if($errors->has('from_date')): ?>
                                    <div class="danger"><?php echo e($errors->first('from_date')); ?></div>
                                <?php endif; ?>
                            </div>
                        </div>
                        <div class="form-group ">
                            <label for="to_date" class="col-sm-2 control-label">To Date</label>
                            <div class="col-sm-10">
                                <input type="text" class="form-control pull-right" id="to_date" name="to_date"
                                       value="<?php echo e(old('to_date')); ?>" required autocomplete="to_date" autofocus>
                                <?php if($errors->has('to_date')): ?>
                                    <div class="danger"><?php echo e($errors->first('to_date')); ?></div>
                                <?php endif; ?>
                            </div>
                        </div>

                        <div class="form-group ">
                            <label for="max_wing_points_assured" class="col-sm-2 control-label">Top Offer</label>
                            <div class="col-sm-10">
                                <input type="checkbox" class="" name="top" value="1">
                                <?php if($errors->has('top')): ?>
                                    <div class="danger"><?php echo e($errors->first('top')); ?></div>
                                <?php endif; ?>
                            </div>
                        </div>


                        
                        <div class="pull-right">
                            <button type="submit" class="btn btn-info"><?php echo e(__('Add Offer')); ?></button>
                            <a type="button" class="btn btn-danger"
                               onclick="window.history.go(-1); return false;"><?php echo e(__('Cancel')); ?></a>
                        </div>

                    </div>
                </form>
            </div>
        </section>
    </div>
<?php $__env->stopSection(); ?>
<?php $__env->startSection('scripts'); ?>
    <script src="<?php echo e(asset('adminlte/bower_components/bootstrap-datepicker/dist/js/bootstrap-datepicker.min.js')); ?>">
    </script>
    <script src="<?php echo e(asset('adminlte/bower_components/select2/dist/js/select2.full.min.js')); ?>"></script>
    <script src="<?php echo e(asset('adminlte/bower_components/ckeditor/ckeditor.js')); ?>"></script>
    <script>
        $(function () {
            CKEDITOR.replace('description')
        });
        $('.select2').select2();
        $(document).ready(function () {
            $('#from_date').datepicker({
                autoclose: true,
                format: 'yyyy-mm-dd',
                startDate: new Date(),
            });

            $('#to_date').datepicker({
                autoclose: true,
                format: 'yyyy-mm-dd',
                startDate: new Date(),
            }).change(function () {
                var from = $('#from_date').val();
                var to = $('#to_date').val();
                if (to < from) {
                    $('#to_date').val("");
                    alert('to date should be grater than from date');
                }
            });
        })

    </script>
    <script>
        var loadFile = function (event) {
            var output = document.getElementById('output');
            output.src = URL.createObjectURL(event.target.files[0]);
        };
    </script>
<?php $__env->stopSection(); ?>

<?php echo $__env->make('layouts.admin', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH C:\wamp64\www\offerswing_test\resources\views/backend/offers/create.blade.php ENDPATH**/ ?>