<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class NewsFeed extends Model
{
    protected $fillable = [
        'title', 'slug', 'description','video_link','top','total_likes','total_shares'
    ];
    
    public function newsImages()
    {
        return $this->hasMany(NewsImage::class, 'news_id');
    }
}
