<?php

namespace App\Http\Controllers\Backend;

use App\Http\Controllers\Controller;
use App\Models\Category;
use App\Models\Claimed;
use App\Models\Deal;
use App\Models\Offer;
use App\Models\Setting;
use App\Models\Shop;
use App\Models\ShopOwner;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class AdminController extends Controller
{
    /**
     * Create a new controller instance.
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        if (1 == Auth::user()->user_type) {
            $offers = Offer::all();
            $offerscount = count($offers);

            $shops = Shop::all();
            $shopscount = count($shops);

            $categories = Category::all();
            $categorycount = count($categories);

            $users = User::getUsers();
            $userscount = count($users);

            return view(
                'backend.admin.home',
                compact(
                    'offers',
                    'offerscount',
                    'shops',
                    'shopscount',
                    'users',
                    'userscount',
                    'categories'
                )
            );
        }
        if (3 == Auth::user()->user_type) {
            $user = auth()->user();
            $user_id = $user['id'];

            $shopowner = $user->shops;

            foreach ($shopowner as $key => $value) {
                $shop_id[] = $value['id'];
            }

            foreach ($shop_id as $key => $value) {
                $shop[$key] = Shop::find($value);
                $shop[$key]['offers'] = Offer::leftjoin('shops', 'shops.id', '=', 'offers.shop_id')->where('offers.shop_id', '=', $value)->get();
                $shop[$key]['deals'] = Deal::leftjoin('shops', 'shops.id', '=', 'deals.shop_id')->where('deals.shop_id', '=', $value)->get();
                // $shop[$key]['claimed_deals'] = Claimed::select('*')
                //     ->leftjoin('deals', 'deals.id', '=', 'claimed.claimed_type_id')
                //     ->leftjoin('users', 'users.id', '=', 'claimed.user_id')
                //     ->leftjoin('shops', 'shops.id', '=', 'deals.shop_id')
                //     ->where('claimed.claimed_type', 'deal')
                //     ->where('shops.id', $shop_id)
                //     ->get()
                // ;
            }

            return view(
                'backend.admin.home',
                compact(
                    'shop'
                )
            );
        }
    }

    public function getUsers()
    {
        $users = User::getUsers();

        return view('backend.user.index', compact('users'));
    }

    public function getAdmins()
    {
        $users = User::getAdmins();

        return view('backend.user.index_admins', compact('users'));
    }

    public function getRetailers()
    {
        $users = User::getRetailers();

        return view('backend.user.index_admins', compact('users'));
    }

    public function profile()
    {
        $user = auth()->user();

        return view('backend.admin.profile', compact('user'));
    }

    public function settings()
    {
        $setting = Setting::all()->toArray();
        foreach ($setting as $item) {
            $settings[$item['name']] = $item['value'];
        }

        return view('backend.admin.settings', compact('settings'));
    }

    public function savesettings(Request $request)
    {
        foreach ($_POST as $key => $value) {
            $setting = Setting::where('name', $key)->first();

            if (!empty($setting)) {
                $setting->value = $value;
                $setting->save();
            }
        }

        return back()->with('alert', 'Settings Updated Sucessfully')->with('class', 'success');
    }
}
