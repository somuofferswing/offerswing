

<?php $__env->startSection('content'); ?>
<div class="content-wrapper">
    <section class="content-header">
        <h1>
            Users
            <small>All Users</small>
        </h1>
        <!-- <ol class="breadcrumb">
              <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
              <li class="active">Dashboard</li>
            </ol> -->
    </section>
    <section class="content">

        <div class="box box-primary">
            <div class="box-header with-border">
                <h3 class="box-title">All Users</h3>
            </div>
            <div class="table-responsive">
                <table class="table table-striped">
                    <thead>
                        <tr>
                            <th>ID</th>
                            <th>User type</th>
                            <th>First Name</th>
                            <th>Last Name</th>
                            <th>Mobile</th>
                            <th>Email</th>
                            <th colspan="2">Action</th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php $__currentLoopData = $users; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $user): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                        <tr>
                            <td><?php echo e($user->id); ?></td>
                            <td>
                                <?php if($user->user_type == 1): ?>
                                <b>Admin</b>
                                <?php endif; ?>

                                <?php if($user->user_type == 2): ?>
                                <b>User</b>
                                <?php endif; ?>

                                <?php if($user->user_type == 3): ?>
                                <b>Retailer</b>
                                <?php endif; ?>
                            </td>
                            <td><?php echo e($user->first_name); ?></td>
                            <td><?php echo e($user->last_name); ?></td>
                            <td><?php echo e($user->mobile); ?></td>
                            <td><?php echo e($user->email); ?></td>

                            <?php if($user->id != 1 || ($user->user_type == 'Admin' &&  $user->id != Auth::id())): ?>
                            <td><button class="btn btn-default"><a
                                        href="<?php echo e(url('/user/edit', [$user->id])); ?>">Edit</a></button></td>
                            
                            <td>
                                <?php if($user->status == 1): ?>
                                <form action="<?php echo e(url('/user/deactivateUser', [$user->id])); ?>" method="POST">
                                    <?php echo csrf_field(); ?>
                                    
                                    <button class="btn btn-warning" type="submit">Deactivate</button>
                                </form>

                                <?php else: ?>

                                <form action="<?php echo e(url('/user/activateUser', [$user->id])); ?>" method="POST">
                                    <?php echo csrf_field(); ?>
                                    
                                    <button class="btn btn-success" type="submit">Activate</button>
                                </form>

                                <?php endif; ?>

                            </td>
                            <td>
                                <form action="<?php echo e(url('/user/deleteuser', [$user->id])); ?>" method="POST"
                                    onSubmit="if(!confirm('Are you sure you want to delete User?')){return false;}">
                                    <?php echo csrf_field(); ?>
                                    
                                    <button class="btn btn-danger" type="submit">Delete</button>
                                </form>
                            </td>
                            <?php endif; ?>
                        </tr>
                        <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                    </tbody>
                </table>
            </div>
        </div>
    </section>
</div>
<?php $__env->stopSection(); ?>

<?php echo $__env->make('layouts.admin', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH /home/offerkrx/public_html/offerswing_test/resources/views/backend/user/allusers.blade.php ENDPATH**/ ?>