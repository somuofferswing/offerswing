@extends('layouts.app')



@section('content')

<style>

    .table-bordered td{

        padding:20px;

        font-weight:bold;

    }

</style>

<div class="ps-breadcrumb">

    <div class="container">

        <ul class="breadcrumb text-capitalize" id="breadcrumb">

            <li><a href="{{ url('/') }}">Home</a></li>

            <li>Wing Coins</li>

        </ul>

    </div>

</div>

<section>

    <div class="ps-page--blog">

        <div class="container">

            <div class="text-center pt-5">

                <img src="{{ asset('main/images/refer-coin.png') }}">

                <div class="ps-page__header">

                    <h1 class="text-center">Wing Coins : {{ $total_balance }}</h1>

                    <hr class="highlighter">

                </div>

            </div>

            

            <div class="row m-5">

                <div class="col-12 col-md-3 text-center mb-3">

                    <a href="{{ url('/referafriend')}}" class="ps-btn bg-primary text-white">Refer a Friend</a>

                </div>

                <div class="col-12 col-md-5 text-center mb-3">

                    <form action="{{ url('/token/redeem') }}" method="POST">

                        @csrf

                        <div class="row">

                            <input type="text" class="form-control col-8" placeholder="Enter your token code"

                                name="token_id" required>

                            <button class="ps-btn col-4" type="submit"> Submit</button>

                        </div>

                    </form>

                </div>

                <div class="col-12 col-md-2 text-center mb-3">

                    <a href="{{url('/redeem')}}" class="ps-btn bg-success text-white">Redeem</a>

                </div>

            </div>



            <div class="p-5">

                <h2 class="text-center">History</h2>

        

                <table class="table table-bordered">

                    <tbody>

                        @foreach ($transactions as $item)

                        @php
                            $meta = $item->meta;
                        @endphp

                        <tr>

                            <td>{{$meta['type']}}</td>

                            <td> @if ($item['type'] == 'withdraw') 
                                -                                
                            @endif {{$item['amount']}}</td>

                        </tr>

                        @endforeach

                    </tbody>

                </table>

            </div>

        </div>

    </div>

</section>

@endsection