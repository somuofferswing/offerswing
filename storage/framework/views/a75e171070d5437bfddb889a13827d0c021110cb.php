    <style>

        .categoryClass{

            text-align:center;

            padding-top:20px;

            color:var(--main-bg-color);

        }

      

    </style>

    <h3 class="categoryClass">Categories</h3>

    <ul class="menu--dropdown">

        <?php if($categories->isNotEmpty()): ?>

            <?php $__currentLoopData = $categories; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $key=>$item): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>

                <li>

                    <a href="<?php echo url('/subcategories',[$item->slug]);; ?>">

                        <img class="categoryImg" src="<?php echo e(asset('storage/'.$item->cat_image)); ?>" alt="">

                        <span class="categoryTitle"><?php echo e(ucwords(strtolower($item->category_name))); ?></span>

                    </a>

                </li>

            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>

        <?php else: ?>

            <li><a href="#">No Categories Found</a></li>

        <?php endif; ?>   

    </ul><?php /**PATH /home/offerkrx/public_html/offerswing_test/resources/views/layouts/mainSidebar.blade.php ENDPATH**/ ?>