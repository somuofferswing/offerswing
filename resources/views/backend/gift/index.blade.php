@extends('layouts.admin')

@section('content')
<div class="content-wrapper">
    <section class="content-header">
        <h1>
           Gifts
            <small>All Gifts</small>
        </h1>
        <!-- <ol class="breadcrumb">
              <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
              <li class="active">Dashboard</li>
            </ol> -->
    </section>
    <section class="content">
        @if(session()->get('success'))
        <div class="alert alert-success" id="success">
            {{ session()->get('success') }}
        </div><br />
        @endif

        <div class="box box-primary">
            <div class="box-header with-border">
                <h3 class="box-title">All Gifts</h3>
                {{-- <a href="{!! url('/gift/create'); !!}" class="pull-right btn btn-info">Add Gift</a> --}}
            </div>

            <div class="table-responsive">
                <table class="table table-striped">
                    <thead>

                        <tr>
                            <th>ID</th>
                            <th>Name</th>
                            <th>Description</th>
                            <th>Image</th>
                            <th>Points To claim</th>
                            <th>Quantity</th>
                            <th>Claims per user</th>
                            <th>Start Date</th>
                            <th>End Date</th>
                            <th colspan="2">Action</th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach($gifts as $gift)
                        <tr>
                            <td>{{$gift->id}}</td>
                            <td>{{$gift->gift_name}}</td>
                            <td>{!! $gift->description !!}</td>
                            <td><img src="<?php echo asset("$gift->image"); ?>" alt="image" height="50px" width="50px"></td>
                            <td>{{$gift->points_to_claim}}</td>
                            <td>{{$gift->quantity}}</td>
                            <td>{{$gift->claims_per_user}}</td>
                            <td>{{$gift->start_date}}</td>
                            <td>{{$gift->end_date}}</td>

                            <td><a class="btn btn-default" href="{{ route('edit-gift', [$gift->id]) }}">Edit</a></td>

                            <td>
                                <form action="{{ url('/gift/delete', [$gift->id])}}" method="post">
                                    @csrf
                                    @method('POST')
                                    <button class="btn btn-danger" type="submit">Delete</button>
                                </form>
                            </td>
                        </tr>
                        @endforeach
                    </tbody>
                </table>
            </div>
        </div>
    </section>
</div>
@endsection
@section('scripts')
<script>
    $('#success').delay(5000).fadeOut('slow');
    $('#reset').on('click',function(){
        // $("#filter")[0].reset();
        $('#shop_id').val('');
        $('#category_id').val('');
        $('#filter')[0].submit();
    });

</script>
@endsection
