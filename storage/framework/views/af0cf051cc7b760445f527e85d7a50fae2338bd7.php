<?php $__env->startSection('content'); ?>
<style>
    /* .mgin1 {
        background: white;
        -moz-box-shadow: 0 0 0px #888;
        -webkit-box-shadow: 0 0 0px#888;
        box-shadow: 0 0 0px #888;
        border-radius:5px;
    }

    .panel-body {
    padding: 0px;
} */
</style>

<div class="container">

    <nav aria-label="breadcrumb">
        <ol class="breadcrumb">
            <li class="breadcrumb-item"><a href="/">Home</a></li>
            <li class="breadcrumb-item active" aria-current="page">Events</li>
        </ol>
    </nav>
    <h3 style="margin-top:50px;" class="text-center border-bottom">Events</h3>

    <div class="row">

        <?php $__currentLoopData = $events; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $item): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
        <div class="col-md-6 my-3">
            <div class="card p-3">
                <a href="<?php echo e(url('eventdetails',[$item->id])); ?>">
                    <img class="card-img-top border-bottom" src='<?php echo asset("$item->event_image"); ?>'
                        alt="Card image cap" style="height:250px;">
                    <div class="card-body">
                        <a href="<?php echo e(url('eventdetails',[$item->id])); ?>" style="color:black;text-decoration:none;">
                            <h5 class="card-title" style="color:#df685d; font-weight:600;"> <?php echo e(ucfirst($item->title)); ?>

                            </h5>
                            <p class="card-text"> <?php echo $item->description; ?> <a
                                    href="<?php echo e(url('eventdetails',[$item->id])); ?>" style="color:#df685d; font-weight:600;">
                                    Read More</a></p>
                        </a>
                        <div class="row py-3">
                            <div class="col-md-6">
                                Time : <?php echo e($item->event_time); ?>

                            </div>

                            <div class="col-md-6 float-right" style="color:#df685d; font-weight:600;"
                                style="padding-right: 15px;">
                                <i class="fas fa-rupee-sign"></i> <?php echo e($item->event_price); ?> Onwards
                            </div>
                        </div>
                </a>
                <div class="row py-3 border-top">
                    <div class="col-md-2">
                        <?php if(Auth::guest()): ?>
                        <a class="btn" data-id="<?php echo e($item->id); ?>" data-toggle="modal" data-target="#myModal1"><i
                                class="far fa-heart" style="color:#e31b23"></i>&nbsp;Like</a>
                        <?php else: ?>
                        <?php if(in_array($item->id,$likes)): ?>
                        <a class="btn unlike" data-id="<?php echo e($item->id); ?>"><i class="fas fa-heart"
                                style="color:#e31b23"></i>&nbsp;Unlike</a>
                        <?php else: ?>
                        <a class="btn like" data-id="<?php echo e($item->id); ?>"> <i class="far fa-heart"
                                style="color:#e31b23"></i>&nbsp;like</a>
                        <?php endif; ?>

                        <?php endif; ?>

                    </div>
                    
                </div>
            </div>

        </div>
        <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
    </div>


</div>






<!-- <a class="btn btn-primary" >Trigger modal</a> -->
<div class="modal fade" id="share">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-body">
                <div class="a2a_kit a2a_kit_size_32 a2a_default_style">
                    
                    <a class="a2a_button_facebook"></a>
                    <a class="a2a_button_twitter"></a>
                    <a class="a2a_button_email"></a>
                    <a class="a2a_button_whatsapp"></a>
                </div>
                <script async src="https://static.addtoany.com/menu/page.js"></script>
            </div>
        </div>
    </div>
</div>



<?php $__env->stopSection(); ?>

<?php $__env->startSection('scripts'); ?>
<script>
    $(document).on('click', ".unlike", function () {
            var id = $(this).data('id');
            var type = "event";
            var element = this;

            var data = {
                    id:id,
                    type:type
                };
            $.ajax({
                    type: "post",
                    url: '<?php echo e(url("removelike")); ?>',
                    headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                     },
                    data: data,
                    cache: false,
                    success: function (data)
                    {
                        if(data.status == "success"){
                            $(element).removeClass('unlike').addClass('like').html('<i class="far fa-heart" style="color:#e31b23"></i> &nbsp;like');
                        }
                    },
                    error: function (data){
                        alert('Fail to run unlike..');
                    }
                });


        });
        $(document).on('click', ".like", function () {
            var id = $(this).data('id');
            var type = "event";
            var element = this;

            var data = {
                    id:id,
                    type:type
                };
            $.ajax({
                    type: "post",
                    url: '<?php echo e(url("addlike")); ?>',
                    headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                     },
                    data: data,
                    cache: false,
                    success: function (data)
                    {
                        console.log(data);
                        if(data.status == "success"){
                                $(element).removeClass('like').addClass('unlike').html('<i class="fas fa-heart" style="color:#e31b23"></i> &nbsp;Unlike');

                        }
                    },
                    error: function (data){
                        alert('Fail to run like..');
                    }
                });


        });
</script>


<?php $__env->stopSection(); ?>
<?php echo $__env->make('layouts.app', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH C:\wamp64\www\offers\resources\views/events.blade.php ENDPATH**/ ?>