@extends('layouts.admin')



@section('content')

<div class="content-wrapper">

    <section class="content-header">

        <h1>

            Dashboard

            <small>Control panel</small>

        </h1>

        <!-- <ol class="breadcrumb">

              <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>

              <li class="active">Dashboard</li>

            </ol> -->

    </section>



    @if (Auth::user()->user_type == 1)

    <section class="content">

        <div class="row">

            <div class="col-lg-3 col-xs-6">

                <div class="small-box bg-aqua">

                    <div class="inner">

                        <h3>{{ $shopscount }}</h3>



                        <p>Shops</p>

                    </div>

                    <div class="icon">

                        <i class="ion ion-bag"></i>

                    </div>

                    <a href="#" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a>

                </div>

            </div>

            <div class="col-lg-3 col-xs-6">

                <div class="small-box bg-green">

                    <div class="inner">

                        <h3>{{ $offerscount }}</h3>



                        <p>offers</p>

                    </div>

                    <div class="icon">

                        <i class="ion ion-stats-bars"></i>

                    </div>

                    <a href="#" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a>

                </div>

            </div>



            <div class="col-lg-3 col-xs-6">

                <div class="small-box bg-yellow">

                    <div class="inner">

                        <h3>{{ $userscount }}</h3>



                        <p>Users</p>

                    </div>

                    <div class="icon">

                        <i class="ion ion-person-add"></i>

                    </div>

                    <a href="#" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a>

                </div>

            </div>



            <div class="col-lg-3 col-xs-6">

                <div class="small-box bg-red">

                    <div class="inner">

                        {{-- <h3>{{ $offersexpiredcount }}</h3> --}}

                        <h3>25</h3>



                        <p>Orders</p>

                    </div>

                    <div class="icon">

                        <i class="ion ion-pie-graph"></i>

                    </div>

                    <a href="#" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a>

                </div>

            </div>

        </div>

        <div class="row">

            <div class="col-md-12">

                <div class="box">

                    <div class="box-header with-border">

                        <h3 class="box-title">Monthly Recap Report</h3>



                        <div class="box-tools pull-right">

                            <button type="button" class="btn btn-box-tool" data-widget="collapse"><i

                                    class="fa fa-minus"></i>

                            </button>

                            <div class="btn-group">

                                <button type="button" class="btn btn-box-tool dropdown-toggle" data-toggle="dropdown">

                                    <i class="fa fa-wrench"></i></button>

                                <ul class="dropdown-menu" role="menu">

                                    <li><a href="#">Action</a></li>

                                    <li><a href="#">Another action</a></li>

                                    <li><a href="#">Something else here</a></li>

                                    <li class="divider"></li>

                                    <li><a href="#">Separated link</a></li>

                                </ul>

                            </div>

                            <button type="button" class="btn btn-box-tool" data-widget="remove"><i

                                    class="fa fa-times"></i></button>

                        </div>

                    </div>

                    <!-- /.box-header -->

                    <div class="box-body">

                        <div class="row">

                            <div class="col-md-8">

                                <p class="text-center">

                                    <strong>Sales: 1 Jan, 2014 - 30 Jul, 2014</strong>

                                </p>



                                <div class="chart">

                                    <!-- Sales Chart Canvas -->

                                    <canvas id="salesChart" style="height: 180px;"></canvas>

                                </div>

                                <!-- /.chart-responsive -->

                            </div>

                            <!-- /.col -->

                            <div class="col-md-4">

                                <p class="text-center">

                                    <strong>Goal Completion</strong>

                                </p>



                                <div class="progress-group">

                                    <span class="progress-text">Add Products to Cart</span>

                                    <span class="progress-number"><b>160</b>/200</span>



                                    <div class="progress sm">

                                        <div class="progress-bar progress-bar-aqua" style="width: 80%"></div>

                                    </div>

                                </div>

                                <!-- /.progress-group -->

                                <div class="progress-group">

                                    <span class="progress-text">Complete Purchase</span>

                                    <span class="progress-number"><b>310</b>/400</span>



                                    <div class="progress sm">

                                        <div class="progress-bar progress-bar-red" style="width: 80%"></div>

                                    </div>

                                </div>

                                <!-- /.progress-group -->

                                <div class="progress-group">

                                    <span class="progress-text">Visit Premium Page</span>

                                    <span class="progress-number"><b>480</b>/800</span>



                                    <div class="progress sm">

                                        <div class="progress-bar progress-bar-green" style="width: 80%"></div>

                                    </div>

                                </div>

                                <!-- /.progress-group -->

                                <div class="progress-group">

                                    <span class="progress-text">Send Inquiries</span>

                                    <span class="progress-number"><b>250</b>/500</span>



                                    <div class="progress sm">

                                        <div class="progress-bar progress-bar-yellow" style="width: 80%"></div>

                                    </div>

                                </div>

                                <!-- /.progress-group -->

                            </div>

                            <!-- /.col -->

                        </div>

                        <!-- /.row -->

                    </div>

                    <!-- ./box-body -->

                    <div class="box-footer">

                        <div class="row">

                            <div class="col-sm-3 col-xs-6">

                                <div class="description-block border-right">

                                    <span class="description-percentage text-green"><i class="fa fa-caret-up"></i>

                                        17%</span>

                                    <h5 class="description-header">$35,210.43</h5>

                                    <span class="description-text">TOTAL REVENUE</span>

                                </div>

                                <!-- /.description-block -->

                            </div>

                            <!-- /.col -->

                            <div class="col-sm-3 col-xs-6">

                                <div class="description-block border-right">

                                    <span class="description-percentage text-yellow"><i class="fa fa-caret-left"></i>

                                        0%</span>

                                    <h5 class="description-header">$10,390.90</h5>

                                    <span class="description-text">TOTAL COST</span>

                                </div>

                                <!-- /.description-block -->

                            </div>

                            <!-- /.col -->

                            <div class="col-sm-3 col-xs-6">

                                <div class="description-block border-right">

                                    <span class="description-percentage text-green"><i class="fa fa-caret-up"></i>

                                        20%</span>

                                    <h5 class="description-header">$24,813.53</h5>

                                    <span class="description-text">TOTAL PROFIT</span>

                                </div>

                                <!-- /.description-block -->

                            </div>

                            <!-- /.col -->

                            <div class="col-sm-3 col-xs-6">

                                <div class="description-block">

                                    <span class="description-percentage text-red"><i class="fa fa-caret-down"></i>

                                        18%</span>

                                    <h5 class="description-header">1200</h5>

                                    <span class="description-text">GOAL COMPLETIONS</span>

                                </div>

                                <!-- /.description-block -->

                            </div>

                        </div>

                        <!-- /.row -->

                    </div>

                    <!-- /.box-footer -->

                </div>

                <!-- /.box -->

            </div>

            <!-- /.col -->

        </div>

        <div class="row">

            <div class="col-sm-6">

                <div class="box box-info">

                    <div class="box-header with-border">

                        <h3 class="box-title">Latest Orders</h3>



                        <div class="box-tools pull-right">

                            <button type="button" class="btn btn-box-tool" data-widget="collapse"><i

                                    class="fa fa-minus"></i>

                            </button>

                            <button type="button" class="btn btn-box-tool" data-widget="remove"><i

                                    class="fa fa-times"></i></button>

                        </div>

                    </div>

                    <!-- /.box-header -->

                    <div class="box-body">

                        <div class="table-responsive">

                            <table class="table no-margin">

                                <thead>

                                    <tr>

                                        <th>Order ID</th>

                                        <th>Item</th>

                                        <th>Status</th>

                                        <th>Popularity</th>

                                    </tr>

                                </thead>

                                <tbody>

                                    <tr>

                                        <td><a href="pages/examples/invoice.html">OR9842</a></td>

                                        <td>Call of Duty IV</td>

                                        <td><span class="label label-success">Shipped</span></td>

                                        <td>

                                            <div class="sparkbar" data-color="#00a65a" data-height="20">

                                                90,80,90,-70,61,-83,63

                                            </div>

                                        </td>

                                    </tr>

                                    <tr>

                                        <td><a href="pages/examples/invoice.html">OR1848</a></td>

                                        <td>Samsung Smart TV</td>

                                        <td><span class="label label-warning">Pending</span></td>

                                        <td>

                                            <div class="sparkbar" data-color="#f39c12" data-height="20">

                                                90,80,-90,70,61,-83,68

                                            </div>

                                        </td>

                                    </tr>

                                    <tr>

                                        <td><a href="pages/examples/invoice.html">OR7429</a></td>

                                        <td>iPhone 6 Plus</td>

                                        <td><span class="label label-danger">Delivered</span></td>

                                        <td>

                                            <div class="sparkbar" data-color="#f56954" data-height="20">

                                                90,-80,90,70,-61,83,63

                                            </div>

                                        </td>

                                    </tr>

                                    <tr>

                                        <td><a href="pages/examples/invoice.html">OR7429</a></td>

                                        <td>Samsung Smart TV</td>

                                        <td><span class="label label-info">Processing</span></td>

                                        <td>

                                            <div class="sparkbar" data-color="#00c0ef" data-height="20">

                                                90,80,-90,70,-61,83,63

                                            </div>

                                        </td>

                                    </tr>

                                    <tr>

                                        <td><a href="pages/examples/invoice.html">OR1848</a></td>

                                        <td>Samsung Smart TV</td>

                                        <td><span class="label label-warning">Pending</span></td>

                                        <td>

                                            <div class="sparkbar" data-color="#f39c12" data-height="20">

                                                90,80,-90,70,61,-83,68

                                            </div>

                                        </td>

                                    </tr>

                                    <tr>

                                        <td><a href="pages/examples/invoice.html">OR7429</a></td>

                                        <td>iPhone 6 Plus</td>

                                        <td><span class="label label-danger">Delivered</span></td>

                                        <td>

                                            <div class="sparkbar" data-color="#f56954" data-height="20">

                                                90,-80,90,70,-61,83,63

                                            </div>

                                        </td>

                                    </tr>

                                    <tr>

                                        <td><a href="pages/examples/invoice.html">OR9842</a></td>

                                        <td>Call of Duty IV</td>

                                        <td><span class="label label-success">Shipped</span></td>

                                        <td>

                                            <div class="sparkbar" data-color="#00a65a" data-height="20">

                                                90,80,90,-70,61,-83,63

                                            </div>

                                        </td>

                                    </tr>

                                </tbody>

                            </table>

                        </div>

                    </div>

                </div>

            </div>

            <div class="col-sm-6">

                <div class="box box-primary">

                    <div class="box-header with-border">

                        <h3 class="box-title">Recently Added Products</h3>



                        <div class="box-tools pull-right">

                            <button type="button" class="btn btn-box-tool" data-widget="collapse"><i

                                    class="fa fa-minus"></i>

                            </button>

                            <button type="button" class="btn btn-box-tool" data-widget="remove"><i

                                    class="fa fa-times"></i></button>

                        </div>

                    </div>

                    <!-- /.box-header -->

                    <div class="box-body">

                        <ul class="products-list product-list-in-box">

                            <li class="item">

                                <div class="product-img">

                                    <img src="dist/img/default-50x50.gif" alt="Product Image">

                                </div>

                                <div class="product-info">

                                    <a href="javascript:void(0)" class="product-title">Samsung TV

                                        <span class="label label-warning pull-right">$1800</span></a>

                                    <span class="product-description">

                                        Samsung 32" 1080p 60Hz LED Smart HDTV.

                                    </span>

                                </div>

                            </li>

                            <!-- /.item -->

                            <li class="item">

                                <div class="product-img">

                                    <img src="dist/img/default-50x50.gif" alt="Product Image">

                                </div>

                                <div class="product-info">

                                    <a href="javascript:void(0)" class="product-title">Bicycle

                                        <span class="label label-info pull-right">$700</span></a>

                                    <span class="product-description">

                                        26" Mongoose Dolomite Men's 7-speed, Navy Blue.

                                    </span>

                                </div>

                            </li>

                            <!-- /.item -->

                            <li class="item">

                                <div class="product-img">

                                    <img src="dist/img/default-50x50.gif" alt="Product Image">

                                </div>

                                <div class="product-info">

                                    <a href="javascript:void(0)" class="product-title">Xbox One <span

                                            class="label label-danger pull-right">$350</span></a>

                                    <span class="product-description">

                                        Xbox One Console Bundle with Halo Master Chief Collection.

                                    </span>

                                </div>

                            </li>

                            <!-- /.item -->

                            <li class="item">

                                <div class="product-img">

                                    <img src="dist/img/default-50x50.gif" alt="Product Image">

                                </div>

                                <div class="product-info">

                                    <a href="javascript:void(0)" class="product-title">PlayStation 4

                                        <span class="label label-success pull-right">$399</span></a>

                                    <span class="product-description">

                                        PlayStation 4 500GB Console (PS4)

                                    </span>

                                </div>

                            </li>

                            <!-- /.item -->

                        </ul>

                    </div>

                </div>

            </div>

        </div>

    </section>



    @else

    <section class="content">

        <div class="row">

            @foreach ($shop as $key=>$item)

            @php
                // echo "<pre>";
                // print_r($item);
                // die;
            @endphp

            <div class="col-md-6">

                <div class="box box-solid">

                    <div class="box-header with-border">

                        <h3 class="box-title">{{ $item->name }}</h3>

                    </div>

                    <!-- /.box-header -->

                    <div class="box-body">

                        <div id="carousel-example-generic" class="carousel slide" data-ride="carousel">

                            {{-- <ol class="carousel-indicators">

                                    <li data-target="#carousel-example-generic" data-slide-to="0" class="active"></li>

                                    <li data-target="#carousel-example-generic" data-slide-to="1" class=""></li>

                                    <li data-target="#carousel-example-generic" data-slide-to="2" class=""></li>

                                </ol> --}}

                            <div class="carousel-inner">

                                @foreach ($item->images as $key => $value)

                                <div class="item {{ ($key == 0) ? 'active' : '' }}">

                                    <img src="<?php echo asset("$value->image_path"); ?>" class="img-responsive">

                                </div>

                                @endforeach

                            </div>

                            <a class="left carousel-control" href="#carousel-example-generic" data-slide="prev">

                                <span class="fa fa-angle-left"></span>

                            </a>

                            <a class="right carousel-control" href="#carousel-example-generic" data-slide="next">

                                <span class="fa fa-angle-right"></span>

                            </a>

                        </div>

                    </div>

                    <div class="row">

                        <div class="col-md-6">

                            <div class=" box-solid">

                                <div class="box-header with-border">





                                    <h3 class="box-title">Address</h3>

                                </div>

                                <!-- /.box-header -->

                                <div class="box-body">

                                    <dl>

                                        <dt>Description</dt>

                                        <dd>{!! $item->shop_description !!}</dd>

                                        <dt>Address</dt>

                                        <dd>

                                            {!! $item->address !!}

                                            <br>

                                            {{ $item->city }}

                                            <br>

                                            {{ $item->state }}

                                            <br>

                                            {{ $item->pincode }}

                                        </dd>



                                    </dl>

                                </div>

                                <!-- /.box-body -->

                            </div>

                            <!-- /.box -->

                        </div>

                        <div class="col-md-6">

                            <div class=" box-solid">

                                <div class="box-header with-border">





                                    <h3 class="box-title">Location</h3>

                                </div>



                                <div class="box-body">

                                    <div class="map" id="map{{ $key }}" style="width:100%;height:150px"

                                        data-lat="{{ $item->lat }}" data-long="{{ $item->lng }}">



                                    </div>

                                </div>

                                <!-- /.box -->

                            </div>

                            <!-- ./col -->

                        </div>



                        <div class="row">

                            <div class="col-md-12">

                                <div class=" box-solid">

                                    <div class="box-header with-border">

                                        <a href="{{  url('/shopoffers',[$item->id]) }}" class="btn  btn-primary">Offers</a>

                                        <a href="{{ url('/shopdeals',[$item->id]) }}" class="btn  btn-success">Deals</a>

                                        {{-- <a href="#" class="btn  btn-info">Claims</a> --}}

                                    </div>



                                </div>



                            </div>

                        </div>

                        <!-- /.box-body -->

                    </div>

                    <!-- /.box -->

                </div>

            </div>

            @endforeach



        </div>







    </section>



    @endif



    @endsection

    @section('scripts')

    <script src="http://maps.googleapis.com/maps/api/js?key=AIzaSyBHLFZ4C3XB7MDwjywzpBC1ZWMH0u2zuE4&libraries=places"

        async defer>

    </script>



    <script>

        $(document).ready(function (){

        $('div.map').each(function(){

           var lat =  $(this).data('lat');

           var long =  $(this).data('long');



           var id = $(this).attr('id');



           var myLatLng = {lat: lat, lng: long};



            var map = new google.maps.Map(document.getElementById(id), {

            zoom: 12,

            center: myLatLng

            });



            var marker = new google.maps.Marker({

            position: myLatLng,

            map: map,

            title: 'Hello World!'

            });

        });



});

    </script>





    @endsection