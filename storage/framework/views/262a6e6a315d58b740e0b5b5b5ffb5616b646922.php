<!DOCTYPE html>
<html lang="<?php echo e(str_replace('_', '-', app()->getLocale())); ?>">

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="format-detection" content="telephone=no">
    <meta name="apple-mobile-web-app-capable" content="yes">
    <meta name="author" content="">
    <meta name="keywords" content="">
    <meta name="description" content="">
    <meta name="csrf-token" content="<?php echo e(csrf_token()); ?>">
    <title>Offerswing</title>
    <style>
      :root {
        --main-bg-color: #fe5347;  
      }
    </style>
    <link href="https://fonts.googleapis.com/css?family=Work+Sans:300,400,500,600,700&amp;amp;subset=latin-ext" rel="stylesheet">
    <link rel="stylesheet" href="<?php echo e(asset('/frontend/plugins/font-awesome/css/font-awesome.min.css')); ?>">
    <link rel="stylesheet" href="<?php echo e(asset('/frontend/fonts/Linearicons/Linearicons/Font/demo-files/demo.css')); ?>">
    <link rel="stylesheet" href="<?php echo e(asset('/frontend/plugins/bootstrap4/css/bootstrap.min.css')); ?>">
    <link rel="stylesheet" href="<?php echo e(asset('/frontend/plugins/owl-carousel/assets/owl.carousel.css')); ?>">
    <link rel="stylesheet" href="<?php echo e(asset('/frontend/plugins/slick/slick/slick.css')); ?>">
    <link rel="stylesheet" href="<?php echo e(asset('/frontend/plugins/lightGallery-master/dist/css/lightgallery.min.css')); ?>">
    <link rel="stylesheet" href="<?php echo e(asset('/frontend/plugins/jquery-bar-rating/dist/themes/fontawesome-stars.css')); ?>">
    <link rel="stylesheet" href="<?php echo e(asset('/frontend/plugins/jquery-ui/jquery-ui.min.css')); ?>">
    <link rel="stylesheet" href="<?php echo e(asset('/frontend/plugins/select2/dist/css/select2.min.css')); ?>">
    <link rel="stylesheet" href="<?php echo e(asset('/frontend/css/spinkit.min.css')); ?>">
    <link rel="stylesheet" href="<?php echo e(asset('/frontend/css/style.css')); ?>">
    <link rel="stylesheet" href="<?php echo e(asset('/frontend/css/current.css')); ?>">
    <!-- <script src="https://kit.fontawesome.com/d13f3c35d9.js" crossorigin="anonymous"></script> -->
    <script src="<?php echo e(asset('frontend/plugins/jquery-1.12.4.min.js')); ?>"></script>
    <script src="<?php echo e(asset('frontend/plugins/popper.min.js')); ?>"></script>
    <script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>
</head>
<body>
        <!-- header -->
        <?php echo $__env->make('layouts.mainheader', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>
        
        <!-- content -->
        <?php echo $__env->yieldContent('content'); ?>

        <!-- footer -->
        <?php echo $__env->make('layouts.mainfooter', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>

    <script src="<?php echo e(asset('frontend/plugins/owl-carousel/owl.carousel.min.js')); ?>"></script>
    <script src="<?php echo e(asset('frontend/plugins/bootstrap4/js/bootstrap.min.js')); ?>"></script>
    <script src="<?php echo e(asset('frontend/plugins/imagesloaded.pkgd.min.js')); ?>"></script>
    <script src="<?php echo e(asset('frontend/plugins/masonry.pkgd.min.js')); ?>"></script>
    <script src="<?php echo e(asset('frontend/plugins/isotope.pkgd.min.js')); ?>"></script>
    <script src="<?php echo e(asset('frontend/plugins/jquery.matchHeight-min.js')); ?>"></script>
    <script src="<?php echo e(asset('frontend/plugins/slick/slick/slick.min.js')); ?>"></script>
    <script src="<?php echo e(asset('frontend/plugins/jquery-bar-rating/dist/jquery.barrating.min.js')); ?>"></script>
    <script src="<?php echo e(asset('frontend/plugins/slick-animation.min.js')); ?>"></script>
    <script src="<?php echo e(asset('frontend/plugins/lightGallery-master/dist/js/lightgallery-all.min.js')); ?>"></script>
    <script src="<?php echo e(asset('frontend/plugins/jquery-ui/jquery-ui.min.js')); ?>"></script>
    <script src="<?php echo e(asset('frontend/plugins/sticky-sidebar/dist/sticky-sidebar.min.js')); ?>"></script>
    <script src="<?php echo e(asset('frontend/plugins/jquery.slimscroll.min.js')); ?>"></script>
    <script src="<?php echo e(asset('frontend/plugins/select2/dist/js/select2.full.min.js')); ?>"></script>
    <!-- custom scripts-->
    <script src="<?php echo e(asset('frontend/js/main.js')); ?>"></script>
    <script>
        $('.datepicker').datepicker({
            dateFormat: "yy-mm-dd",
            maxDate: new Date(),
            
        });
    </script>

</body>
</html>
<?php /**PATH C:\wamp64\www\offers\resources\views/layouts/app.blade.php ENDPATH**/ ?>