<?php $__env->startSection('content'); ?>
<link href="//maxcdn.bootstrapcdn.com/font-awesome/4.1.0/css/font-awesome.min.css" rel="stylesheet">
<style>
    .social:hover {
        -webkit-transform: scale(1.1);
        -moz-transform: scale(1.1);
        -o-transform: scale(1.1);
    }

    .social {
        -webkit-transform: scale(0.8);
        /* Browser Variations: */

        -moz-transform: scale(0.8);
        -o-transform: scale(0.8);
        -webkit-transition-duration: 0.5s;
        -moz-transition-duration: 0.5s;
        -o-transition-duration: 0.5s;
    }

    /*
    Multicoloured Hover Variations
*/

    #social-fb:hover {
        color: #3B5998;
    }

    #social-tw:hover {
        color: #4099FF;
    }

    #social-gp:hover {
        color: #d34836;
    }

    #social-em:hover {
        color: #f39c12;
    }
</style>

<div class="container">
    <nav aria-label="breadcrumb">
        <ol class="breadcrumb">
            <li class="breadcrumb-item"><a href="/">Home</a></li>
            <li class="breadcrumb-item active" aria-current="page">Refer a friend</li>
        </ol>
    </nav>
    <div class="col-md-10 offset-1 py-3 border">
        <div class="col-md-12">
            <img src="<?php echo e(asset('main/images/1_HD_Final_V5.png')); ?>" class="img-fluid" style="height:250px;width:100%" />
        </div>
        <div class="col-md-12 text-center ">

            <h2 class="btn btn-danger btn-block">ReferalCode:</h2>

            <h2><?php echo e($user['refid']); ?></h2>

            
            
            <br>
            <div class="sharethis-inline-share-buttons"></div>
            <script type="text/javascript"
                src="https://platform-api.sharethis.com/js/sharethis.js#property=5d8dd8b5981da200122534d4&product=inline-share-buttons"
                async="async"></script>
        </div>
    </div>
</div>
<?php $__env->stopSection(); ?>

<?php $__env->startSection('scripts'); ?>


<?php $__env->stopSection(); ?>

<?php echo $__env->make('layouts.app', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH C:\xampp\htdocs\offerswing\resources\views/referafriend.blade.php ENDPATH**/ ?>