<?php

namespace App\Http\Controllers;

use App\Models\Deal;
use App\Models\Offer;
use App\Models\Shop;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class SearchController extends Controller
{
    /**
     * Show the form for creating a new resource.
     *
     *
     *
     * @return \Illuminate\Http\Response
     */
    public function autocomplete(Request $request)
    {
        // $city = Session::get('city');

        $city = $request->session()->get('city');

        // DB::enableQueryLog();

        $data['shop'] = Shop::select('shop_name', 'id')

            ->where('shop_name', 'LIKE', "%{$request->input('query')}%")

            // ->where("city", $city)

            ->get()

            ->toArray()
        ;

        $data['offers'] = Offer::select('offer_name', 'offers.id')

            ->leftjoin('shops', 'shops.id', '=', 'offers.shop_id')

            ->where('offer_name', 'LIKE', "%{$request->input('query')}%")

            // ->where("shops.city", $city)

            ->get()->toArray();

        // $query = DB::getQueryLog();

        // $query = end($query);

        // dd($query);

        $data['deals'] = Deal::select('deals.name', 'deals.id')

            ->leftjoin('shops', 'shops.id', '=', 'deals.shop_id')

            ->where('name', 'LIKE', "%{$request->input('query')}%")

            // ->where("shops.city", $city)

            ->get()->toArray();

        var_dump($data);
        die;

        return response()->json($data);
    }

    /**
     * Show the form for creating a new resource.
     *
     *
     *
     * @return \Illuminate\Http\Response
     */
    public function searchpage(Request $request)
    {
        // $city = Session::get('city');

        $city = $request->session()->get('city');

        $data['shop'] = Shop::select('shop_name', 'id')

            ->where('shop_name', 'LIKE', "%{$request->input('query')}%")

            // ->where("city", $city)

            ->get()

            ->toArray()
        ;

        $data['offers'] = Offer::select('offer_name', 'offers.id')

            ->leftjoin('shops', 'shops.id', '=', 'offers.shop_id')

            ->where('offer_name', 'LIKE', "%{$request->input('query')}%")

            // ->where("shops.city", $city)

            ->get()->toArray();

        $data['deals'] = Deal::select('deals.name', 'deals.id')

            ->leftjoin('shops', 'shops.id', '=', 'deals.shop_id')

            ->where('name', 'LIKE', "%{$request->input('query')}%")

            // ->where("shops.city", $city)

            ->get()->toArray();

        // var_dump($data);die;

        return response()->json($data);
    }
}
