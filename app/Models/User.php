<?php

namespace App\Models;

use App\Http\Traits\HasWallet;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Illuminate\Support\Facades\DB;
use Laravel\Passport\HasApiTokens;

class User extends Authenticatable implements MustVerifyEmail
{
    use HasApiTokens;
    use Notifiable;
    use HasWallet;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'referrer_id','first_name', 'last_name', 'email', 'password', 'mobile', 'dob', 'gender', 'otp', 'firebse_id', 'reference_code', 'status', 'user_type', 'refid',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];

    /**
     * A user has a referrer.
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function referrer()
    {
        return $this->belongsTo(User::class, 'referrer_id', 'id');
    }

    /**
     * A user has many referrals.
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function referrals()
    {
        return $this->hasMany(User::class, 'referrer_id', 'id');
    }

    public static function getUsers()
    {
        return DB::table('users')->where('user_type', '=', '2')->get();
    }

    public static function getAdmins()
    {
        return DB::table('users')->where('user_type', '=', '1')->get();
    }

    public static function getRetailers()
    {
        return DB::table('users')->where('user_type', '=', '3')->get();
    }

    public function getPhoneNumber()
    {
        return (string) $this->mobile;
    }

    public function shops()
    {
        return $this->belongsToMany(Shop::class);
    }
}
