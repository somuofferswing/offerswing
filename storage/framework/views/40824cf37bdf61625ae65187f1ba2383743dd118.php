<?php $__env->startSection('content'); ?>
<style>
    ul.socialIcons {
        padding: 0;
        text-align: center;
    }

    .socialIcons li {
        background: yellow;
        list-style: none;
        display: inline-block;
        /* margin: 15px;
    margin-top: 15%; */
        border-radius: 2em;
        overflow: hidden;
    }

    .socialIcons li a {
        display: block;
        padding: .5em;
        max-width: 2.3em;
        min-width: 2.3em;
        height: 2.3em;
        white-space: nowrap;
        line-height: 1.5em;
        transition: .5s;
        text-decoration: none;
        font-family: arial;
        color: #fff;
    }

    .socialIcons li i {
        margin-right: .5em;
    }

    .socialIcons li:hover a {
        max-width: 200px;
        padding-right: 1em;
    }

    .socialIcons .facebook {
        background: #3b5998;
        box-shadow: 0 0 16px #3b5998;
    }

    .socialIcons .twitter {
        background: #00aced;
        box-shadow: 0 0 16px #00aced;
    }

    .socialIcons .instagram {
        background: #cd486b;
        box-shadow: 0 0 16px #cd486b;
    }

    .socialIcons .pinterest {
        background: #c92228;
        box-shadow: 0 0 16px #c92228;
    }

    .socialIcons .steam {
        background: #666666;
        box-shadow: 0 0 16px #666666;
    }
</style>


<div class="container">
    <div class="row">
        <div class="col-md-8 offset-2">
            <h3 style="color:#df685d; font-weight:bold;"><?php echo e(ucfirst($event->title)); ?></h3>
            <img src="<?php echo asset("$event->event_image"); ?>" alt="" class="img-fluid"
                style="margin:0 auto;display: block;height:250px;">
            <div class="row">
                <div class="col-md-6">
                    <i class="fas fa-calendar-alt"></i> <b> <?php echo e($event->event_date); ?></b><br />
                    <i class="far fa-clock"></i> <b> <?php echo e($event->event_time); ?></b>
                </div>
                <div class="col-md-6 text-right" style="color:#df685d; font-weight:600;">
                    <i class="fas fa-rupee-sign"></i> <?php echo e($event->event_price); ?> Onwards
                    <ul class="socialIcons" style="    position: absolute;top: 20px;border: 0px;right: 10px;top: 80%;">
                        <li class="facebook"><a href="#"><i class="fab fa-fw fa-facebook"></i>Facebook</a></li>
                        <li class="twitter"><a href="#"><i class="fab fa-fw fa-twitter"></i>Twitter</a></li>
                        <li class="instagram"><a href="#"><i class="fab fa-fw fa-instagram"></i>Instagram</a></li>
                    </ul>
                </div>
            </div>
            <div class="py-3">
                <ul class="nav nav-tabs" style="background: #fff;">
                    <li class="nav-item"><a class="nav-link active" data-toggle="tab" href="#about">About Event</a></li>
                    <li class="nav-item"><a class="nav-link" data-toggle="tab" href="#price">Price & Timings</a></li>
                    <li class="nav-item"><a class="nav-link" data-toggle="tab" href="#venue">Venue</a></li>
                    <li class="nav-item"><a class="nav-link" data-toggle="tab" href="#tc">Terms & Conditions</a></li>
                </ul>
                <div class="tab-content" style="background:white;">
                    <div id="about" class="tab-pane container active ">
                        <p><?php echo $event->description; ?></p>
                    </div>
                    <div id="price" class="tab-pane container fade">

                        <i class="fas fa-calendar-alt"></i> <b style="color:#2a74ab"> <?php echo e($event->event_date); ?></b><br />
                        <i class="far fa-clock"></i> <b style="color:#2a74ab"> <?php echo e($event->event_time); ?></b>
                        <p> <i class="fas fa-rupee-sign"></i> <?php echo e($event->event_price); ?> </p>
                        <br>

                    </div>
                    <div id="venue" class="tab-pane container fade">

                        <p><?php echo e($event->event_address); ?></p>
                        <br>
                        <div id="map" style="width:100%;height:450px"></div>
                    </div>
                    <div id="tc" class="tab-pane container fade">
                        <p><?php echo $event->terms_conditions; ?></p>
                    </div>
                </div>

            </div>



            <br>
            <div class="row">
                <div class="col-md-6">
                    <?php if(Auth::guest()): ?>
                    <a href="" class="btn btn-danger btn-block" data-toggle="" data-target="">Call Us</a>
                    <?php else: ?>
                    <a href="<?php echo e(url('getevent',[$event->id])); ?>" class="btn btn-danger btn-block"> Call Us</a>
                    <?php endif; ?>
                </div>
                <div class="col-md-6">
                    <?php if(Auth::guest()): ?>
                    <a href="" class="btn btn-danger btn-block" data-toggle="modal" data-target="#myModal1">Book
                        Now</a>
                    <?php else: ?>
                    <a href="<?php echo e(url('getevent',[$event->id])); ?>" class="btn btn-danger btn-block"> Book Now</a>
                    <?php endif; ?>
                </div>

            </div>

        </div>
    </div>
</div>

<!-- <a class="btn btn-primary" >Trigger modal</a> -->
<div class="modal fade" id="share">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-body">
                <div class="a2a_kit a2a_kit_size_32 a2a_default_style">
                    
                    <a class="a2a_button_facebook"></a>
                    <a class="a2a_button_twitter"></a>
                    <a class="a2a_button_email"></a>
                    <a class="a2a_button_whatsapp"></a>
                </div>
                <script async src="https://static.addtoany.com/menu/page.js"></script>
            </div>
        </div>
    </div>
</div>



<?php $__env->stopSection(); ?>

<?php $__env->startSection('scripts'); ?>

<script>
    $(document).on('click', ".unlike", function () {
            var id = $(this).data('id');
            var type = "event";
            var element = this;

            var data = {
                    id:id,
                    type:type
                };
            $.ajax({
                    type: "post",
                    url: '<?php echo e(url("removelike")); ?>',
                    headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                     },
                    data: data,
                    cache: false,
                    success: function (data)
                    {
                        if(data.status == "success"){
                            $(element).removeClass('unlike').addClass('like').html('<i class="far fa-heart" style="color:#e31b23"></i> &nbsp;like');
                        }
                    },
                    error: function (data){
                        alert('Fail to run unlike..');
                    }
                });


        });
        $(document).on('click', ".like", function () {
            var id = $(this).data('id');
            var type = "event";
            var element = this;

            var data = {
                    id:id,
                    type:type
                };
            $.ajax({
                    type: "post",
                    url: '<?php echo e(url("addlike")); ?>',
                    headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                     },
                    data: data,
                    cache: false,
                    success: function (data)
                    {
                        console.log(data);
                        if(data.status == "success"){
                                $(element).removeClass('like').addClass('unlike').html('<i class="fas fa-heart" style="color:#e31b23"></i> &nbsp;Unlike');

                        }
                    },
                    error: function (data){
                        alert('Fail to run like..');
                    }
                });


        });

        function initMap() {
            var lat =  <?php echo e($event->event_lat); ?>;
           var long =  <?php echo e($event->event_lng); ?>;
            var myLatLng = {lat: lat, lng: long};

            var map = new google.maps.Map(document.getElementById('map'), {
                zoom: 14,
                center: myLatLng
            });

            var marker = new google.maps.Marker({
                position: myLatLng,
                map: map,
                title: 'Hello World!'
            });
        }
</script>
<script
    src="http://maps.googleapis.com/maps/api/js?key=AIzaSyBHLFZ4C3XB7MDwjywzpBC1ZWMH0u2zuE4&libraries=places&callback=initMap"
    async defer>
</script>

<?php $__env->stopSection(); ?>

<?php echo $__env->make('layouts.app', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH C:\wamp64\www\offers\resources\views/single_event.blade.php ENDPATH**/ ?>