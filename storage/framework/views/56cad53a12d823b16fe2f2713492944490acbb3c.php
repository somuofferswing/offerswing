

<?php $__env->startSection('content'); ?>
<div class="content-wrapper">
    <section class="content-header">
        <h1>
           Events
            <small>All Events</small>
        </h1>
        <!-- <ol class="breadcrumb">
              <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
              <li class="active">Dashboard</li>
            </ol> -->
    </section>
    <section class="content">
        <?php if(session()->get('success')): ?>
        <div class="alert alert-success" id="success">
            <?php echo e(session()->get('success')); ?>

        </div><br />
        <?php endif; ?>

        <div class="box box-primary">
            <div class="box-header with-border">
                <h3 class="box-title">All Events</h3>
                <a href="<?php echo url('/events/create');; ?>" class="pull-right btn btn-info">Add Event</a>
            </div>

            <div class="table-responsive">
                <table class="table table-striped">
                    <thead>

                        <tr>
                            <th>ID</th>
                            <th>Title</th>
                            <th>Description</th>
                            <th>Image</th>
                            <th>Address</th>
                            <th>Date</th>
                            <th>Time</th>
                            <th colspan="2">Action</th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php $__currentLoopData = $events; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $event): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                        <tr>
                            <td><?php echo e($event->id); ?></td>
                            <td><?php echo e($event->title); ?></td>
                            <td><?php echo $event->description; ?></td>
                            <td><img src="<?php echo asset("$event->event_image"); ?>" alt="image" height="50px" width="50px"></td>
                            <td><?php echo e($event->event_address); ?></td>
                            <td><?php echo e($event->event_date); ?></td>
                            <td><?php echo e($event->event_time); ?></td>

                            <td><a class="btn btn-default" href="<?php echo e(route('edit-events', [$event->id])); ?>">Edit</a></td>

                            <td>
                                <form action="<?php echo e(route('delete-events', [$event->id])); ?>" method="post">
                                    <?php echo csrf_field(); ?>
                                    <?php echo method_field('DELETE'); ?>
                                    <button class="btn btn-danger" type="submit">Delete</button>
                                </form>
                            </td>
                        </tr>
                        <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                    </tbody>
                </table>
            </div>
        </div>
    </section>
</div>
<?php $__env->stopSection(); ?>
<?php $__env->startSection('scripts'); ?>
<script>
    $('#success').delay(5000).fadeOut('slow');
    $('#reset').on('click',function(){
        // $("#filter")[0].reset();
        $('#shop_id').val('');
        $('#category_id').val('');
        $('#filter')[0].submit();
    });

</script>
<?php $__env->stopSection(); ?>

<?php echo $__env->make('layouts.admin', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH /home/offerkrx/public_html/offerswing/resources/views/events/index.blade.php ENDPATH**/ ?>