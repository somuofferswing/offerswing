@extends('layouts.admin')

@section('content')
<div class="content-wrapper">
    <section class="content-header">
        <h1>
            Slider
            <small>Edit Slider</small>
        </h1>
        <!-- <ol class="breadcrumb">
              <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
              <li class="active">Dashboard</li>
            </ol> -->
    </section>

    <section class="content">
        <div class="box-body" style="padding: 10px 50px;">
            <form class="form-horizontal" method="POST" action="{{ url('/slider/store') }}"
                enctype="multipart/form-data">
                @csrf
                <div class="form-group">
                    <label for="slider_name" class="col-sm-2  control-label">Name</label>
                    <div class="col-sm-10">
                        <input id="slider_name" type="text" class="form-control" name="slider_name" value="" required
                            autocomplete="slider_name" autofocus>
                    </div>
                </div>

                <hr>
                <button id="addnew" type="button" class="btn btn-primary pull-right">Add New row</button>
                <div class="table-responsive">

                    <table class="table">
                        <tr>
                            <th>Name</th>
                            <th>Image</th>
                            <th>url(optional)</th>
                        </tr>


                        <tr>

                            <td><input type="text" name="slider_data[0][name]" value=""></td>
                            <td>
                                {{-- <input type="file" name="slider_data_image[]" onchange="loadFile(event)"" data-key="{{$key}}">
                                --}}
                                <input type="file" name="slider_data_image[]"
                                    onchange="document.getElementById('image0').src = window.URL.createObjectURL(this.files[0])"
                                    required>
                                <img id="image0" height="100px">
                            </td>
                            <td><input type="text" name="slider_data[0][url]" value=""></td>
                        </tr>

                    </table>
                </div>

                <div class="pull-right">
                        <button class="btn btn-info" type="submit">Create</button>
                        <button class="btn btn-danger" type="button">Cancel</button>
                    </div>
            </form>
        </div>

    </section>
</div>
@endsection
@section('scripts')

<script>
    var loadFile = function(event) {
    var reader = new FileReader();
    reader.onload = function(){
      var output = document.getElementById('output');
      output.src = reader.result;
    };
    reader.readAsDataURL(event.target.files[0]);
  };

  $('#addnew').on('click',function(){

    var count = $('table tr').length;
    var len = count-1;

            var markup = `<tr></tr><td><input type="text" name="slider_data[`+len+`][name]" value="" required></td>
                        <td>
                            <input type="file" name="slider_data_image[]" onchange="document.getElementById('image`+len+`').src = window.URL.createObjectURL(this.files[0])">
                                <img id="image`+len+`" height="100px" required>
                            </td>
                            <td><input type="text" name="slider_data[`+len+`][url]" value=""></td></tr>`;
            $("table tbody").append(markup);
  });
</script>

@endsection
