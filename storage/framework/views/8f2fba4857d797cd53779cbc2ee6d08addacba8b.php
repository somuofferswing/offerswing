<?php $__env->startSection('content'); ?>

<div class="container">
    <div class="col-md-10 offset-1 border py-3">
        <img src="<?php echo e(asset('main/images/1_HD_Final_V5.png')); ?>" class="img-responsive"
            style="height:250px;width:100%" />
        <h1 class="text-center" style="color:black"><?php echo e($total_wing_points->points); ?></h1>
        <div class="row py-3">
            <div class="col-md-3 text-center">
                <a href="<?php echo e(url('/referafriend')); ?>" class="btn btn-primary">Refer a Friend</a>
            </div>
            <div class="col-md-6 text-center">
                <form action="<?php echo e(url('/token/redeem')); ?>" method="POST">
                    <?php echo csrf_field(); ?>
                    <div class="row">
                        <input type="text" class="form-control col-md-8" placeholder="Enter your token code"
                            name="token_id">
                        <button class="btn btn-danger col-md-4" type="submit"> Submit</button>
                    </div>
                </form>
            </div>
            <div class="col-md-3 text-center">
                <a href="<?php echo e(url('/redeem')); ?>" class="btn btn-primary">Redeem</a>
            </div>

        </div>
        <div class="table-responsive">
            <h3 class="btn btn-primary btn-block">History</h3>
            
            <table class="table table-bordered">

                <tbody>
                    <?php $__currentLoopData = $wing_points; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $item): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                    <tr>
                        <td><?php echo e($item->type); ?></td>
                        <td><?php echo e($item->points); ?></td>
                    </tr>

                    <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                </tbody>
            </table>
        </div>
    </div>
</div>

<?php $__env->stopSection(); ?>

<?php echo $__env->make('layouts.app', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH C:\wamp64\www\offers\resources\views/wingcoins.blade.php ENDPATH**/ ?>