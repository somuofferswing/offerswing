@extends('layouts.admin')

@section('content')
<link rel="stylesheet"
    href="{{ asset('adminlte/bower_components/bootstrap-datepicker/dist/css/bootstrap-datepicker.min.css') }}">
<div class="content-wrapper">
    <section class="content-header">
        <h1>
            NewsFeed
            <small>Add News</small>
        </h1>
        <!-- <ol class="breadcrumb">
              <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
              <li class="active">Dashboard</li>
            </ol> -->
    </section>

    <section class="content">

        <div class="box box-info">
            <div class="box-header with-border">
                <h3 class="box-title">Add News</h3>
            </div>
            <form class="form-horizontal" method="POST" action="{{ url('/faq/store') }}" >
                @csrf
                <div class="box-body">
                    <div class="form-group ">
                        <label for="question" class="col-sm-2 control-label">Question</label>
                        <div class="col-sm-10">
                            <input id="question" type="text" class="form-control" name="question" value="{{ old('question') }}"
                                required>
                            @if ($errors->has('question'))
                            <div class="danger">{{ $errors->first('question') }}</div>
                            @endif
                        </div>
                    </div>
                    <div class="form-group ">
                        <label for="answer" class="col-sm-2 control-label">Answer</label>
                        <div class="col-sm-10">
                            <textarea id="answer" type="text" class="form-control" name="answer" required
                                >{{ old('answer') }}</textarea>
                            @if ($errors->has('answer'))
                            <div class="danger">{{ $errors->first('description') }}</div>
                            @endif
                        </div>
                    </div>

                    <div class="pull-right">
                            <button type="submit" class="btn btn-info">{{ __('Create') }}</button>
                                <button type="button" class="btn btn-danger" onclick="window.history.go(-1); return false;">{{ __('Cancel') }}</button>
                        </div>
</div>
</form>
</div>
</section>
</div>
@endsection
@section('scripts')
<script src="{{ asset('adminlte/bower_components/ckeditor/ckeditor.js') }}"></script>
<script>
    $(function () {

    CKEDITOR.replace('answer')
  })
</script>

@endsection
