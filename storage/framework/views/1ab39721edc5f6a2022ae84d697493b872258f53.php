    <footer class="ps-footer ps-footer--3">
        <div class="ps-container">
            <div class="ps-block--site-features ps-block--site-features-2">
                <div class="ps-block__item">
                    <div class="ps-block__left"><i class="icon-calendar-text"></i></div>
                    <div class="ps-block__right">
                        <h4>Create Event / Deal</h4>
                        <p>+91 9704 98 96 97</p>
                    </div>
                </div>
                <div class="ps-block__item">
                    <div class="ps-block__left"><i class="fa fa-whatsapp"></i></div>
                    <div class="ps-block__right">
                        <h4>Stay Connected</h4>
                        <p>with US</p>
                    </div>
                </div>
                <div class="ps-block__item">
                    <div class="ps-block__left"><i class="icon-bubbles"></i></div>
                    <div class="ps-block__right">
                        <h4>Follow Us</h4>
                        <ul class="ps-list--social mt-2">
                            <li>
                                <a class="facebook" href="https://www.facebook.com/Offerswing-378561272765031/" target="_blank" title="Facebook"><i class="fa fa-facebook"></i>
                                </a>
                            </li>
                            <li>
                                <a class="twitter" href="https://twitter.com/offerswing2" target="_blank" title="Twitter"><i class="fa fa-twitter"></i>
                                </a>
                            </li>
                            <li>
                                <a class="instagram" href="https://www.instagram.com/offerswing2019/" target="_blank" title="instagram"><i class="fa fa-instagram"></i>
                                </a>
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
            <div class="ps-footer__widgets p-0">
                <aside class="widget widget_footer widget_contact-us">
                    <h4 class="widget-title">Contact us</h4>
                    <div class="widget_content">
                        <p>Call us 24/7</p>
                        <h3>+91 97049 89697</h3>
                        <p>Srikakulam, Andhra Pradesh 532001 <br><a href="mailto:info@offerswing.com">info@offerswing.com</a></p>
                    </div>
                </aside>
                <aside class="widget widget_footer">
                    <h4 class="widget-title">Quick links</h4>
                    <ul class="ps-list--link">
                        <li><a href="<?php echo e(url('page',['privacy_policy'])); ?>">Privacy Policy</a></li>
                        <li><a href="<?php echo e(url('page',['terms_conditions'])); ?>">Terms & Conditions</a></li>
                        <li><a href="<?php echo e(url('/addyourbusiness')); ?>">Add Your Business</a></li>
                        <li><a href="<?php echo e(url('/faq')); ?>">FAQs</a></li>
                    </ul>
                </aside>
                <aside class="widget widget_footer">
                    <h4 class="widget-title">Company</h4>
                    <ul class="ps-list--link">
                        <li><a href="<?php echo e(url('/aboutus')); ?>">About us</a></li>
                        <li><a href="<?php echo e(url('/contactus')); ?>">Contact us</a></li>
                    </ul>
                </aside>
                <aside class="widget widget_footer">
                    <h4 class="widget-title">Download APP</h4>
                    <ul class="ps-list--link">
                        <li><a href="#"><img src="<?php echo e(asset('main/images/PlayStore.png')); ?>" style="height:40px"></a></li>
                        <li><a href="#"><img src="<?php echo e(asset('main/images/apple-app-store-icon.png')); ?>" style="height:45px"></a></li>
                    </ul>
                </aside>
            </div>
        </div>
    </footer>
    <style>
       
    </style>
    <div id="back2top"><i class="icon-chevron-up"></i></div>
    <div class="ps-site-overlay"></div>
    <div id="loader-wrapper">
        <div class="loader-section section-left"></div>
        <div class="loader-section section-right"></div>
    </div>

    <!-- login modal -->
    <div class="modal" id="myModal1" tabindex="-1" role="dialog">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
               
                <div class="modal-body">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <i class="fa fa-remove"></i>
                </button>
                <div class="ps-form--account ps-tab-root">
                    <ul class="ps-tab-list mb-3">
                        <li class="active"><a href="#sign-in">Login</a></li>
                        <li><a href="#register">Register</a></li>
                    </ul>
                    <div class="ps-tabs">

                        <!-- login tab -->
                        <div class="ps-tab active" id="sign-in">
                            <div class="ps-form__content">
                                <h5>Log In Your Account</h5>
                                <form id="loginform" class="form-horizontal" onsubmit="return LoginUser()">
                                <?php echo csrf_field(); ?>
                                <input type="hidden" id="login_user_type" name="user_type" value="2">
                                <div class="form-group">
                                    <input class="form-control" type="text" id="login_email" name="email" placeholder="Email *" required>
                                </div>
                                <div class="form-group form-forgot">
                                    <input class="form-control" type="password" id="login_password" name="password" placeholder="Password *" required>
                                    <a class="tabChange" href="#forgot-pwd">Forgot?</a>
                                </div>
                                <div class="form-group">
                                    <div class="ps-checkbox">
                                        <input class="form-control" type="checkbox" id="login-remember" name="remember">
                                        <label for="login-remember">Rememeber me</label>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <button class="ps-btn ps-btn--fullwidth" type="submit">Login</button>
                                </div>
                                </form>
                            </div>
                            <div class="ps-form__footer">
                                <p>Login with:</p>
                                <a href="#sign-in-otp" class="ps-btn bg-dark text-warning tabChange">OTP</a>
                            </div>
                        </div>

                        <!-- register tab -->
                        <div class="ps-tab" id="register">
                            <div class="ps-form__content">
                                <h5>Register An Account</h5>
                                <form id="signupform" method="POST" onsubmit="return SignupUser()">
                                    <?php echo csrf_field(); ?>
                                    <input type="hidden" id="register_user_type" name="user_type" value="2">
                                    <div class="form-group">
                                        <input class="form-control" type="text" id="register_firstname" name="first_name" placeholder="First Name *" required>
                                    </div>
                                    <div class="form-group">
                                        <input class="form-control" type="text" id="register_lastname" name="last_name" placeholder="Last Name *" required>
                                    </div>
                                    <div class="form-group">
                                        <input class="form-control" type="text" id="register_mobile" name="mobile" placeholder="Mobile *" required>
                                    </div>
                                    <div class="form-group">
                                        <input class="form-control" type="text" id="register_email" name="email" placeholder="Email *" required>
                                    </div>
                                    <div class="form-group">
                                        <input class="form-control datepicker" type="text" id="register_dob" name="dob" required placeholder="Date Of Birth *">
                                    </div>
                                    <div class="form-group">
                                        <div class="form-check form-check-inline ps-radio">
                                            <input class="form-check-input" type="radio" id="gender_male" name="register_gender" value="MALE" required>
                                            <label class="form-check-label" for="gender_male">Male</label>
                                        </div>
                                        <div class="form-check form-check-inline ps-radio">
                                            <input class="form-check-input" type="radio" name="register_gender" id="gender_female" value="FEMALE" required>
                                            <label class="form-check-label" for="gender_female">Female</label>
                                        </div>
                                        <div class="form-check form-check-inline ps-radio">
                                            <input class="form-check-input" type="radio" name="register_gender" id="gender_others" value="OTHERS" required>
                                            <label class="form-check-label" for="gender_others">Other</label>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <input class="form-control" type="password" id="register_password" name="password" placeholder="Password *" required>
                                    </div>
                                    <div class="form-group">
                                        <input class="form-control" type="password" id="register_password_confirm" name="password_confirmation" placeholder="Confirm Password *" required>
                                    </div>
                                    <div class="form-group">
                                        <input class="form-control" type="text" id="register_referal_code" name="referal_code" placeholder="Referal Code (Optional)">
                                    </div>
                                    <div class="form-group">
                                        <button class="ps-btn ps-btn--fullwidth" type="submit">Sign Up</button>
                                    </div>
                                </form>
                            </div>
                        </div>

                        <!-- sign-in otp tab -->
                        <div class="ps-tab" id="sign-in-otp">
                            <div class="ps-form__content">
                                <h5>Login with OTP</h5>
                                <form id="otpform" onsubmit="return SendOtp()" role="form">
                                    <?php echo csrf_field(); ?>
                                    <div class="form-group">
                                        <input class="form-control" type="number" id="otp_mobile" name="otp_mobile" placeholder="Mobile *" required>
                                    </div>
                                    <div class="form-group text-center">
                                        <button class="ps-btn" type="submit">Send OTP</button>
                                    </div>
                                </form>
                            </div>
                        </div>

                        <!-- verify otp tab -->
                        <div class="ps-tab" id="verify-otp">
                            <div class="ps-form__content">
                                <h5>Verify OTP</h5>
                                <form id="verifyotp" onsubmit="return Otpverify()">
                                    <?php echo csrf_field(); ?>
                                    <div class="form-group">
                                        <input class="form-control" type="text" id="otp" name="otp" placeholder="Please enter OTP *" required>
                                    </div>
                                    <div class="form-group">
                                        <button class="ps-btn" type="submit">Verify OTP</button>
                                        <button type="button" id="resendotp" class="ps-btn bg-dark text-warning" disabled onClick="resendOTP()">Resend OTP</button>
                                    </div>
                                </form>
                            </div>
                        </div>

                        <!-- reset password tab -->
                        <div class="ps-tab" id="forgot-pwd">
                            <div class="ps-form__content">
                                <h5>Reset Password</h5>
                                <form id="forgotpwd" onsubmit="return forgotPassword()">
                                    <?php echo csrf_field(); ?>
                                    <div class="form-group">
                                        <input class="form-control" type="text" id="reset_email" name="reset_email" placeholder="Email *" required>
                                    </div>
                                    <div class="form-group text-center">
                                        <button class="ps-btn " type="submit">Send Password Reset Link</button>
                                    </div>
                                </form>
                            </div>
                        </div>
                        

                    </div>
                </div>
                
                </div>
            </div>
        </div>
    </div>

    <script>
    $(document).ready(function() {
        
        $('#login_modal').click(function(e){
            $("ul.ps-tab-list li.active, .ps-tab").removeClass("active");
            $('#sign-in').addClass("active");
            $('a[href="#sign-in"]').parent("li").addClass("active");
        });

        $('#register_modal').click(function(e){
            $("ul.ps-tab-list li.active, .ps-tab").removeClass("active");
            $('#register').addClass("active");
            $('a[href="#register"]').parent("li").addClass("active");
        });

        $('a.tabChange').click(function(e){
            e.preventDefault();

            let id = $(this).attr('href');
            $("ul.ps-tab-list li.active, .ps-tab").removeClass("active");
            $(id).addClass("active");
        });
    });
   
    </script>
    <script>
    function LoginUser()
        {
            var email    = $("#login_email").val();
            var user_type    = $("#login_user_type").val();
            var password = $("#login_password").val();
            var data = {
                email:email,
                password:password,
                user_type: user_type
            };
            console.log(data);
            // Ajax Post
            $.ajax({
                type: "post",
                url: '<?php echo e(url("customlogin")); ?>',
                headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                 },
                data: data,
                cache: false,
                success: function (data)
                {
                    console.log(data);
                    if(data.status == "success"){
                        
                      location.reload();

                    }else{
                        console.log(data);
                        alert('Please!check Your credentials');
                        console.log('login request sent !');
                        console.log('status: ' +data.status);
                        console.log('message: ' +data.message);
                    }
                },
                error: function (data){

                    console.log(data);
                    console.log(data.responseJSON.errors);
                    var errors = data.responseJSON.errors;

                    // if(errors.mobile){
                    //     $('#mobile_error').addClass('text-danger');
                    //     $('#mobile_error').html(errors.mobile[0]);
                    // }

                    // if(errors.email){
                    //     $('#login_email_error').addClass('text-danger');
                    //     $('#login_email_error').html(errors.email[0]);
                    // }

                    // if(errors.password){
                    //     $('#login_password_error').addClass('text-danger');
                    //     $('#login_password_error').html(errors.password[0]);
                    // }
                }
            });
            return false;
        }

        function SignupUser()
        {
            // next code goes here...
            var first_name       = $("#register_firstname").val();
            var last_name       = $("#register_lastname").val();
            var email       = $("#register_email").val();
            var password       = $("#register_password").val();
            var mobile       = $("#register_mobile").val();
            var dob       = $("#register_dob").val();
            var gender       = $("input[name='register_gender']:checked").val();
            var referal_code       = $("#register_referal_code").val();
            var user_type       = $("#register_user_type").val();
            var password_confirmation       = $("#register_password_confirm").val();
            // garnish the data
            var data = {
                first_name:first_name,
                last_name:last_name,
                email:email,
                password:password,
                referal_code:referal_code,
                user_type:user_type,
                password_confirmation:password_confirmation,
                mobile:mobile,
                dob:dob,
                gender:gender
            };
            // ajax post
            $.ajax({
                type: "post",
                url: '<?php echo e(url("customregister")); ?>',
                headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                 },
                data: data,
                cache: false,
                success: function (data){
                    console.log(data);

                    message = data.message;
                    if(data.status == "Success"){
                        $('#sigupstatus').addClass("alert-success").text(message);
                        location.reload();
                    }else{
                        $('#otpStatus').addClass("alert-danger").text(message);
                        console.log(data);
                    }

                    // $('#otpverify').show();
                    // $('#signupbox').hide();
                    // $('#loginbox').hide();

                    // location.reload();
                },
                error: function (data){
                    // console.log(data);
                    // console.log(data.responseJSON.errors);

                    var errors = data.responseJSON.errors;

                    if(errors.mobile){
                        $('#mobile_error').addClass('text-danger');
                        $('#mobile_error').html(errors.mobile[0]);
                    }

                    if(errors.email){
                        $('#email_error').addClass('text-danger');
                        $('#email_error').html(errors.email[0]);
                    }

                    if(errors.password){
                        $('#password_error').addClass('text-danger');
                        $('#password_error').html(errors.password[0]);
                    }
                }
            });
            return false;
        }


            var sec = 59;
            var myTimer = document.getElementById('myTimer');
            var myBtn = document.getElementById('resendotp');

            function countDown() {
            if (sec < 10) {
                myTimer.innerHTML = "00:0" + sec;
            } else {
                myTimer.innerHTML = "00:"+sec;
            }
            if (sec <= 0) {
                $("#resendotp").removeAttr("disabled");
                $("#myTimer").fadeTo(2500, 0);
                return;
            }
            sec -= 1;
            window.setTimeout(countDown, 1000);
            }



            function resendOTP()
        {
            var mobile    = $("#otp_mobile").val();

            var data = {
                mobile:mobile
            };
            // Ajax Post
            $.ajax({
                type: "post",
                url: '<?php echo e(url("resendOTP")); ?>',
                headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                 },
                data: data,
                cache: false,
                success: function (data)
                {
                    message = data.message;
                    if(data.status == "Success"){
                        $('#otpStatus').addClass("alert-success").text(message);
                        $('#otp_mobile').attr('disabled','disabled');

                        $('#sendotptomobile').hide();
                        $('#verifyotp').show();
                        countDown();

                    }else{
                        $('#otpStatus').addClass("alert-danger").text(message);
                        console.log(data);
                    }
                },
                error: function (data){

                    console.log(data);
                    console.log(data.responseJSON.errors);
                    var errors = data.responseJSON.errors;

                    // if(errors.mobile){
                    //     $('#mobile_error').addClass('text-danger');
                    //     $('#mobile_error').html(errors.mobile[0]);
                    // }

                    if(errors.email){
                        $('#login_email_error').addClass('text-danger');
                        $('#login_email_error').html(errors.email[0]);
                    }

                    if(errors.password){
                        $('#login_password_error').addClass('text-danger');
                        $('#login_password_error').html(errors.password[0]);
                    }
                }
            });
            return false;
        }



        function SendOtp()
        {
            var mobile    = $("#otp_mobile").val();

            var data = {
                mobile:mobile
            };
            // Ajax Post
            $.ajax({
                type: "post",
                url: '<?php echo e(url("loginSendOtp")); ?>',
                headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                 },
                data: data,
                cache: false,
                success: function (data)
                {
                    message = data.message;
                    if(data.status == "Success"){
                        console.log(data);
                        $('#otpStatus').addClass("alert-success").text(message);
                        var mobile = $('#otp_mobile').val();
                        $('#otp_mobile').attr('disabled','disabled');

                        $('#sendotptomobile').hide();
                        $('#verifyotp').show();
                        countDown();

                    }else{
                        $('#otpStatus').addClass("alert-danger").text(message);
                        console.log(data);
                    }
                },
                error: function (data){

                    console.log(data);
                    console.log(data.responseJSON.errors);
                    var errors = data.responseJSON.errors;

                    // if(errors.mobile){
                    //     $('#mobile_error').addClass('text-danger');
                    //     $('#mobile_error').html(errors.mobile[0]);
                    // }

                    if(errors.email){
                        $('#login_email_error').addClass('text-danger');
                        $('#login_email_error').html(errors.email[0]);
                    }

                    if(errors.password){
                        $('#login_password_error').addClass('text-danger');
                        $('#login_password_error').html(errors.password[0]);
                    }
                }
            });
            return false;
        }


        function Otpverify()
        {
            // next code goes here...
            var otp  = $("#otp").val();

            var data = {
                code:otp,
            };
            // ajax post
            $.ajax({
                type: "post",
                url: '<?php echo e(url("verifyotp")); ?>',
                headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                 },
                data: data,
                cache: false,
                success: function (data){

                    console.log(data);
                    var message = data.message;
                    if(data.status == "Success"){
                    $('#otpStatus').addClass("alert-success").text(message);
                    location.reload();
                    }else{
                    $('#otpStatus').addClass("alert-danger").text(message);
                    }
                },
                error: function (data){
                    console.log(data);
                    console.log(data.responseJSON.errors);

                    // var errors = data.responseJSON.errors;
                }
            });
            return false;
        }

        function forgotPassword()
        {
            // next code goes here...
            var email  = $("#reset_email").val();

            var data = {
                email:email
            };
            $.ajax({
                type: "post",
                url: '<?php echo e(url("customforgotpassword")); ?>',
                headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                 },
                data: data,
                cache: false,
                success: function (data){
                    console.log(data);
                    var message = data.msg;
                    if(data.status == "Success"){

                    $('#reset_status').addClass("alert-success").text(message);

                    }else{
                        $('#reset_status').addClass("alert-danger").text(message);
                    }
                },
                error: function (data){
                    console.log(data);
                    console.log(data.responseJSON.errors);

                    // var errors = data.responseJSON.errors;
                }
            });
            return false;
        }
</script>

<!-- alertmodel -->
<div class="modal fade" id="modal-alert">
    <div class="modal-dialog modal-dialog-centered">
        <div class="modal-content">
            <div class="modal-body">
                <p id="alert_message" class="alert alert-<?php echo e(session('class')); ?>">
                    <?php echo e(session('alert')); ?>

                </p>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
            </div>
        </div>
    </div>
</div>
<!-- alertmodel --><?php /**PATH C:\xampp\htdocs\offerswing\resources\views/layouts/mainfooter.blade.php ENDPATH**/ ?>