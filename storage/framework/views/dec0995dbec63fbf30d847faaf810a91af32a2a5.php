<?php $__env->startSection('content'); ?>
<style>
    #map {
        /* overflow: inherit !important; */

        height: 240px;
        /* box-sizing: border-box;
     padding: 0;
     margin: 0; */
    }

    .mgin1 {
        background: white;
        -moz-box-shadow: 0 0 0px #888;
        -webkit-box-shadow: 0 0 0px#888;
        box-shadow: 0 0 0px #888;
    }

    .mgin2 {

        -moz-box-shadow: 0 0 0px #888;
        -webkit-box-shadow: 0 0 0px#888;
        box-shadow: 0 0 0px #888;
    }

    .fadd p {
        /* padding-bottom: 10px; */
        margin-bottom: 14px;
    }





    ul.socialIcons {
        padding: 0;
        text-align: center;
    }

    .socialIcons li {
        background: yellow;
        list-style: none;
        display: inline-block;
        /* margin: 15px;
    margin-top: 15%; */
        border-radius: 2em;
        overflow: hidden;
    }

    .socialIcons li a {
        display: block;
        padding: .5em;
        max-width: 2.3em;
        min-width: 2.3em;
        height: 2.3em;
        white-space: nowrap;
        line-height: 1.5em;
        transition: .5s;
        text-decoration: none;
        font-family: arial;
        color: #fff;
    }

    .socialIcons li i {
        margin-right: .5em;
    }

    .socialIcons li:hover a {
        max-width: 200px;
        padding-right: 1em;
    }

    .socialIcons .facebook {
        background: #3b5998;
        box-shadow: 0 0 16px #3b5998;
    }

    .socialIcons .twitter {
        background: #00aced;
        box-shadow: 0 0 16px #00aced;
    }

    .socialIcons .instagram {
        background: #cd486b;
        box-shadow: 0 0 16px #cd486b;
    }

    .socialIcons .pinterest {
        background: #c92228;
        box-shadow: 0 0 16px #c92228;
    }

    .socialIcons .steam {
        background: #666666;
        box-shadow: 0 0 16px #666666;
    }




    /* .nav-tabs>li.active>a {

        cursor: default;
        background-color: #df685d !important;
        border: 1px solid #ddd;
        border-bottom-color: transparent;
        color: white !important;
    } */

    .nav-tabs>li.active>a {
        cursor: default;
        background-color: #eee !important;
        border: 1px solid #ddd;
        border-bottom-color: transparent;
        color: #171616 !important;
        border-top: 3px solid #df685d !important;
    }

    .nav>li>a {
        position: relative;
        display: block;
        padding: 15px 20px;

    }

    body {

        background-color: #e1e9ec
    }

    .thumbnail {
        display: block;
        padding: 4px;
        margin-bottom: 20px;
        line-height: 1.42857143;
        background-color: #fff;
        border: 0px solid #ddd;
        border-radius: 4px;
        -webkit-transition: border .2s ease-in-out;
        -o-transition: border .2s ease-in-out;
        transition: border .2s ease-in-out;
    }
</style>


<body>
    <div class="container" style="margin-top:50px;">
        <?php if(session('alert')): ?>
        <div class="alert alert-<?php echo e(session('class')); ?>">
            <?php echo e(session('alert')); ?>

        </div>
        <?php endif; ?>

        <div class="row mgin1" style="padding:10px;">
            <div class="col-md-4 ">
                
                <h2><?php echo e($shops->shop_name); ?>

                    <span class="pull-right">
                        <?php if(Auth::guest()): ?>
                        <a class="btn" data-id="<?php echo e($shops->id); ?>" data-toggle="modal" data-target="#myModal1"><i class="far fa-heart"
                            style="color:#e31b23"></i></a>
                        <?php else: ?>
                        <?php if(!empty($likes)): ?>
                        <a class="btn unlike" data-id="<?php echo e($shops->id); ?>"><i class="fas fa-heart"
                                style="color:#e31b23"></i></a>
                        <?php else: ?>
                        <a class="btn like" data-id="<?php echo e($shops->id); ?>"> <i class="far fa-heart"
                                style="color:#e31b23"></i></a>
                        <?php endif; ?>

                        <?php endif; ?>
                    </span></h2>
                <h5><b><span><img src="<?php echo e(asset('main/images/phoneicon.png')); ?>" alt="" style="height:20px;margin-right:5px;"></span>
                    </b>
                    <span><?php echo e($shops->mobile); ?></span></h5>
                <h5><b><span><img src="<?php echo e(asset('main/images/whatsapp.png')); ?>" alt="" style="height:20px;margin-right:5px;"></span></b>
                    <span><?php echo e($shops->phone_number_2); ?>8099156899</span></h5>
                <h5><b><span><img src="<?php echo e(asset('main/images/mailicon 2.png')); ?>" alt=""
                                style="height:15px;margin-right:5px;"></span></b>
                    <span><?php echo e($shops->email); ?></span></h5>
                <h5>

                    <?php for($i = 0; $i < 5; $i++): ?> <?php if($i< (int)$shops->shop_rating): ?>
                        <span class="fas fa-star" style="color:#df685d"></span>
                        <?php else: ?>
                        <?php if(($shops->shop_rating-(int)$shops->shop_rating) == 0.5 && ((int)$shops->shop_rating) ==
                        $i ): ?>
                        <span class="fas fa-star-half-alt" style="color:#df685d"></span>
                        <?php else: ?>
                        <span class="far fa-star" style="color:#df685d"></span>
                        <?php endif; ?>
                        <?php endif; ?>
                        <?php endfor; ?>
                </h5>
                <ul class="socialIcons" style="    position: absolute;
                top: 20px;
                border: 0px;
                /* left: 23px; */
                right: 10px;
                top: 80%;">
                    <li class="facebook"><a href="#"><i class="fab fa-fw fa-facebook"></i>Facebook</a></li>
                    <li class="twitter"><a href="#"><i class="fab fa-fw fa-twitter"></i>Twitter</a></li>
                    <li class="instagram"><a href="#"><i class="fab fa-fw fa-instagram"></i>Instagram</a></li>
                </ul>
            </div>


            <div class="col-md-4 ">
                <?php
                $image = $shops->shop_images
                ?>
                <div id="myCarousel" class="carousel slide " data-ride="carousel">
                    <ol class="carousel-indicators">
                        <?php $__currentLoopData = $image; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $indexKey =>$item): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                        <li data-target="#myCarousel" data-slide-to="<?php echo e($indexKey+1); ?>"
                            class="<?php echo e(($indexKey == 0) ? 'active' : ''); ?>"></li>


                        <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                    </ol>

                    <div class="carousel-inner">
                        <?php if($image): ?>
                        <?php $__currentLoopData = $image; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $indexKey =>$item): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>

                        <?php if($item->type == 'shop_image'): ?>
                        <div class="item <?php echo e(($indexKey == 0) ? 'active' : ''); ?>">

                            <img src="<?php echo asset("$item->image_path"); ?>" alt="Los Angeles"
                                style="width:100%;max-height:240px; height:240px;">
                        </div>
                        <?php endif; ?>
                        <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                        <?php else: ?>
                        No image

                        <?php endif; ?>

                    </div>

                    <a class="left carousel-control" href="#myCarousel" data-slide="prev">
                        <span class="glyphicon glyphicon-chevron-left"></span>
                        <span class="sr-only">Previous</span>
                    </a>
                    <a class="right carousel-control" href="#myCarousel" data-slide="next">
                        <span class="glyphicon glyphicon-chevron-right"></span>
                        <span class="sr-only">Next</span>
                    </a>
                </div>
            </div>
            <div class="col-md-4 ">
                <div id="map"></div>
            </div>

            <div class="col-md-12 ">
                <hr style="border:1px solid #a4a4a4;">
            </div>



            <!-- <div class="col-md-0 hhi thumbnail " >



            <div class="">

                <a href="">
                    <img src="<?php echo e(asset('main/images/20.jpeg')); ?>" alt="" style="height:150px;  width: 300px; " >


                </a>
            </div>
           </div>-->



            <div class="col-md-6 hhi" style="border-right: 1px solid #a4a4a4; ">
                <div class="  fadd" style="padding:0px 8px;">
                    <h4 style="text-align:center;font-weight:800;border-bottom:1px dotted #ccc;">Full Address:</h4>
                    <p style="font-size:16px;">
                        <?php echo e($shops->shop_name); ?>,<?php echo e($shops->address); ?>,<?php echo e($shops->city); ?>,<?php echo e($shops->state); ?>,<?php echo e($shops->pincode); ?>

                    </p>


                </div>
            </div>
            <div class="col-md-6 hhi col-xs-12 ">

                <h4 style="text-align:center;font-weight:800;border-bottom:1px dotted #ccc;">Timings</h4>
                <p style="text-align:center;"> Opened</p>

                <!-- <?php
            $active_days = explode(",",$shops->active_days);;
            ?>
            <?php $__currentLoopData = $days; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $item): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
            <p style="margin-bottom:15px;">
                <div class="row">
                    <div class="col-md-2 col-xs-3 timeslot">
                        <span style="text-align: left"><?php echo e(substr($item->name,0,3)); ?></span> <span style="float:right;">:
                        </span>
                    </div>
                    <div class="col-md-10 col-xs-9 timeslot">
                        <?php if(in_array( $item->id , $active_days)): ?>
                        <?php echo e(date('h:i A',strtotime($shops->start_time))); ?> -
                        <?php echo e(date('h:i A',strtotime($shops->end_time))); ?>

                        <?php else: ?>
                        Closed
                        <?php endif; ?>
                    </div>
                </div>


            </p>
            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>-->
            </div>


        </div>



        <div class="row mgin1" style="margin-top:20px;">
            <div class="col-md-12 tabsection" style="padding:0;">
                <ul class="nav nav-tabs" style="background: #fff;">
                    <li class="active"><a data-toggle="tab" href="#home">Offers</a></li>
                    <li><a data-toggle="tab" href="#menu1">About us</a></li>
                    <li><a data-toggle="tab" href="#menu2">Wing Deals</a></li>
                    <li><a data-toggle="tab" href="#menu3" <?php if($shops->menus->isEmpty()): ?>
                            style="display:none;" <?php endif; ?>>Menus</a></li>
                </ul>

                <div class="tab-content">
                    <div id="home" class="tab-pane fade in active " style="padding:20px;">

                        <?php if($shops->offersOngoing->isNotEmpty()): ?>
                        <?php $__currentLoopData = $shops->offersOngoing; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $item): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>

                        <div class="panel panel-default">
                            <div class="panel-body">
                                <h3 style="color: #df685d;"><?php echo e(strtoupper($item->offer_name)); ?></h3>
                                <div class="row">

                                    <img src="<?php echo asset("$item->offer_image"); ?>" class="img-responsive"
                                        style="height:200px;width:auto;padding:10px">

                                    <h5><b>Description</b></h5>

                                    <?php echo $item->description; ?>



                                    <h5><b>Offer Dates :</b> </h5>

                                    <?php echo e($item->from_date); ?> to <?php echo e($item->to_date); ?>


                                </div>
                            </div>
                        </div>

                        <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>

                        <?php else: ?>

                        <h5>No Offers </h5>

                        <?php endif; ?>


                    </div>

                    <div id="menu1" class="tab-pane fade " style="padding:20px;">
                        <h3 style="color: #df685d;"><?php echo e(strtoupper($shops->shop_name)); ?></h3>
                        <p><?php echo $shops->shop_description; ?></p>
                        <h3 style="color: #df685d;">Phone No.</h3>
                        <p>+91 <?php echo e($shops->phone_number); ?></p>

                        <p>
                            <h3>Timings</h3>
                        </p>
                        <?php
                        $active_days = explode(",",$shops->active_days);;
                        ?>
                        <?php $__currentLoopData = $days; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $item): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                        <p style="margin-bottom:15px;">
                            <div class="row">
                                <div class="col-md-2 col-xs-3 timeslot">
                                    <span
                                        style="text-align: left"><?php echo e(($item->id== 5) ? substr($item->name,0,4) : substr($item->name,0,3)); ?></span>
                                    <span style="float:right;">:
                                    </span>
                                </div>
                                <div class="col-md-10 col-xs-9 timeslot">
                                    <?php if(in_array( $item->id , $active_days)): ?>
                                    <?php echo e(date('h:i A',strtotime($shops[strtolower( ($item->id== 5) ? substr($item->name,0,4) : substr($item->name,0,3))."_start"]))); ?>

                                    -
                                    <?php echo e(date('h:i A',strtotime($shops[strtolower( ($item->id== 5) ? substr($item->name,0,4) : substr($item->name,0,3))."_end"]))); ?>

                                    <?php else: ?>
                                    Closed
                                    <?php endif; ?>
                                </div>
                            </div>


                        </p>
                        <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                    </div>
                    <div id="menu2" class="tab-pane fade " style="padding:20px;">
                        <?php if($shops->dealsOngoing->isNotEmpty()): ?>
                        <?php $__currentLoopData = $shops->dealsOngoing; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $item): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                        <div class="panel panel-default">
                            <div class="panel-body">

                                
                                
                                <div class="row">
                                    <div class="col-sm-4">
                                        <img src="<?php echo asset("$item->pic"); ?>" class="img-responsive"
                                            style="height:200px;width:100%;padding:10px">
                                    </div>
                                    <div class="col-sm-8">
                                        <div class="col-sm-9">
                                            <h3 style="font-weight:800;"><?php echo e(strtoupper($item->name)); ?></h3>
                                            <div><?php echo $item->description; ?></div>
                                            <br>
                                            <p><b>Valid Upto: </b> <?php echo e($item->valid_upto); ?></p>
                                            <div>
                                                <a type="button" class="btn btn-default dealdetails"
                                                    data-id="<?php echo e($item->id); ?>">Details</a>
                                            </div>

                                        </div>
                                        <div class="col-sm-3 text-center">
                                            <?php if($item->payment_type == 'wingpoints'): ?>
                                            <h5>(wingpoints) <?php echo e($item->price); ?></h5>


                                            <?php else: ?>
                                            <h3>₹.<?php echo e($item->price); ?></h3>
                                            <?php endif; ?>
                                            <p>Flat <?php echo e($item->discount_percentage); ?> % off*</p>


                                            <?php if(Auth::guest()): ?>
                                            <button class="btn btn-primary" data-toggle="modal"
                                                data-target="#myModal1">Claim Now</button>
                                            <?php else: ?>
                                            <?php if($item->payment_type != 'wingpoints'): ?>
                                            <a href="<?php echo e(url('/getdeal',[ $item->id])); ?>" class="btn btn-danger"
                                                data-id="<?php echo e($item->id); ?>">Claim
                                                Now</a>

                                            <?php else: ?>

                                            <?php if(Session::get('total_wingpoints')->points >= $item->price): ?>
                                            <a href="<?php echo e(url('/getdeal',[ $item->id])); ?>" class="btn btn-danger"
                                                data-id="<?php echo e($item->id); ?>">Claim
                                                Now</a>
                                            <?php else: ?>

                                            <button class="btn btn-default" disabled> Not Enough Points </button>
                                            <?php endif; ?>

                                            <?php endif; ?>

                                            <?php endif; ?>
                                        </div>
                                    </div>
                                </div>

                            </div>

                        </div>

                        <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>

                        <?php else: ?>
                        <h5>No Deals </h5>

                        <?php endif; ?>


                    </div>
                    <div id="menu3" class="tab-pane fade" <?php if($shops->menus->isEmpty()): ?>
                        style="padding:20px;display:none;" <?php endif; ?> >
                        <?php $__currentLoopData = $shops->menus; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $item): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                        <div class="col-md-4 thumbnail">
                            <img src="<?php echo asset("$item->image_path"); ?>" alt=""
                                style="width:100%;max-height:240px; height:240px;">
                        </div>
                        <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                    </div>
                </div>
            </div>
        </div>

        <div class="modal fade" id="dealdetails">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true"> &times; </button>
                        <h4 class="modal-title"></h4>
                    </div>
                    <div class="modal-body">

                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                    </div>
                </div>
            </div>
        </div>

        <?php $__env->stopSection(); ?>
        <?php $__env->startSection('scripts'); ?>
        <script async defer
            src="https://maps.googleapis.com/maps/api/js?key=AIzaSyBHLFZ4C3XB7MDwjywzpBC1ZWMH0u2zuE4&callback=initMap">
        </script>
        <script>
            function initMap() {
        var myLatLng = {lat: <?php echo e($shops->lat); ?>, lng: <?php echo e($shops->lng); ?> };

        var map = new google.maps.Map(document.getElementById('map'), {
          zoom: 12,
          center: myLatLng
        });

        var marker = new google.maps.Marker({
          position: myLatLng,
          map: map,
          title: 'Hello World!'
        });
      }

      $('.dealdetails').click(function () {
          let id =  $(this).data('id');

        $.ajax({
          type: "GET",
          url: "<?php echo e(url('/dealdetails/')); ?>"+"/"+id,

          success: function (response) {

            var json_obj = $.parseJSON(response);
            console.log(json_obj);
            var mymodal = $('#dealdetails');
            mymodal.find('.modal-title').html(`
             <div>
                <h3>`+ json_obj[0]['name'].toUpperCase()+`</h3>

             </div>
             `);

             mymodal.find('.modal-body').html(`
             <div>
                <h4><u>Description:</u></h4>
                <p>`+json_obj[0]['description'] +`</p>
                <hr>
                <h4><u>Active days:</u></h4>
                <p>`+json_obj[0]['days_applicable'].join()+`</p>

                <h4><u>Valid for:</u></h4>
                <p>`+'one Person only'+`</p>

                <h4><u>Valid Dates:</u></h4>
                <p>`+json_obj[0]['valid_from']+' To '+json_obj[0]['valid_from']+`</p>

                <h4><u>Terms & Conditions:</u></h4>
                <p>`+json_obj[0]['terms_conditions'] +`</p>

                <?php if(Auth::guest()): ?>
                                        <button class="btn btn-primary" data-toggle="modal" data-target="#myModal1">Claim Now</button>
                                        <?php else: ?>
                                        <button class="btn btn-primary claim" data-id= `+id+`>Claim Now</button>
                                        <?php endif; ?>

             </div>
             `);
            $('#dealdetails').modal('show');
          }
      });

      })


      $(document).on('click', ".unlike", function () {
        var id = $(this).data('id');
        var element = this;

        var data = {
                shop_id:id
            };
        $.ajax({
                type: "post",
                url: '<?php echo e(url("removefavouriteshop")); ?>',
                headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                 },
                data: data,
                cache: false,
                success: function (data)
                {
                    if(data.status == "success"){
                        $(element).removeClass('unlike').addClass('like').html('<i class="far fa-heart" style="color:#e31b23"></i>');
                    }
                },
                error: function (data){
                    alert('Fail to run unlike..');
                }
            });


    });
    $(document).on('click', ".like", function () {
        var id = $(this).data('id');
        var element = this;

        var data = {
                shop_id:id
            };
        $.ajax({
                type: "post",
                url: '<?php echo e(url("addfavouriteshop")); ?>',
                headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                 },
                data: data,
                cache: false,
                success: function (data)
                {
                    console.log(data);
                    if(data.status == "success"){
                            $(element).removeClass('like').addClass('unlike').html('<i class="fas fa-heart" style="color:#e31b23"></i>');

                    }
                },
                error: function (data){
                    alert('Fail to run like..');
                }
            });


    });



        </script>

        <?php $__env->stopSection(); ?>
</body>

<?php echo $__env->make('layouts.app', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH C:\wamp64\www\offers\resources\views/shopdetails.blade.php ENDPATH**/ ?>