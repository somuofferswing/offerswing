<?php $__env->startSection('content'); ?>
<style>
    .ps-section--shopping {
        padding: 30px 0;
    }

    .ps-table--shopping-cart tbody tr td {
        text-align: center;
    }

    .ps-table--shopping-cart tbody tr td:first-child {
        text-align: left;
    }
</style>

<section>
    
    <div class="ps-page--simple">

        <div class="ps-section--shopping ps-shopping-cart">

            <div class="container">

                <div class="ps-section__header pb-5">

                    <h1>Your Order</h1>

                </div>

                <div class="ps-section__content">

                    <div class="table-responsive">

                        <table class="table ps-table--shopping-cart">

                            <thead>

                                <tr>

                                    <th>ITEM NAME</th>

                                    <th>TYPE</th>

                                    <th>PRICE</th>

                                    <th>QUANTITY</th>

                                    <th>TOTAL</th>

                                    <th></th>

                                </tr>

                            </thead>

                            <tbody>

                                <?php if($order_details['order_type']== 'DEAL'): ?>

                                <tr>

                                    <td><?php echo e($deal['name']); ?></td>

                                    <td><?php echo e($order_details['type']); ?></td>

                                    <td>₹ <?php echo e($deal['price']); ?></td>

                                    <td><?php echo e($order_details['quantity']); ?></td>



                                    <td>₹ <?php echo e($deal['price']); ?> * <?php echo e($order_details['quantity']); ?></td>

                                    <td><a class="delete" href="#"><i class="icon-cross"></i></a></td>

                                </tr>



                                <div class="col-md-12">

                                    <div class="pull-right">

                                        <table class="table" id="price">

                                            <tr>

                                                <td>Price :</td>

                                                <td>₹ <?php echo e($deal['price']); ?> * <?php echo e($order_details['quantity']); ?></td>

                                                <td>₹ <span
                                                        class="pull-right"><?php echo e($deal['price'] * $order_details['quantity']); ?></span>
                                                </td>

                                            </tr>

                                            <tr>

                                                <td>Total Price :</td>

                                                <td></td>

                                                <td>₹ <span
                                                        class="pull-right"><?php echo e($order_details['final_price']); ?></span>
                                                </td>

                                            </tr>

                                        </table>

                                    </div>

                                </div>

                                <?php else: ?>

                                <tr>

                                    <td><?php echo e($deal['title']); ?></td>

                                    <td><?php echo e($order_details['order_type']); ?></td>

                                    <td>₹ <?php echo e($deal['event_price']); ?></td>

                                    <td>

                                        <div class="form-group--number">

                                            <button class="up quantity" id="plus">+</button>

                                            <button class="down quantity" id="minus">-</button>

                                            <input class="form-control" type="text" placeholder="1"
                                                value="<?php echo e($order_details['quantity']); ?>">

                                        </div>

                                    </td>

                                    <td>₹ <?php echo e($deal['event_price'] * $order_details['quantity']); ?></td>

                                    <td><a class="delete" href="#"><i class="icon-cross"></i></a></td>

                                </tr>

                                <?php endif; ?>

                            </tbody>

                        </table>

                    </div>

                    <div class="ps-section__cart-actions">

                        <a class="ps-btn bg-dark" href="<?php echo e(url('/eventdetails',[$deal['id']])); ?>">

                            <i class="icon-arrow-left"></i> Back to Event

                        </a>

                        <a class="ps-btn" href="<?php echo e(url('/proceedtopay',[$order_details['order_id']])); ?>">

                            <i class="fa fa-send pr-2"></i>Proceed to Pay

                        </a>

                    </div>

                </div>



            </div>

        </div>

    </div>

</section>



<?php $__env->stopSection(); ?>
<?php echo $__env->make('layouts.app', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH C:\wamp64\www\offers\resources\views/frontend/orders.blade.php ENDPATH**/ ?>