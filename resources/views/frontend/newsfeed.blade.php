@extends('layouts.app')

@section('content')
<style>
@media (min-width: 992px){
    .ps-blog--sidebar .ps-blog__left{
        padding-right:0px;
    }
}
    
.ps-post--small-thumbnail .ps-post__content{
    padding: 0px 20px;
}
.owl-carousel .owl-item img {
    width: inherit;
}

</style>
<div class="ps-breadcrumb">
    <div class="container">
        <ul class="breadcrumb text-capitalize" id="breadcrumb">
            <li><a href="{{ url('/') }}">Home</a></li>
            <li>News Feed</li>
        </ul>
    </div>
</div>

<section>
    <div class="ps-page--blog">
        <div class="container">
            
            <div class="ps-page__header">
                <h1>News Feed</h1>
                <hr class="highlighter">
            </div>
            
            <div class="ps-blog">
                @foreach ($newsfeed as $item)
                    <div class="ps-post ps-post--small-thumbnail">
                        <div class="ps-post__thumbnail">
                            @php
                            $images = $item->newsImages;
                            $image = $images[0]['image'];
                            @endphp
                             <div class="ps-carousel--nav-inside owl-slider" data-owl-auto="true" data-owl-loop="true" data-owl-speed="5000" data-owl-gap="0" data-owl-nav="true" data-owl-dots="true" data-owl-item="1" data-owl-item-xs="1" data-owl-item-sm="1" data-owl-item-md="1" data-owl-item-lg="1" data-owl-duration="1000" data-owl-mousedrag="on" style="height:inherit">
                                <img src="{{ asset('storage/'.$image)}}" alt="">
                                <img src="{{ asset('storage/'.$image)}}" alt="">
                            </div>
                        </div>
                        <div class="ps-post__content">
                            <div class="ps-post__top">
                                <span class="ps-post__title" >{{ ucfirst($item->title) }}
                                    @if(Auth::guest())
                                    <span class="badge zoom ml-2" data-id="{{ $item->id }}"  data-toggle="modal" data-target="#myModal1">
                                        <span data-toggle="tooltip" data-placement="right" title="" data-original-title="Like">
                                        <i class="fa fa-thumbs-o-up fa-lg text-danger"></i>
                                        </span>
                                    </span>
                                    @else
                                        @if (in_array($item->id,$likes))
                                            <span class="badge zoom ml-2 unlike" data-id="{{ $item->id }}" data-toggle="tooltip" data-placement="right" title="" data-original-title="Unlike">
                                                <i class="fa fa-thumbs-o-down fa-lg text-danger"></i>
                                            </span>
                                        @else
                                            <span class="badge zoom ml-2 like" data-id="{{ $item->id }}" data-toggle="tooltip" data-placement="right" title="" data-original-title="Like">
                                                <i class="fa fa-thumbs-o-up fa-lg text-danger"></i>
                                            </span>
                                        @endif
                                    @endif
                                </span>
                                <p>
                                    {!! str_limit($item->description, 100, '...<a class="text-primary read_more" href="javascript:void(0);">Read more</a>') !!}
                                </p>
                            </div>
                            <div class="ps-post__bottom">
                                <span class="text-muted">{{ date('h:i A',strtotime($item->created_at)).' on '.date( 'd-m-Y, l' ,strtotime($item->created_at)) }}</span>
                                <ul class="ps-list--social pull-right">
                                    {{-- 
                                    <li>
                                        <a class="text-info" href="#" target="_blank" title="share"><i class="fa fa-share-alt"></i></a>
                                    </li>
                                    <li>
                                        <a class="facebook" href="#" target="_blank" title="Facebook"><i class="fa fa-facebook"></i></a>
                                    </li>
                                    <li>
                                        <a class="twitter" href="#" target="_blank" title="Twitter"><i class="fa fa-twitter"></i></a>
                                    </li>
                                    <li>
                                        <a class="instagram" href="#" target="_blank" title="instagram"><i class="fa fa-instagram"></i></a>
                                    </li>
                                    <li>
                                        <a class="text-success" href="#" target="_blank" title="whatsapp"><i class="fa fa-whatsapp"></i></a>
                                    </li>
                                    --}}
                                    <li>
                                        <a class="text-info copy_share_url" href="javascript:void(0);" title="copy" data-url="{{ Request::url() }}"><i class="fa fa-share-alt"></i></a>
                                    </li>
                                    {!! Share::currentPage(null, ['class' => 'facebook', 'title' => 'facebook'], '', '')->facebook() !!}
                                    {!! Share::currentPage(null, ['class' => 'twitter', 'title' => 'twitter'], '', '')->twitter() !!}
                                    {!! Share::currentPage(null, ['class' => 'linkedin', 'title' => 'linkedin'], '', '')->linkedin() !!}
                                    {!! Share::currentPage(null, ['class' => 'text-success', 'title' => 'whatsapp'], '', '')->whatsapp() !!} 
                                    
                                </ul>
                               
                            </div>
                         
                        </div>
                    </div>
                    @endforeach
                </div>
            
        </div>
    </div>
</section>

<!-- <a class="btn btn-primary" >Trigger modal</a> -->
<div class="modal fade" id="share">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-body">
                <div class="a2a_kit a2a_kit_size_32 a2a_default_style">
                    <a class="a2a_button_facebook"></a>
                    <a class="a2a_button_twitter"></a>
                    <a class="a2a_button_whatsapp"></a>
                </div>
                <script async src="https://static.addtoany.com/menu/page.js"></script>
            </div>
        </div>
    </div>
</div>

<script>

    $(document).on('click', ".unlike", function (e) {
        e.preventDefault();
        var id = $(this).data('id');
        var element = this;

        var data = {
                shop_id:id
            };
        $.ajax({
                type: "post",
                url: '{{ url("removelike") }}',
                headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                 },
                data: data,
                cache: false,
                success: function (data)
                {
                    if(data.status == "success"){
                        $(element).removeClass('unlike').addClass('like').attr('data-original-title', 'Like').html('<i class="fa fa-thumbs-o-up text-danger"></i>');
                    }
                },
                error: function (data){
                    swal('Fail to run unlike..');
                }
            });
    });

    $(document).on('click', ".like", function (e) {
        e.preventDefault();
        var id = $(this).data('id');
        var element = this;

        var data = {
                shop_id:id
            };
        $.ajax({
            type: "post",
            url: '{{ url("addlike") }}',
            headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
            data: data,
            cache: false,
            success: function (data)
            {
                if(data.status == "success"){
                    $(element).removeClass('like').addClass('unlike').attr('data-original-title', 'Unlike').html('<i class="fa fa-thumbs-o-down text-danger"></i>');
                }
            },
            error: function (err){
                swal('Fail to run like..');
            }
        });
    });
</script>


@endsection