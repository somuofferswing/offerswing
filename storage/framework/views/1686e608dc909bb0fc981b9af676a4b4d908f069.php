<?php $__env->startSection('content'); ?>

<div class="content-wrapper">

    <section class="content-header">

        <h1>

            Dashboard

            <small>Control panel</small>

        </h1>

        <!-- <ol class="breadcrumb">

              <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>

              <li class="active">Dashboard</li>

            </ol> -->

    </section>



    <?php if(Auth::user()->user_type == 1): ?>

    <section class="content">

        <div class="row">

            <div class="col-lg-3 col-xs-6">

                <div class="small-box bg-aqua">

                    <div class="inner">

                        <h3><?php echo e($shopscount); ?></h3>



                        <p>Shops</p>

                    </div>

                    <div class="icon">

                        <i class="ion ion-bag"></i>

                    </div>

                    <a href="#" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a>

                </div>

            </div>

            <div class="col-lg-3 col-xs-6">

                <div class="small-box bg-green">

                    <div class="inner">

                        <h3><?php echo e($offerscount); ?></h3>



                        <p>offers</p>

                    </div>

                    <div class="icon">

                        <i class="ion ion-stats-bars"></i>

                    </div>

                    <a href="#" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a>

                </div>

            </div>



            <div class="col-lg-3 col-xs-6">

                <div class="small-box bg-yellow">

                    <div class="inner">

                        <h3><?php echo e($userscount); ?></h3>



                        <p>Users</p>

                    </div>

                    <div class="icon">

                        <i class="ion ion-person-add"></i>

                    </div>

                    <a href="#" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a>

                </div>

            </div>



            <div class="col-lg-3 col-xs-6">

                <div class="small-box bg-red">

                    <div class="inner">

                        

                        <h3>25</h3>



                        <p>Orders</p>

                    </div>

                    <div class="icon">

                        <i class="ion ion-pie-graph"></i>

                    </div>

                    <a href="#" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a>

                </div>

            </div>

        </div>

        <div class="row">

            <div class="col-md-12">

                <div class="box">

                    <div class="box-header with-border">

                        <h3 class="box-title">Monthly Recap Report</h3>



                        <div class="box-tools pull-right">

                            <button type="button" class="btn btn-box-tool" data-widget="collapse"><i

                                    class="fa fa-minus"></i>

                            </button>

                            <div class="btn-group">

                                <button type="button" class="btn btn-box-tool dropdown-toggle" data-toggle="dropdown">

                                    <i class="fa fa-wrench"></i></button>

                                <ul class="dropdown-menu" role="menu">

                                    <li><a href="#">Action</a></li>

                                    <li><a href="#">Another action</a></li>

                                    <li><a href="#">Something else here</a></li>

                                    <li class="divider"></li>

                                    <li><a href="#">Separated link</a></li>

                                </ul>

                            </div>

                            <button type="button" class="btn btn-box-tool" data-widget="remove"><i

                                    class="fa fa-times"></i></button>

                        </div>

                    </div>

                    <!-- /.box-header -->

                    <div class="box-body">

                        <div class="row">

                            <div class="col-md-8">

                                <p class="text-center">

                                    <strong>Sales: 1 Jan, 2014 - 30 Jul, 2014</strong>

                                </p>



                                <div class="chart">

                                    <!-- Sales Chart Canvas -->

                                    <canvas id="salesChart" style="height: 180px;"></canvas>

                                </div>

                                <!-- /.chart-responsive -->

                            </div>

                            <!-- /.col -->

                            <div class="col-md-4">

                                <p class="text-center">

                                    <strong>Goal Completion</strong>

                                </p>



                                <div class="progress-group">

                                    <span class="progress-text">Add Products to Cart</span>

                                    <span class="progress-number"><b>160</b>/200</span>



                                    <div class="progress sm">

                                        <div class="progress-bar progress-bar-aqua" style="width: 80%"></div>

                                    </div>

                                </div>

                                <!-- /.progress-group -->

                                <div class="progress-group">

                                    <span class="progress-text">Complete Purchase</span>

                                    <span class="progress-number"><b>310</b>/400</span>



                                    <div class="progress sm">

                                        <div class="progress-bar progress-bar-red" style="width: 80%"></div>

                                    </div>

                                </div>

                                <!-- /.progress-group -->

                                <div class="progress-group">

                                    <span class="progress-text">Visit Premium Page</span>

                                    <span class="progress-number"><b>480</b>/800</span>



                                    <div class="progress sm">

                                        <div class="progress-bar progress-bar-green" style="width: 80%"></div>

                                    </div>

                                </div>

                                <!-- /.progress-group -->

                                <div class="progress-group">

                                    <span class="progress-text">Send Inquiries</span>

                                    <span class="progress-number"><b>250</b>/500</span>



                                    <div class="progress sm">

                                        <div class="progress-bar progress-bar-yellow" style="width: 80%"></div>

                                    </div>

                                </div>

                                <!-- /.progress-group -->

                            </div>

                            <!-- /.col -->

                        </div>

                        <!-- /.row -->

                    </div>

                    <!-- ./box-body -->

                    <div class="box-footer">

                        <div class="row">

                            <div class="col-sm-3 col-xs-6">

                                <div class="description-block border-right">

                                    <span class="description-percentage text-green"><i class="fa fa-caret-up"></i>

                                        17%</span>

                                    <h5 class="description-header">$35,210.43</h5>

                                    <span class="description-text">TOTAL REVENUE</span>

                                </div>

                                <!-- /.description-block -->

                            </div>

                            <!-- /.col -->

                            <div class="col-sm-3 col-xs-6">

                                <div class="description-block border-right">

                                    <span class="description-percentage text-yellow"><i class="fa fa-caret-left"></i>

                                        0%</span>

                                    <h5 class="description-header">$10,390.90</h5>

                                    <span class="description-text">TOTAL COST</span>

                                </div>

                                <!-- /.description-block -->

                            </div>

                            <!-- /.col -->

                            <div class="col-sm-3 col-xs-6">

                                <div class="description-block border-right">

                                    <span class="description-percentage text-green"><i class="fa fa-caret-up"></i>

                                        20%</span>

                                    <h5 class="description-header">$24,813.53</h5>

                                    <span class="description-text">TOTAL PROFIT</span>

                                </div>

                                <!-- /.description-block -->

                            </div>

                            <!-- /.col -->

                            <div class="col-sm-3 col-xs-6">

                                <div class="description-block">

                                    <span class="description-percentage text-red"><i class="fa fa-caret-down"></i>

                                        18%</span>

                                    <h5 class="description-header">1200</h5>

                                    <span class="description-text">GOAL COMPLETIONS</span>

                                </div>

                                <!-- /.description-block -->

                            </div>

                        </div>

                        <!-- /.row -->

                    </div>

                    <!-- /.box-footer -->

                </div>

                <!-- /.box -->

            </div>

            <!-- /.col -->

        </div>

        <div class="row">

            <div class="col-sm-6">

                <div class="box box-info">

                    <div class="box-header with-border">

                        <h3 class="box-title">Latest Orders</h3>



                        <div class="box-tools pull-right">

                            <button type="button" class="btn btn-box-tool" data-widget="collapse"><i

                                    class="fa fa-minus"></i>

                            </button>

                            <button type="button" class="btn btn-box-tool" data-widget="remove"><i

                                    class="fa fa-times"></i></button>

                        </div>

                    </div>

                    <!-- /.box-header -->

                    <div class="box-body">

                        <div class="table-responsive">

                            <table class="table no-margin">

                                <thead>

                                    <tr>

                                        <th>Order ID</th>

                                        <th>Item</th>

                                        <th>Status</th>

                                        <th>Popularity</th>

                                    </tr>

                                </thead>

                                <tbody>

                                    <tr>

                                        <td><a href="pages/examples/invoice.html">OR9842</a></td>

                                        <td>Call of Duty IV</td>

                                        <td><span class="label label-success">Shipped</span></td>

                                        <td>

                                            <div class="sparkbar" data-color="#00a65a" data-height="20">

                                                90,80,90,-70,61,-83,63

                                            </div>

                                        </td>

                                    </tr>

                                    <tr>

                                        <td><a href="pages/examples/invoice.html">OR1848</a></td>

                                        <td>Samsung Smart TV</td>

                                        <td><span class="label label-warning">Pending</span></td>

                                        <td>

                                            <div class="sparkbar" data-color="#f39c12" data-height="20">

                                                90,80,-90,70,61,-83,68

                                            </div>

                                        </td>

                                    </tr>

                                    <tr>

                                        <td><a href="pages/examples/invoice.html">OR7429</a></td>

                                        <td>iPhone 6 Plus</td>

                                        <td><span class="label label-danger">Delivered</span></td>

                                        <td>

                                            <div class="sparkbar" data-color="#f56954" data-height="20">

                                                90,-80,90,70,-61,83,63

                                            </div>

                                        </td>

                                    </tr>

                                    <tr>

                                        <td><a href="pages/examples/invoice.html">OR7429</a></td>

                                        <td>Samsung Smart TV</td>

                                        <td><span class="label label-info">Processing</span></td>

                                        <td>

                                            <div class="sparkbar" data-color="#00c0ef" data-height="20">

                                                90,80,-90,70,-61,83,63

                                            </div>

                                        </td>

                                    </tr>

                                    <tr>

                                        <td><a href="pages/examples/invoice.html">OR1848</a></td>

                                        <td>Samsung Smart TV</td>

                                        <td><span class="label label-warning">Pending</span></td>

                                        <td>

                                            <div class="sparkbar" data-color="#f39c12" data-height="20">

                                                90,80,-90,70,61,-83,68

                                            </div>

                                        </td>

                                    </tr>

                                    <tr>

                                        <td><a href="pages/examples/invoice.html">OR7429</a></td>

                                        <td>iPhone 6 Plus</td>

                                        <td><span class="label label-danger">Delivered</span></td>

                                        <td>

                                            <div class="sparkbar" data-color="#f56954" data-height="20">

                                                90,-80,90,70,-61,83,63

                                            </div>

                                        </td>

                                    </tr>

                                    <tr>

                                        <td><a href="pages/examples/invoice.html">OR9842</a></td>

                                        <td>Call of Duty IV</td>

                                        <td><span class="label label-success">Shipped</span></td>

                                        <td>

                                            <div class="sparkbar" data-color="#00a65a" data-height="20">

                                                90,80,90,-70,61,-83,63

                                            </div>

                                        </td>

                                    </tr>

                                </tbody>

                            </table>

                        </div>

                    </div>

                </div>

            </div>

            <div class="col-sm-6">

                <div class="box box-primary">

                    <div class="box-header with-border">

                        <h3 class="box-title">Recently Added Products</h3>



                        <div class="box-tools pull-right">

                            <button type="button" class="btn btn-box-tool" data-widget="collapse"><i

                                    class="fa fa-minus"></i>

                            </button>

                            <button type="button" class="btn btn-box-tool" data-widget="remove"><i

                                    class="fa fa-times"></i></button>

                        </div>

                    </div>

                    <!-- /.box-header -->

                    <div class="box-body">

                        <ul class="products-list product-list-in-box">

                            <li class="item">

                                <div class="product-img">

                                    <img src="dist/img/default-50x50.gif" alt="Product Image">

                                </div>

                                <div class="product-info">

                                    <a href="javascript:void(0)" class="product-title">Samsung TV

                                        <span class="label label-warning pull-right">$1800</span></a>

                                    <span class="product-description">

                                        Samsung 32" 1080p 60Hz LED Smart HDTV.

                                    </span>

                                </div>

                            </li>

                            <!-- /.item -->

                            <li class="item">

                                <div class="product-img">

                                    <img src="dist/img/default-50x50.gif" alt="Product Image">

                                </div>

                                <div class="product-info">

                                    <a href="javascript:void(0)" class="product-title">Bicycle

                                        <span class="label label-info pull-right">$700</span></a>

                                    <span class="product-description">

                                        26" Mongoose Dolomite Men's 7-speed, Navy Blue.

                                    </span>

                                </div>

                            </li>

                            <!-- /.item -->

                            <li class="item">

                                <div class="product-img">

                                    <img src="dist/img/default-50x50.gif" alt="Product Image">

                                </div>

                                <div class="product-info">

                                    <a href="javascript:void(0)" class="product-title">Xbox One <span

                                            class="label label-danger pull-right">$350</span></a>

                                    <span class="product-description">

                                        Xbox One Console Bundle with Halo Master Chief Collection.

                                    </span>

                                </div>

                            </li>

                            <!-- /.item -->

                            <li class="item">

                                <div class="product-img">

                                    <img src="dist/img/default-50x50.gif" alt="Product Image">

                                </div>

                                <div class="product-info">

                                    <a href="javascript:void(0)" class="product-title">PlayStation 4

                                        <span class="label label-success pull-right">$399</span></a>

                                    <span class="product-description">

                                        PlayStation 4 500GB Console (PS4)

                                    </span>

                                </div>

                            </li>

                            <!-- /.item -->

                        </ul>

                    </div>

                </div>

            </div>

        </div>

    </section>



    <?php else: ?>

    <section class="content">

        <div class="row">

            <?php $__currentLoopData = $shop; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $key=>$item): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>

            <?php
                // echo "<pre>";
                // print_r($item);
                // die;
            ?>

            <div class="col-md-6">

                <div class="box box-solid">

                    <div class="box-header with-border">

                        <h3 class="box-title"><?php echo e($item->name); ?></h3>

                    </div>

                    <!-- /.box-header -->

                    <div class="box-body">

                        <div id="carousel-example-generic" class="carousel slide" data-ride="carousel">

                            

                            <div class="carousel-inner">

                                <?php $__currentLoopData = $item->images; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $key => $value): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>

                                <div class="item <?php echo e(($key == 0) ? 'active' : ''); ?>">

                                    <img src="<?php echo asset("$value->image_path"); ?>" class="img-responsive">

                                </div>

                                <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>

                            </div>

                            <a class="left carousel-control" href="#carousel-example-generic" data-slide="prev">

                                <span class="fa fa-angle-left"></span>

                            </a>

                            <a class="right carousel-control" href="#carousel-example-generic" data-slide="next">

                                <span class="fa fa-angle-right"></span>

                            </a>

                        </div>

                    </div>

                    <div class="row">

                        <div class="col-md-6">

                            <div class=" box-solid">

                                <div class="box-header with-border">





                                    <h3 class="box-title">Address</h3>

                                </div>

                                <!-- /.box-header -->

                                <div class="box-body">

                                    <dl>

                                        <dt>Description</dt>

                                        <dd><?php echo $item->shop_description; ?></dd>

                                        <dt>Address</dt>

                                        <dd>

                                            <?php echo $item->address; ?>


                                            <br>

                                            <?php echo e($item->city); ?>


                                            <br>

                                            <?php echo e($item->state); ?>


                                            <br>

                                            <?php echo e($item->pincode); ?>


                                        </dd>



                                    </dl>

                                </div>

                                <!-- /.box-body -->

                            </div>

                            <!-- /.box -->

                        </div>

                        <div class="col-md-6">

                            <div class=" box-solid">

                                <div class="box-header with-border">





                                    <h3 class="box-title">Location</h3>

                                </div>



                                <div class="box-body">

                                    <div class="map" id="map<?php echo e($key); ?>" style="width:100%;height:150px"

                                        data-lat="<?php echo e($item->lat); ?>" data-long="<?php echo e($item->lng); ?>">



                                    </div>

                                </div>

                                <!-- /.box -->

                            </div>

                            <!-- ./col -->

                        </div>



                        <div class="row">

                            <div class="col-md-12">

                                <div class=" box-solid">

                                    <div class="box-header with-border">

                                        <a href="<?php echo e(url('/shopoffers',[$item->id])); ?>" class="btn  btn-primary">Offers</a>

                                        <a href="<?php echo e(url('/shopdeals',[$item->id])); ?>" class="btn  btn-success">Deals</a>

                                        

                                    </div>



                                </div>



                            </div>

                        </div>

                        <!-- /.box-body -->

                    </div>

                    <!-- /.box -->

                </div>

            </div>

            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>



        </div>







    </section>



    <?php endif; ?>



    <?php $__env->stopSection(); ?>

    <?php $__env->startSection('scripts'); ?>

    <script src="http://maps.googleapis.com/maps/api/js?key=AIzaSyBHLFZ4C3XB7MDwjywzpBC1ZWMH0u2zuE4&libraries=places"

        async defer>

    </script>



    <script>

        $(document).ready(function (){

        $('div.map').each(function(){

           var lat =  $(this).data('lat');

           var long =  $(this).data('long');



           var id = $(this).attr('id');



           var myLatLng = {lat: lat, lng: long};



            var map = new google.maps.Map(document.getElementById(id), {

            zoom: 12,

            center: myLatLng

            });



            var marker = new google.maps.Marker({

            position: myLatLng,

            map: map,

            title: 'Hello World!'

            });

        });



});

    </script>





    <?php $__env->stopSection(); ?>
<?php echo $__env->make('layouts.admin', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH D:\wamp64\www\offerswing_test\resources\views/backend/admin/home.blade.php ENDPATH**/ ?>