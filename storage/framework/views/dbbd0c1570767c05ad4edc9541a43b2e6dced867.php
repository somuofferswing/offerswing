

<?php $__env->startSection('content'); ?>
    <div class="content-wrapper">
        <section class="content-header">
            <h1>
                Shops
                <small>All Shops</small>
            </h1>
            <!-- <ol class="breadcrumb">
              <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
              <li class="active">Dashboard</li>
            </ol> -->
        </section>
        <section class="content">

            <div class="box box-primary">
                <div class="box-header with-border">
                    <h3 class="box-title">All Pages</h3>
                </div>
                <div class="table-responsive">
                    <table class="table table-striped">
                        <thead>
                        <tr>
                            <th>Page Name</th>
                            <th>Content</th>
                            <th colspan="3">Action</th>
                        </tr>
                        </thead>
                        <tbody>
                        <?php $__currentLoopData = $pages; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $page): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                            <tr>
                                <td><?php echo e($page->page_name); ?></td>
                                <td><?php echo $page->content; ?></td>
                                
                                <td><a href="<?php echo e(url('/pages/edit', [$page->id])); ?>" class="btn btn-default">Edit</a></td>
                                <td><a class="btn btn-danger del" data-sub="false" data-id="<?php echo e($page->id); ?>">Delete</a></td>

                                

                            </tr>
                        <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                        </tbody>
                    </table>
                </div>
            </div>
    </section>
    </div>
<?php $__env->stopSection(); ?>
<?php $__env->startSection('scripts'); ?>

<script>
$('.del').on('click',function(){
        var id = $(this).data('id');
        if (confirm('Do you want to delete page?')) {
            // alert('Thanks for confirming');
            $.ajax({
                type: "POST",
                url: "<?php echo e(url('pages/delete/')); ?>"+"/"+id,
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
                success: function (response) {
                    // console.log(response);
                    // alert(response);
                    window.location.reload();
                }
            });
        } else {

        }

});
</script>

<?php $__env->stopSection(); ?>

<?php echo $__env->make('layouts.admin', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH /home/offerkrx/public_html/offerswing_test/resources/views/backend/pages/index.blade.php ENDPATH**/ ?>