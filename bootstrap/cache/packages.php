<?php return array (
  'beyondcode/laravel-dump-server' => 
  array (
    'providers' => 
    array (
      0 => 'BeyondCode\\DumpServer\\DumpServerServiceProvider',
    ),
  ),
  'fideloper/proxy' => 
  array (
    'providers' => 
    array (
      0 => 'Fideloper\\Proxy\\TrustedProxyServiceProvider',
    ),
  ),
  'gr8shivam/laravel-sms-api' => 
  array (
    'providers' => 
    array (
      0 => 'Gr8Shivam\\SmsApi\\SmsApiServiceProvider',
    ),
    'aliases' => 
    array (
      'SmsApi' => 'Gr8Shivam\\SmsApi\\SmsApiFacade',
    ),
  ),
  'jorenvanhocht/laravel-share' => 
  array (
    'providers' => 
    array (
      0 => 'Jorenvh\\Share\\Providers\\ShareServiceProvider',
    ),
    'aliases' => 
    array (
      'Share' => 'Jorenvh\\Share\\ShareFacade',
    ),
  ),
  'laravel/passport' => 
  array (
    'providers' => 
    array (
      0 => 'Laravel\\Passport\\PassportServiceProvider',
    ),
  ),
  'laravel/tinker' => 
  array (
    'providers' => 
    array (
      0 => 'Laravel\\Tinker\\TinkerServiceProvider',
    ),
  ),
  'nesbot/carbon' => 
  array (
    'providers' => 
    array (
      0 => 'Carbon\\Laravel\\ServiceProvider',
    ),
  ),
  'nunomaduro/collision' => 
  array (
    'providers' => 
    array (
      0 => 'NunoMaduro\\Collision\\Adapters\\Laravel\\CollisionServiceProvider',
    ),
  ),
  'simplesoftwareio/simple-qrcode' => 
  array (
    'providers' => 
    array (
      0 => 'SimpleSoftwareIO\\QrCode\\QrCodeServiceProvider',
    ),
    'aliases' => 
    array (
      'QrCode' => 'SimpleSoftwareIO\\QrCode\\Facades\\QrCode',
    ),
  ),
  'softon/indipay' => 
  array (
    'providers' => 
    array (
      0 => 'Softon\\Indipay\\IndipayServiceProvider',
    ),
    'aliases' => 
    array (
      'Indipay' => 'Softon\\Indipay\\Facades\\Indipay',
    ),
  ),
);