<?php $__env->startSection('content'); ?>
<div class="container-fluid d-flex flex-column py-3">
    <div class="row py-3">
        <div class="col-md-9 flex-grow-1">

            <?php $__env->startComponent('components.carousel',['slider'=> $slider_1]); ?>
            <?php $__env->slot('coursel_id'); ?>
            coursel
            <?php $__env->endSlot(); ?>
            <?php echo $__env->renderComponent(); ?>

        </div>

        <div class="col-md-3 flex-grow-1">
            <?php $__env->startComponent('components.carousel',['slider'=> $slider_2]); ?>
            <?php $__env->slot('coursel_id'); ?>
            coursel1
            <?php $__env->endSlot(); ?>
            <?php echo $__env->renderComponent(); ?>
        </div>
    </div>

    <div class="d-flex">
        <div class="col-md-12 border-bottom">
            <ul class="nav nav-fill w-100 py-3">
                <li class="nav-item border-right">
                    <a class="nav-link" href="<?php echo e(url('/wingdeals')); ?>">
                        <img src="<?php echo e(asset('main/images/icons/deals.png')); ?>" alt="" class="img-fluid"
                            style="height: 50px;">
                        
                    </a>
                </li>
                <li class="nav-item border-right">
                    <a class="nav-link" href="<?php echo e(url('/event')); ?>"><img
                            src="<?php echo e(asset('main/images/icons/events.png')); ?>" alt="" class="img-fluid"
                            style="height: 50px;">

                        
                    </a>
                </li>
                <li class="nav-item border-right">
                    <a class="nav-link" href="<?php echo e(url('/newsfeeds')); ?>">
                        <img src="<?php echo e(asset('main/images/icons/news.png')); ?>" alt="" class="img-fluid"
                            style="height: 50px;">
                        <br />
                        
                    </a>
                </li>
                <li class="nav-item border-right">
                    <a class="nav-link" href="<?php echo e(url('/redeem')); ?>">
                        <img src="<?php echo e(asset('main/images/icons/wing.png')); ?>" alt="" class="img-fluid"
                            style="height: 50px;">

                        
                    </a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="https://play.google.com/store?hl=en" target="_blank">
                        <img src="<?php echo e(asset('main/images/icons/download.png')); ?>" alt="" class="img-fluid"
                            style="height: 50px;">

                        <p>
                            <h5 style="color: #134c7e">Download App</h5>
                        </p>
                    </a>
                </li>
        </div>
    </div>
    <div class="d-flex pb-3">
        <?php echo $__env->make('layouts.appmainsidebar', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>
        <div class="maincontent d-flex flex-grow-1">
            <div>
                <div class="row" style="margin-top:20px;">
                    <?php $__currentLoopData = $imagediv2_1; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $item): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                    <div class="col-md-6">
                        <div class=" ">
                            <img src="<?php echo e(asset("$item->image")); ?>" style="width:100%;height: 343px;
                                max-height: 343px;" class="img-fluid" />
                        </div>
                    </div>
                    <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                </div>


                <div class="row" style="margin-top: 20px;">

                    <?php $__currentLoopData = $imagediv3_1; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $item): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                    <div class="col-md-4">
                        <div class=" ">
                            <img src="<?php echo e(asset("$item->image")); ?>" style="width:100%;height: 307px;
                        max-height: 307px;" class="img-fluid" />
                        </div>
                    </div>
                    <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>


                </div>

                <div class="row" style="margin-top: 20px;">
                    <?php $__currentLoopData = $imagediv2_2; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $item): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                    <div class="col-md-6">
                        <div class=" ">
                            <img src="<?php echo e(asset("$item->image")); ?>" style="width:100%;height: 343px;
                        max-height: 343px;" class="img-fluid" />
                        </div>
                    </div>
                    <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>

                </div>

                <div class="row" style="margin-top: 20px;">
                    <?php $__currentLoopData = $imagediv3_2; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $item): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                    <div class="col-md-4">
                        <div class=" ">
                            <img src="<?php echo e(asset("$item->image")); ?>" style="width:100%;height: 307px;
                        max-height: 307px;" class="img-fluid" />
                        </div>
                    </div>
                    <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                </div>


                <div class="row" style="margin-top: 20px;">
                    <?php $__currentLoopData = $imagediv2_3; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $item): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                    <div class="col-md-6">
                        <div class=" ">
                            <img src="<?php echo e(asset("$item->image")); ?>" style="width:100%;height: 343px;
                        max-height: 343px;" class="img-fluid" />
                        </div>
                    </div>
                    <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>

                </div>

                <div class="row" style="margin-top: 20px;">
                    <?php $__currentLoopData = $imagediv3_3; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $item): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                    <div class="col-md-4">
                        <div class=" ">
                            <img src="<?php echo e(asset("$item->image")); ?>" style="width:100%;height: 223px;
                        max-height: 223px;" class="img-fluid" />
                        </div>
                    </div>
                    <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>

                </div>

                <div class="row" style="margin-top: 20px;">
                    <?php $__currentLoopData = $imagediv2_4; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $item): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                    <div class="col-md-6">
                        <div class=" ">
                            <img src="<?php echo e(asset("$item->image")); ?>" style="width:100%;height: 343px;
                        max-height: 343px;" class="img-fluid" />
                        </div>
                    </div>
                    <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>

                </div>
                <div class="row" style="margin-top: 20px;">
                    <?php $__currentLoopData = $imagediv2_5; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $item): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                    <div class="col-md-6">
                        <div class=" ">
                            <img src="<?php echo e(asset("$item->image")); ?>" style="width:100%;height: 343px;
                        max-height: 343px;" class="img-fluid" />
                        </div>
                    </div>
                    <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>

                </div>
                <div class="row" style="margin-top: 20px;">
                    <?php $__currentLoopData = $imagediv3_3; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $item): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                    <div class="col-md-4">
                        <div class=" ">
                            <img src="<?php echo e(asset("$item->image")); ?>" style="width:100%;height: 223px;
                        max-height: 223px;" class="img-fluid" />
                        </div>
                    </div>
                    <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>

                </div>


            </div>
        </div>
    </div>
</div>


<!--giveus feedback-->

<?php $__env->stopSection(); ?>

<?php echo $__env->make('layouts.app', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH C:\wamp64\www\offers\resources\views/welcome.blade.php ENDPATH**/ ?>