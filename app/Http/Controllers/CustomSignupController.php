<?php

namespace App\Http\Controllers;

use App\Models\Otp;
use App\Models\Setting;
use App\Models\User;
use Auth;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Session;

class CustomSignupController extends Controller
{
    public function addUser(Request $request)
    {
        // now we get our form data from Request
        // var_dump($request);die;
        $this->validate($request, [
            'first_name' => 'required|string|max:255',
            'last_name' => 'required|string|max:255',
            'email' => 'required|string|email|max:255|unique:users',
            'password' => 'required|string|min:8|confirmed',
            'mobile' => 'required|numeric|digits:10|unique:users',
            'dob' => 'nullable|before:today',
            'gender' => 'required|in:MALE,FEMALE,OTHERS',
        ]);

        $email = $request->email;
        $password = $request->password;
        $user_type = $request->user_type;
        $first_name = $request->first_name;
        $last_name = $request->last_name;
        $mobile = $request->mobile;
        $dob = date('Y-m-d', strtotime($request->dob));
        $gender = $request->gender;
        $referal_code = $request->referal_code;

        $hash_password = Hash::make($password);
        // do other process

        $referrer = User::where('referral_token', $referal_code)->whereNotNull('referral_token')->first();

        $users = new User();
        $users->email = $email;
        $users->referrer_id = $referrer ? $referrer->id : null;
        $users->password = $hash_password;
        $users->first_name = $first_name;
        $users->last_name = $last_name;
        $users->user_type = $user_type;
        $users->mobile = $mobile;
        $users->dob = $dob;
        $users->gender = $gender;
        $users->referral_token = $this->generateReferralcode($first_name);

        if ($users->save()) {
            $welcome_points = Setting::where('name', 'welcome_points')->first()->toArray();
            $points = $welcome_points['value'];
            
            $user = User::find($users->id);
            $user->deposit($points, 'deposit', ['type' => 'Welcome Points', 'description' => 'Rewarded on registration with OurSite'], 'Welcome');

            if (!empty($referrer)) {
                $referral_points = Setting::where('name', 'referral_points')->first()->toArray();
                $rpoints = $referral_points['value'];

                $referral_user = User::find($referrer->id);
                $referral_user->deposit($rpoints, 'deposit', ['type' => 'Refrral Points', 'description' => 'Rewarded on referral'], 'Referral');
            }

            $users->sendEmailVerificationNotification();

            if (Auth::guard('web')->attempt(['email' => $email, 'password' => $password, 'user_type' => $user_type])) {
                $msg = [
                    'status' => 'Success',
                    'message' => 'Login Successful',
                ];
            }
        } else {
            $msg = [
                'status' => 'error',
                'message' => 'Failed Signup',
            ];
        }

        return response()->json($msg);
    }

    public function generateRandomString($length = 10)
    {
        // $characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
        $characters = '0123456789';
        $charactersLength = strlen($characters);
        $randomString = '';
        for ($i = 0; $i < $length; ++$i) {
            $randomString .= $characters[rand(0, $charactersLength - 1)];
        }

        return $randomString;
    }

    public function generateReferralcode($name)
    {
        $refid = strtoupper($name.$this->generateRandomString(4));
        $reff = User::where('referral_token', $refid)->get()->toArray();
        if (!empty($reff)) {
            $this->generateReferralcode($name);
        } else {
            return $refid;
        }
    }

    // public function rewardpointsforreferral($referral, $user_id)
    // {
    //     $reff = User::where('refid', $referral)->first()->toArray();

    //     $referral_points = Setting::where('name', 'referral_points')->where('status', '1')->first()->toArray();
    //     $points = $referral_points['value'];

    //     $wingpoints = new WingPoints();
    //     $wingpoints->user_id = $reff['id'];
    //     $wingpoints->type = 'REFERRAL';
    //     $wingpoints->points = $points;
    //     $wingpoints->referral_used_user_id = $user_id;

    //     $wingpoints->save();

    //     return 'success';
    // }
}
