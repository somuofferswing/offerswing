<?php $__env->startSection('content'); ?>
<style>
    .img-wrap {
        position: relative;
    }

    .img-wrap .close {
        position: absolute;
        top: 2px;
        right: 2px;
        z-index: 100;
        color: red;
        background: white;
    }
</style>

<link rel="stylesheet"
    href="<?php echo e(asset('adminlte/bower_components/bootstrap-datepicker/dist/css/bootstrap-datepicker.min.css')); ?>">

<div class="content-wrapper">

    <section class="content-header">

        <h1>

            news

            <small>Add Deal</small>

        </h1>

        <!-- <ol class="breadcrumb">

              <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>

              <li class="active">Dashboard</li>

            </ol> -->

    </section>



    <section class="content">



        <div class="box box-info">

            <div class="box-header with-border">

                <h3 class="box-title">Edit Deal</h3>

            </div>

            <form class="form-horizontal" method="POST" action="<?php echo e(route('update-news',[$news->id])); ?>"
                enctype="multipart/form-data">

                <?php echo csrf_field(); ?>

                <div class="box-body">

                    <div class="form-group ">

                        <label for="title" class="col-sm-2 control-label">Title</label>

                        <div class="col-sm-10">

                            <input id="name" type="text" class="form-control" name="title" value="<?php echo e($news->title); ?>"
                                required>

                            <?php if($errors->has('title')): ?>

                            <div class="danger"><?php echo e($errors->first('title')); ?></div>

                            <?php endif; ?>

                        </div>

                    </div>

                    <div class="form-group">

                        <label for="news_image" class="col-sm-2 control-label">Image</label>

                        <div class="col-sm-10">

                            <input type="file" class="form-control-file" id="news_image" name="images[]" multiple>
                            <?php
                            $images = $news->newsImages;
                            ?>
                            <?php $__currentLoopData = $images; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $item): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                            <div class="img-wrap" style="display:inline-block;">
                                <span class="close"><a href="#">&times;</a></span>
                                <img class="img-thumbnail" src='<?php echo e(asset('storage/'.$item->image)); ?>' height="100px"
                                    width="100px">
                            </div>
                            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>


                        </div>

                    </div>

                    <div class="form-group">

                        <label for="video_link" class="col-sm-2 control-label">Video Link </label>

                        <div class="col-sm-10">
                            <input type="text" class="form-control" id="video_link" name="video_link"
                                value="<?php echo e($news->video_link); ?>">
                        </div>

                    </div>

                    <div class="form-group ">

                        <label for="description" class="col-sm-2 control-label">Description</label>

                        <div class="col-sm-10">

                            <textarea id="description" type="text" class="form-control" name="description" required
                                autocomplete="description" autofocus><?php echo e($news->description); ?></textarea>

                            <?php if($errors->has('description')): ?>

                            <div class="danger"><?php echo e($errors->first('description')); ?></div>

                            <?php endif; ?>

                        </div>

                    </div>



                    <div class="pull-right">

                        <button type="submit" class="btn btn-info"><?php echo e(__('Update')); ?></button>

                        <button type="button" class="btn btn-danger"
                            onclick="window.history.go(-1); return false;"><?php echo e(__('Cancel')); ?></button>

                    </div>



                </div>

            </form>

        </div>

    </section>

</div>

<?php $__env->stopSection(); ?>

<?php $__env->startSection('scripts'); ?>

<script src="<?php echo e(asset('adminlte/bower_components/bootstrap-datepicker/dist/js/bootstrap-datepicker.min.js')); ?>">
</script>

<script src="<?php echo e(asset('adminlte/bower_components/select2/dist/js/select2.full.min.js')); ?>"></script>

<script src="<?php echo e(asset('adminlte/bower_components/ckeditor/ckeditor.js')); ?>"></script>

<script>
    $(function () {



    CKEDITOR.replace('description')

  })

</script>

<script>
    var loadFile = function(event) {

            var output = document.getElementById('output');

            output.src = URL.createObjectURL(event.target.files[0]);

          };

</script>

<?php $__env->stopSection(); ?>
<?php echo $__env->make('layouts.admin', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH /home/offerkrx/public_html/offerswing_test/resources/views/backend/news/edit.blade.php ENDPATH**/ ?>