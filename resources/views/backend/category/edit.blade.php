@extends('layouts.admin')

@section('content')
    <div class="content-wrapper">
        <section class="content-header">
            <h1>
                Category
                <small>Add Category</small>
            </h1>
            <!-- <ol class="breadcrumb">
              <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
              <li class="active">Dashboard</li>
            </ol> -->
        </section>

        <section class="content">

            <div class="col-md-8 col-md-offset-2">
                <div class="box box-info">
                    <div class="box-header with-border">
                        <h3 class="box-title">Add Category</h3>
                    </div>
                    <form class="form-horizontal" method="POST" action="{{ route('update-category',[$category->id]) }}"  enctype="multipart/form-data">
                        @csrf
                        <div class="box-body">

                            <div class="form-group">
                                <label for="category_name" class="col-sm-2 control-label">Category Name</label>
                                <div class="col-sm-10">
                                    <input id="category_name" type="text" class="form-control" name="category_name"
                                           value="{{ $category->category_name }}" required autocomplete="category_name"
                                           autofocus >
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="parent_id" class="col-sm-2 control-label">Parent</label>

                                <div class="col-sm-10">
                                    <select class="form-control" name="parent_id" id="parent_id">
                                        <option value="">-- Select --</option>
                                    </select>
                                </div>

                            </div>
                            <div class="form-group">
                                <label for="category_image" class="col-sm-2 control-label">Image</label>
                                <div class="col-sm-10">
                                <input type="file" class="form-control-file" id="category_image" name="cat_image" onchange="loadFile(event)">
                                {{-- <img  height="100px" width="100px"/> --}}
                                <img class="img-thumbnail" src='{{ asset("storage/".$category->cat_image) }}' height="100px" width="100px" id="output">
                                </div>
                            </div>
                            <button type="submit" class="btn btn-info pull-right">{{ __('Update') }}</button>
                        </div>
                    </form>
                </div>
            </div>
        </section>
    </div>
@endsection

@section('scripts')

<script>
var loadFile = function(event) {
    var output = document.getElementById('output');
    output.src = URL.createObjectURL(event.target.files[0]);
  };

  $(document).ready(function(){
        var data = {!! json_encode($categories) !!};
        var parent_id = {{ $category->parent_id }};

        data.forEach(element => {
            id = element.id;
            value = element.category_name;
            if(id == parent_id){
                $('#parent_id').append(`<option value="${id}" selected>
                                     ${value} </option>`);

            }else{
                $('#parent_id').append(`<option value="${id}">
                                     ${value} </option>`);
            }

            if(element.children_recursive) { rloop(element.children_recursive,1); }
        });


        function rloop(data2,i){

            var prefix = '';
            for (let j = 0; j < i; j++) {
                    prefix += "-";
            }
            data2.forEach(element => {
            id = element.id;
            value = element.category_name;
            if(id == parent_id){
                $('#parent_id').append(`<option value="${id}" selected>
                                      ${prefix} ${value} </option>`);

            }else{
                $('#parent_id').append(`<option value="${id}">
                                      ${prefix} ${value} </option>`);
            }

            i++;
            if(element.children_recursive) { rloop(element.children_recursive,i); }
            });
        }

    });
</script>

@endsection
