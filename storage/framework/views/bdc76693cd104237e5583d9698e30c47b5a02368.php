<?php $__env->startSection('content'); ?>
<style>
    a.disabled {
        pointer-events: none;
        cursor: default;
    }

    .rgifts p {
        margin-bottom: 0px;
    }

    .mgin1 {
        background: white;
        -moz-box-shadow: 0 0 0px #888;
        -webkit-box-shadow: 0 0 0px#888;
        box-shadow: 0 0 0px #888;
        border-radius: 5px;
    }

    body {

        background-color: #f7f7f7;
    }
</style>


<body>
    <div class="container">
        <nav aria-label="breadcrumb">
            <ol class="breadcrumb">
                <li class="breadcrumb-item"><a href="#">Home</a></li>
                <li class="breadcrumb-item active" aria-current="page">Redeem Gifts</li>
            </ol>
        </nav>
        <h2 class="border-bottom text-center">Redeem Gifts</h2>
        <div class="py-4">
            <?php $__currentLoopData = $gifts; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $item): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
            <div class="card col-md-4">
                <img class="card-img-top img-fluid border-bottom" src="<?php echo asset("$item->image"); ?>"
                    alt="Card image cap" style="height:200px;">
                <div class="card-body">
                    <h5 class="card-title" style="color:#d83e44"><?php echo e(ucfirst($item->gift_name)); ?></h5>
                    <p class="card-text"><?php echo $item->description; ?></p>

                    <div class="row">
                        <div class="">
                            <p style="color:#d83e44">Points Required: <?php echo e($item->points_to_claim); ?></p>
                        </div>
                        <div class="ml-auto">
                            <?php if(Auth::guest()): ?>
                            <a href="" class="btn btn-danger" data-toggle="modal" data-target="#myModal1">
                                Claim Now</a>

                            <?php else: ?>
                            <?php if(Session::get('total_wingpoints')->points >= $item->points_to_claim ): ?>

                            <a href="<?php echo e(url('getgift',[$item->id])); ?>" class="btn btn-danger">
                                Claim Now</a>

                            <?php else: ?>
                            <button class="btn btn-primary" disabled>Not Enough Points </button>
                            <?php endif; ?>

                            <?php endif; ?>
                        </div>

                    </div>



                    
                </div>
            </div>

            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>

        </div>

    </div>

    <!-- <a class="btn btn-primary" >Trigger modal</a> -->
    <div class="modal fade" id="share">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-body">
                    <div class="a2a_kit a2a_kit_size_32 a2a_default_style">
                        
                        <a class="a2a_button_facebook"></a>
                        <a class="a2a_button_twitter"></a>
                        <a class="a2a_button_email"></a>
                        <a class="a2a_button_whatsapp"></a>
                    </div>
                    <script async src="https://static.addtoany.com/menu/page.js"></script>
                </div>
            </div>
        </div>
    </div>



    <?php $__env->stopSection(); ?>

    <?php $__env->startSection('scripts'); ?>
    <script>
        $(document).on('click', ".unlike", function () {
            var id = $(this).data('id');
            var type = "event";
            var element = this;

            var data = {
                    id:id,
                    type:type
                };
            $.ajax({
                    type: "post",
                    url: '<?php echo e(url("removelike")); ?>',
                    headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                     },
                    data: data,
                    cache: false,
                    success: function (data)
                    {
                        if(data.status == "success"){
                            $(element).removeClass('unlike').addClass('like').html('<i class="far fa-heart" style="color:#e31b23"></i> &nbsp;like');
                        }
                    },
                    error: function (data){
                        alert('Fail to run unlike..');
                    }
                });


        });
        $(document).on('click', ".like", function () {
            var id = $(this).data('id');
            var type = "event";
            var element = this;

            var data = {
                    id:id,
                    type:type
                };
            $.ajax({
                    type: "post",
                    url: '<?php echo e(url("addlike")); ?>',
                    headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                     },
                    data: data,
                    cache: false,
                    success: function (data)
                    {
                        console.log(data);
                        if(data.status == "success"){
                                $(element).removeClass('like').addClass('unlike').html('<i class="fas fa-heart" style="color:#e31b23"></i> &nbsp;Unlike');

                        }
                    },
                    error: function (data){
                        alert('Fail to run like..');
                    }
                });


        });
    </script>


    <?php $__env->stopSection(); ?>
</body>

<?php echo $__env->make('layouts.app', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH C:\wamp64\www\offers\resources\views/redeem.blade.php ENDPATH**/ ?>