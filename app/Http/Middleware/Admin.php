<?php



namespace App\Http\Middleware;

use Closure;
use Auth;

class Admin
{
    public function handle($request, Closure $next)
    {
        if (Auth::user()->user_type == 1 || Auth::user()->user_type == 3) {

            return $next($request);
        }

        return redirect()->route('home'); // If user is not an admin.
    }
}
