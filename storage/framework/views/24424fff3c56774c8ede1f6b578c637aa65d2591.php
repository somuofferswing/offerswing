<?php $__env->startSection('content'); ?>

<div class="content-wrapper">

    <section class="content-header">

        <h1>

            Amenity

            <small>All Amenities</small>

        </h1>

        <!-- <ol class="breadcrumb">

              <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>

              <li class="active">Dashboard</li>

            </ol> -->

    </section>

    <section class="content">

        <div class="box box-primary">

            <div class="box-header with-border">

                <h3 class="box-title">All Categories</h3>

                <a href="<?php echo url('/amenity/create');; ?>" class="btn btn-primary pull-right">Add Amenity</a>

            </div>

            <div class="table-responsive">

                <table class="table table-striped">

                    <thead>

                        <tr>

                            <th>S.No</th>

                            <th>Name</th>

                            <th>Image</th>

                            

                            <th colspan="2">Action</th>

                        </tr>

                    </thead>

                    <tbody>

                        <?php $__currentLoopData = $amenities; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $key=>$amenity): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>

                        <tr>

                            <td><?php echo e($key+1); ?></td>

                            <td><?php echo e($amenity->name); ?></td>

                            <td><img class="img-thumbnail" src='<?php echo e(asset('storage/'.$amenity->image)); ?>' height="75px"
                                    width="75px"></td>

                            



                            

                            <td><a class="btn btn-default" href="<?php echo e(route('edit-amenity', [$amenity->id])); ?>">Edit</a>
                            </td>



                            <td><a class="btn btn-danger del" data-sub="false" data-id="<?php echo e($amenity->id); ?>">Delete</a>
                            </td>





                        </tr>

                        <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>

                    </tbody>

                </table>

            </div>

        </div>



    </section>

</div>

<?php $__env->stopSection(); ?>

<?php $__env->startSection('scripts'); ?>

<script>
    $('.del').on('click',function(){

    var bool = $(this).data('sub');
    var id = $(this).data('id');
    if(!bool){
        $.ajax({
            type: "POST",
            url: "<?php echo e(url('amenity/delete/')); ?>"+"/"+id,
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            },
            success: function (response) {
                alert(response);
                window.location.reload();
            }
        });

    }
});

</script>



<?php $__env->stopSection(); ?>
<?php echo $__env->make('layouts.admin', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH C:\wamp64\www\offers\resources\views/backend/amenities/index.blade.php ENDPATH**/ ?>