@extends('layouts.app')

@section('content')

<div class="panel panel-default">
    <div class="panel-body">

    </div>
    <div class="panel-footer">
        <a class="btn like" data-id="{{ $item->id }}"><i class="far fa-heart" style="color:#e31b23"></i>
            &nbsp;Like</a>
        <a class="btn" data-toggle="modal" href='#share'><i class="fa fa-thumbs-up"></i>share</a>
    </div>
</div>

@endsection


@section('scripts')
<script>

</script>
@endsection
