@extends('layouts.admin')

@section('content')
<div class="content-wrapper">
    <section class="content-header">
        <h1>
            Users
            <small>All Users</small>
        </h1>
        <!-- <ol class="breadcrumb">
              <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
              <li class="active">Dashboard</li>
            </ol> -->
    </section>
    <section class="content">

        <div class="box box-primary">
            <div class="box-header with-border">
                <h3 class="box-title">All Users</h3>
            </div>
            <div class="table-responsive">
                <table class="table table-striped">
                    <thead>
                        <tr>
                            <th>ID</th>
                            <th>User type</th>
                            <th>First Name</th>
                            <th>Last Name</th>
                            <th>Mobile</th>
                            <th>Email</th>
                            <th colspan="2">Action</th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach($users as $user)
                        <tr>
                            <td>{{$user->id}}</td>
                            <td>
                                @if ($user->user_type == 1)
                                <b>Admin</b>
                                @endif

                                @if ($user->user_type == 2)
                                <b>User</b>
                                @endif

                                @if ($user->user_type == 3)
                                <b>Retailer</b>
                                @endif
                            </td>
                            <td>{{$user->first_name}}</td>
                            <td>{{$user->last_name}}</td>
                            <td>{{$user->mobile}}</td>
                            <td>{{$user->email}}</td>
                            <td>
                                <form action="{{ url('/user/retrive', [$user->id]) }}" method="POST" onSubmit="if(!confirm('Are you sure you want to retrive User back?')){return false;}">
                                    @csrf
                                    <button class="btn btn-default" type="submit">Retrive</button>
                                </form>
                            <td>
                                <form action="{{ url('/user/delete', [$user->id]) }}" method="POST" onSubmit="if(!confirm('Are you sure you want to delete User Permanently?')){return false;}">
                                    @csrf
                                    <button class="btn btn-danger" type="submit">Delete</button>
                                </form>
                            </td>
                        </tr>
                        @endforeach
                    </tbody>
                </table>
            </div>
        </div>
    </section>
</div>
@endsection

@section('scripts')
<script>

</script>
@endsection
