<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

// Route::middleware('auth:api')->get('/user', function (Request $request) {
//     return $request->user();
// });

Route::post('register', 'API\UserController@register');
Route::post('login', 'API\UserController@login');
Route::get('appindex', 'API\HomeController@index');

Route::get('categories', 'API\CategoryController@mainCategories');
Route::get('subcategories/{id}', 'API\CategoryController@getsubCategories');

Route::get('shops', 'API\ShopController@index');
Route::get('shopsbycategory/{id}', 'API\ShopController@getShopsbyCategory')->name('shopsbycategory');
Route::get('shopdetails/{id}', 'API\ShopController@getShopDetails');

Route::get('offers', 'API\OffersController@index');
Route::get('shopdetails/{id}', 'API\OffersController@getOffersDetails');
Route::get('shopsbycategory/{id}', 'API\OffersController@getShopsbyCategory')->name('shopsbycategory');

Route::middleware('auth:api')->group(function () {
    Route::get('logout', 'API\UserController@logout');
});
