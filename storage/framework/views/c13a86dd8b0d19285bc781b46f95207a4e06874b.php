

<?php $__env->startSection('content'); ?>
<div class="content-wrapper">
    <section class="content-header">
        <h1>
            News Feed
            <small>All NewsFeed</small>
        </h1>
        <!-- <ol class="breadcrumb">
              <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
              <li class="active">Dashboard</li>
            </ol> -->
    </section>
    <section class="content">
        <?php if(session()->get('success')): ?>
        <div class="alert alert-success" id="success">
            <?php echo e(session()->get('success')); ?>

        </div><br />
        <?php endif; ?>

        <div class="box box-primary">
            <div class="box-header with-border">
                <h3 class="box-title">All NewsFeed</h3>
                <a href="<?php echo url('/faq/create');; ?>" class="pull-right btn btn-info">Add Faq</a>
            </div>

            <div class="table-responsive">
                <table class="table table-striped">
                    <thead>

                        <tr>
                            <th>ID</th>
                            <th>Question</th>
                            <th>Answer</th>

                            <th colspan="2">Action</th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php $__currentLoopData = $faq; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $faq): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                        <tr>
                            <td><?php echo e($faq->id); ?></td>
                            <td><?php echo e($faq->question); ?></td>
                            <td><?php echo $faq->answer; ?></td>

                            <td><a class="btn btn-default" href="<?php echo e(url('/faq/edit', [$faq->id])); ?>">Edit</a></td>

                            <td>
                                <form action="<?php echo e(url('/faq/delete',[$faq->id])); ?>" method="POST">
                                    <?php echo csrf_field(); ?>
                                    
                                    <button class="btn btn-danger" type="submit">Delete</button>
                                </form>
                            </td>
                        </tr>
                        <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                    </tbody>
                </table>
            </div>
        </div>
    </section>
</div>
<?php $__env->stopSection(); ?>
<?php $__env->startSection('scripts'); ?>
<?php $__env->stopSection(); ?>

<?php echo $__env->make('layouts.admin', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH /home/offerkrx/public_html/offerswing/resources/views/faq/index.blade.php ENDPATH**/ ?>