<div class="sidebar ml-2 mr-3" id="sidebar">
    <ul class="nav flex-column sidebarboxshadow" style="background: white;">
        <img src="<?php echo e(asset('main/images/icons/cat.png')); ?>" alt="" class="img-fluid" width="100%">
        <?php $__currentLoopData = $categories; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $item): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
        <li class="nav-item  <?php if(request()->route('id') == $item->id): ?>
            active
            <?php endif; ?> ">
            
            <a href=" <?php echo url('/subcategories',[$item->id]);; ?>" class="nav-link">
                <img src="<?php echo asset("$item->cat_image"); ?>" height="30px" width="30px" class="img-fluid">
                <?php echo e(ucfirst(strtolower($item->category_name))); ?>

            </a>
        </li>
        <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
    </ul>
</div>
<?php /**PATH C:\xampp\htdocs\offerswing\resources\views/layouts/appmainsidebar.blade.php ENDPATH**/ ?>