<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class Deal extends Model
{
    protected $fillable = [];

    public static function getExpiredDeals()
    {
        $date = date('Y-m-d');

        return DB::table('deals')->where('valid_upto', '<', $date)->get();
    }

    public function shop()
    {
        return $this->belongsTo(Shop::class, 'shop_id', 'id');
    }

    public static function dealOrders($id)
    {
        return Order::where('type', 'deal')->where('type_id', $id)->get();
    }

    public function orders()
    {
        return $this->hasMany(Order::class);
    }

    // public function category()
    // {
    //     return $this->belongsToMany(Category::class);
    // }
}
