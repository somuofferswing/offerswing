@extends('layouts.admin')

@section('content')
<div class="content-wrapper">
    <section class="content-header">
        <h1>
            News Feed
            <small>All NewsFeed</small>
        </h1>
        <!-- <ol class="breadcrumb">
              <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
              <li class="active">Dashboard</li>
            </ol> -->
    </section>
    <section class="content">
        @if(session()->get('success'))
        <div class="alert alert-success" id="success">
            {{ session()->get('success') }}
        </div><br />
        @endif

        <div class="box box-primary">
            <div class="box-header with-border">
                <h3 class="box-title">All NewsFeed</h3>
                <a href="{!! url('/faq/create'); !!}" class="pull-right btn btn-info">Add Faq</a>
            </div>

            <div class="table-responsive">
                <table class="table table-striped">
                    <thead>

                        <tr>
                            <th>ID</th>
                            <th>Question</th>
                            <th>Answer</th>

                            <th colspan="2">Action</th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach($faq as $faq)
                        <tr>
                            <td>{{$faq->id}}</td>
                            <td>{{$faq->question}}</td>
                            <td>{!! $faq->answer !!}</td>

                            <td><a class="btn btn-default" href="{{ url('/faq/edit', [$faq->id]) }}">Edit</a></td>

                            <td>
                                <form action="{{ url('/faq/delete',[$faq->id]) }}" method="POST">
                                    @csrf
                                    {{-- @method('DELETE') --}}
                                    <button class="btn btn-danger" type="submit">Delete</button>
                                </form>
                            </td>
                        </tr>
                        @endforeach
                    </tbody>
                </table>
            </div>
        </div>
    </section>
</div>
@endsection
@section('scripts')
@endsection
