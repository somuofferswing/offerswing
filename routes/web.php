<?php

/*

|--------------------------------------------------------------------------

| Web Routes

|--------------------------------------------------------------------------

|

| Here is where you can register web routes for your application. These

| routes are loaded by the RouteServiceProvider within a group which

| contains the "web" middleware group. Now create something great!

|

 */

// Route::get('/', function () {

//     return view('welcome');

// });

Route::get('/admin', function () {
    if (Auth::check()) {
        return redirect()->route('dashboard');
    }

    return view('backend/admin/adminlogin');
});

Route::group(['middleware' => 'after'], function () {
    Auth::routes(['verify' => true]);

    Route::get('/m2', 'HomeController@m2')->name('M2');

    Route::get('/', 'HomeController@index')->name('Mainpage');

    Route::get('/home', 'HomeController@index')->name('home');

    Route::get('/aboutus', 'HomeController@aboutus')->name('aboutus');

    Route::get('/contactus', 'HomeController@contactus')->name('contactus');

    Route::get('/addyourbusiness', 'HomeController@addyourbusiness')->name('addyourbusiness');

    Route::get('/faq', 'HomeController@faq')->name('faq');


    Route::get('/wingpoints', 'HomeController@wingpoints');

    Route::get('/wingdeals', 'HomeController@wingdeals');

    Route::get('/redeem', 'HomeController@redeem');

    Route::get('/favouriteshops', 'HomeController@allfavouriteshops');

    Route::get('/newsfeeds', 'HomeController@newsfeed')->name('newsfeeds');

    Route::get('/event', 'HomeController@events')->name('event');

    Route::get('/subcategories/{slug}', 'HomeController@subCategories')->name('sub-categories');

    Route::get('/categoryshops/{slug}', 'HomeController@shopsByCategory')->name('sub-categories-shops');

    Route::get('/viewshop/{slug}', 'HomeController@shopDetails')->name('shop-details');

    Route::get('/dealdetails/{slug}', 'HomeController@getDealDetails')->name('deal-details');


    Route::get('/addtocart/{deal_id}', 'HomeController@addtoCart')->name('addtocart');

    Route::post('/customlogin', 'CustomLoginController@loginUser');

    Route::post('/customforgotpassword', 'CustomLoginController@forgotPassword');

    Route::post('/loginSendOtp', 'CustomLoginController@loginSendOtp');

    Route::post('/resendOTP', 'CustomLoginController@resendOTP');

    Route::post('/customregister', 'CustomSignupController@addUser');

    Route::post('/verifyotp', 'CustomLoginController@verifyOtp');

    Route::post('/addlike', 'HomeController@addlike');
    Route::post('/notifications', 'HomeController@getNotifications');

    Route::post('/removelike', 'HomeController@removelike');

    Route::post('/addfavouriteshop', 'HomeController@addfavouriteshop');

    Route::post('/removefavouriteshop', 'HomeController@removefavouriteshop');

    Route::get('/userprofile', 'HomeController@userprofile')->middleware('verified');

    Route::get('/getdeal/{id}', 'DealsController@getDeal');

    Route::get('/orderdeal/{id}', 'DealsController@dealOrder')->name('dealorder');

    Route::get('/getevent/{id}', 'EventsController@getEvent');

    Route::get('/orderevent/{id}', 'EventsController@eventOrder')->name('eventorder');

    Route::get('/getgift/{id}', 'GiftsController@getGift');

    Route::get('/ordergift/{id}', 'GiftsController@giftOrder')->name('giftorder');

    Route::post('/uploadprofileimage', 'HomeController@ajaximageupload')->name('uploadprofileimage');

    Route::post('/updateprofile', 'HomeController@updateprofile');

    Route::post('/updatepassword', 'HomeController@update_password');

    Route::post('/contact_post', 'HomeController@contact_post');

    Route::post('/addyourbusiness_post', 'HomeController@addyourbusiness_post');

    Route::get('/orders', 'HomeController@orders')->middleware('auth');

    Route::get('/order/{order_id}', 'HomeController@order')->name('user_order')->middleware('auth');

    Route::post('/updateorder', 'HomeController@updateorder');

    Route::post('/deleteorder', 'HomeController@deleteorder');

    Route::get('/proceedtopay/{id}', 'PaymentController@proceedtopay')->middleware('auth');

    Route::post('/indipay/response', 'PaymentController@indipay_response');

    Route::get('/claimed/{id}', 'HomeController@claimed')->name('claimed')->middleware('auth');

    Route::get('/gift_claimed/{id}', 'HomeController@giftclaimed')->name('claimed')->middleware('auth');

    Route::get('/referafriend', 'HomeController@referafriend')->middleware('auth');

    Route::get('/eventdetails/{id}', 'HomeController@eventdetails');

    Route::get('/search', 'HomeController@search');

    Route::get('autocomplete', 'SearchController@autocomplete')->name('autocomplete');

    Route::post('/token/redeem', 'TokenController@redeemToken');

    Route::get('/page/{page_name}', 'HomeController@getPage');
});

Route::group(['middleware' => 'admin'], function () {
    Route::get('/admin/home', 'Backend\AdminController@index')->name('dashboard');

    Route::get('/admin/profile', 'Backend\AdminController@profile');

    Route::get('/admin/settings', 'Backend\AdminController@settings');

    Route::post('/admin/savesettings', 'Backend\AdminController@savesettings');

    Route::get('/category/create', 'Backend\CategoriesController@create')->name('create-category');

    Route::post('/category/store', 'Backend\CategoriesController@store')->name('store-category');

    Route::get('/category/view/{id}', 'Backend\CategoriesController@show')->name('view-category');

    Route::get('/categories', 'Backend\CategoriesController@index')->name('Categories');

    Route::get('/edit/category/{id}', 'Backend\CategoriesController@edit')->name('edit-category');

    Route::post('/edit/category/{id}', 'Backend\CategoriesController@update')->name('update-category');

    Route::post('/category/delete/{id}', 'Backend\CategoriesController@delete')->name('delete-category');

    Route::get('/shop/create', 'Backend\ShopController@create')->name('create-shop');

    Route::post('/shop/store', 'Backend\ShopController@store')->name('store-shop');

    Route::get('/shop/view/{id}', 'Backend\ShopController@show')->name('view-shop');

    Route::get('/shop/edit/{id}', 'Backend\ShopController@edit')->name('edit-shop');

    Route::post('/shop/update/{id}', 'Backend\ShopController@update')->name('update-shop');

    Route::post('/shop/delete/{id}', 'Backend\ShopController@delete')->name('delete-shop');

    Route::get('/shops', 'Backend\ShopController@index')->name('shops');

    Route::get('/token/create', 'Backend\TokenController@create');

    Route::post('/token/store', 'Backend\TokenController@store');

    Route::get('/token/view/{id}', 'Backend\TokenController@show');

    Route::get('/token/edit/{id}', 'Backend\TokenController@edit');

    Route::post('/token/update/{id}', 'Backend\TokenController@update');

    Route::post('/token/delete/{id}', 'Backend\TokenController@destroy');

    Route::get('/tokens', 'Backend\TokenController@index');

    Route::get('/deal/create', 'Backend\DealsController@create')->name('create-deal');

    Route::post('/deal/store', 'Backend\DealsController@store')->name('store-deal');

    Route::get('/deal/view/{id}', 'Backend\DealsController@show')->name('view-deal');

    Route::get('/deal/edit/{id}', 'Backend\DealsController@edit')->name('edit-deal');

    Route::post('/deal/update/{id}', 'Backend\DealsController@update')->name('update-deal');

    Route::delete('/deal/delete/{id}', 'Backend\DealsController@destroy')->name('delete-deal');

    Route::get('/deals', 'Backend\DealsController@index')->name('deals');

    Route::get('/deal/claims/{id}', 'Backend\DealsController@claims');

    Route::get('/offer/create', 'Backend\OffersController@create')->name('create-offer');

    Route::post('offer/store', 'Backend\OffersController@store')->name('store-offer');

    Route::get('/offer/view/{id}', 'Backend\OffersController@show')->name('view-offer');

    Route::get('/offer/edit/{id}', 'Backend\OffersController@edit')->name('edit-offer');

    Route::get('/offer/claims/{id}', 'Backend\OffersController@claims');

    Route::post('/offer/update/{id}', 'Backend\OffersController@update')->name('update-offer');

    Route::delete('/offer/delete/{id}', 'Backend\OffersController@destroy')->name('delete-offer');

    Route::get('/offers', 'Backend\OffersController@index')->name('offers');

    Route::get('/news/create', 'Backend\NewsfeedController@create')->name('create-news');

    Route::post('news/store', 'Backend\NewsfeedController@store')->name('store-news');

    Route::get('/news/view/{id}', 'Backend\NewsfeedController@show')->name('view-news');

    Route::get('/news/edit/{id}', 'Backend\NewsfeedController@edit')->name('edit-news');

    Route::post('/news/update/{id}', 'Backend\NewsfeedController@update')->name('update-news');

    Route::delete('/news/delete/{id}', 'Backend\NewsfeedController@destroy')->name('delete-news');

    Route::get('/news', 'Backend\NewsfeedController@index')->name('news');

    Route::get('/events/create', 'Backend\EventsController@create')->name('create-events');

    Route::post('/events/store', 'Backend\EventsController@store')->name('store-events');

    Route::get('/events/view/{id}', 'Backend\EventsController@show')->name('view-events');

    Route::get('/events/edit/{id}', 'Backend\EventsController@edit')->name('edit-events');

    Route::post('/events/update/{id}', 'Backend\EventsController@update')->name('update-events');

    Route::delete('/events/delete/{id}', 'Backend\EventsController@destroy')->name('delete-events');

    Route::get('/events', 'Backend\EventsController@index')->name('events');

    Route::get('/orders/create', 'Backend\OrderController@create');

    Route::post('/orders/store', 'Backend\OrderController@store');

    Route::get('/orders/view/{id}', 'Backend\OrderController@show');

    Route::get('/orders/edit/{id}', 'Backend\OrderController@edit');

    Route::post('/orders/update/{id}', 'Backend\OrderController@update');

    Route::delete('/orders/delete/{id}', 'Backend\OrderController@destroy');

    // Route::get('/orders', 'OrderController@index');

    // Route::get('/orders', 'OrderController@index');

    Route::get('/users', 'Backend\AdminController@getUsers');

    Route::get('/allusers', 'Backend\UserController@index');

    Route::get('/alldeleted', 'Backend\UserController@alldeleted');

    Route::get('/admins', 'Backend\AdminController@getAdmins');

    Route::get('/retailers', 'Backend\AdminController@getRetailers');

    Route::get('/adduser', 'Backend\UserController@create');

    Route::post('/storeuser', 'Backend\UserController@store');

    Route::get('/user/edit/{id}', 'Backend\UserController@edit');

    Route::post('/updateuser/{id}', 'Backend\UserController@update');

    Route::post('/user/retrive/{id}', 'Backend\UserController@retrive');

    Route::post('/user/delete/{id}', 'Backend\UserController@destroy');

    Route::post('/user/deleteuser/{id}', 'Backend\UserController@delete');

    Route::post('/user/deactivateUser/{id}', 'Backend\UserController@deactivateUser');

    Route::post('/user/activateUser/{id}', 'Backend\UserController@activateUser');

    //    Route::get('image/upload','ImageUploadController@fileCreate');

    Route::post('image/upload/store', 'Backend\ImageUploadController@fileStore');

    Route::post('image/delete', 'Backend\ImageUploadController@fileDestroy');

    Route::post('image/delete/{id}', 'Backend\ImageUploadController@destroy')->name('delete-image');

    Route::get('/faq/create', 'Backend\FaqController@create')->name('create-faq');

    Route::post('/faq/store', 'Backend\FaqController@store')->name('store-faq');

    Route::get('/faq/view/{id}', 'Backend\FaqController@show')->name('view-faq');

    Route::get('/faqs', 'Backend\FaqController@index')->name('faq');

    Route::get('/faq/edit/{id}', 'Backend\FaqController@edit')->name('edit-faq');

    Route::post('/faq/edit/{id}', 'Backend\FaqController@update')->name('update-faq');

    Route::post('/faq/delete/{id}', 'Backend\FaqController@destroy');

    Route::get('/gift/create', 'Backend\GiftsController@create')->name('create-gift');

    Route::post('/gift/store', 'Backend\GiftsController@store')->name('store-gift');

    Route::get('/gift/view/{id}', 'Backend\GiftsController@show')->name('view-gift');

    Route::get('/gifts', 'Backend\GiftsController@index')->name('gifts');

    Route::get('/gift/edit/{id}', 'Backend\GiftsController@edit')->name('edit-gift');

    Route::post('/gift/edit/{id}', 'Backend\GiftsController@update')->name('update-gift');

    Route::post('/gift/delete/{id}', 'Backend\GiftsController@destroy');

    Route::get('/slider/create', 'Backend\SliderController@create');

    Route::post('/slider/store', 'Backend\SliderController@store');

    Route::get('/slider/edit/{id}', 'Backend\SliderController@edit');

    Route::post('/slider/update/{id}', 'Backend\SliderController@update');

    Route::get('/imagediv/create', 'Backend\ImageDivController@create');

    Route::post('/imagediv/store', 'Backend\ImageDivController@store');

    Route::get('/imagediv/edit/{id}', 'Backend\ImageDivController@edit');

    Route::post('/imagediv/update/{id}', 'Backend\ImageDivController@update');

    Route::get('/pages/create', 'Backend\PagesController@create');

    Route::post('/pages/store', 'Backend\PagesController@store');

    Route::get('/pages/view/{id}', 'Backend\PagesController@show');

    Route::get('/pages', 'Backend\PagesController@index');

    Route::get('/pages/edit/{id}', 'Backend\PagesController@edit');

    Route::post('/pages/edit/{id}', 'Backend\PagesController@update');

    Route::post('/pages/delete/{id}', 'Backend\PagesController@destroy');

    Route::get('/shopdeals/{id}', 'Backend\DealsController@shopDeals');

    Route::get('/shopoffers/{id}', 'Backend\OffersController@shopOffers');

    Route::get('/viewshopdeal/{id}', 'Backend\DealsController@viewShopDeal');

    Route::post('/claimStatus', 'Backend\ClaimedController@claimStatus');


    Route::get('/amenities', 'Backend\AmenityController@index');
    Route::get('/amenity/create', 'Backend\AmenityController@create')->name('create-amenity');
    Route::post('amenity/store', 'Backend\AmenityController@store')->name('store-amenity');
    Route::get('/amenity/view/{id}', 'Backend\AmenityController@show')->name('view-amenity');
    Route::get('/amenity/edit/{id}', 'Backend\AmenityController@edit')->name('edit-amenity');
    Route::post('/amenity/update/{id}', 'Backend\AmenityController@update')->name('update-amenity');
    Route::post('/amenity/delete/{id}', 'Backend\AmenityController@destroy')->name('delete-amenity');
});
