

<?php $__env->startSection('content'); ?>
<div class="ps-page--single" id="contact-us">
        <div class="ps-breadcrumb">
            <div class="container">
                <ul class="breadcrumb">
                    <li><a href="<?php echo e(url('/')); ?>">Home</a></li>
                    <li>Contact Us</li>
                </ul>
            </div>
        </div>

        <div>
            <iframe
                src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3788.0543204118726!2d83.89259651489675!3d18.29912388068506!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x3a3c156b1b0f18e9%3A0xfb3a50c82c88f058!2sEasywing+Private+Limited!5e0!3m2!1sen!2sin!4v1562417480601!5m2!1sen!2sin"
                width="100%" height="450" frameborder="0"
                allowfullscreen></iframe>
        </div>

        <div class="ps-contact-info">
            <div class="container">
                <div class="ps-section__header">
                    <h3 class="defaultColor">GET IN TOUCH WITH US</h3>
                </div>
                <div class="ps-section__content">
                    <div class="row">
                        <div class="col-xl-4 col-lg-4 col-md-4 col-sm-6 col-12 ">
                            <div class="ps-block--contact-info">
                                <h4>Company</h4>
                                <p>Easywing Pvt. Ltd</p>
                            </div>
                        </div>
                        <div class="col-xl-4 col-lg-4 col-md-4 col-sm-6 col-12 ">
                            <div class="ps-block--contact-info">
                                <h4>Meet us here</h4>
                                <p><span>D No:6-8-4 Jabba Street, Near Krishna Park, Oppo: KVB bank, Krishna Park Area, Ranasthali, New Colony, Srikakulam, Andhra Pradesh 532001</span></p>
                            </div>
                        </div>
                        <div class="col-xl-4 col-lg-4 col-md-4 col-sm-6 col-12 ">
                            <div class="ps-block--contact-info">
                                <h4>Contact us on</h4>
                                <p><i class="fa fa-envelope"></i> info@offerswing.com<br>
                                    <i class="fa fa-phone"></i> (+91) 97049 89697<br><i class="fa fa-whatsapp"></i> (+91) 9704 04 9596</p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="ps-contact-form">
            <div class="container">
                <form class="ps-form--contact-us" action="<?php echo e(url('contact_post')); ?>" method="post" id="contactForm" name="contact-form" data-toggle="validator">
                    <h3 class="defaultColor">Please fill out the following from to get queries</h3>
                    <?php echo csrf_field(); ?>
                    <div class="row">
                        <input type="hidden" name="w_email" value="syedspgon@gmail.com">
                        <div class="col-xl-6 col-lg-6 col-md-6 col-sm-12 col-12 ">
                            <div class="form-group">
                                <input class="form-control" type="text" id="name" name="name" placeholder="Name *" required>
                                <div class="help-block with-errors"></div>
                            </div>
                        </div>
                        <div class="col-xl-6 col-lg-6 col-md-6 col-sm-12 col-12 ">
                            <div class="form-group">
                                <input class="form-control" type="phone" id="phone" name="phone" placeholder="Mobile Number *" required>
                                <div class="help-block with-errors"></div>
                            </div>
                        </div>
                        <div class="col-xl-6 col-lg-6 col-md-6 col-sm-12 col-12 ">
                            <div class="form-group">
                                <input class="form-control" type="email" id="email" name="email"  placeholder="Email *" required>
                                <div class="help-block with-errors"></div>
                            </div>
                        </div>
                        <div class="col-xl-6 col-lg-6 col-md-6 col-sm-12 col-12 ">
                            <div class="form-group">
                                <input class="form-control" type="text" id="msg_subject" name="subject" placeholder="Subject *" required>
                                <div class="help-block with-errors"></div>
                            </div>
                        </div>
                        <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12 ">
                            <div class="form-group">
                                <textarea class="form-control" id="message" name="message" rows="5" placeholder="Message" required></textarea>
                                <div class="help-block with-errors"></div>
                            </div>
                        </div>
                    </div>
                    <div class="form-group submit">
                        <button class="ps-btn" id="form-submit">Send message</button>
                        <div id="msgSubmit" class="h3 text-center hidden"></div>
                        <div class="clearfix"></div>
                    </div>
                    <div class="d-flex justify-content-center">
                        <div class="sk-flow d-none" id="contactSpinner">
                            <div class="sk-flow-dot"></div>
                            <div class="sk-flow-dot"></div>
                            <div class="sk-flow-dot"></div>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>

    <script>
        $('#contactForm').submit(function(){
            $('#form-submit').hide();
            $('#contactSpinner').removeClass('d-none');
        });
    </script>

<?php $__env->stopSection(); ?>

<?php echo $__env->make('layouts.app', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH /home/offerkrx/public_html/offerswing_test/resources/views/frontend/contactus.blade.php ENDPATH**/ ?>