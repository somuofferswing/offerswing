@extends('layouts.admin')

@section('content')
    <div class="content-wrapper">
        <section class="content-header">
            <h1>
                Shops
                <small>All Shops</small>
            </h1>
            <!-- <ol class="breadcrumb">
              <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
              <li class="active">Dashboard</li>
            </ol> -->
        </section>
        <section class="content">

            <div class="box box-primary">
                <div class="box-header with-border">
                    <h3 class="box-title">All Shops</h3>
                </div>
                <div class="table-responsive">
                    <table class="table table-striped">
                        <thead>
                        <tr>
                            <th>Shop ID</th>
                            <th>Shop Name</th>
                            <th>Email</th>
                            <th>Mobile</th>
                            <th>Address</th>
                            <th>City</th>
                            <th>State</th>
                            <th colspan="3">Action</th>
                        </tr>
                        </thead>
                        <tbody>
                        @foreach($shops as $shop)
                            <tr>
                                <td>{{ $shop->id }}</td>
                                <td>{{ $shop->shop_name }}</td>
                                <td>{{ $shop->email }}</td>
                                <td>{{ $shop->phone_number }}</td>
                                <td>{{ $shop->address }}</td>
                                <td>{{ $shop->city }}</td>
                                <td>{{ $shop->state }}</td>
                                <td><a href="{{ route('view-shop', [$shop->id]) }}" class="btn btn-primary">View</a></td>
                                <td><a href="{{ route('edit-shop', [$shop->id]) }}" class="btn btn-default">Edit</a></td>
                                <td><a class="btn btn-danger del" data-sub="false" data-id="{{$shop->id}}">Delete</a></td>

                                {{-- <td><a href="{{ route('delete-shop', [$shop->id]) }}" class="btn btn-danger">Delete</a></td> --}}
{{--                                <td>Delete</td>--}}
                            </tr>
                        @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
    </section>
    </div>
@endsection
@section('scripts')

<script>
$('.del').on('click',function(){
        var id = $(this).data('id');
        if (confirm('Do you want to delete shop?')) {
            // alert('Thanks for confirming');
            $.ajax({
                type: "POST",
                url: "{{  url('shop/delete/') }}"+"/"+id,
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
                success: function (response) {
                    // console.log(response);
                    alert(response);
                    window.location.reload();
                }
            });
        } else {

        }

});
</script>

@endsection
