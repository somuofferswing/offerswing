<?php $__env->startSection('content'); ?>
<style>
    /* .mgin1 {
        background: white;
        -moz-box-shadow: 0 0 0px #888;
        -webkit-box-shadow: 0 0 0px#888;
        box-shadow: 0 0 0px #888;
        border-radius: 5px;
    }

    .panel-body {
        padding: 0px;
    } */
</style>

<div class="container">
    <nav aria-label="breadcrumb">
        <ol class="breadcrumb">
            <li class="breadcrumb-item"><a href="/">Home</a></li>
            <li class="breadcrumb-item active" aria-current="page">Wing Deals</li>
        </ol>
    </nav>
    <h4 style="font-weight: 800;text-align: center; " class="wng">Wing Deals</h4>
    <hr style="border-bottom:2px solid #df685d;width:5%;margin:0px auto;">

    <div class="row py-3">
        <?php $__currentLoopData = $deals; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $key=>$item): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
        <div class="col-md-4">
            <div class="card">
                <img class="card-img-top border-bottom" src='<?php echo asset("$item->pic"); ?>' alt="Card image cap">
                <div class="card-body">
                    <h5 class="card-title" style="color:#df685d; font-weight:600;"><?php echo e(ucfirst($item->name)); ?></h5>
                    
                    <div class="row p-3">
                        <div>
                            <b>Valid Upto: </b> <?php echo e($item->valid_upto); ?>

                        </div>
                        <div class="ml-auto">
                            <b>Flat <?php echo e($item->discount_percentage); ?> % off* </b>
                        </div>
                    </div>
                    <div class="row p-3">
                        <a type="button" class="btn btn-default dealdetails mr-auto"
                            data-id="<?php echo e($item->id); ?>">Details</a>
                        
                        <?php if(Auth::guest()): ?>
                        <button class="btn btn-primary" data-toggle="modal" data-target="#myModal1" style="background-color:#df685d;">Claim
                            Now</button>
                        <?php else: ?>
                        <?php if($item->payment_type != 'wingpoints'): ?>
                        <a href="<?php echo e(url('/getdeal',[ $item->id])); ?>" class="btn btn-danger"
                            data-id="<?php echo e($item->id); ?>">Claim
                            Now</a>

                        <?php else: ?>

                        <?php if(Session::get('total_wingpoints')->points >= $item->price): ?>
                        <a href="<?php echo e(url('/getdeal',[ $item->id])); ?>" class="btn btn-danger"
                            data-id="<?php echo e($item->id); ?>">Claim
                            Now</a>
                        <?php else: ?>

                        <button class="btn btn-default" disabled> Not Enough Points </button>
                        <?php endif; ?>

                        <?php endif; ?>

                        <?php endif; ?>

                    </div>

                </div>
            </div>
        </div>
        <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
    </div>

</div>

<!-- Modal -->
<div class="modal fade" id="dealdetails" tabindex="-1" role="dialog" aria-labelledby="modelTitleId" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title">Modal title</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
            </div>
            <div class="modal-body">
                Body
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                
            </div>
        </div>
    </div>
</div>



<?php $__env->stopSection(); ?>

<?php $__env->startSection('scripts'); ?>
<script>
    $('.dealdetails').click(function () {
          let id =  $(this).data('id');

        $.ajax({
          type: "GET",
          url: "<?php echo e(url('/dealdetails/')); ?>"+"/"+id,

          success: function (response) {

            var json_obj = $.parseJSON(response);
            console.log(json_obj);
            var mymodal = $('#dealdetails');
            mymodal.find('.modal-title').html(`
             <div>
                <h3>`+ json_obj[0]['name'].toUpperCase()+`</h3>

             </div>
             `);

             mymodal.find('.modal-body').html(`
             <div>
                <h4><u>Description:</u></h4>
                <p>`+json_obj[0]['description'] +`</p>
                <hr>
                <h4><u>Active days:</u></h4>
                <p>`+json_obj[0]['days_applicable'].join()+`</p>

                <h4><u>Valid for:</u></h4>
                <p>`+'one Person only'+`</p>

                <h4><u>Valid Dates:</u></h4>
                <p>`+json_obj[0]['valid_from']+' To '+json_obj[0]['valid_from']+`</p>

                <h4><u>Terms & Conditions:</u></h4>
                <p>`+json_obj[0]['terms_conditions'] +`</p>



             </div>
             `);
            // $('#dealdetails').modal('show');
            mymodal.modal('show');
          }
      });

      })
</script>

<?php $__env->stopSection(); ?>

<?php echo $__env->make('layouts.app', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH C:\wamp64\www\offers\resources\views/wingdeals.blade.php ENDPATH**/ ?>