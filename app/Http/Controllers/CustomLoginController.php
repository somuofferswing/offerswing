<?php

namespace App\Http\Controllers;

use App\Models\Otp;
use App\Models\User;
use Auth;
use Illuminate\Contracts\Auth\PasswordBroker;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Session;

class CustomLoginController extends Controller
{
    // do login Auth
    public function loginUser(Request $request)
    {
        $email = $request->email;
        $password = $request->password;
        $user_type = $request->user_type;
        $remember = (1 == $request->remember) ? true : false;

        if (Auth::guard('web')->attempt(['email' => $email, 'password' => $password, 'user_type' => $user_type], $remember)) {
            // if (Auth::guard('web')->attempt(['email' => $email, 'password' => $password])) {
            $msg = [
                'status' => 'success',
                'message' => 'Login Successful',
            ];

        // return response()->json($msg);
        } else {
            $msg = [
                'status' => 'error',
                'message' => 'Login Fail !',
            ];
        }

        return response()->json($msg);
    }

    public function loginSendOtp(Request $request)
    {
        // $email = $request->email;
        $mobile = $request->mobile;

        // if (!empty($email)) {
        //     $user = User::where('email', $email)->first();
        // } elseif (!empty($mobile)) {
        // $user = User::where('mobile', $mobile)->first();
        // }

        $user = User::where('mobile', $mobile)->first();

        $otp = Otp::create([
            'user_id' => $user->id,
        ]);

        if ($otp->sendCode()) {
            Session::put('token_id', $otp->id);
            Session::put('user_id', $user->id);
            Session::put('remember', $request->get('remember'));
            Session::put('resendcount', 0);

            $msg = [
                'status' => 'Success',
                'message' => 'OTP has been sent to Your Phone Number',
            ];
        } else {
            $otp->delete();

            $msg = [
                'status' => 'error',
                'message' => 'Failed sending OTP to your Phone number',
            ];
        }

        return response()->json($msg);
    }

    public function resendOTP(Request $request)
    {
        $mobile = $request->mobile;

        $user = User::where('mobile', $mobile)->first();

        if (!Session::has('token_id', 'user_id')) {
            return redirect('login');
        }
        $otp = Otp::find(Session::get('token_id'));

        $count = Session::get('resendcount');

        if ($otp->sendCode()) {
            // Session::put('token_id', $otp->id);
            // Session::put('user_id', $user->id);
            // Session::put('remember', $request->get('remember'));
            ++$count;
            Session::put('resendcount', $count);

            $msg = [
                'status' => 'Success',
                'message' => 'OTP has been sent to Your Phone Number',
            ];
        } else {
            $otp->delete();

            $msg = [
                'status' => 'error',
                'message' => 'Failed sending OTP to your Phone number',
            ];
        }

        return response()->json($msg);
    }

    public function verifyOtp(Request $request)
    {
        // throttle for too many attempts
        if (!Session::has('token_id', 'user_id')) {
            return redirect('login');
        }
        $otp = Otp::find(Session::get('token_id'));

        if (!$otp ||
            !$otp->isValid() ||
            (int) $request->code !== $otp->code ||
            (int) Session::get('user_id') !== $otp->user->id
        ) {
            $msg = [
                'status' => 'error',
                'message' => 'OTP didn\'t match',
            ];

        // return response()->json($msg);
        } else {
            $otp->used = true;
            $otp->save();
            Auth::login($otp->user, Session::get('remember', false));
            Session::forget('token_id', 'user_id', 'remember');

            $msg = [
                'status' => 'Success',
                'message' => 'Logged in Successfully',
            ];
        }

        return response()->json($msg);
    }

    public function forgotPassword(Request $request, PasswordBroker $passwords)
    {
        if ($request->ajax()) {
            $this->validate($request, ['email' => 'required|email']);

            $response = $passwords->sendResetLink($request->only('email'));

            switch ($response) {
                case PasswordBroker::RESET_LINK_SENT:
                   return[
                       'status' => 'Success',
                       'msg' => 'A password link has been sent to your email address',
                   ];

                case PasswordBroker::INVALID_USER:
                   return[
                       'status' => 'Error',
                       'msg' => "We can't find a user with that email address",
                   ];
            }
        }

        return false;
    }
}
