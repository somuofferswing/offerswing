<!DOCTYPE html>
<html lang="en-US">
    <head>
        <meta charset="utf-8">
    </head>
    <body>
        <h2>Contact:</h2>

        <div>
            <ul>
                <li>Name: <?php echo e($input['name']); ?></li>
                <li>Email: <?php echo e($input['email']); ?></li>
                <li>Subject: <?php echo e($input['subject']); ?></li>
                <li>Message: <?php echo e($input['message']); ?></li>
            </ul>
        </div>

    </body>
</html>
<?php /**PATH /home/offerkrx/public_html/offerswing_test/resources/views/email/contact.blade.php ENDPATH**/ ?>