@extends('layouts.admin')

@section('content')
<link rel="stylesheet"
    href="{{ asset('adminlte/bower_components/bootstrap-datepicker/dist/css/bootstrap-datepicker.min.css') }}">
<div class="content-wrapper">
    <section class="content-header">
        <h1>
            Deals
            <small>Add Deal</small>
        </h1>
        <!-- <ol class="breadcrumb">
                  <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
                  <li class="active">Dashboard</li>
                </ol> -->
    </section>

    <section class="content">

        <div class="box box-info">
            <div class="box-header with-border">
                <h3 class="box-title">Add Deal</h3>
                <a href="{!! url('/deal/create'); !!}" class="pull-right btn btn-info">Add Deal</a>
            </div>
            <form class="form-horizontal" method="POST" action="{{ route('store-deal') }}"
                enctype="multipart/form-data">
                @csrf
                <input type="hidden" value="{{$user_id}}" name="created_by">
                <div class="box-body">
                    <div class="form-group ">
                        <label for="name" class="col-sm-2 control-label">Name</label>
                        <div class="col-sm-10">
                            <input id="name" type="text" class="form-control" name="name" value="{{ old('name') }}"
                                required autocomplete="name" autofocus>
                            @if ($errors->has('name'))
                            <div class="danger">{{ $errors->first('name') }}</div>
                            @endif
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="pic" class="col-sm-2 control-label">Image</label>
                        <div class="col-sm-10">
                            <input type="file" class="form-control-file" id="pic" name="pic" onchange="loadFile(event)">
                            <img id="output" height="100px" width="100px" />
                        </div>
                    </div>
                    <div class="form-group ">
                        <label for="description" class="col-sm-2 control-label">Description</label>
                        <div class="col-sm-10">
                            <textarea id="description" type="text" class="form-control" name="description" required
                                autocomplete="description" autofocus>{{ old('description') }}</textarea>
                            @if ($errors->has('description'))
                            <div class="danger">{{ $errors->first('description') }}</div>
                            @endif
                        </div>
                    </div>


                    <div class="form-group ">
                        <label for="shop_id" class="col-sm-2 control-label">Shop</label>
                        <div class="col-sm-10">

                            <select class="form-control" id="shop_id" name="shop_id" value="{{ old('shop_id') }}"
                                required autocomplete="shop_id" autofocus>
                                <option>--Select--</option>
                                @foreach($shops as $shop)
                                <option value="{{ $shop->id }}">{{ $shop->name }}</option>
                                @endforeach
                            </select>
                            @if ($errors->has('shop_id'))
                            <div class="danger">{{ $errors->first('shop_id') }}</div>
                            @endif
                        </div>
                    </div>

                    <div class="form-group ">
                        <label for="times_applicable" class="col-sm-2 control-label">Days Applicable</label>
                        <div class="col-sm-10">
                            <select name="days_applicable[]" id="days_applicable" class="form-control select2"
                                data-placeholder="Select days applicable" multiple="multiple">
                                @foreach (config('global.days') as $key =>$item)
                                <option value="{{ $key }}">{{ $item }}</option>
                                @endforeach
                            </select>
                            @if ($errors->has('days_applicable'))
                            <div class="danger">{{ $errors->first('days_applicable') }}</div>
                            @endif
                        </div>
                    </div>
                    <div class="form-group ">
                        <label for="valid_from" class="col-sm-2 control-label">From Date</label>
                        <div class="col-sm-10">
                            <input type="text" class="form-control pull-right" id="deal_start_date"
                                name="deal_start_date" value="{{ old('deal_start_date') }}" required autofocus>

                            @if ($errors->has('deal_start_date'))
                            <div class="danger">{{ $errors->first('deal_start_date') }}</div>
                            @endif
                        </div>
                    </div>
                    <div class="form-group ">
                        <label for="valid_upto" class="col-sm-2 control-label">To Date</label>
                        <div class="col-sm-10">
                            <input type="text" class="form-control pull-right" id="deal_end_date" name="deal_end_date"
                                value="{{ old('deal_end_date') }}" required autofocus>
                            @if ($errors->has('deal_end_date'))
                            <div class="danger">{{ $errors->first('deal_end_date') }}</div>
                            @endif
                        </div>
                    </div>
                    <div class="form-group ">
                        <label for="original_price" class="col-sm-2 control-label">Item Original Price</label>
                        <div class="col-sm-10">
                            <input id="original_price" type="text" class="form-control" name="original_price"
                                value="{{ old('original_price') }}" required autocomplete="name" autofocus>

                            @if ($errors->has('original_price'))
                            <div class="danger">{{ $errors->first('original_price') }}</div>
                            @endif
                        </div>
                    </div>
                    <div class="form-group ">
                        <label for="payment_type" class="col-sm-2 control-label">Payment Type</label>
                        <div class="col-sm-10">
                            <select class="form-control" id="payment_type" name="payment_type" required autofocus>
                                {{-- <option>--Select--</option> --}}
                                <option value="cash">Cash</option>
                                <option value="wingpoints">Wing Points</option>
                            </select>
                            @if ($errors->has('payment_type'))
                            <div class="danger">{{ $errors->first('payment_type') }}</div>
                            @endif
                        </div>
                    </div>

                    <div class="form-group ">
                        <label for="price" class="col-sm-2 control-label">Deal Price</label>
                        <div class="col-sm-10">
                            <input id="price" type="text" class="form-control" name="price" value="{{ old('price') }}"
                                required autofocus>
                            @if ($errors->has('price'))
                            <div class="danger">{{ $errors->first('price') }}</div>
                            @endif
                        </div>
                    </div>
                    <div class="form-group points_to_claim" style="display: none">
                        <label for="points_to_claim" class="col-sm-2 control-label">Wing points to claim</label>
                        <div class="col-sm-10">
                            <input id="points_to_claim" type="text" class="form-control" name="points_to_claim"
                                value="{{ old('points_to_claim') }}" autofocus>
                            @if ($errors->has('points_to_claim'))
                            <div class="danger">{{ $errors->first('points_to_claim') }}</div>
                            @endif
                        </div>
                    </div>
                    <div class="form-group ">
                        <label for="max_discount" class="col-sm-2 control-label">Validity in days</label>
                        <div class="col-sm-10">
                            <input id="max_discount" type="text" class="form-control" name="deal_validity_in_days"
                                value="{{ old('deal_validity_in_days') }}" required autofocus>
                            @if ($errors->has('deal_validity_in_days'))
                            <div class="danger">{{ $errors->first('deal_validity_in_days') }}</div>
                            @endif
                        </div>
                    </div>

                    <div class="form-group ">
                        <label for="quantity" class="col-sm-2 control-label">Quantity</label>
                        <div class="col-sm-10">
                            <input id="quantity" type="text" class="form-control" name="quantity"
                                value="{{ old('quantity') }}" autofocus>
                            @if ($errors->has('quantity'))
                            <div class="danger">{{ $errors->first('quantity') }}</div>
                            @endif
                        </div>
                    </div>
                    <div class="form-group ">
                        <label for="max_count_for_user" class="col-sm-2 control-label">Max per User</label>
                        <div class="col-sm-10">
                            <input id="max_count_for_user" type="text" class="form-control" name="max_count_for_user"
                                value="{{ old('max_count_for_user') }}" required autocomplete="max_count_for_user"
                                autofocus>
                            @if ($errors->has('max_count_for_user'))
                            <div class="danger">{{ $errors->first('max_count_for_user') }}</div>
                            @endif
                        </div>
                    </div>
                    <div class="form-group ">
                        <label for="valid_for" class="col-sm-2 control-label">Valid For</label>
                        <div class="col-sm-10">
                            <select class="form-control select2" name="valid_for[]" id="valid_for" required multiple>
                                @foreach(config('global.valid_for') as $validfor)
                                <option value="{{ $validfor['name'] }}">{{ $validfor['name'] }}</option>
                                @endforeach
                                @if ($errors->has('valid_for'))
                                <div class="danger">{{ $errors->first('max_count_for_user') }}</div>
                                @endif
                            </select>
                        </div>
                    </div>
                    <div class="form-group ">
                        <label for="points_rewarded" class="col-sm-2 control-label">Wing points receive </label>
                        <div class="col-sm-10">
                            <input id="points_rewarded" type="text" class="form-control" name="points_rewarded"
                                value="{{ old('points_rewarded') }}" autofocus>
                            @if ($errors->has('points_rewarded'))
                            <div class="danger">{{ $errors->first('points_rewarded') }}</div>
                            @endif
                        </div>
                    </div>

                    <div class="form-group ">
                        <label for="max_wing_points_assured" class="col-sm-2 control-label">Top Deal</label>
                        <div class="col-sm-10">
                            <input type="checkbox" class="" name="top" value="1">
                            @if ($errors->has('top'))
                            <div class="danger">{{ $errors->first('top') }}</div>
                            @endif
                        </div>
                    </div>

                    <div class="form-group ">
                        <label for="max_wing_points_assured" class="col-sm-2 control-label">Active</label>
                        <div class="col-sm-10">
                            <input type="checkbox" class="" name="active" value="1">
                            @if ($errors->has('active'))
                            <div class="danger">{{ $errors->first('active') }}</div>
                            @endif
                        </div>
                    </div>

                    <div class="form-group ">
                        <label for="terms_conditions" class="col-sm-2 control-label">Terms & Conditions</label>
                        <div class="col-sm-10">
                            <textarea id="terms_conditions" type="text" class="form-control" name="terms_conditions"
                                rows="10" cols="80">{{ old('terms_conditions') }}</textarea>
                            @if ($errors->has('terms_conditions'))
                            <div class="danger">{{ $errors->first('terms_conditions') }}</div>
                            @endif
                        </div>
                    </div>
                    <button type="button" class="btn btn-info pull-right">{{ __('Cancel') }}</button>
                    <button type="submit" class="btn btn-info pull-right">{{ __('Add Deal') }}</button>
                </div>
            </form>
        </div>
    </section>
</div>
@endsection
@section('scripts')
<script src="{{ asset('adminlte/bower_components/bootstrap-datepicker/dist/js/bootstrap-datepicker.min.js') }}">
</script>
<script src="{{ asset('adminlte/bower_components/select2/dist/js/select2.full.min.js') }}"></script>
<script src="{{ asset('adminlte/bower_components/ckeditor/ckeditor.js') }}"></script>
<script>
    $(function () {
            CKEDITOR.replace('terms_conditions');
            CKEDITOR.replace('description')
        });
        $('.select2').select2();
        $(document).ready(function () {
            $('#deal_start_date').datepicker({
                autoclose: true,
                format: 'yyyy-mm-dd',
                startDate: new Date(),
            });

            $('#deal_end_date').datepicker({
                autoclose: true,
                format: 'yyyy-mm-dd',
                startDate: new Date(),
            }).change(function () {
                var from = $('#deal_start_date').val();
                var to = $('#deal_end_date').val();
                if (to < from) {
                    $('#deal_end_date').val("");
                    alert('to date should be grater than from date');
                }
            });
        })

</script>
<script>
    var loadFile = function (event) {
            var output = document.getElementById('output');
            output.src = URL.createObjectURL(event.target.files[0]);
        };
</script>
<script>
    $('#payment_type').on('change',function(){
            var type = $(this).val();
            if(type == 'wingpoints'){
                $('.points_to_claim').show();
            }else{
                $('.points_to_claim').hide();
            }

        });
</script>
@endsection
