

<?php $__env->startSection('content'); ?>
    <div class="content-wrapper">
        <section class="content-header">
            <h1>
                Category
                <small>All Categories</small>
            </h1>
            <!-- <ol class="breadcrumb">
              <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
              <li class="active">Dashboard</li>
            </ol> -->
        </section>
        <section class="content">
            <div class="box box-primary">
                <div class="box-header with-border">
                    <h3 class="box-title">All Categories</h3>
                    <a href="<?php echo url('/category/create');; ?>" class="btn btn-primary pull-right">Add Category</a>
                </div>
                <div class="table-responsive">
                    <table class="table table-striped">
                        <thead>
                        <tr>
                            <th>S.No</th>
                            <th>Name</th>
                            <th>Image</th>
                            <th>Parent Category</th>
                            <th colspan="2">Action</th>
                        </tr>
                        </thead>
                        <tbody>
                        <?php $__currentLoopData = $allcategories; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $key=>$category): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                            <tr>
                                <td><?php echo e($key+1); ?></td>
                                <td><?php echo e($category->category_name); ?></td>
                                <td><img class="img-thumbnail" src='<?php echo asset("$category->cat_image"); ?>' height="75px" width="75px"></td>
                                <td><?php echo e($category->parent_category_name); ?></td>

                                
                                <td><a class="btn btn-default" href="<?php echo e(route('edit-category', [$category->id])); ?>">Edit</a></td>

                                    <td><a class="btn btn-danger del" data-sub="false" data-id="<?php echo e($category->id); ?>">Delete</a></td>


                            </tr>
                        <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                        </tbody>
                    </table>
                    <div class="pull-right" style="padding:10px">
                            <?php echo e($allcategories->links()); ?>

                    </div>
                </div>
            </div>

    </section>
    </div>
<?php $__env->stopSection(); ?>
<?php $__env->startSection('scripts'); ?>
<script>
$('.del').on('click',function(){
    var bool = $(this).data('sub');
    var id = $(this).data('id');

    if(!bool){
        $.ajax({
            type: "POST",
            url: "<?php echo e(url('category/delete/')); ?>"+"/"+id,
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            },
            success: function (response) {
                alert(response);
                window.location.reload();
            }
        });
    }else{
        if (confirm('Do you want to delete all sub categories too?')) {
            // alert('Thanks for confirming');
            $.ajax({
                type: "POST",
                url: "<?php echo e(url('category/delete/')); ?>"+"/"+id,
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
                success: function (response) {
                    alert(response);
                    window.location.reload();
                }
            });
            // delete(id);
        } else {

        }
    }

});

// function delete(id){
//     $.ajax({
//                 type: "POST",
//                 url: "<?php echo e(url('category/delete/')); ?>"+id,
//                 headers: {
//                     'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
//                 },
//                 success: function (response) {
//                     alert(response);
//                     window.location.reload();
//                 }
//             });

// }
</script>

<?php $__env->stopSection(); ?>

<?php echo $__env->make('layouts.admin', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH /home/offerkrx/public_html/offerswing/resources/views/category/index.blade.php ENDPATH**/ ?>