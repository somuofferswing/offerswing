<?php

namespace App\Http\Controllers\Backend;

use App\Http\Controllers\Controller;
use App\Models\Token;
use App\Models\WingPoints;
use Illuminate\Http\Request;

class TokenController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $tokens = Token::all();

        return view('backend.token.index', compact('tokens'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('backend.token.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $token = new Token();

        $this->validate($request, [
            'token_id' => 'required',

            'points_rewarded' => 'required',
        ]);

        $token->token_id = $request['token_id'];

        $token->points_rewarded = $request['points_rewarded'];

        $token->save();

        return redirect('/token/create')->with('alert', 'token created Successfully')->with('class', 'success');
    }

    /**
     * Display the specified resource.
     *
     * @param int $id
     *
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param int $id
     *
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $token = Token::find($id);

        return view('backend.token.edit', compact('token'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param int $id
     *
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $token = Token::find($id);

        $this->validate($request, [
            'token_id' => 'required',

            'points_rewarded' => 'required',
        ]);

        $token->token_id = $request['token_id'];

        $token->points_rewarded = $request['points_rewarded'];

        $token->save();

        return redirect('/tokens')->with('alert', 'question has Created Successfully')->with('class', 'success');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param int $id
     *
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $token = Token::find($id);

        $token->delete();

        return redirect('/tokens')->with('alert', 'Token deleted Successfully')->with('class', 'success');
    }

    public function redeemToken(Request $request)
    {
        $user = auth()->user();

        $this->validate($request, [
            'token_id' => 'required',
        ]);

        $tid = $request['token_id'];

        $token = Token::where('token_id', $tid)->first();

        $user_id = $user['id'];

        if (!empty($token)) {
            $token_status = $token->claim_status;

            if (0 == $token_status) {
                $wing_points = new WingPoints();

                $wing_points->type = 'TOKEN';

                $wing_points->user_id = $user_id;

                $wing_points->type_id = $token->id;

                $wing_points->points = $token->points_rewarded;

                $wing_points->save();

                $token->claim_status = 1;

                $token->save();

                return back()->with('alert', 'Token Claimed Successfully')->with('class', 'success');
            }

            return back()->with('alert', 'Token Claimed Already')->with('class', 'danger');
        }

        return back()->with('alert', 'Please check the Token Code')->with('class', 'danger');
    }
}
