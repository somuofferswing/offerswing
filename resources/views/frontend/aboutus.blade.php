@extends('layouts.app')

@section('content')
<style>
    .ps-section__desc>ul>li{
        text-align: left;
        color: #666;
        font-size: 18px;
        margin-bottom: 16px;
        line-height: 1.6em;
    }

    .sideHeading{
        margin-bottom: 50px;
        font-size: 36px;
        color: #000;
        font-weight: 400;
        text-align: center;
    }

    .ps-about-vendors{
        padding: 100px 0;
        background-color: #f5f5f5;
    }
    .ps-about-users{
        padding: 100px 0;
    }
    .ps-section__header{
        max-width: 820px;
        margin: 0 auto;
    }
</style>
<div class="ps-breadcrumb">
    <div class="container">
        <ul class="breadcrumb">
            <li><a href="{{ url('/') }}">Home</a></li>
            <li>About Us</li>
        </ul>
    </div>
</div>

<div class="ps-page--single" id="about-us">
    <img src="http://nouthemes.net/html/martfury/img/bg/about-us.jpg" alt="">
    <div class="ps-about-intro">
        <div class="container">
            <div class="ps-section__header mb-0">
                <h4 class="defaultColor">Welcome to Offerswing</h4>
                <h3>Offerswing.com is India's best app which will help you find the best local discounts, deals, offers, and any local search.</h3>
                <p>Instead of looking at all the confusing advertisements around you, you are requested to have a look at our user-friendly app “Offerswing.com” which saves your time, money and energy finding the best local deals. It will help customers discover fantastic offers at their favorite outlets and this can also become a sales channel for offline merchants.</p>
                <p>We provide you the facility by which you can easily sort the shop and its products by distance, price, brand value in all the 48 major and 250 subcategories depending on location and we will get you the required notification which will tempt you to grab them. Through this easy navigation system, you can find the essential information like vendor’s location and shop timings at a single click.</p>
                <p>We being entevested some time miss the best ongoing deals in our locality due to out of time having been occupied by some other work.</p>
                <p>“ Offerswing “ is the app that is taken shape from the need and thought of “Why we have no space to share the information with the people around “.</p>
            </div>
        </div>
    </div>
    <div class="ps-about-vendors">
        <div class="ps-container">
            <div class="ps-section__header">
                <h4 class="sideHeading defaultColor">Our promises to vendors</h4>
                <div class="row text-center">
                    <div class="col-12 col-sm-5">
                        <img src="{{ asset('main/images/icons/ll.png') }}">
                    </div>

                    <div class="col-12 col-sm-7">
                        <div class="ps-section__desc">
                            <ul>
                                <li>Your Business promotion is our responsibility./ We will take your promotion work to the next level.</li>
                                <li>We Bring The Best deals from our side.</li>
                                <li>We upgrade running offers & promotions on your dedicated page.</li>
                                <li>Transforming your business into digital India./ business in a digital way.</li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="ps-about-users">
        <div class="ps-container">
            <div class="ps-section__header">
                <h4 class="sideHeading defaultColor">Benefits to App Users</h4>
                <div class="row text-center">
                    <div class="col-12 col-sm-7">
                        <div class="ps-section__desc">
                            <ul>
                                <li>We provide you hassle-free registration, complimentary coupons, and referrals.</li>
                                <li>You will get the notifications for the best discounts and offers in your location.</li>
                                <li>Auto-detection of your location with the help of GPS to reach your favorite store.</li>
                            </ul>
                        </div>
                    </div>
                    <div class="col-12 col-sm-5">
                        <img src="{{ asset('main/images/icons/users.png') }}">
                    </div>
                </div>
            </div>
        </div>
    </div>
    </div>

</div>

@endsection