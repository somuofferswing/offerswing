<?php if(Auth::user()->user_type == 1): ?>
<aside class="main-sidebar">
    <section class="sidebar">
        <ul class="sidebar-menu" data-widget="tree">
            <li class="header">MAIN NAVIGATION</li>
            <li class="treeview">
                <a href="#">
                    <i class="fa fa-users"></i> <span> Users</span>
                    <span class="pull-right-container">
                        <i class="fa fa-angle-left pull-right"></i>
                    </span>
                </a>
                <ul class="treeview-menu">
                    <li><a href="<?php echo url('/allusers');; ?>"><i class="fa fa-circle-o"></i> All Users</a></li>
                    <li><a href="<?php echo url('/adduser');; ?>"><i class="fa fa-circle-o"></i> Add User</a></li>
                    <li><a href="<?php echo url('/users');; ?>"><i class="fa fa-circle-o"></i> Users</a></li>
                    <li><a href="<?php echo url('/admins');; ?>"><i class="fa fa-circle-o"></i> Admins</a></li>
                    <li><a href="<?php echo url('/retailers');; ?>"><i class="fa fa-circle-o"></i> Retailers</a></li>
                    <li><a href="<?php echo url('/alldeleted');; ?>"><i class="fa fa-circle-o"></i> All Deleted Users</a></li>
                </ul>
            </li>
            <li class="treeview">
                <a href="#">
                    <i class="fa fa-list-alt"></i>
                    <span> Categories</span>
                    <span class="pull-right-container">
                        <i class="fa fa-angle-left pull-right"></i>
                    </span>
                </a>
                <ul class="treeview-menu">
                    <li><a href="<?php echo url('/categories');; ?>"><i class="fa fa-circle-o"></i> All Categories</a></li>
                    <li><a href="<?php echo url('/category/create');; ?>"><i class="fa fa-circle-o"></i> Add
                            Category/SubCategory</a></li>
                </ul>
            </li>
            <li class="treeview">
                <a href="#">
                    <i class="fa fa-home"></i>
                    <span> Shops</span>
                    <span class="pull-right-container">
                        <i class="fa fa-angle-left pull-right"></i>
                    </span>
                </a>
                <ul class="treeview-menu">
                    <li><a href="<?php echo url('/shops');; ?>"><i class="fa fa-circle-o"></i> All Shops</a></li>
                    <li><a href="<?php echo url('/shop/create');; ?>"><i class="fa fa-circle-o"></i> Add Shop</a></li>
                    <li><a href="<?php echo url('/amenities');; ?>"><i class="fa fa-circle-o"></i> Shop Amenities</a></li>
                    <li><a href="<?php echo url('/payments');; ?>"><i class="fa fa-circle-o"></i> Shop accepted payment types</a></li>
                </ul>
            </li>
            <li>
            <li class="treeview">
                <a href="#">
                    <i class="fa fa-gift"></i>
                    <span> Wing Deals</span>
                    <span class="pull-right-container">
                        <i class="fa fa-angle-left pull-right"></i>
                    </span>
                </a>
                <ul class="treeview-menu">
                    <li><a href="<?php echo url('/deals');; ?>"><i class="fa fa-circle-o"></i> All Deals</a></li>
                    <li><a href="<?php echo url('/deal/create');; ?>"><i class="fa fa-circle-o"></i> Add Deal</a></li>
                </ul>
            </li>
            <li class="treeview">
                <a href="#">
                    <i class="fa fa-gift"></i>
                    <span>Offers</span>
                    <span class="pull-right-container">
                        <i class="fa fa-angle-left pull-right"></i>
                    </span>
                </a>
                <ul class="treeview-menu">
                    <li><a href="<?php echo url('/offers');; ?>"><i class="fa fa-circle-o"></i> All offers</a></li>
                    <li><a href="<?php echo url('/offer/create');; ?>"><i class="fa fa-circle-o"></i> Add Offer</a></li>
                </ul>
            </li>
            <li class="treeview">
                <a href="#">
                    <i class="fa fa-gift"></i>
                    <span>News</span>
                    <span class="pull-right-container">
                        <i class="fa fa-angle-left pull-right"></i>
                    </span>
                </a>
                <ul class="treeview-menu">
                    <li><a href="<?php echo url('/news');; ?>"><i class="fa fa-circle-o"></i> All NewsFeed</a></li>
                    <li><a href="<?php echo url('/news/create');; ?>"><i class="fa fa-circle-o"></i> Add Newsfeed</a></li>
                </ul>
            </li>
            <li class="treeview">
                <a href="#">
                    <i class="fa fa-gift"></i>
                    <span>Events</span>
                    <span class="pull-right-container">
                        <i class="fa fa-angle-left pull-right"></i>
                    </span>
                </a>
                <ul class="treeview-menu">
                    <li><a href="<?php echo url('/events');; ?>"><i class="fa fa-circle-o"></i> All Events</a></li>
                    <li><a href="<?php echo url('/events/create');; ?>"><i class="fa fa-circle-o"></i> Add Event</a></li>
                </ul>
            </li>
            <li class="treeview">
                <a href="#">
                    <i class="fa fa-gift"></i>
                    <span>Gifts</span>
                    <span class="pull-right-container">
                        <i class="fa fa-angle-left pull-right"></i>
                    </span>
                </a>
                <ul class="treeview-menu">
                    <li><a href="<?php echo url('/gifts');; ?>"><i class="fa fa-circle-o"></i> All gifts</a></li>
                    <li><a href="<?php echo url('/gift/create');; ?>"><i class="fa fa-circle-o"></i> Add gift</a></li>
                </ul>
            </li>

            <li class="treeview">
                <a href="#">
                    <i class="fa fa-gift"></i>
                    <span>Tokens</span>
                    <span class="pull-right-container">
                        <i class="fa fa-angle-left pull-right"></i>
                    </span>
                </a>
                <ul class="treeview-menu">
                    <li><a href="<?php echo url('/tokens');; ?>"><i class="fa fa-circle-o"></i> All Tokens</a></li>
                    <li><a href="<?php echo url('/token/create');; ?>"><i class="fa fa-circle-o"></i> Add Token</a></li>
                </ul>
            </li>
            <li class="treeview">
                <a href="#">
                    <i class="fa fa-gift"></i>
                    <span> Orders</span>
                    <span class="pull-right-container">
                        <i class="fa fa-angle-left pull-right"></i>
                    </span>
                </a>
                <ul class="treeview-menu">
                    <li><a href="#"><i class="fa fa-circle-o"></i> All Orders</a></li>
                    <li><a href="#"><i class="fa fa-circle-o"></i> Pending Orders</a></li>
                    <li><a href="#"><i class="fa fa-circle-o"></i> Finished Orders</a></li>
                </ul>
            </li>

            <li class="treeview">
                <a href="#">
                    <i class="fa fa-gift"></i>
                    <span>FAQs</span>
                    <span class="pull-right-container">
                        <i class="fa fa-angle-left pull-right"></i>
                    </span>
                </a>
                <ul class="treeview-menu">
                    <li><a href="<?php echo url('/faqs');; ?>"><i class="fa fa-circle-o"></i> All Faq</a></li>
                    <li><a href="<?php echo url('/faq/create');; ?>"><i class="fa fa-circle-o"></i> Add Faq</a></li>
                </ul>
            </li>

            <li class="treeview">
                <a href="#">
                    <i class="fa fa-gift"></i>
                    <span>Pages</span>
                    <span class="pull-right-container">
                        <i class="fa fa-angle-left pull-right"></i>
                    </span>
                </a>
                <ul class="treeview-menu">
                    <li><a href="<?php echo url('/pages');; ?>"><i class="fa fa-circle-o"></i> All Pages</a></li>
                    
                </ul>
            </li>

            



            <li class="treeview">
                <a href="#">
                    <i class="fa fa-gift"></i>
                    <span>Home page</span>
                    <span class="pull-right-container">
                        <i class="fa fa-angle-left pull-right"></i>
                    </span>
                </a>
                <ul class="treeview-menu">
                    <li><a href="<?php echo url('/slider/edit/1'); ?>"><i class="fa fa-circle-o"></i> Slider 1 edit</a></li>
                    <li><a href="<?php echo url('/slider/edit/2'); ?>"><i class="fa fa-circle-o"></i> Slider 1 edit</a></li>
                    <li><a href="<?php echo url('/imagediv/edit/1'); ?>"><i class="fa fa-circle-o"></i> Images row 1 edit</a>
                    </li>
                    <li><a href="<?php echo url('/imagediv/edit/4'); ?>"><i class="fa fa-circle-o"></i> Images row 2 edit</a>
                    </li>
                    <li><a href="<?php echo url('/imagediv/edit/2'); ?>"><i class="fa fa-circle-o"></i> Images row 3 edit</a>
                    </li>
                    <li><a href="<?php echo url('/imagediv/edit/5'); ?>"><i class="fa fa-circle-o"></i> Images row 4 edit</a>
                    </li>
                    <li><a href="<?php echo url('/imagediv/edit/3'); ?>"><i class="fa fa-circle-o"></i> Images row 5 edit</a>
                    </li>
                    <li><a href="<?php echo url('/imagediv/edit/6'); ?>"><i class="fa fa-circle-o"></i> Images row 6 edit</a>
                    </li>
                    <li><a href="<?php echo url('/imagediv/edit/7'); ?>"><i class="fa fa-circle-o"></i> Images row 7 edit</a>
                    </li>
                    <li><a href="<?php echo url('/imagediv/edit/8'); ?>"><i class="fa fa-circle-o"></i> Images row 8 edit</a>
                    </li>
                    <li><a href="<?php echo url('/imagediv/edit/9'); ?>"><i class="fa fa-circle-o"></i> Images row 9 edit</a>
                    </li>

                </ul>
            </li>

            <li>
                <a href="<?php echo url('/admin/settings'); ?>">
                    <i class="fa fa-cog"></i>
                    <span> Settings</span>
                </a>
            </li>
        </ul>
    </section>
</aside>




<?php endif; ?>
<?php /**PATH /home/offerkrx/public_html/offerswing_test/resources/views/layouts/adminsidebar.blade.php ENDPATH**/ ?>