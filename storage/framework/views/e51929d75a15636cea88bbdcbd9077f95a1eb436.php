<?php $__env->startSection('content'); ?>
<style>
.highlighter{
    border-bottom:2px solid #df685d;
    width:10%;
    margin:0px auto;
}

.ps-page--blog .ps-page__header {
    padding: 30px 0px;
}

@media (min-width: 992px){
    .ps-blog--sidebar .ps-blog__left{
        padding-right:30px;
    }
}
	
.ps-post--small-thumbnail .ps-post__content{
    padding: 0px 20px;
}

.zoom{
    cursor:pointer;
}

.zoom:hover {
    transform: scale(1.5);
    -webkit-transition: transform .2s ease-in-out;
}

.ps-post:hover{
    margin-bottom: 30px;
    padding: 20px;
    border: 1px solid #bfbfbf;
    -webkit-transition: all .4s ease;
    transition: all .4s ease;
}

</style>
<div class="ps-breadcrumb">
    <div class="ps-container">
        <ul class="breadcrumb text-capitalize" id="breadcrumb">
            <li><a href="<?php echo e(url('/')); ?>">Home</a></li>
            <li>News Feed</li>
        </ul>
    </div>
</div>

<section>
    <div class="ps-page--blog">
        <div class="container">
            
            <div class="ps-page__header">
                <h1>News Feed</h1>
                <hr class="highlighter">
            </div>
            
            <div class="ps-blog--sidebar">
                <div class="ps-blog__left">
                <?php $__currentLoopData = $newsfeed; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $item): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                    <div class="ps-post ps-post--small-thumbnail">
                        <div class="ps-post__thumbnail">
                            <img src="<?php echo asset("$item->news_image"); ?>" alt="">
                        </div>
                        <div class="ps-post__content">
                            <div class="ps-post__top">
                                <span class="ps-post__title" ><?php echo e(ucfirst($item->title)); ?>

                                    <?php if(Auth::guest()): ?>
                                    <span class="badge zoom ml-2" data-id="<?php echo e($item->id); ?>"  data-toggle="modal" data-target="#myModal1">
                                        <span data-toggle="tooltip" data-placement="right" title="" data-original-title="Like">
                                        <i class="fa fa-heart-o fa-lg text-danger"></i>
                                        </span>
                                    </span>
                                    <?php else: ?>
                                        <?php if(in_array($item->id,$likes)): ?>
                                            <span class="badge zoom ml-2 unlike" data-id="<?php echo e($item->id); ?>" data-toggle="tooltip" data-placement="right" title="" data-original-title="Unlike">
                                                <i class="fa fa-heart fa-lg text-danger"></i>
                                            </span>
                                        <?php else: ?>
                                            <span class="badge zoom ml-2 like" data-id="<?php echo e($item->id); ?>" data-toggle="tooltip" data-placement="right" title="" data-original-title="Like">
                                                <i class="fa fa-heart-o fa-lg text-danger"></i>
                                            </span>
                                        <?php endif; ?>
                                    <?php endif; ?>
                                </span>
                                <?php echo $item->description; ?>

                            </div>
                            <div class="ps-post__bottom">
                                <span class="text-muted"><?php echo e(date('h:i A',strtotime($item->created_at)).' on '.date( 'd-m-Y, l' ,strtotime($item->created_at))); ?></span>
                                <ul class="ps-list--social pull-right">
                                    <li>
                                        <a class="facebook" href="#" target="_blank" title="Facebook"><i class="fa fa-facebook"></i>
                                        </a>
                                    </li>
                                    <li>
                                        <a class="twitter" href="#" target="_blank" title="Twitter"><i class="fa fa-twitter"></i>
                                        </a>
                                    </li>
                                    <li>
                                        <a class="instagram" href="#" target="_blank" title="instagram"><i class="fa fa-instagram"></i>
                                        </a>
                                    </li>
                                </ul>
                               
                            </div>
                         
                        </div>
                    </div>
                    <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                </div>
                <div class="ps-blog__right">
                    <div class="ps-block__slider">
                        <div class="ps-carousel--product-box owl-slider" data-owl-auto="true" data-owl-loop="true" data-owl-speed="7000" data-owl-gap="0" data-owl-nav="true" data-owl-dots="true" data-owl-item="1" data-owl-item-xs="1" data-owl-item-sm="1" data-owl-item-md="1" data-owl-item-lg="1" data-owl-duration="500" data-owl-mousedrag="off">
                            <a href="#">
                                <img src="http://nouthemes.net/html/martfury/img/slider/home-3/clothing-1.jpg" alt="">
                            </a>
                            <a href="#">
                                <img src="http://nouthemes.net/html/martfury/img/slider/home-3/clothing-2.jpg" alt="">
                            </a>
                            <a href="#">
                                <img src="http://nouthemes.net/html/martfury/img/slider/home-3/clothing-3.jpg" alt="">
                            </a>
                        </div>
                    </div>
                </div>
            </div>
            
        </div>
    </div>
</section>

<!-- <a class="btn btn-primary" >Trigger modal</a> -->
<div class="modal fade" id="share">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-body">
                <div class="a2a_kit a2a_kit_size_32 a2a_default_style">
                    <a class="a2a_button_facebook"></a>
                    <a class="a2a_button_twitter"></a>
                    <a class="a2a_button_whatsapp"></a>
                </div>
                <script async src="https://static.addtoany.com/menu/page.js"></script>
            </div>
        </div>
    </div>
</div>

<script>
    $(document).on('click', ".unlike", function () {
        var id = $(this).data('id');
        var type = "news_feed";
        var element = this;

        var data = {
                id:id,
                type:type
            };
        $.ajax({
                type: "post",
                url: '<?php echo e(url("removelike")); ?>',
                headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                 },
                data: data,
                cache: false,
                success: function (data)
                {
                    if(data.status == "success"){
                        $(element).removeClass('unlike').addClass('like').html('<i class="far fa-heart" style="color:#e31b23"></i> &nbsp;like');
                    }
                },
                error: function (data){
                    alert('Fail to run unlike..');
                }
            });


    });
    $(document).on('click', ".like", function () {
        var id = $(this).data('id');
        var type = "news_feed";
        var element = this;

        var data = {
                id:id,
                type:type
            };
        $.ajax({
                type: "post",
                url: '<?php echo e(url("addlike")); ?>',
                headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                 },
                data: data,
                cache: false,
                success: function (data)
                {
                    console.log(data);
                    if(data.status == "success"){
                            $(element).removeClass('like').addClass('unlike').html('<i class="fas fa-heart" style="color:#e31b23"></i> &nbsp;Unlike');

                    }
                },
                error: function (data){
                    alert('Fail to run like..');
                }
            });


    });
</script>


<?php $__env->stopSection(); ?>
<?php echo $__env->make('layouts.app', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH C:\xampp\htdocs\offerswing\resources\views/newsfeed.blade.php ENDPATH**/ ?>