<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Offer extends Model
{
    protected $fillable = [
        'shop_id', 'description', 'from_date', 'to_date', 'offer_name', 'offer_image', 'category_id',
    ];

    public function shop()
    {
        return $this->belongsTo(Shop::class, 'shop_id', 'id');
    }

    // public function category()
    // {
    //     return $this->belongsToMany(Category::class);
    // }
}
