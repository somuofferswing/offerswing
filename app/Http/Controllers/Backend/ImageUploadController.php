<?php

namespace App\Http\Controllers\Backend;

use App\Http\Controllers\Controller;
use App\Models\ImageUpload;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;

class ImageUploadController extends Controller
{
    public function fileStore(Request $request)
    {
        $type = $request->type;
        $image = $request->file('file');

        // $fileName = $image->getClientOriginalName();
        $imageName = 'shop_'.time().'.'.$image->getClientOriginalExtension();
        $path = $image->storeAs('shopimages', $imageName, 'public');

        $imageUpload = new ImageUpload();

        $imageUpload->image_name = $imageName;
        $imageUpload->image_path = $path;
        $imageUpload->type = $type;

        $shop_id = $request['shop_id'];
        if ($shop_id) {
            $imageUpload->shop_id = $shop_id;
        }

        $imageUpload->save();

        $id = $imageUpload->id;

        return response()->json(['success' => $imageName, 'id' => $id]);
    }

    public function fileDestroy(Request $request)
    {
        $filename = $request->get('filename');

        $image_upload = ImageUpload::where('image_name', $filename)->first();

        $id = $image_upload->id;

        Storage::disk('public')->delete($image_upload->image_name);
        
        ImageUpload::where('image_name', $filename)->delete();

        return response()->json(['success' => 1, 'id' => $id]);
    }

    public function destroy($id)
    {
        $image_upload = ImageUpload::find($id);
        Storage::disk('public')->delete($image_upload->image_name);
        $image_upload->delete();

        return response()->json(['success' => 1]);
        // return redirect('/shares')->with('success', 'Stock has been deleted Successfully');
    }
}
