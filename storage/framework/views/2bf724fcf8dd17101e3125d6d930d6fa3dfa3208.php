<?php $__env->startSection('content'); ?>

<style>

    /* card */

    .ps-post{
        margin-bottom: 30px;
        padding: 20px;
        border: 1px solid #bfbfbf;
    }

    .ps-post:hover{
        border: 1px solid #fcb800;
        -webkit-transition: all .4s ease;
        transition: all .4s ease
    }

    .ps-post .ps-post__thumbnail{
        height:200px;
    }
    .ps-post .ps-post__thumbnail img{
        height:100%;
    }


    .ps-post .ps-post__thumbnail .ps-post__badge {
        border-radius:50%;
        background-color:white;
        color:#dc3545;
        cursor:pointer;
    }

    .ps-post .ps-post__badge i{
        color:#dc3545;
    }

</style>

<div class="ps-breadcrumb">
    <div class="ps-container">
        <ul class="breadcrumb text-capitalize" id="breadcrumb">
            <li><a href="<?php echo e(url('/')); ?>">Home</a></li>
            <li>Events</li>
        </ul>
    </div>
</div>
<section>
    <div class="ps-page--blog">
        <div class="container">
            
            <div class="ps-page__header">
                <h1>Events</h1>
                <hr class="highlighter">
            </div>
            
            <div class="ps-blog">
                <div class="ps-blog__content">
                    <div class="row">
                        <?php $__currentLoopData = $events; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $item): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                        <div class="col-xl-4 col-lg-4 col-md-6 col-sm-12 col-12 ">
                            <div class="ps-post">
                                <div class="ps-post__thumbnail">
                                    <a class="ps-post__overlay" href="<?php echo e(url('eventdetails',[$item->id])); ?>"></a>
                                    <img src="<?php echo asset("$item->event_image"); ?>" alt="">
                                    <div class="ps-post__badge">
                                        <?php if(Auth::guest()): ?>
                                            <span class="zoom" onClick="return false;" data-id="<?php echo e($item->id); ?>"  data-toggle="modal" data-target="#myModal1">
                                                <span data-toggle="tooltip" data-placement="right" title="" data-original-title="Like">
                                                <i class="fa fa-heart-o fa-lg"></i>
                                                </span>
                                            </span>
                                        <?php else: ?>
                                            <?php if(in_array($item->id,$likes)): ?>
                                                <span class="zoom  unlike" data-id="<?php echo e($item->id); ?>" data-toggle="tooltip" data-placement="right" title="" data-original-title="Unlike">
                                                    <i class="fa fa-heart fa-lg"></i>
                                                </span>
                                            <?php else: ?>
                                                <span class="zoom like" data-id="<?php echo e($item->id); ?>" data-toggle="tooltip" data-placement="right" title="" data-original-title="Like">
                                                    <i class="fa fa-heart-o fa-lg"></i>
                                                </span>
                                            <?php endif; ?>
                                        <?php endif; ?>
                                    </div>
                                </div>
                                <div class="ps-post__content">
                                    <div class="ps-post__meta">
                                        <span>
                                            <?php echo e(date('M d, Y', strtotime($item->event_date))); ?> 
                                            <?php echo e($item->event_time); ?>

                                        </span>
                                        <span class="pull-right text-danger">
                                            &#x20b9 <?php echo e($item->event_price); ?> Onwards
                                        </span>
                                    </div>
                                    <a class="ps-post__title" href="<?php echo e(url('eventdetails',[$item->id])); ?>">
                                        <?php echo e(ucfirst($item->title)); ?>

                                      
                                    </a>
                                </div>
                            </div>
                        </div>
                        <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                    </div>
                </div>
            </div>
            
        </div>
    </div>
</section>


<!-- <a class="btn btn-primary" >Trigger modal</a> -->
<div class="modal fade" id="share">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-body">
                <div class="a2a_kit a2a_kit_size_32 a2a_default_style">
                    
                    <a class="a2a_button_facebook"></a>
                    <a class="a2a_button_twitter"></a>
                    <a class="a2a_button_email"></a>
                    <a class="a2a_button_whatsapp"></a>
                </div>
                <script async src="https://static.addtoany.com/menu/page.js"></script>
            </div>
        </div>
    </div>
</div>

<script>
    $(document).on('click', ".unlike", function () {
            var id = $(this).data('id');
            var type = "event";
            var element = this;

            var data = {
                    id:id,
                    type:type
                };
            $.ajax({
                    type: "post",
                    url: '<?php echo e(url("removelike")); ?>',
                    headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                     },
                    data: data,
                    cache: false,
                    success: function (data)
                    {
                        if(data.status == "success"){
                            $(element).removeClass('unlike').addClass('like').html('<i class="fa fa-heart-o"></i>');
                        }
                    },
                    error: function (data){
                        alert('Fail to run unlike..');
                    }
                });


        });
        $(document).on('click', ".like", function () {
            var id = $(this).data('id');
            var type = "event";
            var element = this;

            var data = {
                    id:id,
                    type:type
                };
            $.ajax({
                    type: "post",
                    url: '<?php echo e(url("addlike")); ?>',
                    headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                     },
                    data: data,
                    cache: false,
                    success: function (data)
                    {
                        console.log(data);
                        if(data.status == "success"){
                                $(element).removeClass('like').addClass('unlike').html('<i class="fa fa-heart"></i>');

                        }
                    },
                    error: function (data){
                        alert('Fail to run like..');
                    }
                });


        });
</script>


<?php $__env->stopSection(); ?>
<?php echo $__env->make('layouts.app', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH C:\xampp\htdocs\offerswing\resources\views/events.blade.php ENDPATH**/ ?>