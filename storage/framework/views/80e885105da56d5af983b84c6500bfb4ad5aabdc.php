<?php $__env->startSection('content'); ?>
<div id="homepage-2">
    <div class="ps-container">
     
        <!-- main slider -->
        <div class="row mb-5">
            <div class="col-12 col-md-9 flex-grow-1">
                <?php $__env->startComponent('components.carousel',['slider'=> $slider_1]); ?>
                <?php $__env->slot('coursel_id'); ?>
                coursel
                <?php $__env->endSlot(); ?>
                <?php echo $__env->renderComponent(); ?>
            </div>
            <div class="col-md-3 flex-grow-1">
                <?php $__env->startComponent('components.carousel',['slider'=> $slider_2]); ?>
                <?php $__env->slot('coursel_id'); ?>
                coursel1
                <?php $__env->endSlot(); ?>
                <?php echo $__env->renderComponent(); ?>
            </div>
        </div>

        <!-- deals block -->
        <div class="ps-site-features bg-white mb-5">
            <div class="ps-block--site-features">
                <div class="ps-block__item justify-content-center">
                    <a href="<?php echo e(url('/wingdeals')); ?>">
                        <img src="<?php echo e(asset('main/images/icons/deals.png')); ?>" alt="" class="img-fluid"
                                style="height: 60px;">
                    </a>
                </div>
                <div class="ps-block__item justify-content-center">
                    <a href="<?php echo e(url('/event')); ?>">
                        <img src="<?php echo e(asset('main/images/icons/events.png')); ?>" alt="" class="img-fluid"
                                style="height: 50px;">
                    </a>
                </div>
                <div class="ps-block__item justify-content-center">
                    <a href="<?php echo e(url('/newsfeeds')); ?>">
                        <img src="<?php echo e(asset('main/images/icons/news.png')); ?>" alt="" class="img-fluid"
                            style="height: 50px;">
                    </a>
                </div>
                <div class="ps-block__item justify-content-center">
                    <div class="ps-block__left">
                        <img src="<?php echo e(asset('main/images/icons/wing.png')); ?>" alt="" class="img-fluid"
                            style="height: 50px;">
                        </div>
                    <div class="ps-block__right">
                    <a href="<?php echo e(url('/redeem')); ?>"><h4>Redeem</h4></a>
                    </div>
                </div>

                <div class="ps-block__item">
                    <div class="ps-block__left"><i class="icon-download2"></i></div>
                    <div class="ps-block__right">
                        <a href="https://play.google.com/store?hl=en" target="_blank"><h4>Download APP</h4></a>
                    </div>
                </div>
            </div>
        </div>
    </div> 

    <div class="ps-home-banner">
        <div class="ps-container">
            <div class="ps-section__left">
                <?php echo $__env->make('layouts.mainSidebar', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>
            </div>
            <div class="ps-section__center">
        
                <div class="ps-carousel--dots owl-slider" data-owl-auto="true" data-owl-loop="true" data-owl-speed="5000" data-owl-gap="0" data-owl-nav="false" data-owl-dots="true" data-owl-item="1" data-owl-item-xs="1" data-owl-item-sm="1" data-owl-item-md="1" data-owl-item-lg="1" data-owl-duration="1000" data-owl-mousedrag="on">
                    <a href="#"><img src="http://nouthemes.net/html/martfury/img/slider/home-5/1.jpg" alt=""></a>
                    <a href="#"><img src="http://nouthemes.net/html/martfury/img/slider/home-5/2.jpg" alt=""></a>
                    <a href="#"><img src="http://nouthemes.net/html/martfury/img/slider/home-5/3.jpg" alt=""></a>
                </div>
            </div>
            <div class="ps-section__right">
                <a href="#"><img src="http://nouthemes.net/html/martfury/img/slider/home-5/promotion-1.jpg" alt=""></a>
                <a href="#"><img src="http://nouthemes.net/html/martfury/img/slider/home-5/promotion-2.jpg" alt=""></a>
                <a class="wide" href="#"><img src="http://nouthemes.net/html/martfury/img/slider/home-5/promotion-3.jpg" alt=""></a>
                <a href="#"><img src="http://nouthemes.net/html/martfury/img/slider/home-5/promotion-4.jpg" alt=""></a>
                <a href="#"><img src="http://nouthemes.net/html/martfury/img/slider/home-5/promotion-5.jpg" alt=""></a>
            </div>
        </div>
    </div>



    <div class="ps-product-box mb-5">
        <div class="ps-container">
        
            <div class="row mt-5">
                <?php $__currentLoopData = $imagediv2_1; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $item): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                <div class="col-12 col-md-6">
                    <a class="ps-collection" href="#">
                        <img src="<?php echo e(asset("$item->image")); ?>" alt="">
                    </a>
                </div>
                <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
            </div>

            <div class="row mt-5">
                <?php $__currentLoopData = $imagediv3_1; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $item): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                <div class="col-12 col-md-4">
                    <a class="ps-collection" href="#">
                        <img src="<?php echo e(asset("$item->image")); ?>" alt="">
                    </a>
                </div>
                <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
            </div>

            <div class="row mt-5">
                <?php $__currentLoopData = $imagediv2_2; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $item): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                <div class="col-12 col-md-6">
                    <a class="ps-collection" href="#">
                        <img src="<?php echo e(asset("$item->image")); ?>" alt="">
                    </a>
                </div>
                <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
            </div>

            <div class="row mt-5">
                <?php $__currentLoopData = $imagediv3_2; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $item): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                <div class="col-12 col-md-4">
                    <a class="ps-collection" href="#">
                        <img src="<?php echo e(asset("$item->image")); ?>" alt="">
                    </a>
                </div>
                <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
            </div>

            <div class="row mt-5">
                <?php $__currentLoopData = $imagediv2_3; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $item): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                <div class="col-12 col-md-6">
                    <a class="ps-collection" href="#">
                        <img src="<?php echo e(asset("$item->image")); ?>" alt="">
                    </a>
                </div>
                <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
            </div>

            <div class="row mt-5">
                <?php $__currentLoopData = $imagediv3_3; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $item): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                <div class="col-12 col-md-4">
                    <a class="ps-collection" href="#">
                        <img src="<?php echo e(asset("$item->image")); ?>" alt="">
                    </a>
                </div>
                <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
            </div>

            <div class="row mt-5">
                <?php $__currentLoopData = $imagediv2_4; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $item): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                <div class="col-12 col-md-6">
                    <a class="ps-collection" href="#">
                        <img src="<?php echo e(asset("$item->image")); ?>" alt="">
                    </a>
                </div>
                <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
            </div>

            <div class="row mt-5">
                <?php $__currentLoopData = $imagediv2_5; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $item): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                <div class="col-12 col-md-6">
                    <a class="ps-collection" href="#">
                        <img src="<?php echo e(asset("$item->image")); ?>" alt="">
                    </a>
                </div>
                <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
            </div>

            <div class="row mt-5">
                <?php $__currentLoopData = $imagediv3_3; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $item): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                <div class="col-12 col-md-4">
                    <a class="ps-collection" href="#">
                        <img src="<?php echo e(asset("$item->image")); ?>" alt="">
                    </a>
                </div>
                <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
            </div>

        </div>
    </div>

</div>
<?php $__env->stopSection(); ?>
<?php echo $__env->make('layouts.app', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH C:\xampp\htdocs\offerswing\resources\views/welcome.blade.php ENDPATH**/ ?>