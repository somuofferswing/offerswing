<?php

namespace App\Http\Controllers;

use App\Models\Deal;
use App\Models\Notification;
use App\Models\Order;
use App\Models\OrderDetail;
use Illuminate\Support\Facades\Crypt;
use Illuminate\Support\Facades\Mail;

class DealsController extends Controller
{
    public function getDeal($slug)
    {
        $user = auth()->user();
        $user_id = $user['id'];
        $user_email = $user['email'];

        // $deal = Deal::find($deal_id);
        $deal = Deal::where(['slug'=>$slug])->first();
        $deal_id = $deal['id'];

        $order = OrderDetail::where('user_id', $user_id)->where('order_type', 'deal')->where('order_type_id', $deal_id)->where('claim_status', 0)->first();

        $previousorders = OrderDetail::where('user_id', $user_id)->where('order_type', 'DEAL')->where('order_type_id', $deal_id)->get();
        $previousorderscount = count($previousorders);

        
        if (empty($order) && ($previousorderscount <= $deal->max_count_for_user)) {
            if ('wingpoints' == $deal->payment_type) {
                $balance = $user->balance;
                if ($balance >= $deal->points_to_claim) {
                    $order = new Order();
                    $order_id = $this->generateRandomString(15);
                    $order->order_id = $order_id;
                    $order->user_id = $user_id;
                    $order->payment_type = $deal->payment_type;
                    $order->payment_status = 'PENDING';
                    $order->amount = $deal->points_to_claim;

                    $order_details = new OrderDetail();
                    $order_details->order_type = 'DEAL';
                    $order_details->user_id = $user_id;
                    $order_details->order_type_id = $deal_id;
                    $order_details->quantity = 1;
                    $order_details->price = $deal->points_to_claim;
                    $order_details->payment_type = $deal->payment_type;
                    $order_details->final_price = $deal->points_to_claim;
                    $order_details->valid_upto = date('Y-m-d', strtotime(time(). ' + '.$deal->deal_validity_in_days.' days'));

                    $order->save();
                    $order->orderDetails()->save($order_details);

                    $user->withdraw($deal->points_to_claim, 'withdraw', ['type' => 'Deal', 'description' => 'Spent on deal','id'=>$deal_id], 'Deal');

                    $order->payment_status = 'SUCCESS';
                    $order->save();

                    $deal->quantity -= 1;
                    $deal->save();

                    return redirect()->route('dealorder', [$order_id]);
                  
                   
                    // return redirect()->route('dealorder', [$order_id]);
                }
                return back()->with('alert', 'Not Enough WingCoins to claim')->with('class', 'warning');
            } elseif ('cash' == $deal->payment_type) {
                
                $days = '+'.$deal->deal_validity_in_days. ' days';
                $valid_upto = date('Y-m-d', strtotime($days));
                
                
                $order = new Order();
                $order_id = $this->generateRandomString(15);
                $order->order_id = $order_id;
                $order->user_id = $user_id;
                $order->payment_type = $deal->payment_type;
                $order->payment_status = 'PENDING';
                $order->amount = $deal->price;

                $order_details = new OrderDetail();
                $order_details->order_type = 'DEAL';
                $order_details->user_id = $user_id;
                $order_details->order_type_id = $deal_id;
                $order_details->quantity = 1;
                $order_details->price = $deal->price;
                $order_details->payment_type = $deal->payment_type;
                $order_details->final_price = $deal->price;
                $order_details->valid_upto = $valid_upto;

                $order->save();
                $order->orderDetails()->save($order_details);

                return redirect()->route('user_order', Crypt::encrypt($order->id));
            }
        } else {
            return back()->with('alert', 'Deal Claimed alredy')->with('class', 'warning');
        }
    }

    public function dealOrder($order_id)
    {
        $user = auth()->user();
        $order = Order::where(['order_id' => $order_id])->first();
        $order_details = $order->orderDetails;
        $deal_id = $order_details->order_type_id;
        $deal = Deal::find($deal_id);
        $user_id = $order_details->user_id;
       
        return view('frontend.dealorders', compact('order', 'order_details', 'deal'));
    }

    public function generateRandomString($length = 10)
    {
        $characters = '0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZ';
        $charactersLength = strlen($characters);
        $randomString = '';
        for ($i = 0; $i < $length; ++$i) {
            $randomString .= $characters[rand(0, $charactersLength - 1)];
        }

        return $randomString;
    }
}
