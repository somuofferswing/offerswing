<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Shop extends Model
{
    protected $fillable = [
        'name', 'email', 'phone_number', 'website', 'address', 'lat', 'lng', 'pincode', 'city', 'state', 'active', 'phone_number2', 'phone_number3', 'fax_no', 'active_days', 'start_time', 'end_time', 'category',
    ];

    public function categories()
    {
        return $this->belongsToMany(Category::class);
    }

    public function users()
    {
        return $this->belongsToMany(User::class);
    }

    public function amenities()
    {
        return $this->belongsToMany(Amenity::class);
    }

    public function images()
    {
        return $this->hasMany(ImageUpload::class);
    }



    public function shop_images()
    {
        return $this->images()->where('type', '=', 'shop_image');
    }

    public function menus()
    {
        return $this->images()->where('type', '=', 'shop_menu');
    }

    public function offers()
    {
        return $this->hasMany(Offer::class);
    }

    public function deals()
    {
        return $this->hasMany(Deal::class);
    }

    public function offersOngoing()
    {
        return $this->offers()->where('from_date', '<=', date('Y-m-d', time()));
    }

    public function dealsOngoing()
    {
        return $this->deals()->where('deal_end_date', '>=', date('Y-m-d', time()));
    }
}
