@extends('layouts.app')

@section('content')
<div class="ps-breadcrumb">
    <div class="container">
        <ul class="breadcrumb text-capitalize" id="breadcrumb">
            <li><a href="{{ url('/') }}">Home</a></li>
        </ul>
    </div>
</div>
<section class="m-5">
    <div class="container">
         <div class="pb-5">
            <div class="ps-carousel--nav-inside owl-slider" data-owl-auto="true" data-owl-loop="true" data-owl-speed="5000" data-owl-gap="0" data-owl-nav="true" data-owl-dots="true" data-owl-item="1" data-owl-item-xs="1" data-owl-item-sm="1" data-owl-item-md="1" data-owl-item-lg="1" data-owl-duration="1000" data-owl-mousedrag="on">
                <a href="#"><img src="http://nouthemes.net/html/martfury/img/slider/shop-default/1.jpg" alt=""></a>
                <a href="#"><img src="http://nouthemes.net/html/martfury/img/slider/shop-default/2.jpg" alt=""></a>
            </div>
        </div>
        @php
        $category_id = request()->route('slug');
        $category = \App\Models\Category::where(['slug'=> $category_id])->first();
        $parent_category = $category->parentCategories;
        @endphp
        <div class="row">
        @if ($sub_categories->isNotEmpty())
            @foreach ($sub_categories as $item)
                <div class="col-xl-3 col-lg-3 col-md-3 col-sm-4 col-6">
                    <div class="ps-block--category">
                        <a class="ps-block__overlay" href="{!! url('/subcategories',[$item->slug]); !!}"></a>
                        @if(!empty($item->cat_image))
                        <img src="<?php echo asset("$item->cat_image"); ?>" alt="" width="90" class="mb-4">
                        @endif
                        <p>{{ $item->category_name }}</p>
                    </div>
                </div>
            @endforeach
        @else
            <h3 class="text-danger text-uppercase">Data Not Available</h3>
        @endif
        </div>
    </div>
</section>

<script>
    $(document).ready(function(){
    var category = JSON.parse('{!! json_encode($category) !!}');
    var parent_category = JSON.parse('{!! json_encode($parent_category) !!}');
    if(parent_category != null){
       var data =  toarray([parent_category]);
       var da = {
                'id': category.id,
                'category_name':category.category_name,
                'parent_id':category.parent_id
            };

        data.unshift(da);
        data.reverse();
        breadcrumb(data);
    }else{
        var data = [];
        var da = {
                'id': category.id,
                'category_name':category.category_name,
                'parent_id':category.parent_id
            };
        data.unshift(da);
        breadcrumb(data);

    }

    function toarray(data,cat){
        var categories = (cat== null) ? []: cat;
        data.forEach(element => {
            id = element.id;
            category_name = element.category_name;
            parent_id = element.parent_id;

            var data = {
                'id': id,
                'category_name':category_name,
                'parent_id':parent_id
            };

            categories.push(data);
            var cat = categories;
            if(element.parent_categories) { toarray([element.parent_categories],cat); }

        });
        return categories;
    }

    function breadcrumb(data){
        data.forEach((element,key,array)=>{
            var id = element.id;
            var category_name = element.category_name.toLowerCase();
            var parent_id = element.parent_id;
            if (key === array.length - 1){

                var li = `<li>`+category_name+`</li>`;
                $('#breadcrumb').append(li);
            }else{

                var li = `<li><a href="{{ url('subcategories') }}/`+id+`">`+category_name+`</a></li>`;
                $('#breadcrumb').append(li);
            }
        });
    }
    });

</script>

@endsection
