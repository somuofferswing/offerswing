<?php $__env->startSection('content'); ?>

<link rel="stylesheet"
    href="<?php echo e(asset('adminlte/bower_components/bootstrap-datepicker/dist/css/bootstrap-datepicker.min.css')); ?>">

<style>
    /*Copied from bootstrap to handle input file multiple*/
    .btn {
        display: inline-block;
        padding: 6px 12px;
        margin-bottom: 0;
        font-size: 14px;
        font-weight: normal;
        line-height: 1.42857143;
        text-align: center;
        white-space: nowrap;
        vertical-align: middle;
        cursor: pointer;
        -webkit-user-select: none;
        -moz-user-select: none;
        -ms-user-select: none;
        user-select: none;
        background-image: none;
        border: 1px solid transparent;
        border-radius: 4px;
    }

    /*Also */
    /* .btn-success {
        border: 1px solid #c5dbec;
        background: #D0E5F5;
        font-weight: bold;
        color: #2e6e9e;
    } */

    /* This is copied from https://github.com/blueimp/jQuery-File-Upload/blob/master/css/jquery.fileupload.css */
    .fileinput-button {
        position: relative;
        overflow: hidden;
    }

    .fileinput-button input {
        position: absolute;
        top: 0;
        right: 0;
        margin: 0;
        opacity: 0;
        -ms-filter: 'alpha(opacity=0)';
        font-size: 200px;
        direction: ltr;
        cursor: pointer;
    }

    .thumb {
        height: 80px;
        width: 100px;
        border: 1px solid #000;
    }

    ul.thumb-Images li {
        width: 120px;
        float: left;
        display: inline-block;
        vertical-align: top;
        height: 120px;
    }

    .img-wrap {
        position: relative;
        display: inline-block;
        font-size: 0;
    }

    .img-wrap .close {
        position: absolute;
        top: 2px;
        right: 2px;
        z-index: 100;
        background-color: #D0E5F5;
        padding: 5px 2px 2px;
        color: #000;
        font-weight: bolder;
        cursor: pointer;
        opacity: .5;
        font-size: 23px;
        line-height: 10px;
        border-radius: 50%;
    }

    .img-wrap:hover .close {
        opacity: 1;
        background-color: #ff0000;
    }

    .FileNameCaptionStyle {
        font-size: 12px;
    }
</style>

<div class="content-wrapper">

    <section class="content-header">

        <h1>

            NewsFeed

            <small>Add News</small>

        </h1>

        <!-- <ol class="breadcrumb">

              <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>

              <li class="active">Dashboard</li>

            </ol> -->

    </section>



    <section class="content">



        <div class="box box-info">

            <div class="box-header with-border">

                <h3 class="box-title">Add News</h3>

            </div>

            <form class="form-horizontal" method="POST" action="<?php echo e(route('store-news')); ?>"
                enctype="multipart/form-data">

                <?php echo csrf_field(); ?>

                <div class="box-body">

                    <div class="form-group ">

                        <label for="title" class="col-sm-2 control-label">Title *</label>

                        <div class="col-sm-10">

                            <input id="name" type="text" class="form-control" name="title" value="<?php echo e(old('title')); ?>"
                                required>

                            <?php if($errors->has('title')): ?>

                            <div class="danger"><?php echo e($errors->first('title')); ?></div>

                            <?php endif; ?>

                        </div>

                    </div>

                    <div class="form-group">

                        <label for="news_image" class="col-sm-2 control-label">Images * </label>

                        <div class="col-sm-10">

                            <span class="btn btn-success fileinput-button">
                                <span>Select Attachment</span>
                                <input type="file" name="images[]" id="news_images" multiple
                                    accept="image/jpeg, image/png, image/gif,"><br />
                            </span>

                            <!-- <input type="file" class="form-control-file" id="news_images" name="news_images[]"  required multiple> -->

                            <div id="image_preview"></div>

                        </div>

                    </div>

                    <div class="form-group">

                        <label for="news_image" class="col-sm-2 control-label">Video Link </label>

                        <div class="col-sm-10">
                            <input type="text" class="form-control" id="video_link" name="video_link">
                        </div>

                    </div>

                    <div class="form-group ">

                        <label for="description" class="col-sm-2 control-label">Description *</label>

                        <div class="col-sm-10">

                            <textarea id="description" type="text" class="form-control" name="description" required
                                autocomplete="description" autofocus><?php echo e(old('description')); ?></textarea>

                            <?php if($errors->has('description')): ?>

                            <div class="danger"><?php echo e($errors->first('description')); ?></div>

                            <?php endif; ?>

                        </div>

                    </div>



                    <div class="pull-right">

                        <button type="submit" class="btn btn-info" id="submit" ><?php echo e(__('Create')); ?></button>

                        <button type="button" class="btn btn-danger"
                            onclick="window.history.go(-1); return false;"><?php echo e(__('Cancel')); ?></button>

                    </div>

                </div>

            </form>

        </div>

    </section>

</div>

<?php $__env->stopSection(); ?>

<?php $__env->startSection('scripts'); ?>

<script src="<?php echo e(asset('adminlte/bower_components/bootstrap-datepicker/dist/js/bootstrap-datepicker.min.js')); ?>">

</script>

<script src="<?php echo e(asset('adminlte/bower_components/ckeditor/ckeditor.js')); ?>"></script>

<script>
    $(function () {



    CKEDITOR.replace('description')

  })

</script>

<script type="text/javascript">

document.addEventListener("DOMContentLoaded", init, false);

var AttachmentArray = [];
var arrCounter = 0;
var filesCounterAlertStatus = false;

var ul = document.createElement('ul');
ul.className = ("thumb-Images");
ul.id = "imgList";

function init() {
    document.querySelector('#news_images').addEventListener('change', handleFileSelect, false);
}

function handleFileSelect(e) {
       if (!e.target.files) return;

    var files = e.target.files;

    // console.log(files);

    for (var i = 0, f; f = files[i]; i++) {
        var fileReader = new FileReader();
        fileReader.onload = (function (readerEvt) {
            return function (e) {
                ApplyFileValidationRules(readerEvt)
                RenderThumbnail(e, readerEvt);
                FillAttachmentArray(e, readerEvt)
            };
        })(f);

        fileReader.readAsDataURL(f);
    }
    document.getElementById('news_images').addEventListener('change', handleFileSelect, false);
}

jQuery(function ($) {
    $('div').on('click', '.img-wrap .close', function () {
        var id = $(this).closest('.img-wrap').find('img').data('id');

        var elementPos = AttachmentArray.map(function (x) { return x.FileName; }).indexOf(id);
        if (elementPos !== -1) {
            AttachmentArray.splice(elementPos, 1);
        }

        $(this).parent().find('img').not().remove();
        $(this).parent().find('div').not().remove();
        $(this).parent().parent().find('div').not().remove();

        var lis = document.querySelectorAll('#imgList li');
        for (var i = 0; li = lis[i]; i++) {
            if (li.innerHTML == "") {
                li.parentNode.removeChild(li);
            }
        }
    });
}
)

function ApplyFileValidationRules(readerEvt)
{

    if (CheckFileType(readerEvt.type) == false) {
        alert("The file (" + readerEvt.name + ") does not match the upload conditions, You can only upload jpg/png/gif files");
        e.preventDefault();
        return;
    }

    if (CheckFileSize(readerEvt.size) == false) {
        alert("The file (" + readerEvt.name + ") does not match the upload conditions, The maximum file size for uploads should not exceed 300 KB");
        e.preventDefault();
        return;
    }

    if (CheckFilesCount(AttachmentArray) == false) {
        if (!filesCounterAlertStatus) {
            filesCounterAlertStatus = true;
            alert("You have added more than 10 files. According to upload conditions you can upload 10 files maximum");
        }
        e.preventDefault();
        return;
    }
}

function CheckFileType(fileType) {
    if (fileType == "image/jpeg") {
        return true;
    }
    else if (fileType == "image/png") {
        return true;
    }
    else if (fileType == "image/gif") {
        return true;
    }
    else {
        return false;
    }
    return true;
}

function CheckFileSize(fileSize) {
    if (fileSize < 300000) {
        return true;
    }
    else {
        return false;
    }
    return true;
}

function CheckFilesCount(AttachmentArray) {
    var len = 0;
    for (var i = 0; i < AttachmentArray.length; i++) {
        if (AttachmentArray[i] !== undefined) {
            len++;
        }
    }

    if (len > 9) {
        return false;
    }
    else
    {
        return true;
    }
}

function RenderThumbnail(e, readerEvt)
{
    var li = document.createElement('li');
    ul.appendChild(li);
    li.innerHTML = ['<div class="img-wrap row"> <span class="close">&times;</span>' +
        '<img class="thumb" src="', e.target.result, '" title="', escape(readerEvt.name), '" data-id="',
        readerEvt.name, '" />' + '</div>'].join('');

    var div = document.createElement('div');
    div.className = "FileNameCaptionStyle";
    li.appendChild(div);
    div.innerHTML = [readerEvt.name].join('');
    document.getElementById('image_preview').insertBefore(ul, null);
}

function FillAttachmentArray(e, readerEvt)
{
    AttachmentArray[arrCounter] =
    {
        AttachmentType: 1,
        ObjectType: 1,
        FileName: readerEvt.name,
        FileDescription: "Attachment",
        NoteText: "",
        MimeType: readerEvt.type,
        Content: e.target.result.split("base64,")[1],
        FileSizeInBytes: readerEvt.size,
    };
    arrCounter = arrCounter + 1;
}

function beforesubmit() {

}

$('#submit').on('click',function(){
    console.log(AttachmentArray);
    $('#news_images').val('');
    $('#news_images').val(AttachmentArray);
    return true;

})
</script>

<?php $__env->stopSection(); ?>

<?php echo $__env->make('layouts.admin', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH C:\wamp64\www\offerswing\resources\views/backend/news/create.blade.php ENDPATH**/ ?>