

<?php $__env->startSection('content'); ?>
<link rel="stylesheet"
    href="<?php echo e(asset('adminlte/bower_components/bootstrap-datepicker/dist/css/bootstrap-datepicker.min.css')); ?>">
<link rel="stylesheet" href="<?php echo e(asset('adminlte/plugins/timepicker/bootstrap-timepicker.min.css')); ?>">
<div class="content-wrapper">
    <section class="content-header">
        <h1>
            Events
            <small>Add Event</small>
        </h1>
        <!-- <ol class="breadcrumb">
              <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
              <li class="active">Dashboard</li>
            </ol> -->
    </section>

    <section class="content">

        <div class="box box-info">
            <div class="box-header with-border">
                <h3 class="box-title">Add Event</h3>
            </div>
            <form class="form-horizontal" method="POST" action="<?php echo e(route('store-events')); ?>"
                enctype="multipart/form-data">
                <?php echo csrf_field(); ?>
                <h3>Event owner </h3>
                <select class="form-control" name="vendor_id" id="parent_id" required>
                    <?php $__currentLoopData = $retailers; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $item): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                    <option value="<?php echo e($item->id); ?>"><?php echo e($item->first_name); ?> <?php echo e($item->last_name); ?>(<?php echo e($item->email); ?>)
                    </option>
                    <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                </select>
                <hr>
                <h3>Event Details</h3>
                <div class="box-body">
                    <div class="form-group ">
                        <label for="title" class="col-sm-2 control-label">Event Name/Title</label>
                        <div class="col-sm-10">
                            <input id="name" type="text" class="form-control" name="title" value="<?php echo e(old('title')); ?>"
                                required>
                            <?php if($errors->has('title')): ?>
                            <div class="danger"><?php echo e($errors->first('title')); ?></div>
                            <?php endif; ?>
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="event_image" class="col-sm-2 control-label">Image</label>
                        <div class="col-sm-10">
                            <input type="file" class="form-control-file" id="event_image" name="event_image"
                                onchange="loadFile(event)" required>
                            <img id="output" height="100px" width="100px" />
                        </div>
                    </div>
                    <div class="form-group ">
                        <label for="description" class="col-sm-2 control-label">Description</label>
                        <div class="col-sm-10">
                            <textarea id="description" type="text" class="form-control" name="description" required
                                autocomplete="description" autofocus><?php echo e(old('description')); ?></textarea>
                            <?php if($errors->has('description')): ?>
                            <div class="danger"><?php echo e($errors->first('description')); ?></div>
                            <?php endif; ?>
                        </div>
                    </div>

                    <div class="form-group ">
                        <label for="terms_conditions" class="col-sm-2 control-label">Terms & Conditions</label>
                        <div class="col-sm-10">
                            <textarea id="terms_conditions" class="form-control" name="terms_conditions" required
                                autocomplete="description" autofocus><?php echo e(old('terms_conditions')); ?></textarea>
                            <?php if($errors->has('terms_conditions')): ?>
                            <div class="danger"><?php echo e($errors->first('terms_conditions')); ?></div>
                            <?php endif; ?>
                        </div>
                    </div>

                    <div id="map" style="height:400px;width:100%">
                    </div>

                    <div class="form-group ">
                        <label for="event_address" class="col-sm-2 control-label">Address</label>
                        <div class="col-sm-10">
                            <textarea id="address" class="form-control" name="event_address" required
                                autocomplete="description" autofocus><?php echo e(old('event_description')); ?></textarea>
                            <?php if($errors->has('event_address')): ?>
                            <div class="danger"><?php echo e($errors->first('event_address')); ?></div>
                            <?php endif; ?>
                        </div>
                    </div>



                    <div class="form-group ">
                        <label for="event_lat" class="col-sm-2 control-label">Lat</label>
                        <div class="col-sm-10">
                            <input id="lat" type="text" class="form-control" name="event_lat" required
                                autocomplete="event_lat" value='<?php echo e(old('event_lat')); ?>' autofocus>
                            <?php if($errors->has('event_lat')): ?>
                            <div class="danger"><?php echo e($errors->first('event_lat')); ?></div>
                            <?php endif; ?>
                        </div>
                    </div>

                    <div class="form-group ">
                        <label for="event_lng" class="col-sm-2 control-label">lng</label>
                        <div class="col-sm-10">
                            <input id="lng" type="text" class="form-control" name="event_lng" required
                                autocomplete="description" value='<?php echo e(old('event_lng')); ?>' autofocus>
                            <?php if($errors->has('event_lng')): ?>
                            <div class="danger"><?php echo e($errors->first('event_lng')); ?></div>
                            <?php endif; ?>
                        </div>
                    </div>
                    <div class="form-group ">
                        <label for="event_date" class="col-sm-2 control-label">Date</label>
                        <div class="col-sm-10">
                            <input id="event_date" type="text" class="form-control" name="event_date" required
                                autocomplete="description" value='<?php echo e(old('event_date')); ?>' autofocus>
                            <?php if($errors->has('event_date')): ?>
                            <div class="danger"><?php echo e($errors->first('event_date')); ?></div>
                            <?php endif; ?>
                        </div>
                    </div>
                    <div class="form-group ">
                        <label for="event_time" class="col-sm-2 control-label">time</label>
                        <div class="col-sm-10">
                            <input id="event_time" type="text" class="form-control" name="event_time" required
                                autocomplete="description" value='<?php echo e(old('event_date')); ?>'>
                            <?php if($errors->has('event_time')): ?>
                            <div class="danger"><?php echo e($errors->first('event_time')); ?></div>
                            <?php endif; ?>
                        </div>
                    </div>
                    <div class="form-group ">
                        <label for="payment_type" class="col-sm-2 control-label">Payment Type</label>
                        <div class="col-sm-10">
                            <select class="form-control" id="payment_type" name="payment_type" required autofocus>
                                <option value="cash">Cash</option>
                                <option value="wingpoints">Wing Points</option>
                            </select>
                            <?php if($errors->has('payment_type')): ?>
                            <div class="danger"><?php echo e($errors->first('payment_type')); ?></div>
                            <?php endif; ?>
                        </div>
                    </div>
                    <div class="form-group points_to_claim" style="display: none">
                        <label for="points_to_claim" class="col-sm-2 control-label">Wing points to claim</label>
                        <div class="col-sm-10">
                            <input id="points_to_claim" type="text" class="form-control" name="points_to_claim"
                                value="<?php echo e(old('points_to_claim')); ?>" autofocus>
                            <?php if($errors->has('points_to_claim')): ?>
                            <div class="danger"><?php echo e($errors->first('points_to_claim')); ?></div>
                            <?php endif; ?>
                        </div>
                    </div>
                    <div class="form-group event_price">
                        <label for="event_price" class="col-sm-2 control-label">Price</label>
                        <div class="col-sm-10">
                            <input id="event_price" type="text" class="form-control" name="event_price" 
                                autocomplete="description" value='<?php echo e(old('event_price')); ?>'>
                            <?php if($errors->has('event_price')): ?>
                            <div class="danger"><?php echo e($errors->first('event_price')); ?></div>
                            <?php endif; ?>
                        </div>
                    </div>
                    <div class="form-group ">
                        <label for="quantity" class="col-sm-2 control-label">Quantity</label>
                        <div class="col-sm-10">
                            <input id="quantity" type="text" class="form-control" name="quantity" required
                                autocomplete="quantity" value='<?php echo e(old('quantity')); ?>' autofocus>
                            <?php if($errors->has('quantity')): ?>
                            <div class="danger"><?php echo e($errors->first('quantity')); ?></div>
                            <?php endif; ?>
                        </div>
                    </div>
                    <div class="form-group ">
                        <label for="date_time_description" class="col-sm-2 control-label">Price & Time
                            Description</label>
                        <div class="col-sm-10">
                            <textarea id="terms_condidate_time_descriptiontions" class="form-control"
                                name="date_time_description" autocomplete="description"
                                autofocus><?php echo e(old('date_time_description')); ?></textarea>
                            <?php if($errors->has('terms_conditions')): ?>
                            <div class="danger"><?php echo e($errors->first('date_time_description')); ?></div>
                            <?php endif; ?>
                        </div>
                    </div>

                    <div class="pull-right">
                        <button type="submit" class="btn btn-info"><?php echo e(__('Create')); ?></button>
                        <button type="button" class="btn btn-danger"
                            onclick="window.history.go(-1); return false;"><?php echo e(__('Cancel')); ?></button>
                    </div>
                </div>
            </form>
        </div>
    </section>
</div>
<?php $__env->stopSection(); ?>
<?php $__env->startSection('scripts'); ?>

<script src="<?php echo e(asset('adminlte/bower_components/bootstrap-datepicker/dist/js/bootstrap-datepicker.min.js')); ?>">
</script>
<script src="<?php echo e(asset('adminlte/bower_components/ckeditor/ckeditor.js')); ?>"></script>
<script src="<?php echo e(asset('adminlte/plugins/timepicker/bootstrap-timepicker.min.js')); ?>"></script>
<script>
    $(function () {
    CKEDITOR.replace('description');
    CKEDITOR.replace('terms_conditions');
    CKEDITOR.replace('date_time_description');
  })
</script>
<script>
    var loadFile = function(event) {
            var output = document.getElementById('output');
            output.src = URL.createObjectURL(event.target.files[0]);
          };
          $(document).ready(function() {
            var date = new Date();
            $('#event_date').datepicker({
                autoclose: true,
                format: 'yyyy-mm-dd',
                startDate: new Date(),
            });

            $('#event_time').timepicker({
            showInputs: false
        });
          });

    function initMap() {
       var  map = new google.maps.Map(document.getElementById('map'), {
          center: {lat: 18.6177, lng: 84.0167},
          zoom: 6
        });
        var input = document.getElementById('address');
        var autocomplete = new google.maps.places.Autocomplete(input);

        autocomplete.bindTo('bounds', map);

        // Set the data fields to return when the user selects a place.
        autocomplete.setFields(
            ['address_components', 'geometry', 'icon', 'name']);

        var infowindow = new google.maps.InfoWindow();
        var infowindowContent = document.getElementById('infowindow-content');
        infowindow.setContent(infowindowContent);
        var marker = new google.maps.Marker({
          map: map,
          anchorPoint: new google.maps.Point(0, -29)
        });

        autocomplete.addListener('place_changed', function() {
          infowindow.close();
          marker.setVisible(false);
          var place = autocomplete.getPlace();
          console.log(place);
          if (!place.geometry) {
            window.alert("No details available for input: '" + place.name + "'");
            return;
          }

          if (place.geometry.viewport) {
            map.fitBounds(place.geometry.viewport);
          } else {
            map.setCenter(place.geometry.location);
            map.setZoom(17);
          }
          marker.setPosition(place.geometry.location);
          marker.setVisible(true);
          $('#lat').val(place.geometry.location.lat());
          $('#lng').val(place.geometry.location.lng());

          var address = '';
          if (place.address_components) {
            address = [
              (place.address_components[0] && place.address_components[0].short_name || ''),
              (place.address_components[1] && place.address_components[1].short_name || ''),
              (place.address_components[2] && place.address_components[2].short_name || '')
            ].join(' ');
          }

          infowindowContent.children['place-icon'].src = place.icon;
          infowindowContent.children['place-name'].textContent = place.name;
          infowindowContent.children['place-address'].textContent = address;
          infowindow.open(map, marker);

          $.each(place.address_components, function (i, address_component) {
            if (address_component.types[0] == "locality"){
                $("#city").val(address_component.long_name);
            }

            if (address_component.types[0] == "administrative_area_level_1"){
                $("#state").val(address_component.long_name);
            }

            if (address_component.types[0] == "postal_code"){
                $("#pincode").val(address_component.long_name);
            }
          });

      });
    }
</script>
<script
    src="http://maps.googleapis.com/maps/api/js?key=AIzaSyBHLFZ4C3XB7MDwjywzpBC1ZWMH0u2zuE4&libraries=places&callback=initMap"
    async defer>
</script>
<script>
    $('#payment_type').on('change',function(){
            var type = $(this).val();
            if(type == 'wingpoints'){
                $('.points_to_claim').show();
                $('.event_price').hide();
            }else{
                $('.points_to_claim').hide();
                $('.event_price').show();
            }

        });
</script>
<?php $__env->stopSection(); ?>

<?php echo $__env->make('layouts.admin', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH /home/offerkrx/public_html/offerswing_test/resources/views/backend/events/create.blade.php ENDPATH**/ ?>