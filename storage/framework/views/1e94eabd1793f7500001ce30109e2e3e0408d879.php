<?php $__env->startSection('content'); ?>
<div class="content-wrapper">
    <section class="content-header">
        <h1>
            Category
            <small>Add Category</small>
        </h1>
        <!-- <ol class="breadcrumb">
              <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
              <li class="active">Dashboard</li>
            </ol> -->
    </section>

    <section class="content">

        <div class="col-md-10 col-md-offset-1">
            <div class="box box-info">
                <div class="box-header with-border">
                    <h3 class="box-title">Settings</h3>
                </div>
                <form class="form-horizontal" method="POST" action="<?php echo e(url('/admin/savesettings')); ?>"
                    enctype="multipart/form-data">
                    <?php echo csrf_field(); ?>
                    <div class="box-body">


                        <h4><b>Wing Points Settings</b></h4>
                        <hr>
                        <div class="form-group">
                            <label for="wing_points" class="col-sm-2 control-label">Welcome Points</label>
                            <div class="col-sm-10">
                                <input id="wing_points" type="text" class="form-control" name="welcome_points"
                                    value="<?php echo e($settings['welcome_points']); ?>" required autocomplete="wing_points" autofocus>
                            </div>
                        </div>

                        <div class="form-group">
                            <label for="referral_points" class="col-sm-2 control-label">Referral Points</label>
                            <div class="col-sm-10">
                                <input id="referral_points" type="text" class="form-control"
                                    name="referral_points" value="<?php echo e($settings['referral_points']); ?>" required
                                     autofocus>
                            </div>
                        </div>

                        <div class="form-group">
                            <label for="referral_points_per_rupee" class="col-sm-2 control-label">Referral Points for 1
                                rupee</label>
                            <div class="col-sm-10">
                                <input id="referral_points_per_rupee" type="text" class="form-control"
                                    name="referral_points_per_rupee" value="<?php echo e($settings['referral_points_per_rupee']); ?>" required
                                    autocomplete="referral_points_per_rupee" autofocus>
                            </div>
                        </div>

                        <br>

                        <h4><b>Contact Settings</b></h4>
                        <hr>

                        <div class="form-group">
                            <label for="contact_email" class="col-sm-2 control-label">Contact Email</label>
                            <div class="col-sm-10">
                                <input id="contact_email" type="text" class="form-control"
                                    name="contact_email" value="<?php echo e($settings['contact_email']); ?>" required
                                    autocomplete="contact_email" autofocus>
                            </div>
                        </div>

                        <div class="form-group">
                            <label for="contact_mobile" class="col-sm-2 control-label">Contact Mobile</label>
                            <div class="col-sm-10">
                                <input id="contact_mobile" type="text" class="form-control"
                                    name="contact_mobile" value="<?php echo e($settings['contact_mobile']); ?>" required
                                    autocomplete="contact_mobile" autofocus>
                            </div>
                        </div>

                        <button type="submit" class="btn btn-info pull-right"><?php echo e(__('Save')); ?></button>
                    </div>
                </form>
            </div>
        </div>
    </section>
</div>
<?php $__env->stopSection(); ?>

<?php echo $__env->make('layouts.admin', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH C:\wamp64\www\offers\resources\views/admin/settings.blade.php ENDPATH**/ ?>