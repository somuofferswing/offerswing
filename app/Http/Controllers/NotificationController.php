<?php

namespace App\Http\Controllers;

use App\Models\Notification;

class NotificationController extends Controller
{
    public function getUserNotification()
    {
        $user = auth()->user();
        $user_id = $user['id'];

        $notifications = Notification::where('user_id', $user_id)->get();
        $read_notifications = Notification::where('user_id', $user_id)->where('is_read', 1)->get();
        $unread_notifications = Notification::where('user_id', $user_id)->where('is_read', 0)->get();

        return view('frontend.notifications', compact('read_notifications', 'unread_notifications', 'notifications'));
    }
}
