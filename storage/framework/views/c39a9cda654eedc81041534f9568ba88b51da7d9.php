

<?php $__env->startSection('content'); ?>
<div class="content-wrapper">
    <section class="content-header">
        <h1>
            Token
            <small>Add Token</small>
        </h1>
        <!-- <ol class="breadcrumb">
              <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
              <li class="active">Dashboard</li>
            </ol> -->
    </section>

    <section class="content">
        
        <div class="box box-info">
            <div class="box-header with-border">
                <h3 class="box-title">Add Token</h3>
            </div>
            <form class="form-horizontal" method="POST" action="<?php echo e(url('/token/store')); ?>"
                enctype="multipart/form-data">
                <?php echo csrf_field(); ?>

                <div class="form-group">
                    <label for="token_id" class="col-sm-1 control-label">Token Id</label>
                    <div class="col-sm-11">
                        <input id="token_id" type="text" class="form-control" name="token_id"
                            value="<?php echo e(old('token_id')); ?>" required >
                    </div>
                </div>

                <div class="form-group">
                    <label for="points_rewarded" class="col-sm-1 control-label">Points Awarded</label>
                    <div class="col-sm-11">
                        <input id="points_rewarded" type="text" class="form-control" name="points_rewarded"
                            value="<?php echo e(old('points_rewarded')); ?>" required>
                    </div>
                </div>

                <div class=" pull-right">
                    <button type="submit" class="btn btn-info "><?php echo e(__('Add Token')); ?></button>
                    <button type="button" class="btn btn-danger "><?php echo e(__('Cancel')); ?></button>

                </div>

            </form>
        </div>
</div>

</section>
</div>
<?php $__env->stopSection(); ?>

<?php echo $__env->make('layouts.admin', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH /home/offerkrx/public_html/offerswing/resources/views/token/create.blade.php ENDPATH**/ ?>