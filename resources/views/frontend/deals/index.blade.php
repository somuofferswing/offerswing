@extends('layouts.admin')



@section('content')

<div class="content-wrapper">

    <section class="content-header">

        <h1>

            Wing Deals

            <small>All Wing Deals</small>

        </h1>

        <!-- <ol class="breadcrumb">

              <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>

              <li class="active">Dashboard</li>

            </ol> -->

    </section>

    <section class="content">

        @if(session()->get('success'))

        <div class="alert alert-success" id="success">

            {{ session()->get('success') }}

        </div><br />

        @endif

        @if (Auth::user()->user_type == 1)

        <div class="box box-primary">

            <div class="box-header with-border">

                <h3 class="box-title">Deals filter</h3>

            </div>

            <div class="box-body">



                <form action="{{ route('deals') }}" id="filter">

                    <div class="row">

                        <div class="form-group  col-sm-6">

                            <label for="shop_id" class="col-sm-2 control-label">Shop</label>

                            <div class="col-sm-10">



                                <select class="form-control" id="shop_id" name="shop_id" value="{{ old('shop_id') }}">

                                    <option value="">--Select--</option>

                                    @foreach($shops as $shop)
                                    <option value="{{ $shop->id }}">{{ $shop->shop_name }}</option>

                                    @endforeach

                                </select>

                            </div>

                        </div>

                        <div class="form-group col-sm-6">

                            <label for="category_id" class="col-sm-2 control-label">Category</label>

                            <div class="col-sm-10">



                                <select class="form-control" id="category_id" name="category_id"

                                    value="{{ old('category_id') }}">

                                    <option value="">--Select--</option>

                                    @foreach($category as $key =>$cat)

                                    <option value="{{ $key }}">{{ $cat }}</option>

                                    @endforeach

                                </select>

                                @if ($errors->has('category_id'))

                                <div class="danger">{{ $errors->first('category_id') }}</div>

                                @endif

                            </div>

                        </div>

                    </div>

                    <div>

                        <button type="submit" class="btn btn-primary">Filter</button>

                        <button type="reset" class="btn btn-warning" id="reset">Reset</button>

                    </div>

                </form>



            </div>

        </div>



        @endif





        <div class="box">

            <div class="box-header with-border">

                <h3 class="box-title">All deals</h3>

            </div>



            <div class="table-responsive">

                <table class="table table-striped">

                    <thead>

                        <tr>

                            <th>DealID</th>

                            <th>Name</th>

                            <th>Description</th>

                            <th>Start Date</th>

                            <th>End Date</th>

                            <th>Price</th>

                            <th>Percentage Discount</th>

                            <th>Max Discount</th>

                            <th colspan="2">Action</th>

                        </tr>

                    </thead>

                    <tbody>

                        @foreach($deals as $deal)

                        <tr>

                            <td>{{$deal->deal_id}}</td>

                            <td>{{$deal->name}}</td>

                            <td>{!! $deal->description !!}</td>

                            <td>{{$deal->valid_from}}</td>

                            <td>{{$deal->valid_upto}}</td>

                            <td>{{$deal->price}}</td>

                            <td>{{$deal->discount_percentage}}</td>

                            <td>{{$deal->max_discount}}</td>

                            @if (Auth::user()->user_type == 3)

                            <td><a class="btn btn-default" href="{{ url('viewshopdeal', [$deal->deal_id]) }}">view

                                    details</a>

                            </td>

                            {{-- <td><a class="btn btn-default" href="{{ url('/deal/claims', [$deal->deal_id]) }}">view

                                    Deal claims</a>

                            </td> --}}

                            @else

                            <td><a class="btn btn-default" href="{{ route('edit-deal', [$deal->deal_id]) }}">Edit</a>

                            </td>



                            <td>

                                <form action="{{ route('delete-deal', [$deal->deal_id])}}" method="post">

                                    @csrf

                                    @method('DELETE')

                                    <button class="btn btn-danger" type="submit">Delete</button>

                                </form>

                            </td>

                            @endif





                        </tr>

                        @endforeach

                    </tbody>

                </table>

            </div>

        </div>

    </section>

</div>

@endsection

@section('scripts')

<script>

    $('#success').delay(5000).fadeOut('slow');

    $('#reset').on('click',function(){

        // $("#filter")[0].reset();

        $('#shop_id').val('');

        $('#category_id').val('');

        $('#filter')[0].submit();

    });



</script>

@endsection

