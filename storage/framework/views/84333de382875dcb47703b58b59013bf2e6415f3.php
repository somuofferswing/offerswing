<?php $__env->startSection('content'); ?>

<style>
    td {

        /* overflow: hidden; */

        max-width: 400px;

        word-wrap: break-word;

    }
</style>

<div class="content-wrapper">

    <section class="content-header">

        <h1>

            Wing Deals

            <small>View Wing Deal(<?php echo e($deal->name); ?>)</small>

        </h1>

    </section>



    <section class="content">

        <img src="<?php echo e(asset('storage/'.$deal->pic)); ?>" alt="<?php echo e($deal->name); ?>" class="img-responsive" width="100%">

        <hr>



        <div class="table-responsive">

            <table class="table table-striped">

                <tr>

                    <td>Id</td>

                    <td><?php echo e($deal->id); ?></td>

                </tr>



                <tr>

                    <td>Name</td>

                    <td><?php echo e($deal->name); ?></td>

                </tr>



                <tr>

                    <td>Description</td>

                    <td><?php echo $deal->description; ?></td>

                </tr>



                <tr>

                    <td>Payment Type</td>

                    <td><?php echo e($deal->payment_type); ?></td>

                </tr>

                <tr>

                    <td>Price</td>

                    <td>
                        <?php echo e($deal->price); ?>

                    </td>

                </tr>

                <tr>

                    <td>Deals Quantity</td>

                    <td><?php echo e($deal->quantity); ?></td>

                </tr>

                <tr>

                    <td>Claims Per user</td>

                    <td><?php echo e($deal->max_count_for_user); ?></td>

                </tr>

                <tr>

                    <td>Deal Show From</td>

                    <td><?php echo e($deal->deal_start_date); ?></td>

                </tr>

                <tr>

                    <td>Deal show To</td>

                    <td><?php echo e($deal->deal_end_date); ?></td>

                </tr>

                <tr>

                    <td>Discount Percentage</td>

                    <td><?php echo e((($deal->original_price- $deal->price )/$deal->original_price) * 100); ?></td>

                </tr>
                



                <tr>

                    <td>Terms & Conditions</td>

                    <td><?php echo $deal->terms_conditions; ?></td>

                </tr>



            </table>

        </div>





        <hr>



        Deal Orders



        <table class="table">

            <tr>

                <th>Unique Claim Code</th>

                <th>User Name</th>

                <th>User Mobile</th>

                <th>User Email</th>

                <th>Bought On</th>

                <th>Claim Status</th>

                <th>Actions</th>

            </tr>



            <?php $__currentLoopData = $OrderDetail; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $item): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>

            <?php
            $order = $item->order;
            $user = $order->user;
            ?>


            <tr>

                <td><?php echo e($order->order_id); ?></td>

                <td><?php echo e($user->first_name.' '.$user->last_name); ?></td>

                <td><?php echo e($user->mobile); ?></td>

                <td><?php echo e($user->email); ?></td>

                <td><?php echo e($item->created_at); ?></td>

                <td><?php if($item->claim_status ==0): ?>

                    Not Used

                    <?php else: ?>

                    Used

                    <?php endif; ?></td>

                <td>

                    <?php if($item->claim_status ==0): ?>

                    <div class="btn btn-info changestatus" data-id="<?php echo e($order->order_id); ?>">

                        Change to used

                    </div>

                    <?php else: ?>

                    <div class="btn btn-danger changestatus" data-id="<?php echo e($order->order_id); ?>">

                        Change to not used

                    </div>

                    <?php endif; ?>

                </td>





            </tr>





            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>



        </table>

    </section>

</div>

<?php $__env->stopSection(); ?>

<?php $__env->startSection('scripts'); ?>

<script>
    $('.changestatus').on('click',function(){

        var id = $(this).data('id');
        // alert(id);
        data = {
            order_id:id
        }
        console.log(data);

        $.ajax({

        type: "POST",

        url: "<?php echo e(url('/claimStatus')); ?>",
        headers: {

            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')

            },

        data: data,

        success: function (response) {
            console.log(response);

            if(response == 'Success'){

                window.location.reload();

            }else{
                console.log(response);

                alert(response);

            }

        }

    });



    });

    

</script>



<?php $__env->stopSection(); ?>
<?php echo $__env->make('layouts.admin', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH C:\wamp64\www\offers\resources\views/backend/deals/show.blade.php ENDPATH**/ ?>