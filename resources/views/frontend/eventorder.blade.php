@extends('layouts.app')

@section('content')
<section >
    <div class="ps-page--product">
        <div class="container">
{{-- @php
    echo "<pre>";
    print_r($event);die;
@endphp --}}
            <div class="row">
                <div class="col-12 col-md-4">
                    <div class="ps-post text-center">
                        <img src="{{ asset('storage/'.$event->event_image)}}">
                        <h3 class="pt-4 m-0">{{ $event->title }}</h3>
                    </div>
                </div>
                <div class="col-12 col-md-8">
                    <div class="text-center">
                        {!! QrCode::size(250)->generate($order->order_id); !!}
                        <div>
                            <strong>Event Date : {{ date('d-m-Y',strtotime($event->event_date)) }}</strong>
                            <strong class="pl-4">Event Time : {{ date('h:i A',strtotime($event->event_time)) }}</strong>
                        </div>
                        
                        <div class="table-responsive">
                        <table class="table table-bordered ps-table ps-table--specification">
                                <tbody>
                                    {{-- <tr>
                                        <td>Booking id</td>
                                        <td>{{ $claimed->unique_code }}</td>
                                    </tr> --}}
                                    <tr>
                                        <td>About Event</td>
                                        <td>{!! $event->description !!}</td>
                                    </tr>
                                    <tr>
                                        <td>Brought on</td>
                                        <td>{{ date('d-m-Y h:i A',strtotime($order_details->created_at)) }}</td>
                                    </tr>
                                    <tr>
                                        <td>Event Date</td>
                                        <td>{{ date('d-m-Y',strtotime($event->event_date)) }}</td>
                                    </tr>
                                    <tr>
                                        <td>Event Time</td>
                                        <td>{{ date('h:i A',strtotime($event->event_time)) }}</td>
                                    </tr>
                                    <tr>
                                        <td>Number of Tickets</td>
                                        <td>{{ $order->quantity }}</td>
                                    </tr>
                                    <tr>
                                        <td>Price</td>
                                        <td>
                                        @if ( $order_details->payment_type = 'wingpoints')
                                        {{ $order_details->quantity }} x {{ $event->points_to_claim }} = {{ $order_details->quantity *  $event->points_to_claim }}
                                        @endif
                                        @if ( $order_details->payment_type = 'cash')
                                        {{ $order->quantity }} x {{ $event->price }} = {{ $order->quantity *  $event->price }}
                                        @endif
                                        </td>
                                        {{-- <td>{{ $order->quantity }} x {{ $event->price }} = {{ $order->quantity *  $event->price }}</td> --}}
                                    </tr>
                                </tbody>
                            
                            </table>
                        </div>
                    </div>
                    <div class="ps-product__content pt-3">
                        <h4 class="ps-product__heading">Event Address</h4>
                        <p>{!! $event->event_address !!}</p>
  
                        <h4 class="ps-product__heading pt-3">Terms & Conditions</h4>
                        <div class="ps-document">
                            {!! $event->terms_conditions !!}
                        </div>
                    </div>

                </div>
            </div>
            
        </div>
    </div>
</section>
@endsection
