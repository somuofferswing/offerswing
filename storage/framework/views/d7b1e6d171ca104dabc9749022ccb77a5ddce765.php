<?php $__env->startSection('content'); ?>

    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/dropzone/5.5.1/dropzone.css">

    <link rel="stylesheet" href="<?php echo e(asset('adminlte/plugins/timepicker/bootstrap-timepicker.min.css')); ?>">

    <style>

        #map {
            height: 500px;
        }

    </style>

    <div class="content-wrapper">

        <section class="content-header">

            <h1>

                Shop

                <small>Add Shop</small>

            </h1>

            <!-- <ol class="breadcrumb">

                  <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>

                  <li class="active">Dashboard</li>

                </ol> -->

        </section>


        <section class="content">

            

            <div class="box box-info">

                <div class="box-header with-border">

                    <h3 class="box-title">Add Shop</h3>

                </div>

                <div class="box-body" style="padding: 10px 50px;">

                    <form class="form-horizontal" method="POST" action="<?php echo e(route('store-shop')); ?>"

                          enctype="multipart/form-data">

                        <?php echo csrf_field(); ?>

                        <input type="hidden" name="imageupload_ids" id="images_id">


                        <h3>Shop Owner Details</h3>


                        <select class="form-control" name="owner_id" id="parent_id" required>

                            <?php $__currentLoopData = $retailers; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $item): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>

                                <option value="<?php echo e($item->id); ?>"><?php echo e($item->first_name); ?> <?php echo e($item->last_name); ?>

                                    (<?php echo e($item->email); ?>)
                                </option>

                            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>

                        </select>


                        <h3>Shop Details</h3>


                        <hr>


                        <div class="form-group">

                            <label for="shop_name" class="col-sm-1 control-label">Shop Name</label>

                            <div class="col-sm-11">

                                <input id="shop_name" type="text" class="form-control" name="name"
                                       value="<?php echo e(old('name')); ?>" required

                                       autocomplete="name" autofocus>

                            </div>

                        </div>

                        <div class="form-group">

                            <label for="email" class="col-sm-1  control-label">Email</label>

                            <div class="col-sm-11">

                                <input id="email" type="email" class="form-control" name="email"
                                       value="<?php echo e(old('email')); ?>" required

                                       autocomplete="email" autofocus>

                                <?php if ($errors->has('email')) :
if (isset($message)) { $messageCache = $message; }
$message = $errors->first('email'); ?>

                                <span class="text-danger"><?php echo e($message); ?></span>

                                <?php unset($message);
if (isset($messageCache)) { $message = $messageCache; }
endif; ?>

                            </div>


                        </div>

                        <div class="form-group">

                            <label for="mobile" class="col-sm-1  control-label">Phone</label>

                            <div class="col-sm-11">

                                <input id="mobile" type="text" class="form-control" name="mobile"
                                       value="<?php echo e(old('mobile')); ?>" required

                                       autocomplete="Phone" autofocus>

                                <?php if ($errors->has('mobile')) :
if (isset($message)) { $messageCache = $message; }
$message = $errors->first('mobile'); ?>

                                <span class="text-danger"><?php echo e($message); ?></span>

                                <?php unset($message);
if (isset($messageCache)) { $message = $messageCache; }
endif; ?>

                            </div>


                        </div>


                        <div class="form-group">

                            <label for="mobile2" class="col-sm-1  control-label">Phone 2</label>

                            <div class="col-sm-11">

                                <input id="mobile2" type="text" class="form-control" name="phone_number2"
                                       value="<?php echo e(old('phone_number2')); ?>"

                                       autocomplete="Phone 2" autofocus>

                                <?php if ($errors->has('mobile')) :
if (isset($message)) { $messageCache = $message; }
$message = $errors->first('mobile'); ?>

                                <span class="text-danger"><?php echo e($message); ?></span>

                                <?php unset($message);
if (isset($messageCache)) { $message = $messageCache; }
endif; ?>

                            </div>


                        </div>

                        <div class="form-group">

                            <label for="mobile3" class="col-sm-1  control-label">Whatsapp Number</label>

                            <div class="col-sm-11">

                                <input id="mobile3" type="text" class="form-control" name="phone_number3"
                                       value="<?php echo e(old('phone_number3')); ?>"

                                       autocomplete="Whatsapp Number" autofocus>

                                <?php if ($errors->has('mobile')) :
if (isset($message)) { $messageCache = $message; }
$message = $errors->first('mobile'); ?>

                                <span class="text-danger"><?php echo e($message); ?></span>

                                <?php unset($message);
if (isset($messageCache)) { $message = $messageCache; }
endif; ?>

                            </div>


                        </div>


                        <div class="form-group">

                            <label for="shop_name" class="col-sm-1 control-label">Category</label>

                            <div class="col-sm-11">

                                <select class="form-control select2" name="category[]" id="shop_category" required
                                        multiple>

                                    <option value="">-- Select --</option>

                                </select>

                            </div>

                        </div>

                        <div class="form-group">

                            <label for="shop_description" class="col-sm-1 control-label">Shop Description</label>

                            <div class="col-sm-11">

        <textarea id="shop_description" type="text" class="form-control" name="description" autofocus

                  rows="6"> <?php echo e(old('description')); ?></textarea>

                            </div>

                        </div>
                        <br>
                        <div class="form-group">

                            <label for="facebook" class="col-sm-1  control-label">Facebook page link</label>

                            <div class="col-sm-11">

                                <input id="facebook" type="text" class="form-control" name="facebook_link"
                                       value="<?php echo e(old('facebook_link')); ?>"

                                       autocomplete="facebook_link" autofocus>

                                <?php if ($errors->has('facebook_link')) :
if (isset($message)) { $messageCache = $message; }
$message = $errors->first('facebook_link'); ?>

                                <span class="text-danger"><?php echo e($message); ?></span>

                                <?php unset($message);
if (isset($messageCache)) { $message = $messageCache; }
endif; ?>

                            </div>


                        </div>

                        <div class="form-group">

                            <label for="twitter" class="col-sm-1  control-label">Twitter page link</label>

                            <div class="col-sm-11">

                                <input id="twitter" type="text" class="form-control" name="twitter_link"
                                       value="<?php echo e(old('twitter_link')); ?>"

                                       autocomplete="twitter_link" autofocus>

                                <?php if ($errors->has('twitter_link')) :
if (isset($message)) { $messageCache = $message; }
$message = $errors->first('twitter_link'); ?>

                                <span class="text-danger"><?php echo e($message); ?></span>

                                <?php unset($message);
if (isset($messageCache)) { $message = $messageCache; }
endif; ?>

                            </div>


                        </div>
                        <div class="form-group">

                            <label for="instagram" class="col-sm-1  control-label">Facebook page link</label>

                            <div class="col-sm-11">

                                <input id="instagram" type="text" class="form-control" name="instagram_link"
                                       value="<?php echo e(old('instagram_link')); ?>"

                                       autocomplete="instagram_link" autofocus>

                                <?php if ($errors->has('instagram_link')) :
if (isset($message)) { $messageCache = $message; }
$message = $errors->first('instagram_link'); ?>

                                <span class="text-danger"><?php echo e($message); ?></span>

                                <?php unset($message);
if (isset($messageCache)) { $message = $messageCache; }
endif; ?>

                            </div>


                        </div>

                        <br>

                        <div class="form-group">

                            <label for="amenity" class="col-sm-1 control-label">Amenities</label>

                            <div class="col-sm-11">

                                <select class="form-control select2" name="amenities[]" id="amenity" required
                                        multiple>

                                    <option value="">-- Select --</option>
                                    <?php $__currentLoopData = $amenities; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $amenity): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                        <option value="<?php echo e($amenity->id); ?>"><?php echo e($amenity->name); ?></option>
                                    <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>

                                </select>

                            </div>

                        </div>

                        <div class="form-group">

                            <label for="payment" class="col-sm-1 control-label">Shop Payment Method</label>

                            <div class="col-sm-11">

                                <select class="form-control select2" name="payment_types[]" id="payment" required
                                        multiple>

                                    <option value="">-- Select --</option>
                                    <?php $__currentLoopData = config('global.payment_types'); $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $payment): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                        <option value="<?php echo e($payment['name']); ?>"><?php echo e($payment['name']); ?></option>
                                    <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>

                                </select>

                            </div>

                        </div>


                        <div class="row">

                            <div class="col-sm-6">

                                <div class="form-group">

                                    <label for="address" class="col-sm-2 control-label">Address</label>

                                    <div class="col-sm-10">

                <textarea id="address" type="text" class="form-control" name="address" required autocomplete="address"

                          autofocus> <?php echo e(old('address')); ?></textarea>

                                    </div>

                                </div>

                                <div class="form-group">

                                    <label for="city" class="col-sm-2  control-label">City</label>

                                    <div class="col-sm-10">

                                        <input id="city" type="text" class="form-control" name="city"
                                               value="<?php echo e(old('city')); ?>" required

                                               autocomplete="city" autofocus>

                                    </div>

                                </div>

                                <div class="form-group">


                                    <label for="state" class="col-sm-2  control-label">State</label>

                                    <div class="col-sm-10">

                                        <input id="state" type="text" class="form-control" name="state"
                                               value="<?php echo e(old('state')); ?>" required

                                               autocomplete="state" autofocus>

                                    </div>


                                </div>

                                <div class="form-group">

                                    <label for="pincode" class="col-sm-2  control-label">Pincode</label>

                                    <div class="col-sm-10">

                                        <input id="pincode" type="text" class="form-control" name="pincode"
                                               value="<?php echo e(old('pincode')); ?>"

                                               required autocomplete="pincode" autofocus>

                                    </div>

                                </div>

                                <div class="form-group">

                                    <label for="lat" class="col-sm-2  control-label">Latitude</label>

                                    <div class="col-sm-10">

                                        <input id="lat" type="text" class="form-control" name="lat"
                                               value="<?php echo e(old('lat')); ?>" required

                                               autocomplete="lat" autofocus>

                                    </div>

                                </div>

                                <div class="form-group">

                                    <label for="lng" class="col-sm-2  control-label">Logitude</label>

                                    <div class="col-sm-10">

                                        <input id="lng" type="text" class="form-control" name="lng"
                                               value="<?php echo e(old('lng')); ?>" required

                                               autocomplete="lng" autofocus>

                                    </div>

                                </div>

                            </div>


                            <div class="col-sm-6">

                                <div class="form-group">

                                    <label for="lng" class="col-sm-2  control-label">Active days</label>

                                    <div class="col-sm-10">

                                        <div class="checkbox">

                                            <label class="col-md-4">

                                                <input type="checkbox" name="days[]" value="2">Monday

                                            </label>

                                            <div class="col-md-8">

                                                <input type="text" class="time col-md-5 2" disabled value="start"
                                                       name="mon_start"> &nbsp;&nbsp;

                                                <input type="text" class="time col-md-5 2" disabled value="end"
                                                       name="mon_end">

                                            </div>

                                        </div>

                                        <div class="checkbox">

                                            <label class="col-md-4">

                                                <input type="checkbox" name="days[]" value="3">Tuesday

                                            </label>

                                            <div class="col-md-8">

                                                <input type="text" class="col-md-5 time 3" disabled value="start"
                                                       name="tue_start">&nbsp;&nbsp;

                                                <input type="text" class="col-md-5 time 3" disabled value="end"
                                                       name="tue_end">

                                            </div>

                                        </div>

                                        <div class="checkbox">

                                            <label class="col-md-4">

                                                <input type="checkbox" name="days[]" value="4">Wednesday

                                            </label>

                                            <div class="col-md-8">

                                                <input type="text" class="col-md-5 time 4" disabled value="start"
                                                       name="wed_start">&nbsp;&nbsp;

                                                <input type="text" class="col-md-5 time 4" disabled value="end"
                                                       name="wed_end">

                                            </div>

                                        </div>

                                        <div class="checkbox">

                                            <label class="col-md-4">

                                                <input type="checkbox" name="days[]" value="5">Thursday

                                            </label>

                                            <div class="col-md-8">

                                                <input type="text" class="col-md-5 time 5" disabled value="start"
                                                       name="thur_start">&nbsp;&nbsp;

                                                <input type="text" class="col-md-5 time 5" disabled value="end"
                                                       name="thur_end">

                                            </div>

                                        </div>

                                        <div class="checkbox">

                                            <label class="col-md-4">

                                                <input type="checkbox" name="days[]" value="6">Friday

                                            </label>

                                            <div class="col-md-8">

                                                <input type="text" class="col-md-5 time 6" disabled value="start"
                                                       name="fri_start">&nbsp;&nbsp;

                                                <input type="text" class="col-md-5 time 6" disabled value="end"
                                                       name="fri_end">

                                            </div>

                                        </div>

                                        <div class="checkbox">

                                            <label class="col-md-4">

                                                <input type="checkbox" name="days[]" value="7">Saturday

                                            </label>

                                            <div class="col-md-8">

                                                <input type="text" class="col-md-5 time 7" disabled value="start"
                                                       name="sat_start">&nbsp;&nbsp;

                                                <input type="text" class="col-md-5 time 7" disabled value="end"
                                                       name="sat_end">

                                            </div>

                                        </div>

                                        <div class="checkbox">

                                            <label class="col-md-4">

                                                <input type="checkbox" name="days[]" value="1">Sunday

                                            </label>

                                            <div class="col-md-8">

                                                <input type="text" class="col-md-5 time 1" disabled value="start"
                                                       name="sun_start">&nbsp;&nbsp;

                                                <input type="text" class="col-md-5 time 1" disabled value="end"
                                                       name="sun_end">

                                            </div>

                                        </div>


                                    </div>

                                </div>

                                <div class="form-group">

                                    <label for="lng" class="col-sm-2  control-label">Rating</label>

                                    <div class="col-sm-10">

                                        <input type="number" class="form-control " name="shop_rating" min="1" max=5
                                               step="0.5">

                                    </div>

                                </div>

                            </div>

                        </div>


                        <div>

                            <div id="map"></div>

                            <div id="infowindow-content">

                                <img src="" width="16" height="16" id="place-icon">

                                <span id="place-name" class="title"></span><br>

                                <span id="place-address"></span>

                            </div>

                        </div>


                        <hr>

                        <h3>Upload Shop Images</h3>

                        <div class="form-group" style="margin:0px;">

                            <div class="dropzone" id="mydropzone">

                            </div>

                        </div>

                        <hr>

                        <div>

                            <div class="form-group">

                                <label for="bank_name" class="col-sm-2 control-label">Menus Present:</label>

                                <div class="col-sm-10">

                                    <input type="radio" name="has_menus" value="1">Yes

                                    <input type="radio" name="has_menus" value="0">No

                                </div>

                            </div>


                            <div id="shop_menus" style="display:none">

                                <h3>Upload Shop Menus</h3>

                                <div class="form-group" style="margin:0px;">

                                    <div class="dropzone" id="mydropzone2">

                                    </div>

                                </div>

                            </div>

                        </div>
                        <hr>

                        <div class="form-group">
                            <label for="ad_image" class="col-sm-2 control-label">Ad Image</label>
                            <div class="col-sm-10">
                                <input type="file" class="form-control-file" id="ad_image" name="ad_image"
                                       onchange="loadFile(event)">
                                <img id="output" height="100px" width="100px" />
                            </div>
                        </div>

                        <div class="form-group">

                            <label for="video_link" class="col-sm-1  control-label">Video Link</label>

                            <div class="col-sm-11">

                                <input id="video_link" type="text" class="form-control" name="video_link"
                                       value="<?php echo e(old('video_link')); ?>"

                                       autocomplete="video_link" autofocus>

                                <?php if ($errors->has('video_link')) :
if (isset($message)) { $messageCache = $message; }
$message = $errors->first('video_link'); ?>

                                <span class="text-danger"><?php echo e($message); ?></span>

                                <?php unset($message);
if (isset($messageCache)) { $message = $messageCache; }
endif; ?>

                            </div>


                        </div>

                        <hr>
                        <div class=" pull-right">

                            <button type="submit" class="btn btn-info "><?php echo e(__('Add Shop')); ?></button>

                            <button type="button" class="btn btn-danger "><?php echo e(__('Cancel')); ?></button>


                        </div>


                    </form>

                </div>

            </div>

            

        </section>

    </div>

<?php $__env->stopSection(); ?>



<?php $__env->startSection('scripts'); ?>

    <script>
        var loadFile = function(event) {
            var output = document.getElementById('output');
            output.src = URL.createObjectURL(event.target.files[0]);
        };
    </script>

    <script src="https://cdnjs.cloudflare.com/ajax/libs/dropzone/5.5.1/dropzone.js"></script>

    <script src="<?php echo e(asset('adminlte/plugins/timepicker/bootstrap-timepicker.min.js')); ?>"></script>

    <script src="<?php echo e(asset('adminlte/bower_components/ckeditor/ckeditor.js')); ?>"></script>
    <script src="<?php echo e(asset('adminlte/bower_components/select2/dist/js/select2.full.min.js')); ?>"></script>

    <script>

        $(document).ready(function () {
            $('.select2').select2();

            var data = <?php echo json_encode($categories); ?>;


            data.forEach(element => {

                id = element.id;

                value = element.category_name;

                $('#shop_category').append(`<option value="${id}">

                                       ${value}

                                  </option>`);

                if (element.children_recursive) {
                    rloop(element.children_recursive, 1);
                }

            });


            function rloop(data2, i) {


                var prefix = '';

                for (let j = 0; j < i; j++) {

                    prefix += "-";

                }

                data2.forEach(element => {

                    id = element.id;

                    value = element.category_name;

                    $('#shop_category').append(`<option value="${id}">

                                      ${prefix} ${value}

                                  </option>`);

                    i++;

                    if (element.children_recursive) {
                        rloop(element.children_recursive, i);
                    }

                });

            }


        });


        $('input[type="checkbox"]').click(function () {

            var checkbox_value = $(this).val();

            if ($(this).prop("checked") == true) {

                $('.' + checkbox_value).attr('disabled', false);

            } else {

                $('.' + checkbox_value).attr('disabled', true);

            }

        });


        $(function () {

            CKEDITOR.replace('shop_description')

        });

        function initMap() {

            var map = new google.maps.Map(document.getElementById('map'), {

                center: {lat: 18.6177, lng: 84.0167},

                zoom: 6

            });

            var input = document.getElementById('address');

            var autocomplete = new google.maps.places.Autocomplete(input);


            autocomplete.bindTo('bounds', map);


            // Set the data fields to return when the user selects a place.

            autocomplete.setFields(
                ['address_components', 'geometry', 'icon', 'name']);


            var infowindow = new google.maps.InfoWindow();

            var infowindowContent = document.getElementById('infowindow-content');

            infowindow.setContent(infowindowContent);

            var marker = new google.maps.Marker({

                map: map,

                anchorPoint: new google.maps.Point(0, -29)

            });


            autocomplete.addListener('place_changed', function () {

                infowindow.close();

                marker.setVisible(false);

                var place = autocomplete.getPlace();

                console.log(place);

                if (!place.geometry) {

                    window.alert("No details available for input: '" + place.name + "'");

                    return;

                }


                if (place.geometry.viewport) {

                    map.fitBounds(place.geometry.viewport);

                } else {

                    map.setCenter(place.geometry.location);

                    map.setZoom(17);

                }

                marker.setPosition(place.geometry.location);

                marker.setVisible(true);

                $('#lat').val(place.geometry.location.lat());

                $('#lng').val(place.geometry.location.lng());


                var address = '';

                if (place.address_components) {

                    address = [

                        (place.address_components[0] && place.address_components[0].short_name || ''),

                        (place.address_components[1] && place.address_components[1].short_name || ''),

                        (place.address_components[2] && place.address_components[2].short_name || '')

                    ].join(' ');

                }


                infowindowContent.children['place-icon'].src = place.icon;

                infowindowContent.children['place-name'].textContent = place.name;

                infowindowContent.children['place-address'].textContent = address;

                infowindow.open(map, marker);


                $.each(place.address_components, function (i, address_component) {

                    if (address_component.types[0] == "locality") {

                        $("#city").val(address_component.long_name);

                    }


                    if (address_component.types[0] == "administrative_area_level_1") {

                        $("#state").val(address_component.long_name);

                    }


                    if (address_component.types[0] == "postal_code") {

                        $("#pincode").val(address_component.long_name);

                    }

                });


            });

        }

    </script>

    <script

        src="http://maps.googleapis.com/maps/api/js?key=AIzaSyBHLFZ4C3XB7MDwjywzpBC1ZWMH0u2zuE4&libraries=places&callback=initMap"

        async defer>

    </script>



    <script type="text/javascript">

        Dropzone.autoDiscover = false;

        var myDropzone = new Dropzone("div#mydropzone", {

            url: "<?php echo e(url('image/upload/store')); ?>",

            params: {'type': 'shop_image'},

            headers: {

                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')

            },

            maxFiles: 10,

            maxFilesize: 12,

            renameFile: function (file) {

                let dt = new Date();

                let time = dt.getTime();

                return time + file.name;

            },

            acceptedFiles: ".jpeg,.jpg,.png,.gif",

            addRemoveLinks: true,

            removedfile: function (file) {

                let name = file.upload.filename;

                $.ajax({

                    headers: {

                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')

                    },

                    type: 'POST',

                    url: '<?php echo e(url("image/delete")); ?>',

                    data: {filename: name},

                    success: function (data) {

                        console.log(data.id);

                        let id = data.id;

                        let img_ids = $('#images_id').val().split(',').map(Number);

                        console.log(img_ids.indexOf(id));

                        img_ids.splice(img_ids.indexOf(id), 1);

                        console.log(img_ids);

                        $('#images_id').val(img_ids.join());

                        console.log($('#images_id').val());

                        console.log("File has been successfully removed!!");

                    },

                    error: function (e) {

                        console.log(e);

                    }
                });

                let fileRef;

                return (fileRef = file.previewElement) != null ?

                    fileRef.parentNode.removeChild(file.previewElement) : void 0;

            },


            success: function (file, response) {

                console.log(response);

                let val = $('#images_id').val();

                if (val == '') {

                    val = response.id;

                } else {

                    val = val + "," + response.id;

                }

                $('#images_id').val(val);

            },

            error: function (file, response) {

                return false;

            }

        });


        var myDropzone2 = new Dropzone("div#mydropzone2", {

            url: "<?php echo e(url('image/upload/store')); ?>",

            params: {'type': 'shop_menu'},

            headers: {

                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')

            },

            maxFiles: 10,

            maxFilesize: 12,

            renameFile: function (file) {

                let dt = new Date();

                let time = dt.getTime();

                return time + file.name;

            },

            acceptedFiles: ".jpeg,.jpg,.png,.gif",

            addRemoveLinks: true,

            removedfile: function (file) {

                let name = file.upload.filename;

                $.ajax({

                    headers: {

                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')

                    },

                    type: 'POST',

                    url: '<?php echo e(url("image/delete")); ?>',

                    data: {filename: name},

                    success: function (data) {

                        console.log(data.id);

                        let id = data.id;

                        let img_ids = $('#images_id').val().split(',').map(Number);

                        console.log(img_ids.indexOf(id));

                        img_ids.splice(img_ids.indexOf(id), 1);

                        console.log(img_ids);

                        $('#images_id').val(img_ids.join());

                        console.log($('#images_id').val());

                        console.log("File has been successfully removed!!");

                    },

                    error: function (e) {

                        console.log(e);

                    }
                });

                let fileRef;

                return (fileRef = file.previewElement) != null ?

                    fileRef.parentNode.removeChild(file.previewElement) : void 0;

            },


            success: function (file, response) {

                console.log(response);

                let val = $('#images_id').val();

                if (val == '') {

                    val = response.id;

                } else {

                    val = val + "," + response.id;

                }

                $('#images_id').val(val);

            },

            error: function (file, response) {

                return false;

            }

        });


        $('.time').timepicker({

            showInputs: false

        });


        // $('#to').timepicker({

        //      showInputs: false

        // });


    </script>

    <script>

        $('input:radio[name=has_menus]').change(function () {

            if (this.value == 1) {

                // alert("Allot Thai Gayo Bhai");

                $('#shop_menus').show();

            } else if (this.value == 0) {

                // alert("Transfer Thai Gayo");


                $('#shop_menus').val('');

                $('#shop_menus').hide();

            }

        });

    </script>

<?php $__env->stopSection(); ?>

<?php echo $__env->make('layouts.admin', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH C:\wamp64\www\offerswing_test\resources\views/backend/shop/create.blade.php ENDPATH**/ ?>