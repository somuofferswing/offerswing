<?php

namespace App\Http\Controllers\Backend;

use App\Http\Controllers\Controller;
use App\Models\Category;
use App\Models\Claimed;
use App\Models\Days;
use App\Models\Deal;
use App\Models\OfferNotification;
use App\Models\Order;
use App\Models\OrderDetail;
use App\Models\Shop;
use App\Models\ShopOwner;
use App\Models\WingPoints;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Storage;

class DealsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index(Request $request)
    {
        if (1 == Auth::user()->user_type) {
            $deal = Deal::select('*', 'deals.id as deal_id')
                ->join('shops as s', 's.id', '=', 'deals.shop_id');

            if ($request['shop_id'] && !empty($request['shop_id'])) {
                $deal->where('shop_id', $request['shop_id']);
            }

            if ($request['category_id'] && !empty($request['category_id'])) {
                $deal->where('category_id', $request['category_id']);
            }
            $deals = $deal->get();
            $shops = Shop::all();
            $category = Category::getCategories();

            return view('backend.deals.index', compact('deals', 'shops', 'category'));
        }
        if (3 == Auth::user()->user_type) {
            $user = auth()->user();
            $user_id = $user['id'];

            $id = $request['id'];
            echo "<pre>";
            \print_r($id);
            die;

            $shopowner = $user->shops;

            // $shopowner = ShopOwner::where('retailer_id', '=', $user_id)->first();
            $shop_id = $shopowner['shop_id'];
            $deals = Deal::select('*', 'deals.id as deal_id')->leftjoin('shops', 'shops.id', '=', 'deals.shop_id')->where('deals.shop_id', '=', $shop_id)->get();
            $category = Category::getCategories();

            return view('backend.deals.index', compact('deals', 'category'));
        }
    }

    public function shopDeals($id)
    {
        $user = auth()->user();

        // $shop_owners = ShopOwner::where('retailer_id', $user['id'])->where('shop_id', $id)->first();

        $deals = Deal::select('*', 'deals.id as deal_id')->leftjoin('shops', 'shops.id', '=', 'deals.shop_id')->where('deals.shop_id', '=', $id)->get();

        $shops = Shop::all();
        $category = Category::getCategories();

        return view('backend.deals.index', compact('deals', 'shops', 'category'));
    }

    public function viewShopDeal($id)
    {
        $user = auth()->user();
        $deal = Deal::find($id);

        $OrderDetail = OrderDetail::where(['order_type'=>'deal','order_type_id'=> $id])->get();

        return view('backend.deals.show', compact('deal', 'OrderDetail'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function create()
    {
        $user = Auth::user();
        $user_id = $user->id;

        $shops = Shop::all();
        $days = config('global.days');

        $category = Category::getCategories();

        return view('backend.deals.create', compact('user_id', 'shops', 'category', 'days'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @return Response
     */
    public function store(Request $request)
    {
        $deal = new Deal();
        $this->validate($request, [
            'name' => 'required',
            'description' => 'required',
            'price' => 'required',
            'payment_type' => 'required',
            'days_applicable' => 'required',
            'deal_start_date' => 'required',
            'deal_end_date' => 'required',
            'deal_validity_in_days' => 'required',
            'quantity' => 'required',
            'max_count_for_user' => 'required',
            'terms_conditions' => 'required',
            'original_price' => 'required',
        ]);

        $shop_details = Shop::find($request['shop_id']);

        $deal->name = $request['name'];
        $deal->description = $request['description'];
        $deal->payment_type = $request['payment_type'];
        // $deal->categories = $shop_details['categories'];
        $deal->shop_id = $request['shop_id'];
        $deal->days_applicable = implode(',', $request['days_applicable']);
        $deal->price = $request['price'];
        $deal->original_price = $request['original_price'];
        $deal->deal_start_date = $request['deal_start_date'];
        $deal->deal_end_date = $request['deal_end_date'];
        $deal->deal_validity_in_days = $request['deal_validity_in_days'];
        $deal->created_by = $request['created_by'];
        $deal->quantity = $request['quantity'];
        $deal->points_to_claim = $request['points_to_claim'];
        $deal->max_count_for_user = $request['max_count_for_user'];
        $deal->points_rewarded = $request['points_rewarded'];
        $deal->terms_conditions = $request['terms_conditions'];
        $deal->top = isset($request['top']) ? $request['top'] : 0;
        $deal->active = isset($request['active']) ? $request['active'] : 0;
        $deal->valid_for = implode(',', $request['valid_for']);
        
        $slug = $this->slugify($request['name']);
        $deal->slug = $slug;


        if ($request->hasFile('pic')) {
            $file = $request->file('pic');
            $fileName = $slug.'_'.time().'.'.$file->getClientOriginalExtension();
            $path = $file->storeAs('deals', $fileName, 'public');
            $deal->pic = $path;
        }

        $deal->save();

        return redirect('/deals')->with('success', 'Deal Ceated Successfully');
    }

    /**
     * Display the specified resource.
     *
     * @param int $id
     *
     * @return Response
     */
    public function show($id)
    {
        $deals = Deal::find($id);

        return view('backend.deals.show', compact('deals'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param int $id
     *
     * @return Response
     */
    public function edit($id)
    {
        $user = Auth::user();
        $user_id = $user->id;

        $shops = Shop::all();
        $days = config('global.days');

        $category = Category::getCategories();

        $deals = Deal::find($id);

        return view('backend.deals.edit', compact('user_id', 'shops', 'category', 'days', 'deals'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param int $id
     *
     * @return Response
     */
    public function update(Request $request, $id)
    {
        $deal = Deal::find($id);
        $this->validate($request, [
            'name' => 'required',
            'description' => 'required',
            'price' => 'required',
            'payment_type' => 'required',
            'days_applicable' => 'required',
            'deal_start_date' => 'required',
            'deal_end_date' => 'required',
            'deal_validity_in_days' => 'required',
            'quantity' => 'required',
            'max_count_for_user' => 'required',
            'terms_conditions' => 'required',
            'original_price' => 'required',
        ]);

        $shop_details = Shop::find($request['shop_id']);

        $deal->name = $request['name'];
        $deal->description = $request['description'];
        $deal->payment_type = $request['payment_type'];
        // $deal->categories = $shop_details['categories'];
        $deal->shop_id = $request['shop_id'];
        $deal->days_applicable = implode(',', $request['days_applicable']);
        $deal->price = $request['price'];
        $deal->original_price = $request['original_price'];
        $deal->deal_start_date = $request['deal_start_date'];
        $deal->deal_end_date = $request['deal_end_date'];
        $deal->deal_validity_in_days = $request['deal_validity_in_days'];
        $deal->created_by = $request['created_by'];
        $deal->quantity = $request['quantity'];
        $deal->points_to_claim = $request['points_to_claim'];
        $deal->max_count_for_user = $request['max_count_for_user'];
        $deal->points_rewarded = $request['points_rewarded'];
        $deal->terms_conditions = $request['terms_conditions'];
        $deal->top = isset($request['top']) ? $request['top'] : 0;
        $deal->active =isset($request['active']) ? $request['active'] : 0;
        $deal->valid_for = implode(',', $request['valid_for']);

        if ($deal->isDirty('name')) {
            $slug = $this->slugify($request['name']);
            $deal->slug = $slug;
        } else {
            $slug = $deal->slug;
        }

        if ($request->hasFile('pic')) {
            Storage::disk('public')->delete($deal->pic);

            $file = $request->file('pic');
            $fileName = $slug.'_'.time().'.'.$file->getClientOriginalExtension();
            $path = $file->storeAs('deals', $fileName, 'public');
            $deal->pic = $path;
        }
        $deal->save();

        return redirect('/deals')->with('success', 'Deal Updated Successfully');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param int $id
     *
     * @return Response
     */
    public function destroy($id)
    {
        $deal = Deal::find($id);
        $deal->delete();

        return redirect('/deals')->with('success', 'Deal has been deleted Successfully');
    }

    public function slugify($str)
    {
        $search = ['Ș', 'Ț', 'ş', 'ţ', 'Ş', 'Ţ', 'ș', 'ț', 'î', 'â', 'ă', 'Î', 'Â', 'Ă', 'ë', 'Ë'];
        $replace = ['s', 't', 's', 't', 's', 't', 's', 't', 'i', 'a', 'a', 'i', 'a', 'a', 'e', 'E'];
        $str = str_ireplace($search, $replace, strtolower(trim($str)));
        $str = preg_replace('/[^\w\d\-\ ]/', '', $str);
        $str = str_replace(' ', '-', $str);

        return $this->getUniqueDealSlug(preg_replace('/\-{2,}/', '-', $str));
    }

    private function getUniqueDealSlug($slug)
    {
        $existingCount = Deal::where('slug', 'like', $slug . '-%')->count();

        if ($existingCount) {
            return $slug . '-' . ($existingCount);
        }

        return $slug;
    }
}
