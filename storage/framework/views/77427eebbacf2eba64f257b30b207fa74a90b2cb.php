<?php $__env->startSection('content'); ?>
<div class="ps-breadcrumb">
    <div class="ps-container">
        <ul class="breadcrumb text-capitalize" id="breadcrumb">
            <li><a href="<?php echo e(url('/')); ?>">Home</a></li>
            <li>My Orders</li>
        </ul>
    </div>
</div>

<section>
    <div class="ps-page--product">
        <div class="ps-container">

            <div class="ps-product--detail">
                <ul class="ps-tab-list">
                    <li class="active"><a href="#tab-deals">Deals</a></li>
                    <li><a href="#tab-events">Events</a></li>
                    <li><a href="#tab-gifts">Gifts</a></li>
                </ul>
                <div class="ps-tabs">

                    <!-- deals tab -->
                    <div class="ps-tab active" id="tab-deals">
                        <?php if($deal_orders->isNotEmpty()): ?>
                            <div class="row">
                            <?php $__currentLoopData = $deal_orders; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $item): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                            <div class="col-12 col-md-4">
                                <div class="card">
                                    <a href="<?php echo e(url('/orderdeal',["$item->order_id"])); ?>">
                                        <?php
                                        $deal = $item->deal;
                                        ?>
                                        <img class="card-img-top img-fluid" src="<?php echo asset("$deal->pic"); ?>"
                                            alt="Card image cap" style="height:200px;width:100%;">
                                        <div class="card-body">
                                            <h5 class="card-title"> <?php echo e(ucfirst($deal->name)); ?> </h5>
                                            <p class="card-text"> <?php echo $deal->description; ?> </p>
                                            <div class="row">
                                                <div>
                                                    Status:
                                                    <?php if($item->used == 0): ?>
                                                    Not Claimed
                                                    <?php else: ?>
                                                    Claimed
                                                    <?php endif; ?>
                                                </div>
                                                <div>
                                                    Expiration :
                                                    <?php if($deal->valid_upto >= date('Y-m-d', time())): ?>
                                                    <?php echo e($deal->valid_upto); ?>

                                                    <?php else: ?>
                                                    Expired
                                                    <?php endif; ?>
                                                </div>

                                            </div>
                                        </div>
                                    </a>
                                </div>
                            </div>
                            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                            </div>
                        <?php else: ?>
                        <p>No Deal Orders</p>
                        <?php endif; ?>
                    </div>
                    <!-- end of deals tab -->

                     <!-- events tab -->
                    <div class="ps-tab" id="tab-events">
                        <?php if($event_orders->isNotEmpty()): ?>
                            <div class="row">
                            <?php $__currentLoopData = $event_orders; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $item): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                            <div class="col-12 col-md-4">
                                <div class="card">
                                    <a href="<?php echo e(url('/orderevent',["$item->order_id"])); ?>">
                                        <?php
                                        $event = $item->event;
                                        ?>
                                        <img class="card-img-top img-fluid" src="<?php echo asset("$event->event_image"); ?>"
                                            alt="Card image cap" style="height:300px;width:100%;">
                                        <div class="card-body">
                                            <h5 class="card-title"> <?php echo e($event->title); ?> </h5>
                                            <div class="row">
                                                <div>
                                                    Event Date :
                                                    <?php echo e($event->event_date); ?>

                                                </div>
                                                <div>
                                                    Status:
                                                    <?php if($item->used == 0): ?>
                                                    Not Claimed
                                                    <?php else: ?>
                                                    Claimed
                                                    <?php endif; ?>
                                                </div>
                                            </div>
                                        </div>
                                    </a>
                                </div>
                            </div>
                            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                            </div>
                        <?php else: ?>
                        <p>No Events Orders</p>
                        <?php endif; ?>
                    </div>
                    <!-- end of events tab -->

                    <!-- gits tab -->
                    <div class="ps-tab" id="tab-gifts">
                        <?php if($gift_orders->isNotEmpty()): ?>
                            <div class="row">
                            <?php $__currentLoopData = $gift_orders; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $item): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                            <div class="dol-12 col-md-4">
                                <div class="card">
                                    <a href="<?php echo e(url('/ordergift',["$item->order_id"])); ?>">
                                        <?php
                                        $gift = $item->gift;
                                        ?>
                                        <img class="card-img-top img-fluid" src="<?php echo asset("$gift->image"); ?>"
                                            alt="Card image cap" style="height:300px;width:100%;">
                                        <div class="card-body">
                                            <h5 class="card-title"> <?php echo e($gift->gift_name); ?> </h5>
                                            <div class="row">
                                                <div>
                                                    Status:
                                                    <?php if($item->used == 0): ?>
                                                    Not Claimed
                                                    <?php else: ?>
                                                    Claimed
                                                    <?php endif; ?>
                                                </div>
                                            </div>
                                        </div>
                                    </a>
                                </div>
                            </div>
                            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                            </div>
                        <?php else: ?>
                        <p> No Gifts Orders</p>
                        <?php endif; ?>
                    </div>
                    <!-- end of gits tab -->

                </div>
            </div>
        </div>
    </div>
</section>
<?php $__env->stopSection(); ?>
<?php echo $__env->make('layouts.app', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH C:\xampp\htdocs\offerswing\resources\views/order.blade.php ENDPATH**/ ?>